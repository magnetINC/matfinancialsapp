﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MATFinancialsI.DAL;
using System.Data;
using Entity;

namespace MATFinancialsI.BLL
{
    public class CallSP
    {
              
        public List<DataTable> GetListDetails(CategoryInfo infoCategory)
        {
            ListBoxFillSP spListBoxFill = new ListBoxFillSP();
            return spListBoxFill.GetList(infoCategory);
        }

        public List<DataTable> GetPieChartTableDetails(CategoryInfo infoCategory)
        {
            PieChartFillSP spPieChartFill = new PieChartFillSP();
            return spPieChartFill.GetPieChartTable(infoCategory);
        }

        public List<DataTable> GetPieChartDetails(CategoryInfo infoCategory)
        {
            PieChartFillSP spPieChartFill = new PieChartFillSP();
            return spPieChartFill.GetPieChart(infoCategory);
        }

        public List<DataTable> GetTopBarChartDetails()
        {
            BarChartFillSP spBarChartFill = new BarChartFillSP();
            return spBarChartFill.GetBarTopChart();
        }

        public List<DataTable> GetAccountGroupBarChartDetails()
        {
            BarChartFillSP spBarChartFill = new BarChartFillSP();
            return spBarChartFill.GetBarAccountGroupChart();
        }

        public List<DataTable> GetBarChartColorDetails(CategoryInfo CategoryInfo)
        {
            BarChartFillSP spBarChartFill = new BarChartFillSP();
            return spBarChartFill.GetBarColor(CategoryInfo);
        }
        public String GetCompanyNameDetails()
        {
            CompanyNameSP spCompanyName = new CompanyNameSP();
            return spCompanyName.GetCompanyName();
        }

        public DateTime GetFromDate()
        {
            FinancialDate spFinancialDate = new FinancialDate();
            return spFinancialDate.GetFromDate();
        }

        public DateTime GetToDate()
        {
            FinancialDate spFinancialDate = new FinancialDate();
            return spFinancialDate.GetToDate();
        }

        public DateTime GetLastDate()
        {
            GetLastDate spFinancialDate = new GetLastDate();
            return spFinancialDate.GetLastDay();
        }

        public List<DataTable> GetPlotterFill(CategoryInfo CategoryInfo)
        {
            PlotterFillSP spPlotterFill = new PlotterFillSP();
            return spPlotterFill.GetPlotterData(CategoryInfo);
        }
         public List<DataTable> GetBudgetNameFill()
        {
            GetBudgetNames spGetBudgetName = new GetBudgetNames();
            return spGetBudgetName.GetBudgetName();
        }

         public List<DataTable> GetViewDetailsFill()
         {
             ViewDetailsFillSP spDetailsFill = new ViewDetailsFillSP();
             return spDetailsFill.GetViewDetailsData();
         }

         public List<DataTable> GetCompanyName()
         {
             SelectCompany spDetailsFill = new SelectCompany();
             return spDetailsFill.GetCompanyNames();
         }
         public bool GetLoginCheck(string strUseName, string strPassword)
         {
             LoginCheck spLoginChecking = new LoginCheck();
             return spLoginChecking.CheckUserAndPassord(strUseName, strPassword);
         }

    }
}
