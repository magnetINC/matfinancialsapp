﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace MATFinancials
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                try
                {
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, "======= Program Startup =========");

                    var objKeyValidation = new MATFinancials.keys.KeyEntryForm();
                    if (!objKeyValidation.IsValidKey) objKeyValidation.ShowDialog();
                    if (!objKeyValidation.IsValidKey)
                    {
                        GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Begin:Key Validation"));
                        Application.Exit();
                        Environment.ExitCode = 0;
                        Environment.Exit(0);
                        GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("End:Key Validation"));
                    }
                    else
                    {

                    }

                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Begin:Startup Splash"));
                    Application.Run(new frmSplash());
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("End:Startup Splash"));
                }
                catch (Exception e)
                {
                    Application.Exit();
                    Environment.ExitCode = 0;
                    Environment.Exit(0);
                }

                GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Startup Here 1"));

                if (Environment.ExitCode == 0) { Application.Exit(); }
                else if (Environment.ExitCode == 565568)
                {
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Startup Here 2"));
                    Application.Run(new formMDI());
                }
                else
                {
                    string userId = (ConfigurationManager.AppSettings["MsSqlUserId"] == null || ConfigurationManager.AppSettings["MsSqlUserId"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlUserId"].ToString();
                    
                    string password = (ConfigurationManager.AppSettings["MsSqlPassword"] == null || ConfigurationManager.AppSettings["MsSqlPassword"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlPassword"].ToString();

                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Begin:Startup Splash pre-frmMsSqlInstaller"));

                    if (password == null)
                    {
                        GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Begin: Startup frmMsSqlInstaller"));
                        Application.Run(new frmMsSqlInstallerforMatFinancials());
                        GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("End: Startup frmMsSqlInstaller"));

                    }
                    else
                    {
                        Login obj = new Login(userId, password);
                        if (obj.ShowDialog() == DialogResult.OK)
                        {
                            Application.Run(new frmMsSqlInstallerforMatFinancials());
                        }
                    }

                }

                GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Startup Here 3"));

                if (Environment.ExitCode == 100)
                {
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, string.Format("Startup Here 4"));
                    Application.Run(new formMDI());
                }
                else if (Environment.ExitCode == 101)
                {
                    MessageBox.Show("MAT Financials database configuration process error. there have some privileges issue!. Pease contact Administrator", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (Environment.ExitCode == 102)
                {
                    MessageBox.Show("MAT Financials database configuration process error. can't get database files!. Pease contact Administrator", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            //
            finally { }
        }

    }
}
