﻿namespace MATFinancials
{
    partial class frmReceiptVoucher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReceiptVoucher));
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.cbxPrintafterSave = new System.Windows.Forms.CheckBox();
            this.lblNarration = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.cmbCashOrBank = new System.Windows.Forms.ComboBox();
            this.lblBankOrCash = new System.Windows.Forms.Label();
            this.lnklblRemove = new System.Windows.Forms.LinkLabel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.btnLedgerAdd = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.dgvReceiptVoucher = new MATFinancials.dgv.DataGridViewEnter();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvtxtSlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtreceiptMasterId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtreceiptDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtledgerPostingId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbAccountLedger = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvbtnAgainst = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvtxtAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbCurrency = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvCmbWithHoldingTax = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvTxtWithholdingTaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtChequeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbProject = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvCmbCategory = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lblPaymentAmt = new System.Windows.Forms.Label();
            this.txtPaymentAmt = new System.Windows.Forms.TextBox();
            this.lblWithHoldingTax = new System.Windows.Forms.Label();
            this.txtWithHoldingTax = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiptVoucher)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(804, 445);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 13);
            this.lblTotal.TabIndex = 496;
            this.lblTotal.Text = "Total";
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(866, 441);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotal.Size = new System.Drawing.Size(200, 20);
            this.txtTotal.TabIndex = 4543;
            // 
            // cbxPrintafterSave
            // 
            this.cbxPrintafterSave.AutoSize = true;
            this.cbxPrintafterSave.BackColor = System.Drawing.Color.Transparent;
            this.cbxPrintafterSave.ForeColor = System.Drawing.Color.Black;
            this.cbxPrintafterSave.Location = new System.Drawing.Point(21, 492);
            this.cbxPrintafterSave.Margin = new System.Windows.Forms.Padding(5);
            this.cbxPrintafterSave.Name = "cbxPrintafterSave";
            this.cbxPrintafterSave.Size = new System.Drawing.Size(97, 17);
            this.cbxPrintafterSave.TabIndex = 6;
            this.cbxPrintafterSave.Text = "Print after save";
            this.cbxPrintafterSave.UseVisualStyleBackColor = false;
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(13, 431);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(50, 13);
            this.lblNarration.TabIndex = 493;
            this.lblNarration.Text = "Narration";
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(121, 431);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(209, 65);
            this.txtNarration.TabIndex = 5;
            this.txtNarration.Enter += new System.EventHandler(this.txtNarration_Enter);
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            this.txtNarration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNarration_KeyPress);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(754, 15);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 490;
            this.lblDate.Text = "Date";
            // 
            // cmbCashOrBank
            // 
            this.cmbCashOrBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrBank.FormattingEnabled = true;
            this.cmbCashOrBank.Location = new System.Drawing.Point(130, 35);
            this.cmbCashOrBank.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCashOrBank.Name = "cmbCashOrBank";
            this.cmbCashOrBank.Size = new System.Drawing.Size(200, 21);
            this.cmbCashOrBank.TabIndex = 2;
            this.cmbCashOrBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrBank_KeyDown);
            // 
            // lblBankOrCash
            // 
            this.lblBankOrCash.AutoSize = true;
            this.lblBankOrCash.BackColor = System.Drawing.Color.Transparent;
            this.lblBankOrCash.ForeColor = System.Drawing.Color.Black;
            this.lblBankOrCash.Location = new System.Drawing.Point(18, 39);
            this.lblBankOrCash.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBankOrCash.Name = "lblBankOrCash";
            this.lblBankOrCash.Size = new System.Drawing.Size(67, 13);
            this.lblBankOrCash.TabIndex = 488;
            this.lblBankOrCash.Text = "Bank / Cash";
            // 
            // lnklblRemove
            // 
            this.lnklblRemove.ActiveLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.AutoSize = true;
            this.lnklblRemove.LinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.Location = new System.Drawing.Point(1021, 369);
            this.lnklblRemove.Margin = new System.Windows.Forms.Padding(5);
            this.lnklblRemove.Name = "lnklblRemove";
            this.lnklblRemove.Size = new System.Drawing.Size(47, 13);
            this.lnklblRemove.TabIndex = 435;
            this.lnklblRemove.TabStop = true;
            this.lnklblRemove.Text = "Remove";
            this.lnklblRemove.VisitedLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblRemove_LinkClicked);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(983, 469);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(892, 469);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(801, 469);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 27);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(710, 469);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.BackColor = System.Drawing.Color.Transparent;
            this.lblVoucherNo.ForeColor = System.Drawing.Color.Black;
            this.lblVoucherNo.Location = new System.Drawing.Point(18, 14);
            this.lblVoucherNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(67, 13);
            this.lblVoucherNo.TabIndex = 482;
            this.lblVoucherNo.Text = "Voucher No.";
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.Location = new System.Drawing.Point(130, 10);
            this.txtVoucherNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.ReadOnly = true;
            this.txtVoucherNo.Size = new System.Drawing.Size(200, 20);
            this.txtVoucherNo.TabIndex = 0;
            this.txtVoucherNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVoucherNo_KeyDown);
            // 
            // btnLedgerAdd
            // 
            this.btnLedgerAdd.BackColor = System.Drawing.Color.Gray;
            this.btnLedgerAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLedgerAdd.FlatAppearance.BorderSize = 0;
            this.btnLedgerAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLedgerAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLedgerAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnLedgerAdd.Location = new System.Drawing.Point(352, 36);
            this.btnLedgerAdd.Margin = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.btnLedgerAdd.Name = "btnLedgerAdd";
            this.btnLedgerAdd.Size = new System.Drawing.Size(21, 20);
            this.btnLedgerAdd.TabIndex = 3;
            this.btnLedgerAdd.Text = "+";
            this.btnLedgerAdd.UseVisualStyleBackColor = false;
            this.btnLedgerAdd.Click += new System.EventHandler(this.btnLedgerAdd_Click);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(863, 11);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(181, 20);
            this.txtDate.TabIndex = 1;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(1041, 11);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(20, 20);
            this.dtpDate.TabIndex = 504;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // dgvReceiptVoucher
            // 
            this.dgvReceiptVoucher.AllowUserToResizeRows = false;
            this.dgvReceiptVoucher.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReceiptVoucher.BackgroundColor = System.Drawing.Color.White;
            this.dgvReceiptVoucher.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReceiptVoucher.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReceiptVoucher.ColumnHeadersHeight = 32;
            this.dgvReceiptVoucher.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvReceiptVoucher.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSlNo,
            this.dgvtxtreceiptMasterId,
            this.dgvtxtreceiptDetailsId,
            this.dgvtxtledgerPostingId,
            this.dgvcmbAccountLedger,
            this.dgvbtnAgainst,
            this.dgvtxtAmount,
            this.dgvcmbCurrency,
            this.dgvCmbWithHoldingTax,
            this.dgvTxtWithholdingTaxAmount,
            this.dgvtxtChequeNo,
            this.dgvtxtChequeDate,
            this.dgvCmbProject,
            this.dgvCmbCategory});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReceiptVoucher.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvReceiptVoucher.EnableHeadersVisualStyles = false;
            this.dgvReceiptVoucher.GridColor = System.Drawing.Color.DimGray;
            this.dgvReceiptVoucher.Location = new System.Drawing.Point(18, 63);
            this.dgvReceiptVoucher.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dgvReceiptVoucher.Name = "dgvReceiptVoucher";
            this.dgvReceiptVoucher.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReceiptVoucher.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvReceiptVoucher.Size = new System.Drawing.Size(1050, 304);
            this.dgvReceiptVoucher.TabIndex = 4;
            this.dgvReceiptVoucher.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvReceiptVoucher_CellBeginEdit);
            this.dgvReceiptVoucher.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceiptVoucher_CellClick);
            this.dgvReceiptVoucher.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceiptVoucher_CellEnter);
            this.dgvReceiptVoucher.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReceiptVoucher_CellValueChanged);
            this.dgvReceiptVoucher.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvReceiptVoucher_CurrentCellDirtyStateChanged);
            this.dgvReceiptVoucher.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvReceiptVoucher_DataError);
            this.dgvReceiptVoucher.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvReceiptVoucher_EditingControlShowing);
            this.dgvReceiptVoucher.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvReceiptVoucher_RowsAdded);
            this.dgvReceiptVoucher.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvReceiptVoucher_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(336, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 5, 5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 4544;
            this.label1.Text = "*";
            // 
            // dgvtxtSlNo
            // 
            this.dgvtxtSlNo.DataPropertyName = "SL.NO";
            this.dgvtxtSlNo.FillWeight = 34.51777F;
            this.dgvtxtSlNo.HeaderText = "Sl No.";
            this.dgvtxtSlNo.Name = "dgvtxtSlNo";
            this.dgvtxtSlNo.ReadOnly = true;
            this.dgvtxtSlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtreceiptMasterId
            // 
            this.dgvtxtreceiptMasterId.DataPropertyName = "receiptMasterId";
            this.dgvtxtreceiptMasterId.HeaderText = "receiptMasterId";
            this.dgvtxtreceiptMasterId.Name = "dgvtxtreceiptMasterId";
            this.dgvtxtreceiptMasterId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtreceiptMasterId.Visible = false;
            // 
            // dgvtxtreceiptDetailsId
            // 
            this.dgvtxtreceiptDetailsId.DataPropertyName = "receiptDetailsId";
            this.dgvtxtreceiptDetailsId.HeaderText = "receiptDetailsId";
            this.dgvtxtreceiptDetailsId.Name = "dgvtxtreceiptDetailsId";
            this.dgvtxtreceiptDetailsId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtreceiptDetailsId.Visible = false;
            // 
            // dgvtxtledgerPostingId
            // 
            this.dgvtxtledgerPostingId.DataPropertyName = "ledgerPostingId";
            this.dgvtxtledgerPostingId.HeaderText = "ledgerPostingId";
            this.dgvtxtledgerPostingId.Name = "dgvtxtledgerPostingId";
            this.dgvtxtledgerPostingId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtledgerPostingId.Visible = false;
            // 
            // dgvcmbAccountLedger
            // 
            this.dgvcmbAccountLedger.DataPropertyName = "ledgerId";
            this.dgvcmbAccountLedger.FillWeight = 101.9353F;
            this.dgvcmbAccountLedger.HeaderText = "Receive from";
            this.dgvcmbAccountLedger.Name = "dgvcmbAccountLedger";
            this.dgvcmbAccountLedger.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvbtnAgainst
            // 
            this.dgvbtnAgainst.DataPropertyName = "Against";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "Apply";
            this.dgvbtnAgainst.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvbtnAgainst.FillWeight = 101.9353F;
            this.dgvbtnAgainst.HeaderText = "";
            this.dgvbtnAgainst.Name = "dgvbtnAgainst";
            this.dgvbtnAgainst.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbtnAgainst.Text = "Against";
            // 
            // dgvtxtAmount
            // 
            this.dgvtxtAmount.DataPropertyName = "amount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dgvtxtAmount.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtAmount.FillWeight = 101.9353F;
            this.dgvtxtAmount.HeaderText = "Amount";
            this.dgvtxtAmount.MaxInputLength = 8;
            this.dgvtxtAmount.Name = "dgvtxtAmount";
            this.dgvtxtAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvcmbCurrency
            // 
            this.dgvcmbCurrency.DataPropertyName = "exchangeRateId";
            this.dgvcmbCurrency.FillWeight = 101.9353F;
            this.dgvcmbCurrency.HeaderText = "Currency";
            this.dgvcmbCurrency.Name = "dgvcmbCurrency";
            // 
            // dgvCmbWithHoldingTax
            // 
            this.dgvCmbWithHoldingTax.HeaderText = "WHT";
            this.dgvCmbWithHoldingTax.Name = "dgvCmbWithHoldingTax";
            // 
            // dgvTxtWithholdingTaxAmount
            // 
            this.dgvTxtWithholdingTaxAmount.HeaderText = "WHT Amount";
            this.dgvTxtWithholdingTaxAmount.Name = "dgvTxtWithholdingTaxAmount";
            this.dgvTxtWithholdingTaxAmount.Visible = false;
            // 
            // dgvtxtChequeNo
            // 
            this.dgvtxtChequeNo.DataPropertyName = "chequeNo";
            this.dgvtxtChequeNo.FillWeight = 101.9353F;
            this.dgvtxtChequeNo.HeaderText = "Cheque No./Ref. No.";
            this.dgvtxtChequeNo.Name = "dgvtxtChequeNo";
            this.dgvtxtChequeNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtChequeDate
            // 
            this.dgvtxtChequeDate.DataPropertyName = "chequeDate";
            this.dgvtxtChequeDate.FillWeight = 101.9353F;
            this.dgvtxtChequeDate.HeaderText = "Cheque Date";
            this.dgvtxtChequeDate.Name = "dgvtxtChequeDate";
            this.dgvtxtChequeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvCmbProject
            // 
            this.dgvCmbProject.FillWeight = 101.9353F;
            this.dgvCmbProject.HeaderText = "Project";
            this.dgvCmbProject.Name = "dgvCmbProject";
            // 
            // dgvCmbCategory
            // 
            this.dgvCmbCategory.FillWeight = 101.9353F;
            this.dgvCmbCategory.HeaderText = "Category";
            this.dgvCmbCategory.Name = "dgvCmbCategory";
            // 
            // lblPaymentAmt
            // 
            this.lblPaymentAmt.AutoSize = true;
            this.lblPaymentAmt.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentAmt.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentAmt.Location = new System.Drawing.Point(748, 423);
            this.lblPaymentAmt.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblPaymentAmt.Name = "lblPaymentAmt";
            this.lblPaymentAmt.Size = new System.Drawing.Size(83, 13);
            this.lblPaymentAmt.TabIndex = 4548;
            this.lblPaymentAmt.Text = "Receipt Amount";
            // 
            // txtPaymentAmt
            // 
            this.txtPaymentAmt.BackColor = System.Drawing.Color.White;
            this.txtPaymentAmt.Location = new System.Drawing.Point(866, 418);
            this.txtPaymentAmt.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtPaymentAmt.Name = "txtPaymentAmt";
            this.txtPaymentAmt.ReadOnly = true;
            this.txtPaymentAmt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPaymentAmt.Size = new System.Drawing.Size(200, 20);
            this.txtPaymentAmt.TabIndex = 4547;
            this.txtPaymentAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPaymentAmt_KeyPress);
            // 
            // lblWithHoldingTax
            // 
            this.lblWithHoldingTax.AutoSize = true;
            this.lblWithHoldingTax.BackColor = System.Drawing.Color.Transparent;
            this.lblWithHoldingTax.ForeColor = System.Drawing.Color.Black;
            this.lblWithHoldingTax.Location = new System.Drawing.Point(751, 398);
            this.lblWithHoldingTax.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblWithHoldingTax.Name = "lblWithHoldingTax";
            this.lblWithHoldingTax.Size = new System.Drawing.Size(84, 13);
            this.lblWithHoldingTax.TabIndex = 4546;
            this.lblWithHoldingTax.Text = "Withholding Tax";
            // 
            // txtWithHoldingTax
            // 
            this.txtWithHoldingTax.BackColor = System.Drawing.Color.White;
            this.txtWithHoldingTax.Location = new System.Drawing.Point(866, 394);
            this.txtWithHoldingTax.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtWithHoldingTax.Name = "txtWithHoldingTax";
            this.txtWithHoldingTax.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtWithHoldingTax.Size = new System.Drawing.Size(200, 20);
            this.txtWithHoldingTax.TabIndex = 4545;
            this.txtWithHoldingTax.TextChanged += new System.EventHandler(this.txtWithHoldingTax_TextChanged);
            this.txtWithHoldingTax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWithHoldingTax_KeyPress);
            // 
            // frmReceiptVoucher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.lblPaymentAmt);
            this.Controls.Add(this.txtPaymentAmt);
            this.Controls.Add(this.lblWithHoldingTax);
            this.Controls.Add(this.txtWithHoldingTax);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnLedgerAdd);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.cbxPrintafterSave);
            this.Controls.Add(this.lblNarration);
            this.Controls.Add(this.txtNarration);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.cmbCashOrBank);
            this.Controls.Add(this.lblBankOrCash);
            this.Controls.Add(this.lnklblRemove);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblVoucherNo);
            this.Controls.Add(this.txtVoucherNo);
            this.Controls.Add(this.dgvReceiptVoucher);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmReceiptVoucher";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Receipts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmReceiptVoucher_FormClosing);
            this.Load += new System.EventHandler(this.frmReceiptVoucher_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmReceiptVoucher_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReceiptVoucher)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.CheckBox cbxPrintafterSave;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cmbCashOrBank;
        private System.Windows.Forms.Label lblBankOrCash;
        private System.Windows.Forms.LinkLabel lnklblRemove;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private dgv.DataGridViewEnter dgvReceiptVoucher;
        private System.Windows.Forms.Button btnLedgerAdd;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtreceiptMasterId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtreceiptDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtledgerPostingId;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbAccountLedger;
        private System.Windows.Forms.DataGridViewButtonColumn dgvbtnAgainst;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbCurrency;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbWithHoldingTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvTxtWithholdingTaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtChequeDate;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbProject;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbCategory;
        private System.Windows.Forms.Label lblPaymentAmt;
        private System.Windows.Forms.TextBox txtPaymentAmt;
        private System.Windows.Forms.Label lblWithHoldingTax;
        private System.Windows.Forms.TextBox txtWithHoldingTax;
        //private System.Windows.Forms.DataGridView dgvReceiptVoucher;
    }
}