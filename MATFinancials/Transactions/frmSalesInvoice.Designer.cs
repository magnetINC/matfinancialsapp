﻿namespace MATFinancials
{
    partial class frmSalesInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSalesInvoice));
            this.txtBillDiscount = new System.Windows.Forms.TextBox();
            this.lblBillDiscount = new System.Windows.Forms.Label();
            this.lblSalesAccount = new System.Windows.Forms.Label();
            this.cmbSalesMode = new System.Windows.Forms.ComboBox();
            this.lblSalesMode = new System.Windows.Forms.Label();
            this.cmbPricingLevel = new System.Windows.Forms.ComboBox();
            this.lblPricingLevel = new System.Windows.Forms.Label();
            this.lblCreaditPeriod = new System.Windows.Forms.Label();
            this.cmbCashOrParty = new System.Windows.Forms.ComboBox();
            this.lblCashorParty = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.lnklblRemove = new System.Windows.Forms.LinkLabel();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.lblNarration = new System.Windows.Forms.Label();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.dgvSalesInvoice = new MATFinancials.dgv.DataGridViewEnter();
            this.dgvtxtSalesInvoiceSlno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceSalesDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSISalesOrderDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceDeliveryNoteDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceQuotationDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvbtnAdd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvtxtItemDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvbtnQview = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvtxtSalesInvoiceUnitConversionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceConversionRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoicembUnitName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbSalesInvoiceGodown = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbSalesInvoiceRack = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbSalesInvoiceBatch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtSalesInvoicePurchaseRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceMrp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceSalesRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceGrossValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceDiscountPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceDiscountAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbSalesInvoiceTaxName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtSalesInvoiceTaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceInRowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceVoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceInvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceVoucherTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesInvoiceAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbCreditNote = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtsalesAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtproductType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbProject = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvCmbCategory = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lblDate = new System.Windows.Forms.Label();
            this.txtCreditPeriod = new System.Windows.Forms.TextBox();
            this.lblDays = new System.Windows.Forms.Label();
            this.cmbSalesMan = new System.Windows.Forms.ComboBox();
            this.lblSalesMan = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.lblTotalQuantity = new System.Windows.Forms.Label();
            this.btnNewLedger = new System.Windows.Forms.Button();
            this.btnNewPricingLevel = new System.Windows.Forms.Button();
            this.btnNewSalesman = new System.Windows.Forms.Button();
            this.btnNewSalesAccount = new System.Windows.Forms.Button();
            this.lblSalesModeOrderNo = new System.Windows.Forms.Label();
            this.cmbSalesModeOrderNo = new System.Windows.Forms.ComboBox();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblFinalTotalAmount = new System.Windows.Forms.Label();
            this.lblTotalQuantitydisplay = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cbxPrintAfterSave = new System.Windows.Forms.CheckBox();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.cmbVoucherType = new System.Windows.Forms.ComboBox();
            this.lblVoucherType = new System.Windows.Forms.Label();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvtxtTtaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxLedgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTTaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTCalculatingMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTApplicableOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvTxtTtaxName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTtaxId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTSlno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSalesInvoiceTax = new MATFinancials.dgv.DataGridViewEnter();
            this.lblLedgerTotal = new System.Windows.Forms.Label();
            this.lblLedgerTotalAmount = new System.Windows.Forms.Label();
            this.lnklblLedgerGridRemove = new System.Windows.Forms.LinkLabel();
            this.lblDrorCr = new System.Windows.Forms.Label();
            this.cmbDrorCr = new System.Windows.Forms.ComboBox();
            this.lblcashOrBank = new System.Windows.Forms.Label();
            this.cmbCashOrbank = new System.Windows.Forms.ComboBox();
            this.dgvtxtAdditionalCoastledgerAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbAdditionalCostledgerName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtAdditionalCostId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAditionalCostSlno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSalesInvoiceLedger = new MATFinancials.dgv.DataGridViewEnter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvCreditNote = new System.Windows.Forms.ComboBox();
            this.lblCreditNoteToApply = new System.Windows.Forms.Label();
            this.cmbDgvCashOrBank = new System.Windows.Forms.ComboBox();
            this.lblBankOrCash = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAdvancePayments = new MATFinancials.dgv.DataGridViewEnter();
            this.dgvChkApply = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvtxtexchangeRateId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtInvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtvoucherTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtvoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmountToApply = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imgPaid = new System.Windows.Forms.PictureBox();
            this.lblPaid = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblAvailableQuantity = new System.Windows.Forms.Label();
            this.chkSendEMail = new System.Windows.Forms.CheckBox();
            this.btnSaveAndEmail = new System.Windows.Forms.Button();
            this.lblTaxTotalAmount = new System.Windows.Forms.Label();
            this.lblTaxTotal = new System.Windows.Forms.Label();
            this.lblBalance2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoiceTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoiceLedger)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdvancePayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPaid)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBillDiscount
            // 
            this.txtBillDiscount.Location = new System.Drawing.Point(929, 443);
            this.txtBillDiscount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBillDiscount.MaxLength = 15;
            this.txtBillDiscount.Name = "txtBillDiscount";
            this.txtBillDiscount.Size = new System.Drawing.Size(121, 22);
            this.txtBillDiscount.TabIndex = 23;
            this.txtBillDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillDiscount.TextChanged += new System.EventHandler(this.txtBillDiscount_TextChanged);
            this.txtBillDiscount.Enter += new System.EventHandler(this.txtBillDiscount_Enter);
            this.txtBillDiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBillDiscount_KeyDown);
            this.txtBillDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBillDiscount_KeyPress_1);
            this.txtBillDiscount.Leave += new System.EventHandler(this.txtBillDiscount_Leave);
            // 
            // lblBillDiscount
            // 
            this.lblBillDiscount.AutoSize = true;
            this.lblBillDiscount.BackColor = System.Drawing.Color.Transparent;
            this.lblBillDiscount.ForeColor = System.Drawing.Color.Black;
            this.lblBillDiscount.Location = new System.Drawing.Point(842, 450);
            this.lblBillDiscount.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblBillDiscount.Name = "lblBillDiscount";
            this.lblBillDiscount.Size = new System.Drawing.Size(81, 16);
            this.lblBillDiscount.TabIndex = 734;
            this.lblBillDiscount.Text = "Bill Discount";
            // 
            // lblSalesAccount
            // 
            this.lblSalesAccount.AutoSize = true;
            this.lblSalesAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesAccount.ForeColor = System.Drawing.Color.Black;
            this.lblSalesAccount.Location = new System.Drawing.Point(18, 109);
            this.lblSalesAccount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblSalesAccount.Name = "lblSalesAccount";
            this.lblSalesAccount.Size = new System.Drawing.Size(68, 16);
            this.lblSalesAccount.TabIndex = 731;
            this.lblSalesAccount.Text = "Sales A/C";
            this.lblSalesAccount.Visible = false;
            // 
            // cmbSalesMode
            // 
            this.cmbSalesMode.AutoCompleteCustomSource.AddRange(new string[] {
            "NA",
            "Against Order",
            "Against Delivery Note",
            "Against Quotation"});
            this.cmbSalesMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesMode.FormattingEnabled = true;
            this.cmbSalesMode.Items.AddRange(new object[] {
            "NA",
            "Against SalesOrder",
            "Against Delivery Note",
            "Against Quotation"});
            this.cmbSalesMode.Location = new System.Drawing.Point(125, 56);
            this.cmbSalesMode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbSalesMode.Name = "cmbSalesMode";
            this.cmbSalesMode.Size = new System.Drawing.Size(172, 24);
            this.cmbSalesMode.TabIndex = 5;
            this.cmbSalesMode.SelectedIndexChanged += new System.EventHandler(this.cmbSalesMode_SelectedIndexChanged);
            this.cmbSalesMode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSalesMode_KeyDown);
            // 
            // lblSalesMode
            // 
            this.lblSalesMode.AutoSize = true;
            this.lblSalesMode.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesMode.ForeColor = System.Drawing.Color.Black;
            this.lblSalesMode.Location = new System.Drawing.Point(18, 60);
            this.lblSalesMode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblSalesMode.Name = "lblSalesMode";
            this.lblSalesMode.Size = new System.Drawing.Size(81, 16);
            this.lblSalesMode.TabIndex = 729;
            this.lblSalesMode.Text = "Sales Mode";
            // 
            // cmbPricingLevel
            // 
            this.cmbPricingLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPricingLevel.FormattingEnabled = true;
            this.cmbPricingLevel.Location = new System.Drawing.Point(855, 85);
            this.cmbPricingLevel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbPricingLevel.Name = "cmbPricingLevel";
            this.cmbPricingLevel.Size = new System.Drawing.Size(172, 24);
            this.cmbPricingLevel.TabIndex = 8;
            this.cmbPricingLevel.SelectedIndexChanged += new System.EventHandler(this.cmbPricingLevel_SelectedIndexChanged);
            this.cmbPricingLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPricingLevel_KeyDown);
            // 
            // lblPricingLevel
            // 
            this.lblPricingLevel.AutoSize = true;
            this.lblPricingLevel.BackColor = System.Drawing.Color.Transparent;
            this.lblPricingLevel.ForeColor = System.Drawing.Color.Black;
            this.lblPricingLevel.Location = new System.Drawing.Point(770, 89);
            this.lblPricingLevel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblPricingLevel.Name = "lblPricingLevel";
            this.lblPricingLevel.Size = new System.Drawing.Size(85, 16);
            this.lblPricingLevel.TabIndex = 725;
            this.lblPricingLevel.Text = "Pricing Level";
            // 
            // lblCreaditPeriod
            // 
            this.lblCreaditPeriod.AutoSize = true;
            this.lblCreaditPeriod.BackColor = System.Drawing.Color.Transparent;
            this.lblCreaditPeriod.ForeColor = System.Drawing.Color.Black;
            this.lblCreaditPeriod.Location = new System.Drawing.Point(768, 35);
            this.lblCreaditPeriod.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCreaditPeriod.Name = "lblCreaditPeriod";
            this.lblCreaditPeriod.Size = new System.Drawing.Size(86, 16);
            this.lblCreaditPeriod.TabIndex = 723;
            this.lblCreaditPeriod.Text = "Credit Period";
            // 
            // cmbCashOrParty
            // 
            this.cmbCashOrParty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrParty.FormattingEnabled = true;
            this.cmbCashOrParty.Location = new System.Drawing.Point(125, 32);
            this.cmbCashOrParty.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCashOrParty.Name = "cmbCashOrParty";
            this.cmbCashOrParty.Size = new System.Drawing.Size(172, 24);
            this.cmbCashOrParty.TabIndex = 2;
            this.cmbCashOrParty.SelectedIndexChanged += new System.EventHandler(this.cmbCashOrParty_SelectedIndexChanged);
            this.cmbCashOrParty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrParty_KeyDown);
            // 
            // lblCashorParty
            // 
            this.lblCashorParty.AutoSize = true;
            this.lblCashorParty.BackColor = System.Drawing.Color.Transparent;
            this.lblCashorParty.ForeColor = System.Drawing.Color.Black;
            this.lblCashorParty.Location = new System.Drawing.Point(18, 36);
            this.lblCashorParty.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCashorParty.Name = "lblCashorParty";
            this.lblCashorParty.Size = new System.Drawing.Size(65, 16);
            this.lblCashorParty.TabIndex = 721;
            this.lblCashorParty.Text = "Customer";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(981, 496);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(69, 27);
            this.btnClose.TabIndex = 29;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(917, 496);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(62, 27);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(660, 497);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(69, 27);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(852, 496);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(62, 27);
            this.btnClear.TabIndex = 27;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Location = new System.Drawing.Point(929, 467);
            this.txtGrandTotal.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(121, 22);
            this.txtGrandTotal.TabIndex = 7897;
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandTotal.TextChanged += new System.EventHandler(this.txtGrandTotal_TextChanged);
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.AutoSize = true;
            this.lblGrandTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblGrandTotal.ForeColor = System.Drawing.Color.Black;
            this.lblGrandTotal.Location = new System.Drawing.Point(844, 471);
            this.lblGrandTotal.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(79, 16);
            this.lblGrandTotal.TabIndex = 714;
            this.lblGrandTotal.Text = "Grand Total";
            // 
            // lnklblRemove
            // 
            this.lnklblRemove.ActiveLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.AutoSize = true;
            this.lnklblRemove.ForeColor = System.Drawing.Color.Brown;
            this.lnklblRemove.LinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.Location = new System.Drawing.Point(990, 400);
            this.lnklblRemove.Margin = new System.Windows.Forms.Padding(5);
            this.lnklblRemove.Name = "lnklblRemove";
            this.lnklblRemove.Size = new System.Drawing.Size(60, 16);
            this.lnklblRemove.TabIndex = 789;
            this.lnklblRemove.TabStop = true;
            this.lnklblRemove.Text = "Remove";
            this.lnklblRemove.VisitedLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblRemove_LinkClicked);
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(78, 482);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(216, 39);
            this.txtNarration.TabIndex = 24;
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(15, 482);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(63, 16);
            this.lblNarration.TabIndex = 711;
            this.lblNarration.Text = "Narration";
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Location = new System.Drawing.Point(125, 9);
            this.txtInvoiceNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtInvoiceNo.MaxLength = 15;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(172, 22);
            this.txtInvoiceNo.TabIndex = 0;
            this.txtInvoiceNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInvoiceNo_KeyDown);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.BackColor = System.Drawing.Color.Transparent;
            this.lblInvoiceNo.ForeColor = System.Drawing.Color.Black;
            this.lblInvoiceNo.Location = new System.Drawing.Point(20, 13);
            this.lblInvoiceNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(75, 16);
            this.lblInvoiceNo.TabIndex = 709;
            this.lblInvoiceNo.Text = "Invoice No.";
            // 
            // dgvSalesInvoice
            // 
            this.dgvSalesInvoice.AllowUserToResizeRows = false;
            this.dgvSalesInvoice.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalesInvoice.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesInvoice.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSalesInvoice.ColumnHeadersHeight = 35;
            this.dgvSalesInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSalesInvoice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSalesInvoiceSlno,
            this.dgvtxtSalesInvoiceSalesDetailsId,
            this.dgvtxtSalesInvoiceProductId,
            this.dgvtxtSISalesOrderDetailsId,
            this.dgvtxtSalesInvoiceDeliveryNoteDetailsId,
            this.dgvtxtSalesInvoiceQuotationDetailsId,
            this.dgvtxtSalesInvoiceBarcode,
            this.dgvtxtSalesInvoiceProductCode,
            this.dgvtxtSalesInvoiceProductName,
            this.dgvbtnAdd,
            this.dgvtxtItemDescription,
            this.dgvtxtSalesInvoiceBrand,
            this.dgvtxtSalesInvoiceQty,
            this.dgvbtnQview,
            this.dgvtxtSalesInvoiceUnitConversionId,
            this.dgvtxtSalesInvoiceConversionRate,
            this.dgvtxtSalesInvoicembUnitName,
            this.dgvcmbSalesInvoiceGodown,
            this.dgvcmbSalesInvoiceRack,
            this.dgvcmbSalesInvoiceBatch,
            this.dgvtxtSalesInvoicePurchaseRate,
            this.dgvtxtSalesInvoiceMrp,
            this.dgvtxtSalesInvoiceSalesRate,
            this.dgvtxtSalesInvoiceRate,
            this.dgvtxtSalesInvoiceGrossValue,
            this.dgvtxtSalesInvoiceDiscountPercentage,
            this.dgvtxtSalesInvoiceDiscountAmount,
            this.dgvtxtSalesInvoiceNetAmount,
            this.dgvcmbSalesInvoiceTaxName,
            this.dgvtxtSalesInvoiceTaxAmount,
            this.dgvtxtSalesInvoiceInRowIndex,
            this.totalUser,
            this.dgvtxtSalesInvoiceVoucherNo,
            this.dgvtxtSalesInvoiceInvoiceNo,
            this.dgvtxtSalesInvoiceVoucherTypeId,
            this.dgvtxtSalesInvoiceAmount,
            this.dgvCmbCreditNote,
            this.dgvtxtsalesAccount,
            this.dgvtxtproductType,
            this.dgvCmbProject,
            this.dgvCmbCategory});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesInvoice.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvSalesInvoice.EnableHeadersVisualStyles = false;
            this.dgvSalesInvoice.GridColor = System.Drawing.Color.DimGray;
            this.dgvSalesInvoice.Location = new System.Drawing.Point(18, 155);
            this.dgvSalesInvoice.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvSalesInvoice.Name = "dgvSalesInvoice";
            this.dgvSalesInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesInvoice.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvSalesInvoice.Size = new System.Drawing.Size(1032, 241);
            this.dgvSalesInvoice.TabIndex = 16;
            this.dgvSalesInvoice.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvSalesInvoice_CellBeginEdit);
            this.dgvSalesInvoice.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoice_CellClick);
            this.dgvSalesInvoice.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoice_CellEndEdit);
            this.dgvSalesInvoice.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoice_CellEnter);
            this.dgvSalesInvoice.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoice_CellLeave);
            this.dgvSalesInvoice.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvSalesInvoice_CurrentCellDirtyStateChanged);
            this.dgvSalesInvoice.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSalesInvoice_DataError);
            this.dgvSalesInvoice.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSalesInvoice_EditingControlShowing);
            this.dgvSalesInvoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSalesInvoice_KeyDown);
            // 
            // dgvtxtSalesInvoiceSlno
            // 
            this.dgvtxtSalesInvoiceSlno.Frozen = true;
            this.dgvtxtSalesInvoiceSlno.HeaderText = "Sl No";
            this.dgvtxtSalesInvoiceSlno.Name = "dgvtxtSalesInvoiceSlno";
            this.dgvtxtSalesInvoiceSlno.ReadOnly = true;
            this.dgvtxtSalesInvoiceSlno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceSlno.Width = 50;
            // 
            // dgvtxtSalesInvoiceSalesDetailsId
            // 
            this.dgvtxtSalesInvoiceSalesDetailsId.HeaderText = "dgvSitxtSalesDetailsId";
            this.dgvtxtSalesInvoiceSalesDetailsId.Name = "dgvtxtSalesInvoiceSalesDetailsId";
            this.dgvtxtSalesInvoiceSalesDetailsId.Visible = false;
            // 
            // dgvtxtSalesInvoiceProductId
            // 
            this.dgvtxtSalesInvoiceProductId.HeaderText = "productId";
            this.dgvtxtSalesInvoiceProductId.Name = "dgvtxtSalesInvoiceProductId";
            this.dgvtxtSalesInvoiceProductId.Visible = false;
            // 
            // dgvtxtSISalesOrderDetailsId
            // 
            this.dgvtxtSISalesOrderDetailsId.HeaderText = "salesOrderDetailsId";
            this.dgvtxtSISalesOrderDetailsId.Name = "dgvtxtSISalesOrderDetailsId";
            this.dgvtxtSISalesOrderDetailsId.Visible = false;
            // 
            // dgvtxtSalesInvoiceDeliveryNoteDetailsId
            // 
            this.dgvtxtSalesInvoiceDeliveryNoteDetailsId.HeaderText = "deliveryNoteDetailsId";
            this.dgvtxtSalesInvoiceDeliveryNoteDetailsId.Name = "dgvtxtSalesInvoiceDeliveryNoteDetailsId";
            this.dgvtxtSalesInvoiceDeliveryNoteDetailsId.Visible = false;
            // 
            // dgvtxtSalesInvoiceQuotationDetailsId
            // 
            this.dgvtxtSalesInvoiceQuotationDetailsId.HeaderText = "quotationDetailsId";
            this.dgvtxtSalesInvoiceQuotationDetailsId.Name = "dgvtxtSalesInvoiceQuotationDetailsId";
            this.dgvtxtSalesInvoiceQuotationDetailsId.Visible = false;
            // 
            // dgvtxtSalesInvoiceBarcode
            // 
            this.dgvtxtSalesInvoiceBarcode.DataPropertyName = "barcode";
            this.dgvtxtSalesInvoiceBarcode.HeaderText = "Barcode";
            this.dgvtxtSalesInvoiceBarcode.Name = "dgvtxtSalesInvoiceBarcode";
            this.dgvtxtSalesInvoiceBarcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceBarcode.Width = 75;
            // 
            // dgvtxtSalesInvoiceProductCode
            // 
            this.dgvtxtSalesInvoiceProductCode.DataPropertyName = "productCode";
            this.dgvtxtSalesInvoiceProductCode.HeaderText = "Product Code";
            this.dgvtxtSalesInvoiceProductCode.Name = "dgvtxtSalesInvoiceProductCode";
            this.dgvtxtSalesInvoiceProductCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceProductCode.Visible = false;
            // 
            // dgvtxtSalesInvoiceProductName
            // 
            this.dgvtxtSalesInvoiceProductName.DataPropertyName = "productName";
            this.dgvtxtSalesInvoiceProductName.HeaderText = "Product Name";
            this.dgvtxtSalesInvoiceProductName.Name = "dgvtxtSalesInvoiceProductName";
            this.dgvtxtSalesInvoiceProductName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceProductName.Width = 160;
            // 
            // dgvbtnAdd
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.NullValue = "+";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbtnAdd.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvbtnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dgvbtnAdd.HeaderText = "";
            this.dgvbtnAdd.Name = "dgvbtnAdd";
            this.dgvbtnAdd.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbtnAdd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvbtnAdd.Text = "";
            this.dgvbtnAdd.Width = 20;
            // 
            // dgvtxtItemDescription
            // 
            this.dgvtxtItemDescription.DataPropertyName = "dgvtxtItemDescription";
            this.dgvtxtItemDescription.HeaderText = "Description";
            this.dgvtxtItemDescription.Name = "dgvtxtItemDescription";
            this.dgvtxtItemDescription.Width = 250;
            // 
            // dgvtxtSalesInvoiceBrand
            // 
            this.dgvtxtSalesInvoiceBrand.HeaderText = "Brand";
            this.dgvtxtSalesInvoiceBrand.Name = "dgvtxtSalesInvoiceBrand";
            this.dgvtxtSalesInvoiceBrand.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceQty
            // 
            this.dgvtxtSalesInvoiceQty.DataPropertyName = "qty";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceQty.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtSalesInvoiceQty.HeaderText = "Qty";
            this.dgvtxtSalesInvoiceQty.MaxInputLength = 8;
            this.dgvtxtSalesInvoiceQty.Name = "dgvtxtSalesInvoiceQty";
            this.dgvtxtSalesInvoiceQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceQty.Width = 105;
            // 
            // dgvbtnQview
            // 
            this.dgvbtnQview.DataPropertyName = "dgvbtnQview";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.NullValue = ":::.";
            this.dgvbtnQview.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvbtnQview.HeaderText = "";
            this.dgvbtnQview.Name = "dgvbtnQview";
            this.dgvbtnQview.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbtnQview.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvbtnQview.Width = 20;
            // 
            // dgvtxtSalesInvoiceUnitConversionId
            // 
            this.dgvtxtSalesInvoiceUnitConversionId.HeaderText = "UnitConversionId";
            this.dgvtxtSalesInvoiceUnitConversionId.Name = "dgvtxtSalesInvoiceUnitConversionId";
            this.dgvtxtSalesInvoiceUnitConversionId.Visible = false;
            // 
            // dgvtxtSalesInvoiceConversionRate
            // 
            this.dgvtxtSalesInvoiceConversionRate.HeaderText = "ConversionRate";
            this.dgvtxtSalesInvoiceConversionRate.Name = "dgvtxtSalesInvoiceConversionRate";
            this.dgvtxtSalesInvoiceConversionRate.Visible = false;
            // 
            // dgvtxtSalesInvoicembUnitName
            // 
            this.dgvtxtSalesInvoicembUnitName.HeaderText = "Unit";
            this.dgvtxtSalesInvoicembUnitName.Name = "dgvtxtSalesInvoicembUnitName";
            this.dgvtxtSalesInvoicembUnitName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvcmbSalesInvoiceGodown
            // 
            this.dgvcmbSalesInvoiceGodown.HeaderText = "Store";
            this.dgvcmbSalesInvoiceGodown.Name = "dgvcmbSalesInvoiceGodown";
            // 
            // dgvcmbSalesInvoiceRack
            // 
            this.dgvcmbSalesInvoiceRack.HeaderText = "Rack";
            this.dgvcmbSalesInvoiceRack.Name = "dgvcmbSalesInvoiceRack";
            // 
            // dgvcmbSalesInvoiceBatch
            // 
            this.dgvcmbSalesInvoiceBatch.HeaderText = "Batch";
            this.dgvcmbSalesInvoiceBatch.Name = "dgvcmbSalesInvoiceBatch";
            // 
            // dgvtxtSalesInvoicePurchaseRate
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoicePurchaseRate.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvtxtSalesInvoicePurchaseRate.HeaderText = "Purchase rate.";
            this.dgvtxtSalesInvoicePurchaseRate.MaxInputLength = 13;
            this.dgvtxtSalesInvoicePurchaseRate.Name = "dgvtxtSalesInvoicePurchaseRate";
            this.dgvtxtSalesInvoicePurchaseRate.ReadOnly = true;
            this.dgvtxtSalesInvoicePurchaseRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceMrp
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceMrp.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvtxtSalesInvoiceMrp.HeaderText = "MRP";
            this.dgvtxtSalesInvoiceMrp.MaxInputLength = 13;
            this.dgvtxtSalesInvoiceMrp.Name = "dgvtxtSalesInvoiceMrp";
            this.dgvtxtSalesInvoiceMrp.ReadOnly = true;
            this.dgvtxtSalesInvoiceMrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceSalesRate
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceSalesRate.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvtxtSalesInvoiceSalesRate.HeaderText = "SalesRate";
            this.dgvtxtSalesInvoiceSalesRate.MaxInputLength = 13;
            this.dgvtxtSalesInvoiceSalesRate.Name = "dgvtxtSalesInvoiceSalesRate";
            this.dgvtxtSalesInvoiceSalesRate.ReadOnly = true;
            this.dgvtxtSalesInvoiceSalesRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceRate
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceRate.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvtxtSalesInvoiceRate.HeaderText = "Rate";
            this.dgvtxtSalesInvoiceRate.MaxInputLength = 10;
            this.dgvtxtSalesInvoiceRate.Name = "dgvtxtSalesInvoiceRate";
            this.dgvtxtSalesInvoiceRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceRate.Width = 105;
            // 
            // dgvtxtSalesInvoiceGrossValue
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceGrossValue.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvtxtSalesInvoiceGrossValue.HeaderText = "Gross Value";
            this.dgvtxtSalesInvoiceGrossValue.Name = "dgvtxtSalesInvoiceGrossValue";
            this.dgvtxtSalesInvoiceGrossValue.ReadOnly = true;
            this.dgvtxtSalesInvoiceGrossValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceGrossValue.Width = 105;
            // 
            // dgvtxtSalesInvoiceDiscountPercentage
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceDiscountPercentage.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvtxtSalesInvoiceDiscountPercentage.HeaderText = "Discount Percentage";
            this.dgvtxtSalesInvoiceDiscountPercentage.MaxInputLength = 10;
            this.dgvtxtSalesInvoiceDiscountPercentage.Name = "dgvtxtSalesInvoiceDiscountPercentage";
            this.dgvtxtSalesInvoiceDiscountPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceDiscountAmount
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceDiscountAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvtxtSalesInvoiceDiscountAmount.HeaderText = "Discount";
            this.dgvtxtSalesInvoiceDiscountAmount.MaxInputLength = 15;
            this.dgvtxtSalesInvoiceDiscountAmount.Name = "dgvtxtSalesInvoiceDiscountAmount";
            this.dgvtxtSalesInvoiceDiscountAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtSalesInvoiceNetAmount
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceNetAmount.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvtxtSalesInvoiceNetAmount.HeaderText = "Net Amount";
            this.dgvtxtSalesInvoiceNetAmount.Name = "dgvtxtSalesInvoiceNetAmount";
            this.dgvtxtSalesInvoiceNetAmount.ReadOnly = true;
            this.dgvtxtSalesInvoiceNetAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceNetAmount.Width = 105;
            // 
            // dgvcmbSalesInvoiceTaxName
            // 
            this.dgvcmbSalesInvoiceTaxName.HeaderText = "Tax";
            this.dgvcmbSalesInvoiceTaxName.Name = "dgvcmbSalesInvoiceTaxName";
            // 
            // dgvtxtSalesInvoiceTaxAmount
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceTaxAmount.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvtxtSalesInvoiceTaxAmount.HeaderText = "Tax Amount";
            this.dgvtxtSalesInvoiceTaxAmount.MaxInputLength = 13;
            this.dgvtxtSalesInvoiceTaxAmount.Name = "dgvtxtSalesInvoiceTaxAmount";
            this.dgvtxtSalesInvoiceTaxAmount.ReadOnly = true;
            this.dgvtxtSalesInvoiceTaxAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceTaxAmount.Visible = false;
            // 
            // dgvtxtSalesInvoiceInRowIndex
            // 
            this.dgvtxtSalesInvoiceInRowIndex.HeaderText = "inRowIndex";
            this.dgvtxtSalesInvoiceInRowIndex.Name = "dgvtxtSalesInvoiceInRowIndex";
            this.dgvtxtSalesInvoiceInRowIndex.Visible = false;
            // 
            // totalUser
            // 
            this.totalUser.HeaderText = "totalUser";
            this.totalUser.Name = "totalUser";
            this.totalUser.Visible = false;
            // 
            // dgvtxtSalesInvoiceVoucherNo
            // 
            this.dgvtxtSalesInvoiceVoucherNo.HeaderText = "VoucherNo";
            this.dgvtxtSalesInvoiceVoucherNo.Name = "dgvtxtSalesInvoiceVoucherNo";
            this.dgvtxtSalesInvoiceVoucherNo.Visible = false;
            // 
            // dgvtxtSalesInvoiceInvoiceNo
            // 
            this.dgvtxtSalesInvoiceInvoiceNo.HeaderText = "InvoiceNo";
            this.dgvtxtSalesInvoiceInvoiceNo.Name = "dgvtxtSalesInvoiceInvoiceNo";
            this.dgvtxtSalesInvoiceInvoiceNo.Visible = false;
            // 
            // dgvtxtSalesInvoiceVoucherTypeId
            // 
            this.dgvtxtSalesInvoiceVoucherTypeId.HeaderText = "VoucherTypeId";
            this.dgvtxtSalesInvoiceVoucherTypeId.Name = "dgvtxtSalesInvoiceVoucherTypeId";
            this.dgvtxtSalesInvoiceVoucherTypeId.Visible = false;
            // 
            // dgvtxtSalesInvoiceAmount
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtSalesInvoiceAmount.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvtxtSalesInvoiceAmount.HeaderText = "Amount";
            this.dgvtxtSalesInvoiceAmount.Name = "dgvtxtSalesInvoiceAmount";
            this.dgvtxtSalesInvoiceAmount.ReadOnly = true;
            this.dgvtxtSalesInvoiceAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesInvoiceAmount.Width = 110;
            // 
            // dgvCmbCreditNote
            // 
            this.dgvCmbCreditNote.HeaderText = "Credit Note";
            this.dgvCmbCreditNote.Name = "dgvCmbCreditNote";
            this.dgvCmbCreditNote.Visible = false;
            // 
            // dgvtxtsalesAccount
            // 
            this.dgvtxtsalesAccount.HeaderText = "SalesAccountID";
            this.dgvtxtsalesAccount.Name = "dgvtxtsalesAccount";
            this.dgvtxtsalesAccount.ReadOnly = true;
            this.dgvtxtsalesAccount.Visible = false;
            // 
            // dgvtxtproductType
            // 
            this.dgvtxtproductType.HeaderText = "Type";
            this.dgvtxtproductType.Name = "dgvtxtproductType";
            this.dgvtxtproductType.ReadOnly = true;
            this.dgvtxtproductType.Visible = false;
            // 
            // dgvCmbProject
            // 
            this.dgvCmbProject.HeaderText = "Project";
            this.dgvCmbProject.Name = "dgvCmbProject";
            // 
            // dgvCmbCategory
            // 
            this.dgvCmbCategory.HeaderText = "Category";
            this.dgvCmbCategory.Name = "dgvCmbCategory";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(774, 10);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(37, 16);
            this.lblDate.TabIndex = 706;
            this.lblDate.Text = "Date";
            // 
            // txtCreditPeriod
            // 
            this.txtCreditPeriod.Location = new System.Drawing.Point(855, 32);
            this.txtCreditPeriod.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCreditPeriod.MaxLength = 3;
            this.txtCreditPeriod.Name = "txtCreditPeriod";
            this.txtCreditPeriod.Size = new System.Drawing.Size(172, 22);
            this.txtCreditPeriod.TabIndex = 4;
            this.txtCreditPeriod.Text = "0";
            this.txtCreditPeriod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditPeriod_KeyDown);
            this.txtCreditPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCreditPeriod_KeyPress);
            this.txtCreditPeriod.Leave += new System.EventHandler(this.txtCreditPeriod_Leave);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.ForeColor = System.Drawing.Color.Brown;
            this.lblDays.Location = new System.Drawing.Point(1026, 36);
            this.lblDays.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(40, 16);
            this.lblDays.TabIndex = 743;
            this.lblDays.Text = "Days";
            // 
            // cmbSalesMan
            // 
            this.cmbSalesMan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesMan.FormattingEnabled = true;
            this.cmbSalesMan.Location = new System.Drawing.Point(125, 129);
            this.cmbSalesMan.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbSalesMan.Name = "cmbSalesMan";
            this.cmbSalesMan.Size = new System.Drawing.Size(172, 24);
            this.cmbSalesMan.TabIndex = 13;
            this.cmbSalesMan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSalesMan_KeyDown);
            // 
            // lblSalesMan
            // 
            this.lblSalesMan.AutoSize = true;
            this.lblSalesMan.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesMan.ForeColor = System.Drawing.Color.Black;
            this.lblSalesMan.Location = new System.Drawing.Point(18, 133);
            this.lblSalesMan.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblSalesMan.Name = "lblSalesMan";
            this.lblSalesMan.Size = new System.Drawing.Size(72, 16);
            this.lblSalesMan.TabIndex = 744;
            this.lblSalesMan.Text = "Sales Man";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(855, 110);
            this.txtCustomer.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(172, 22);
            this.txtCustomer.TabIndex = 12;
            this.txtCustomer.Visible = false;
            this.txtCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomer_KeyDown);
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.BackColor = System.Drawing.Color.Transparent;
            this.lblCustomer.ForeColor = System.Drawing.Color.Black;
            this.lblCustomer.Location = new System.Drawing.Point(768, 113);
            this.lblCustomer.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(65, 16);
            this.lblCustomer.TabIndex = 746;
            this.lblCustomer.Text = "Customer";
            this.lblCustomer.Visible = false;
            // 
            // lblTotalQuantity
            // 
            this.lblTotalQuantity.AutoSize = true;
            this.lblTotalQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalQuantity.ForeColor = System.Drawing.Color.Black;
            this.lblTotalQuantity.Location = new System.Drawing.Point(658, 418);
            this.lblTotalQuantity.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblTotalQuantity.Name = "lblTotalQuantity";
            this.lblTotalQuantity.Size = new System.Drawing.Size(63, 16);
            this.lblTotalQuantity.TabIndex = 748;
            this.lblTotalQuantity.Text = "Total qty.";
            // 
            // btnNewLedger
            // 
            this.btnNewLedger.BackColor = System.Drawing.Color.Gray;
            this.btnNewLedger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewLedger.FlatAppearance.BorderSize = 0;
            this.btnNewLedger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewLedger.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewLedger.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewLedger.Location = new System.Drawing.Point(315, 29);
            this.btnNewLedger.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnNewLedger.Name = "btnNewLedger";
            this.btnNewLedger.Size = new System.Drawing.Size(21, 20);
            this.btnNewLedger.TabIndex = 3;
            this.btnNewLedger.Text = "+";
            this.btnNewLedger.UseVisualStyleBackColor = false;
            this.btnNewLedger.Click += new System.EventHandler(this.btnNewLedger_Click);
            // 
            // btnNewPricingLevel
            // 
            this.btnNewPricingLevel.BackColor = System.Drawing.Color.Gray;
            this.btnNewPricingLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewPricingLevel.FlatAppearance.BorderSize = 0;
            this.btnNewPricingLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewPricingLevel.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewPricingLevel.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewPricingLevel.Location = new System.Drawing.Point(1029, 87);
            this.btnNewPricingLevel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnNewPricingLevel.Name = "btnNewPricingLevel";
            this.btnNewPricingLevel.Size = new System.Drawing.Size(21, 20);
            this.btnNewPricingLevel.TabIndex = 9;
            this.btnNewPricingLevel.Text = "+";
            this.btnNewPricingLevel.UseVisualStyleBackColor = false;
            this.btnNewPricingLevel.Click += new System.EventHandler(this.btnNewPricingLevel_Click);
            // 
            // btnNewSalesman
            // 
            this.btnNewSalesman.BackColor = System.Drawing.Color.Gray;
            this.btnNewSalesman.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewSalesman.FlatAppearance.BorderSize = 0;
            this.btnNewSalesman.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewSalesman.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewSalesman.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewSalesman.Location = new System.Drawing.Point(315, 126);
            this.btnNewSalesman.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnNewSalesman.Name = "btnNewSalesman";
            this.btnNewSalesman.Size = new System.Drawing.Size(21, 20);
            this.btnNewSalesman.TabIndex = 14;
            this.btnNewSalesman.Text = "+";
            this.btnNewSalesman.UseVisualStyleBackColor = false;
            this.btnNewSalesman.Click += new System.EventHandler(this.btnNewSalesman_Click);
            // 
            // btnNewSalesAccount
            // 
            this.btnNewSalesAccount.BackColor = System.Drawing.Color.Gray;
            this.btnNewSalesAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewSalesAccount.FlatAppearance.BorderSize = 0;
            this.btnNewSalesAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewSalesAccount.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewSalesAccount.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewSalesAccount.Location = new System.Drawing.Point(315, 102);
            this.btnNewSalesAccount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnNewSalesAccount.Name = "btnNewSalesAccount";
            this.btnNewSalesAccount.Size = new System.Drawing.Size(21, 20);
            this.btnNewSalesAccount.TabIndex = 11;
            this.btnNewSalesAccount.Text = "+";
            this.btnNewSalesAccount.UseVisualStyleBackColor = false;
            this.btnNewSalesAccount.Visible = false;
            this.btnNewSalesAccount.Click += new System.EventHandler(this.btnNewSalesAccount_Click);
            // 
            // lblSalesModeOrderNo
            // 
            this.lblSalesModeOrderNo.AutoSize = true;
            this.lblSalesModeOrderNo.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesModeOrderNo.ForeColor = System.Drawing.Color.Black;
            this.lblSalesModeOrderNo.Location = new System.Drawing.Point(18, 84);
            this.lblSalesModeOrderNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblSalesModeOrderNo.Name = "lblSalesModeOrderNo";
            this.lblSalesModeOrderNo.Size = new System.Drawing.Size(66, 16);
            this.lblSalesModeOrderNo.TabIndex = 760;
            this.lblSalesModeOrderNo.Text = "Order No.";
            // 
            // cmbSalesModeOrderNo
            // 
            this.cmbSalesModeOrderNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesModeOrderNo.FormattingEnabled = true;
            this.cmbSalesModeOrderNo.Location = new System.Drawing.Point(125, 80);
            this.cmbSalesModeOrderNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbSalesModeOrderNo.Name = "cmbSalesModeOrderNo";
            this.cmbSalesModeOrderNo.Size = new System.Drawing.Size(172, 24);
            this.cmbSalesModeOrderNo.TabIndex = 7;
            this.cmbSalesModeOrderNo.SelectedIndexChanged += new System.EventHandler(this.cmbSalesModeOrderNo_SelectedIndexChanged);
            this.cmbSalesModeOrderNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSalesModeOrderNo_KeyDown);
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Location = new System.Drawing.Point(929, 420);
            this.txtTotalAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(121, 22);
            this.txtTotalAmount.TabIndex = 8898;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalAmount.TextChanged += new System.EventHandler(this.txtTotalAmount_TextChanged);
            // 
            // lblFinalTotalAmount
            // 
            this.lblFinalTotalAmount.AutoSize = true;
            this.lblFinalTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblFinalTotalAmount.ForeColor = System.Drawing.Color.Black;
            this.lblFinalTotalAmount.Location = new System.Drawing.Point(837, 427);
            this.lblFinalTotalAmount.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblFinalTotalAmount.Name = "lblFinalTotalAmount";
            this.lblFinalTotalAmount.Size = new System.Drawing.Size(87, 16);
            this.lblFinalTotalAmount.TabIndex = 764;
            this.lblFinalTotalAmount.Text = "Total Amount";
            // 
            // lblTotalQuantitydisplay
            // 
            this.lblTotalQuantitydisplay.AutoSize = true;
            this.lblTotalQuantitydisplay.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalQuantitydisplay.ForeColor = System.Drawing.Color.Black;
            this.lblTotalQuantitydisplay.Location = new System.Drawing.Point(726, 418);
            this.lblTotalQuantitydisplay.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblTotalQuantitydisplay.Name = "lblTotalQuantitydisplay";
            this.lblTotalQuantitydisplay.Size = new System.Drawing.Size(15, 16);
            this.lblTotalQuantitydisplay.TabIndex = 766;
            this.lblTotalQuantitydisplay.Text = "0";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(855, 6);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(172, 22);
            this.txtDate.TabIndex = 1;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "yyyy/mm/dd";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(1023, 6);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(21, 22);
            this.dtpDate.TabIndex = 768;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // cbxPrintAfterSave
            // 
            this.cbxPrintAfterSave.AutoSize = true;
            this.cbxPrintAfterSave.BackColor = System.Drawing.Color.Transparent;
            this.cbxPrintAfterSave.ForeColor = System.Drawing.Color.Black;
            this.cbxPrintAfterSave.Location = new System.Drawing.Point(508, 504);
            this.cbxPrintAfterSave.Name = "cbxPrintAfterSave";
            this.cbxPrintAfterSave.Size = new System.Drawing.Size(115, 20);
            this.cbxPrintAfterSave.TabIndex = 25;
            this.cbxPrintAfterSave.Text = "Print after save";
            this.cbxPrintAfterSave.UseVisualStyleBackColor = false;
            this.cbxPrintAfterSave.CheckedChanged += new System.EventHandler(this.cbxPrintAfterSave_CheckedChanged);
            this.cbxPrintAfterSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxPrintAfterSave_KeyDown);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.ForeColor = System.Drawing.Color.Black;
            this.lblCurrency.Location = new System.Drawing.Point(768, 137);
            this.lblCurrency.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(61, 16);
            this.lblCurrency.TabIndex = 770;
            this.lblCurrency.Text = "Currency";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Location = new System.Drawing.Point(855, 131);
            this.cmbCurrency.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(172, 24);
            this.cmbCurrency.TabIndex = 15;
            this.cmbCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCurrency_KeyDown);
            // 
            // cmbVoucherType
            // 
            this.cmbVoucherType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVoucherType.FormattingEnabled = true;
            this.cmbVoucherType.Location = new System.Drawing.Point(855, 59);
            this.cmbVoucherType.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbVoucherType.Name = "cmbVoucherType";
            this.cmbVoucherType.Size = new System.Drawing.Size(172, 24);
            this.cmbVoucherType.TabIndex = 6;
            this.cmbVoucherType.SelectedIndexChanged += new System.EventHandler(this.cmbVoucherType_SelectedIndexChanged);
            this.cmbVoucherType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVoucherType_KeyDown);
            // 
            // lblVoucherType
            // 
            this.lblVoucherType.AutoSize = true;
            this.lblVoucherType.BackColor = System.Drawing.Color.Transparent;
            this.lblVoucherType.ForeColor = System.Drawing.Color.Black;
            this.lblVoucherType.Location = new System.Drawing.Point(770, 62);
            this.lblVoucherType.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblVoucherType.Name = "lblVoucherType";
            this.lblVoucherType.Size = new System.Drawing.Size(63, 16);
            this.lblVoucherType.TabIndex = 791;
            this.lblVoucherType.Text = "Apply On";
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.ForeColor = System.Drawing.Color.Red;
            this.lblVoucherNo.Location = new System.Drawing.Point(301, 106);
            this.lblVoucherNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(13, 16);
            this.lblVoucherNo.TabIndex = 8900;
            this.lblVoucherNo.Text = "*";
            this.lblVoucherNo.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(301, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 16);
            this.label3.TabIndex = 8901;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(301, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 16);
            this.label4.TabIndex = 8902;
            this.label4.Text = "*";
            // 
            // dgvtxtTtaxAmount
            // 
            this.dgvtxtTtaxAmount.DataPropertyName = "taxAmount";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtTtaxAmount.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvtxtTtaxAmount.HeaderText = "Amount";
            this.dgvtxtTtaxAmount.Name = "dgvtxtTtaxAmount";
            this.dgvtxtTtaxAmount.ReadOnly = true;
            this.dgvtxtTtaxAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTaxLedgerId
            // 
            this.dgvtxtTaxLedgerId.DataPropertyName = "ledgerId";
            this.dgvtxtTaxLedgerId.HeaderText = "LedgerId";
            this.dgvtxtTaxLedgerId.Name = "dgvtxtTaxLedgerId";
            this.dgvtxtTaxLedgerId.ReadOnly = true;
            this.dgvtxtTaxLedgerId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTaxLedgerId.Visible = false;
            // 
            // dgvtxtTTaxRate
            // 
            this.dgvtxtTTaxRate.DataPropertyName = "rate";
            this.dgvtxtTTaxRate.HeaderText = "rate";
            this.dgvtxtTTaxRate.Name = "dgvtxtTTaxRate";
            this.dgvtxtTTaxRate.ReadOnly = true;
            this.dgvtxtTTaxRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTTaxRate.Visible = false;
            // 
            // dgvtxtTCalculatingMode
            // 
            this.dgvtxtTCalculatingMode.DataPropertyName = "calculatingMode";
            this.dgvtxtTCalculatingMode.HeaderText = "calculatingMode";
            this.dgvtxtTCalculatingMode.Name = "dgvtxtTCalculatingMode";
            this.dgvtxtTCalculatingMode.ReadOnly = true;
            this.dgvtxtTCalculatingMode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTCalculatingMode.Visible = false;
            // 
            // dgvtxtTApplicableOn
            // 
            this.dgvtxtTApplicableOn.DataPropertyName = "applicableOn";
            this.dgvtxtTApplicableOn.HeaderText = "applicableOn";
            this.dgvtxtTApplicableOn.Name = "dgvtxtTApplicableOn";
            this.dgvtxtTApplicableOn.ReadOnly = true;
            this.dgvtxtTApplicableOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTApplicableOn.Visible = false;
            // 
            // dgvTxtTtaxName
            // 
            this.dgvTxtTtaxName.DataPropertyName = "taxName";
            this.dgvTxtTtaxName.HeaderText = "Tax Name";
            this.dgvTxtTtaxName.Name = "dgvTxtTtaxName";
            this.dgvTxtTtaxName.ReadOnly = true;
            this.dgvTxtTtaxName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTtaxId
            // 
            this.dgvtxtTtaxId.DataPropertyName = "taxId";
            this.dgvtxtTtaxId.HeaderText = "taxId";
            this.dgvtxtTtaxId.Name = "dgvtxtTtaxId";
            this.dgvtxtTtaxId.ReadOnly = true;
            this.dgvtxtTtaxId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTtaxId.Visible = false;
            // 
            // dgvtxtTSlno
            // 
            this.dgvtxtTSlno.HeaderText = "Sl NO";
            this.dgvtxtTSlno.Name = "dgvtxtTSlno";
            this.dgvtxtTSlno.ReadOnly = true;
            this.dgvtxtTSlno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvSalesInvoiceTax
            // 
            this.dgvSalesInvoiceTax.AllowUserToAddRows = false;
            this.dgvSalesInvoiceTax.AllowUserToDeleteRows = false;
            this.dgvSalesInvoiceTax.AllowUserToResizeColumns = false;
            this.dgvSalesInvoiceTax.AllowUserToResizeRows = false;
            this.dgvSalesInvoiceTax.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalesInvoiceTax.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalesInvoiceTax.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesInvoiceTax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvSalesInvoiceTax.ColumnHeadersHeight = 25;
            this.dgvSalesInvoiceTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSalesInvoiceTax.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtTSlno,
            this.dgvtxtTtaxId,
            this.dgvTxtTtaxName,
            this.dgvtxtTApplicableOn,
            this.dgvtxtTCalculatingMode,
            this.dgvtxtTTaxRate,
            this.dgvtxtTaxLedgerId,
            this.dgvtxtTtaxAmount});
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesInvoiceTax.DefaultCellStyle = dataGridViewCellStyle19;
            this.dgvSalesInvoiceTax.EnableHeadersVisualStyles = false;
            this.dgvSalesInvoiceTax.GridColor = System.Drawing.Color.DimGray;
            this.dgvSalesInvoiceTax.Location = new System.Drawing.Point(18, 400);
            this.dgvSalesInvoiceTax.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvSalesInvoiceTax.Name = "dgvSalesInvoiceTax";
            this.dgvSalesInvoiceTax.ReadOnly = true;
            this.dgvSalesInvoiceTax.RowHeadersVisible = false;
            this.dgvSalesInvoiceTax.Size = new System.Drawing.Size(367, 78);
            this.dgvSalesInvoiceTax.TabIndex = 20;
            this.dgvSalesInvoiceTax.Visible = false;
            this.dgvSalesInvoiceTax.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoiceTax_CellContentClick);
            this.dgvSalesInvoiceTax.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSalesInvoiceTax_RowsAdded);
            // 
            // lblLedgerTotal
            // 
            this.lblLedgerTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblLedgerTotal.ForeColor = System.Drawing.Color.Black;
            this.lblLedgerTotal.Location = new System.Drawing.Point(97, 142);
            this.lblLedgerTotal.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblLedgerTotal.Name = "lblLedgerTotal";
            this.lblLedgerTotal.Size = new System.Drawing.Size(47, 13);
            this.lblLedgerTotal.TabIndex = 762;
            this.lblLedgerTotal.Text = "Total Additional Cost";
            // 
            // lblLedgerTotalAmount
            // 
            this.lblLedgerTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblLedgerTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedgerTotalAmount.ForeColor = System.Drawing.Color.Brown;
            this.lblLedgerTotalAmount.Location = new System.Drawing.Point(161, 139);
            this.lblLedgerTotalAmount.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblLedgerTotalAmount.Name = "lblLedgerTotalAmount";
            this.lblLedgerTotalAmount.Size = new System.Drawing.Size(118, 21);
            this.lblLedgerTotalAmount.TabIndex = 763;
            this.lblLedgerTotalAmount.Text = "00.00";
            this.lblLedgerTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lnklblLedgerGridRemove
            // 
            this.lnklblLedgerGridRemove.ActiveLinkColor = System.Drawing.Color.Brown;
            this.lnklblLedgerGridRemove.AutoSize = true;
            this.lnklblLedgerGridRemove.ForeColor = System.Drawing.Color.Brown;
            this.lnklblLedgerGridRemove.LinkColor = System.Drawing.Color.Brown;
            this.lnklblLedgerGridRemove.Location = new System.Drawing.Point(288, 142);
            this.lnklblLedgerGridRemove.Margin = new System.Windows.Forms.Padding(5);
            this.lnklblLedgerGridRemove.Name = "lnklblLedgerGridRemove";
            this.lnklblLedgerGridRemove.Size = new System.Drawing.Size(60, 16);
            this.lnklblLedgerGridRemove.TabIndex = 8899;
            this.lnklblLedgerGridRemove.TabStop = true;
            this.lnklblLedgerGridRemove.Text = "Remove";
            this.lnklblLedgerGridRemove.VisitedLinkColor = System.Drawing.Color.Brown;
            this.lnklblLedgerGridRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblLedgerGridRemove_LinkClicked);
            // 
            // lblDrorCr
            // 
            this.lblDrorCr.AutoSize = true;
            this.lblDrorCr.BackColor = System.Drawing.Color.Transparent;
            this.lblDrorCr.ForeColor = System.Drawing.Color.Black;
            this.lblDrorCr.Location = new System.Drawing.Point(3, 16);
            this.lblDrorCr.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblDrorCr.Name = "lblDrorCr";
            this.lblDrorCr.Size = new System.Drawing.Size(37, 16);
            this.lblDrorCr.TabIndex = 753;
            this.lblDrorCr.Text = "Dr/cr";
            // 
            // cmbDrorCr
            // 
            this.cmbDrorCr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDrorCr.FormattingEnabled = true;
            this.cmbDrorCr.Items.AddRange(new object[] {
            "Dr",
            "Cr"});
            this.cmbDrorCr.Location = new System.Drawing.Point(40, 13);
            this.cmbDrorCr.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbDrorCr.Name = "cmbDrorCr";
            this.cmbDrorCr.Size = new System.Drawing.Size(49, 24);
            this.cmbDrorCr.TabIndex = 17;
            this.cmbDrorCr.SelectedIndexChanged += new System.EventHandler(this.cmbDrorCr_SelectedIndexChanged);
            this.cmbDrorCr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDrorCr_KeyDown);
            // 
            // lblcashOrBank
            // 
            this.lblcashOrBank.AutoSize = true;
            this.lblcashOrBank.BackColor = System.Drawing.Color.Transparent;
            this.lblcashOrBank.ForeColor = System.Drawing.Color.Black;
            this.lblcashOrBank.Location = new System.Drawing.Point(174, 16);
            this.lblcashOrBank.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblcashOrBank.Name = "lblcashOrBank";
            this.lblcashOrBank.Size = new System.Drawing.Size(51, 16);
            this.lblcashOrBank.TabIndex = 772;
            this.lblcashOrBank.Text = "Ledger";
            // 
            // cmbCashOrbank
            // 
            this.cmbCashOrbank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrbank.FormattingEnabled = true;
            this.cmbCashOrbank.Items.AddRange(new object[] {
            "Dr",
            "Cr"});
            this.cmbCashOrbank.Location = new System.Drawing.Point(244, 13);
            this.cmbCashOrbank.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCashOrbank.Name = "cmbCashOrbank";
            this.cmbCashOrbank.Size = new System.Drawing.Size(144, 24);
            this.cmbCashOrbank.TabIndex = 18;
            this.cmbCashOrbank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrbank_KeyDown);
            // 
            // dgvtxtAdditionalCoastledgerAmount
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAdditionalCoastledgerAmount.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgvtxtAdditionalCoastledgerAmount.FillWeight = 115.736F;
            this.dgvtxtAdditionalCoastledgerAmount.HeaderText = "Amount";
            this.dgvtxtAdditionalCoastledgerAmount.MaxInputLength = 8;
            this.dgvtxtAdditionalCoastledgerAmount.Name = "dgvtxtAdditionalCoastledgerAmount";
            this.dgvtxtAdditionalCoastledgerAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvCmbAdditionalCostledgerName
            // 
            this.dgvCmbAdditionalCostledgerName.HeaderText = "A/c Ledger";
            this.dgvCmbAdditionalCostledgerName.Name = "dgvCmbAdditionalCostledgerName";
            // 
            // dgvtxtAdditionalCostId
            // 
            this.dgvtxtAdditionalCostId.HeaderText = "dgvtxtAdditionalCostId";
            this.dgvtxtAdditionalCostId.Name = "dgvtxtAdditionalCostId";
            this.dgvtxtAdditionalCostId.Visible = false;
            // 
            // dgvtxtAditionalCostSlno
            // 
            this.dgvtxtAditionalCostSlno.FillWeight = 68.52792F;
            this.dgvtxtAditionalCostSlno.HeaderText = "Sl No";
            this.dgvtxtAditionalCostSlno.Name = "dgvtxtAditionalCostSlno";
            this.dgvtxtAditionalCostSlno.ReadOnly = true;
            this.dgvtxtAditionalCostSlno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvSalesInvoiceLedger
            // 
            this.dgvSalesInvoiceLedger.AllowUserToResizeColumns = false;
            this.dgvSalesInvoiceLedger.AllowUserToResizeRows = false;
            this.dgvSalesInvoiceLedger.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalesInvoiceLedger.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalesInvoiceLedger.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesInvoiceLedger.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvSalesInvoiceLedger.ColumnHeadersHeight = 25;
            this.dgvSalesInvoiceLedger.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSalesInvoiceLedger.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtAditionalCostSlno,
            this.dgvtxtAdditionalCostId,
            this.dgvCmbAdditionalCostledgerName,
            this.dgvtxtAdditionalCoastledgerAmount});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesInvoiceLedger.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgvSalesInvoiceLedger.EnableHeadersVisualStyles = false;
            this.dgvSalesInvoiceLedger.GridColor = System.Drawing.Color.DimGray;
            this.dgvSalesInvoiceLedger.Location = new System.Drawing.Point(51, 37);
            this.dgvSalesInvoiceLedger.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvSalesInvoiceLedger.Name = "dgvSalesInvoiceLedger";
            this.dgvSalesInvoiceLedger.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesInvoiceLedger.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvSalesInvoiceLedger.Size = new System.Drawing.Size(229, 58);
            this.dgvSalesInvoiceLedger.TabIndex = 19;
            this.dgvSalesInvoiceLedger.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvSalesInvoiceLedger_CellBeginEdit);
            this.dgvSalesInvoiceLedger.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoiceLedger_CellClick);
            this.dgvSalesInvoiceLedger.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalesInvoiceLedger_CellValueChanged);
            this.dgvSalesInvoiceLedger.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSalesInvoiceLedger_DataError);
            this.dgvSalesInvoiceLedger.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvSalesInvoiceLedger_EditingControlShowing);
            this.dgvSalesInvoiceLedger.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvSalesInvoiceLedger_RowsAdded);
            this.dgvSalesInvoiceLedger.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSalesInvoiceLedger_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblDrorCr);
            this.panel1.Controls.Add(this.cmbDrorCr);
            this.panel1.Controls.Add(this.cmbCashOrbank);
            this.panel1.Controls.Add(this.lblcashOrBank);
            this.panel1.Controls.Add(this.lnklblLedgerGridRemove);
            this.panel1.Controls.Add(this.lblLedgerTotalAmount);
            this.panel1.Controls.Add(this.lblLedgerTotal);
            this.panel1.Location = new System.Drawing.Point(293, 482);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(92, 39);
            this.panel1.TabIndex = 8903;
            this.panel1.Visible = false;
            // 
            // dgvCreditNote
            // 
            this.dgvCreditNote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dgvCreditNote.FormattingEnabled = true;
            this.dgvCreditNote.Location = new System.Drawing.Point(508, 60);
            this.dgvCreditNote.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dgvCreditNote.Name = "dgvCreditNote";
            this.dgvCreditNote.Size = new System.Drawing.Size(200, 24);
            this.dgvCreditNote.TabIndex = 8904;
            this.dgvCreditNote.Visible = false;
            this.dgvCreditNote.SelectedIndexChanged += new System.EventHandler(this.dgvCreditNote_SelectedIndexChanged);
            // 
            // lblCreditNoteToApply
            // 
            this.lblCreditNoteToApply.AutoSize = true;
            this.lblCreditNoteToApply.BackColor = System.Drawing.Color.Transparent;
            this.lblCreditNoteToApply.ForeColor = System.Drawing.Color.Black;
            this.lblCreditNoteToApply.Location = new System.Drawing.Point(349, 4);
            this.lblCreditNoteToApply.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCreditNoteToApply.Name = "lblCreditNoteToApply";
            this.lblCreditNoteToApply.Size = new System.Drawing.Size(125, 16);
            this.lblCreditNoteToApply.TabIndex = 8905;
            this.lblCreditNoteToApply.Text = "Balance(s) to Apply";
            this.lblCreditNoteToApply.Visible = false;
            // 
            // cmbDgvCashOrBank
            // 
            this.cmbDgvCashOrBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDgvCashOrBank.FormattingEnabled = true;
            this.cmbDgvCashOrBank.Location = new System.Drawing.Point(509, 87);
            this.cmbDgvCashOrBank.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbDgvCashOrBank.Name = "cmbDgvCashOrBank";
            this.cmbDgvCashOrBank.Size = new System.Drawing.Size(200, 24);
            this.cmbDgvCashOrBank.TabIndex = 8906;
            // 
            // lblBankOrCash
            // 
            this.lblBankOrCash.AutoSize = true;
            this.lblBankOrCash.BackColor = System.Drawing.Color.Transparent;
            this.lblBankOrCash.ForeColor = System.Drawing.Color.Black;
            this.lblBankOrCash.Location = new System.Drawing.Point(397, 91);
            this.lblBankOrCash.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBankOrCash.Name = "lblBankOrCash";
            this.lblBankOrCash.Size = new System.Drawing.Size(80, 16);
            this.lblBankOrCash.TabIndex = 8907;
            this.lblBankOrCash.Text = "Bank / Cash";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(715, 90);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 5, 5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 16);
            this.label1.TabIndex = 8908;
            this.label1.Text = "*";
            this.label1.Visible = false;
            // 
            // dgvAdvancePayments
            // 
            this.dgvAdvancePayments.AllowUserToAddRows = false;
            this.dgvAdvancePayments.AllowUserToDeleteRows = false;
            this.dgvAdvancePayments.AllowUserToResizeColumns = false;
            this.dgvAdvancePayments.AllowUserToResizeRows = false;
            this.dgvAdvancePayments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAdvancePayments.BackgroundColor = System.Drawing.Color.White;
            this.dgvAdvancePayments.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAdvancePayments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvAdvancePayments.ColumnHeadersHeight = 38;
            this.dgvAdvancePayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAdvancePayments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvChkApply,
            this.dgvtxtexchangeRateId,
            this.dgvtxtInvoiceNo,
            this.dgvtxtvoucherTypeId,
            this.dgvtxtvoucherNo,
            this.dgvtxtType,
            this.dgvtxtValue,
            this.dgvtxtAmountToApply});
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAdvancePayments.DefaultCellStyle = dataGridViewCellStyle25;
            this.dgvAdvancePayments.EnableHeadersVisualStyles = false;
            this.dgvAdvancePayments.GridColor = System.Drawing.Color.DimGray;
            this.dgvAdvancePayments.Location = new System.Drawing.Point(352, 23);
            this.dgvAdvancePayments.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvAdvancePayments.Name = "dgvAdvancePayments";
            this.dgvAdvancePayments.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvAdvancePayments.RowHeadersVisible = false;
            this.dgvAdvancePayments.Size = new System.Drawing.Size(406, 105);
            this.dgvAdvancePayments.TabIndex = 8913;
            this.dgvAdvancePayments.Visible = false;
            this.dgvAdvancePayments.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAdvancePayments_CellValueChanged);
            // 
            // dgvChkApply
            // 
            this.dgvChkApply.HeaderText = "Apply";
            this.dgvChkApply.Name = "dgvChkApply";
            // 
            // dgvtxtexchangeRateId
            // 
            this.dgvtxtexchangeRateId.HeaderText = "Exchange Rate Id";
            this.dgvtxtexchangeRateId.Name = "dgvtxtexchangeRateId";
            this.dgvtxtexchangeRateId.Visible = false;
            // 
            // dgvtxtInvoiceNo
            // 
            this.dgvtxtInvoiceNo.HeaderText = "Invoice No";
            this.dgvtxtInvoiceNo.Name = "dgvtxtInvoiceNo";
            this.dgvtxtInvoiceNo.Visible = false;
            // 
            // dgvtxtvoucherTypeId
            // 
            this.dgvtxtvoucherTypeId.HeaderText = "Voucher Type Id";
            this.dgvtxtvoucherTypeId.Name = "dgvtxtvoucherTypeId";
            this.dgvtxtvoucherTypeId.Visible = false;
            // 
            // dgvtxtvoucherNo
            // 
            this.dgvtxtvoucherNo.HeaderText = "Voucher No";
            this.dgvtxtvoucherNo.Name = "dgvtxtvoucherNo";
            this.dgvtxtvoucherNo.Visible = false;
            // 
            // dgvtxtType
            // 
            this.dgvtxtType.HeaderText = "Type";
            this.dgvtxtType.Name = "dgvtxtType";
            // 
            // dgvtxtValue
            // 
            this.dgvtxtValue.HeaderText = "Value";
            this.dgvtxtValue.Name = "dgvtxtValue";
            // 
            // dgvtxtAmountToApply
            // 
            this.dgvtxtAmountToApply.HeaderText = "Amount to Apply";
            this.dgvtxtAmountToApply.Name = "dgvtxtAmountToApply";
            // 
            // imgPaid
            // 
            this.imgPaid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgPaid.Image = ((System.Drawing.Image)(resources.GetObject("imgPaid.Image")));
            this.imgPaid.Location = new System.Drawing.Point(545, 400);
            this.imgPaid.Name = "imgPaid";
            this.imgPaid.Size = new System.Drawing.Size(104, 66);
            this.imgPaid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPaid.TabIndex = 8914;
            this.imgPaid.TabStop = false;
            // 
            // lblPaid
            // 
            this.lblPaid.AutoSize = true;
            this.lblPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaid.Location = new System.Drawing.Point(403, 421);
            this.lblPaid.Name = "lblPaid";
            this.lblPaid.Size = new System.Drawing.Size(40, 20);
            this.lblPaid.TabIndex = 8915;
            this.lblPaid.Text = "Paid";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.Location = new System.Drawing.Point(403, 450);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(71, 20);
            this.lblBalance.TabIndex = 8916;
            this.lblBalance.Text = "Balance:";
            // 
            // lblAvailableQuantity
            // 
            this.lblAvailableQuantity.AutoSize = true;
            this.lblAvailableQuantity.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.lblAvailableQuantity.Location = new System.Drawing.Point(464, 132);
            this.lblAvailableQuantity.Name = "lblAvailableQuantity";
            this.lblAvailableQuantity.Size = new System.Drawing.Size(30, 19);
            this.lblAvailableQuantity.TabIndex = 8917;
            this.lblAvailableQuantity.Text = "AV";
            // 
            // chkSendEMail
            // 
            this.chkSendEMail.AutoSize = true;
            this.chkSendEMail.BackColor = System.Drawing.Color.Transparent;
            this.chkSendEMail.ForeColor = System.Drawing.Color.Black;
            this.chkSendEMail.Location = new System.Drawing.Point(406, 505);
            this.chkSendEMail.Name = "chkSendEMail";
            this.chkSendEMail.Size = new System.Drawing.Size(96, 20);
            this.chkSendEMail.TabIndex = 8918;
            this.chkSendEMail.Text = "Send Email";
            this.chkSendEMail.UseVisualStyleBackColor = false;
            this.chkSendEMail.Visible = false;
            // 
            // btnSaveAndEmail
            // 
            this.btnSaveAndEmail.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSaveAndEmail.FlatAppearance.BorderSize = 0;
            this.btnSaveAndEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAndEmail.ForeColor = System.Drawing.Color.Black;
            this.btnSaveAndEmail.Location = new System.Drawing.Point(732, 497);
            this.btnSaveAndEmail.Name = "btnSaveAndEmail";
            this.btnSaveAndEmail.Size = new System.Drawing.Size(118, 27);
            this.btnSaveAndEmail.TabIndex = 8919;
            this.btnSaveAndEmail.Text = "Save && Email";
            this.btnSaveAndEmail.UseVisualStyleBackColor = false;
            this.btnSaveAndEmail.Click += new System.EventHandler(this.btnSaveAndEmail_Click);
            // 
            // lblTaxTotalAmount
            // 
            this.lblTaxTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxTotalAmount.ForeColor = System.Drawing.Color.Brown;
            this.lblTaxTotalAmount.Location = new System.Drawing.Point(697, 448);
            this.lblTaxTotalAmount.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblTaxTotalAmount.Name = "lblTaxTotalAmount";
            this.lblTaxTotalAmount.Size = new System.Drawing.Size(46, 20);
            this.lblTaxTotalAmount.TabIndex = 8923;
            this.lblTaxTotalAmount.Text = "00.00";
            this.lblTaxTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTaxTotalAmount.Visible = false;
            // 
            // lblTaxTotal
            // 
            this.lblTaxTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTaxTotal.Location = new System.Drawing.Point(657, 450);
            this.lblTaxTotal.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblTaxTotal.Name = "lblTaxTotal";
            this.lblTaxTotal.Size = new System.Drawing.Size(47, 13);
            this.lblTaxTotal.TabIndex = 8922;
            this.lblTaxTotal.Text = "Total Tax Amount";
            this.lblTaxTotal.Visible = false;
            // 
            // lblBalance2
            // 
            this.lblBalance2.AutoSize = true;
            this.lblBalance2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance2.Location = new System.Drawing.Point(476, 450);
            this.lblBalance2.Name = "lblBalance2";
            this.lblBalance2.Size = new System.Drawing.Size(18, 20);
            this.lblBalance2.TabIndex = 8924;
            this.lblBalance2.Text = "0";
            // 
            // frmSalesInvoice
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 531);
            this.Controls.Add(this.lblBalance2);
            this.Controls.Add(this.lblTaxTotalAmount);
            this.Controls.Add(this.lblTaxTotal);
            this.Controls.Add(this.btnSaveAndEmail);
            this.Controls.Add(this.chkSendEMail);
            this.Controls.Add(this.lblAvailableQuantity);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.lblPaid);
            this.Controls.Add(this.imgPaid);
            this.Controls.Add(this.txtCreditPeriod);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.dgvAdvancePayments);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbDgvCashOrBank);
            this.Controls.Add(this.lblBankOrCash);
            this.Controls.Add(this.dgvCreditNote);
            this.Controls.Add(this.lblCreditNoteToApply);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVoucherNo);
            this.Controls.Add(this.cmbVoucherType);
            this.Controls.Add(this.lblVoucherType);
            this.Controls.Add(this.cmbCurrency);
            this.Controls.Add(this.lblCurrency);
            this.Controls.Add(this.cbxPrintAfterSave);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lblTotalQuantitydisplay);
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.lblFinalTotalAmount);
            this.Controls.Add(this.cmbSalesModeOrderNo);
            this.Controls.Add(this.lblSalesModeOrderNo);
            this.Controls.Add(this.btnNewSalesAccount);
            this.Controls.Add(this.btnNewSalesman);
            this.Controls.Add(this.btnNewPricingLevel);
            this.Controls.Add(this.lblTotalQuantity);
            this.Controls.Add(this.txtCustomer);
            this.Controls.Add(this.lblCustomer);
            this.Controls.Add(this.cmbSalesMan);
            this.Controls.Add(this.lblSalesMan);
            this.Controls.Add(this.lblDays);
            this.Controls.Add(this.btnNewLedger);
            this.Controls.Add(this.txtBillDiscount);
            this.Controls.Add(this.lblBillDiscount);
            this.Controls.Add(this.dgvSalesInvoiceTax);
            this.Controls.Add(this.lblSalesAccount);
            this.Controls.Add(this.cmbSalesMode);
            this.Controls.Add(this.lblSalesMode);
            this.Controls.Add(this.cmbPricingLevel);
            this.Controls.Add(this.lblPricingLevel);
            this.Controls.Add(this.lblCreaditPeriod);
            this.Controls.Add(this.cmbCashOrParty);
            this.Controls.Add(this.lblCashorParty);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtGrandTotal);
            this.Controls.Add(this.lblGrandTotal);
            this.Controls.Add(this.lnklblRemove);
            this.Controls.Add(this.txtNarration);
            this.Controls.Add(this.lblNarration);
            this.Controls.Add(this.txtInvoiceNo);
            this.Controls.Add(this.lblInvoiceNo);
            this.Controls.Add(this.dgvSalesInvoice);
            this.Controls.Add(this.lblDate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmSalesInvoice";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sales Invoice";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSalesInvoice_FormClosing);
            this.Load += new System.EventHandler(this.frmSalesInvoice_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSalesInvoice_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoiceTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesInvoiceLedger)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdvancePayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPaid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBillDiscount;
        private System.Windows.Forms.Label lblBillDiscount;
        private System.Windows.Forms.Label lblSalesAccount;
        private System.Windows.Forms.ComboBox cmbSalesMode;
        private System.Windows.Forms.Label lblSalesMode;
        private System.Windows.Forms.ComboBox cmbPricingLevel;
        private System.Windows.Forms.Label lblPricingLevel;
        private System.Windows.Forms.Label lblCreaditPeriod;
        private System.Windows.Forms.ComboBox cmbCashOrParty;
        private System.Windows.Forms.Label lblCashorParty;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.LinkLabel lnklblRemove;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private System.Windows.Forms.Label lblInvoiceNo;
        // private System.Windows.Forms.DataGridView dgvSalesInvoice;
        private dgv.DataGridViewEnter dgvSalesInvoice;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox txtCreditPeriod;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.ComboBox cmbSalesMan;
        private System.Windows.Forms.Label lblSalesMan;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label lblTotalQuantity;
        private System.Windows.Forms.Button btnNewLedger;
        private System.Windows.Forms.Button btnNewPricingLevel;
        private System.Windows.Forms.Button btnNewSalesman;
        private System.Windows.Forms.Button btnNewSalesAccount;
        private System.Windows.Forms.Label lblSalesModeOrderNo;
        private System.Windows.Forms.ComboBox cmbSalesModeOrderNo;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.Label lblFinalTotalAmount;
        private System.Windows.Forms.Label lblTotalQuantitydisplay;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.CheckBox cbxPrintAfterSave;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.ComboBox cmbCurrency;
        private System.Windows.Forms.ComboBox cmbVoucherType;
        private System.Windows.Forms.Label lblVoucherType;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTtaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxLedgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTTaxRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTCalculatingMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTApplicableOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvTxtTtaxName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTtaxId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTSlno;
        private dgv.DataGridViewEnter dgvSalesInvoiceTax;
        private System.Windows.Forms.Label lblLedgerTotal;
        private System.Windows.Forms.Label lblLedgerTotalAmount;
        private System.Windows.Forms.LinkLabel lnklblLedgerGridRemove;
        private System.Windows.Forms.Label lblDrorCr;
        private System.Windows.Forms.ComboBox cmbDrorCr;
        private System.Windows.Forms.Label lblcashOrBank;
        private System.Windows.Forms.ComboBox cmbCashOrbank;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAdditionalCoastledgerAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbAdditionalCostledgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAdditionalCostId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAditionalCostSlno;
        private dgv.DataGridViewEnter dgvSalesInvoiceLedger;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox dgvCreditNote;
        private System.Windows.Forms.Label lblCreditNoteToApply;
        private System.Windows.Forms.ComboBox cmbDgvCashOrBank;
        private System.Windows.Forms.Label lblBankOrCash;
        private System.Windows.Forms.Label label1;
        private dgv.DataGridViewEnter dgvAdvancePayments;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvChkApply;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtexchangeRateId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtInvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtvoucherTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtvoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmountToApply;
        private System.Windows.Forms.PictureBox imgPaid;
        private System.Windows.Forms.Label lblPaid;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Label lblAvailableQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceSlno;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceSalesDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSISalesOrderDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceDeliveryNoteDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceQuotationDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceProductName;
        private System.Windows.Forms.DataGridViewButtonColumn dgvbtnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtItemDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceQty;
        private System.Windows.Forms.DataGridViewButtonColumn dgvbtnQview;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceUnitConversionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceConversionRate;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvtxtSalesInvoicembUnitName;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbSalesInvoiceGodown;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbSalesInvoiceRack;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbSalesInvoiceBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoicePurchaseRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceMrp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceSalesRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceGrossValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceDiscountPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceDiscountAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceNetAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbSalesInvoiceTaxName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceTaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceInRowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceVoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceInvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceVoucherTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesInvoiceAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbCreditNote;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtsalesAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtproductType;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbProject;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbCategory;
        private System.Windows.Forms.CheckBox chkSendEMail;
        private System.Windows.Forms.Button btnSaveAndEmail;
        private System.Windows.Forms.Label lblTaxTotalAmount;
        private System.Windows.Forms.Label lblTaxTotal;
        private System.Windows.Forms.Label lblBalance2;
    }
}