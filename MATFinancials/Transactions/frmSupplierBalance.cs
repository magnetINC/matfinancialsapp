﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmSupplierBalance : Form
    {
        Padding newPadding = new Padding(50, 0, 0, 0);
        int intNoOfRowsToPrint = 0;
        public frmSupplierBalance()
        {
            InitializeComponent();
        }

        private void frmSupplierBalance_Load(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString("dd-MMM-yyyy");
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString("dd-MMM-yyyy");
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (intNoOfRowsToPrint == 0)
                {
                    MessageBox.Show("No row to print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Print();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL11:" + ex.Message;
            }
        }

        private void Print()
        {
            try
            {
                DataSet dsSupplierBalance = GetDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.SupplierBalancePrinting(dsSupplierBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL4:" + ex.Message;
            }
        }

        private DataSet GetDataSet()
        {
            DataSet dsSupplierBalance = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblSupplierBalance = GetDataTable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.ProfitAndLossReportPrintCompany(PublicVariables._decCurrentCompanyId);
                dsSupplierBalance.Tables.Add(dtblSupplierBalance);
                dsSupplierBalance.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL3:" + ex.Message;
            }
            return dsSupplierBalance;
        }

        private DataTable GetDataTable()
        {
            DataTable dtblSupplierBalance = new DataTable();
            try
            {
                dtblSupplierBalance.Columns.Add("S/No");
                dtblSupplierBalance.Columns.Add("Supplier");
                dtblSupplierBalance.Columns.Add("Balance");
                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvSupplierBalance.Rows)
                {
                    drow = dtblSupplierBalance.NewRow();
                    drow["S/No"] = dr.Cells["dgvtxtSNo"].Value;
                    drow["Supplier"] = dr.Cells["dgvtxtLedgerName"].Value;
                    drow["Balance"] = dr.Cells["dgvtxtBalance"].Value;
                    dtblSupplierBalance.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL2:" + ex.Message;
            }
            return dtblSupplierBalance;
        }

        public void FillGrid()
        {
            try
            {
                int gridRow = 0;
                decimal totalCredit = 0;
                DataSet dsSupplier = new DataSet();
                DataTable dtblSupplier = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();

                DBMatConnection connection = new DBMatConnection();
                string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                    "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                    "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 22 AND lp.date <= '{0}'" +
                    "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", txtToDate.Text.ToString());
                dsSupplier = connection.ExecuteQuery(query);

                dgvSupplierBalance.Rows.Clear();
                dtblSupplier = dsSupplier.Tables[0];

                dtblSupplier = (from d in dtblSupplier.AsEnumerable()
                                where d.Field<decimal>("openingBalance") != 0
                                select d).CopyToDataTable();
                this.dgvSupplierBalance.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                this.dgvSupplierBalance.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();

                int gridRowNumber = 0;
                for (int i = 0; i < dtblSupplier.Rows.Count; i++)
                {
                    dgvSupplierBalance.Rows.Add();
                    gridRowNumber++;
                    dgvSupplierBalance.Rows[i].Cells["dgvtxtSNo"].Value = gridRowNumber;
                    dgvSupplierBalance.Rows[i].Cells["dgvtxtLedgerName"].Value = dtblSupplier.Rows[i]["ledgerName"].ToString();
                    dgvSupplierBalance.Rows[i].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(dtblSupplier.Rows[i]["openingBalance"]).ToString("N2");
                    totalCredit += Convert.ToDecimal(dtblSupplier.Rows[i]["openingBalance"]);
                }
                gridRow = dgvSupplierBalance.Rows.Count - 1;
                dgvSupplierBalance.Rows.Add();
                dgvSupplierBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "____________________";
                dgvSupplierBalance.Rows.Add();
                gridRow++;
                dgvSupplierBalance.Rows[gridRow].DefaultCellStyle.Font = new Font(dgvSupplierBalance.Font, FontStyle.Bold);
                dgvSupplierBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "Total";
                dgvSupplierBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(totalCredit).ToString("N2");
                dgvSupplierBalance.Rows.Add();
                gridRow++;
                dgvSupplierBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "====================";
                intNoOfRowsToPrint = dtblSupplier.Rows.Count;
            }
            catch (Exception ex)
            {

            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = dtpFromDate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL6:" + ex.Message;
            }
        }

        private void dtpTodate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpTodate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL7:" + ex.Message;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            { }
        }
        public void Clear()
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString("dd-MMM-yyyy");
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString("dd-MMM-yyyy");
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {

            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvSupplierBalance, "SUPPLIER BALANCE AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }
    }
}
