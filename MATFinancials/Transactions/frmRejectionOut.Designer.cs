﻿namespace MATFinancials
{
    partial class frmRejectionOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRejectionOut));
            this.lblTotalAmount = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.cbxPrintAfterSave = new System.Windows.Forms.CheckBox();
            this.cmbCashOrParty = new System.Windows.Forms.ComboBox();
            this.lblCashorParty = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lnklblRemove = new System.Windows.Forms.LinkLabel();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.lblNarration = new System.Windows.Forms.Label();
            this.txtRejectionOutNo = new System.Windows.Forms.TextBox();
            this.lblRejectionoutno = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTransportationCompany = new System.Windows.Forms.Label();
            this.lblLrNo = new System.Windows.Forms.Label();
            this.txtLrNo = new System.Windows.Forms.TextBox();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnLedgerAdd = new System.Windows.Forms.Button();
            this.dgvProduct = new MATFinancials.dgv.DataGridViewEnter();
            this.txtTransportationCompany = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbVoucherType = new System.Windows.Forms.ComboBox();
            this.lblMaterialReceiptNo = new System.Windows.Forms.Label();
            this.cmbMaterialReceiptNo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblAvailableQuantity = new System.Windows.Forms.Label();
            this.dgvtxtSlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtinvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtvoucherTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtvoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InRowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbUnitPerQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbUnitPerRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtmaterialReceiptDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtRejectionOutDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtproductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvbtnQview = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvcmbUnit = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.unitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtunitConversionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtconversionRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtUnitId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbGodown = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbRack = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbBatch = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbProject = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvCmbCategory = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.AutoSize = true;
            this.lblTotalAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalAmount.ForeColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Location = new System.Drawing.Point(767, 450);
            this.lblTotalAmount.Margin = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Size = new System.Drawing.Size(70, 13);
            this.lblTotalAmount.TabIndex = 1015;
            this.lblTotalAmount.Text = "Total Amount";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Location = new System.Drawing.Point(862, 447);
            this.txtTotalAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalAmount.Size = new System.Drawing.Size(200, 20);
            this.txtTotalAmount.TabIndex = 67969;
            // 
            // cbxPrintAfterSave
            // 
            this.cbxPrintAfterSave.AutoSize = true;
            this.cbxPrintAfterSave.BackColor = System.Drawing.Color.Transparent;
            this.cbxPrintAfterSave.ForeColor = System.Drawing.Color.Black;
            this.cbxPrintAfterSave.Location = new System.Drawing.Point(20, 473);
            this.cbxPrintAfterSave.Name = "cbxPrintAfterSave";
            this.cbxPrintAfterSave.Size = new System.Drawing.Size(97, 17);
            this.cbxPrintAfterSave.TabIndex = 15;
            this.cbxPrintAfterSave.Text = "Print after save";
            this.cbxPrintAfterSave.UseVisualStyleBackColor = false;
            this.cbxPrintAfterSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxPrintAfterSave_KeyDown);
            // 
            // cmbCashOrParty
            // 
            this.cmbCashOrParty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrParty.FormattingEnabled = true;
            this.cmbCashOrParty.Location = new System.Drawing.Point(141, 39);
            this.cmbCashOrParty.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCashOrParty.Name = "cmbCashOrParty";
            this.cmbCashOrParty.Size = new System.Drawing.Size(200, 21);
            this.cmbCashOrParty.TabIndex = 2;
            this.cmbCashOrParty.SelectedValueChanged += new System.EventHandler(this.cmbCashOrParty_SelectedValueChanged_1);
            this.cmbCashOrParty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrParty_KeyDown);
            // 
            // lblCashorParty
            // 
            this.lblCashorParty.AutoSize = true;
            this.lblCashorParty.BackColor = System.Drawing.Color.Transparent;
            this.lblCashorParty.ForeColor = System.Drawing.Color.Black;
            this.lblCashorParty.Location = new System.Drawing.Point(18, 43);
            this.lblCashorParty.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCashorParty.Name = "lblCashorParty";
            this.lblCashorParty.Size = new System.Drawing.Size(45, 13);
            this.lblCashorParty.TabIndex = 1001;
            this.lblCashorParty.Text = "Supplier";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(979, 471);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(888, 471);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(706, 471);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyUp);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(797, 471);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 27);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lnklblRemove
            // 
            this.lnklblRemove.ActiveLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.AutoSize = true;
            this.lnklblRemove.LinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.Location = new System.Drawing.Point(1018, 376);
            this.lnklblRemove.Margin = new System.Windows.Forms.Padding(5);
            this.lnklblRemove.Name = "lnklblRemove";
            this.lnklblRemove.Size = new System.Drawing.Size(47, 13);
            this.lnklblRemove.TabIndex = 5678;
            this.lnklblRemove.TabStop = true;
            this.lnklblRemove.Text = "Remove";
            this.lnklblRemove.VisitedLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblRemove_LinkClicked);
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(862, 393);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(200, 50);
            this.txtNarration.TabIndex = 9;
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            this.txtNarration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNarration_KeyPress);
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(767, 393);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(50, 13);
            this.lblNarration.TabIndex = 993;
            this.lblNarration.Text = "Narration";
            // 
            // txtRejectionOutNo
            // 
            this.txtRejectionOutNo.Location = new System.Drawing.Point(141, 14);
            this.txtRejectionOutNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtRejectionOutNo.Name = "txtRejectionOutNo";
            this.txtRejectionOutNo.Size = new System.Drawing.Size(200, 20);
            this.txtRejectionOutNo.TabIndex = 0;
            this.txtRejectionOutNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRejectionOutNo_KeyDown);
            // 
            // lblRejectionoutno
            // 
            this.lblRejectionoutno.AutoSize = true;
            this.lblRejectionoutno.BackColor = System.Drawing.Color.Transparent;
            this.lblRejectionoutno.ForeColor = System.Drawing.Color.Black;
            this.lblRejectionoutno.Location = new System.Drawing.Point(18, 18);
            this.lblRejectionoutno.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblRejectionoutno.Name = "lblRejectionoutno";
            this.lblRejectionoutno.Size = new System.Drawing.Size(92, 13);
            this.lblRejectionoutno.TabIndex = 991;
            this.lblRejectionoutno.Text = "Rejection Out No.";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(738, 22);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 988;
            this.lblDate.Text = "Date";
            // 
            // lblTransportationCompany
            // 
            this.lblTransportationCompany.AutoSize = true;
            this.lblTransportationCompany.BackColor = System.Drawing.Color.Transparent;
            this.lblTransportationCompany.ForeColor = System.Drawing.Color.Black;
            this.lblTransportationCompany.Location = new System.Drawing.Point(18, 385);
            this.lblTransportationCompany.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblTransportationCompany.Name = "lblTransportationCompany";
            this.lblTransportationCompany.Size = new System.Drawing.Size(122, 13);
            this.lblTransportationCompany.TabIndex = 1020;
            this.lblTransportationCompany.Text = "Transportation Company";
            // 
            // lblLrNo
            // 
            this.lblLrNo.AutoSize = true;
            this.lblLrNo.BackColor = System.Drawing.Color.Transparent;
            this.lblLrNo.ForeColor = System.Drawing.Color.Black;
            this.lblLrNo.Location = new System.Drawing.Point(18, 410);
            this.lblLrNo.Margin = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.lblLrNo.Name = "lblLrNo";
            this.lblLrNo.Size = new System.Drawing.Size(41, 13);
            this.lblLrNo.TabIndex = 1022;
            this.lblLrNo.Text = "LR No.";
            // 
            // txtLrNo
            // 
            this.txtLrNo.Location = new System.Drawing.Point(146, 406);
            this.txtLrNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtLrNo.Name = "txtLrNo";
            this.txtLrNo.Size = new System.Drawing.Size(200, 20);
            this.txtLrNo.TabIndex = 10;
            this.txtLrNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLrNo_KeyDown);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(862, 17);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(181, 20);
            this.txtDate.TabIndex = 1;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(1042, 17);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(20, 20);
            this.dtpDate.TabIndex = 686758;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpRejectionOutDate_ValueChanged);
            // 
            // btnLedgerAdd
            // 
            this.btnLedgerAdd.BackColor = System.Drawing.Color.Gray;
            this.btnLedgerAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLedgerAdd.FlatAppearance.BorderSize = 0;
            this.btnLedgerAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLedgerAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLedgerAdd.ForeColor = System.Drawing.Color.White;
            this.btnLedgerAdd.Location = new System.Drawing.Point(349, 39);
            this.btnLedgerAdd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnLedgerAdd.Name = "btnLedgerAdd";
            this.btnLedgerAdd.Size = new System.Drawing.Size(21, 20);
            this.btnLedgerAdd.TabIndex = 3;
            this.btnLedgerAdd.Text = "+";
            this.btnLedgerAdd.UseVisualStyleBackColor = false;
            this.btnLedgerAdd.Click += new System.EventHandler(this.btnLedgerAdd_Click);
            // 
            // dgvProduct
            // 
            this.dgvProduct.AllowUserToAddRows = false;
            this.dgvProduct.AllowUserToDeleteRows = false;
            this.dgvProduct.AllowUserToResizeColumns = false;
            this.dgvProduct.AllowUserToResizeRows = false;
            this.dgvProduct.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProduct.BackgroundColor = System.Drawing.Color.White;
            this.dgvProduct.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProduct.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProduct.ColumnHeadersHeight = 35;
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSlNo,
            this.dgvtxtinvoiceNo,
            this.dgvtxtvoucherTypeId,
            this.dgvtxtvoucherNo,
            this.InRowIndex,
            this.dgvcmbUnitPerQuantity,
            this.dgvcmbUnitPerRate,
            this.dgvtxtmaterialReceiptDetailsId,
            this.dgvtxtRejectionOutDetailsId,
            this.dgvtxtBarcode,
            this.dgvtxtproductId,
            this.dgvtxtProductCode,
            this.dgvtxtProductName,
            this.dgvtxtQty,
            this.dgvbtnQview,
            this.dgvcmbUnit,
            this.unitName,
            this.dgvtxtunitConversionId,
            this.dgvtxtconversionRate,
            this.dgvtxtUnitId,
            this.dgvcmbGodown,
            this.dgvcmbRack,
            this.dgvcmbBatch,
            this.dgvtxtRate,
            this.dgvtxtAmount,
            this.dgvCmbProject,
            this.dgvCmbCategory});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduct.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvProduct.EnableHeadersVisualStyles = false;
            this.dgvProduct.GridColor = System.Drawing.Color.DimGray;
            this.dgvProduct.Location = new System.Drawing.Point(18, 98);
            this.dgvProduct.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvProduct.RowHeadersVisible = false;
            this.dgvProduct.Size = new System.Drawing.Size(1045, 263);
            this.dgvProduct.TabIndex = 7;
            this.dgvProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellClick);
            this.dgvProduct.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellEndEdit);
            this.dgvProduct.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellEnter);
            this.dgvProduct.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgvProduct_CurrentCellDirtyStateChanged);
            this.dgvProduct.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvProduct_DataError);
            this.dgvProduct.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvProduct_EditingControlShowing);
            this.dgvProduct.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvProduct_RowsAdded);
            this.dgvProduct.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProduct_KeyDown);
            // 
            // txtTransportationCompany
            // 
            this.txtTransportationCompany.Location = new System.Drawing.Point(146, 381);
            this.txtTransportationCompany.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTransportationCompany.Name = "txtTransportationCompany";
            this.txtTransportationCompany.Size = new System.Drawing.Size(200, 20);
            this.txtTransportationCompany.TabIndex = 8;
            this.txtTransportationCompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTransportationCompany_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(738, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1148;
            this.label1.Text = "Apply On";
            // 
            // cmbVoucherType
            // 
            this.cmbVoucherType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVoucherType.FormattingEnabled = true;
            this.cmbVoucherType.Location = new System.Drawing.Point(862, 42);
            this.cmbVoucherType.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbVoucherType.Name = "cmbVoucherType";
            this.cmbVoucherType.Size = new System.Drawing.Size(200, 21);
            this.cmbVoucherType.TabIndex = 4;
            this.cmbVoucherType.SelectedValueChanged += new System.EventHandler(this.cmbVoucherType_SelectedValueChanged);
            this.cmbVoucherType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbVoucherType_KeyDown);
            // 
            // lblMaterialReceiptNo
            // 
            this.lblMaterialReceiptNo.AutoSize = true;
            this.lblMaterialReceiptNo.BackColor = System.Drawing.Color.Transparent;
            this.lblMaterialReceiptNo.ForeColor = System.Drawing.Color.Black;
            this.lblMaterialReceiptNo.Location = new System.Drawing.Point(18, 69);
            this.lblMaterialReceiptNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblMaterialReceiptNo.Name = "lblMaterialReceiptNo";
            this.lblMaterialReceiptNo.Size = new System.Drawing.Size(104, 13);
            this.lblMaterialReceiptNo.TabIndex = 1005;
            this.lblMaterialReceiptNo.Text = "Material Receipt No.";
            // 
            // cmbMaterialReceiptNo
            // 
            this.cmbMaterialReceiptNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaterialReceiptNo.FormattingEnabled = true;
            this.cmbMaterialReceiptNo.Location = new System.Drawing.Point(141, 65);
            this.cmbMaterialReceiptNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbMaterialReceiptNo.Name = "cmbMaterialReceiptNo";
            this.cmbMaterialReceiptNo.Size = new System.Drawing.Size(202, 21);
            this.cmbMaterialReceiptNo.TabIndex = 5;
            this.cmbMaterialReceiptNo.SelectedValueChanged += new System.EventHandler(this.cmbMaterialReceiptNo_SelectedValueChanged);
            this.cmbMaterialReceiptNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbMaterialReceiptNo_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(347, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 1149;
            this.label2.Text = "*";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Location = new System.Drawing.Point(862, 68);
            this.cmbCurrency.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(201, 21);
            this.cmbCurrency.TabIndex = 6;
            this.cmbCurrency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCurrency_KeyDown);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrency.ForeColor = System.Drawing.Color.Black;
            this.lblCurrency.Location = new System.Drawing.Point(738, 73);
            this.lblCurrency.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(52, 13);
            this.lblCurrency.TabIndex = 1151;
            this.lblCurrency.Text = "Currency ";
            // 
            // lblAvailableQuantity
            // 
            this.lblAvailableQuantity.AutoSize = true;
            this.lblAvailableQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvailableQuantity.Location = new System.Drawing.Point(438, 76);
            this.lblAvailableQuantity.Name = "lblAvailableQuantity";
            this.lblAvailableQuantity.Size = new System.Drawing.Size(23, 15);
            this.lblAvailableQuantity.TabIndex = 686759;
            this.lblAvailableQuantity.Text = "AV";
            // 
            // dgvtxtSlNo
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dgvtxtSlNo.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtSlNo.FillWeight = 96.75908F;
            this.dgvtxtSlNo.HeaderText = "Sl No";
            this.dgvtxtSlNo.Name = "dgvtxtSlNo";
            this.dgvtxtSlNo.ReadOnly = true;
            this.dgvtxtSlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtinvoiceNo
            // 
            this.dgvtxtinvoiceNo.HeaderText = "invoiceNo";
            this.dgvtxtinvoiceNo.Name = "dgvtxtinvoiceNo";
            this.dgvtxtinvoiceNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtinvoiceNo.Visible = false;
            // 
            // dgvtxtvoucherTypeId
            // 
            this.dgvtxtvoucherTypeId.HeaderText = "voucherTypeId";
            this.dgvtxtvoucherTypeId.Name = "dgvtxtvoucherTypeId";
            this.dgvtxtvoucherTypeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtvoucherTypeId.Visible = false;
            // 
            // dgvtxtvoucherNo
            // 
            this.dgvtxtvoucherNo.HeaderText = "voucherNo";
            this.dgvtxtvoucherNo.Name = "dgvtxtvoucherNo";
            this.dgvtxtvoucherNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtvoucherNo.Visible = false;
            // 
            // InRowIndex
            // 
            this.InRowIndex.HeaderText = "inRowIndex";
            this.InRowIndex.Name = "InRowIndex";
            this.InRowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.InRowIndex.Visible = false;
            // 
            // dgvcmbUnitPerQuantity
            // 
            this.dgvcmbUnitPerQuantity.HeaderText = "UnitperQuantity";
            this.dgvcmbUnitPerQuantity.Name = "dgvcmbUnitPerQuantity";
            this.dgvcmbUnitPerQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvcmbUnitPerQuantity.Visible = false;
            // 
            // dgvcmbUnitPerRate
            // 
            this.dgvcmbUnitPerRate.HeaderText = "UnitperRate";
            this.dgvcmbUnitPerRate.Name = "dgvcmbUnitPerRate";
            this.dgvcmbUnitPerRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvcmbUnitPerRate.Visible = false;
            // 
            // dgvtxtmaterialReceiptDetailsId
            // 
            this.dgvtxtmaterialReceiptDetailsId.HeaderText = "MaterialReceiptDetailsId";
            this.dgvtxtmaterialReceiptDetailsId.Name = "dgvtxtmaterialReceiptDetailsId";
            this.dgvtxtmaterialReceiptDetailsId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtmaterialReceiptDetailsId.Visible = false;
            // 
            // dgvtxtRejectionOutDetailsId
            // 
            this.dgvtxtRejectionOutDetailsId.HeaderText = "RejectionOutDetailsId";
            this.dgvtxtRejectionOutDetailsId.Name = "dgvtxtRejectionOutDetailsId";
            this.dgvtxtRejectionOutDetailsId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtRejectionOutDetailsId.Visible = false;
            // 
            // dgvtxtBarcode
            // 
            this.dgvtxtBarcode.FillWeight = 96.75908F;
            this.dgvtxtBarcode.HeaderText = "Barcode";
            this.dgvtxtBarcode.Name = "dgvtxtBarcode";
            this.dgvtxtBarcode.ReadOnly = true;
            this.dgvtxtBarcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtproductId
            // 
            this.dgvtxtproductId.HeaderText = "ProductId";
            this.dgvtxtproductId.Name = "dgvtxtproductId";
            this.dgvtxtproductId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtproductId.Visible = false;
            // 
            // dgvtxtProductCode
            // 
            this.dgvtxtProductCode.FillWeight = 96.75908F;
            this.dgvtxtProductCode.HeaderText = "Product Code";
            this.dgvtxtProductCode.Name = "dgvtxtProductCode";
            this.dgvtxtProductCode.ReadOnly = true;
            this.dgvtxtProductCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtProductName
            // 
            this.dgvtxtProductName.FillWeight = 96.75908F;
            this.dgvtxtProductName.HeaderText = "Product Name";
            this.dgvtxtProductName.Name = "dgvtxtProductName";
            this.dgvtxtProductName.ReadOnly = true;
            this.dgvtxtProductName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtQty
            // 
            this.dgvtxtQty.DataPropertyName = "qty";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtQty.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtQty.FillWeight = 96.75908F;
            this.dgvtxtQty.HeaderText = "Qty";
            this.dgvtxtQty.MaxInputLength = 8;
            this.dgvtxtQty.Name = "dgvtxtQty";
            this.dgvtxtQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvbtnQview
            // 
            this.dgvbtnQview.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgvbtnQview.FillWeight = 142.132F;
            this.dgvbtnQview.HeaderText = "";
            this.dgvbtnQview.Name = "dgvbtnQview";
            this.dgvbtnQview.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbtnQview.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvbtnQview.Width = 20;
            // 
            // dgvcmbUnit
            // 
            this.dgvcmbUnit.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dgvcmbUnit.FillWeight = 96.75908F;
            this.dgvcmbUnit.HeaderText = "Unit";
            this.dgvcmbUnit.Name = "dgvcmbUnit";
            this.dgvcmbUnit.ReadOnly = true;
            this.dgvcmbUnit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // unitName
            // 
            this.unitName.DataPropertyName = "unitName";
            this.unitName.HeaderText = "UnitName";
            this.unitName.Name = "unitName";
            this.unitName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.unitName.Visible = false;
            // 
            // dgvtxtunitConversionId
            // 
            this.dgvtxtunitConversionId.HeaderText = "UnitConversionId";
            this.dgvtxtunitConversionId.Name = "dgvtxtunitConversionId";
            this.dgvtxtunitConversionId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtunitConversionId.Visible = false;
            // 
            // dgvtxtconversionRate
            // 
            this.dgvtxtconversionRate.HeaderText = "ConversionRate";
            this.dgvtxtconversionRate.Name = "dgvtxtconversionRate";
            this.dgvtxtconversionRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtconversionRate.Visible = false;
            // 
            // dgvtxtUnitId
            // 
            this.dgvtxtUnitId.HeaderText = "UnitId";
            this.dgvtxtUnitId.Name = "dgvtxtUnitId";
            this.dgvtxtUnitId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtUnitId.Visible = false;
            // 
            // dgvcmbGodown
            // 
            this.dgvcmbGodown.FillWeight = 96.75908F;
            this.dgvcmbGodown.HeaderText = "Store";
            this.dgvcmbGodown.Name = "dgvcmbGodown";
            this.dgvcmbGodown.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvcmbRack
            // 
            this.dgvcmbRack.FillWeight = 96.75908F;
            this.dgvcmbRack.HeaderText = "Rack";
            this.dgvcmbRack.Name = "dgvcmbRack";
            this.dgvcmbRack.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvcmbBatch
            // 
            this.dgvcmbBatch.FillWeight = 96.75908F;
            this.dgvcmbBatch.HeaderText = "Batch";
            this.dgvcmbBatch.Name = "dgvcmbBatch";
            this.dgvcmbBatch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dgvtxtRate
            // 
            this.dgvtxtRate.DataPropertyName = "rate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dgvtxtRate.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvtxtRate.FillWeight = 96.75908F;
            this.dgvtxtRate.HeaderText = "Rate";
            this.dgvtxtRate.MaxInputLength = 13;
            this.dgvtxtRate.Name = "dgvtxtRate";
            this.dgvtxtRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtAmount
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dgvtxtAmount.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvtxtAmount.FillWeight = 96.75908F;
            this.dgvtxtAmount.HeaderText = "Amount";
            this.dgvtxtAmount.Name = "dgvtxtAmount";
            this.dgvtxtAmount.ReadOnly = true;
            this.dgvtxtAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvCmbProject
            // 
            this.dgvCmbProject.FillWeight = 96.75908F;
            this.dgvCmbProject.HeaderText = "Project";
            this.dgvCmbProject.Name = "dgvCmbProject";
            // 
            // dgvCmbCategory
            // 
            this.dgvCmbCategory.FillWeight = 96.75908F;
            this.dgvCmbCategory.HeaderText = "Category";
            this.dgvCmbCategory.Name = "dgvCmbCategory";
            // 
            // frmRejectionOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.lblAvailableQuantity);
            this.Controls.Add(this.cmbCurrency);
            this.Controls.Add(this.lblCurrency);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbVoucherType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLedgerAdd);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.lblLrNo);
            this.Controls.Add(this.txtLrNo);
            this.Controls.Add(this.lblTransportationCompany);
            this.Controls.Add(this.txtTransportationCompany);
            this.Controls.Add(this.lblTotalAmount);
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.cbxPrintAfterSave);
            this.Controls.Add(this.cmbMaterialReceiptNo);
            this.Controls.Add(this.lblMaterialReceiptNo);
            this.Controls.Add(this.cmbCashOrParty);
            this.Controls.Add(this.lblCashorParty);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lnklblRemove);
            this.Controls.Add(this.txtNarration);
            this.Controls.Add(this.lblNarration);
            this.Controls.Add(this.txtRejectionOutNo);
            this.Controls.Add(this.lblRejectionoutno);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.dgvProduct);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmRejectionOut";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rejection Out";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRejectionOut_FormClosing);
            this.Load += new System.EventHandler(this.frmRejectionOut_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRejectionOut_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTotalAmount;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.CheckBox cbxPrintAfterSave;
        private System.Windows.Forms.ComboBox cmbCashOrParty;
        private System.Windows.Forms.Label lblCashorParty;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.LinkLabel lnklblRemove;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.TextBox txtRejectionOutNo;
        private System.Windows.Forms.Label lblRejectionoutno;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTransportationCompany;
        private System.Windows.Forms.Label lblLrNo;
        private System.Windows.Forms.TextBox txtLrNo;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Button btnLedgerAdd;
        private System.Windows.Forms.TextBox txtTransportationCompany;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbVoucherType;
        private dgv.DataGridViewEnter dgvProduct;
        private System.Windows.Forms.Label lblMaterialReceiptNo;
        private System.Windows.Forms.ComboBox cmbMaterialReceiptNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCurrency;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.Label lblAvailableQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtinvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtvoucherTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtvoucherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn InRowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcmbUnitPerQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcmbUnitPerRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtmaterialReceiptDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtRejectionOutDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtproductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtQty;
        private System.Windows.Forms.DataGridViewButtonColumn dgvbtnQview;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn unitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtunitConversionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtconversionRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtUnitId;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbGodown;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbRack;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbProject;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbCategory;
    }
}