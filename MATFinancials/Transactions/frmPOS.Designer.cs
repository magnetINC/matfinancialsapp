﻿namespace MATFinancials
{
    partial class frmPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPOS));
            this.btnNewSalesMan = new System.Windows.Forms.Button();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCashOrParty = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblCashOrParty = new System.Windows.Forms.Label();
            this.dgvPointOfSales = new System.Windows.Forms.DataGridView();
            this.lblDate = new System.Windows.Forms.Label();
            this.cmbSalesMan = new System.Windows.Forms.ComboBox();
            this.lblSalesMan = new System.Windows.Forms.Label();
            this.btnNewLedger = new System.Windows.Forms.Button();
            this.btnNewSalesAccount = new System.Windows.Forms.Button();
            this.cmbSalesAccount = new System.Windows.Forms.ComboBox();
            this.lblsalesAccount = new System.Windows.Forms.Label();
            this.btnNewCounter = new System.Windows.Forms.Button();
            this.cmbCounter = new System.Windows.Forms.ComboBox();
            this.lblCounter = new System.Windows.Forms.Label();
            this.lnklblRemove = new System.Windows.Forms.LinkLabel();
            this.dgvPOSTax = new System.Windows.Forms.DataGridView();
            this.dgvtxtSINO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxLedgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxttaxrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxttax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxApplicableOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxttaxCalculateMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.temp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblLedgerTotal = new System.Windows.Forms.Label();
            this.cbxPrintAfterSave = new System.Windows.Forms.CheckBox();
            this.lblTaxTotalAmount = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbPricingLevel = new System.Windows.Forms.ComboBox();
            this.lblPricingLevel = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radCash = new System.Windows.Forms.RadioButton();
            this.radBank = new System.Windows.Forms.RadioButton();
            this.gbxAmountDetails = new System.Windows.Forms.GroupBox();
            this.txtCardAmount = new System.Windows.Forms.TextBox();
            this.txtCardRefNo = new System.Windows.Forms.TextBox();
            this.lblCardRefNo = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblCardAmount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtPaidAmount = new System.Windows.Forms.TextBox();
            this.lblPaidAmount = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.txtBillDiscount = new System.Windows.Forms.TextBox();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.gbxDetails = new System.Windows.Forms.GroupBox();
            this.lblUnitConversion = new System.Windows.Forms.Label();
            this.lblUnitConversionRate = new System.Windows.Forms.Label();
            this.lblUnitConversionId = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblAmount = new System.Windows.Forms.Label();
            this.txtDiscountAmount = new System.Windows.Forms.TextBox();
            this.lblDiscountAmt = new System.Windows.Forms.Label();
            this.txtDiscountPercentage = new System.Windows.Forms.TextBox();
            this.lblDiscountPercentage = new System.Windows.Forms.Label();
            this.txtNetAmount = new System.Windows.Forms.TextBox();
            this.lblnetAmount = new System.Windows.Forms.Label();
            this.txtTaxAmount = new System.Windows.Forms.TextBox();
            this.lblTaxAmount = new System.Windows.Forms.Label();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.lblTax = new System.Windows.Forms.Label();
            this.txtGrossValue = new System.Windows.Forms.TextBox();
            this.lblGrossValue = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.lblRate = new System.Windows.Forms.Label();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.cmbBatch = new System.Windows.Forms.ComboBox();
            this.lblBatch = new System.Windows.Forms.Label();
            this.cmbRack = new System.Windows.Forms.ComboBox();
            this.lblRack = new System.Windows.Forms.Label();
            this.cmbGodown = new System.Windows.Forms.ComboBox();
            this.lblGodown = new System.Windows.Forms.Label();
            this.cmbItem = new System.Windows.Forms.ComboBox();
            this.lblItem = new System.Windows.Forms.Label();
            this.txtProductCode = new System.Windows.Forms.TextBox();
            this.lblProductcode = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.radBoth = new System.Windows.Forms.RadioButton();
            this.radCustomer = new System.Windows.Forms.RadioButton();
            this.cmbCashAndBank = new System.Windows.Forms.ComboBox();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.lblAvailableQuantity = new System.Windows.Forms.Label();
            this.cmbDefaultPrinter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.radCheque = new System.Windows.Forms.RadioButton();
            this.lblDiscountPercent = new System.Windows.Forms.Label();
            this.txtDiscountPercent = new System.Windows.Forms.TextBox();
            this.btnHold = new System.Windows.Forms.Button();
            this.dgvtxtSlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGrossValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtNetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxttaxid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTaxAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtTotalAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtBatchId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtBatchno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtRackId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtRackName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGodownId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGodownName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtUnitId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtunitconversionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtDiscountPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProjectId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtCategoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtSalesAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPointOfSales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPOSTax)).BeginInit();
            this.gbxAmountDetails.SuspendLayout();
            this.gbxDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNewSalesMan
            // 
            this.btnNewSalesMan.BackColor = System.Drawing.Color.Gray;
            this.btnNewSalesMan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewSalesMan.FlatAppearance.BorderSize = 0;
            this.btnNewSalesMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewSalesMan.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewSalesMan.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewSalesMan.Location = new System.Drawing.Point(985, 7);
            this.btnNewSalesMan.Name = "btnNewSalesMan";
            this.btnNewSalesMan.Size = new System.Drawing.Size(34, 27);
            this.btnNewSalesMan.TabIndex = 5;
            this.btnNewSalesMan.Text = "+";
            this.btnNewSalesMan.UseVisualStyleBackColor = false;
            this.btnNewSalesMan.Visible = false;
            this.btnNewSalesMan.Click += new System.EventHandler(this.btnNewSalesMan_Click);
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.Location = new System.Drawing.Point(108, 9);
            this.txtVoucherNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(188, 26);
            this.txtVoucherNo.TabIndex = 0;
            this.txtVoucherNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVoucherNo_KeyDown);
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.BackColor = System.Drawing.Color.Transparent;
            this.lblVoucherNo.ForeColor = System.Drawing.Color.Black;
            this.lblVoucherNo.Location = new System.Drawing.Point(20, 12);
            this.lblVoucherNo.Margin = new System.Windows.Forms.Padding(5);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(78, 19);
            this.lblVoucherNo.TabIndex = 1119;
            this.lblVoucherNo.Text = "Receipt No.";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(825, 542);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // cmbCashOrParty
            // 
            this.cmbCashOrParty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrParty.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCashOrParty.FormattingEnabled = true;
            this.cmbCashOrParty.Location = new System.Drawing.Point(106, 71);
            this.cmbCashOrParty.Margin = new System.Windows.Forms.Padding(5);
            this.cmbCashOrParty.Name = "cmbCashOrParty";
            this.cmbCashOrParty.Size = new System.Drawing.Size(181, 25);
            this.cmbCashOrParty.TabIndex = 5;
            this.cmbCashOrParty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrParty_KeyDown);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(1098, 542);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(1007, 542);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 22;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(916, 542);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 27);
            this.btnClear.TabIndex = 21;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblCashOrParty
            // 
            this.lblCashOrParty.AutoSize = true;
            this.lblCashOrParty.BackColor = System.Drawing.Color.Transparent;
            this.lblCashOrParty.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashOrParty.ForeColor = System.Drawing.Color.Black;
            this.lblCashOrParty.Location = new System.Drawing.Point(21, 48);
            this.lblCashOrParty.Margin = new System.Windows.Forms.Padding(5);
            this.lblCashOrParty.Name = "lblCashOrParty";
            this.lblCashOrParty.Size = new System.Drawing.Size(78, 15);
            this.lblCashOrParty.TabIndex = 1113;
            this.lblCashOrParty.Text = "Select Option";
            // 
            // dgvPointOfSales
            // 
            this.dgvPointOfSales.AllowUserToAddRows = false;
            this.dgvPointOfSales.AllowUserToDeleteRows = false;
            this.dgvPointOfSales.AllowUserToResizeColumns = false;
            this.dgvPointOfSales.AllowUserToResizeRows = false;
            this.dgvPointOfSales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPointOfSales.BackgroundColor = System.Drawing.Color.White;
            this.dgvPointOfSales.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPointOfSales.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPointOfSales.ColumnHeadersHeight = 35;
            this.dgvPointOfSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvPointOfSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSlNo,
            this.rowId,
            this.dgvtxtProductId,
            this.dgvtxtProductCode,
            this.dgvtxtProductName,
            this.dgvtxtQuantity,
            this.dgvtxtUnit,
            this.dgvtxtRate,
            this.dgvtxtGrossValue,
            this.dgvtxtDiscount,
            this.dgvtxtNetAmount,
            this.dgvtxtTaxPercentage,
            this.dgvtxttaxid,
            this.dgvtxtTaxAmount,
            this.dgvtxtTotalAmount,
            this.dgvtxtBatchId,
            this.dgvtxtBatchno,
            this.dgvtxtRackId,
            this.dgvtxtRackName,
            this.dgvtxtGodownId,
            this.dgvtxtGodownName,
            this.dgvtxtUnitId,
            this.dgvtxtunitconversionId,
            this.dgvtxtBarcode,
            this.dgvtxtDiscountPercentage,
            this.dgvtxtSalesDetailsId,
            this.dgvtxtProjectId,
            this.dgvtxtCategoryId,
            this.dgvtxtSalesAccount});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPointOfSales.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvPointOfSales.EnableHeadersVisualStyles = false;
            this.dgvPointOfSales.GridColor = System.Drawing.Color.DimGray;
            this.dgvPointOfSales.Location = new System.Drawing.Point(6, 261);
            this.dgvPointOfSales.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.dgvPointOfSales.MultiSelect = false;
            this.dgvPointOfSales.Name = "dgvPointOfSales";
            this.dgvPointOfSales.ReadOnly = true;
            this.dgvPointOfSales.RowHeadersVisible = false;
            this.dgvPointOfSales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvPointOfSales.Size = new System.Drawing.Size(1199, 153);
            this.dgvPointOfSales.TabIndex = 13;
            this.dgvPointOfSales.TabStop = false;
            this.dgvPointOfSales.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPointOfSales_CellDoubleClick);
            this.dgvPointOfSales.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPointOfSales_RowsAdded);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(305, 14);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(38, 19);
            this.lblDate.TabIndex = 1122;
            this.lblDate.Text = "Date";
            // 
            // cmbSalesMan
            // 
            this.cmbSalesMan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesMan.FormattingEnabled = true;
            this.cmbSalesMan.Location = new System.Drawing.Point(936, 7);
            this.cmbSalesMan.Margin = new System.Windows.Forms.Padding(5);
            this.cmbSalesMan.Name = "cmbSalesMan";
            this.cmbSalesMan.Size = new System.Drawing.Size(70, 27);
            this.cmbSalesMan.TabIndex = 4;
            this.cmbSalesMan.Visible = false;
            this.cmbSalesMan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSalesMan_KeyDown);
            // 
            // lblSalesMan
            // 
            this.lblSalesMan.AutoSize = true;
            this.lblSalesMan.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesMan.ForeColor = System.Drawing.Color.Black;
            this.lblSalesMan.Location = new System.Drawing.Point(862, 11);
            this.lblSalesMan.Margin = new System.Windows.Forms.Padding(5);
            this.lblSalesMan.Name = "lblSalesMan";
            this.lblSalesMan.Size = new System.Drawing.Size(66, 19);
            this.lblSalesMan.TabIndex = 1124;
            this.lblSalesMan.Text = "Salesman";
            this.lblSalesMan.Visible = false;
            // 
            // btnNewLedger
            // 
            this.btnNewLedger.BackColor = System.Drawing.Color.Gray;
            this.btnNewLedger.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewLedger.FlatAppearance.BorderSize = 0;
            this.btnNewLedger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewLedger.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewLedger.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewLedger.Location = new System.Drawing.Point(488, 72);
            this.btnNewLedger.Name = "btnNewLedger";
            this.btnNewLedger.Size = new System.Drawing.Size(28, 24);
            this.btnNewLedger.TabIndex = 7;
            this.btnNewLedger.Text = "+";
            this.btnNewLedger.UseVisualStyleBackColor = false;
            this.btnNewLedger.Click += new System.EventHandler(this.btnNewLedger_Click);
            // 
            // btnNewSalesAccount
            // 
            this.btnNewSalesAccount.BackColor = System.Drawing.Color.Gray;
            this.btnNewSalesAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewSalesAccount.FlatAppearance.BorderSize = 0;
            this.btnNewSalesAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewSalesAccount.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewSalesAccount.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewSalesAccount.Location = new System.Drawing.Point(1205, 67);
            this.btnNewSalesAccount.Name = "btnNewSalesAccount";
            this.btnNewSalesAccount.Size = new System.Drawing.Size(28, 21);
            this.btnNewSalesAccount.TabIndex = 9;
            this.btnNewSalesAccount.Text = "+";
            this.btnNewSalesAccount.UseVisualStyleBackColor = false;
            this.btnNewSalesAccount.Visible = false;
            this.btnNewSalesAccount.Click += new System.EventHandler(this.btnNewSalesAccount_Click);
            // 
            // cmbSalesAccount
            // 
            this.cmbSalesAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesAccount.FormattingEnabled = true;
            this.cmbSalesAccount.Location = new System.Drawing.Point(1088, 105);
            this.cmbSalesAccount.Margin = new System.Windows.Forms.Padding(5);
            this.cmbSalesAccount.Name = "cmbSalesAccount";
            this.cmbSalesAccount.Size = new System.Drawing.Size(84, 27);
            this.cmbSalesAccount.TabIndex = 7;
            this.cmbSalesAccount.Visible = false;
            this.cmbSalesAccount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSalesAccount_KeyDown);
            // 
            // lblsalesAccount
            // 
            this.lblsalesAccount.AutoSize = true;
            this.lblsalesAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblsalesAccount.ForeColor = System.Drawing.Color.Black;
            this.lblsalesAccount.Location = new System.Drawing.Point(1134, 64);
            this.lblsalesAccount.Margin = new System.Windows.Forms.Padding(5);
            this.lblsalesAccount.Name = "lblsalesAccount";
            this.lblsalesAccount.Size = new System.Drawing.Size(63, 19);
            this.lblsalesAccount.TabIndex = 1127;
            this.lblsalesAccount.Text = "Sales A/c";
            this.lblsalesAccount.Visible = false;
            // 
            // btnNewCounter
            // 
            this.btnNewCounter.BackColor = System.Drawing.Color.Gray;
            this.btnNewCounter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNewCounter.FlatAppearance.BorderSize = 0;
            this.btnNewCounter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewCounter.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewCounter.ForeColor = System.Drawing.Color.Maroon;
            this.btnNewCounter.Location = new System.Drawing.Point(1062, 42);
            this.btnNewCounter.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewCounter.Name = "btnNewCounter";
            this.btnNewCounter.Size = new System.Drawing.Size(28, 25);
            this.btnNewCounter.TabIndex = 11;
            this.btnNewCounter.Text = "+";
            this.btnNewCounter.UseVisualStyleBackColor = false;
            this.btnNewCounter.Visible = false;
            this.btnNewCounter.Click += new System.EventHandler(this.btnNewCounter_Click);
            // 
            // cmbCounter
            // 
            this.cmbCounter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCounter.FormattingEnabled = true;
            this.cmbCounter.Location = new System.Drawing.Point(837, 42);
            this.cmbCounter.Margin = new System.Windows.Forms.Padding(7);
            this.cmbCounter.Name = "cmbCounter";
            this.cmbCounter.Size = new System.Drawing.Size(225, 27);
            this.cmbCounter.TabIndex = 10;
            this.cmbCounter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCounter_KeyDown);
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.BackColor = System.Drawing.Color.Transparent;
            this.lblCounter.ForeColor = System.Drawing.Color.Black;
            this.lblCounter.Location = new System.Drawing.Point(747, 46);
            this.lblCounter.Margin = new System.Windows.Forms.Padding(5);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(74, 19);
            this.lblCounter.TabIndex = 1130;
            this.lblCounter.Text = "Sales Point";
            // 
            // lnklblRemove
            // 
            this.lnklblRemove.ActiveLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.AutoSize = true;
            this.lnklblRemove.LinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.Location = new System.Drawing.Point(1147, 420);
            this.lnklblRemove.Name = "lnklblRemove";
            this.lnklblRemove.Size = new System.Drawing.Size(58, 19);
            this.lnklblRemove.TabIndex = 18;
            this.lnklblRemove.TabStop = true;
            this.lnklblRemove.Text = "Remove";
            this.lnklblRemove.VisitedLinkColor = System.Drawing.Color.Brown;
            this.lnklblRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblRemove_LinkClicked);
            // 
            // dgvPOSTax
            // 
            this.dgvPOSTax.AllowUserToAddRows = false;
            this.dgvPOSTax.AllowUserToDeleteRows = false;
            this.dgvPOSTax.AllowUserToResizeColumns = false;
            this.dgvPOSTax.AllowUserToResizeRows = false;
            this.dgvPOSTax.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPOSTax.BackgroundColor = System.Drawing.Color.White;
            this.dgvPOSTax.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPOSTax.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvPOSTax.ColumnHeadersHeight = 35;
            this.dgvPOSTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvPOSTax.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSINO,
            this.dgvtxtTaxLedgerId,
            this.dgvtxtTaxName,
            this.dgvtxtTaxAmt,
            this.dgvtxttaxrate,
            this.dgvtxttax,
            this.dgvtxtTaxApplicableOn,
            this.dgvtxttaxCalculateMode,
            this.Column2,
            this.temp});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPOSTax.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvPOSTax.EnableHeadersVisualStyles = false;
            this.dgvPOSTax.GridColor = System.Drawing.Color.DimGray;
            this.dgvPOSTax.Location = new System.Drawing.Point(8, 437);
            this.dgvPOSTax.Margin = new System.Windows.Forms.Padding(4, 4, 4, 15);
            this.dgvPOSTax.Name = "dgvPOSTax";
            this.dgvPOSTax.ReadOnly = true;
            this.dgvPOSTax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvPOSTax.RowHeadersVisible = false;
            this.dgvPOSTax.Size = new System.Drawing.Size(249, 80);
            this.dgvPOSTax.TabIndex = 13;
            this.dgvPOSTax.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPOSTax_RowsAdded);
            // 
            // dgvtxtSINO
            // 
            this.dgvtxtSINO.HeaderText = "Sl No";
            this.dgvtxtSINO.Name = "dgvtxtSINO";
            this.dgvtxtSINO.ReadOnly = true;
            this.dgvtxtSINO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTaxLedgerId
            // 
            this.dgvtxtTaxLedgerId.DataPropertyName = "ledgerId";
            this.dgvtxtTaxLedgerId.HeaderText = "ledgerId";
            this.dgvtxtTaxLedgerId.Name = "dgvtxtTaxLedgerId";
            this.dgvtxtTaxLedgerId.ReadOnly = true;
            this.dgvtxtTaxLedgerId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTaxLedgerId.Visible = false;
            // 
            // dgvtxtTaxName
            // 
            this.dgvtxtTaxName.DataPropertyName = "taxName";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvtxtTaxName.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvtxtTaxName.HeaderText = "Tax Name";
            this.dgvtxtTaxName.Name = "dgvtxtTaxName";
            this.dgvtxtTaxName.ReadOnly = true;
            this.dgvtxtTaxName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTaxAmt
            // 
            this.dgvtxtTaxAmt.DataPropertyName = "taxAmount";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtTaxAmt.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgvtxtTaxAmt.HeaderText = "Amount";
            this.dgvtxtTaxAmt.Name = "dgvtxtTaxAmt";
            this.dgvtxtTaxAmt.ReadOnly = true;
            this.dgvtxtTaxAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxttaxrate
            // 
            this.dgvtxttaxrate.DataPropertyName = "rate";
            this.dgvtxttaxrate.HeaderText = "rate";
            this.dgvtxttaxrate.Name = "dgvtxttaxrate";
            this.dgvtxttaxrate.ReadOnly = true;
            this.dgvtxttaxrate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxttaxrate.Visible = false;
            // 
            // dgvtxttax
            // 
            this.dgvtxttax.DataPropertyName = "taxId";
            this.dgvtxttax.HeaderText = "TaxId";
            this.dgvtxttax.Name = "dgvtxttax";
            this.dgvtxttax.ReadOnly = true;
            this.dgvtxttax.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxttax.Visible = false;
            // 
            // dgvtxtTaxApplicableOn
            // 
            this.dgvtxtTaxApplicableOn.DataPropertyName = "applicableOn";
            this.dgvtxtTaxApplicableOn.HeaderText = "ApplicableOn";
            this.dgvtxtTaxApplicableOn.Name = "dgvtxtTaxApplicableOn";
            this.dgvtxtTaxApplicableOn.ReadOnly = true;
            this.dgvtxtTaxApplicableOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtTaxApplicableOn.Visible = false;
            // 
            // dgvtxttaxCalculateMode
            // 
            this.dgvtxttaxCalculateMode.DataPropertyName = "calculatingMode";
            this.dgvtxttaxCalculateMode.HeaderText = "CalculatingMode";
            this.dgvtxttaxCalculateMode.Name = "dgvtxttaxCalculateMode";
            this.dgvtxttaxCalculateMode.ReadOnly = true;
            this.dgvtxttaxCalculateMode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxttaxCalculateMode.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "selectedtaxId";
            this.Column2.HeaderText = "selectedtaxId";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Visible = false;
            // 
            // temp
            // 
            this.temp.HeaderText = "temp";
            this.temp.Name = "temp";
            this.temp.ReadOnly = true;
            this.temp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.temp.Visible = false;
            // 
            // lblLedgerTotal
            // 
            this.lblLedgerTotal.AutoSize = true;
            this.lblLedgerTotal.ForeColor = System.Drawing.Color.Black;
            this.lblLedgerTotal.Location = new System.Drawing.Point(120, 525);
            this.lblLedgerTotal.Margin = new System.Windows.Forms.Padding(7);
            this.lblLedgerTotal.Name = "lblLedgerTotal";
            this.lblLedgerTotal.Size = new System.Drawing.Size(45, 19);
            this.lblLedgerTotal.TabIndex = 1166;
            this.lblLedgerTotal.Text = "Total :";
            // 
            // cbxPrintAfterSave
            // 
            this.cbxPrintAfterSave.AutoSize = true;
            this.cbxPrintAfterSave.BackColor = System.Drawing.Color.Transparent;
            this.cbxPrintAfterSave.ForeColor = System.Drawing.Color.Black;
            this.cbxPrintAfterSave.Location = new System.Drawing.Point(617, 538);
            this.cbxPrintAfterSave.Margin = new System.Windows.Forms.Padding(4);
            this.cbxPrintAfterSave.Name = "cbxPrintAfterSave";
            this.cbxPrintAfterSave.Size = new System.Drawing.Size(120, 23);
            this.cbxPrintAfterSave.TabIndex = 19;
            this.cbxPrintAfterSave.TabStop = false;
            this.cbxPrintAfterSave.Text = "Print after save";
            this.cbxPrintAfterSave.UseVisualStyleBackColor = false;
            this.cbxPrintAfterSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxPrintAfterSave_KeyDown);
            // 
            // lblTaxTotalAmount
            // 
            this.lblTaxTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxTotalAmount.ForeColor = System.Drawing.Color.Brown;
            this.lblTaxTotalAmount.Location = new System.Drawing.Point(177, 522);
            this.lblTaxTotalAmount.Margin = new System.Windows.Forms.Padding(7);
            this.lblTaxTotalAmount.Name = "lblTaxTotalAmount";
            this.lblTaxTotalAmount.Size = new System.Drawing.Size(80, 26);
            this.lblTaxTotalAmount.TabIndex = 1169;
            this.lblTaxTotalAmount.Text = "00.00";
            this.lblTaxTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(282, 457);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(197, 61);
            this.txtNarration.TabIndex = 14;
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(282, 437);
            this.label26.Margin = new System.Windows.Forms.Padding(5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(67, 19);
            this.label26.TabIndex = 1161;
            this.label26.Text = "Narration";
            // 
            // cmbPricingLevel
            // 
            this.cmbPricingLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPricingLevel.FormattingEnabled = true;
            this.cmbPricingLevel.Location = new System.Drawing.Point(618, 11);
            this.cmbPricingLevel.Margin = new System.Windows.Forms.Padding(5);
            this.cmbPricingLevel.Name = "cmbPricingLevel";
            this.cmbPricingLevel.Size = new System.Drawing.Size(122, 27);
            this.cmbPricingLevel.TabIndex = 2;
            this.cmbPricingLevel.SelectedIndexChanged += new System.EventHandler(this.cmbPricingLevel_SelectedIndexChanged);
            this.cmbPricingLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPricingLevel_KeyDown);
            // 
            // lblPricingLevel
            // 
            this.lblPricingLevel.AutoSize = true;
            this.lblPricingLevel.BackColor = System.Drawing.Color.Transparent;
            this.lblPricingLevel.ForeColor = System.Drawing.Color.Black;
            this.lblPricingLevel.Location = new System.Drawing.Point(532, 15);
            this.lblPricingLevel.Margin = new System.Windows.Forms.Padding(5);
            this.lblPricingLevel.Name = "lblPricingLevel";
            this.lblPricingLevel.Size = new System.Drawing.Size(85, 19);
            this.lblPricingLevel.TabIndex = 1171;
            this.lblPricingLevel.Text = "Pricing Level";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(501, 11);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(27, 26);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(348, 11);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(180, 26);
            this.txtDate.TabIndex = 1;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(518, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 19);
            this.label1.TabIndex = 1176;
            this.label1.Text = "*";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(1113, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 19);
            this.label2.TabIndex = 1177;
            this.label2.Text = "*";
            this.label2.Visible = false;
            // 
            // radCash
            // 
            this.radCash.AutoSize = true;
            this.radCash.Checked = true;
            this.radCash.Location = new System.Drawing.Point(106, 45);
            this.radCash.Name = "radCash";
            this.radCash.Size = new System.Drawing.Size(57, 23);
            this.radCash.TabIndex = 1178;
            this.radCash.TabStop = true;
            this.radCash.Text = "Cash";
            this.radCash.UseVisualStyleBackColor = true;
            this.radCash.CheckedChanged += new System.EventHandler(this.radCash_CheckedChanged);
            // 
            // radBank
            // 
            this.radBank.AutoSize = true;
            this.radBank.Location = new System.Drawing.Point(165, 45);
            this.radBank.Name = "radBank";
            this.radBank.Size = new System.Drawing.Size(57, 23);
            this.radBank.TabIndex = 1179;
            this.radBank.Text = "Bank";
            this.radBank.UseVisualStyleBackColor = true;
            this.radBank.CheckedChanged += new System.EventHandler(this.radBank_CheckedChanged);
            // 
            // gbxAmountDetails
            // 
            this.gbxAmountDetails.Controls.Add(this.txtCardAmount);
            this.gbxAmountDetails.Controls.Add(this.txtCardRefNo);
            this.gbxAmountDetails.Controls.Add(this.lblCardRefNo);
            this.gbxAmountDetails.Controls.Add(this.txtBalance);
            this.gbxAmountDetails.Controls.Add(this.lblBalance);
            this.gbxAmountDetails.Controls.Add(this.lblCardAmount);
            this.gbxAmountDetails.Controls.Add(this.groupBox2);
            this.gbxAmountDetails.Controls.Add(this.txtPaidAmount);
            this.gbxAmountDetails.Controls.Add(this.lblPaidAmount);
            this.gbxAmountDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAmountDetails.ForeColor = System.Drawing.Color.Maroon;
            this.gbxAmountDetails.Location = new System.Drawing.Point(738, 421);
            this.gbxAmountDetails.Name = "gbxAmountDetails";
            this.gbxAmountDetails.Size = new System.Drawing.Size(254, 115);
            this.gbxAmountDetails.TabIndex = 1181;
            this.gbxAmountDetails.TabStop = false;
            this.gbxAmountDetails.Text = "Details";
            // 
            // txtCardAmount
            // 
            this.txtCardAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardAmount.Location = new System.Drawing.Point(104, 42);
            this.txtCardAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCardAmount.MaxLength = 10;
            this.txtCardAmount.Name = "txtCardAmount";
            this.txtCardAmount.Size = new System.Drawing.Size(125, 20);
            this.txtCardAmount.TabIndex = 1186;
            this.txtCardAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCardRefNo
            // 
            this.txtCardRefNo.Location = new System.Drawing.Point(103, 90);
            this.txtCardRefNo.Name = "txtCardRefNo";
            this.txtCardRefNo.Size = new System.Drawing.Size(125, 21);
            this.txtCardRefNo.TabIndex = 1185;
            // 
            // lblCardRefNo
            // 
            this.lblCardRefNo.AutoSize = true;
            this.lblCardRefNo.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardRefNo.ForeColor = System.Drawing.Color.Black;
            this.lblCardRefNo.Location = new System.Drawing.Point(26, 94);
            this.lblCardRefNo.Margin = new System.Windows.Forms.Padding(7);
            this.lblCardRefNo.Name = "lblCardRefNo";
            this.lblCardRefNo.Size = new System.Drawing.Size(51, 13);
            this.lblCardRefNo.TabIndex = 1184;
            this.lblCardRefNo.Text = "Teller No";
            this.lblCardRefNo.Visible = false;
            // 
            // txtBalance
            // 
            this.txtBalance.BackColor = System.Drawing.Color.White;
            this.txtBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBalance.Location = new System.Drawing.Point(103, 67);
            this.txtBalance.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(125, 20);
            this.txtBalance.TabIndex = 100;
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.ForeColor = System.Drawing.Color.Black;
            this.lblBalance.Location = new System.Drawing.Point(26, 71);
            this.lblBalance.Margin = new System.Windows.Forms.Padding(5);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(46, 13);
            this.lblBalance.TabIndex = 1159;
            this.lblBalance.Text = "Balance";
            // 
            // lblCardAmount
            // 
            this.lblCardAmount.AutoSize = true;
            this.lblCardAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCardAmount.ForeColor = System.Drawing.Color.Black;
            this.lblCardAmount.Location = new System.Drawing.Point(26, 46);
            this.lblCardAmount.Margin = new System.Windows.Forms.Padding(5);
            this.lblCardAmount.Name = "lblCardAmount";
            this.lblCardAmount.Size = new System.Drawing.Size(68, 13);
            this.lblCardAmount.TabIndex = 1182;
            this.lblCardAmount.Text = "Card Amount";
            this.lblCardAmount.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Black;
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(10, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(70, 1);
            this.groupBox2.TabIndex = 1180;
            this.groupBox2.TabStop = false;
            // 
            // txtPaidAmount
            // 
            this.txtPaidAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaidAmount.Location = new System.Drawing.Point(103, 17);
            this.txtPaidAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtPaidAmount.MaxLength = 10;
            this.txtPaidAmount.Name = "txtPaidAmount";
            this.txtPaidAmount.Size = new System.Drawing.Size(125, 20);
            this.txtPaidAmount.TabIndex = 0;
            this.txtPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPaidAmount.TextChanged += new System.EventHandler(this.txtPaidAmount_TextChanged);
            this.txtPaidAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPaidAmount_KeyDown);
            this.txtPaidAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPaidAmount_KeyPress);
            this.txtPaidAmount.Leave += new System.EventHandler(this.txtPaidAmount_Leave);
            // 
            // lblPaidAmount
            // 
            this.lblPaidAmount.AutoSize = true;
            this.lblPaidAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaidAmount.ForeColor = System.Drawing.Color.Black;
            this.lblPaidAmount.Location = new System.Drawing.Point(26, 21);
            this.lblPaidAmount.Margin = new System.Windows.Forms.Padding(5);
            this.lblPaidAmount.Name = "lblPaidAmount";
            this.lblPaidAmount.Size = new System.Drawing.Size(67, 13);
            this.lblPaidAmount.TabIndex = 1157;
            this.lblPaidAmount.Text = "Paid Amount";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.BackColor = System.Drawing.Color.White;
            this.txtGrandTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtGrandTotal.Location = new System.Drawing.Point(1083, 512);
            this.txtGrandTotal.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(125, 20);
            this.txtGrandTotal.TabIndex = 1187;
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrandTotal.TextChanged += new System.EventHandler(this.txtGrandTotal_TextChanged);
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.AutoSize = true;
            this.lblGrandTotal.ForeColor = System.Drawing.Color.Black;
            this.lblGrandTotal.Location = new System.Drawing.Point(986, 513);
            this.lblGrandTotal.Margin = new System.Windows.Forms.Padding(5);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(80, 19);
            this.lblGrandTotal.TabIndex = 1186;
            this.lblGrandTotal.Text = "Grand Total";
            // 
            // txtBillDiscount
            // 
            this.txtBillDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtBillDiscount.Location = new System.Drawing.Point(1083, 487);
            this.txtBillDiscount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBillDiscount.MaxLength = 10;
            this.txtBillDiscount.Name = "txtBillDiscount";
            this.txtBillDiscount.Size = new System.Drawing.Size(125, 20);
            this.txtBillDiscount.TabIndex = 1182;
            this.txtBillDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillDiscount.TextChanged += new System.EventHandler(this.txtBillDiscount_TextChanged);
            this.txtBillDiscount.Enter += new System.EventHandler(this.txtBillDiscount_Enter);
            this.txtBillDiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBillDiscount_KeyDown);
            this.txtBillDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscountAmount_KeyPress);
            this.txtBillDiscount.Leave += new System.EventHandler(this.txtBillDiscount_Leave);
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.ForeColor = System.Drawing.Color.Black;
            this.lblDiscount.Location = new System.Drawing.Point(986, 489);
            this.lblDiscount.Margin = new System.Windows.Forms.Padding(5);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(93, 19);
            this.lblDiscount.TabIndex = 1185;
            this.lblDiscount.Text = "Discount Amt";
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.BackColor = System.Drawing.Color.White;
            this.txtTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtTotalAmount.Location = new System.Drawing.Point(1083, 439);
            this.txtTotalAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            this.txtTotalAmount.Size = new System.Drawing.Size(125, 20);
            this.txtTotalAmount.TabIndex = 1184;
            this.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(986, 442);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(5);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(38, 19);
            this.lblTotal.TabIndex = 1183;
            this.lblTotal.Text = "Total";
            // 
            // gbxDetails
            // 
            this.gbxDetails.Controls.Add(this.lblUnitConversion);
            this.gbxDetails.Controls.Add(this.lblUnitConversionRate);
            this.gbxDetails.Controls.Add(this.lblUnitConversionId);
            this.gbxDetails.Controls.Add(this.btnAdd);
            this.gbxDetails.Controls.Add(this.txtAmount);
            this.gbxDetails.Controls.Add(this.lblAmount);
            this.gbxDetails.Controls.Add(this.txtDiscountAmount);
            this.gbxDetails.Controls.Add(this.lblDiscountAmt);
            this.gbxDetails.Controls.Add(this.txtDiscountPercentage);
            this.gbxDetails.Controls.Add(this.lblDiscountPercentage);
            this.gbxDetails.Controls.Add(this.txtNetAmount);
            this.gbxDetails.Controls.Add(this.lblnetAmount);
            this.gbxDetails.Controls.Add(this.txtTaxAmount);
            this.gbxDetails.Controls.Add(this.lblTaxAmount);
            this.gbxDetails.Controls.Add(this.cmbTax);
            this.gbxDetails.Controls.Add(this.lblTax);
            this.gbxDetails.Controls.Add(this.txtGrossValue);
            this.gbxDetails.Controls.Add(this.lblGrossValue);
            this.gbxDetails.Controls.Add(this.txtRate);
            this.gbxDetails.Controls.Add(this.lblRate);
            this.gbxDetails.Controls.Add(this.cmbUnit);
            this.gbxDetails.Controls.Add(this.lblUnit);
            this.gbxDetails.Controls.Add(this.txtQuantity);
            this.gbxDetails.Controls.Add(this.lblQuantity);
            this.gbxDetails.Controls.Add(this.cmbBatch);
            this.gbxDetails.Controls.Add(this.lblBatch);
            this.gbxDetails.Controls.Add(this.cmbRack);
            this.gbxDetails.Controls.Add(this.lblRack);
            this.gbxDetails.Controls.Add(this.cmbGodown);
            this.gbxDetails.Controls.Add(this.lblGodown);
            this.gbxDetails.Controls.Add(this.cmbItem);
            this.gbxDetails.Controls.Add(this.lblItem);
            this.gbxDetails.Controls.Add(this.txtProductCode);
            this.gbxDetails.Controls.Add(this.lblProductcode);
            this.gbxDetails.Controls.Add(this.txtBarcode);
            this.gbxDetails.Controls.Add(this.lblBarcode);
            this.gbxDetails.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxDetails.ForeColor = System.Drawing.Color.Maroon;
            this.gbxDetails.Location = new System.Drawing.Point(6, 105);
            this.gbxDetails.Name = "gbxDetails";
            this.gbxDetails.Padding = new System.Windows.Forms.Padding(4);
            this.gbxDetails.Size = new System.Drawing.Size(1199, 156);
            this.gbxDetails.TabIndex = 1188;
            this.gbxDetails.TabStop = false;
            this.gbxDetails.Text = "Details";
            // 
            // lblUnitConversion
            // 
            this.lblUnitConversion.AutoSize = true;
            this.lblUnitConversion.Location = new System.Drawing.Point(473, 202);
            this.lblUnitConversion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitConversion.Name = "lblUnitConversion";
            this.lblUnitConversion.Size = new System.Drawing.Size(0, 19);
            this.lblUnitConversion.TabIndex = 17;
            this.lblUnitConversion.Visible = false;
            // 
            // lblUnitConversionRate
            // 
            this.lblUnitConversionRate.AutoSize = true;
            this.lblUnitConversionRate.Location = new System.Drawing.Point(405, 202);
            this.lblUnitConversionRate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitConversionRate.Name = "lblUnitConversionRate";
            this.lblUnitConversionRate.Size = new System.Drawing.Size(0, 19);
            this.lblUnitConversionRate.TabIndex = 16;
            this.lblUnitConversionRate.Visible = false;
            // 
            // lblUnitConversionId
            // 
            this.lblUnitConversionId.AutoSize = true;
            this.lblUnitConversionId.Location = new System.Drawing.Point(405, 202);
            this.lblUnitConversionId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitConversionId.Name = "lblUnitConversionId";
            this.lblUnitConversionId.Size = new System.Drawing.Size(0, 19);
            this.lblUnitConversionId.TabIndex = 1161;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.Black;
            this.btnAdd.Location = new System.Drawing.Point(1110, 122);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 27);
            this.btnAdd.TabIndex = 16;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.Color.White;
            this.txtAmount.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(708, 99);
            this.txtAmount.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(485, 22);
            this.txtAmount.TabIndex = 15;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblAmount.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.ForeColor = System.Drawing.Color.Black;
            this.lblAmount.Location = new System.Drawing.Point(606, 101);
            this.lblAmount.Margin = new System.Windows.Forms.Padding(7);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(59, 19);
            this.lblAmount.TabIndex = 1159;
            this.lblAmount.Text = "Amount";
            // 
            // txtDiscountAmount
            // 
            this.txtDiscountAmount.Enabled = false;
            this.txtDiscountAmount.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountAmount.ForeColor = System.Drawing.Color.Black;
            this.txtDiscountAmount.Location = new System.Drawing.Point(404, 122);
            this.txtDiscountAmount.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtDiscountAmount.MaxLength = 8;
            this.txtDiscountAmount.Name = "txtDiscountAmount";
            this.txtDiscountAmount.Size = new System.Drawing.Size(165, 22);
            this.txtDiscountAmount.TabIndex = 11;
            this.txtDiscountAmount.Text = "0";
            this.txtDiscountAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscountAmount.Visible = false;
            this.txtDiscountAmount.TextChanged += new System.EventHandler(this.txtDiscountAmount_TextChanged);
            this.txtDiscountAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscountAmount_KeyDown);
            this.txtDiscountAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscountAmount_KeyPress);
            this.txtDiscountAmount.Leave += new System.EventHandler(this.txtDiscountAmount_Leave);
            // 
            // lblDiscountAmt
            // 
            this.lblDiscountAmt.AutoSize = true;
            this.lblDiscountAmt.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscountAmt.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountAmt.ForeColor = System.Drawing.Color.Black;
            this.lblDiscountAmt.Location = new System.Drawing.Point(302, 126);
            this.lblDiscountAmt.Margin = new System.Windows.Forms.Padding(7);
            this.lblDiscountAmt.Name = "lblDiscountAmt";
            this.lblDiscountAmt.Size = new System.Drawing.Size(96, 19);
            this.lblDiscountAmt.TabIndex = 1157;
            this.lblDiscountAmt.Text = "Discount Amt.";
            this.lblDiscountAmt.Visible = false;
            // 
            // txtDiscountPercentage
            // 
            this.txtDiscountPercentage.Enabled = false;
            this.txtDiscountPercentage.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscountPercentage.ForeColor = System.Drawing.Color.Black;
            this.txtDiscountPercentage.Location = new System.Drawing.Point(116, 123);
            this.txtDiscountPercentage.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtDiscountPercentage.MaxLength = 6;
            this.txtDiscountPercentage.Name = "txtDiscountPercentage";
            this.txtDiscountPercentage.Size = new System.Drawing.Size(165, 22);
            this.txtDiscountPercentage.TabIndex = 10;
            this.txtDiscountPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscountPercentage.Visible = false;
            this.txtDiscountPercentage.TextChanged += new System.EventHandler(this.txtDiscountPercentage_TextChanged);
            this.txtDiscountPercentage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscountPercentage_KeyDown);
            this.txtDiscountPercentage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscountAmount_KeyPress);
            this.txtDiscountPercentage.Leave += new System.EventHandler(this.txtDiscountPercentage_Leave);
            // 
            // lblDiscountPercentage
            // 
            this.lblDiscountPercentage.AutoSize = true;
            this.lblDiscountPercentage.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscountPercentage.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountPercentage.ForeColor = System.Drawing.Color.Black;
            this.lblDiscountPercentage.Location = new System.Drawing.Point(14, 122);
            this.lblDiscountPercentage.Margin = new System.Windows.Forms.Padding(7);
            this.lblDiscountPercentage.Name = "lblDiscountPercentage";
            this.lblDiscountPercentage.Size = new System.Drawing.Size(78, 19);
            this.lblDiscountPercentage.TabIndex = 1155;
            this.lblDiscountPercentage.Text = "Discount %";
            this.lblDiscountPercentage.Visible = false;
            // 
            // txtNetAmount
            // 
            this.txtNetAmount.BackColor = System.Drawing.Color.White;
            this.txtNetAmount.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetAmount.ForeColor = System.Drawing.Color.Black;
            this.txtNetAmount.Location = new System.Drawing.Point(708, 75);
            this.txtNetAmount.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtNetAmount.Name = "txtNetAmount";
            this.txtNetAmount.ReadOnly = true;
            this.txtNetAmount.Size = new System.Drawing.Size(485, 22);
            this.txtNetAmount.TabIndex = 12;
            this.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNetAmount.TextChanged += new System.EventHandler(this.txtNetAmount_TextChanged);
            // 
            // lblnetAmount
            // 
            this.lblnetAmount.AutoSize = true;
            this.lblnetAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblnetAmount.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnetAmount.ForeColor = System.Drawing.Color.Black;
            this.lblnetAmount.Location = new System.Drawing.Point(606, 77);
            this.lblnetAmount.Margin = new System.Windows.Forms.Padding(7);
            this.lblnetAmount.Name = "lblnetAmount";
            this.lblnetAmount.Size = new System.Drawing.Size(85, 19);
            this.lblnetAmount.TabIndex = 1153;
            this.lblnetAmount.Text = "Net Amount";
            // 
            // txtTaxAmount
            // 
            this.txtTaxAmount.BackColor = System.Drawing.Color.White;
            this.txtTaxAmount.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxAmount.ForeColor = System.Drawing.Color.Black;
            this.txtTaxAmount.Location = new System.Drawing.Point(404, 95);
            this.txtTaxAmount.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtTaxAmount.Name = "txtTaxAmount";
            this.txtTaxAmount.ReadOnly = true;
            this.txtTaxAmount.Size = new System.Drawing.Size(165, 22);
            this.txtTaxAmount.TabIndex = 14;
            this.txtTaxAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaxAmount.Visible = false;
            // 
            // lblTaxAmount
            // 
            this.lblTaxAmount.AutoSize = true;
            this.lblTaxAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxAmount.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTaxAmount.ForeColor = System.Drawing.Color.Black;
            this.lblTaxAmount.Location = new System.Drawing.Point(302, 98);
            this.lblTaxAmount.Margin = new System.Windows.Forms.Padding(7);
            this.lblTaxAmount.Name = "lblTaxAmount";
            this.lblTaxAmount.Size = new System.Drawing.Size(81, 19);
            this.lblTaxAmount.TabIndex = 1151;
            this.lblTaxAmount.Text = "Tax Amount";
            this.lblTaxAmount.Visible = false;
            // 
            // cmbTax
            // 
            this.cmbTax.BackColor = System.Drawing.Color.White;
            this.cmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTax.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTax.ForeColor = System.Drawing.Color.Black;
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(117, 97);
            this.cmbTax.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(165, 21);
            this.cmbTax.TabIndex = 13;
            this.cmbTax.Visible = false;
            this.cmbTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTax_KeyDown);
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.BackColor = System.Drawing.Color.Transparent;
            this.lblTax.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTax.ForeColor = System.Drawing.Color.Black;
            this.lblTax.Location = new System.Drawing.Point(14, 100);
            this.lblTax.Margin = new System.Windows.Forms.Padding(7);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(27, 19);
            this.lblTax.TabIndex = 1149;
            this.lblTax.Text = "Tax";
            this.lblTax.Visible = false;
            // 
            // txtGrossValue
            // 
            this.txtGrossValue.BackColor = System.Drawing.Color.White;
            this.txtGrossValue.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrossValue.ForeColor = System.Drawing.Color.Black;
            this.txtGrossValue.Location = new System.Drawing.Point(1028, 52);
            this.txtGrossValue.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtGrossValue.Name = "txtGrossValue";
            this.txtGrossValue.ReadOnly = true;
            this.txtGrossValue.Size = new System.Drawing.Size(165, 22);
            this.txtGrossValue.TabIndex = 9;
            this.txtGrossValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGrossValue.TextChanged += new System.EventHandler(this.txtGrossValue_TextChanged);
            // 
            // lblGrossValue
            // 
            this.lblGrossValue.AutoSize = true;
            this.lblGrossValue.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrossValue.ForeColor = System.Drawing.Color.Black;
            this.lblGrossValue.Location = new System.Drawing.Point(926, 54);
            this.lblGrossValue.Margin = new System.Windows.Forms.Padding(7);
            this.lblGrossValue.Name = "lblGrossValue";
            this.lblGrossValue.Size = new System.Drawing.Size(81, 19);
            this.lblGrossValue.TabIndex = 1147;
            this.lblGrossValue.Text = "Gross Value";
            // 
            // txtRate
            // 
            this.txtRate.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRate.ForeColor = System.Drawing.Color.Black;
            this.txtRate.Location = new System.Drawing.Point(708, 52);
            this.txtRate.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtRate.MaxLength = 9;
            this.txtRate.Name = "txtRate";
            this.txtRate.Size = new System.Drawing.Size(165, 22);
            this.txtRate.TabIndex = 8;
            this.txtRate.Text = "0";
            this.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRate.TextChanged += new System.EventHandler(this.txtRate_TextChanged);
            this.txtRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRate_KeyDown);
            this.txtRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRate_KeyPress);
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.BackColor = System.Drawing.Color.Transparent;
            this.lblRate.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRate.ForeColor = System.Drawing.Color.Black;
            this.lblRate.Location = new System.Drawing.Point(606, 52);
            this.lblRate.Margin = new System.Windows.Forms.Padding(7);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(36, 19);
            this.lblRate.TabIndex = 1145;
            this.lblRate.Text = "Rate";
            // 
            // cmbUnit
            // 
            this.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnit.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnit.ForeColor = System.Drawing.Color.Black;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Location = new System.Drawing.Point(404, 67);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(165, 21);
            this.cmbUnit.TabIndex = 7;
            this.cmbUnit.SelectedValueChanged += new System.EventHandler(this.cmbUnit_SelectedValueChanged);
            this.cmbUnit.Enter += new System.EventHandler(this.cmbUnit_Enter);
            this.cmbUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUnit_KeyDown);
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblUnit.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnit.ForeColor = System.Drawing.Color.Black;
            this.lblUnit.Location = new System.Drawing.Point(302, 70);
            this.lblUnit.Margin = new System.Windows.Forms.Padding(7);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(35, 19);
            this.lblUnit.TabIndex = 1143;
            this.lblUnit.Text = "Unit";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantity.ForeColor = System.Drawing.Color.Black;
            this.txtQuantity.Location = new System.Drawing.Point(117, 67);
            this.txtQuantity.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtQuantity.MaxLength = 9;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(165, 22);
            this.txtQuantity.TabIndex = 6;
            this.txtQuantity.Text = "0";
            this.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            this.txtQuantity.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQuantity_KeyDown);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblQuantity.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantity.ForeColor = System.Drawing.Color.Black;
            this.lblQuantity.Location = new System.Drawing.Point(14, 73);
            this.lblQuantity.Margin = new System.Windows.Forms.Padding(7);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(35, 19);
            this.lblQuantity.TabIndex = 1141;
            this.lblQuantity.Text = "Qty.";
            // 
            // cmbBatch
            // 
            this.cmbBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBatch.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBatch.ForeColor = System.Drawing.Color.Black;
            this.cmbBatch.FormattingEnabled = true;
            this.cmbBatch.Location = new System.Drawing.Point(708, 28);
            this.cmbBatch.Margin = new System.Windows.Forms.Padding(7);
            this.cmbBatch.Name = "cmbBatch";
            this.cmbBatch.Size = new System.Drawing.Size(485, 21);
            this.cmbBatch.TabIndex = 5;
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.BackColor = System.Drawing.Color.Transparent;
            this.lblBatch.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatch.ForeColor = System.Drawing.Color.Black;
            this.lblBatch.Location = new System.Drawing.Point(606, 29);
            this.lblBatch.Margin = new System.Windows.Forms.Padding(7);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(43, 19);
            this.lblBatch.TabIndex = 1139;
            this.lblBatch.Text = "Batch";
            // 
            // cmbRack
            // 
            this.cmbRack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRack.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRack.ForeColor = System.Drawing.Color.Black;
            this.cmbRack.FormattingEnabled = true;
            this.cmbRack.Location = new System.Drawing.Point(404, 42);
            this.cmbRack.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.cmbRack.Name = "cmbRack";
            this.cmbRack.Size = new System.Drawing.Size(165, 21);
            this.cmbRack.TabIndex = 4;
            this.cmbRack.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRack_KeyDown);
            // 
            // lblRack
            // 
            this.lblRack.AutoSize = true;
            this.lblRack.BackColor = System.Drawing.Color.Transparent;
            this.lblRack.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRack.ForeColor = System.Drawing.Color.Black;
            this.lblRack.Location = new System.Drawing.Point(302, 43);
            this.lblRack.Margin = new System.Windows.Forms.Padding(7);
            this.lblRack.Name = "lblRack";
            this.lblRack.Size = new System.Drawing.Size(37, 19);
            this.lblRack.TabIndex = 1137;
            this.lblRack.Text = "Rack";
            // 
            // cmbGodown
            // 
            this.cmbGodown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGodown.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGodown.ForeColor = System.Drawing.Color.Black;
            this.cmbGodown.FormattingEnabled = true;
            this.cmbGodown.Location = new System.Drawing.Point(117, 42);
            this.cmbGodown.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.cmbGodown.Name = "cmbGodown";
            this.cmbGodown.Size = new System.Drawing.Size(165, 21);
            this.cmbGodown.TabIndex = 3;
            this.cmbGodown.SelectedIndexChanged += new System.EventHandler(this.cmbGodown_SelectedIndexChanged);
            this.cmbGodown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGodown_KeyDown);
            // 
            // lblGodown
            // 
            this.lblGodown.AutoSize = true;
            this.lblGodown.BackColor = System.Drawing.Color.Transparent;
            this.lblGodown.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGodown.ForeColor = System.Drawing.Color.Black;
            this.lblGodown.Location = new System.Drawing.Point(14, 48);
            this.lblGodown.Margin = new System.Windows.Forms.Padding(7);
            this.lblGodown.Name = "lblGodown";
            this.lblGodown.Size = new System.Drawing.Size(41, 19);
            this.lblGodown.TabIndex = 1135;
            this.lblGodown.Text = "Store";
            // 
            // cmbItem
            // 
            this.cmbItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbItem.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbItem.ForeColor = System.Drawing.Color.Black;
            this.cmbItem.FormattingEnabled = true;
            this.cmbItem.Location = new System.Drawing.Point(708, 4);
            this.cmbItem.Margin = new System.Windows.Forms.Padding(7);
            this.cmbItem.Name = "cmbItem";
            this.cmbItem.Size = new System.Drawing.Size(485, 21);
            this.cmbItem.TabIndex = 2;
            this.cmbItem.SelectedIndexChanged += new System.EventHandler(this.cmbItem_SelectedIndexChanged);
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            this.lblItem.BackColor = System.Drawing.Color.Transparent;
            this.lblItem.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItem.ForeColor = System.Drawing.Color.Black;
            this.lblItem.Location = new System.Drawing.Point(606, 5);
            this.lblItem.Margin = new System.Windows.Forms.Padding(7);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(37, 19);
            this.lblItem.TabIndex = 1125;
            this.lblItem.Text = "Item";
            // 
            // txtProductCode
            // 
            this.txtProductCode.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductCode.ForeColor = System.Drawing.Color.Black;
            this.txtProductCode.Location = new System.Drawing.Point(404, 17);
            this.txtProductCode.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtProductCode.Name = "txtProductCode";
            this.txtProductCode.Size = new System.Drawing.Size(165, 22);
            this.txtProductCode.TabIndex = 1;
            this.txtProductCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProductCode_KeyDown);
            this.txtProductCode.Leave += new System.EventHandler(this.txtProductCode_Leave);
            // 
            // lblProductcode
            // 
            this.lblProductcode.AutoSize = true;
            this.lblProductcode.BackColor = System.Drawing.Color.Transparent;
            this.lblProductcode.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductcode.ForeColor = System.Drawing.Color.Black;
            this.lblProductcode.Location = new System.Drawing.Point(302, 19);
            this.lblProductcode.Margin = new System.Windows.Forms.Padding(7);
            this.lblProductcode.Name = "lblProductcode";
            this.lblProductcode.Size = new System.Drawing.Size(93, 19);
            this.lblProductcode.TabIndex = 1123;
            this.lblProductcode.Text = "Product Code";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode.ForeColor = System.Drawing.Color.Black;
            this.txtBarcode.Location = new System.Drawing.Point(117, 17);
            this.txtBarcode.Margin = new System.Windows.Forms.Padding(7, 7, 7, 0);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(165, 22);
            this.txtBarcode.TabIndex = 0;
            this.txtBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarcode_KeyDown);
            this.txtBarcode.Leave += new System.EventHandler(this.txtBarcode_Leave);
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblBarcode.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarcode.ForeColor = System.Drawing.Color.Black;
            this.lblBarcode.Location = new System.Drawing.Point(14, 23);
            this.lblBarcode.Margin = new System.Windows.Forms.Padding(7);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(58, 19);
            this.lblBarcode.TabIndex = 1121;
            this.lblBarcode.Text = "Barcode";
            // 
            // radBoth
            // 
            this.radBoth.AutoSize = true;
            this.radBoth.Location = new System.Drawing.Point(304, 45);
            this.radBoth.Name = "radBoth";
            this.radBoth.Size = new System.Drawing.Size(134, 23);
            this.radBoth.TabIndex = 1189;
            this.radBoth.TabStop = true;
            this.radBoth.Text = "Both (cash / card)";
            this.radBoth.UseVisualStyleBackColor = true;
            this.radBoth.CheckedChanged += new System.EventHandler(this.radBoth_CheckedChanged);
            // 
            // radCustomer
            // 
            this.radCustomer.AutoSize = true;
            this.radCustomer.Location = new System.Drawing.Point(444, 45);
            this.radCustomer.Name = "radCustomer";
            this.radCustomer.Size = new System.Drawing.Size(87, 23);
            this.radCustomer.TabIndex = 1180;
            this.radCustomer.Text = "Customer";
            this.radCustomer.UseVisualStyleBackColor = true;
            this.radCustomer.Visible = false;
            this.radCustomer.CheckedChanged += new System.EventHandler(this.radCustomer_CheckedChanged);
            // 
            // cmbCashAndBank
            // 
            this.cmbCashAndBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashAndBank.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCashAndBank.FormattingEnabled = true;
            this.cmbCashAndBank.Location = new System.Drawing.Point(304, 71);
            this.cmbCashAndBank.Margin = new System.Windows.Forms.Padding(5);
            this.cmbCashAndBank.Name = "cmbCashAndBank";
            this.cmbCashAndBank.Size = new System.Drawing.Size(181, 25);
            this.cmbCashAndBank.TabIndex = 1177;
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.BackColor = System.Drawing.Color.Red;
            this.lblEmployeeName.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeName.ForeColor = System.Drawing.Color.White;
            this.lblEmployeeName.Location = new System.Drawing.Point(834, 11);
            this.lblEmployeeName.Margin = new System.Windows.Forms.Padding(7);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(28, 18);
            this.lblEmployeeName.TabIndex = 1190;
            this.lblEmployeeName.Text = "-----";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.BackColor = System.Drawing.Color.Transparent;
            this.lblUserId.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.ForeColor = System.Drawing.Color.Black;
            this.lblUserId.Location = new System.Drawing.Point(1027, 14);
            this.lblUserId.Margin = new System.Windows.Forms.Padding(7);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(37, 19);
            this.lblUserId.TabIndex = 1191;
            this.lblUserId.Text = "Item";
            this.lblUserId.Visible = false;
            // 
            // lblAvailableQuantity
            // 
            this.lblAvailableQuantity.AutoSize = true;
            this.lblAvailableQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblAvailableQuantity.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvailableQuantity.ForeColor = System.Drawing.Color.Black;
            this.lblAvailableQuantity.Location = new System.Drawing.Point(1045, 16);
            this.lblAvailableQuantity.Margin = new System.Windows.Forms.Padding(5);
            this.lblAvailableQuantity.Name = "lblAvailableQuantity";
            this.lblAvailableQuantity.Size = new System.Drawing.Size(0, 17);
            this.lblAvailableQuantity.TabIndex = 1192;
            // 
            // cmbDefaultPrinter
            // 
            this.cmbDefaultPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultPrinter.FormattingEnabled = true;
            this.cmbDefaultPrinter.Location = new System.Drawing.Point(837, 76);
            this.cmbDefaultPrinter.Margin = new System.Windows.Forms.Padding(7);
            this.cmbDefaultPrinter.Name = "cmbDefaultPrinter";
            this.cmbDefaultPrinter.Size = new System.Drawing.Size(225, 27);
            this.cmbDefaultPrinter.TabIndex = 1193;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(710, 80);
            this.label3.Margin = new System.Windows.Forms.Padding(5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 19);
            this.label3.TabIndex = 1194;
            this.label3.Text = "Set Default Printer";
            // 
            // radCheque
            // 
            this.radCheque.AutoSize = true;
            this.radCheque.Location = new System.Drawing.Point(228, 44);
            this.radCheque.Name = "radCheque";
            this.radCheque.Size = new System.Drawing.Size(74, 23);
            this.radCheque.TabIndex = 1195;
            this.radCheque.Text = "Cheque";
            this.radCheque.UseVisualStyleBackColor = true;
            this.radCheque.CheckedChanged += new System.EventHandler(this.radCheque_CheckedChanged);
            // 
            // lblDiscountPercent
            // 
            this.lblDiscountPercent.AutoSize = true;
            this.lblDiscountPercent.Location = new System.Drawing.Point(986, 467);
            this.lblDiscountPercent.Name = "lblDiscountPercent";
            this.lblDiscountPercent.Size = new System.Drawing.Size(78, 19);
            this.lblDiscountPercent.TabIndex = 1196;
            this.lblDiscountPercent.Text = "Discount %";
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtDiscountPercent.Location = new System.Drawing.Point(1083, 463);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(125, 20);
            this.txtDiscountPercent.TabIndex = 1197;
            this.txtDiscountPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscountPercent.TextChanged += new System.EventHandler(this.txtDiscountPercent_TextChanged);
            this.txtDiscountPercent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscountPercent_KeyDown);
            this.txtDiscountPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiscountPercent_KeyPress);
            this.txtDiscountPercent.Leave += new System.EventHandler(this.txtDiscountPercent_Leave);
            // 
            // btnHold
            // 
            this.btnHold.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnHold.FlatAppearance.BorderSize = 0;
            this.btnHold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHold.ForeColor = System.Drawing.Color.Black;
            this.btnHold.Location = new System.Drawing.Point(348, 538);
            this.btnHold.Name = "btnHold";
            this.btnHold.Size = new System.Drawing.Size(85, 27);
            this.btnHold.TabIndex = 1198;
            this.btnHold.Text = "Hold";
            this.btnHold.UseVisualStyleBackColor = false;
            this.btnHold.Click += new System.EventHandler(this.btnHold_Click);
            // 
            // dgvtxtSlNo
            // 
            this.dgvtxtSlNo.FillWeight = 60.9137F;
            this.dgvtxtSlNo.HeaderText = "Sl No";
            this.dgvtxtSlNo.Name = "dgvtxtSlNo";
            this.dgvtxtSlNo.ReadOnly = true;
            this.dgvtxtSlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rowId
            // 
            this.rowId.HeaderText = "rowId";
            this.rowId.Name = "rowId";
            this.rowId.ReadOnly = true;
            this.rowId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.rowId.Visible = false;
            // 
            // dgvtxtProductId
            // 
            this.dgvtxtProductId.DataPropertyName = "productId";
            this.dgvtxtProductId.HeaderText = "ProductId";
            this.dgvtxtProductId.Name = "dgvtxtProductId";
            this.dgvtxtProductId.ReadOnly = true;
            this.dgvtxtProductId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtProductId.Visible = false;
            // 
            // dgvtxtProductCode
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dgvtxtProductCode.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtProductCode.FillWeight = 103.5533F;
            this.dgvtxtProductCode.HeaderText = "Product Code";
            this.dgvtxtProductCode.Name = "dgvtxtProductCode";
            this.dgvtxtProductCode.ReadOnly = true;
            this.dgvtxtProductCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtProductName
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dgvtxtProductName.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtProductName.FillWeight = 103.5533F;
            this.dgvtxtProductName.HeaderText = "Product";
            this.dgvtxtProductName.Name = "dgvtxtProductName";
            this.dgvtxtProductName.ReadOnly = true;
            this.dgvtxtProductName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtQuantity
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtQuantity.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvtxtQuantity.FillWeight = 103.5533F;
            this.dgvtxtQuantity.HeaderText = "Qty.";
            this.dgvtxtQuantity.MinimumWidth = 8;
            this.dgvtxtQuantity.Name = "dgvtxtQuantity";
            this.dgvtxtQuantity.ReadOnly = true;
            this.dgvtxtQuantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtUnit
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dgvtxtUnit.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvtxtUnit.FillWeight = 103.5533F;
            this.dgvtxtUnit.HeaderText = "Unit";
            this.dgvtxtUnit.Name = "dgvtxtUnit";
            this.dgvtxtUnit.ReadOnly = true;
            this.dgvtxtUnit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtRate
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtRate.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvtxtRate.FillWeight = 103.5533F;
            this.dgvtxtRate.HeaderText = "Rate";
            this.dgvtxtRate.Name = "dgvtxtRate";
            this.dgvtxtRate.ReadOnly = true;
            this.dgvtxtRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtGrossValue
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtGrossValue.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvtxtGrossValue.FillWeight = 103.5533F;
            this.dgvtxtGrossValue.HeaderText = "Gross Value";
            this.dgvtxtGrossValue.Name = "dgvtxtGrossValue";
            this.dgvtxtGrossValue.ReadOnly = true;
            this.dgvtxtGrossValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtDiscount
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtDiscount.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvtxtDiscount.FillWeight = 103.5533F;
            this.dgvtxtDiscount.HeaderText = "Discount";
            this.dgvtxtDiscount.Name = "dgvtxtDiscount";
            this.dgvtxtDiscount.ReadOnly = true;
            this.dgvtxtDiscount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtNetAmount
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtNetAmount.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvtxtNetAmount.FillWeight = 103.5533F;
            this.dgvtxtNetAmount.HeaderText = "NetAmount";
            this.dgvtxtNetAmount.Name = "dgvtxtNetAmount";
            this.dgvtxtNetAmount.ReadOnly = true;
            this.dgvtxtNetAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTaxPercentage
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.dgvtxtTaxPercentage.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvtxtTaxPercentage.FillWeight = 103.5533F;
            this.dgvtxtTaxPercentage.HeaderText = "Tax";
            this.dgvtxtTaxPercentage.Name = "dgvtxtTaxPercentage";
            this.dgvtxtTaxPercentage.ReadOnly = true;
            this.dgvtxtTaxPercentage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvtxtTaxPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxttaxid
            // 
            this.dgvtxttaxid.HeaderText = "Tax";
            this.dgvtxttaxid.Name = "dgvtxttaxid";
            this.dgvtxttaxid.ReadOnly = true;
            this.dgvtxttaxid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxttaxid.Visible = false;
            // 
            // dgvtxtTaxAmount
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtTaxAmount.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvtxtTaxAmount.FillWeight = 103.5533F;
            this.dgvtxtTaxAmount.HeaderText = "Tax Amount";
            this.dgvtxtTaxAmount.Name = "dgvtxtTaxAmount";
            this.dgvtxtTaxAmount.ReadOnly = true;
            this.dgvtxtTaxAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtTotalAmount
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtTotalAmount.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvtxtTotalAmount.FillWeight = 103.5533F;
            this.dgvtxtTotalAmount.HeaderText = "Amount";
            this.dgvtxtTotalAmount.Name = "dgvtxtTotalAmount";
            this.dgvtxtTotalAmount.ReadOnly = true;
            this.dgvtxtTotalAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtBatchId
            // 
            this.dgvtxtBatchId.DataPropertyName = "batchId";
            this.dgvtxtBatchId.HeaderText = "batchId";
            this.dgvtxtBatchId.Name = "dgvtxtBatchId";
            this.dgvtxtBatchId.ReadOnly = true;
            this.dgvtxtBatchId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtBatchId.Visible = false;
            // 
            // dgvtxtBatchno
            // 
            this.dgvtxtBatchno.DataPropertyName = "batchNo";
            this.dgvtxtBatchno.HeaderText = "Batch";
            this.dgvtxtBatchno.Name = "dgvtxtBatchno";
            this.dgvtxtBatchno.ReadOnly = true;
            this.dgvtxtBatchno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtBatchno.Visible = false;
            // 
            // dgvtxtRackId
            // 
            this.dgvtxtRackId.DataPropertyName = "rackId";
            this.dgvtxtRackId.HeaderText = "rackId";
            this.dgvtxtRackId.Name = "dgvtxtRackId";
            this.dgvtxtRackId.ReadOnly = true;
            this.dgvtxtRackId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtRackId.Visible = false;
            // 
            // dgvtxtRackName
            // 
            this.dgvtxtRackName.DataPropertyName = "rackName";
            this.dgvtxtRackName.HeaderText = "Rack";
            this.dgvtxtRackName.Name = "dgvtxtRackName";
            this.dgvtxtRackName.ReadOnly = true;
            this.dgvtxtRackName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtRackName.Visible = false;
            // 
            // dgvtxtGodownId
            // 
            this.dgvtxtGodownId.DataPropertyName = "godownId";
            this.dgvtxtGodownId.HeaderText = "godownId";
            this.dgvtxtGodownId.Name = "dgvtxtGodownId";
            this.dgvtxtGodownId.ReadOnly = true;
            this.dgvtxtGodownId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGodownId.Visible = false;
            // 
            // dgvtxtGodownName
            // 
            this.dgvtxtGodownName.DataPropertyName = "godownName";
            this.dgvtxtGodownName.HeaderText = "Godown";
            this.dgvtxtGodownName.Name = "dgvtxtGodownName";
            this.dgvtxtGodownName.ReadOnly = true;
            this.dgvtxtGodownName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGodownName.Visible = false;
            // 
            // dgvtxtUnitId
            // 
            this.dgvtxtUnitId.DataPropertyName = "unitId";
            this.dgvtxtUnitId.HeaderText = "UnitId";
            this.dgvtxtUnitId.Name = "dgvtxtUnitId";
            this.dgvtxtUnitId.ReadOnly = true;
            this.dgvtxtUnitId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtUnitId.Visible = false;
            // 
            // dgvtxtunitconversionId
            // 
            this.dgvtxtunitconversionId.DataPropertyName = "unitconversionId";
            this.dgvtxtunitconversionId.HeaderText = "unitconversionId";
            this.dgvtxtunitconversionId.Name = "dgvtxtunitconversionId";
            this.dgvtxtunitconversionId.ReadOnly = true;
            this.dgvtxtunitconversionId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtunitconversionId.Visible = false;
            // 
            // dgvtxtBarcode
            // 
            this.dgvtxtBarcode.HeaderText = "Barcode";
            this.dgvtxtBarcode.Name = "dgvtxtBarcode";
            this.dgvtxtBarcode.ReadOnly = true;
            this.dgvtxtBarcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtBarcode.Visible = false;
            // 
            // dgvtxtDiscountPercentage
            // 
            this.dgvtxtDiscountPercentage.HeaderText = "DiscountPercentage";
            this.dgvtxtDiscountPercentage.Name = "dgvtxtDiscountPercentage";
            this.dgvtxtDiscountPercentage.ReadOnly = true;
            this.dgvtxtDiscountPercentage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtDiscountPercentage.Visible = false;
            // 
            // dgvtxtSalesDetailsId
            // 
            this.dgvtxtSalesDetailsId.HeaderText = "salesDetailsId";
            this.dgvtxtSalesDetailsId.Name = "dgvtxtSalesDetailsId";
            this.dgvtxtSalesDetailsId.ReadOnly = true;
            this.dgvtxtSalesDetailsId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtSalesDetailsId.Visible = false;
            // 
            // dgvtxtProjectId
            // 
            this.dgvtxtProjectId.HeaderText = "Project";
            this.dgvtxtProjectId.Name = "dgvtxtProjectId";
            this.dgvtxtProjectId.ReadOnly = true;
            this.dgvtxtProjectId.Visible = false;
            // 
            // dgvtxtCategoryId
            // 
            this.dgvtxtCategoryId.HeaderText = "Category";
            this.dgvtxtCategoryId.Name = "dgvtxtCategoryId";
            this.dgvtxtCategoryId.ReadOnly = true;
            this.dgvtxtCategoryId.Visible = false;
            // 
            // dgvtxtSalesAccount
            // 
            this.dgvtxtSalesAccount.HeaderText = "Sales Account";
            this.dgvtxtSalesAccount.Name = "dgvtxtSalesAccount";
            this.dgvtxtSalesAccount.ReadOnly = true;
            this.dgvtxtSalesAccount.Visible = false;
            // 
            // frmPOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1256, 573);
            this.Controls.Add(this.btnHold);
            this.Controls.Add(this.txtDiscountPercent);
            this.Controls.Add(this.lblDiscountPercent);
            this.Controls.Add(this.radCheque);
            this.Controls.Add(this.cmbDefaultPrinter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblAvailableQuantity);
            this.Controls.Add(this.lblUserId);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.cmbCashAndBank);
            this.Controls.Add(this.radBoth);
            this.Controls.Add(this.gbxDetails);
            this.Controls.Add(this.txtGrandTotal);
            this.Controls.Add(this.lblGrandTotal);
            this.Controls.Add(this.txtBillDiscount);
            this.Controls.Add(this.lblDiscount);
            this.Controls.Add(this.txtTotalAmount);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.gbxAmountDetails);
            this.Controls.Add(this.radCustomer);
            this.Controls.Add(this.radBank);
            this.Controls.Add(this.radCash);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.cmbPricingLevel);
            this.Controls.Add(this.lblPricingLevel);
            this.Controls.Add(this.txtNarration);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.lblTaxTotalAmount);
            this.Controls.Add(this.cbxPrintAfterSave);
            this.Controls.Add(this.lblLedgerTotal);
            this.Controls.Add(this.dgvPOSTax);
            this.Controls.Add(this.lnklblRemove);
            this.Controls.Add(this.btnNewCounter);
            this.Controls.Add(this.cmbCounter);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.cmbSalesMan);
            this.Controls.Add(this.lblSalesMan);
            this.Controls.Add(this.btnNewSalesAccount);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.cmbSalesAccount);
            this.Controls.Add(this.lblsalesAccount);
            this.Controls.Add(this.btnNewSalesMan);
            this.Controls.Add(this.btnNewLedger);
            this.Controls.Add(this.txtVoucherNo);
            this.Controls.Add(this.lblVoucherNo);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cmbCashOrParty);
            this.Controls.Add(this.dgvPointOfSales);
            this.Controls.Add(this.lblCashOrParty);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClear);
            this.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmPOS";
            this.Padding = new System.Windows.Forms.Padding(20, 15, 20, 15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "POS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPOS_FormClosing);
            this.Load += new System.EventHandler(this.frmPOS_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPOS_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPointOfSales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPOSTax)).EndInit();
            this.gbxAmountDetails.ResumeLayout(false);
            this.gbxAmountDetails.PerformLayout();
            this.gbxDetails.ResumeLayout(false);
            this.gbxDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNewSalesMan;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbCashOrParty;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblCashOrParty;
        private System.Windows.Forms.DataGridView dgvPointOfSales;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cmbSalesMan;
        private System.Windows.Forms.Label lblSalesMan;
        private System.Windows.Forms.Button btnNewLedger;
        private System.Windows.Forms.Button btnNewSalesAccount;
        private System.Windows.Forms.ComboBox cmbSalesAccount;
        private System.Windows.Forms.Label lblsalesAccount;
        private System.Windows.Forms.Button btnNewCounter;
        private System.Windows.Forms.ComboBox cmbCounter;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.LinkLabel lnklblRemove;
        private System.Windows.Forms.DataGridView dgvPOSTax;
        private System.Windows.Forms.Label lblLedgerTotal;
        private System.Windows.Forms.CheckBox cbxPrintAfterSave;
        private System.Windows.Forms.Label lblTaxTotalAmount;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbPricingLevel;
        private System.Windows.Forms.Label lblPricingLevel;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSINO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxLedgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxttaxrate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxttax;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxApplicableOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxttaxCalculateMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn temp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radCash;
        private System.Windows.Forms.RadioButton radBank;
        private System.Windows.Forms.GroupBox gbxAmountDetails;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.TextBox txtPaidAmount;
        private System.Windows.Forms.Label lblPaidAmount;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.TextBox txtBillDiscount;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.GroupBox gbxDetails;
        private System.Windows.Forms.Label lblUnitConversion;
        private System.Windows.Forms.Label lblUnitConversionRate;
        private System.Windows.Forms.Label lblUnitConversionId;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.TextBox txtDiscountAmount;
        private System.Windows.Forms.Label lblDiscountAmt;
        private System.Windows.Forms.TextBox txtDiscountPercentage;
        private System.Windows.Forms.Label lblDiscountPercentage;
        private System.Windows.Forms.TextBox txtNetAmount;
        private System.Windows.Forms.Label lblnetAmount;
        private System.Windows.Forms.TextBox txtTaxAmount;
        private System.Windows.Forms.Label lblTaxAmount;
        private System.Windows.Forms.ComboBox cmbTax;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.TextBox txtGrossValue;
        private System.Windows.Forms.Label lblGrossValue;
        private System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.ComboBox cmbBatch;
        private System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.ComboBox cmbRack;
        private System.Windows.Forms.Label lblRack;
        private System.Windows.Forms.ComboBox cmbGodown;
        private System.Windows.Forms.Label lblGodown;
        private System.Windows.Forms.ComboBox cmbItem;
        private System.Windows.Forms.Label lblItem;
        private System.Windows.Forms.TextBox txtProductCode;
        private System.Windows.Forms.Label lblProductcode;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.RadioButton radBoth;
        private System.Windows.Forms.RadioButton radCustomer;
        private System.Windows.Forms.ComboBox cmbCashAndBank;
        private System.Windows.Forms.Label lblCardAmount;
        private System.Windows.Forms.Label lblCardRefNo;
        private System.Windows.Forms.TextBox txtCardRefNo;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblUserId;  
        private System.Windows.Forms.Label lblAvailableQuantity;
        private System.Windows.Forms.TextBox txtCardAmount;
        private System.Windows.Forms.ComboBox cmbDefaultPrinter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radCheque;
        private System.Windows.Forms.Label lblDiscountPercent;
        private System.Windows.Forms.TextBox txtDiscountPercent;
        private System.Windows.Forms.Button btnHold;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGrossValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtDiscount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtNetAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxttaxid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTaxAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtTotalAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtBatchId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtBatchno;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtRackId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtRackName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGodownId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGodownName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtUnitId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtunitconversionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtDiscountPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesDetailsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProjectId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtCategoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSalesAccount;
    }
}