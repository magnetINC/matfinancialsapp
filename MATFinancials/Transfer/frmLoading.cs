﻿ 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MATFinancials
{
    public partial class frmLoading : Form
    {
        #region Function
        public frmLoading()
        {
            InitializeComponent();
        }
        public void ShowFromSendMail()
        {
            try
            {
                label1.Text = "Sending...";
                ShowDialog();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "Loading:1" + ex.Message;
            }
        }
        #endregion
        #region Events
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                lblTableName.Text = frmCopyData.strTable;
            }
            
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "Loading:2" + ex.Message;
            }
        }
        #endregion
    }
}
