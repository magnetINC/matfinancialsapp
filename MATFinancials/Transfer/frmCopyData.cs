﻿ 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MATFinancials
{
    public partial class frmCopyData : Form
    {
        #region Public Variables
        /// <summary>
        /// Public variable declaration part
        /// </summary>
        public static string strTable = string.Empty;
        string strSourceDbPath = string.Empty;
        string strDestinationDb = string.Empty;
        string strFailed = string.Empty;
        frmLoading f1;
        #endregion
        #region Functions
        /// <summary>
        /// Create instance of frmJournalVoucher
        /// </summary>
        public frmCopyData()
        {
            InitializeComponent();
        }
        #endregion
        #region Events
        /// <summary>
        /// On 'Source' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSource_Click(object sender, EventArgs e)
        {
            try
            {
            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                txtSourcePath.Text = openFileDialog1.FileName;
            }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CData:1" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Destination' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDestination_Click(object sender, EventArgs e)
        {
            try
            {
            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                txtDestinationPath.Text = openFileDialog1.FileName;
                
            }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CData:2" + ex.Message;
            }
        }
        /// <summary>
        /// Background worker running on Copy data from source to destination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
            DataCopy dc = new DataCopy();
            strFailed = dc.CopyData(strSourceDbPath, strDestinationDb);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CData:3" + ex.Message;
            }
        }
        /// <summary>
        /// Background worker process completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                f1.Close();
                if (strFailed == "")
                {
                    lblSuccess.Visible = true;
                    lblSuccess.Text = "Data copied succesffully";
                    lblSuccess.ForeColor = Color.White;
                }
                else
                {
                    MessageBox.Show("Failed for the tables\n" + strFailed, "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CData:4" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Copy' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCopy_Click(object sender, EventArgs e)
        {
            try
            {
            strSourceDbPath = txtSourcePath.Text.Trim();
            strDestinationDb = txtDestinationPath.Text.Trim();
            if (strSourceDbPath == string.Empty)
            {
                MessageBox.Show("Select source path", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (strDestinationDb == string.Empty)
                {
                    MessageBox.Show("Select destination path", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    lblSuccess.Visible = false;
                    backgroundWorker1.RunWorkerAsync();
                    f1 = new frmLoading();
                    f1.ShowDialog();
                    
                }
            }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CData:5" + ex.Message;
            }
        }
        #endregion
    }
}
