﻿ 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MATFinancials
{
    public partial class frmQueryLogin : Form
    {
        #region Function
        /// <summary>
        /// Create instance of frmJournalVoucher
        /// </summary>
        public frmQueryLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Function to clear controls
        /// </summary>
        public void Clear()
        {
            try
            {
                txtUserName.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtUserName.Focus();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:1" + ex.Message;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// On 'Login' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                txtUserName.Text = txtUserName.Text.Trim();
                txtPassword.Text = txtPassword.Text.Trim();
                if (txtUserName.Text.ToLower() == "admin" && txtPassword.Text == "cybro")
                {
                    this.Close();
                    frmStoredProcedureInserter frmStoredProcedureInserter = new frmStoredProcedureInserter();
                    frmStoredProcedureInserter _isOpen = Application.OpenForms["frmStoredProcedureInserter"] as frmStoredProcedureInserter;
                    if (_isOpen == null)
                    {
                        frmStoredProcedureInserter.WindowState = FormWindowState.Normal;
                        frmStoredProcedureInserter.MdiParent = formMDI.MDIObj;//formMDI.frmMDI;
                        frmStoredProcedureInserter.Show();
                    }
                    else
                    {
                        _isOpen.MdiParent = formMDI.MDIObj;
                        if (_isOpen.WindowState == FormWindowState.Minimized)
                        {
                            _isOpen.WindowState = FormWindowState.Normal;
                        }
                        if (_isOpen.Enabled)
                        {
                            _isOpen.Activate();
                            _isOpen.BringToFront();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:2" + ex.Message;
            }
        }
        /// <summary>
        /// key navigation for textbox control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtPassword.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:3" + ex.Message;
            }
        }
        /// <summary>
        /// key navigation for textbox control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnLogin.Focus();
                }
                else if (e.KeyCode == Keys.Back)
                {
                    txtUserName.Focus();
                    txtUserName.SelectionStart = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:4" + ex.Message;
            }
        }
        /// <summary>
        /// key navigation for button control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Back)
                {
                    txtPassword.Focus();
                    txtPassword.SelectionStart = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:5" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Clear' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:6" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Close' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
            this.Close();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:7" + ex.Message;
            }
        }
        /// <summary>
        /// On frmQueryLogin form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmQueryLogin_Load(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "QRYL:8" + ex.Message;
            }
        }
        #endregion
    }
}
