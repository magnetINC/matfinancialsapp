﻿namespace MATFinancials
{
    partial class frmMsSqlInstallerforMatFinancials
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMsSqlInstallerforMatFinancials));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.TextBox();
            this.rbtnMultyUser = new System.Windows.Forms.RadioButton();
            this.rbtnSingleUser = new System.Windows.Forms.RadioButton();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnInstall = new System.Windows.Forms.Button();
            this.gbFeature = new System.Windows.Forms.GroupBox();
            this.pnlCredencial = new System.Windows.Forms.Panel();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxSap = new System.Windows.Forms.CheckBox();
            this.cbxMssqlServer = new System.Windows.Forms.CheckBox();
            this.pbxProcess = new System.Windows.Forms.ProgressBar();
            this.lblEvent = new System.Windows.Forms.Label();
            this.pbxEvent = new System.Windows.Forms.ProgressBar();
            this.lblProcess = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblInstallationFeedback2 = new System.Windows.Forms.Label();
            this.rbtnNetworkServers = new System.Windows.Forms.RadioButton();
            this.rbtnLocalServer = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.cmbPath = new System.Windows.Forms.ComboBox();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbServers1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbInstance1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOkServer = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPasswordSql = new System.Windows.Forms.TextBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblInstallationFeedback = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            this.gbFeature.SuspendLayout();
            this.pnlCredencial.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(404, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "MAT FINANCIALS MSSQL SERVER INSTALLATION";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblMessage);
            this.panel1.Controls.Add(this.rbtnMultyUser);
            this.panel1.Controls.Add(this.rbtnSingleUser);
            this.panel1.Controls.Add(this.btnBack);
            this.panel1.Controls.Add(this.btnInstall);
            this.panel1.Controls.Add(this.gbFeature);
            this.panel1.Controls.Add(this.pbxProcess);
            this.panel1.Controls.Add(this.lblEvent);
            this.panel1.Controls.Add(this.pbxEvent);
            this.panel1.Controls.Add(this.lblProcess);
            this.panel1.Location = new System.Drawing.Point(4, 60);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 500);
            this.panel1.TabIndex = 6;
            this.panel1.Visible = false;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMessage.Location = new System.Drawing.Point(15, 385);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(4);
            this.lblMessage.Multiline = true;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.ReadOnly = true;
            this.lblMessage.Size = new System.Drawing.Size(440, 39);
            this.lblMessage.TabIndex = 14;
            this.lblMessage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rbtnMultyUser
            // 
            this.rbtnMultyUser.AutoSize = true;
            this.rbtnMultyUser.Location = new System.Drawing.Point(369, 19);
            this.rbtnMultyUser.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnMultyUser.Name = "rbtnMultyUser";
            this.rbtnMultyUser.Size = new System.Drawing.Size(91, 23);
            this.rbtnMultyUser.TabIndex = 13;
            this.rbtnMultyUser.Text = "Multi User";
            this.rbtnMultyUser.UseVisualStyleBackColor = true;
            // 
            // rbtnSingleUser
            // 
            this.rbtnSingleUser.AutoSize = true;
            this.rbtnSingleUser.Checked = true;
            this.rbtnSingleUser.Location = new System.Drawing.Point(260, 19);
            this.rbtnSingleUser.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnSingleUser.Name = "rbtnSingleUser";
            this.rbtnSingleUser.Size = new System.Drawing.Size(95, 23);
            this.rbtnSingleUser.TabIndex = 12;
            this.rbtnSingleUser.TabStop = true;
            this.rbtnSingleUser.Text = "Single User";
            this.rbtnSingleUser.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(219, 439);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(113, 44);
            this.btnBack.TabIndex = 10;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnInstall
            // 
            this.btnInstall.Location = new System.Drawing.Point(341, 437);
            this.btnInstall.Margin = new System.Windows.Forms.Padding(4);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(113, 44);
            this.btnInstall.TabIndex = 9;
            this.btnInstall.Text = "Install";
            this.btnInstall.UseVisualStyleBackColor = true;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // gbFeature
            // 
            this.gbFeature.BackColor = System.Drawing.Color.Transparent;
            this.gbFeature.Controls.Add(this.pnlCredencial);
            this.gbFeature.Controls.Add(this.cbxSap);
            this.gbFeature.Controls.Add(this.cbxMssqlServer);
            this.gbFeature.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbFeature.ForeColor = System.Drawing.Color.Crimson;
            this.gbFeature.Location = new System.Drawing.Point(12, 18);
            this.gbFeature.Margin = new System.Windows.Forms.Padding(4);
            this.gbFeature.Name = "gbFeature";
            this.gbFeature.Padding = new System.Windows.Forms.Padding(4);
            this.gbFeature.Size = new System.Drawing.Size(460, 236);
            this.gbFeature.TabIndex = 8;
            this.gbFeature.TabStop = false;
            this.gbFeature.Text = "Select features to install.";
            // 
            // pnlCredencial
            // 
            this.pnlCredencial.Controls.Add(this.txtConfirmPassword);
            this.pnlCredencial.Controls.Add(this.txtPassword);
            this.pnlCredencial.Controls.Add(this.label5);
            this.pnlCredencial.Controls.Add(this.label4);
            this.pnlCredencial.Controls.Add(this.label3);
            this.pnlCredencial.Location = new System.Drawing.Point(-8, 76);
            this.pnlCredencial.Margin = new System.Windows.Forms.Padding(4);
            this.pnlCredencial.Name = "pnlCredencial";
            this.pnlCredencial.Size = new System.Drawing.Size(476, 145);
            this.pnlCredencial.TabIndex = 4;
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(176, 97);
            this.txtConfirmPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.Size = new System.Drawing.Size(241, 26);
            this.txtConfirmPassword.TabIndex = 15;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(176, 56);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(241, 26);
            this.txtPassword.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(20, 56);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 19);
            this.label5.TabIndex = 13;
            this.label5.Text = "Enter Password :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(20, 97);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 19);
            this.label4.TabIndex = 12;
            this.label4.Text = "Confirm Password :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(336, 19);
            this.label3.TabIndex = 11;
            this.label3.Text = "Built-in SQL Server system administrator account";
            // 
            // cbxSap
            // 
            this.cbxSap.AutoSize = true;
            this.cbxSap.Checked = true;
            this.cbxSap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxSap.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSap.ForeColor = System.Drawing.Color.Black;
            this.cbxSap.Location = new System.Drawing.Point(251, 40);
            this.cbxSap.Margin = new System.Windows.Forms.Padding(4);
            this.cbxSap.Name = "cbxSap";
            this.cbxSap.Size = new System.Drawing.Size(146, 23);
            this.cbxSap.TabIndex = 3;
            this.cbxSap.Text = "SAP Crystal Report.";
            this.cbxSap.UseVisualStyleBackColor = true;
            // 
            // cbxMssqlServer
            // 
            this.cbxMssqlServer.AutoSize = true;
            this.cbxMssqlServer.Checked = true;
            this.cbxMssqlServer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxMssqlServer.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxMssqlServer.ForeColor = System.Drawing.Color.Black;
            this.cbxMssqlServer.Location = new System.Drawing.Point(48, 40);
            this.cbxMssqlServer.Margin = new System.Windows.Forms.Padding(4);
            this.cbxMssqlServer.Name = "cbxMssqlServer";
            this.cbxMssqlServer.Size = new System.Drawing.Size(155, 23);
            this.cbxMssqlServer.TabIndex = 0;
            this.cbxMssqlServer.Text = "MS SQL Server 2012";
            this.cbxMssqlServer.UseVisualStyleBackColor = true;
            this.cbxMssqlServer.CheckedChanged += new System.EventHandler(this.cbxMssqlServer_CheckedChanged);
            // 
            // pbxProcess
            // 
            this.pbxProcess.Location = new System.Drawing.Point(13, 347);
            this.pbxProcess.Margin = new System.Windows.Forms.Padding(4);
            this.pbxProcess.MarqueeAnimationSpeed = 60;
            this.pbxProcess.Name = "pbxProcess";
            this.pbxProcess.Size = new System.Drawing.Size(443, 15);
            this.pbxProcess.TabIndex = 7;
            // 
            // lblEvent
            // 
            this.lblEvent.AutoSize = true;
            this.lblEvent.Location = new System.Drawing.Point(12, 257);
            this.lblEvent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEvent.Name = "lblEvent";
            this.lblEvent.Size = new System.Drawing.Size(62, 19);
            this.lblEvent.TabIndex = 6;
            this.lblEvent.Text = "Program";
            // 
            // pbxEvent
            // 
            this.pbxEvent.Location = new System.Drawing.Point(13, 282);
            this.pbxEvent.Margin = new System.Windows.Forms.Padding(4);
            this.pbxEvent.Name = "pbxEvent";
            this.pbxEvent.Size = new System.Drawing.Size(441, 15);
            this.pbxEvent.TabIndex = 5;
            // 
            // lblProcess
            // 
            this.lblProcess.AutoSize = true;
            this.lblProcess.Location = new System.Drawing.Point(11, 325);
            this.lblProcess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(55, 19);
            this.lblProcess.TabIndex = 4;
            this.lblProcess.Text = "Process";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.lblInstallationFeedback2);
            this.panel2.Controls.Add(this.rbtnNetworkServers);
            this.panel2.Controls.Add(this.rbtnLocalServer);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.cmbPath);
            this.panel2.Controls.Add(this.linkLabel4);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.cmbServers1);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.cmbInstance1);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.btnOkServer);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(17, 58);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(544, 565);
            this.panel2.TabIndex = 7;
            this.panel2.Visible = false;
            // 
            // lblInstallationFeedback2
            // 
            this.lblInstallationFeedback2.AutoSize = true;
            this.lblInstallationFeedback2.Location = new System.Drawing.Point(45, 39);
            this.lblInstallationFeedback2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInstallationFeedback2.Name = "lblInstallationFeedback2";
            this.lblInstallationFeedback2.Size = new System.Drawing.Size(0, 19);
            this.lblInstallationFeedback2.TabIndex = 66;
            // 
            // rbtnNetworkServers
            // 
            this.rbtnNetworkServers.AutoSize = true;
            this.rbtnNetworkServers.Location = new System.Drawing.Point(328, 57);
            this.rbtnNetworkServers.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnNetworkServers.Name = "rbtnNetworkServers";
            this.rbtnNetworkServers.Size = new System.Drawing.Size(127, 23);
            this.rbtnNetworkServers.TabIndex = 1;
            this.rbtnNetworkServers.Text = "Network Servers";
            this.rbtnNetworkServers.UseVisualStyleBackColor = true;
            // 
            // rbtnLocalServer
            // 
            this.rbtnLocalServer.AutoSize = true;
            this.rbtnLocalServer.Checked = true;
            this.rbtnLocalServer.Location = new System.Drawing.Point(206, 57);
            this.rbtnLocalServer.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnLocalServer.Name = "rbtnLocalServer";
            this.rbtnLocalServer.Size = new System.Drawing.Size(106, 23);
            this.rbtnLocalServer.TabIndex = 0;
            this.rbtnLocalServer.TabStop = true;
            this.rbtnLocalServer.Text = "Local Servers";
            this.rbtnLocalServer.UseVisualStyleBackColor = true;
            this.rbtnLocalServer.CheckedChanged += new System.EventHandler(this.rbtnLocalServer_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(353, 510);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(91, 44);
            this.button2.TabIndex = 8;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmbPath
            // 
            this.cmbPath.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPath.FormattingEnabled = true;
            this.cmbPath.Location = new System.Drawing.Point(33, 460);
            this.cmbPath.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPath.Name = "cmbPath";
            this.cmbPath.Size = new System.Drawing.Size(409, 27);
            this.cmbPath.TabIndex = 6;
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.LinkColor = System.Drawing.Color.Maroon;
            this.linkLabel4.Location = new System.Drawing.Point(392, 94);
            this.linkLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(54, 19);
            this.linkLabel4.TabIndex = 2;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Refresh";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(32, 432);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(138, 19);
            this.label13.TabIndex = 56;
            this.label13.Text = "MAT Financials Path :";
            // 
            // cmbServers1
            // 
            this.cmbServers1.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbServers1.FormattingEnabled = true;
            this.cmbServers1.Location = new System.Drawing.Point(37, 123);
            this.cmbServers1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbServers1.Name = "cmbServers1";
            this.cmbServers1.Size = new System.Drawing.Size(409, 27);
            this.cmbServers1.TabIndex = 3;
            this.cmbServers1.SelectedValueChanged += new System.EventHandler(this.cmbServers1_SelectedValueChanged);
            this.cmbServers1.BindingContextChanged += new System.EventHandler(this.cmbServers1_BindingContextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 94);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 19);
            this.label9.TabIndex = 52;
            this.label9.Text = "System Name";
            // 
            // cmbInstance1
            // 
            this.cmbInstance1.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbInstance1.FormattingEnabled = true;
            this.cmbInstance1.Location = new System.Drawing.Point(37, 186);
            this.cmbInstance1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbInstance1.Name = "cmbInstance1";
            this.cmbInstance1.Size = new System.Drawing.Size(409, 27);
            this.cmbInstance1.TabIndex = 4;
            this.cmbInstance1.SelectedValueChanged += new System.EventHandler(this.cmbInstance1_SelectedValueChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(232, 510);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 44);
            this.button1.TabIndex = 7;
            this.button1.Text = "Back";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnOkServer
            // 
            this.btnOkServer.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkServer.ForeColor = System.Drawing.Color.Maroon;
            this.btnOkServer.Location = new System.Drawing.Point(353, 405);
            this.btnOkServer.Margin = new System.Windows.Forms.Padding(4);
            this.btnOkServer.Name = "btnOkServer";
            this.btnOkServer.Size = new System.Drawing.Size(89, 44);
            this.btnOkServer.TabIndex = 5;
            this.btnOkServer.Text = "Connect";
            this.btnOkServer.UseVisualStyleBackColor = true;
            this.btnOkServer.Click += new System.EventHandler(this.btnOkServer_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(35, 159);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 19);
            this.label12.TabIndex = 42;
            this.label12.Text = "Server Instance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(474, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "CONFIGURATION TO USE AN EXISTING MSSQL SERVER INSTANCE";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtPasswordSql);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(12, 228);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(492, 183);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtPasswordSql
            // 
            this.txtPasswordSql.Enabled = false;
            this.txtPasswordSql.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordSql.Location = new System.Drawing.Point(20, 143);
            this.txtPasswordSql.Margin = new System.Windows.Forms.Padding(4);
            this.txtPasswordSql.Name = "txtPasswordSql";
            this.txtPasswordSql.Size = new System.Drawing.Size(408, 26);
            this.txtPasswordSql.TabIndex = 3;
            this.txtPasswordSql.UseSystemPasswordChar = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.Location = new System.Drawing.Point(243, 17);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(218, 23);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.Text = "Use SQL Server Authentication.";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(28, 17);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(207, 23);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Use Windows Authentication.";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // txtUserName
            // 
            this.txtUserName.Enabled = false;
            this.txtUserName.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(21, 78);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(408, 26);
            this.txtUserName.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(21, 56);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 19);
            this.label10.TabIndex = 51;
            this.label10.Text = "User ID :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 119);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 19);
            this.label11.TabIndex = 52;
            this.label11.Text = "Password :";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblInstallationFeedback);
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.pictureBox3);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.linkLabel2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.linkLabel1);
            this.panel3.Location = new System.Drawing.Point(4, 57);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(547, 503);
            this.panel3.TabIndex = 8;
            // 
            // lblInstallationFeedback
            // 
            this.lblInstallationFeedback.AutoSize = true;
            this.lblInstallationFeedback.Location = new System.Drawing.Point(12, 3);
            this.lblInstallationFeedback.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInstallationFeedback.Name = "lblInstallationFeedback";
            this.lblInstallationFeedback.Size = new System.Drawing.Size(0, 19);
            this.lblInstallationFeedback.TabIndex = 16;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::MATFinancials.Properties.Resources.mssql;
            this.pictureBox5.Location = new System.Drawing.Point(29, 406);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(192, 83);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 14;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::MATFinancials.Properties.Resources.MicrosoftSQL_Server;
            this.pictureBox4.Location = new System.Drawing.Point(304, 409);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(140, 75);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(35, 385);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(450, 1);
            this.panel5.TabIndex = 11;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::MATFinancials.Properties.Resources.mssql;
            this.pictureBox3.Location = new System.Drawing.Point(29, 255);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(115, 114);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::MATFinancials.Properties.Resources.MicrosoftSQL_Server;
            this.pictureBox2.Location = new System.Drawing.Point(27, 69);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(123, 142);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(160, 261);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(337, 108);
            this.label7.TabIndex = 3;
            this.label7.Text = "An easy to use - wizard based - UI will help you through the installation process" +
    "es of MSSQL Server 2012 and SAP Crystal Report runtime. These are pre-requisite " +
    "packages. ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.LinkColor = System.Drawing.Color.Crimson;
            this.linkLabel2.Location = new System.Drawing.Point(12, 229);
            this.linkLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(453, 16);
            this.linkLabel2.TabIndex = 2;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "SQL SERVER AND REPORT RUNTIME INSTALLATION WIZARD";
            this.linkLabel2.Click += new System.EventHandler(this.linkLabel2_Click);
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(155, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(309, 91);
            this.label6.TabIndex = 1;
            this.label6.Text = "An intuitive wizard for MAT Financials database configuration on an existing MSSQ" +
    "L server instance.";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.Crimson;
            this.linkLabel1.Location = new System.Drawing.Point(13, 42);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(407, 16);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "MAT FINANCIALS DATABASE CONFIGURATION WIZARD";
            this.linkLabel1.Click += new System.EventHandler(this.linkLabel1_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 635);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.MarqueeAnimationSpeed = 30;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(487, 12);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 9;
            // 
            // frmMsSqlInstallerforMatFinancials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(599, 652);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMsSqlInstallerforMatFinancials";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Configuration";
            this.Load += new System.EventHandler(this.frmMySqlInstallerforMatFinancials_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbFeature.ResumeLayout(false);
            this.gbFeature.PerformLayout();
            this.pnlCredencial.ResumeLayout(false);
            this.pnlCredencial.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar pbxProcess;
        private System.Windows.Forms.Label lblEvent;
        private System.Windows.Forms.ProgressBar pbxEvent;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbFeature;
        private System.Windows.Forms.CheckBox cbxSap;
        private System.Windows.Forms.CheckBox cbxMssqlServer;
        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbServers1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbInstance1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnOkServer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.ComboBox cmbPath;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.RadioButton rbtnNetworkServers;
        private System.Windows.Forms.RadioButton rbtnLocalServer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rbtnMultyUser;
        private System.Windows.Forms.RadioButton rbtnSingleUser;
        private System.Windows.Forms.TextBox lblMessage;
        private System.Windows.Forms.Panel pnlCredencial;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtPasswordSql;
        private System.Windows.Forms.Label lblInstallationFeedback2;
        private System.Windows.Forms.Label lblInstallationFeedback;
    }
}