﻿namespace MATFinancials.Other
{
    partial class frmAvailableQuantity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAvailableQuantity));
            this.dgvAvailableQuantity = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvSalesOrder = new System.Windows.Forms.DataGridView();
            this.SalesOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvpurchaseOrder = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblItemName = new System.Windows.Forms.Label();
            this.lblItemCode = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godownName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyBal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAllSites = new System.Windows.Forms.Label();
            this.lblQtyonSalesOrder = new System.Windows.Forms.Label();
            this.lblQtyonHand = new System.Windows.Forms.Label();
            this.lblQtyonSalesOrder2 = new System.Windows.Forms.Label();
            this.lblQtyonHand2 = new System.Windows.Forms.Label();
            this.lblQtyAvailable2 = new System.Windows.Forms.Label();
            this.lblQtyOnPurchaseOrder2 = new System.Windows.Forms.Label();
            this.lblQtyOnPurchaseOrder = new System.Windows.Forms.Label();
            this.lblIncoming = new System.Windows.Forms.Label();
            this.lblQtyAvailable = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvpurchaseOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAvailableQuantity
            // 
            this.dgvAvailableQuantity.AllowUserToAddRows = false;
            this.dgvAvailableQuantity.AllowUserToDeleteRows = false;
            this.dgvAvailableQuantity.AllowUserToResizeColumns = false;
            this.dgvAvailableQuantity.AllowUserToResizeRows = false;
            this.dgvAvailableQuantity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAvailableQuantity.BackgroundColor = System.Drawing.Color.White;
            this.dgvAvailableQuantity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAvailableQuantity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvAvailableQuantity.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAvailableQuantity.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAvailableQuantity.ColumnHeadersHeight = 31;
            this.dgvAvailableQuantity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productName,
            this.dgvtxtItemCode,
            this.godownName,
            this.qtyBal});
            this.dgvAvailableQuantity.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAvailableQuantity.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvAvailableQuantity.EnableHeadersVisualStyles = false;
            this.dgvAvailableQuantity.GridColor = System.Drawing.Color.White;
            this.dgvAvailableQuantity.Location = new System.Drawing.Point(12, 54);
            this.dgvAvailableQuantity.MultiSelect = false;
            this.dgvAvailableQuantity.Name = "dgvAvailableQuantity";
            this.dgvAvailableQuantity.ReadOnly = true;
            this.dgvAvailableQuantity.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAvailableQuantity.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvAvailableQuantity.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvAvailableQuantity.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvAvailableQuantity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAvailableQuantity.ShowEditingIcon = false;
            this.dgvAvailableQuantity.Size = new System.Drawing.Size(326, 150);
            this.dgvAvailableQuantity.TabIndex = 1317;
            this.dgvAvailableQuantity.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAvailableQuantity_CellClick);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(479, 228);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(69, 27);
            this.btnClose.TabIndex = 1319;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvSalesOrder
            // 
            this.dgvSalesOrder.AllowUserToAddRows = false;
            this.dgvSalesOrder.AllowUserToDeleteRows = false;
            this.dgvSalesOrder.AllowUserToResizeColumns = false;
            this.dgvSalesOrder.AllowUserToResizeRows = false;
            this.dgvSalesOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalesOrder.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalesOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSalesOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvSalesOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvSalesOrder.ColumnHeadersHeight = 31;
            this.dgvSalesOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SalesOrder});
            this.dgvSalesOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalesOrder.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvSalesOrder.EnableHeadersVisualStyles = false;
            this.dgvSalesOrder.GridColor = System.Drawing.Color.White;
            this.dgvSalesOrder.Location = new System.Drawing.Point(117, 211);
            this.dgvSalesOrder.MultiSelect = false;
            this.dgvSalesOrder.Name = "dgvSalesOrder";
            this.dgvSalesOrder.ReadOnly = true;
            this.dgvSalesOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSalesOrder.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvSalesOrder.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSalesOrder.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvSalesOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalesOrder.ShowEditingIcon = false;
            this.dgvSalesOrder.Size = new System.Drawing.Size(36, 44);
            this.dgvSalesOrder.TabIndex = 1320;
            this.dgvSalesOrder.Visible = false;
            // 
            // SalesOrder
            // 
            this.SalesOrder.DataPropertyName = "SalesOrder";
            dataGridViewCellStyle9.Format = "N2";
            dataGridViewCellStyle9.NullValue = null;
            this.SalesOrder.DefaultCellStyle = dataGridViewCellStyle9;
            this.SalesOrder.HeaderText = "Sales Order";
            this.SalesOrder.Name = "SalesOrder";
            this.SalesOrder.ReadOnly = true;
            // 
            // dgvpurchaseOrder
            // 
            this.dgvpurchaseOrder.AllowUserToAddRows = false;
            this.dgvpurchaseOrder.AllowUserToDeleteRows = false;
            this.dgvpurchaseOrder.AllowUserToResizeColumns = false;
            this.dgvpurchaseOrder.AllowUserToResizeRows = false;
            this.dgvpurchaseOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvpurchaseOrder.BackgroundColor = System.Drawing.Color.White;
            this.dgvpurchaseOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvpurchaseOrder.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvpurchaseOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvpurchaseOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvpurchaseOrder.ColumnHeadersHeight = 31;
            this.dgvpurchaseOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgvpurchaseOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvpurchaseOrder.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgvpurchaseOrder.EnableHeadersVisualStyles = false;
            this.dgvpurchaseOrder.GridColor = System.Drawing.Color.White;
            this.dgvpurchaseOrder.Location = new System.Drawing.Point(157, 211);
            this.dgvpurchaseOrder.MultiSelect = false;
            this.dgvpurchaseOrder.Name = "dgvpurchaseOrder";
            this.dgvpurchaseOrder.ReadOnly = true;
            this.dgvpurchaseOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvpurchaseOrder.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgvpurchaseOrder.RowHeadersVisible = false;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvpurchaseOrder.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvpurchaseOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvpurchaseOrder.ShowEditingIcon = false;
            this.dgvpurchaseOrder.Size = new System.Drawing.Size(41, 44);
            this.dgvpurchaseOrder.TabIndex = 1321;
            this.dgvpurchaseOrder.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "purchaseOrder";
            dataGridViewCellStyle14.Format = "N2";
            dataGridViewCellStyle14.NullValue = null;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn1.HeaderText = "Purchase Order";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // lblItemName
            // 
            this.lblItemName.AutoSize = true;
            this.lblItemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemName.Location = new System.Drawing.Point(114, 6);
            this.lblItemName.Name = "lblItemName";
            this.lblItemName.Size = new System.Drawing.Size(27, 15);
            this.lblItemName.TabIndex = 1322;
            this.lblItemName.Text = " ... ";
            // 
            // lblItemCode
            // 
            this.lblItemCode.AutoSize = true;
            this.lblItemCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemCode.Location = new System.Drawing.Point(114, 27);
            this.lblItemCode.Name = "lblItemCode";
            this.lblItemCode.Size = new System.Drawing.Size(27, 15);
            this.lblItemCode.TabIndex = 1323;
            this.lblItemCode.Text = " ... ";
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.Location = new System.Drawing.Point(12, 28);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(63, 15);
            this.lblCode.TabIndex = 1325;
            this.lblCode.Text = "Item Code";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(12, 7);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(68, 15);
            this.lblName.TabIndex = 1324;
            this.lblName.Text = "Item Name";
            // 
            // productName
            // 
            this.productName.DataPropertyName = "productName";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.productName.DefaultCellStyle = dataGridViewCellStyle2;
            this.productName.HeaderText = "Item";
            this.productName.Name = "productName";
            this.productName.ReadOnly = true;
            this.productName.Visible = false;
            // 
            // dgvtxtItemCode
            // 
            this.dgvtxtItemCode.DataPropertyName = "productCode";
            this.dgvtxtItemCode.HeaderText = "Item Code";
            this.dgvtxtItemCode.Name = "dgvtxtItemCode";
            this.dgvtxtItemCode.ReadOnly = true;
            this.dgvtxtItemCode.Visible = false;
            // 
            // godownName
            // 
            this.godownName.DataPropertyName = "godownName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            this.godownName.DefaultCellStyle = dataGridViewCellStyle3;
            this.godownName.HeaderText = "Store Name";
            this.godownName.Name = "godownName";
            this.godownName.ReadOnly = true;
            // 
            // qtyBal
            // 
            this.qtyBal.DataPropertyName = "qtyBal";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.qtyBal.DefaultCellStyle = dataGridViewCellStyle4;
            this.qtyBal.HeaderText = "Qty on Hand";
            this.qtyBal.Name = "qtyBal";
            this.qtyBal.ReadOnly = true;
            // 
            // lblAllSites
            // 
            this.lblAllSites.AutoSize = true;
            this.lblAllSites.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllSites.Location = new System.Drawing.Point(516, 27);
            this.lblAllSites.Name = "lblAllSites";
            this.lblAllSites.Size = new System.Drawing.Size(59, 15);
            this.lblAllSites.TabIndex = 1326;
            this.lblAllSites.Text = "All Sites";
            // 
            // lblQtyonSalesOrder
            // 
            this.lblQtyonSalesOrder.AutoSize = true;
            this.lblQtyonSalesOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyonSalesOrder.Location = new System.Drawing.Point(360, 84);
            this.lblQtyonSalesOrder.Name = "lblQtyonSalesOrder";
            this.lblQtyonSalesOrder.Size = new System.Drawing.Size(72, 15);
            this.lblQtyonSalesOrder.TabIndex = 1328;
            this.lblQtyonSalesOrder.Text = "Sales Order";
            // 
            // lblQtyonHand
            // 
            this.lblQtyonHand.AutoSize = true;
            this.lblQtyonHand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyonHand.Location = new System.Drawing.Point(360, 55);
            this.lblQtyonHand.Name = "lblQtyonHand";
            this.lblQtyonHand.Size = new System.Drawing.Size(74, 15);
            this.lblQtyonHand.TabIndex = 1327;
            this.lblQtyonHand.Text = "Qty on Hand";
            // 
            // lblQtyonSalesOrder2
            // 
            this.lblQtyonSalesOrder2.AutoSize = true;
            this.lblQtyonSalesOrder2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyonSalesOrder2.Location = new System.Drawing.Point(511, 84);
            this.lblQtyonSalesOrder2.Name = "lblQtyonSalesOrder2";
            this.lblQtyonSalesOrder2.Size = new System.Drawing.Size(27, 15);
            this.lblQtyonSalesOrder2.TabIndex = 1330;
            this.lblQtyonSalesOrder2.Text = " ... ";
            // 
            // lblQtyonHand2
            // 
            this.lblQtyonHand2.AutoSize = true;
            this.lblQtyonHand2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyonHand2.Location = new System.Drawing.Point(511, 55);
            this.lblQtyonHand2.Name = "lblQtyonHand2";
            this.lblQtyonHand2.Size = new System.Drawing.Size(27, 15);
            this.lblQtyonHand2.TabIndex = 1329;
            this.lblQtyonHand2.Text = " ... ";
            // 
            // lblQtyAvailable2
            // 
            this.lblQtyAvailable2.AutoSize = true;
            this.lblQtyAvailable2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyAvailable2.Location = new System.Drawing.Point(511, 122);
            this.lblQtyAvailable2.Name = "lblQtyAvailable2";
            this.lblQtyAvailable2.Size = new System.Drawing.Size(27, 15);
            this.lblQtyAvailable2.TabIndex = 1331;
            this.lblQtyAvailable2.Text = " ... ";
            // 
            // lblQtyOnPurchaseOrder2
            // 
            this.lblQtyOnPurchaseOrder2.AutoSize = true;
            this.lblQtyOnPurchaseOrder2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyOnPurchaseOrder2.Location = new System.Drawing.Point(511, 183);
            this.lblQtyOnPurchaseOrder2.Name = "lblQtyOnPurchaseOrder2";
            this.lblQtyOnPurchaseOrder2.Size = new System.Drawing.Size(27, 15);
            this.lblQtyOnPurchaseOrder2.TabIndex = 1333;
            this.lblQtyOnPurchaseOrder2.Text = " ... ";
            // 
            // lblQtyOnPurchaseOrder
            // 
            this.lblQtyOnPurchaseOrder.AutoSize = true;
            this.lblQtyOnPurchaseOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyOnPurchaseOrder.Location = new System.Drawing.Point(360, 183);
            this.lblQtyOnPurchaseOrder.Name = "lblQtyOnPurchaseOrder";
            this.lblQtyOnPurchaseOrder.Size = new System.Drawing.Size(93, 15);
            this.lblQtyOnPurchaseOrder.TabIndex = 1336;
            this.lblQtyOnPurchaseOrder.Text = "Purchase Order";
            // 
            // lblIncoming
            // 
            this.lblIncoming.AutoSize = true;
            this.lblIncoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncoming.Location = new System.Drawing.Point(360, 154);
            this.lblIncoming.Name = "lblIncoming";
            this.lblIncoming.Size = new System.Drawing.Size(68, 15);
            this.lblIncoming.TabIndex = 1335;
            this.lblIncoming.Text = "INCOMING";
            // 
            // lblQtyAvailable
            // 
            this.lblQtyAvailable.AutoSize = true;
            this.lblQtyAvailable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtyAvailable.Location = new System.Drawing.Point(360, 122);
            this.lblQtyAvailable.Name = "lblQtyAvailable";
            this.lblQtyAvailable.Size = new System.Drawing.Size(121, 15);
            this.lblQtyAvailable.TabIndex = 1334;
            this.lblQtyAvailable.Text = "Available Quantity";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Black;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(467, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(105, 1);
            this.groupBox1.TabIndex = 1337;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Black;
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(467, 45);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(105, 1);
            this.groupBox2.TabIndex = 1338;
            this.groupBox2.TabStop = false;
            // 
            // frmAvailableQuantity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(590, 263);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblQtyOnPurchaseOrder);
            this.Controls.Add(this.lblIncoming);
            this.Controls.Add(this.lblQtyAvailable);
            this.Controls.Add(this.lblQtyOnPurchaseOrder2);
            this.Controls.Add(this.lblQtyAvailable2);
            this.Controls.Add(this.lblQtyonSalesOrder2);
            this.Controls.Add(this.lblQtyonHand2);
            this.Controls.Add(this.lblQtyonSalesOrder);
            this.Controls.Add(this.lblQtyonHand);
            this.Controls.Add(this.lblAllSites);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblItemCode);
            this.Controls.Add(this.lblItemName);
            this.Controls.Add(this.dgvpurchaseOrder);
            this.Controls.Add(this.dgvSalesOrder);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvAvailableQuantity);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAvailableQuantity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Available Quantity";
            this.Load += new System.EventHandler(this.frmAvailableQuantity_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvpurchaseOrder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAvailableQuantity;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvSalesOrder;
        private System.Windows.Forms.DataGridView dgvpurchaseOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label lblItemName;
        private System.Windows.Forms.Label lblItemCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.DataGridViewTextBoxColumn productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn godownName;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyBal;
        private System.Windows.Forms.Label lblAllSites;
        private System.Windows.Forms.Label lblQtyonSalesOrder;
        private System.Windows.Forms.Label lblQtyonHand;
        private System.Windows.Forms.Label lblQtyonSalesOrder2;
        private System.Windows.Forms.Label lblQtyonHand2;
        private System.Windows.Forms.Label lblQtyAvailable2;
        private System.Windows.Forms.Label lblQtyOnPurchaseOrder2;
        private System.Windows.Forms.Label lblQtyOnPurchaseOrder;
        private System.Windows.Forms.Label lblIncoming;
        private System.Windows.Forms.Label lblQtyAvailable;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}