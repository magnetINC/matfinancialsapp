﻿
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MATFinancials
{
    public partial class frmAboutUs : Form
    {
        #region Function
        /// <summary>
        /// Creates an instance of frmAboutUs class
        /// </summary>
        public frmAboutUs()
        {
            InitializeComponent();
        }
        #endregion
        #region Events
        /// <summary>
        /// On link label click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbtnOM_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.yudiz.com/");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AU1:" + ex.Message;
            }
        }
        /// <summary>
        /// On link label click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbtnCybro_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://yudiz.com/");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AU2:" + ex.Message;
            }
        }
        #endregion
        #region Navigation
        /// <summary>
        /// Escape key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmAboutUs_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (PublicVariables.isMessageClose)
                    {
                        Messages.CloseMessage(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AU3:" + ex.Message;
            }
        }
        #endregion

        private void frmAboutUs_Load(object sender, EventArgs e)
        {
            try
            {
                //label5.Text = Application.ProductName;
                //start pravin 
                label5.Text = "MAT Financials";

                //start pravin 

                //label1.Text = "Version : " + Application.ProductVersion;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AU4:" + ex.Message;
            }
        }
    }
}
