﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Other
{
    public class PostingsHelper
    {
        string strVoucherNo;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        decimal decNewExchangeRateId = 0;
        decimal decOldExchangeId = 0;
        DataTable dt = new DataTable();
        PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
        DateTime TransactionDate = DateTime.Now;
        public PostingsHelper()
        {
            string strVoucherNo = string.Empty;
            dt = new DataTable();
        }
        public void PartyBalanceAdd(PartyBalanceInfo infoPartyBalance)
        {
            //int inTableRowCount = dtblPartyBalance.Rows.Count;
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            try
            {
                InfopartyBalance.CreditPeriod = 0;//
                InfopartyBalance.Date = infoPartyBalance.Date;
                InfopartyBalance.LedgerId = infoPartyBalance.LedgerId;
                InfopartyBalance.ReferenceType = infoPartyBalance.ReferenceType;
                InfopartyBalance.AgainstInvoiceNo = infoPartyBalance.AgainstInvoiceNo;
                InfopartyBalance.AgainstVoucherNo = infoPartyBalance.AgainstVoucherNo;
                InfopartyBalance.AgainstVoucherTypeId = infoPartyBalance.AgainstVoucherTypeId;
                InfopartyBalance.VoucherTypeId = infoPartyBalance.VoucherTypeId;
                InfopartyBalance.InvoiceNo = infoPartyBalance.InvoiceNo;
                InfopartyBalance.VoucherNo = infoPartyBalance.VoucherNo;
                InfopartyBalance.Credit = infoPartyBalance.Credit;
                InfopartyBalance.Debit = infoPartyBalance.Debit;
                InfopartyBalance.ExchangeRateId = infoPartyBalance.ExchangeRateId;
                InfopartyBalance.Extra1 = infoPartyBalance.Extra1;
                InfopartyBalance.Extra2 = infoPartyBalance.Extra2;
                InfopartyBalance.FinancialYearId = infoPartyBalance.FinancialYearId;
                spPartyBalance.PartyBalanceAdd(InfopartyBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT20:" + ex.Message;
            }
        }
        public void PartyBalanceEdit(decimal PartyBalanceId, PartyBalanceInfo infoPartyBalance)
        {
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            try
            {
                InfopartyBalance.PartyBalanceId = PartyBalanceId;
                InfopartyBalance.CreditPeriod = infoPartyBalance.CreditPeriod;
                InfopartyBalance.Date = infoPartyBalance.Date;
                InfopartyBalance.LedgerId = infoPartyBalance.LedgerId;
                InfopartyBalance.ReferenceType = infoPartyBalance.ReferenceType;
                InfopartyBalance.ExchangeRateId = infoPartyBalance.ExchangeRateId;
                InfopartyBalance.AgainstInvoiceNo = infoPartyBalance.AgainstInvoiceNo;
                InfopartyBalance.AgainstVoucherNo = infoPartyBalance.AgainstVoucherNo;
                InfopartyBalance.AgainstVoucherTypeId = infoPartyBalance.AgainstVoucherTypeId;
                InfopartyBalance.VoucherTypeId = infoPartyBalance.VoucherTypeId;
                InfopartyBalance.VoucherNo = infoPartyBalance.VoucherNo;
                InfopartyBalance.InvoiceNo = infoPartyBalance.InvoiceNo;
                InfopartyBalance.Credit = infoPartyBalance.Credit;
                InfopartyBalance.Debit = infoPartyBalance.Debit;
                //InfopartyBalance.ExchangeRateId = Convert.ToDecimal(dt.Rows[0]["CurrencyId"].ToString());
                decNewExchangeRateId = InfopartyBalance.ExchangeRateId;
                decOldExchangeId = InfopartyBalance.ExchangeRateId;
                InfopartyBalance.Extra1 = infoPartyBalance.Extra1;
                InfopartyBalance.Extra2 = infoPartyBalance.Extra2;
                InfopartyBalance.FinancialYearId = infoPartyBalance.FinancialYearId;
                spPartyBalance.PartyBalanceEdit(InfopartyBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT33:" + ex.Message;
            }
        }
        public bool CreditNoteMasterAddOrEdit(CreditNoteMasterInfo infoCreditNoteMaster, CreditNoteDetailsInfo infoCreditNoteDetails, bool CreditMasterEdit, decimal decSelectedCurrencyRate)
        {
            decimal decEffectRow = 0;
            bool isRowAffected = false;
            try
            {
                CreditNoteMasterSP spCreditnoteMaster = new CreditNoteMasterSP();
                //CreditNoteMasterInfo infoCreditNoteMaster = new CreditNoteMasterInfo();
                CreditNoteDetailsSP spCreditNoteDetails = new CreditNoteDetailsSP();
                //CreditNoteDetailsInfo infoCreditNoteDetails = new CreditNoteDetailsInfo();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();

                /*****************Update in CreditNoteMaster table *************/
                decimal decTotalDebit = 0;
                decimal decTotalCredit = 0;

                infoCreditNoteMaster.CreditNoteMasterId = infoCreditNoteMaster.CreditNoteMasterId;
                infoCreditNoteMaster.VoucherNo = infoCreditNoteMaster.VoucherNo;
                infoCreditNoteMaster.InvoiceNo = infoCreditNoteMaster.InvoiceNo;
                infoCreditNoteMaster.SuffixPrefixId = infoCreditNoteMaster.SuffixPrefixId;
                infoCreditNoteMaster.Date = infoCreditNoteMaster.Date;
                infoCreditNoteMaster.Narration = infoCreditNoteMaster.Narration;
                infoCreditNoteMaster.UserId = infoCreditNoteMaster.UserId;
                infoCreditNoteMaster.VoucherTypeId = infoCreditNoteMaster.VoucherTypeId;
                infoCreditNoteMaster.FinancialYearId = infoCreditNoteMaster.FinancialYearId;
                infoCreditNoteMaster.ExtraDate = infoCreditNoteMaster.ExtraDate;
                infoCreditNoteMaster.Extra1 = infoCreditNoteMaster.Extra1;
                infoCreditNoteMaster.Extra2 = infoCreditNoteMaster.Extra2;

                //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());

                infoCreditNoteMaster.TotalAmount = infoCreditNoteMaster.TotalAmount;
                decEffectRow = spCreditnoteMaster.CreditNoteMasterEdit(infoCreditNoteMaster);
            }
            catch (Exception ex) { }
            if (decEffectRow > 0)
            {
                isRowAffected = true;
            }
            return isRowAffected;
        }
        public void CreditNoteDetailsAddOrEdit(CreditNoteMasterInfo infoCreditNoteMaster, CreditNoteDetailsInfo infoCreditNoteDetails, bool CreditMasterEdit, decimal decSelectedCurrencyRate, string newInvoiceNo)
        {
            /**********************CreditNote Details Edit********************/
            CreditNoteDetailsSP spCreditNoteDetails = new CreditNoteDetailsSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            decimal decCreditNoteDetailsId = 0;
            decimal decLedgerId = 0;
            decimal decDebit = 0;
            decimal decCredit = 0;
            try
            {

                infoCreditNoteDetails.CreditNoteMasterId = infoCreditNoteMaster.CreditNoteMasterId; // decCreditNoteMasterId;
                infoCreditNoteDetails.ExtraDate = DateTime.Now;
                infoCreditNoteDetails.Extra1 = string.Empty;
                infoCreditNoteDetails.Extra2 = string.Empty;

                //-----------to delete details, LedgerPosting and bankReconciliation of removed rows--------------// 

                //foreach (object obj in arrlstOfRemove)
                //{
                //    string str = Convert.ToString(obj);
                //    spCreditNoteDetails.CreditNoteDetailsDelete(Convert.ToDecimal(str));
                //    spLedgerPosting.LedgerPostDeleteByDetailsId(Convert.ToDecimal(str), strVoucherNo, infoCreditNoteMaster.VoucherTypeId);
                //}
                //spLedgerPosting.LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId(strVoucherNo, decCreditNoteVoucherTypeId, 12);
                spLedgerPosting.LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId(infoCreditNoteMaster.VoucherNo, infoCreditNoteMaster.VoucherTypeId, 12);
                //=============================================================================================//

                infoCreditNoteDetails.LedgerId = infoCreditNoteDetails.LedgerId;
                decLedgerId = infoCreditNoteDetails.LedgerId;

                infoCreditNoteDetails.Debit = infoCreditNoteDetails.Debit;
                infoCreditNoteDetails.Credit = infoCreditNoteDetails.Credit;

                decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;

                infoCreditNoteDetails.ChequeNo = infoCreditNoteDetails.ChequeNo;
                infoCreditNoteDetails.ChequeDate = infoCreditNoteDetails.ChequeDate;
                // ----------------------- update the ledger posting -------------------------------//
                //if (dgvCreditNote.Rows[inI].Cells["dgvtxtDetailsId"].Value != null && dgvCreditNote.Rows[inI].Cells["dgvtxtDetailsId"].Value.ToString() != string.Empty)
                //{
                infoCreditNoteDetails.CreditNoteDetailsId = infoCreditNoteDetails.CreditNoteDetailsId;
                spCreditNoteDetails.CreditNoteDetailsEdit(infoCreditNoteDetails);


                //PartyBalanceAddOrEdit(inI);
                dt = new DataTable();
                dt = SpPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(22, infoCreditNoteMaster.VoucherNo, infoCreditNoteMaster.Date);
                decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());
                TransactionDate = infoCreditNoteMaster.Date;
                PartyBalanceEditNew(decPartyBalanceId, infoCreditNoteMaster, newInvoiceNo);

                decCreditNoteDetailsId = infoCreditNoteDetails.CreditNoteDetailsId;
                decimal decLedgerPostId = spLedgerPosting.LedgerPostingIdFromDetailsId(infoCreditNoteDetails.CreditNoteDetailsId, infoCreditNoteMaster.VoucherNo, 22);

                //LedgerPostingEdit(decLedgerPostId, decLedgerId, decCredit, decDebit, decCreditNoteDetailsId, infoCreditNoteMaster);

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT35:" + ex.Message;
            }
        }
        public void PartyBalanceEditNew(decimal decPartyBalanceId, CreditNoteMasterInfo infoCreditNoteMaster, string newInvoiceNo)
        {

            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            try
            {
                InfopartyBalance.PartyBalanceId = decPartyBalanceId;
                InfopartyBalance.CreditPeriod = 0;//
                InfopartyBalance.Date = TransactionDate;
                InfopartyBalance.LedgerId = Convert.ToDecimal(dt.Rows[0]["LedgerId"].ToString());
                InfopartyBalance.ReferenceType = "Against"; // dt.Rows[0]["ReferenceType"].ToString();
                // ============= the credit note should be against the invoice on which it is being applied ============= //
                dt.Rows[0]["ReferenceType"] = "Against";
                dt.Rows[0]["ReferenceType"] = "Against";
                if (dt.Rows[0]["ReferenceType"].ToString() == "New" || dt.Rows[0]["ReferenceType"].ToString() == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0"; //dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = 22;
                    InfopartyBalance.InvoiceNo = infoCreditNoteMaster.InvoiceNo;
                    InfopartyBalance.VoucherNo = infoCreditNoteMaster.VoucherNo;
                }
                else
                {
                    InfopartyBalance.ExchangeRateId = Convert.ToDecimal(dt.Rows[0]["OldExchangeRate"].ToString());
                    InfopartyBalance.AgainstInvoiceNo = infoCreditNoteMaster.InvoiceNo;
                    InfopartyBalance.AgainstVoucherNo = infoCreditNoteMaster.VoucherNo;
                    InfopartyBalance.AgainstVoucherTypeId = infoCreditNoteMaster.VoucherTypeId; // 22; // credit note voucher id
                    InfopartyBalance.VoucherTypeId = 19;    // sales invoice voucher id  Convert.ToDecimal(dt.Rows[0]["AgainstVoucherTypeId"].ToString());
                    InfopartyBalance.VoucherNo = newInvoiceNo; // dt.Rows[0]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.InvoiceNo = newInvoiceNo; // dt.Rows[0]["AgainstInvoiceNo"].ToString();
                }
                //if (dgvCreditNote.Rows[inRowIndex].Cells["dgvcmbDrOrCr"].Value.ToString() == "Dr")
                //{
                //    InfopartyBalance.Debit = Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());
                //    InfopartyBalance.Credit = 0;
                //}
                //else
                //{
                InfopartyBalance.Credit = infoCreditNoteMaster.TotalAmount; //Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());
                InfopartyBalance.Debit = 0;
                //}
                InfopartyBalance.ExchangeRateId = Convert.ToDecimal(dt.Rows[0]["CurrencyId"].ToString());
                decNewExchangeRateId = InfopartyBalance.ExchangeRateId;
                decOldExchangeId = InfopartyBalance.ExchangeRateId;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                spPartyBalance.PartyBalanceEdit(InfopartyBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT33:" + ex.Message;
            }
        }
        public void LedgerPostingEdit(decimal decLedgerPostingId, decimal decLedgerId, decimal decCredit, decimal decDebit, decimal decDetailsId, CreditNoteMasterInfo infoCreditNoteMaster)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            //decimal decNewExchangeRateId = 0;
            //decimal decOldExchangeId = 0;
            try
            {

                infoLedgerPosting.LedgerPostingId = decLedgerPostingId;
                infoLedgerPosting.Date = TransactionDate;
                infoLedgerPosting.VoucherTypeId = 22;
                infoLedgerPosting.VoucherNo = infoCreditNoteMaster.VoucherNo;
                infoLedgerPosting.DetailsId = decDetailsId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.InvoiceNo = infoCreditNoteMaster.InvoiceNo;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = infoCreditNoteMaster.Date;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.LedgerId = decLedgerId;
                infoLedgerPosting.Credit = decCredit;
                infoLedgerPosting.Debit = decDebit;
                infoLedgerPosting.ExtraDate = DateTime.Now;

                spLedgerPosting.LedgerPostingEdit(infoLedgerPosting);

                // ---------------------------------- post exchange rate difenrnce to ledger --------------------------- //
                /*
                else
                {
                    infoLedgerPosting.LedgerId = 12;
                    //decNewExchangeRateId = decNewExchangeRateId;
                    decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                    //decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                    decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                    //decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                    decimal decForexAmount = (infoCreditNoteMaster.TotalAmount * decNewExchangeRate) - (infoCreditNoteMaster.TotalAmount * decOldExchange);
                    if (dr["DebitOrCredit"].ToString() == "Dr")
                    {
                        if (decForexAmount >= 0)
                        {

                            infoLedgerPosting.Debit = decForexAmount;
                            infoLedgerPosting.Credit = 0;
                        }
                        else
                        {
                            infoLedgerPosting.Credit = -1 * decForexAmount;
                            infoLedgerPosting.Debit = 0;
                        }
                    }
                    else
                    {
                        if (decForexAmount >= 0)
                        {

                            infoLedgerPosting.Credit = decForexAmount;
                            infoLedgerPosting.Debit = 0;
                        }
                        else
                        {
                            infoLedgerPosting.Debit = -1 * decForexAmount;
                            infoLedgerPosting.Credit = 0;
                        }
                    }
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                */

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT19:" + ex.Message;
            }

        }
        public bool ReceiptMasterEdit(ReceiptMasterInfo infoReceiptMaster, decimal decSelectedCurrencyRate)
        {
            decimal decEffectRow = 0;
            bool isRowAffected = false;
            try
            {
                ReceiptMasterSP spReceiptMaster = new ReceiptMasterSP();
                infoReceiptMaster.ReceiptMasterId = infoReceiptMaster.ReceiptMasterId;
                infoReceiptMaster.VoucherNo = infoReceiptMaster.VoucherNo;
                infoReceiptMaster.InvoiceNo = infoReceiptMaster.InvoiceNo;
                infoReceiptMaster.SuffixPrefixId = infoReceiptMaster.SuffixPrefixId;
                infoReceiptMaster.Date = infoReceiptMaster.Date;
                infoReceiptMaster.Narration = infoReceiptMaster.Narration;
                infoReceiptMaster.UserId = infoReceiptMaster.UserId;
                infoReceiptMaster.VoucherTypeId = infoReceiptMaster.VoucherTypeId;
                infoReceiptMaster.FinancialYearId = infoReceiptMaster.FinancialYearId;
                infoReceiptMaster.ExtraDate = infoReceiptMaster.ExtraDate;
                infoReceiptMaster.Extra1 = infoReceiptMaster.Extra1;
                infoReceiptMaster.Extra2 = infoReceiptMaster.Extra2;

                infoReceiptMaster.TotalAmount = infoReceiptMaster.TotalAmount;
                decEffectRow = spReceiptMaster.ReceiptMasterEdit(infoReceiptMaster);
            }
            catch (Exception ex) { }
            if (decEffectRow > 0)
            {
                isRowAffected = true;
            }
            return isRowAffected;
        }
        public void ReceiptDetailsEdit(ReceiptMasterInfo infoReceiptMaster, ReceiptDetailsInfo infoReceiptDetails, bool CreditMasterEdit, decimal decSelectedCurrencyRate, string newInvoiceNo)
        {
            ReceiptDetailsSP spReceiptDetails = new ReceiptDetailsSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            decimal decCreditNoteDetailsId = 0;
            decimal decLedgerId = 0;
            decimal decDebit = 0;
            decimal decCredit = 0;
            decimal Amount = 0;
            try
            {
                infoReceiptDetails.ReceiptMasterId = infoReceiptMaster.ReceiptMasterId;
                infoReceiptDetails.ExtraDate = DateTime.Now;
                infoReceiptDetails.Extra1 = string.Empty;
                infoReceiptDetails.Extra2 = string.Empty;
                //spLedgerPosting.LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId(infoReceiptMaster.VoucherNo, infoReceiptMaster.VoucherTypeId, 12);
                infoReceiptDetails.LedgerId = infoReceiptDetails.LedgerId;
                decLedgerId = infoReceiptDetails.LedgerId;
                infoReceiptDetails.Amount = infoReceiptDetails.Amount;
                Amount = infoReceiptDetails.Amount * decSelectedCurrencyRate;
                decDebit = infoReceiptDetails.Amount * decSelectedCurrencyRate;
                decCredit = infoReceiptDetails.Amount * decSelectedCurrencyRate;
                infoReceiptDetails.ChequeNo = infoReceiptDetails.ChequeNo;
                infoReceiptDetails.ChequeDate = infoReceiptDetails.ChequeDate;
                infoReceiptDetails.ReceiptDetailsId = infoReceiptDetails.ReceiptDetailsId;
                spReceiptDetails.ReceiptDetailsEdit(infoReceiptDetails);

                dt = new DataTable();
                dt = SpPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(5, infoReceiptMaster.VoucherNo, infoReceiptMaster.Date);
                decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());
                TransactionDate = infoReceiptMaster.Date;

                PartyBalanceEdit<ReceiptMasterInfo>(decPartyBalanceId, infoReceiptMaster, newInvoiceNo);

                decCreditNoteDetailsId = infoReceiptDetails.ReceiptDetailsId;
                decimal decLedgerPostId = spLedgerPosting.LedgerPostingIdFromDetailsId(infoReceiptDetails.ReceiptDetailsId, infoReceiptMaster.VoucherNo, 5);


                //LedgerPostingEdit<ReceiptMasterInfo>(Amount, decCreditNoteDetailsId, infoReceiptMaster);

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT35:" + ex.Message;
            }
        }

        public bool JournalMasterEdit(JournalMasterInfo infoJournalMaster, decimal decSelectedCurrencyRate)
        {
            decimal decEffectRow = 0;
            bool isRowAffected = false;
            try
            {
                JournalMasterSP spJournalMaster = new JournalMasterSP();
                /*
                infoReceiptMaster.ReceiptMasterId = infoReceiptMaster.ReceiptMasterId;
                infoReceiptMaster.VoucherNo = infoReceiptMaster.VoucherNo;
                infoReceiptMaster.InvoiceNo = infoReceiptMaster.InvoiceNo;
                infoReceiptMaster.SuffixPrefixId = infoReceiptMaster.SuffixPrefixId;
                infoReceiptMaster.Date = infoReceiptMaster.Date;
                infoReceiptMaster.Narration = infoReceiptMaster.Narration;
                infoReceiptMaster.UserId = infoReceiptMaster.UserId;
                infoReceiptMaster.VoucherTypeId = infoReceiptMaster.VoucherTypeId;
                infoReceiptMaster.FinancialYearId = infoReceiptMaster.FinancialYearId;
                infoReceiptMaster.ExtraDate = infoReceiptMaster.ExtraDate;
                infoReceiptMaster.Extra1 = infoReceiptMaster.Extra1;
                infoReceiptMaster.Extra2 = infoReceiptMaster.Extra2;
                infoReceiptMaster.TotalAmount = infoReceiptMaster.TotalAmount;
                */
                decEffectRow = spJournalMaster.JournalMasterEdit(infoJournalMaster);
            }
            catch (Exception ex) { }
            if (decEffectRow > 0)
            {
                isRowAffected = true;
            }
            return isRowAffected;
        }
        public void JournalDetailsEdit(JournalMasterInfo infoJournalMaster, JournalDetailsInfo infoJournalDetails, bool CreditMasterEdit, decimal decSelectedCurrencyRate, string newInvoiceNo)
        {
            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            decimal decCreditNoteDetailsId = 0;
            decimal decLedgerId = 0;
            decimal decDebit = 0;
            decimal decCredit = 0;
            decimal Amount = 0;
            try
            {
                infoJournalDetails.JournalMasterId = infoJournalMaster.JournalMasterId;
                //spLedgerPosting.LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId(infoReceiptMaster.VoucherNo, infoReceiptMaster.VoucherTypeId, 12);
                decLedgerId = infoJournalDetails.LedgerId;
                //Amount = infoReceiptDetails.Amount * decSelectedCurrencyRate;
                decDebit = infoJournalDetails.Debit * decSelectedCurrencyRate;
                decCredit = infoJournalDetails.Credit * decSelectedCurrencyRate;
                spJournalDetails.JournalDetailsEdit(infoJournalDetails);

                dt = new DataTable();
                dt = SpPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(6, infoJournalMaster.VoucherNo, infoJournalMaster.Date);
                decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());
                TransactionDate = infoJournalMaster.Date;

                PartyBalanceEdit<JournalMasterInfo>(decPartyBalanceId, infoJournalMaster, newInvoiceNo);

                decCreditNoteDetailsId = infoJournalDetails.JournalDetailsId;
                decimal decLedgerPostId = spLedgerPosting.LedgerPostingIdFromDetailsId(infoJournalDetails.JournalDetailsId, infoJournalMaster.VoucherNo, 5);

                //LedgerPostingEdit<ReceiptMasterInfo>(Amount, decCreditNoteDetailsId, infoReceiptMaster);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT35:" + ex.Message;
            }
        }


        public void PartyBalanceEdit<T>(decimal decPartyBalanceId, T PartyBalanceDetails, string newInvoiceNo)
        {
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            dynamic dyn = PartyBalanceDetails;
            try
            {
                InfopartyBalance.PartyBalanceId = decPartyBalanceId;
                InfopartyBalance.CreditPeriod = 0;//
                InfopartyBalance.Date = TransactionDate;
                InfopartyBalance.LedgerId = Convert.ToDecimal(dt.Rows[0]["LedgerId"].ToString());   // dyn.ledgerId; 
                InfopartyBalance.ReferenceType = "Against"; // dt.Rows[0]["ReferenceType"].ToString();
                // ============= the credit note should be against the invoice on which it is being applied ============= //
                dt.Rows[0]["ReferenceType"] = "Against";
                if (dt.Rows[0]["ReferenceType"].ToString() == "New" || dt.Rows[0]["ReferenceType"].ToString() == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0"; //dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = 5;
                    InfopartyBalance.InvoiceNo = dyn.InvoiceNo;
                    InfopartyBalance.VoucherNo = dyn.VoucherNo;
                }
                else
                {
                    InfopartyBalance.ExchangeRateId = Convert.ToDecimal(dt.Rows[0]["OldExchangeRate"].ToString());
                    InfopartyBalance.AgainstInvoiceNo = dyn.InvoiceNo;
                    InfopartyBalance.AgainstVoucherNo = dyn.VoucherNo;
                    InfopartyBalance.AgainstVoucherTypeId = dyn.VoucherTypeId; // 22; // credit note voucher id
                    InfopartyBalance.VoucherTypeId = 19;    // sales invoice voucher id  Convert.ToDecimal(dt.Rows[0]["AgainstVoucherTypeId"].ToString());
                    InfopartyBalance.VoucherNo = newInvoiceNo; // dt.Rows[0]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.InvoiceNo = newInvoiceNo; // dt.Rows[0]["AgainstInvoiceNo"].ToString();
                }

                InfopartyBalance.Credit = dyn.TotalAmount; //Convert.ToDecimal(dt.Rows[0]["Amount"].ToString());
                InfopartyBalance.Debit = 0;

                InfopartyBalance.ExchangeRateId = Convert.ToDecimal(dt.Rows[0]["CurrencyId"].ToString());
                decNewExchangeRateId = InfopartyBalance.ExchangeRateId;
                decOldExchangeId = InfopartyBalance.ExchangeRateId;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                spPartyBalance.PartyBalanceEdit(InfopartyBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT33:" + ex.Message;
            }
        }
        public void LedgerPostingEdit<T>(decimal Amount, decimal decDetailsId, T LedgerPostingDetails)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            dynamic dyn = LedgerPostingDetails;
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;

            dt = new DataTable();
            dt = spLedgerPosting.LedgerPostingDetails((decimal)dyn.VoucherTypeId, Convert.ToString(dyn.VoucherNo));
            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    infoLedgerPosting.LedgerPostingId = Convert.ToDecimal(row["ledgerPostingId"].ToString());
                    infoLedgerPosting.Date = Convert.ToDateTime(row["date"].ToString());
                    infoLedgerPosting.VoucherTypeId = Convert.ToDecimal(row["voucherTypeId"].ToString());
                    infoLedgerPosting.VoucherNo = row["voucherNo"].ToString();
                    infoLedgerPosting.DetailsId = Convert.ToDecimal(row["detailsId"].ToString()) == 0 ? 0 : Convert.ToDecimal(row["detailsId"].ToString());
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = row["invoiceNo"].ToString();
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = Convert.ToDateTime(row["chequeDate"].ToString());
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = Convert.ToDecimal(row["ledgerId"].ToString());
                    infoLedgerPosting.Credit = Convert.ToDecimal(row["credit"].ToString()) == 0 ? 0 : Amount;
                    infoLedgerPosting.Debit = Convert.ToDecimal(row["debit"].ToString()) == 0 ? 0 : Amount;
                    infoLedgerPosting.ExtraDate = DateTime.Now;

                    spLedgerPosting.LedgerPostingEdit(infoLedgerPosting);
                }
                // ---------------------------------- post exchange rate difenrnce to ledger --------------------------- //
                /*
                else
                {
                    infoLedgerPosting.LedgerId = 12;
                    //decNewExchangeRateId = decNewExchangeRateId;
                    decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                    //decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                    decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                    //decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                    decimal decForexAmount = (infoCreditNoteMaster.TotalAmount * decNewExchangeRate) - (infoCreditNoteMaster.TotalAmount * decOldExchange);
                    if (dr["DebitOrCredit"].ToString() == "Dr")
                    {
                        if (decForexAmount >= 0)
                        {

                            infoLedgerPosting.Debit = decForexAmount;
                            infoLedgerPosting.Credit = 0;
                        }
                        else
                        {
                            infoLedgerPosting.Credit = -1 * decForexAmount;
                            infoLedgerPosting.Debit = 0;
                        }
                    }
                    else
                    {
                        if (decForexAmount >= 0)
                        {

                            infoLedgerPosting.Credit = decForexAmount;
                            infoLedgerPosting.Debit = 0;
                        }
                        else
                        {
                            infoLedgerPosting.Debit = -1 * decForexAmount;
                            infoLedgerPosting.Credit = 0;
                        }
                    }
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                */

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "CRNT19:" + ex.Message;
            }
        }
    }
}
