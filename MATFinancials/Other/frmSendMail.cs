﻿


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web.Mail;
using System.Reflection;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;
using MATFinancials.Other;
using MATFinancials.Classes.HelperClasses;

namespace MATFinancials
{
    public partial class frmSendMail : Form
    {
        #region Variables
        frmLoading frmLodingObj = new frmLoading();
        bool isSend = false;
        bool isCheck = false;
        int inBodyCount = 0;
        frmSalesInvoice frmSalesInvoiceObj;
        frmSalesOrder frmSalesOrderObj;
        frmSalesQuotation frmSalesQuotationObj;
        frmPurchaseInvoice frmPurchaseInvoiceObj;
        public static string FileNameToSend { get; set; }
        public static bool HasFileBeenSent { get; set; }

        #endregion
        #region Functions

        public frmSendMail()
        {
            InitializeComponent();
        }

        /// <summary>
        /// public variables.
        /// </summary>

        public string MailFrom { get; set; }
        public string MailPassword { get; set; }

        public void clear()
        {
            try
            {
                txtMailId.Text = string.Empty;
                txtSubjest.Text = string.Empty;
                txtBody.Text = string.Empty;
                inBodyCount = 0;
                getItemFromReport();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM1" + ex.Message;
            }
        }

        public bool ValidateEmail(string recipientsEmailId)
        {
            bool result = true;
            try
            {
                if (recipientsEmailId != string.Empty)
                {
                    System.Text.RegularExpressions.Regex rEMail = new System.Text.RegularExpressions.Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");//^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$
                    if (recipientsEmailId.Length > 0)
                    {
                        if (!rEMail.IsMatch(recipientsEmailId))
                        {
                            MessageBox.Show("Invalid Email", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            result = false;
                            txtMailId.Focus();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM2" + ex.Message;
            }
            return result;
        }

        public void SendAttachmentEmail()
        {
            try
            {
                string mailTo = "", MailFrom = "", MailPassword = "", smtpServer = "", mailTitle = "";
                Int32 portNumber = 0;
                bool enableSSL = true;
                DAL.DBMatConnection db = new DAL.DBMatConnection();
                frmSendMail validateEmail = new frmSendMail();
                DataSet ds = db.getDataSet("invoiceMailDetailsAll");
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    MailFrom = Convert.ToString(ds.Tables[0].Rows[0]["emailID"].ToString());
                    MailPassword = Convert.ToString(ds.Tables[0].Rows[0]["logonPassword"].ToString());
                    smtpServer = ds.Tables[0].Rows[0]["smtp"].ToString();
                    portNumber = Convert.ToInt32(ds.Tables[0].Rows[0]["portNumber"]);
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    isSend = true;
                    MessageBox.Show("Please Ensure to Create an Email Address Before Attempting to Send a Mail..", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                mailTo = txtMailId.Text;
                MailAddress senderAddress = new MailAddress(MailFrom);
                MailAddress receiverAddress = new MailAddress(mailTo);
                System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage(senderAddress, receiverAddress);

                // Cursor.Current = Cursors.WaitCursor;
                // mailMsg.From = "chukwumaprince08@gmail.com";
                // mailMsg.To = "chukwumaprince08@gmail.com";
                // mailMsg.Subject = txtMailId.Text.Trim()+ "--" + txtSubjest.Text.Trim();
                // mailMsg.BodyFormat = MailFormat.Text;
                // mailMsg.Body = txtBody.Text;
                // foreach (string strPath in lstbxAttach.Items)
                // {
                //     MailAttachment attach = new MailAttachment(strPath);
                //     mailMsg.Attachments.Add(attach);
                // }
                // mailMsg.Priority = MailPriority.High;
                // SmtpMail.SmtpServer = "smtp.gmail.com";//smtp is :smtp.gmail.com
                //// SmtpMail.SmtpServer = "plus.smtp.mail.yahoo.com";//smtp is :smtp.yahoo.com
                // mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");
                // mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "");
                // mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "");
                // // - smtp.gmail.com use port 465 or 587
                // // - plus.smtp.mail.yahoo.com use port 465 or 587
                // mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", "465");
                // mailMsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "true");
                // SmtpMail.Send(mailMsg);
                // Cursor.Current = Cursors.Default;


                string message = txtBody.Text;
                string title = txtSubjest.Text;
                string decryptedPassword = new MATFinancials.Classes.Security().base64Decode(MailPassword);

                mailMsg.Body = message;
                mailMsg.Subject = title;
                if(txtCc.Text != string.Empty && validateEmail.ValidateEmail(txtCc.Text.ToString()))
                {
                    MailAddress copy = new MailAddress(txtCc.Text.ToString());
                    mailMsg.CC.Add(copy);
                }

                foreach (string strPath in lstbxAttach.Items)
                {
                    //MailAttachment attach = new MailAttachment(strPath);
                    System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(strPath);
                    mailMsg.Attachments.Add(attach);
                }

                var client = new SmtpClient()
                {
                    Host = smtpServer,
                    Port = portNumber,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(MailFrom, decryptedPassword),
                    EnableSsl = enableSSL,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Timeout = 20000
                };

                // client.Send("chukwumaprince08@gmai.com", mailTo, title, message);  
                client.Send(mailMsg);
                isSend = true;
                isCheck = true;
                MessageBox.Show("Mail sent successfully ", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                isSend = true;
                MessageBox.Show("Mail sending failed", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void CallEmailConfiguration()
        {
            try
            {
                if (CheckUserPrivilege.PrivilegeCheck(PublicVariables._decCurrentUserId, "frmEmailConfiguration", "View"))
                {
                    frmEmailConfiguration objEmail = new frmEmailConfiguration();
                    frmEmailConfiguration open = Application.OpenForms["frmEmailConfiguration"] as frmEmailConfiguration;
                    if (open == null)
                    {
                        objEmail.MdiParent = this;
                        objEmail.Show();
                    }
                    else
                    {
                        open.Activate();
                        if (open.WindowState == FormWindowState.Minimized)
                        {
                            open.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Messages.NoPrivillageMessage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void SendMail()
        {
            try
            {
                if (ValidateEmail(txtMailId.Text.Trim()))
                {

                    if (txtSubjest.Text == string.Empty)
                    {
                        if (MessageBox.Show("Send this message without a subject ?", "MAT Financials", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                        {
                            if (txtBody.Text == string.Empty)
                            {
                                if (MessageBox.Show("Send this message without text in the body ?", "MAT Financials", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                                {
                                    bwrk1.RunWorkerAsync();
                                    frmLodingObj.ShowFromSendMail();
                                }
                                else
                                {
                                    txtBody.Focus();
                                }
                            }
                            else
                            {
                                bwrk1.RunWorkerAsync();
                                frmLodingObj.ShowFromSendMail();
                            }
                        }
                        else
                        {
                            txtSubjest.Focus();
                        }
                    }
                    else
                    {
                        if (txtBody.Text == string.Empty)
                        {
                            if (MessageBox.Show("Send this message without text in the body ?", "MAT Financials", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                bwrk1.RunWorkerAsync();
                                frmLodingObj.ShowFromSendMail();
                            }
                            else
                            {
                                txtBody.Focus();
                            }
                        }
                        else
                        {
                            bwrk1.RunWorkerAsync();
                            frmLodingObj.ShowFromSendMail();
                        }
                    }
                }
                if (isCheck)
                {
                    clear();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM3" + ex.Message;
            }
        }

        private void getItemFromReport()
        {
            try
            {
                frmReport rpt = new frmReport();
                //if (rpt.ShowDialog() == DialogResult.OK)
                //{
                //lstbxAttach.Items.Add(rpt.getItem());
                if (!string.IsNullOrEmpty(frmSendMail.FileNameToSend))
                    lstbxAttach.Items.Add(frmSendMail.FileNameToSend);
                //}
                rpt.Close();
                rpt.Dispose();
            }
            catch (Exception ex)
            {
                string log = ex.Message;
            }
        }
        #endregion
        #region Events
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog folderB = new OpenFileDialog();
                folderB.ShowDialog();
                if (folderB.FileName.ToString() != string.Empty)
                {
                    lstbxAttach.Items.Add(folderB.FileName.ToString());
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM4" + ex.Message;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM5" + ex.Message;
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMailId.Text == string.Empty)
                {
                    MessageBox.Show("Enter your Email id to send mail", "MAT Financials", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    txtMailId.Focus();
                }
                else
                {
                    SendMail();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM6" + ex.Message;
            }

        }

        private void frmSendMail_Load(object sender, EventArgs e)
        {
            try
            {
                // frmSendMail.FileNameToSend = string.Empty;
                // frmSendMail.HasFileBeenSent = false;
                clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM7" + ex.Message;
            }
        }

        private void bwrk1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                SendAttachmentEmail();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM8" + ex.Message;
            }
        }

        private void bwrk1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (frmLodingObj != null && isSend)
                {
                    frmLodingObj.Close();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM9" + ex.Message;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (Messages.CloseConfirmation())
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM10" + ex.Message;
                throw;
            }
        }
        #endregion
        #region Navigation
        private void txtMailId_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtSubjest.Focus();
                    txtSubjest.SelectionStart = 0;
                    txtSubjest.SelectionLength = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM11" + ex.Message;
            }
        }
        private void txtSubjest_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtBody.Focus();
                }
                else if (e.KeyCode == Keys.Back)
                {
                    if (txtSubjest.Text.Trim() == string.Empty || txtSubjest.SelectionStart == 0)
                    {
                        txtMailId.SelectionStart = 0;
                        txtMailId.SelectionLength = 0;
                        txtMailId.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM12" + ex.Message;
            }
        }

        private void txtBody_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {


                if (e.KeyChar == 13)
                {
                    inBodyCount++;
                    if (inBodyCount == 3)
                    {
                        inBodyCount = 0;
                        btnSend.Focus();
                    }
                }
                else
                {
                    inBodyCount = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM13" + ex.Message;
            }
        }
        #endregion
        public void CallFromSalesInvoice(frmSalesInvoice frmSalesInvoice, invoiceEmailDetails invoiceEmailDetails)
        {
            try
            {
                frmSalesInvoice.Enabled = false;
                this.frmSalesInvoiceObj = frmSalesInvoice;
                base.Show();
                txtMailId.Text = invoiceEmailDetails.mailTo;
                txtSubjest.Text = invoiceEmailDetails.mailTitle;
                lstbxAttach.Items.Add(invoiceEmailDetails.attachment);
                txtBody.Text = invoiceEmailDetails.message; 
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM14:" + ex.Message;
            }
        }
        public void CallFromPurchaseInvoice(frmPurchaseInvoice frmPurchaseInvoice, invoiceEmailDetails invoiceEmailDetails)
        {
            try
            {
                frmPurchaseInvoice.Enabled = false;
                this.frmPurchaseInvoiceObj = frmPurchaseInvoice;
                base.Show();
                txtMailId.Text = invoiceEmailDetails.mailTo;
                txtSubjest.Text = invoiceEmailDetails.mailTitle;
                lstbxAttach.Items.Add(invoiceEmailDetails.attachment);
                txtBody.Text = invoiceEmailDetails.message;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM15:" + ex.Message;
            }
        }

        private void frmSendMail_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (frmSalesInvoiceObj != null)
                {
                    frmSalesInvoiceObj.ReturnFromSendMailForm();
                    frmSalesInvoiceObj.Enabled = true;
                }
                if (frmPurchaseInvoiceObj != null)
                {
                    frmPurchaseInvoiceObj.ReturnFromSendMailForm();
                    frmPurchaseInvoiceObj.Enabled = true;
                }
                if (frmSalesQuotationObj != null)
                {
                    frmSalesQuotationObj.ReturnFromSendMailForm();
                    frmSalesQuotationObj.Enabled = true;
                }
                if (frmSalesOrderObj != null)
                {
                    frmSalesOrderObj.ReturnFromSendMailForm();
                    frmSalesOrderObj.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM16:" + ex.Message;
            }
        }
        public void CallFromSalesQuotation(frmSalesQuotation frmSalesQuotation, invoiceEmailDetails invoiceEmailDetails)
        {
            try
            {
                frmSalesQuotation.Enabled = false;
                this.frmSalesQuotationObj = frmSalesQuotation;
                base.Show();
                txtMailId.Text = invoiceEmailDetails.mailTo;
                txtSubjest.Text = invoiceEmailDetails.mailTitle;
                lstbxAttach.Items.Add(invoiceEmailDetails.attachment);
                txtBody.Text = invoiceEmailDetails.message;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM17:" + ex.Message;
            }
        }
        public void CallFromSalesOrder(frmSalesOrder frmSalesOrder, invoiceEmailDetails invoiceEmailDetails)
        {
            try
            {
                frmSalesOrder.Enabled = false;
                this.frmSalesOrderObj = frmSalesOrder;
                base.Show();
                txtMailId.Text = invoiceEmailDetails.mailTo;
                txtSubjest.Text = invoiceEmailDetails.mailTitle;
                lstbxAttach.Items.Add(invoiceEmailDetails.attachment);
                txtBody.Text = invoiceEmailDetails.message;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SM18:" + ex.Message;
            }
        }
    }
}
