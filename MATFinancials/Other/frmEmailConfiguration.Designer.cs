﻿namespace MATFinancials.Other
{
    partial class frmEmailConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailConfiguration));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmailId = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.emailConfigPanel = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbMailProvider = new System.Windows.Forms.ComboBox();
            this.lblMailProvider = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkEnableSSL = new System.Windows.Forms.CheckBox();
            this.lblPortNumber = new System.Windows.Forms.Label();
            this.txtPortNumber = new System.Windows.Forms.TextBox();
            this.lblsmtp = new System.Windows.Forms.Label();
            this.txtSmtp = new System.Windows.Forms.TextBox();
            this.chkIdDefault = new System.Windows.Forms.CheckBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvEmailConfiguration = new System.Windows.Forms.DataGridView();
            this.SNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.smtp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.portNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enableSSL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailConfigPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmailConfiguration)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(503, 145);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 27);
            this.btnClose.TabIndex = 41;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(426, 145);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(76, 27);
            this.btnClear.TabIndex = 39;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(359, 145);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 27);
            this.btnSave.TabIndex = 38;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(9, 41);
            this.lblName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(46, 13);
            this.lblName.TabIndex = 396;
            this.lblName.Text = "Email ID";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 73);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 397;
            this.label1.Text = "Password";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtEmailId
            // 
            this.txtEmailId.Location = new System.Drawing.Point(103, 38);
            this.txtEmailId.Name = "txtEmailId";
            this.txtEmailId.Size = new System.Drawing.Size(249, 20);
            this.txtEmailId.TabIndex = 398;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(103, 67);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = 'x';
            this.txtPassword.Size = new System.Drawing.Size(249, 20);
            this.txtPassword.TabIndex = 399;
            // 
            // emailConfigPanel
            // 
            this.emailConfigPanel.Controls.Add(this.label8);
            this.emailConfigPanel.Controls.Add(this.cmbMailProvider);
            this.emailConfigPanel.Controls.Add(this.lblMailProvider);
            this.emailConfigPanel.Controls.Add(this.label7);
            this.emailConfigPanel.Controls.Add(this.label6);
            this.emailConfigPanel.Controls.Add(this.label5);
            this.emailConfigPanel.Controls.Add(this.label4);
            this.emailConfigPanel.Controls.Add(this.label3);
            this.emailConfigPanel.Controls.Add(this.chkEnableSSL);
            this.emailConfigPanel.Controls.Add(this.lblPortNumber);
            this.emailConfigPanel.Controls.Add(this.txtPortNumber);
            this.emailConfigPanel.Controls.Add(this.lblsmtp);
            this.emailConfigPanel.Controls.Add(this.txtSmtp);
            this.emailConfigPanel.Controls.Add(this.chkIdDefault);
            this.emailConfigPanel.Controls.Add(this.btnDelete);
            this.emailConfigPanel.Controls.Add(this.txtConfirmPassword);
            this.emailConfigPanel.Controls.Add(this.label2);
            this.emailConfigPanel.Controls.Add(this.txtPassword);
            this.emailConfigPanel.Controls.Add(this.btnClose);
            this.emailConfigPanel.Controls.Add(this.lblName);
            this.emailConfigPanel.Controls.Add(this.btnClear);
            this.emailConfigPanel.Controls.Add(this.btnSave);
            this.emailConfigPanel.Controls.Add(this.txtEmailId);
            this.emailConfigPanel.Controls.Add(this.label1);
            this.emailConfigPanel.Location = new System.Drawing.Point(206, 31);
            this.emailConfigPanel.Name = "emailConfigPanel";
            this.emailConfigPanel.Size = new System.Drawing.Size(694, 187);
            this.emailConfigPanel.TabIndex = 400;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(671, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 13);
            this.label8.TabIndex = 1099;
            this.label8.Text = "*";
            // 
            // cmbMailProvider
            // 
            this.cmbMailProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMailProvider.FormattingEnabled = true;
            this.cmbMailProvider.Items.AddRange(new object[] {
            "Gmail",
            "Yahoo",
            "Hotmail"});
            this.cmbMailProvider.Location = new System.Drawing.Point(470, 38);
            this.cmbMailProvider.Name = "cmbMailProvider";
            this.cmbMailProvider.Size = new System.Drawing.Size(195, 21);
            this.cmbMailProvider.TabIndex = 1098;
            // 
            // lblMailProvider
            // 
            this.lblMailProvider.AutoSize = true;
            this.lblMailProvider.Location = new System.Drawing.Point(386, 40);
            this.lblMailProvider.Name = "lblMailProvider";
            this.lblMailProvider.Size = new System.Drawing.Size(68, 13);
            this.lblMailProvider.TabIndex = 1097;
            this.lblMailProvider.Text = "Mail Provider";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(671, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 13);
            this.label7.TabIndex = 1096;
            this.label7.Text = "*";
            this.label7.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(671, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 1095;
            this.label6.Text = "*";
            this.label6.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(356, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 1094;
            this.label5.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(356, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 1093;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(356, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 1092;
            this.label3.Text = "*";
            // 
            // chkEnableSSL
            // 
            this.chkEnableSSL.AutoSize = true;
            this.chkEnableSSL.Location = new System.Drawing.Point(470, 105);
            this.chkEnableSSL.Name = "chkEnableSSL";
            this.chkEnableSSL.Size = new System.Drawing.Size(82, 17);
            this.chkEnableSSL.TabIndex = 410;
            this.chkEnableSSL.Text = "Enable SSL";
            this.chkEnableSSL.UseVisualStyleBackColor = true;
            this.chkEnableSSL.Visible = false;
            // 
            // lblPortNumber
            // 
            this.lblPortNumber.AutoSize = true;
            this.lblPortNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblPortNumber.ForeColor = System.Drawing.Color.Black;
            this.lblPortNumber.Location = new System.Drawing.Point(383, 85);
            this.lblPortNumber.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblPortNumber.Name = "lblPortNumber";
            this.lblPortNumber.Size = new System.Drawing.Size(43, 13);
            this.lblPortNumber.TabIndex = 408;
            this.lblPortNumber.Text = "Port No";
            this.lblPortNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPortNumber.Visible = false;
            // 
            // txtPortNumber
            // 
            this.txtPortNumber.Location = new System.Drawing.Point(432, 82);
            this.txtPortNumber.Name = "txtPortNumber";
            this.txtPortNumber.Size = new System.Drawing.Size(233, 20);
            this.txtPortNumber.TabIndex = 409;
            this.txtPortNumber.Visible = false;
            // 
            // lblsmtp
            // 
            this.lblsmtp.AutoSize = true;
            this.lblsmtp.BackColor = System.Drawing.Color.Transparent;
            this.lblsmtp.ForeColor = System.Drawing.Color.Black;
            this.lblsmtp.Location = new System.Drawing.Point(383, 64);
            this.lblsmtp.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblsmtp.Name = "lblsmtp";
            this.lblsmtp.Size = new System.Drawing.Size(37, 13);
            this.lblsmtp.TabIndex = 405;
            this.lblsmtp.Text = "SMTP";
            this.lblsmtp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblsmtp.Visible = false;
            // 
            // txtSmtp
            // 
            this.txtSmtp.Location = new System.Drawing.Point(432, 61);
            this.txtSmtp.Name = "txtSmtp";
            this.txtSmtp.Size = new System.Drawing.Size(233, 20);
            this.txtSmtp.TabIndex = 407;
            this.txtSmtp.Visible = false;
            // 
            // chkIdDefault
            // 
            this.chkIdDefault.AutoSize = true;
            this.chkIdDefault.Location = new System.Drawing.Point(103, 128);
            this.chkIdDefault.Name = "chkIdDefault";
            this.chkIdDefault.Size = new System.Drawing.Size(71, 17);
            this.chkIdDefault.TabIndex = 404;
            this.chkIdDefault.Text = "Is Default";
            this.chkIdDefault.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(588, 145);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(77, 27);
            this.btnDelete.TabIndex = 402;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmPassword.Location = new System.Drawing.Point(103, 102);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = 'x';
            this.txtConfirmPassword.Size = new System.Drawing.Size(249, 20);
            this.txtConfirmPassword.TabIndex = 401;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(9, 105);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 400;
            this.label2.Text = "Confirm Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvEmailConfiguration
            // 
            this.dgvEmailConfiguration.AllowUserToAddRows = false;
            this.dgvEmailConfiguration.AllowUserToDeleteRows = false;
            this.dgvEmailConfiguration.AllowUserToResizeColumns = false;
            this.dgvEmailConfiguration.AllowUserToResizeRows = false;
            this.dgvEmailConfiguration.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmailConfiguration.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvEmailConfiguration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmailConfiguration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SNO,
            this.emailId,
            this.IsDefault,
            this.smtp,
            this.portNumber,
            this.enableSSL});
            this.dgvEmailConfiguration.Location = new System.Drawing.Point(206, 224);
            this.dgvEmailConfiguration.Name = "dgvEmailConfiguration";
            this.dgvEmailConfiguration.ReadOnly = true;
            this.dgvEmailConfiguration.Size = new System.Drawing.Size(694, 133);
            this.dgvEmailConfiguration.TabIndex = 403;
            this.dgvEmailConfiguration.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmailConfiguration_CellContentDoubleClick);
            this.dgvEmailConfiguration.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmailConfiguration_CellDoubleClick);
            // 
            // SNO
            // 
            this.SNO.DataPropertyName = "SNO";
            this.SNO.FillWeight = 60.16294F;
            this.SNO.HeaderText = "S|No";
            this.SNO.Name = "SNO";
            this.SNO.ReadOnly = true;
            this.SNO.Visible = false;
            // 
            // emailId
            // 
            this.emailId.DataPropertyName = "emailId";
            this.emailId.FillWeight = 163.6949F;
            this.emailId.HeaderText = "EMAIL ID";
            this.emailId.Name = "emailId";
            this.emailId.ReadOnly = true;
            // 
            // IsDefault
            // 
            this.IsDefault.DataPropertyName = "IsDefault";
            this.IsDefault.FillWeight = 76.14214F;
            this.IsDefault.HeaderText = "IS DEFAULT";
            this.IsDefault.Name = "IsDefault";
            this.IsDefault.ReadOnly = true;
            // 
            // smtp
            // 
            this.smtp.DataPropertyName = "smtp";
            this.smtp.HeaderText = "SMTP";
            this.smtp.Name = "smtp";
            this.smtp.ReadOnly = true;
            this.smtp.Visible = false;
            // 
            // portNumber
            // 
            this.portNumber.DataPropertyName = "portNumber";
            this.portNumber.HeaderText = "PORT NUMBER";
            this.portNumber.Name = "portNumber";
            this.portNumber.ReadOnly = true;
            this.portNumber.Visible = false;
            // 
            // enableSSL
            // 
            this.enableSSL.DataPropertyName = "enableSSL";
            this.enableSSL.HeaderText = "ENABLE SSL";
            this.enableSSL.Name = "enableSSL";
            this.enableSSL.ReadOnly = true;
            this.enableSSL.Visible = false;
            // 
            // frmEmailConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 461);
            this.Controls.Add(this.dgvEmailConfiguration);
            this.Controls.Add(this.emailConfigPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEmailConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Configuration";
            this.Load += new System.EventHandler(this.frmEmailConfiguration_Load);
            this.emailConfigPanel.ResumeLayout(false);
            this.emailConfigPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmailConfiguration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmailId;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Panel emailConfigPanel;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvEmailConfiguration;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox chkIdDefault;
        private System.Windows.Forms.Label lblsmtp;
        private System.Windows.Forms.TextBox txtSmtp;
        private System.Windows.Forms.Label lblPortNumber;
        private System.Windows.Forms.TextBox txtPortNumber;
        private System.Windows.Forms.CheckBox chkEnableSSL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbMailProvider;
        private System.Windows.Forms.Label lblMailProvider;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailId;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn smtp;
        private System.Windows.Forms.DataGridViewTextBoxColumn portNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn enableSSL;
    }
}