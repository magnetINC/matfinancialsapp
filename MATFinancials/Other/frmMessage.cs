﻿
 
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MATFinancials
{
    public partial class frmMessage : Form
    {
        /// <summary>
        /// Creates an instance of frmMessage class
        /// </summary>
        public frmMessage()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Closes this form on 'Close' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "msg1:" + ex.Message;
            }
        }
    }
}
