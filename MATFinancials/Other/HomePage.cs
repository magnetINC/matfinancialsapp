﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Other
{
    public partial class frmHomePage : Form
    {
        string calculationMethod = string.Empty;
        bool showLedgerBalances = true;
        public frmHomePage()
        {
            InitializeComponent();
        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            // Check whether user has right to view financial balances on the home page
            if (CheckUserPrivilege.PrivilegeCheck(PublicVariables._decCurrentUserId, "frmAccountLedgersonHomePage" /*this.Name*/, "View"))
            {
                panelBalances.Visible = true;
            }
            else
            {
                showLedgerBalances = false;
                //panelBalances.Visible = false;
                //dgvHomeGrid.Visible = false;
                txtFromDate.Visible = false;
                dtpFromDate.Visible = false;
                txtToDate.Visible = false;
                label14.Visible = false;
                label6.Visible = false;
                label3.Visible = false;
            }
            GridFill();
        }
        protected override void WndProc(ref Message message)
        {
            //Code to make the form not movable is commented to make the form movable

            //const int WM_SYSCOMMAND = 0x0112;
            //const int SC_MOVE = 0xF010;

            //switch (message.Msg)
            //{
            //    case WM_SYSCOMMAND:
            //        int command = message.WParam.ToInt32() & 0xfff0;
            //        if (command == SC_MOVE)
            //            return;
            //        break;
            //}

            base.WndProc(ref message);
        }

        public void GridFill()
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpToDate.MinDate = PublicVariables._dtFromDate;
                dtpToDate.MaxDate = PublicVariables._dtToDate;
                dtpToDate.Value = PublicVariables._dtToDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpToDate.Text = dtpToDate.Value.ToString("dd-MMM-yyyy");

                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");

                DateValidation objvalidation = new DateValidation();
                objvalidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }

                DataTable dtbl = new DataTable();
                AccountLedgerSP sp = new AccountLedgerSP();
                string vt = "";
                dtbl = sp.HomePageTransactionGridFill(/*Convert.ToDateTime(txtFromDate.Text),*/ Convert.ToDateTime(txtToDate.Text));
                foreach(DataRow dr in dtbl.Rows)
                {
                    vt = dr["ledgerName"].ToString();
                    if (vt == "Account Payables")
                    {
                        dgvHomeGrid.Rows.Add();
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["GroupName"].Value = "SUPPLIERS";
                                              
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = dr["accountGroupId"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtPerticulars"].Value = dr["ledgerName"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtbalance"].Value = (Convert.ToDecimal(dr["Balance"])).ToString("N2"); 
                    }
                    else if (vt == "Account Receivables")
                    {
                        dgvHomeGrid.Rows.Add();
                     
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["GroupName"].Value = "CUSTOMERS";

                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = dr["accountGroupId"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtPerticulars"].Value = dr["ledgerName"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtbalance"].Value = (Convert.ToDecimal(dr["Balance"])).ToString("N2");
                    }
                }
                dgvHomeGrid.Rows.Add();
                dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["GroupName"].Value = "BANKS";
                foreach (DataRow dr in dtbl.Rows)
                {
                    if((dr["ledgerName"].ToString()) != "Account Receivables" && (dr["ledgerName"].ToString()) != "Account Payables")
                    {
                        dgvHomeGrid.Rows.Add();
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = dr["accountGroupId"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtPerticulars"].Value = dr["ledgerName"];
                        dgvHomeGrid.Rows[dgvHomeGrid.Rows.Count - 1].Cells["dgvtxtbalance"].Value = (Convert.ToDecimal(dr["Balance"])).ToString("N2");

                    }
                }
                if(!showLedgerBalances)
                {
                    for(int i = 0; i< dgvHomeGrid.Rows.Count; i++)
                    {
                        dgvHomeGrid.Columns["dgvtxtbalance"].Visible = false;
                        //DataGridViewBand band = dgvHomeGrid.Rows[i];
                        //band.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("HP"+ ex.Message);
            }
        }
                
        private void lblSalesQuotation_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalesQuotation frm = new frmSalesQuotation();
                frmSalesQuotation open = Application.OpenForms["frmSalesQuotation"] as frmSalesQuotation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();                   
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void lblSalesInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalesInvoice frm = new frmSalesInvoice();
                frmSalesInvoice open = Application.OpenForms["frmSalesInvoice"] as frmSalesInvoice;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblReceiptPayment_Click(object sender, EventArgs e)
        {
            try
            {
                frmReceiptVoucher frm = new frmReceiptVoucher();
                frmReceiptVoucher open = Application.OpenForms["frmReceiptVoucher"] as frmReceiptVoucher;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblPurchaseOrder_Click(object sender, EventArgs e)
        {            
            try
            {
                frmPurchaseOrder frm = new frmPurchaseOrder();
                frmPurchaseOrder open = Application.OpenForms["frmPurchaseOrder"] as frmPurchaseOrder;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblReceiveStock_Click(object sender, EventArgs e)
        {                    

            try
            {
                frmMaterialReceipt frm = new frmMaterialReceipt();
                frmMaterialReceipt open = Application.OpenForms["frmMaterialReceipt"] as frmMaterialReceipt;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblPurchaseInvoice_Click(object sender, EventArgs e)
        {           

            try
            {
                frmPurchaseInvoice frm = new frmPurchaseInvoice();
                frmPurchaseInvoice open = Application.OpenForms["frmPurchaseInvoice"] as frmPurchaseInvoice;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblBillPayments_Click(object sender, EventArgs e)
        {         

            try
            {
                frmPaymentVoucher frm = new frmPaymentVoucher();
                frmPaymentVoucher open = Application.OpenForms["frmPaymentVoucher"] as frmPaymentVoucher;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblProductService_Click(object sender, EventArgs e)
        {
            
            try
            {
                frmProductCreation frm = new frmProductCreation();
                frmProductCreation open = Application.OpenForms["frmProductCreation"] as frmProductCreation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {
            try
            {
                frmAccountLedger frm = new frmAccountLedger();
                frmAccountLedger open = Application.OpenForms["frmAccountLedger"] as frmAccountLedger;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            try
            {
                frmSalesInvoice frm = new frmSalesInvoice();
                frmSalesInvoice open = Application.OpenForms["frmSalesInvoice"] as frmSalesInvoice;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                frmSalesQuotation frm = new frmSalesQuotation();
                frmSalesQuotation open = Application.OpenForms["frmSalesQuotation"] as frmSalesQuotation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                frmReceiptVoucher frm = new frmReceiptVoucher();
                frmReceiptVoucher open = Application.OpenForms["frmReceiptVoucher"] as frmReceiptVoucher;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {
           
            try
            {
                frmBankReconciliation frm = new frmBankReconciliation();
                frmBankReconciliation open = Application.OpenForms["frmBankReconciliation"] as frmBankReconciliation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            

            try
            {
                frmBankReconciliation frm = new frmBankReconciliation();
                frmBankReconciliation open = Application.OpenForms["frmBankReconciliation"] as frmBankReconciliation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {
            try
            {
                frmPaySlip objPaySlip = new frmPaySlip();
                frmPaySlip open = Application.OpenForms["frmPaySlip"] as frmPaySlip;
                if (open == null)
                {
                    objPaySlip.MdiParent = formMDI.ActiveForm;
                    objPaySlip.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            try
            {
                frmPaySlip objPaySlip = new frmPaySlip();
                frmPaySlip open = Application.OpenForms["frmPaySlip"] as frmPaySlip;
                if (open == null)
                {
                    objPaySlip.MdiParent = formMDI.ActiveForm;
                    objPaySlip.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            try
            {
                frmBonusDeduction frm = new frmBonusDeduction();
                frmBonusDeduction open = Application.OpenForms["frmBonusDeduction"] as frmBonusDeduction;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            try
            {
                frmBonusDeduction frm = new frmBonusDeduction();
                frmBonusDeduction open = Application.OpenForms["frmBonusDeduction"] as frmBonusDeduction;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label17_Click(object sender, EventArgs e)
        {
            try
            {
                frmEmployeeCreation frm = new frmEmployeeCreation();
                frmEmployeeCreation open = Application.OpenForms["frmEmployeeCreation"] as frmEmployeeCreation;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label19_Click(object sender, EventArgs e)
        {
            try
            {
                frmPayHead frm = new frmPayHead();
                frmPayHead open = Application.OpenForms["frmPayHead"] as frmPayHead;
                if (open == null)
                {
                    frm.MdiParent = formMDI.ActiveForm;
                    frm.Show();
                }
                else
                {
                    open.Activate();
                    if (open.WindowState == FormWindowState.Minimized)
                    {
                        open.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
