﻿using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Other
{
    public partial class frmAvailableQuantity : Form
    {
        decimal productId = 0;
        frmPurchaseInvoice frmPurchaseInvoiceObj = null;
        frmSalesInvoice frmSalesInvoiceObj = null;
        frmDeliveryNote frmDeliveryNoteObj = null;
        frmMaterialReceipt frmMaterialReceiptObj = null;
        frmRejectionIn frmRejectionInObj = null;
        frmSalesReturn frmSalesReturnObj = null;
        frmRejectionOut frmRejectionOutObj = null;
        frmPurchaseReturn frmPurchaseReurnObj = null;
        frmSalesQuotation frmSalesQuotationObj = null;
        frmPurchaseOrder frmPurchaseOrderObj = null;
        frmSalesOrder frmSalesOrderObj = null;
        public frmAvailableQuantity()
        {
            InitializeComponent();
        }

        private void frmAvailableQuantity_Load(object sender, EventArgs e)
        {

            GridFill();
        }


        public void GridFill()
        {
            try
            {
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();
                PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
                PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
                DBMatConnection conn = new DBMatConnection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable(), dt2 = new DataTable();
                DataTable dtbl = new DataTable(), dtbl2 = new DataTable();

                decimal salesOrderBalance = 0, purchaseOrderBalance = 0;

                dt = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(0, 0, 0);
                dt2 = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(0, 0, 0);
                dtbl = SalaryHelper.salesOrderDetails();
                dtbl2 = SalaryHelper.purchaseOrderDetails();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow row = dtbl.NewRow();
                        row["salesOrderMasterId"] = Convert.ToDecimal(dt.Rows[i]["salesOrderMasterId"]);
                        dtbl.Rows.Add(row);
                    }
                    // get balances remaining for all the available sales order for the item
                    foreach (DataRow row in dt.Rows)
                    {
                        DataTable dtblDetails = new DataTable();
                        dtblDetails = spSalesOrderDetails.SalesInvoiceGridfillAgainestSalesOrderUsingSalesDetails
                           (Convert.ToDecimal(row["salesOrderMasterId"]), 0, 0);
                        salesOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == PublicVariables.productId)
                            .Select(i => i.Field<decimal>("qty")).Sum());
                    }
                }
                
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        DataRow row = dtbl2.NewRow();
                        row["purchaseOrderMasterId"] = Convert.ToDecimal(dt2.Rows[i]["purchaseOrderMasterId"]);
                        dtbl2.Rows.Add(row);
                    }
                    // get balances remaining for all the available purchase order for the item
                    foreach (DataRow row in dt2.Rows)
                    {
                        DataTable dtblDetails = new DataTable();
                        dtblDetails = spPurchaseOrderDetails.PurchaseOrderDetailsViewByOrderMasterIdWithRemainingByNotInCurrPI
                                   (Convert.ToDecimal(row["purchaseOrderMasterId"]), 0, 0);
                        purchaseOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == PublicVariables.productId)
                            .Select(i => i.Field<decimal>("qty")).Sum());
                    }
                }
                productId = PublicVariables.productId;
                conn.AddParameter("@productId", productId);
                conn.AddParameter("@date", PublicVariables._dtToDate);
                conn.AddParameter("@salesArrayList", dtbl);
                conn.AddParameter("@purchaseArrayList", dtbl2);
                ds = conn.getDataSet("CurrentStockQuickView");

                if (ds != null && ds.Tables.Count > 1)
                {
                    dgvAvailableQuantity.DataSource = ds.Tables[0];
                    dgvSalesOrder.DataSource = ds.Tables[1];
                    dgvpurchaseOrder.DataSource = ds.Tables[2];
                    lblItemName.Text = ds.Tables[0].Rows[0]["productName"].ToString();
                    lblItemCode.Text = ds.Tables[0].Rows[0]["productCode"].ToString();
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    decimal qtyOnHand = (from i in ds.Tables[0].AsEnumerable()
                                         select i.Field<decimal>("qtyBal")).Sum();
                    decimal qtyOnSalesOrder = salesOrderBalance; // (from i in ds.Tables[1].AsEnumerable()select i.Field<decimal>("salesOrder")).Sum();
                    decimal qtyOnPurchaseOrder = purchaseOrderBalance; // (from i in ds.Tables[2].AsEnumerable()select i.Field<decimal>("purchaseOrder")).Sum();
                    lblQtyonHand2.Text = qtyOnHand.ToString("N2");
                    lblQtyonSalesOrder2.Text = qtyOnSalesOrder.ToString("N2");
                    decimal AvailableQuantity = qtyOnHand - qtyOnSalesOrder;
                    lblQtyAvailable2.Text = AvailableQuantity.ToString("N2");
                    lblQtyOnPurchaseOrder2.Text = qtyOnPurchaseOrder.ToString("N2");
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ10" + ex.Message;
            }
        }
        public void CallFromPurchaseInvoice(frmPurchaseInvoice frmPurchaseInvoiceObj)
        {
            try
            {
                //frmPurchaseInvoiceObj.Enabled = false;
                this.frmPurchaseInvoiceObj = frmPurchaseInvoiceObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:01" + ex.Message;
            }
        }

        public void CallFromSalesInvoice(frmSalesInvoice frmSalesInvoice)
        {
            try
            {
                this.frmSalesInvoiceObj = frmSalesInvoice;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ02" + ex.Message;
            }
        }
        public void CallFromSalesQuotation(frmSalesQuotation frmSalesQuotation)
        {
            try
            {
                this.frmSalesQuotationObj = frmSalesQuotation;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ09" + ex.Message;
            }
        }
        public void CallFromDeliveryNote(frmDeliveryNote frmDeliveryNoteObj)
        {
            try
            {
                this.frmDeliveryNoteObj = frmDeliveryNoteObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:03" + ex.Message;
            }
        }
        public void CallFromPurchaseOrder(frmPurchaseOrder frmPurchaseOrderObj)
        {
            try
            {
                this.frmPurchaseOrderObj = frmPurchaseOrderObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:03" + ex.Message;
            }
        }
        public void CallFromSalesOrder(frmSalesOrder frmSalesOrderObj)
        {
            try
            {
                this.frmSalesOrderObj = frmSalesOrderObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:03" + ex.Message;
            }
        }

        public void CallFromRejectionIn(frmRejectionIn frmRejectionInObj)
        {
            try
            {
                this.frmRejectionInObj = frmRejectionInObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:04" + ex.Message;
            }
        }
        public void CallFromSalesReturn(frmSalesReturn frmSalesReturnObj)
        {
            try
            {
                this.frmSalesReturnObj = frmSalesReturnObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:05" + ex.Message;
            }
        }

        public void CallFromMaterialReceipt(frmMaterialReceipt frmMaterialReceiptObj)
        {
            try
            {
                this.frmMaterialReceiptObj = frmMaterialReceiptObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:06" + ex.Message;
            }
        }

        public void CallFromRejectionOut(frmRejectionOut frmRejectionOutObj)
        {
            try
            {
                this.frmRejectionOutObj = frmRejectionOutObj;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:07" + ex.Message;
            }
        }

        public void CallFromPurchaseReturn(frmPurchaseReturn frmPurchaseReturn)
        {
            try
            {
                this.frmPurchaseReurnObj = frmPurchaseReturn;
                base.Show();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ:08" + ex.Message;
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AQ00:" + ex.Message;
            }
        }

        private void dgvAvailableQuantity_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
