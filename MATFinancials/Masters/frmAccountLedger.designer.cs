﻿namespace MATFinancials
{
    partial class frmAccountLedger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountLedger));
            this.btnSearch = new System.Windows.Forms.Button();
            this.dgvAccountLedger = new System.Windows.Forms.DataGridView();
            this.dgvtxtCreditOrDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SLNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtLedgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ledgerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtLedger = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtLedgerNameSearch = new System.Windows.Forms.TextBox();
            this.lblLedgerNameSearch = new System.Windows.Forms.Label();
            this.cmbGroupSearch = new System.Windows.Forms.ComboBox();
            this.lblGroupSearch = new System.Windows.Forms.Label();
            this.tbSecondaryDetails = new System.Windows.Forms.TabPage();
            this.cmbRoute = new System.Windows.Forms.ComboBox();
            this.lblRoute = new System.Windows.Forms.Label();
            this.cmbArea = new System.Windows.Forms.ComboBox();
            this.lblArea = new System.Windows.Forms.Label();
            this.txtCst = new System.Windows.Forms.TextBox();
            this.txtCreditPeriod = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtPan = new System.Windows.Forms.TextBox();
            this.txtTin = new System.Windows.Forms.TextBox();
            this.txtCreditLimit = new System.Windows.Forms.TextBox();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.txtMailingName = new System.Windows.Forms.TextBox();
            this.lblCst = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblPan = new System.Windows.Forms.Label();
            this.lblTin = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblAccountNo = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.cmbBillByBill = new System.Windows.Forms.ComboBox();
            this.lblBillbyBill = new System.Windows.Forms.Label();
            this.lblMobile = new System.Windows.Forms.Label();
            this.cmbPricingLevel = new System.Windows.Forms.ComboBox();
            this.lblPricingLevel = new System.Windows.Forms.Label();
            this.lblMailingName = new System.Windows.Forms.Label();
            this.tbMainDetails = new System.Windows.Forms.TabPage();
            this.cbxActive = new System.Windows.Forms.CheckBox();
            this.lblAccountCode = new System.Windows.Forms.Label();
            this.txtAccountCode = new System.Windows.Forms.TextBox();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblSalaryTypeValidator = new System.Windows.Forms.Label();
            this.btnAccountGroupAdd = new System.Windows.Forms.Button();
            this.txtOpeningBalance = new System.Windows.Forms.TextBox();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.txtLedgerName = new System.Windows.Forms.TextBox();
            this.gbxDetails = new System.Windows.Forms.GroupBox();
            this.txtBranchCode = new System.Windows.Forms.TextBox();
            this.lblBranchCode = new System.Windows.Forms.Label();
            this.txtBranchName = new System.Windows.Forms.TextBox();
            this.lblBranchName = new System.Windows.Forms.Label();
            this.txtAcNo = new System.Windows.Forms.TextBox();
            this.lblACNo = new System.Windows.Forms.Label();
            this.lblNarration = new System.Windows.Forms.Label();
            this.cmbOpeningBalanceCrOrDr = new System.Windows.Forms.ComboBox();
            this.lblOpeningBalance = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.tbctrlLedger = new System.Windows.Forms.TabControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.cmbLedgersToShow = new System.Windows.Forms.ComboBox();
            this.lblShow = new System.Windows.Forms.Label();
            this.dgvAccountLedgerDetailsView = new System.Windows.Forms.DataGridView();
            this.txtDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtvoucherTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.voucherTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masterId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.invoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLedgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtMemo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTransactionSummary = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountLedger)).BeginInit();
            this.tbSecondaryDetails.SuspendLayout();
            this.tbMainDetails.SuspendLayout();
            this.gbxDetails.SuspendLayout();
            this.tbctrlLedger.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountLedgerDetailsView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Gray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(290, 57);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 21);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSearch_KeyDown);
            // 
            // dgvAccountLedger
            // 
            this.dgvAccountLedger.AllowDrop = true;
            this.dgvAccountLedger.AllowUserToAddRows = false;
            this.dgvAccountLedger.AllowUserToDeleteRows = false;
            this.dgvAccountLedger.AllowUserToOrderColumns = true;
            this.dgvAccountLedger.AllowUserToResizeRows = false;
            this.dgvAccountLedger.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAccountLedger.BackgroundColor = System.Drawing.Color.White;
            this.dgvAccountLedger.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAccountLedger.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAccountLedger.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAccountLedger.ColumnHeadersHeight = 35;
            this.dgvAccountLedger.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAccountLedger.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtCreditOrDebit,
            this.SLNO,
            this.dgvtxtLedgerId,
            this.ledgerName,
            this.accountGroupName,
            this.dgvtxtLedger,
            this.TotalBalance});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAccountLedger.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAccountLedger.EnableHeadersVisualStyles = false;
            this.dgvAccountLedger.GridColor = System.Drawing.Color.White;
            this.dgvAccountLedger.Location = new System.Drawing.Point(0, 85);
            this.dgvAccountLedger.MultiSelect = false;
            this.dgvAccountLedger.Name = "dgvAccountLedger";
            this.dgvAccountLedger.ReadOnly = true;
            this.dgvAccountLedger.RowHeadersVisible = false;
            this.dgvAccountLedger.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAccountLedger.Size = new System.Drawing.Size(372, 347);
            this.dgvAccountLedger.TabIndex = 39;
            this.dgvAccountLedger.TabStop = false;
            this.dgvAccountLedger.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountLedger_CellDoubleClick);
            this.dgvAccountLedger.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAccountLedger_DataBindingComplete);
            this.dgvAccountLedger.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvAccountLedger_KeyUp);
            // 
            // dgvtxtCreditOrDebit
            // 
            this.dgvtxtCreditOrDebit.DataPropertyName = "crOrDr";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dgvtxtCreditOrDebit.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtCreditOrDebit.HeaderText = "CrOrDr";
            this.dgvtxtCreditOrDebit.Name = "dgvtxtCreditOrDebit";
            this.dgvtxtCreditOrDebit.ReadOnly = true;
            this.dgvtxtCreditOrDebit.Visible = false;
            // 
            // SLNO
            // 
            this.SLNO.DataPropertyName = "SLNO";
            this.SLNO.FillWeight = 50.76142F;
            this.SLNO.HeaderText = "Sl.No";
            this.SLNO.Name = "SLNO";
            this.SLNO.ReadOnly = true;
            // 
            // dgvtxtLedgerId
            // 
            this.dgvtxtLedgerId.DataPropertyName = "ledgerId";
            this.dgvtxtLedgerId.HeaderText = "ledgerID";
            this.dgvtxtLedgerId.Name = "dgvtxtLedgerId";
            this.dgvtxtLedgerId.ReadOnly = true;
            this.dgvtxtLedgerId.Visible = false;
            // 
            // ledgerName
            // 
            this.ledgerName.DataPropertyName = "ledgerName";
            this.ledgerName.FillWeight = 116.4129F;
            this.ledgerName.HeaderText = "Ledger";
            this.ledgerName.Name = "ledgerName";
            this.ledgerName.ReadOnly = true;
            // 
            // accountGroupName
            // 
            this.accountGroupName.DataPropertyName = "accountGroupName";
            this.accountGroupName.FillWeight = 116.4129F;
            this.accountGroupName.HeaderText = "Account Group";
            this.accountGroupName.Name = "accountGroupName";
            this.accountGroupName.ReadOnly = true;
            // 
            // dgvtxtLedger
            // 
            this.dgvtxtLedger.DataPropertyName = "ledgerName";
            this.dgvtxtLedger.HeaderText = "Ledger";
            this.dgvtxtLedger.Name = "dgvtxtLedger";
            this.dgvtxtLedger.ReadOnly = true;
            this.dgvtxtLedger.Visible = false;
            // 
            // TotalBalance
            // 
            this.TotalBalance.DataPropertyName = "balance";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.TotalBalance.DefaultCellStyle = dataGridViewCellStyle3;
            this.TotalBalance.FillWeight = 116.4129F;
            this.TotalBalance.HeaderText = "Balance";
            this.TotalBalance.Name = "TotalBalance";
            this.TotalBalance.ReadOnly = true;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(420, 188);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(511, 188);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(238, 188);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(329, 188);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 27);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtLedgerNameSearch
            // 
            this.txtLedgerNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLedgerNameSearch.ForeColor = System.Drawing.Color.Black;
            this.txtLedgerNameSearch.Location = new System.Drawing.Point(144, 3);
            this.txtLedgerNameSearch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtLedgerNameSearch.Name = "txtLedgerNameSearch";
            this.txtLedgerNameSearch.Size = new System.Drawing.Size(231, 20);
            this.txtLedgerNameSearch.TabIndex = 0;
            this.txtLedgerNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLedgerNameSearch_KeyDown);
            // 
            // lblLedgerNameSearch
            // 
            this.lblLedgerNameSearch.AutoSize = true;
            this.lblLedgerNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedgerNameSearch.ForeColor = System.Drawing.Color.Black;
            this.lblLedgerNameSearch.Location = new System.Drawing.Point(92, 7);
            this.lblLedgerNameSearch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblLedgerNameSearch.Name = "lblLedgerNameSearch";
            this.lblLedgerNameSearch.Size = new System.Drawing.Size(41, 13);
            this.lblLedgerNameSearch.TabIndex = 171;
            this.lblLedgerNameSearch.Text = "Name  ";
            // 
            // cmbGroupSearch
            // 
            this.cmbGroupSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroupSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGroupSearch.ForeColor = System.Drawing.Color.Black;
            this.cmbGroupSearch.FormattingEnabled = true;
            this.cmbGroupSearch.Location = new System.Drawing.Point(144, 28);
            this.cmbGroupSearch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbGroupSearch.Name = "cmbGroupSearch";
            this.cmbGroupSearch.Size = new System.Drawing.Size(231, 21);
            this.cmbGroupSearch.TabIndex = 1;
            this.cmbGroupSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroupSearch_KeyDown);
            // 
            // lblGroupSearch
            // 
            this.lblGroupSearch.AutoSize = true;
            this.lblGroupSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGroupSearch.ForeColor = System.Drawing.Color.Black;
            this.lblGroupSearch.Location = new System.Drawing.Point(91, 29);
            this.lblGroupSearch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblGroupSearch.Name = "lblGroupSearch";
            this.lblGroupSearch.Size = new System.Drawing.Size(42, 13);
            this.lblGroupSearch.TabIndex = 169;
            this.lblGroupSearch.Text = "Group  ";
            // 
            // tbSecondaryDetails
            // 
            this.tbSecondaryDetails.BackColor = System.Drawing.Color.Gainsboro;
            this.tbSecondaryDetails.Controls.Add(this.cmbRoute);
            this.tbSecondaryDetails.Controls.Add(this.lblRoute);
            this.tbSecondaryDetails.Controls.Add(this.cmbArea);
            this.tbSecondaryDetails.Controls.Add(this.lblArea);
            this.tbSecondaryDetails.Controls.Add(this.txtCst);
            this.tbSecondaryDetails.Controls.Add(this.txtCreditPeriod);
            this.tbSecondaryDetails.Controls.Add(this.txtEmail);
            this.tbSecondaryDetails.Controls.Add(this.txtPhone);
            this.tbSecondaryDetails.Controls.Add(this.txtPan);
            this.tbSecondaryDetails.Controls.Add(this.txtTin);
            this.tbSecondaryDetails.Controls.Add(this.txtCreditLimit);
            this.tbSecondaryDetails.Controls.Add(this.txtAccountNo);
            this.tbSecondaryDetails.Controls.Add(this.txtAddress);
            this.tbSecondaryDetails.Controls.Add(this.txtMobile);
            this.tbSecondaryDetails.Controls.Add(this.txtMailingName);
            this.tbSecondaryDetails.Controls.Add(this.lblCst);
            this.tbSecondaryDetails.Controls.Add(this.label21);
            this.tbSecondaryDetails.Controls.Add(this.lblEmail);
            this.tbSecondaryDetails.Controls.Add(this.lblPhone);
            this.tbSecondaryDetails.Controls.Add(this.lblPan);
            this.tbSecondaryDetails.Controls.Add(this.lblTin);
            this.tbSecondaryDetails.Controls.Add(this.label16);
            this.tbSecondaryDetails.Controls.Add(this.lblAccountNo);
            this.tbSecondaryDetails.Controls.Add(this.lblAddress);
            this.tbSecondaryDetails.Controls.Add(this.cmbBillByBill);
            this.tbSecondaryDetails.Controls.Add(this.lblBillbyBill);
            this.tbSecondaryDetails.Controls.Add(this.lblMobile);
            this.tbSecondaryDetails.Controls.Add(this.cmbPricingLevel);
            this.tbSecondaryDetails.Controls.Add(this.lblPricingLevel);
            this.tbSecondaryDetails.Controls.Add(this.lblMailingName);
            this.tbSecondaryDetails.Location = new System.Drawing.Point(4, 22);
            this.tbSecondaryDetails.Name = "tbSecondaryDetails";
            this.tbSecondaryDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tbSecondaryDetails.Size = new System.Drawing.Size(705, 228);
            this.tbSecondaryDetails.TabIndex = 1;
            this.tbSecondaryDetails.Text = "Secondary Details";
            // 
            // cmbRoute
            // 
            this.cmbRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRoute.FormattingEnabled = true;
            this.cmbRoute.Location = new System.Drawing.Point(408, 184);
            this.cmbRoute.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbRoute.Name = "cmbRoute";
            this.cmbRoute.Size = new System.Drawing.Size(199, 21);
            this.cmbRoute.TabIndex = 25;
            this.cmbRoute.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRoute_KeyDown);
            // 
            // lblRoute
            // 
            this.lblRoute.AutoSize = true;
            this.lblRoute.BackColor = System.Drawing.Color.Transparent;
            this.lblRoute.ForeColor = System.Drawing.Color.Black;
            this.lblRoute.Location = new System.Drawing.Point(330, 191);
            this.lblRoute.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblRoute.Name = "lblRoute";
            this.lblRoute.Size = new System.Drawing.Size(24, 13);
            this.lblRoute.TabIndex = 192;
            this.lblRoute.Text = "City";
            // 
            // cmbArea
            // 
            this.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbArea.FormattingEnabled = true;
            this.cmbArea.Location = new System.Drawing.Point(408, 158);
            this.cmbArea.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbArea.Name = "cmbArea";
            this.cmbArea.Size = new System.Drawing.Size(199, 21);
            this.cmbArea.TabIndex = 24;
            this.cmbArea.SelectedIndexChanged += new System.EventHandler(this.cmbArea_SelectedIndexChanged);
            this.cmbArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbArea_KeyDown);
            // 
            // lblArea
            // 
            this.lblArea.AutoSize = true;
            this.lblArea.BackColor = System.Drawing.Color.Transparent;
            this.lblArea.ForeColor = System.Drawing.Color.Black;
            this.lblArea.Location = new System.Drawing.Point(330, 165);
            this.lblArea.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(32, 13);
            this.lblArea.TabIndex = 190;
            this.lblArea.Text = "State";
            // 
            // txtCst
            // 
            this.txtCst.Location = new System.Drawing.Point(408, 133);
            this.txtCst.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtCst.MaxLength = 20;
            this.txtCst.Name = "txtCst";
            this.txtCst.Size = new System.Drawing.Size(199, 20);
            this.txtCst.TabIndex = 23;
            this.txtCst.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCst_KeyDown);
            // 
            // txtCreditPeriod
            // 
            this.txtCreditPeriod.Location = new System.Drawing.Point(408, 108);
            this.txtCreditPeriod.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtCreditPeriod.MaxLength = 3;
            this.txtCreditPeriod.Name = "txtCreditPeriod";
            this.txtCreditPeriod.Size = new System.Drawing.Size(199, 20);
            this.txtCreditPeriod.TabIndex = 22;
            this.txtCreditPeriod.Enter += new System.EventHandler(this.txtCreditPeriod_Enter);
            this.txtCreditPeriod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditPeriod_KeyDown);
            this.txtCreditPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCreditPeriod_KeyPress);
            this.txtCreditPeriod.Leave += new System.EventHandler(this.txtCreditPeriod_Leave);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(408, 57);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(199, 20);
            this.txtEmail.TabIndex = 20;
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail_KeyDown);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(408, 32);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtPhone.MaxLength = 15;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(199, 20);
            this.txtPhone.TabIndex = 19;
            this.txtPhone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPhone_KeyDown);
            this.txtPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // txtPan
            // 
            this.txtPan.Location = new System.Drawing.Point(94, 189);
            this.txtPan.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtPan.MaxLength = 20;
            this.txtPan.Name = "txtPan";
            this.txtPan.Size = new System.Drawing.Size(200, 20);
            this.txtPan.TabIndex = 17;
            this.txtPan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPan_KeyDown);
            // 
            // txtTin
            // 
            this.txtTin.Location = new System.Drawing.Point(94, 164);
            this.txtTin.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtTin.MaxLength = 20;
            this.txtTin.Name = "txtTin";
            this.txtTin.Size = new System.Drawing.Size(200, 20);
            this.txtTin.TabIndex = 16;
            this.txtTin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTin_KeyDown);
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.Location = new System.Drawing.Point(94, 139);
            this.txtCreditLimit.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtCreditLimit.MaxLength = 13;
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.Size = new System.Drawing.Size(200, 20);
            this.txtCreditLimit.TabIndex = 15;
            this.txtCreditLimit.Enter += new System.EventHandler(this.txtCreditLimit_Enter);
            this.txtCreditLimit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditLimit_KeyDown);
            this.txtCreditLimit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCreditLimit_KeyPress);
            this.txtCreditLimit.Leave += new System.EventHandler(this.txtCreditLimit_Leave);
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Location = new System.Drawing.Point(408, 7);
            this.txtAccountNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtAccountNo.MaxLength = 18;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Size = new System.Drawing.Size(199, 20);
            this.txtAccountNo.TabIndex = 18;
            this.txtAccountNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAccountNo_KeyDown);
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(94, 33);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(200, 50);
            this.txtAddress.TabIndex = 12;
            this.txtAddress.Enter += new System.EventHandler(this.txtAddress_Enter);
            this.txtAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddress_KeyDown);
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(94, 88);
            this.txtMobile.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtMobile.MaxLength = 15;
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(200, 20);
            this.txtMobile.TabIndex = 13;
            this.txtMobile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMobile_KeyDown);
            this.txtMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobile_KeyPress);
            // 
            // txtMailingName
            // 
            this.txtMailingName.Location = new System.Drawing.Point(94, 8);
            this.txtMailingName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtMailingName.Name = "txtMailingName";
            this.txtMailingName.Size = new System.Drawing.Size(200, 20);
            this.txtMailingName.TabIndex = 11;
            this.txtMailingName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMailingName_KeyDown);
            // 
            // lblCst
            // 
            this.lblCst.AutoSize = true;
            this.lblCst.BackColor = System.Drawing.Color.Transparent;
            this.lblCst.ForeColor = System.Drawing.Color.Black;
            this.lblCst.Location = new System.Drawing.Point(330, 140);
            this.lblCst.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblCst.Name = "lblCst";
            this.lblCst.Size = new System.Drawing.Size(44, 13);
            this.lblCst.TabIndex = 188;
            this.lblCst.Text = "Position";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(330, 115);
            this.label21.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 13);
            this.label21.TabIndex = 186;
            this.label21.Text = "Credit Period";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblEmail.ForeColor = System.Drawing.Color.Black;
            this.lblEmail.Location = new System.Drawing.Point(330, 64);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 184;
            this.lblEmail.Text = "E-mail";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.BackColor = System.Drawing.Color.Transparent;
            this.lblPhone.ForeColor = System.Drawing.Color.Black;
            this.lblPhone.Location = new System.Drawing.Point(330, 39);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(38, 13);
            this.lblPhone.TabIndex = 182;
            this.lblPhone.Text = "Phone";
            // 
            // lblPan
            // 
            this.lblPan.AutoSize = true;
            this.lblPan.BackColor = System.Drawing.Color.Transparent;
            this.lblPan.ForeColor = System.Drawing.Color.Black;
            this.lblPan.Location = new System.Drawing.Point(14, 191);
            this.lblPan.Margin = new System.Windows.Forms.Padding(5);
            this.lblPan.Name = "lblPan";
            this.lblPan.Size = new System.Drawing.Size(29, 13);
            this.lblPan.TabIndex = 180;
            this.lblPan.Text = "PAN";
            // 
            // lblTin
            // 
            this.lblTin.AutoSize = true;
            this.lblTin.BackColor = System.Drawing.Color.Transparent;
            this.lblTin.ForeColor = System.Drawing.Color.Black;
            this.lblTin.Location = new System.Drawing.Point(14, 166);
            this.lblTin.Margin = new System.Windows.Forms.Padding(5);
            this.lblTin.Name = "lblTin";
            this.lblTin.Size = new System.Drawing.Size(25, 13);
            this.lblTin.TabIndex = 178;
            this.lblTin.Text = "TIN";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(14, 141);
            this.label16.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 176;
            this.label16.Text = "Credit Limit";
            // 
            // lblAccountNo
            // 
            this.lblAccountNo.AutoSize = true;
            this.lblAccountNo.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountNo.ForeColor = System.Drawing.Color.Black;
            this.lblAccountNo.Location = new System.Drawing.Point(330, 14);
            this.lblAccountNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblAccountNo.Name = "lblAccountNo";
            this.lblAccountNo.Size = new System.Drawing.Size(67, 13);
            this.lblAccountNo.TabIndex = 174;
            this.lblAccountNo.Text = "Account No.";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblAddress.ForeColor = System.Drawing.Color.Black;
            this.lblAddress.Location = new System.Drawing.Point(14, 31);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 172;
            this.lblAddress.Text = "Address";
            // 
            // cmbBillByBill
            // 
            this.cmbBillByBill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBillByBill.FormattingEnabled = true;
            this.cmbBillByBill.Items.AddRange(new object[] {
            "Yes"});
            this.cmbBillByBill.Location = new System.Drawing.Point(408, 82);
            this.cmbBillByBill.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbBillByBill.Name = "cmbBillByBill";
            this.cmbBillByBill.Size = new System.Drawing.Size(199, 21);
            this.cmbBillByBill.TabIndex = 21;
            this.cmbBillByBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBillByBill_KeyDown);
            // 
            // lblBillbyBill
            // 
            this.lblBillbyBill.AutoSize = true;
            this.lblBillbyBill.BackColor = System.Drawing.Color.Transparent;
            this.lblBillbyBill.ForeColor = System.Drawing.Color.Black;
            this.lblBillbyBill.Location = new System.Drawing.Point(330, 89);
            this.lblBillbyBill.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBillbyBill.Name = "lblBillbyBill";
            this.lblBillbyBill.Size = new System.Drawing.Size(50, 13);
            this.lblBillbyBill.TabIndex = 169;
            this.lblBillbyBill.Text = "Bill by Bill";
            // 
            // lblMobile
            // 
            this.lblMobile.AutoSize = true;
            this.lblMobile.BackColor = System.Drawing.Color.Transparent;
            this.lblMobile.ForeColor = System.Drawing.Color.Black;
            this.lblMobile.Location = new System.Drawing.Point(14, 90);
            this.lblMobile.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(38, 13);
            this.lblMobile.TabIndex = 167;
            this.lblMobile.Text = "Mobile";
            // 
            // cmbPricingLevel
            // 
            this.cmbPricingLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPricingLevel.FormattingEnabled = true;
            this.cmbPricingLevel.Location = new System.Drawing.Point(94, 113);
            this.cmbPricingLevel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbPricingLevel.Name = "cmbPricingLevel";
            this.cmbPricingLevel.Size = new System.Drawing.Size(200, 21);
            this.cmbPricingLevel.TabIndex = 14;
            this.cmbPricingLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPricingLevel_KeyDown);
            // 
            // lblPricingLevel
            // 
            this.lblPricingLevel.AutoSize = true;
            this.lblPricingLevel.BackColor = System.Drawing.Color.Transparent;
            this.lblPricingLevel.ForeColor = System.Drawing.Color.Black;
            this.lblPricingLevel.Location = new System.Drawing.Point(14, 115);
            this.lblPricingLevel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblPricingLevel.Name = "lblPricingLevel";
            this.lblPricingLevel.Size = new System.Drawing.Size(68, 13);
            this.lblPricingLevel.TabIndex = 164;
            this.lblPricingLevel.Text = "Pricing Level";
            // 
            // lblMailingName
            // 
            this.lblMailingName.AutoSize = true;
            this.lblMailingName.BackColor = System.Drawing.Color.Transparent;
            this.lblMailingName.ForeColor = System.Drawing.Color.Black;
            this.lblMailingName.Location = new System.Drawing.Point(11, 10);
            this.lblMailingName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMailingName.Name = "lblMailingName";
            this.lblMailingName.Size = new System.Drawing.Size(82, 13);
            this.lblMailingName.TabIndex = 163;
            this.lblMailingName.Text = "Company Name";
            // 
            // tbMainDetails
            // 
            this.tbMainDetails.BackColor = System.Drawing.Color.Gainsboro;
            this.tbMainDetails.Controls.Add(this.cbxActive);
            this.tbMainDetails.Controls.Add(this.lblAccountCode);
            this.tbMainDetails.Controls.Add(this.txtAccountCode);
            this.tbMainDetails.Controls.Add(this.dtpToDate);
            this.tbMainDetails.Controls.Add(this.label1);
            this.tbMainDetails.Controls.Add(this.txtToDate);
            this.tbMainDetails.Controls.Add(this.txtFromDate);
            this.tbMainDetails.Controls.Add(this.btnClose);
            this.tbMainDetails.Controls.Add(this.dtpFromDate);
            this.tbMainDetails.Controls.Add(this.lblSalaryTypeValidator);
            this.tbMainDetails.Controls.Add(this.btnDelete);
            this.tbMainDetails.Controls.Add(this.btnClear);
            this.tbMainDetails.Controls.Add(this.btnAccountGroupAdd);
            this.tbMainDetails.Controls.Add(this.btnSave);
            this.tbMainDetails.Controls.Add(this.txtOpeningBalance);
            this.tbMainDetails.Controls.Add(this.txtNarration);
            this.tbMainDetails.Controls.Add(this.txtLedgerName);
            this.tbMainDetails.Controls.Add(this.gbxDetails);
            this.tbMainDetails.Controls.Add(this.lblNarration);
            this.tbMainDetails.Controls.Add(this.cmbOpeningBalanceCrOrDr);
            this.tbMainDetails.Controls.Add(this.lblOpeningBalance);
            this.tbMainDetails.Controls.Add(this.cmbGroup);
            this.tbMainDetails.Controls.Add(this.lblGroup);
            this.tbMainDetails.Controls.Add(this.lblName);
            this.tbMainDetails.Location = new System.Drawing.Point(4, 22);
            this.tbMainDetails.Margin = new System.Windows.Forms.Padding(5);
            this.tbMainDetails.Name = "tbMainDetails";
            this.tbMainDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tbMainDetails.Size = new System.Drawing.Size(705, 228);
            this.tbMainDetails.TabIndex = 0;
            this.tbMainDetails.Text = "Main Details";
            // 
            // cbxActive
            // 
            this.cbxActive.AutoSize = true;
            this.cbxActive.ForeColor = System.Drawing.Color.Black;
            this.cbxActive.Location = new System.Drawing.Point(99, 197);
            this.cbxActive.Name = "cbxActive";
            this.cbxActive.Size = new System.Drawing.Size(56, 17);
            this.cbxActive.TabIndex = 1319;
            this.cbxActive.Text = "Active";
            this.cbxActive.UseVisualStyleBackColor = true;
            // 
            // lblAccountCode
            // 
            this.lblAccountCode.AutoSize = true;
            this.lblAccountCode.BackColor = System.Drawing.Color.Transparent;
            this.lblAccountCode.ForeColor = System.Drawing.Color.Black;
            this.lblAccountCode.Location = new System.Drawing.Point(2, 12);
            this.lblAccountCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblAccountCode.Name = "lblAccountCode";
            this.lblAccountCode.Size = new System.Drawing.Size(75, 13);
            this.lblAccountCode.TabIndex = 1318;
            this.lblAccountCode.Text = "Account Code";
            // 
            // txtAccountCode
            // 
            this.txtAccountCode.Location = new System.Drawing.Point(99, 8);
            this.txtAccountCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtAccountCode.Name = "txtAccountCode";
            this.txtAccountCode.Size = new System.Drawing.Size(200, 20);
            this.txtAccountCode.TabIndex = 1317;
            // 
            // dtpToDate
            // 
            this.dtpToDate.Location = new System.Drawing.Point(616, 130);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(22, 20);
            this.dtpToDate.TabIndex = 1316;
            this.dtpToDate.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(300, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "*";
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(470, 130);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(5);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.ReadOnly = true;
            this.txtToDate.Size = new System.Drawing.Size(144, 20);
            this.txtToDate.TabIndex = 1315;
            this.txtToDate.Visible = false;
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(330, 130);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.ReadOnly = true;
            this.txtFromDate.Size = new System.Drawing.Size(113, 20);
            this.txtFromDate.TabIndex = 1313;
            this.txtFromDate.Visible = false;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Location = new System.Drawing.Point(445, 130);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(19, 20);
            this.dtpFromDate.TabIndex = 1314;
            this.dtpFromDate.Visible = false;
            // 
            // lblSalaryTypeValidator
            // 
            this.lblSalaryTypeValidator.AutoSize = true;
            this.lblSalaryTypeValidator.ForeColor = System.Drawing.Color.Red;
            this.lblSalaryTypeValidator.Location = new System.Drawing.Point(301, 40);
            this.lblSalaryTypeValidator.Margin = new System.Windows.Forms.Padding(5);
            this.lblSalaryTypeValidator.Name = "lblSalaryTypeValidator";
            this.lblSalaryTypeValidator.Size = new System.Drawing.Size(11, 13);
            this.lblSalaryTypeValidator.TabIndex = 175;
            this.lblSalaryTypeValidator.Text = "*";
            // 
            // btnAccountGroupAdd
            // 
            this.btnAccountGroupAdd.BackColor = System.Drawing.Color.Gray;
            this.btnAccountGroupAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAccountGroupAdd.FlatAppearance.BorderSize = 0;
            this.btnAccountGroupAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccountGroupAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccountGroupAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnAccountGroupAdd.Location = new System.Drawing.Point(314, 60);
            this.btnAccountGroupAdd.Name = "btnAccountGroupAdd";
            this.btnAccountGroupAdd.Size = new System.Drawing.Size(20, 20);
            this.btnAccountGroupAdd.TabIndex = 4;
            this.btnAccountGroupAdd.Text = "+";
            this.btnAccountGroupAdd.UseVisualStyleBackColor = false;
            this.btnAccountGroupAdd.Click += new System.EventHandler(this.btnAccountGroupAdd_Click);
            // 
            // txtOpeningBalance
            // 
            this.txtOpeningBalance.Enabled = false;
            this.txtOpeningBalance.Location = new System.Drawing.Point(99, 87);
            this.txtOpeningBalance.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtOpeningBalance.MaxLength = 13;
            this.txtOpeningBalance.Name = "txtOpeningBalance";
            this.txtOpeningBalance.Size = new System.Drawing.Size(146, 20);
            this.txtOpeningBalance.TabIndex = 2;
            this.txtOpeningBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOpeningBalance.Visible = false;
            this.txtOpeningBalance.Enter += new System.EventHandler(this.txtOpeningBalance_Enter);
            this.txtOpeningBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOpeningBalance_KeyDown);
            this.txtOpeningBalance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOpeningBalance_KeyPress);
            this.txtOpeningBalance.Leave += new System.EventHandler(this.txtOpeningBalance_Leave);
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(99, 112);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(200, 70);
            this.txtNarration.TabIndex = 4;
            this.txtNarration.Enter += new System.EventHandler(this.txtNarration_Enter);
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            // 
            // txtLedgerName
            // 
            this.txtLedgerName.Location = new System.Drawing.Point(99, 36);
            this.txtLedgerName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtLedgerName.Name = "txtLedgerName";
            this.txtLedgerName.Size = new System.Drawing.Size(200, 20);
            this.txtLedgerName.TabIndex = 0;
            this.txtLedgerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLedgerName_KeyDown);
            this.txtLedgerName.Leave += new System.EventHandler(this.txtLedgerName_Leave);
            // 
            // gbxDetails
            // 
            this.gbxDetails.BackColor = System.Drawing.Color.Transparent;
            this.gbxDetails.Controls.Add(this.txtBranchCode);
            this.gbxDetails.Controls.Add(this.lblBranchCode);
            this.gbxDetails.Controls.Add(this.txtBranchName);
            this.gbxDetails.Controls.Add(this.lblBranchName);
            this.gbxDetails.Controls.Add(this.txtAcNo);
            this.gbxDetails.Controls.Add(this.lblACNo);
            this.gbxDetails.ForeColor = System.Drawing.Color.Black;
            this.gbxDetails.Location = new System.Drawing.Point(337, 7);
            this.gbxDetails.Name = "gbxDetails";
            this.gbxDetails.Size = new System.Drawing.Size(280, 119);
            this.gbxDetails.TabIndex = 6;
            this.gbxDetails.TabStop = false;
            this.gbxDetails.Text = "Details";
            // 
            // txtBranchCode
            // 
            this.txtBranchCode.Location = new System.Drawing.Point(88, 54);
            this.txtBranchCode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtBranchCode.MaxLength = 18;
            this.txtBranchCode.Name = "txtBranchCode";
            this.txtBranchCode.Size = new System.Drawing.Size(186, 20);
            this.txtBranchCode.TabIndex = 2;
            this.txtBranchCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBranchCode_KeyDown);
            // 
            // lblBranchCode
            // 
            this.lblBranchCode.AutoSize = true;
            this.lblBranchCode.ForeColor = System.Drawing.Color.Black;
            this.lblBranchCode.Location = new System.Drawing.Point(0, 57);
            this.lblBranchCode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBranchCode.Name = "lblBranchCode";
            this.lblBranchCode.Size = new System.Drawing.Size(69, 13);
            this.lblBranchCode.TabIndex = 149;
            this.lblBranchCode.Text = "Branch Code";
            // 
            // txtBranchName
            // 
            this.txtBranchName.Location = new System.Drawing.Point(88, 29);
            this.txtBranchName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtBranchName.Name = "txtBranchName";
            this.txtBranchName.Size = new System.Drawing.Size(186, 20);
            this.txtBranchName.TabIndex = 1;
            this.txtBranchName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBranchName_KeyDown);
            // 
            // lblBranchName
            // 
            this.lblBranchName.AutoSize = true;
            this.lblBranchName.ForeColor = System.Drawing.Color.Black;
            this.lblBranchName.Location = new System.Drawing.Point(0, 33);
            this.lblBranchName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBranchName.Name = "lblBranchName";
            this.lblBranchName.Size = new System.Drawing.Size(72, 13);
            this.lblBranchName.TabIndex = 147;
            this.lblBranchName.Text = "Branch Name";
            // 
            // txtAcNo
            // 
            this.txtAcNo.Location = new System.Drawing.Point(88, 79);
            this.txtAcNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtAcNo.MaxLength = 18;
            this.txtAcNo.Name = "txtAcNo";
            this.txtAcNo.Size = new System.Drawing.Size(186, 20);
            this.txtAcNo.TabIndex = 0;
            this.txtAcNo.Visible = false;
            this.txtAcNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAcNo_KeyDown);
            // 
            // lblACNo
            // 
            this.lblACNo.AutoSize = true;
            this.lblACNo.ForeColor = System.Drawing.Color.Black;
            this.lblACNo.Location = new System.Drawing.Point(0, 82);
            this.lblACNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblACNo.Name = "lblACNo";
            this.lblACNo.Size = new System.Drawing.Size(48, 13);
            this.lblACNo.TabIndex = 145;
            this.lblACNo.Text = "Ac / No.";
            this.lblACNo.Visible = false;
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(2, 114);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(50, 13);
            this.lblNarration.TabIndex = 161;
            this.lblNarration.Text = "Narration";
            // 
            // cmbOpeningBalanceCrOrDr
            // 
            this.cmbOpeningBalanceCrOrDr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOpeningBalanceCrOrDr.FormattingEnabled = true;
            this.cmbOpeningBalanceCrOrDr.Items.AddRange(new object[] {
            "Dr",
            "Cr"});
            this.cmbOpeningBalanceCrOrDr.Location = new System.Drawing.Point(249, 87);
            this.cmbOpeningBalanceCrOrDr.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbOpeningBalanceCrOrDr.Name = "cmbOpeningBalanceCrOrDr";
            this.cmbOpeningBalanceCrOrDr.Size = new System.Drawing.Size(51, 21);
            this.cmbOpeningBalanceCrOrDr.TabIndex = 3;
            this.cmbOpeningBalanceCrOrDr.Visible = false;
            this.cmbOpeningBalanceCrOrDr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOpeningBalanceCrOrDr_KeyDown);
            // 
            // lblOpeningBalance
            // 
            this.lblOpeningBalance.AutoSize = true;
            this.lblOpeningBalance.BackColor = System.Drawing.Color.Transparent;
            this.lblOpeningBalance.ForeColor = System.Drawing.Color.Black;
            this.lblOpeningBalance.Location = new System.Drawing.Point(2, 91);
            this.lblOpeningBalance.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblOpeningBalance.Name = "lblOpeningBalance";
            this.lblOpeningBalance.Size = new System.Drawing.Size(89, 13);
            this.lblOpeningBalance.TabIndex = 145;
            this.lblOpeningBalance.Text = "Opening Balance";
            this.lblOpeningBalance.Visible = false;
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(99, 61);
            this.cmbGroup.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(200, 21);
            this.cmbGroup.TabIndex = 1;
            this.cmbGroup.SelectedValueChanged += new System.EventHandler(this.cmbGroup_SelectedValueChanged);
            this.cmbGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroup_KeyDown);
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblGroup.ForeColor = System.Drawing.Color.Black;
            this.lblGroup.Location = new System.Drawing.Point(2, 65);
            this.lblGroup.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(36, 13);
            this.lblGroup.TabIndex = 139;
            this.lblGroup.Text = "Group";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(2, 40);
            this.lblName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 136;
            this.lblName.Text = "Name";
            // 
            // tbctrlLedger
            // 
            this.tbctrlLedger.Controls.Add(this.tbMainDetails);
            this.tbctrlLedger.Controls.Add(this.tbSecondaryDetails);
            this.tbctrlLedger.Location = new System.Drawing.Point(387, 2);
            this.tbctrlLedger.Name = "tbctrlLedger";
            this.tbctrlLedger.SelectedIndex = 0;
            this.tbctrlLedger.Size = new System.Drawing.Size(713, 254);
            this.tbctrlLedger.TabIndex = 0;
            this.tbctrlLedger.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tbctrlLedger_Selecting);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(497, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(624, 256);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnExport);
            this.groupBox2.Controls.Add(this.cmbLedgersToShow);
            this.groupBox2.Controls.Add(this.lblShow);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.dgvAccountLedger);
            this.groupBox2.Controls.Add(this.txtLedgerNameSearch);
            this.groupBox2.Controls.Add(this.cmbGroupSearch);
            this.groupBox2.Controls.Add(this.lblGroupSearch);
            this.groupBox2.Controls.Add(this.lblLedgerNameSearch);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(5, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 462);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(236, 438);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(139, 27);
            this.btnExport.TabIndex = 1317;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Visible = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // cmbLedgersToShow
            // 
            this.cmbLedgersToShow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLedgersToShow.FormattingEnabled = true;
            this.cmbLedgersToShow.Items.AddRange(new object[] {
            "All",
            "Active",
            "Inactive"});
            this.cmbLedgersToShow.Location = new System.Drawing.Point(144, 54);
            this.cmbLedgersToShow.Name = "cmbLedgersToShow";
            this.cmbLedgersToShow.Size = new System.Drawing.Size(128, 23);
            this.cmbLedgersToShow.TabIndex = 173;
            // 
            // lblShow
            // 
            this.lblShow.AutoSize = true;
            this.lblShow.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShow.Location = new System.Drawing.Point(91, 57);
            this.lblShow.Name = "lblShow";
            this.lblShow.Size = new System.Drawing.Size(38, 15);
            this.lblShow.TabIndex = 172;
            this.lblShow.Text = "Show";
            // 
            // dgvAccountLedgerDetailsView
            // 
            this.dgvAccountLedgerDetailsView.AllowUserToAddRows = false;
            this.dgvAccountLedgerDetailsView.AllowUserToDeleteRows = false;
            this.dgvAccountLedgerDetailsView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAccountLedgerDetailsView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvAccountLedgerDetailsView.BackgroundColor = System.Drawing.Color.White;
            this.dgvAccountLedgerDetailsView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAccountLedgerDetailsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccountLedgerDetailsView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtDate,
            this.txtvoucherTypeName,
            this.voucherTypeId,
            this.masterId,
            this.invoiceNo,
            this.txtLedgerId,
            this.txtDebit,
            this.txtCredit,
            this.dgvtxtMemo,
            this.POS});
            this.dgvAccountLedgerDetailsView.GridColor = System.Drawing.Color.White;
            this.dgvAccountLedgerDetailsView.Location = new System.Drawing.Point(386, 256);
            this.dgvAccountLedgerDetailsView.Name = "dgvAccountLedgerDetailsView";
            this.dgvAccountLedgerDetailsView.ReadOnly = true;
            this.dgvAccountLedgerDetailsView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAccountLedgerDetailsView.Size = new System.Drawing.Size(725, 233);
            this.dgvAccountLedgerDetailsView.TabIndex = 338;
            this.dgvAccountLedgerDetailsView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAccountLedgerDetailsView_CellDoubleClick);
            // 
            // txtDate
            // 
            this.txtDate.HeaderText = "Date";
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            // 
            // txtvoucherTypeName
            // 
            this.txtvoucherTypeName.HeaderText = "Type";
            this.txtvoucherTypeName.Name = "txtvoucherTypeName";
            this.txtvoucherTypeName.ReadOnly = true;
            // 
            // voucherTypeId
            // 
            this.voucherTypeId.HeaderText = "Voucher Type Id";
            this.voucherTypeId.Name = "voucherTypeId";
            this.voucherTypeId.ReadOnly = true;
            this.voucherTypeId.Visible = false;
            // 
            // masterId
            // 
            this.masterId.HeaderText = "MasterId";
            this.masterId.Name = "masterId";
            this.masterId.ReadOnly = true;
            this.masterId.Visible = false;
            // 
            // invoiceNo
            // 
            this.invoiceNo.HeaderText = "Num";
            this.invoiceNo.Name = "invoiceNo";
            this.invoiceNo.ReadOnly = true;
            // 
            // txtLedgerId
            // 
            this.txtLedgerId.HeaderText = "Ledger ID";
            this.txtLedgerId.Name = "txtLedgerId";
            this.txtLedgerId.ReadOnly = true;
            this.txtLedgerId.Visible = false;
            // 
            // txtDebit
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.txtDebit.DefaultCellStyle = dataGridViewCellStyle5;
            this.txtDebit.HeaderText = "Debit";
            this.txtDebit.Name = "txtDebit";
            this.txtDebit.ReadOnly = true;
            // 
            // txtCredit
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.txtCredit.DefaultCellStyle = dataGridViewCellStyle6;
            this.txtCredit.HeaderText = "Credit";
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.ReadOnly = true;
            // 
            // dgvtxtMemo
            // 
            this.dgvtxtMemo.HeaderText = "Memo";
            this.dgvtxtMemo.Name = "dgvtxtMemo";
            this.dgvtxtMemo.ReadOnly = true;
            // 
            // POS
            // 
            this.POS.HeaderText = "POS";
            this.POS.Name = "POS";
            this.POS.ReadOnly = true;
            this.POS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.POS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.POS.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(831, 495);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 339;
            this.label2.Text = "Total Transaction";
            // 
            // lblTransactionSummary
            // 
            this.lblTransactionSummary.AutoSize = true;
            this.lblTransactionSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransactionSummary.ForeColor = System.Drawing.Color.Green;
            this.lblTransactionSummary.Location = new System.Drawing.Point(973, 495);
            this.lblTransactionSummary.Name = "lblTransactionSummary";
            this.lblTransactionSummary.Size = new System.Drawing.Size(32, 13);
            this.lblTransactionSummary.TabIndex = 340;
            this.lblTransactionSummary.Text = "0.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(20, 472);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(357, 32);
            this.label3.TabIndex = 1151;
            this.label3.Text = "                                        Hint: \r\nDouble Click on A Transaction To " +
    "view Transaction Details";
            // 
            // frmAccountLedger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1118, 511);
            this.Controls.Add(this.tbctrlLedger);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTransactionSummary);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvAccountLedgerDetailsView);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmAccountLedger";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Ledger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAccountLedger_FormClosing);
            this.Load += new System.EventHandler(this.frmAccountLedger_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAccountLedger_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountLedger)).EndInit();
            this.tbSecondaryDetails.ResumeLayout(false);
            this.tbSecondaryDetails.PerformLayout();
            this.tbMainDetails.ResumeLayout(false);
            this.tbMainDetails.PerformLayout();
            this.gbxDetails.ResumeLayout(false);
            this.gbxDetails.PerformLayout();
            this.tbctrlLedger.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountLedgerDetailsView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgvAccountLedger;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtLedgerNameSearch;
        private System.Windows.Forms.Label lblLedgerNameSearch;
        private System.Windows.Forms.ComboBox cmbGroupSearch;
        private System.Windows.Forms.Label lblGroupSearch;
        private System.Windows.Forms.TabPage tbSecondaryDetails;
        private System.Windows.Forms.ComboBox cmbRoute;
        private System.Windows.Forms.Label lblRoute;
        private System.Windows.Forms.ComboBox cmbArea;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.TextBox txtCst;
        private System.Windows.Forms.TextBox txtCreditPeriod;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtPan;
        private System.Windows.Forms.TextBox txtTin;
        private System.Windows.Forms.TextBox txtCreditLimit;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.TextBox txtMailingName;
        private System.Windows.Forms.Label lblCst;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblPan;
        private System.Windows.Forms.Label lblTin;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblAccountNo;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.ComboBox cmbBillByBill;
        private System.Windows.Forms.Label lblBillbyBill;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.ComboBox cmbPricingLevel;
        private System.Windows.Forms.Label lblPricingLevel;
        private System.Windows.Forms.Label lblMailingName;
        private System.Windows.Forms.TabPage tbMainDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSalaryTypeValidator;
        private System.Windows.Forms.Button btnAccountGroupAdd;
        private System.Windows.Forms.TextBox txtOpeningBalance;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.GroupBox gbxDetails;
        private System.Windows.Forms.TextBox txtBranchCode;
        private System.Windows.Forms.Label lblBranchCode;
        private System.Windows.Forms.TextBox txtBranchName;
        private System.Windows.Forms.Label lblBranchName;
        private System.Windows.Forms.TextBox txtAcNo;
        private System.Windows.Forms.Label lblACNo;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.ComboBox cmbOpeningBalanceCrOrDr;
        private System.Windows.Forms.Label lblOpeningBalance;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TabControl tbctrlLedger;
        private System.Windows.Forms.TextBox txtLedgerName;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvAccountLedgerDetailsView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTransactionSummary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Label lblAccountCode;
        private System.Windows.Forms.TextBox txtAccountCode;
        private System.Windows.Forms.Label lblShow;
        private System.Windows.Forms.ComboBox cmbLedgersToShow;
        private System.Windows.Forms.CheckBox cbxActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtCreditOrDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn SLNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtLedgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ledgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountGroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtLedger;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtvoucherTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn voucherTypeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn masterId;
        private System.Windows.Forms.DataGridViewTextBoxColumn invoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtLedgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtMemo;
        private System.Windows.Forms.DataGridViewTextBoxColumn POS;
    }
}