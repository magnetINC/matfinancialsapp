﻿using MATFinancials.Classes.Info;
using MATFinancials.Classes.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Masters
{
    public partial class frmCategory : Form
    {
        #region Public Variables
        bool isEditMode = false;
        int CategoryIdToUpdate;
        string CategoryNameToUpdate;
        DateTime CreatedOn;
        #endregion
        private void frmCategory_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        public frmCategory()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCategoryName.Text == null || txtCategoryName.Text == string.Empty)
                {
                    Messages.InformationMessage("Enter category name");
                    txtCategoryName.Focus();
                    return;
                }
                SaveOrEdit();
            }
            catch (Exception ex) { }
        }

        private void SaveOrEdit()
        {
            if (!isEditMode)
            {
                CategorySP categorySP = new CategorySP();
                DataTable dt = categorySP.CategoryViewAll();
                if (dt != null && dt.Rows.Count > 0)
                {
                    var ExistingRecord = (from c in dt.AsEnumerable()
                                          where c.Field<string>("CategoryName").ToLower().Trim() == txtCategoryName.Text.ToLower().Trim()
                                          select new { ExistingRecord = c.Field<string>("CategoryName") }).ToArray();
                    if (ExistingRecord.Any())
                    {
                        Messages.InformationMessage("Record already exist");
                        txtCategoryName.Focus();
                        return;
                    }
                }
                Save();
            }
            else
            {
                if (txtCategoryName.Text == null || txtCategoryName.Text == string.Empty)
                {
                    Messages.InformationMessage("Category name cannot be empty");
                    txtCategoryName.Focus();
                    return;
                }
                Edit();
            }
        }

        private void Save()
        {
            CategoryInfo categoryInfo = new CategoryInfo();
            CategorySP categorySP = new CategorySP();
            categoryInfo.CategoryName = txtCategoryName.Text;
            categoryInfo.CreatedBy = Convert.ToInt32(PublicVariables._decCurrentUserId);
            if (txtDate.Text != string.Empty && txtDate.Text != "")
            {
                categoryInfo.CreatedOn = Convert.ToDateTime(txtDate.Text);
            }
            else
            {
                categoryInfo.CreatedOn = DateTime.Now.Date;
            }
            categoryInfo.ModifiedOn = DateTime.Now;
            if (categorySP.CategoryAdd(categoryInfo))
            {
                Messages.SavedMessage();
                Clear();
                fillGrid();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void Clear()
        {
            try
            {
                txtCategoryName.Text = string.Empty;
                txtDate.Text = string.Empty;
            }
            catch (Exception ex) { }
        }

        private void fillGrid()
        {
            try
            {
                //dgvCategories.Rows.Clear();
                CategorySP categorySP = new CategorySP();
                DataTable dt = categorySP.CategoryViewAll();
                if (dt != null && dt.Rows.Count > 0)
                {
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    dgvCategories.Rows.Add();
                    //    //this.dgvCategories.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                    //    //this.dgvCategories.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();
                    //    dgvCategories.Rows[dgvCategories.Rows.Count - 1].Cells["dgvtxtCategoryId"].Value = dr["CategoryId"];
                    //    dgvCategories.Rows[dgvCategories.Rows.Count - 1].Cells["dgvtxtCategoryName"].Value = dr["CategoryName"];
                    //}
                    var categoryList = (from c in dt.AsEnumerable()
                                        orderby c.Field<string>("CategoryName")
                                        select new
                                        {
                                            CategoryId = c.Field<int>("CategoryId"),
                                            CategoryName = c.Field<string>("CategoryName").ToString()
                                           ,CreatedOn = c.Field<DateTime>("CreatedOn").ToString()
                                        }).ToList();

                    dgvCategories.DataSource = categoryList;
                }
            }

            catch (Exception ex)
            {

            }
        }

        private void Edit()
        {
            CategoryInfo categoryInfo = new CategoryInfo();
            CategorySP categorySP = new CategorySP();
            categoryInfo.CategoryId = CategoryIdToUpdate;
            categoryInfo.CategoryName = txtCategoryName.Text;
            categoryInfo.CreatedBy = Convert.ToInt32(PublicVariables._decCurrentUserId);
            if (txtDate.Text != string.Empty && txtDate.Text != "")
            {
                categoryInfo.CreatedOn = Convert.ToDateTime(txtDate.Text);
            }
            else
            {
                categoryInfo.CreatedOn = DateTime.Now.Date;
            }
            categoryInfo.ModifiedBy = Convert.ToInt32(PublicVariables._decCurrentUserId);
            categoryInfo.ModifiedOn = DateTime.Now;
            if (categorySP.CategoryUpdate(categoryInfo))
            {
                Messages.UpdatedMessage();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCategoryName.Text == null || txtCategoryName.Text == string.Empty)
                {
                    Messages.InformationMessage("select category to delete");
                    txtCategoryName.Focus();
                    return;
                }
                DeleteFunction();
            }
            catch (Exception ex) { }
        }

        private void DeleteFunction()
        {
            CategoryInfo categoryInfo = new CategoryInfo();
            CategorySP categorySP = new CategorySP();
            categoryInfo.CategoryId = CategoryIdToUpdate;
            if (categorySP.CategoryDelete(categoryInfo))
            {
                Messages.DeletedMessage();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex) { }
        }

        private void dgvCategories_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if(e.RowIndex > -1)
                {
                    CategoryIdToUpdate = Convert.ToInt32(dgvCategories.Rows[e.RowIndex].Cells["dgvtxtCategoryId"].Value);
                    CategoryNameToUpdate = dgvCategories.Rows[e.RowIndex].Cells["dgvtxtCategoryName"].Value.ToString();
                    CreatedOn = Convert.ToDateTime(dgvCategories.Rows[e.RowIndex].Cells["CreatedOn"].Value.ToString());
                    Clear();
                    FillControls();
                    btnSave.Text = "Update";
                    isEditMode = true;
                    txtCategoryName.Focus();
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void FillControls()
        {
            try
            {
                txtCategoryName.Text = CategoryNameToUpdate;
                txtDate.Text = CreatedOn.ToString();
            }
            catch(Exception ex)
            {

            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpDate.Value;
                this.txtDate.Text = date.ToString("dd-MMM-yyyy");

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "Category:" + ex.Message;
            }
        }
    }
}
