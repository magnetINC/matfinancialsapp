﻿using MATFinancials.Classes.SP;
using MATFinancials.Classes.Info;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Masters
{
    public partial class frmProject : Form
    {
        #region Public Variables
        bool isEditMode = false;
        int ProjectIdToUpdate;
        string ProjectNameToUpdate;
        DateTime CreatedOn;
        #endregion

        public frmProject()
        {
            InitializeComponent();
        }

        private void Project_Load(object sender, EventArgs e)
        {
            fillGrid();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProjectName.Text == null || txtProjectName.Text == string.Empty)
                {
                    Messages.InformationMessage("Enter project name");
                    txtProjectName.Focus();
                    return;
                }
                SaveOrEdit();
            }
            catch (Exception ex) { }
        }

        private void SaveOrEdit()
        {
            if (!isEditMode)
            {
                ProjectSP ProjectSP = new ProjectSP();
                DataTable dt = ProjectSP.ProjectViewAll();
                if (dt != null && dt.Rows.Count > 0)
                {
                    var ExistingRecord = (from c in dt.AsEnumerable()
                                          where c.Field<string>("ProjectName").ToLower().Trim() == txtProjectName.Text.ToLower().Trim()
                                          select new { ExistingRecord = c.Field<string>("ProjectName") }).ToArray();
                    if (ExistingRecord.Any())
                    {
                        Messages.InformationMessage("Record already exist");
                        txtProjectName.Focus();
                        return;
                    }
                }
                Save();
            }
            else
            {
                if (txtProjectName.Text == null || txtProjectName.Text == string.Empty)
                {
                    Messages.InformationMessage("Project name cannot be empty");
                    txtProjectName.Focus();
                    return;
                }
                Edit();
            }
        }

        private void Save()
        {
            ProjectInfo ProjectInfo = new ProjectInfo();
            ProjectSP ProjectSP = new ProjectSP();
            ProjectInfo.ProjectName = txtProjectName.Text;
            ProjectInfo.CreatedBy = Convert.ToInt32(PublicVariables._decCurrentUserId);
            if (txtDate.Text != string.Empty && txtDate.Text != "")
            {
                ProjectInfo.CreatedOn = Convert.ToDateTime(txtDate.Text);
            }
            else
            {
                ProjectInfo.CreatedOn = DateTime.Now.Date;
            }
            ProjectInfo.ModifiedOn = DateTime.Now;
            if (ProjectSP.ProjectAdd(ProjectInfo))
            {
                Messages.SavedMessage();
                Clear();
                fillGrid();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void Clear()
        {
            try
            {
                txtProjectName.Text = string.Empty;
                txtDate.Text = string.Empty;
            }
            catch (Exception ex) { }
        }

        private void fillGrid()
        {
            try
            {
                //dgvProject.Rows.Clear();
                ProjectSP ProjectSP = new ProjectSP();
                DataTable dt = ProjectSP.ProjectViewAll();
                if (dt != null && dt.Rows.Count > 0)
                {
                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    dgvProject.Rows.Add();
                    //    //this.dgvCategories.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                    //    //this.dgvCategories.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();
                    //    dgvProject.Rows[dgvProject.Rows.Count - 1].Cells["dgvtxtProjectName"].Value = dr["ProjectId"];
                    //    dgvProject.Rows[dgvProject.Rows.Count - 1].Cells["dgvtxtProjectName"].Value = dr["ProjectName"];
                    //}
                    var ProjectList = (from c in dt.AsEnumerable()
                                       orderby c.Field<string>("ProjectName")
                                       select new
                                       {
                                           ProjectId = c.Field<int>("ProjectId"),
                                           ProjectName = c.Field<string>("ProjectName").ToString()
                                           ,CreatedOn = c.Field<DateTime>("CreatedOn").ToString()
                                            }).ToList();

                    dgvProject.DataSource = ProjectList;
                }
            }

            catch (Exception ex)
            {

            }
        }

        private void Edit()
        {
            ProjectInfo ProjectInfo = new ProjectInfo();
            ProjectSP ProjectSP = new ProjectSP();
            ProjectInfo.ProjectId = ProjectIdToUpdate;
            ProjectInfo.ProjectName = txtProjectName.Text;
            ProjectInfo.ModifiedBy = Convert.ToInt32(PublicVariables._decCurrencyId);
            if (txtDate.Text != string.Empty && txtDate.Text != "")
            {
                ProjectInfo.ModifiedOn = Convert.ToDateTime(txtDate.Text);
            }
            else
            {
                ProjectInfo.ModifiedOn = DateTime.Now.Date;
            }
            if (ProjectSP.ProjectUpdate(ProjectInfo))
            {
                Messages.UpdatedMessage();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProjectName.Text == null || txtProjectName.Text == string.Empty)
                {
                    Messages.InformationMessage("select project to delete");
                    txtProjectName.Focus();
                    return;
                }
                DeleteFunction();
            }
            catch (Exception ex) { }
        }

        private void DeleteFunction()
        {
            ProjectInfo ProjectInfo = new ProjectInfo();
            ProjectSP ProjectSP = new ProjectSP();
            ProjectInfo.ProjectId = ProjectIdToUpdate;
            if (ProjectSP.ProjectDelete(ProjectInfo))
            {
                Messages.DeletedMessage();
            }
            else
            {
                Messages.ErrorMessage("Error in operation! check data and try again.");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex) { }
        }
        private void FillControls()
        {
            try
            {
                txtProjectName.Text = ProjectNameToUpdate;
                txtDate.Text = CreatedOn.ToString();
            }
            catch (Exception ex)
            {

            }
        }

        private void dgvProject_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    ProjectIdToUpdate = Convert.ToInt32(dgvProject.Rows[e.RowIndex].Cells["dgvtxtProjectId"].Value);
                    ProjectNameToUpdate = dgvProject.Rows[e.RowIndex].Cells["dgvtxtProjectName"].Value.ToString();
                    CreatedOn = Convert.ToDateTime(dgvProject.Rows[e.RowIndex].Cells["CreatedOn"].Value.ToString());
                    Clear();
                    FillControls();
                    btnSave.Text = "Update";
                    isEditMode = true;
                    txtProjectName.Focus();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpDate.Value;
                this.txtDate.Text = date.ToString("dd-MMM-yyyy");

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "Project:" + ex.Message;
            }
        }
    }
}
