﻿namespace MATFinancials
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomer));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxActive = new System.Windows.Forms.CheckBox();
            this.lblCustomerNameValidator = new System.Windows.Forms.Label();
            this.txtBranchCode = new System.Windows.Forms.TextBox();
            this.btnRoutAdd = new System.Windows.Forms.Button();
            this.btnAreaAdd = new System.Windows.Forms.Button();
            this.btnPricingLevelAdd = new System.Windows.Forms.Button();
            this.lblRout = new System.Windows.Forms.Label();
            this.cmbRoute = new System.Windows.Forms.ComboBox();
            this.txtPan = new System.Windows.Forms.TextBox();
            this.txtCST = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTin = new System.Windows.Forms.TextBox();
            this.lblTin = new System.Windows.Forms.Label();
            this.txtCreditPeriod = new System.Windows.Forms.TextBox();
            this.cmbBillbyBill = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblPricingLevel = new System.Windows.Forms.Label();
            this.cmbPricingLevel = new System.Windows.Forms.ComboBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.lblBranchCode = new System.Windows.Forms.Label();
            this.lblphone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblMobile = new System.Windows.Forms.Label();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.lblAccountNo = new System.Windows.Forms.Label();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.cmbDrorCr = new System.Windows.Forms.ComboBox();
            this.lblMailingName = new System.Windows.Forms.Label();
            this.txtMailingName = new System.Windows.Forms.TextBox();
            this.txtBranchName = new System.Windows.Forms.TextBox();
            this.lblBranchName = new System.Windows.Forms.Label();
            this.lblArea = new System.Windows.Forms.Label();
            this.txtCreditLimit = new System.Windows.Forms.TextBox();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.lblOpeningBalance = new System.Windows.Forms.Label();
            this.txtOpeningBalance = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.lblbBillbyBill = new System.Windows.Forms.Label();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.cmbArea = new System.Windows.Forms.ComboBox();
            this.lblPan = new System.Windows.Forms.Label();
            this.lblNarration = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvCustomerTransactionDetails = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblRoutSearch = new System.Windows.Forms.Label();
            this.cmbRoutSearch = new System.Windows.Forms.ComboBox();
            this.lblAreaSearch = new System.Windows.Forms.Label();
            this.cmbAreaSearch = new System.Windows.Forms.ComboBox();
            this.txtCustomerNameSearch = new System.Windows.Forms.TextBox();
            this.lblCustomerNameSearch = new System.Windows.Forms.Label();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.SlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtledgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtOpeningBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbBalancesToShow = new System.Windows.Forms.ComboBox();
            this.cmbLedgersToShow = new System.Windows.Forms.ComboBox();
            this.lblShow = new System.Windows.Forms.Label();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCustomerBalance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbQuicklaunch = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtvoucherTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.masterId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtInvoiceNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Memo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLedgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ledgerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerTransactionDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.cbxActive);
            this.groupBox1.Controls.Add(this.lblCustomerNameValidator);
            this.groupBox1.Controls.Add(this.txtBranchCode);
            this.groupBox1.Controls.Add(this.btnRoutAdd);
            this.groupBox1.Controls.Add(this.btnAreaAdd);
            this.groupBox1.Controls.Add(this.btnPricingLevelAdd);
            this.groupBox1.Controls.Add(this.lblRout);
            this.groupBox1.Controls.Add(this.cmbRoute);
            this.groupBox1.Controls.Add(this.txtPan);
            this.groupBox1.Controls.Add(this.txtCST);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtTin);
            this.groupBox1.Controls.Add(this.lblTin);
            this.groupBox1.Controls.Add(this.txtCreditPeriod);
            this.groupBox1.Controls.Add(this.cmbBillbyBill);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.txtNarration);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.lblPricingLevel);
            this.groupBox1.Controls.Add(this.cmbPricingLevel);
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.txtphone);
            this.groupBox1.Controls.Add(this.lblBranchCode);
            this.groupBox1.Controls.Add(this.lblphone);
            this.groupBox1.Controls.Add(this.lblAddress);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.lblMobile);
            this.groupBox1.Controls.Add(this.txtMobile);
            this.groupBox1.Controls.Add(this.lblAccountNo);
            this.groupBox1.Controls.Add(this.txtAccountNo);
            this.groupBox1.Controls.Add(this.cmbDrorCr);
            this.groupBox1.Controls.Add(this.lblMailingName);
            this.groupBox1.Controls.Add(this.txtMailingName);
            this.groupBox1.Controls.Add(this.txtBranchName);
            this.groupBox1.Controls.Add(this.lblBranchName);
            this.groupBox1.Controls.Add(this.lblArea);
            this.groupBox1.Controls.Add(this.txtCreditLimit);
            this.groupBox1.Controls.Add(this.lblCreditLimit);
            this.groupBox1.Controls.Add(this.lblOpeningBalance);
            this.groupBox1.Controls.Add(this.txtOpeningBalance);
            this.groupBox1.Controls.Add(this.txtCustomerName);
            this.groupBox1.Controls.Add(this.lblbBillbyBill);
            this.groupBox1.Controls.Add(this.lblCustomerName);
            this.groupBox1.Controls.Add(this.cmbArea);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(415, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 284);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // cbxActive
            // 
            this.cbxActive.AutoSize = true;
            this.cbxActive.ForeColor = System.Drawing.Color.Black;
            this.cbxActive.Location = new System.Drawing.Point(419, 193);
            this.cbxActive.Name = "cbxActive";
            this.cbxActive.Size = new System.Drawing.Size(56, 17);
            this.cbxActive.TabIndex = 1320;
            this.cbxActive.Text = "Active";
            this.cbxActive.UseVisualStyleBackColor = true;
            // 
            // lblCustomerNameValidator
            // 
            this.lblCustomerNameValidator.AutoSize = true;
            this.lblCustomerNameValidator.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerNameValidator.Location = new System.Drawing.Point(301, 11);
            this.lblCustomerNameValidator.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCustomerNameValidator.Name = "lblCustomerNameValidator";
            this.lblCustomerNameValidator.Size = new System.Drawing.Size(11, 13);
            this.lblCustomerNameValidator.TabIndex = 332;
            this.lblCustomerNameValidator.Text = "*";
            // 
            // txtBranchCode
            // 
            this.txtBranchCode.Location = new System.Drawing.Point(99, 31);
            this.txtBranchCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBranchCode.Name = "txtBranchCode";
            this.txtBranchCode.Size = new System.Drawing.Size(200, 20);
            this.txtBranchCode.TabIndex = 6;
            this.txtBranchCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBranchCode_KeyDown);
            // 
            // btnRoutAdd
            // 
            this.btnRoutAdd.BackColor = System.Drawing.Color.Gray;
            this.btnRoutAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRoutAdd.FlatAppearance.BorderSize = 0;
            this.btnRoutAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoutAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoutAdd.ForeColor = System.Drawing.Color.DarkRed;
            this.btnRoutAdd.Location = new System.Drawing.Point(600, 166);
            this.btnRoutAdd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnRoutAdd.Name = "btnRoutAdd";
            this.btnRoutAdd.Size = new System.Drawing.Size(20, 20);
            this.btnRoutAdd.TabIndex = 23;
            this.btnRoutAdd.Text = "+";
            this.btnRoutAdd.UseVisualStyleBackColor = false;
            this.btnRoutAdd.Click += new System.EventHandler(this.btnRoutAdd_Click);
            // 
            // btnAreaAdd
            // 
            this.btnAreaAdd.BackColor = System.Drawing.Color.Gray;
            this.btnAreaAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAreaAdd.FlatAppearance.BorderSize = 0;
            this.btnAreaAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAreaAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAreaAdd.ForeColor = System.Drawing.Color.DarkRed;
            this.btnAreaAdd.Location = new System.Drawing.Point(279, 237);
            this.btnAreaAdd.Name = "btnAreaAdd";
            this.btnAreaAdd.Size = new System.Drawing.Size(20, 20);
            this.btnAreaAdd.TabIndex = 20;
            this.btnAreaAdd.Text = "+";
            this.btnAreaAdd.UseVisualStyleBackColor = false;
            this.btnAreaAdd.Click += new System.EventHandler(this.btnAreaAdd_Click);
            // 
            // btnPricingLevelAdd
            // 
            this.btnPricingLevelAdd.BackColor = System.Drawing.Color.Gray;
            this.btnPricingLevelAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPricingLevelAdd.FlatAppearance.BorderSize = 0;
            this.btnPricingLevelAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPricingLevelAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPricingLevelAdd.ForeColor = System.Drawing.Color.DarkRed;
            this.btnPricingLevelAdd.Location = new System.Drawing.Point(599, 97);
            this.btnPricingLevelAdd.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.btnPricingLevelAdd.Name = "btnPricingLevelAdd";
            this.btnPricingLevelAdd.Size = new System.Drawing.Size(20, 20);
            this.btnPricingLevelAdd.TabIndex = 12;
            this.btnPricingLevelAdd.Text = "+";
            this.btnPricingLevelAdd.UseVisualStyleBackColor = false;
            this.btnPricingLevelAdd.Click += new System.EventHandler(this.btnPricingLevelAdd_Click);
            // 
            // lblRout
            // 
            this.lblRout.AutoSize = true;
            this.lblRout.ForeColor = System.Drawing.Color.Black;
            this.lblRout.Location = new System.Drawing.Point(385, 169);
            this.lblRout.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblRout.Name = "lblRout";
            this.lblRout.Size = new System.Drawing.Size(24, 13);
            this.lblRout.TabIndex = 331;
            this.lblRout.Text = "City";
            // 
            // cmbRoute
            // 
            this.cmbRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRoute.FormattingEnabled = true;
            this.cmbRoute.Location = new System.Drawing.Point(419, 166);
            this.cmbRoute.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbRoute.Name = "cmbRoute";
            this.cmbRoute.Size = new System.Drawing.Size(176, 21);
            this.cmbRoute.TabIndex = 22;
            this.cmbRoute.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRoute_KeyDown);
            // 
            // txtPan
            // 
            this.txtPan.Location = new System.Drawing.Point(99, 232);
            this.txtPan.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtPan.MaxLength = 20;
            this.txtPan.Name = "txtPan";
            this.txtPan.Size = new System.Drawing.Size(20, 20);
            this.txtPan.TabIndex = 18;
            this.txtPan.Visible = false;
            this.txtPan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPan_KeyDown);
            // 
            // txtCST
            // 
            this.txtCST.Location = new System.Drawing.Point(99, 212);
            this.txtCST.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCST.MaxLength = 20;
            this.txtCST.Name = "txtCST";
            this.txtCST.Size = new System.Drawing.Size(200, 20);
            this.txtCST.TabIndex = 17;
            this.txtCST.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCST_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(44, 215);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 329;
            this.label19.Text = "Position";
            // 
            // txtTin
            // 
            this.txtTin.Location = new System.Drawing.Point(419, 143);
            this.txtTin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtTin.MaxLength = 20;
            this.txtTin.Name = "txtTin";
            this.txtTin.Size = new System.Drawing.Size(200, 20);
            this.txtTin.TabIndex = 16;
            this.txtTin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTin_KeyDown);
            // 
            // lblTin
            // 
            this.lblTin.AutoSize = true;
            this.lblTin.ForeColor = System.Drawing.Color.Black;
            this.lblTin.Location = new System.Drawing.Point(384, 150);
            this.lblTin.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblTin.Name = "lblTin";
            this.lblTin.Size = new System.Drawing.Size(25, 13);
            this.lblTin.TabIndex = 328;
            this.lblTin.Text = "TIN";
            // 
            // txtCreditPeriod
            // 
            this.txtCreditPeriod.Location = new System.Drawing.Point(419, 120);
            this.txtCreditPeriod.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCreditPeriod.MaxLength = 15;
            this.txtCreditPeriod.Name = "txtCreditPeriod";
            this.txtCreditPeriod.Size = new System.Drawing.Size(200, 20);
            this.txtCreditPeriod.TabIndex = 14;
            this.txtCreditPeriod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditPeriod_KeyDown);
            this.txtCreditPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCreditPeriod_KeyPress);
            // 
            // cmbBillbyBill
            // 
            this.cmbBillbyBill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBillbyBill.FormattingEnabled = true;
            this.cmbBillbyBill.Items.AddRange(new object[] {
            "Yes"});
            this.cmbBillbyBill.Location = new System.Drawing.Point(99, 161);
            this.cmbBillbyBill.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbBillbyBill.Name = "cmbBillbyBill";
            this.cmbBillbyBill.Size = new System.Drawing.Size(200, 21);
            this.cmbBillbyBill.TabIndex = 13;
            this.cmbBillbyBill.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBillbyBill_KeyDown);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(545, 237);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(77, 27);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(467, 237);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(77, 27);
            this.btnDelete.TabIndex = 26;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(309, 237);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 27);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(388, 237);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(77, 27);
            this.btnClear.TabIndex = 25;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(90, 237);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(61, 12);
            this.txtNarration.TabIndex = 21;
            this.txtNarration.Visible = false;
            this.txtNarration.Enter += new System.EventHandler(this.txtNarration_Enter);
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            this.txtNarration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNarration_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(342, 127);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 13);
            this.label16.TabIndex = 326;
            this.label16.Text = "Credit Period";
            // 
            // lblPricingLevel
            // 
            this.lblPricingLevel.AutoSize = true;
            this.lblPricingLevel.ForeColor = System.Drawing.Color.Black;
            this.lblPricingLevel.Location = new System.Drawing.Point(341, 102);
            this.lblPricingLevel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblPricingLevel.Name = "lblPricingLevel";
            this.lblPricingLevel.Size = new System.Drawing.Size(68, 13);
            this.lblPricingLevel.TabIndex = 325;
            this.lblPricingLevel.Text = "Pricing Level";
            // 
            // cmbPricingLevel
            // 
            this.cmbPricingLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPricingLevel.FormattingEnabled = true;
            this.cmbPricingLevel.Location = new System.Drawing.Point(419, 96);
            this.cmbPricingLevel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbPricingLevel.Name = "cmbPricingLevel";
            this.cmbPricingLevel.Size = new System.Drawing.Size(180, 21);
            this.cmbPricingLevel.TabIndex = 11;
            this.cmbPricingLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPricingLevel_KeyDown);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.ForeColor = System.Drawing.Color.Black;
            this.lblEmail.Location = new System.Drawing.Point(374, 75);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 324;
            this.lblEmail.Text = "E-mail";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(419, 72);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(200, 20);
            this.txtEmail.TabIndex = 10;
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail_KeyDown);
            // 
            // txtphone
            // 
            this.txtphone.Location = new System.Drawing.Point(419, 49);
            this.txtphone.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtphone.MaxLength = 15;
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(200, 20);
            this.txtphone.TabIndex = 8;
            this.txtphone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtphone_KeyDown);
            // 
            // lblBranchCode
            // 
            this.lblBranchCode.AutoSize = true;
            this.lblBranchCode.ForeColor = System.Drawing.Color.Black;
            this.lblBranchCode.Location = new System.Drawing.Point(20, 35);
            this.lblBranchCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBranchCode.Name = "lblBranchCode";
            this.lblBranchCode.Size = new System.Drawing.Size(69, 13);
            this.lblBranchCode.TabIndex = 322;
            this.lblBranchCode.Text = "Branch Code";
            // 
            // lblphone
            // 
            this.lblphone.AutoSize = true;
            this.lblphone.ForeColor = System.Drawing.Color.Black;
            this.lblphone.Location = new System.Drawing.Point(371, 53);
            this.lblphone.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblphone.Name = "lblphone";
            this.lblphone.Size = new System.Drawing.Size(38, 13);
            this.lblphone.TabIndex = 323;
            this.lblphone.Text = "Phone";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.ForeColor = System.Drawing.Color.Black;
            this.lblAddress.Location = new System.Drawing.Point(43, 112);
            this.lblAddress.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(45, 13);
            this.lblAddress.TabIndex = 321;
            this.lblAddress.Text = "Address";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(99, 106);
            this.txtAddress.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(200, 50);
            this.txtAddress.TabIndex = 9;
            this.txtAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddress_KeyDown);
            this.txtAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAddress_KeyPress);
            // 
            // lblMobile
            // 
            this.lblMobile.AutoSize = true;
            this.lblMobile.ForeColor = System.Drawing.Color.Black;
            this.lblMobile.Location = new System.Drawing.Point(50, 87);
            this.lblMobile.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(38, 13);
            this.lblMobile.TabIndex = 320;
            this.lblMobile.Text = "Mobile";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(99, 81);
            this.txtMobile.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtMobile.MaxLength = 15;
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(200, 20);
            this.txtMobile.TabIndex = 7;
            this.txtMobile.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMobile_KeyDown);
            // 
            // lblAccountNo
            // 
            this.lblAccountNo.AutoSize = true;
            this.lblAccountNo.ForeColor = System.Drawing.Color.Black;
            this.lblAccountNo.Location = new System.Drawing.Point(366, 28);
            this.lblAccountNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblAccountNo.Name = "lblAccountNo";
            this.lblAccountNo.Size = new System.Drawing.Size(43, 13);
            this.lblAccountNo.TabIndex = 319;
            this.lblAccountNo.Text = "A/c no.";
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Location = new System.Drawing.Point(419, 25);
            this.txtAccountNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtAccountNo.MaxLength = 20;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Size = new System.Drawing.Size(200, 20);
            this.txtAccountNo.TabIndex = 4;
            this.txtAccountNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAccountNo_KeyDown);
            // 
            // cmbDrorCr
            // 
            this.cmbDrorCr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDrorCr.Enabled = false;
            this.cmbDrorCr.FormattingEnabled = true;
            this.cmbDrorCr.Items.AddRange(new object[] {
            "Dr",
            "Cr"});
            this.cmbDrorCr.Location = new System.Drawing.Point(546, 210);
            this.cmbDrorCr.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbDrorCr.Name = "cmbDrorCr";
            this.cmbDrorCr.Size = new System.Drawing.Size(36, 21);
            this.cmbDrorCr.TabIndex = 3;
            this.cmbDrorCr.Visible = false;
            this.cmbDrorCr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDrorCr_KeyDown);
            // 
            // lblMailingName
            // 
            this.lblMailingName.AutoSize = true;
            this.lblMailingName.ForeColor = System.Drawing.Color.Black;
            this.lblMailingName.Location = new System.Drawing.Point(327, 4);
            this.lblMailingName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblMailingName.Name = "lblMailingName";
            this.lblMailingName.Size = new System.Drawing.Size(82, 13);
            this.lblMailingName.TabIndex = 318;
            this.lblMailingName.Text = "Company Name";
            // 
            // txtMailingName
            // 
            this.txtMailingName.Location = new System.Drawing.Point(419, 0);
            this.txtMailingName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtMailingName.Name = "txtMailingName";
            this.txtMailingName.Size = new System.Drawing.Size(200, 20);
            this.txtMailingName.TabIndex = 1;
            this.txtMailingName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMailingName_KeyDown);
            // 
            // txtBranchName
            // 
            this.txtBranchName.Location = new System.Drawing.Point(99, 56);
            this.txtBranchName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBranchName.Name = "txtBranchName";
            this.txtBranchName.Size = new System.Drawing.Size(200, 20);
            this.txtBranchName.TabIndex = 5;
            this.txtBranchName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBranchName_KeyDown);
            // 
            // lblBranchName
            // 
            this.lblBranchName.AutoSize = true;
            this.lblBranchName.ForeColor = System.Drawing.Color.Black;
            this.lblBranchName.Location = new System.Drawing.Point(16, 63);
            this.lblBranchName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBranchName.Name = "lblBranchName";
            this.lblBranchName.Size = new System.Drawing.Size(72, 13);
            this.lblBranchName.TabIndex = 317;
            this.lblBranchName.Text = "Branch Name";
            // 
            // lblArea
            // 
            this.lblArea.AutoSize = true;
            this.lblArea.ForeColor = System.Drawing.Color.Black;
            this.lblArea.Location = new System.Drawing.Point(56, 240);
            this.lblArea.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(32, 13);
            this.lblArea.TabIndex = 316;
            this.lblArea.Text = "State";
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.Location = new System.Drawing.Point(99, 187);
            this.txtCreditLimit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCreditLimit.MaxLength = 15;
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.Size = new System.Drawing.Size(200, 20);
            this.txtCreditLimit.TabIndex = 15;
            this.txtCreditLimit.TextChanged += new System.EventHandler(this.txtCreditLimit_TextChanged);
            this.txtCreditLimit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCreditLimit_KeyDown);
            this.txtCreditLimit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCreditLimit_KeyPress);
            this.txtCreditLimit.Leave += new System.EventHandler(this.txtCreditLimit_Leave);
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.ForeColor = System.Drawing.Color.Black;
            this.lblCreditLimit.Location = new System.Drawing.Point(30, 190);
            this.lblCreditLimit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(58, 13);
            this.lblCreditLimit.TabIndex = 315;
            this.lblCreditLimit.Text = "Credit Limit";
            // 
            // lblOpeningBalance
            // 
            this.lblOpeningBalance.AutoSize = true;
            this.lblOpeningBalance.Enabled = false;
            this.lblOpeningBalance.ForeColor = System.Drawing.Color.Black;
            this.lblOpeningBalance.Location = new System.Drawing.Point(319, 213);
            this.lblOpeningBalance.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblOpeningBalance.Name = "lblOpeningBalance";
            this.lblOpeningBalance.Size = new System.Drawing.Size(89, 13);
            this.lblOpeningBalance.TabIndex = 314;
            this.lblOpeningBalance.Text = "Opening Balance";
            this.lblOpeningBalance.Visible = false;
            // 
            // txtOpeningBalance
            // 
            this.txtOpeningBalance.Enabled = false;
            this.txtOpeningBalance.Location = new System.Drawing.Point(419, 212);
            this.txtOpeningBalance.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtOpeningBalance.MaxLength = 10;
            this.txtOpeningBalance.Name = "txtOpeningBalance";
            this.txtOpeningBalance.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtOpeningBalance.Size = new System.Drawing.Size(117, 20);
            this.txtOpeningBalance.TabIndex = 2;
            this.txtOpeningBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOpeningBalance.Visible = false;
            this.txtOpeningBalance.Enter += new System.EventHandler(this.txtOpeningBalance_Enter);
            this.txtOpeningBalance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOpeningBalance_KeyDown);
            this.txtOpeningBalance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOpeningBalance_KeyPress_1);
            this.txtOpeningBalance.Leave += new System.EventHandler(this.txtOpeningBalance_Leave);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(99, 6);
            this.txtCustomerName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(200, 20);
            this.txtCustomerName.TabIndex = 0;
            this.txtCustomerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerName_KeyDown);
            // 
            // lblbBillbyBill
            // 
            this.lblbBillbyBill.AutoSize = true;
            this.lblbBillbyBill.ForeColor = System.Drawing.Color.Black;
            this.lblbBillbyBill.Location = new System.Drawing.Point(38, 164);
            this.lblbBillbyBill.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblbBillbyBill.Name = "lblbBillbyBill";
            this.lblbBillbyBill.Size = new System.Drawing.Size(50, 13);
            this.lblbBillbyBill.TabIndex = 313;
            this.lblbBillbyBill.Text = "Bill by Bill";
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.ForeColor = System.Drawing.Color.Black;
            this.lblCustomerName.Location = new System.Drawing.Point(6, 9);
            this.lblCustomerName.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(82, 13);
            this.lblCustomerName.TabIndex = 312;
            this.lblCustomerName.Text = "Customer Name";
            // 
            // cmbArea
            // 
            this.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbArea.FormattingEnabled = true;
            this.cmbArea.Location = new System.Drawing.Point(99, 237);
            this.cmbArea.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbArea.Name = "cmbArea";
            this.cmbArea.Size = new System.Drawing.Size(175, 21);
            this.cmbArea.TabIndex = 19;
            this.cmbArea.SelectedIndexChanged += new System.EventHandler(this.cmbArea_SelectedIndexChanged);
            this.cmbArea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbArea_KeyDown);
            // 
            // lblPan
            // 
            this.lblPan.AutoSize = true;
            this.lblPan.ForeColor = System.Drawing.Color.Black;
            this.lblPan.Location = new System.Drawing.Point(377, 493);
            this.lblPan.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblPan.Name = "lblPan";
            this.lblPan.Size = new System.Drawing.Size(30, 15);
            this.lblPan.TabIndex = 330;
            this.lblPan.Text = "PAN";
            this.lblPan.Visible = false;
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(347, 497);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(60, 15);
            this.lblNarration.TabIndex = 327;
            this.lblNarration.Text = "Narration";
            this.lblNarration.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.dgvCustomerTransactionDetails);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(415, 266);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(665, 218);
            this.groupBox2.TabIndex = 1147;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Maroon;
            this.label4.Location = new System.Drawing.Point(21, 3);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 18);
            this.label4.TabIndex = 337;
            this.label4.Text = "Transaction Details";
            // 
            // dgvCustomerTransactionDetails
            // 
            this.dgvCustomerTransactionDetails.AllowUserToAddRows = false;
            this.dgvCustomerTransactionDetails.AllowUserToDeleteRows = false;
            this.dgvCustomerTransactionDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustomerTransactionDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCustomerTransactionDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvCustomerTransactionDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCustomerTransactionDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomerTransactionDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtDate,
            this.txtvoucherTypeName,
            this.masterId,
            this.dgvtxtInvoiceNo,
            this.Memo,
            this.txtLedgerId,
            this.txtDebit,
            this.txtCredit,
            this.txtBalance,
            this.POS,
            this.ledgerId});
            this.dgvCustomerTransactionDetails.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCustomerTransactionDetails.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCustomerTransactionDetails.GridColor = System.Drawing.Color.White;
            this.dgvCustomerTransactionDetails.Location = new System.Drawing.Point(5, 20);
            this.dgvCustomerTransactionDetails.Name = "dgvCustomerTransactionDetails";
            this.dgvCustomerTransactionDetails.ReadOnly = true;
            this.dgvCustomerTransactionDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomerTransactionDetails.Size = new System.Drawing.Size(658, 193);
            this.dgvCustomerTransactionDetails.TabIndex = 0;
            this.dgvCustomerTransactionDetails.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCustomerTransactionDetails_CellDoubleClick);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Silver;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(317, 114);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 21);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblRoutSearch
            // 
            this.lblRoutSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoutSearch.ForeColor = System.Drawing.Color.Black;
            this.lblRoutSearch.Location = new System.Drawing.Point(85, 66);
            this.lblRoutSearch.Margin = new System.Windows.Forms.Padding(5);
            this.lblRoutSearch.Name = "lblRoutSearch";
            this.lblRoutSearch.Size = new System.Drawing.Size(37, 20);
            this.lblRoutSearch.TabIndex = 289;
            this.lblRoutSearch.Text = "City";
            // 
            // cmbRoutSearch
            // 
            this.cmbRoutSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRoutSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRoutSearch.FormattingEnabled = true;
            this.cmbRoutSearch.Location = new System.Drawing.Point(128, 61);
            this.cmbRoutSearch.Margin = new System.Windows.Forms.Padding(5);
            this.cmbRoutSearch.Name = "cmbRoutSearch";
            this.cmbRoutSearch.Size = new System.Drawing.Size(274, 23);
            this.cmbRoutSearch.TabIndex = 2;
            this.cmbRoutSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbRoutSearch_KeyDown);
            // 
            // lblAreaSearch
            // 
            this.lblAreaSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAreaSearch.ForeColor = System.Drawing.Color.Black;
            this.lblAreaSearch.Location = new System.Drawing.Point(86, 33);
            this.lblAreaSearch.Margin = new System.Windows.Forms.Padding(5);
            this.lblAreaSearch.Name = "lblAreaSearch";
            this.lblAreaSearch.Size = new System.Drawing.Size(36, 20);
            this.lblAreaSearch.TabIndex = 288;
            this.lblAreaSearch.Text = "State";
            // 
            // cmbAreaSearch
            // 
            this.cmbAreaSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAreaSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAreaSearch.FormattingEnabled = true;
            this.cmbAreaSearch.Location = new System.Drawing.Point(128, 33);
            this.cmbAreaSearch.Name = "cmbAreaSearch";
            this.cmbAreaSearch.Size = new System.Drawing.Size(274, 23);
            this.cmbAreaSearch.TabIndex = 1;
            this.cmbAreaSearch.SelectedIndexChanged += new System.EventHandler(this.cmbAreaSearch_SelectedIndexChanged);
            this.cmbAreaSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbAreaSearch_KeyDown);
            // 
            // txtCustomerNameSearch
            // 
            this.txtCustomerNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerNameSearch.Location = new System.Drawing.Point(128, 8);
            this.txtCustomerNameSearch.Margin = new System.Windows.Forms.Padding(5);
            this.txtCustomerNameSearch.Name = "txtCustomerNameSearch";
            this.txtCustomerNameSearch.Size = new System.Drawing.Size(274, 21);
            this.txtCustomerNameSearch.TabIndex = 0;
            this.txtCustomerNameSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerNameSearch_KeyDown);
            // 
            // lblCustomerNameSearch
            // 
            this.lblCustomerNameSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerNameSearch.ForeColor = System.Drawing.Color.Black;
            this.lblCustomerNameSearch.Location = new System.Drawing.Point(31, 7);
            this.lblCustomerNameSearch.Margin = new System.Windows.Forms.Padding(5);
            this.lblCustomerNameSearch.Name = "lblCustomerNameSearch";
            this.lblCustomerNameSearch.Size = new System.Drawing.Size(91, 20);
            this.lblCustomerNameSearch.TabIndex = 287;
            this.lblCustomerNameSearch.Text = "Customer Name ";
            this.lblCustomerNameSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AllowUserToAddRows = false;
            this.dgvCustomer.AllowUserToDeleteRows = false;
            this.dgvCustomer.AllowUserToResizeColumns = false;
            this.dgvCustomer.AllowUserToResizeRows = false;
            this.dgvCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustomer.BackgroundColor = System.Drawing.Color.White;
            this.dgvCustomer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCustomer.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCustomer.ColumnHeadersHeight = 25;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SlNo,
            this.dgvtxtledgerId,
            this.dgvtxtCustomerName,
            this.dgvtxtOpeningBalance});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCustomer.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvCustomer.EnableHeadersVisualStyles = false;
            this.dgvCustomer.GridColor = System.Drawing.Color.White;
            this.dgvCustomer.Location = new System.Drawing.Point(6, 138);
            this.dgvCustomer.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dgvCustomer.MultiSelect = false;
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.ReadOnly = true;
            this.dgvCustomer.RowHeadersVisible = false;
            this.dgvCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomer.Size = new System.Drawing.Size(398, 343);
            this.dgvCustomer.TabIndex = 286;
            this.dgvCustomer.TabStop = false;
            this.dgvCustomer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCustomer_CellClick);
            this.dgvCustomer.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCustomer_CellDoubleClick);
            // 
            // SlNo
            // 
            this.SlNo.DataPropertyName = "Sl.No";
            this.SlNo.FillWeight = 36.73222F;
            this.SlNo.HeaderText = "Sl.No";
            this.SlNo.Name = "SlNo";
            this.SlNo.ReadOnly = true;
            this.SlNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtledgerId
            // 
            this.dgvtxtledgerId.DataPropertyName = "ledgerId";
            this.dgvtxtledgerId.HeaderText = "Ledger ID";
            this.dgvtxtledgerId.Name = "dgvtxtledgerId";
            this.dgvtxtledgerId.ReadOnly = true;
            this.dgvtxtledgerId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtledgerId.Visible = false;
            // 
            // dgvtxtCustomerName
            // 
            this.dgvtxtCustomerName.DataPropertyName = "ledgerName";
            this.dgvtxtCustomerName.FillWeight = 152.2843F;
            this.dgvtxtCustomerName.HeaderText = "Customer Name";
            this.dgvtxtCustomerName.Name = "dgvtxtCustomerName";
            this.dgvtxtCustomerName.ReadOnly = true;
            this.dgvtxtCustomerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtOpeningBalance
            // 
            this.dgvtxtOpeningBalance.DataPropertyName = "openingBalance";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.dgvtxtOpeningBalance.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvtxtOpeningBalance.FillWeight = 110.9835F;
            this.dgvtxtOpeningBalance.HeaderText = "Balance";
            this.dgvtxtOpeningBalance.Name = "dgvtxtOpeningBalance";
            this.dgvtxtOpeningBalance.ReadOnly = true;
            this.dgvtxtOpeningBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox4.Controls.Add(this.cmbBalancesToShow);
            this.groupBox4.Controls.Add(this.cmbLedgersToShow);
            this.groupBox4.Controls.Add(this.lblShow);
            this.groupBox4.Controls.Add(this.dtpToDate);
            this.groupBox4.Controls.Add(this.txtToDate);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtFromDate);
            this.groupBox4.Controls.Add(this.dtpFromDate);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.lblCustomerNameSearch);
            this.groupBox4.Controls.Add(this.dgvCustomer);
            this.groupBox4.Controls.Add(this.lblPan);
            this.groupBox4.Controls.Add(this.btnSearch);
            this.groupBox4.Controls.Add(this.txtCustomerNameSearch);
            this.groupBox4.Controls.Add(this.lblRoutSearch);
            this.groupBox4.Controls.Add(this.cmbRoutSearch);
            this.groupBox4.Controls.Add(this.cmbAreaSearch);
            this.groupBox4.Controls.Add(this.lblAreaSearch);
            this.groupBox4.Controls.Add(this.lblNarration);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(4, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(412, 519);
            this.groupBox4.TabIndex = 1148;
            this.groupBox4.TabStop = false;
            // 
            // cmbBalancesToShow
            // 
            this.cmbBalancesToShow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBalancesToShow.FormattingEnabled = true;
            this.cmbBalancesToShow.Items.AddRange(new object[] {
            "All",
            "Open Balances"});
            this.cmbBalancesToShow.Location = new System.Drawing.Point(262, 89);
            this.cmbBalancesToShow.Name = "cmbBalancesToShow";
            this.cmbBalancesToShow.Size = new System.Drawing.Size(140, 23);
            this.cmbBalancesToShow.TabIndex = 1320;
            // 
            // cmbLedgersToShow
            // 
            this.cmbLedgersToShow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLedgersToShow.FormattingEnabled = true;
            this.cmbLedgersToShow.Items.AddRange(new object[] {
            "All",
            "Active",
            "Inactive"});
            this.cmbLedgersToShow.Location = new System.Drawing.Point(128, 89);
            this.cmbLedgersToShow.Name = "cmbLedgersToShow";
            this.cmbLedgersToShow.Size = new System.Drawing.Size(128, 23);
            this.cmbLedgersToShow.TabIndex = 334;
            // 
            // lblShow
            // 
            this.lblShow.AutoSize = true;
            this.lblShow.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShow.Location = new System.Drawing.Point(84, 92);
            this.lblShow.Name = "lblShow";
            this.lblShow.Size = new System.Drawing.Size(38, 15);
            this.lblShow.TabIndex = 333;
            this.lblShow.Text = "Show";
            // 
            // dtpToDate
            // 
            this.dtpToDate.Location = new System.Drawing.Point(150, 112);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(22, 21);
            this.dtpToDate.TabIndex = 1312;
            this.dtpToDate.Visible = false;
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(100, 112);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(5);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.ReadOnly = true;
            this.txtToDate.Size = new System.Drawing.Size(42, 21);
            this.txtToDate.TabIndex = 1311;
            this.txtToDate.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Cambria", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(27, 486);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(310, 32);
            this.label3.TabIndex = 1149;
            this.label3.Text = "                                        Hint: \r\nDouble Click on A Customer To vie" +
    "w Transaction Details";
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(30, 111);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.ReadOnly = true;
            this.txtFromDate.Size = new System.Drawing.Size(42, 21);
            this.txtFromDate.TabIndex = 1309;
            this.txtFromDate.Visible = false;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Location = new System.Drawing.Point(76, 112);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(19, 21);
            this.dtpFromDate.TabIndex = 1310;
            this.dtpFromDate.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(732, 395);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1149;
            this.label2.Text = "Total Balance:";
            // 
            // txtCustomerBalance
            // 
            this.txtCustomerBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerBalance.Location = new System.Drawing.Point(901, 494);
            this.txtCustomerBalance.Margin = new System.Windows.Forms.Padding(5);
            this.txtCustomerBalance.Name = "txtCustomerBalance";
            this.txtCustomerBalance.ReadOnly = true;
            this.txtCustomerBalance.Size = new System.Drawing.Size(179, 21);
            this.txtCustomerBalance.TabIndex = 290;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(837, 499);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 333;
            this.label1.Text = "Total Bal:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(465, 492);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 27);
            this.button1.TabIndex = 1149;
            this.button1.Text = "Export Customer Statement";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbQuicklaunch
            // 
            this.cmbQuicklaunch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbQuicklaunch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbQuicklaunch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbQuicklaunch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbQuicklaunch.FormattingEnabled = true;
            this.cmbQuicklaunch.Items.AddRange(new object[] {
            "Sales Quotation",
            "Sales Order",
            "Delivery Note",
            "Rejection In",
            "Sales Invoice",
            "Sales Return",
            "Receipts"});
            this.cmbQuicklaunch.Location = new System.Drawing.Point(627, 494);
            this.cmbQuicklaunch.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbQuicklaunch.Name = "cmbQuicklaunch";
            this.cmbQuicklaunch.Size = new System.Drawing.Size(209, 21);
            this.cmbQuicklaunch.TabIndex = 1150;
            this.cmbQuicklaunch.Text = "---Manage Transactions---";
            this.cmbQuicklaunch.SelectedIndexChanged += new System.EventHandler(this.cmbQuicklaunch_SelectedIndexChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(415, 492);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(48, 27);
            this.btnPrint.TabIndex = 1151;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtDate
            // 
            this.txtDate.DataPropertyName = "txtDate";
            this.txtDate.HeaderText = "Date";
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtvoucherTypeName
            // 
            this.txtvoucherTypeName.DataPropertyName = "txtvoucherTypeName";
            this.txtvoucherTypeName.HeaderText = "Type";
            this.txtvoucherTypeName.Name = "txtvoucherTypeName";
            this.txtvoucherTypeName.ReadOnly = true;
            this.txtvoucherTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // masterId
            // 
            this.masterId.HeaderText = "MasterId";
            this.masterId.Name = "masterId";
            this.masterId.ReadOnly = true;
            this.masterId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.masterId.Visible = false;
            // 
            // dgvtxtInvoiceNo
            // 
            this.dgvtxtInvoiceNo.DataPropertyName = "dgvtxtInvoiceNo";
            this.dgvtxtInvoiceNo.HeaderText = "Num";
            this.dgvtxtInvoiceNo.Name = "dgvtxtInvoiceNo";
            this.dgvtxtInvoiceNo.ReadOnly = true;
            this.dgvtxtInvoiceNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Memo
            // 
            this.Memo.DataPropertyName = "Memo";
            this.Memo.HeaderText = "Memo";
            this.Memo.Name = "Memo";
            this.Memo.ReadOnly = true;
            this.Memo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtLedgerId
            // 
            this.txtLedgerId.DataPropertyName = "txtLedgerId";
            this.txtLedgerId.HeaderText = "Number";
            this.txtLedgerId.Name = "txtLedgerId";
            this.txtLedgerId.ReadOnly = true;
            this.txtLedgerId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtLedgerId.Visible = false;
            // 
            // txtDebit
            // 
            this.txtDebit.DataPropertyName = "txtDebit";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.txtDebit.DefaultCellStyle = dataGridViewCellStyle1;
            this.txtDebit.HeaderText = "Debit";
            this.txtDebit.Name = "txtDebit";
            this.txtDebit.ReadOnly = true;
            this.txtDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtCredit
            // 
            this.txtCredit.DataPropertyName = "txtCredit";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.txtCredit.DefaultCellStyle = dataGridViewCellStyle2;
            this.txtCredit.HeaderText = "Credit";
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.ReadOnly = true;
            this.txtCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtBalance
            // 
            this.txtBalance.DataPropertyName = "txtBalance";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.txtBalance.DefaultCellStyle = dataGridViewCellStyle3;
            this.txtBalance.HeaderText = "Balance";
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // POS
            // 
            this.POS.HeaderText = "POS";
            this.POS.Name = "POS";
            this.POS.ReadOnly = true;
            this.POS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.POS.Visible = false;
            // 
            // ledgerId
            // 
            this.ledgerId.HeaderText = "Ledger Id";
            this.ledgerId.Name = "ledgerId";
            this.ledgerId.ReadOnly = true;
            this.ledgerId.Visible = false;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 521);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.cmbQuicklaunch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCustomerBalance);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmCustomer";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Center";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCustomer_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerTransactionDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCustomerNameValidator;
        private System.Windows.Forms.TextBox txtBranchCode;
        private System.Windows.Forms.Button btnRoutAdd;
        private System.Windows.Forms.Button btnAreaAdd;
        private System.Windows.Forms.Button btnPricingLevelAdd;
        private System.Windows.Forms.Label lblRout;
        private System.Windows.Forms.ComboBox cmbRoute;
        private System.Windows.Forms.TextBox txtPan;
        private System.Windows.Forms.Label lblPan;
        private System.Windows.Forms.TextBox txtCST;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTin;
        private System.Windows.Forms.Label lblTin;
        private System.Windows.Forms.TextBox txtCreditPeriod;
        private System.Windows.Forms.ComboBox cmbBillbyBill;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblPricingLevel;
        public System.Windows.Forms.ComboBox cmbPricingLevel;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.Label lblBranchCode;
        private System.Windows.Forms.Label lblphone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label lblAccountNo;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.ComboBox cmbDrorCr;
        private System.Windows.Forms.Label lblMailingName;
        private System.Windows.Forms.TextBox txtMailingName;
        private System.Windows.Forms.TextBox txtBranchName;
        private System.Windows.Forms.Label lblBranchName;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.TextBox txtCreditLimit;
        private System.Windows.Forms.Label lblCreditLimit;
        private System.Windows.Forms.Label lblOpeningBalance;
        private System.Windows.Forms.TextBox txtOpeningBalance;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label lblbBillbyBill;
        private System.Windows.Forms.Label lblCustomerName;
        public System.Windows.Forms.ComboBox cmbArea;
        private System.Windows.Forms.GroupBox btgroupBox2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblRoutSearch;
        private System.Windows.Forms.Label lblAreaSearch;
        private System.Windows.Forms.ComboBox cmbAreaSearch;
        private System.Windows.Forms.TextBox txtCustomerNameSearch;
        private System.Windows.Forms.Label lblCustomerNameSearch;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.ComboBox cmbRoutSearch;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvCustomerTransactionDetails;
        private System.Windows.Forms.TextBox txtCustomerBalance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtledgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtCustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtOpeningBalance;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cmbQuicklaunch;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ComboBox cmbLedgersToShow;
        private System.Windows.Forms.Label lblShow;
        private System.Windows.Forms.CheckBox cbxActive;
        private System.Windows.Forms.ComboBox cmbBalancesToShow;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtvoucherTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn masterId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtInvoiceNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Memo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtLedgerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn POS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ledgerId;
    }
}