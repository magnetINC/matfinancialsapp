﻿using System;

namespace MATFinancials
{
    partial class frmProductCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductCreation));
            this.cmbSize = new System.Windows.Forms.ComboBox();
            this.lblSize = new System.Windows.Forms.Label();
            this.txtMrp = new System.Windows.Forms.TextBox();
            this.txtPurchaseRate = new System.Windows.Forms.TextBox();
            this.lblMrp = new System.Windows.Forms.Label();
            this.lblPurchaseRate = new System.Windows.Forms.Label();
            this.lblBrand = new System.Windows.Forms.Label();
            this.txtProductCode = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblProductCode = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.dgvProductCreation = new MATFinancials.dgv.DataGridViewEnter();
            this.dgvtxtslno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbtgodown = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvcmbrack = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtbatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxManfDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtExpDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtqty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcmbUnit = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvtxtbatchId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtstockpostId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtCheck = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtunit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCmbProject = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvCmbCategory = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cmbTax = new System.Windows.Forms.ComboBox();
            this.lblTax = new System.Windows.Forms.Label();
            this.txtMaximumStock = new System.Windows.Forms.TextBox();
            this.lblMaximumStock = new System.Windows.Forms.Label();
            this.cmbAllowBatch = new System.Windows.Forms.ComboBox();
            this.lblAllowBatch = new System.Windows.Forms.Label();
            this.cmbDefaultGodown = new System.Windows.Forms.ComboBox();
            this.lblDefaultGodown = new System.Windows.Forms.Label();
            this.cmbBom = new System.Windows.Forms.ComboBox();
            this.lblBom = new System.Windows.Forms.Label();
            this.btnBrandAdd = new System.Windows.Forms.Button();
            this.btnSizeAdd = new System.Windows.Forms.Button();
            this.btnTaxAdd = new System.Windows.Forms.Button();
            this.btnDefaultGodownAdd = new System.Windows.Forms.Button();
            this.btnUnitAdd = new System.Windows.Forms.Button();
            this.btnGroupAdd = new System.Windows.Forms.Button();
            this.cmbUnit = new System.Windows.Forms.ComboBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblGroup = new System.Windows.Forms.Label();
            this.cmbModalNo = new System.Windows.Forms.ComboBox();
            this.lblModalNo = new System.Windows.Forms.Label();
            this.btnModalNo = new System.Windows.Forms.Button();
            this.txtReorderLevel = new System.Windows.Forms.TextBox();
            this.lblReorderLevel = new System.Windows.Forms.Label();
            this.txtMinimumStock = new System.Windows.Forms.TextBox();
            this.txtSalesRate = new System.Windows.Forms.TextBox();
            this.lblMinimumStock = new System.Windows.Forms.Label();
            this.lblSalesRate = new System.Windows.Forms.Label();
            this.cmbTaxApplicableOn = new System.Windows.Forms.ComboBox();
            this.lblTaxApplicableOn = new System.Windows.Forms.Label();
            this.btnDefaultAdd = new System.Windows.Forms.Button();
            this.cmbDefaultRack = new System.Windows.Forms.ComboBox();
            this.lblDefaultRate = new System.Windows.Forms.Label();
            this.cmbMultipleUnit = new System.Windows.Forms.ComboBox();
            this.lblMultipleUnit = new System.Windows.Forms.Label();
            this.cmbOpeningStock = new System.Windows.Forms.ComboBox();
            this.lblOpeningStock = new System.Windows.Forms.Label();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.cbxActive = new System.Windows.Forms.CheckBox();
            this.cbxReminder = new System.Windows.Forms.CheckBox();
            this.cmbBrand = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.txtPartNo = new System.Windows.Forms.TextBox();
            this.lblPartNo = new System.Windows.Forms.Label();
            this.lnklblRemove = new System.Windows.Forms.LinkLabel();
            this.radProduct = new System.Windows.Forms.RadioButton();
            this.radService = new System.Windows.Forms.RadioButton();
            this.cmbSalesAccount = new System.Windows.Forms.ComboBox();
            this.lblSalesAccount = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbExpenseAccount = new System.Windows.Forms.ComboBox();
            this.chkAutomaticBarcode = new System.Windows.Forms.CheckBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.lblNarration = new System.Windows.Forms.Label();
            this.lbltotalInventoryValue = new System.Windows.Forms.Label();
            this.txtTotalInventoryValue = new System.Windows.Forms.TextBox();
            this.btnCreateSalesAccount = new System.Windows.Forms.Button();
            this.btnCreateExpenseAccount = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductCreation)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbSize
            // 
            this.cmbSize.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSize.FormattingEnabled = true;
            this.cmbSize.Location = new System.Drawing.Point(824, 54);
            this.cmbSize.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbSize.Name = "cmbSize";
            this.cmbSize.Size = new System.Drawing.Size(180, 21);
            this.cmbSize.TabIndex = 10;
            this.cmbSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSize_KeyDown);
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.BackColor = System.Drawing.Color.Transparent;
            this.lblSize.ForeColor = System.Drawing.Color.Black;
            this.lblSize.Location = new System.Drawing.Point(786, 61);
            this.lblSize.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(27, 13);
            this.lblSize.TabIndex = 422;
            this.lblSize.Text = "Size";
            this.lblSize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMrp
            // 
            this.txtMrp.Location = new System.Drawing.Point(824, 82);
            this.txtMrp.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtMrp.MaxLength = 13;
            this.txtMrp.Name = "txtMrp";
            this.txtMrp.Size = new System.Drawing.Size(180, 20);
            this.txtMrp.TabIndex = 12;
            this.txtMrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMrp.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMrp_MouseClick);
            this.txtMrp.TextChanged += new System.EventHandler(this.txtMrp_TextChanged);
            this.txtMrp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMrp_KeyDown);
            this.txtMrp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMrp_KeyPress);
            this.txtMrp.Leave += new System.EventHandler(this.txtMrp_Leave);
            // 
            // txtPurchaseRate
            // 
            this.txtPurchaseRate.Location = new System.Drawing.Point(155, 77);
            this.txtPurchaseRate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtPurchaseRate.MaxLength = 10;
            this.txtPurchaseRate.Name = "txtPurchaseRate";
            this.txtPurchaseRate.Size = new System.Drawing.Size(180, 20);
            this.txtPurchaseRate.TabIndex = 13;
            this.txtPurchaseRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPurchaseRate.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPurchaseRate_MouseClick);
            this.txtPurchaseRate.TextChanged += new System.EventHandler(this.txtPurchaseRate_TextChanged);
            this.txtPurchaseRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPurchaseRate_KeyDown);
            this.txtPurchaseRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPurchaseRate_KeyPress);
            this.txtPurchaseRate.Leave += new System.EventHandler(this.txtPurchaseRate_Leave);
            // 
            // lblMrp
            // 
            this.lblMrp.AutoSize = true;
            this.lblMrp.BackColor = System.Drawing.Color.Transparent;
            this.lblMrp.ForeColor = System.Drawing.Color.Black;
            this.lblMrp.Location = new System.Drawing.Point(782, 85);
            this.lblMrp.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMrp.Name = "lblMrp";
            this.lblMrp.Size = new System.Drawing.Size(31, 13);
            this.lblMrp.TabIndex = 414;
            this.lblMrp.Text = "MRP";
            this.lblMrp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPurchaseRate
            // 
            this.lblPurchaseRate.AutoSize = true;
            this.lblPurchaseRate.BackColor = System.Drawing.Color.Transparent;
            this.lblPurchaseRate.ForeColor = System.Drawing.Color.Black;
            this.lblPurchaseRate.Location = new System.Drawing.Point(62, 80);
            this.lblPurchaseRate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblPurchaseRate.Name = "lblPurchaseRate";
            this.lblPurchaseRate.Size = new System.Drawing.Size(78, 13);
            this.lblPurchaseRate.TabIndex = 413;
            this.lblPurchaseRate.Text = "Purchase Rate";
            this.lblPurchaseRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.BackColor = System.Drawing.Color.Transparent;
            this.lblBrand.ForeColor = System.Drawing.Color.Black;
            this.lblBrand.Location = new System.Drawing.Point(778, 35);
            this.lblBrand.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(35, 13);
            this.lblBrand.TabIndex = 409;
            this.lblBrand.Text = "Brand";
            this.lblBrand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtProductCode
            // 
            this.txtProductCode.Location = new System.Drawing.Point(155, 52);
            this.txtProductCode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtProductCode.Name = "txtProductCode";
            this.txtProductCode.Size = new System.Drawing.Size(180, 20);
            this.txtProductCode.TabIndex = 1;
            this.txtProductCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProductCode_KeyDown);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(972, 478);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 27);
            this.btnClose.TabIndex = 37;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(881, 478);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(84, 27);
            this.btnDelete.TabIndex = 36;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(790, 478);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(84, 27);
            this.btnClear.TabIndex = 35;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(155, 27);
            this.txtName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(180, 20);
            this.txtName.TabIndex = 0;
            this.txtName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyDown);
            // 
            // lblProductCode
            // 
            this.lblProductCode.AutoSize = true;
            this.lblProductCode.BackColor = System.Drawing.Color.Transparent;
            this.lblProductCode.ForeColor = System.Drawing.Color.Black;
            this.lblProductCode.Location = new System.Drawing.Point(68, 55);
            this.lblProductCode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblProductCode.Name = "lblProductCode";
            this.lblProductCode.Size = new System.Drawing.Size(72, 13);
            this.lblProductCode.TabIndex = 399;
            this.lblProductCode.Text = "Product Code";
            this.lblProductCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(699, 478);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 27);
            this.btnSave.TabIndex = 34;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyUp);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(24, 28);
            this.lblName.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(116, 13);
            this.lblName.TabIndex = 395;
            this.lblName.Text = "Product/Service Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvProductCreation
            // 
            this.dgvProductCreation.AllowUserToResizeColumns = false;
            this.dgvProductCreation.AllowUserToResizeRows = false;
            this.dgvProductCreation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProductCreation.BackgroundColor = System.Drawing.Color.White;
            this.dgvProductCreation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProductCreation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProductCreation.ColumnHeadersHeight = 25;
            this.dgvProductCreation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProductCreation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtslno,
            this.dgvcmbtgodown,
            this.dgvcmbrack,
            this.dgvtxtbatch,
            this.dgvtxManfDate,
            this.dgvtxtExpDate,
            this.dgvtxtqty,
            this.dgvtxtrate,
            this.dgvcmbUnit,
            this.dgvtxtbatchId,
            this.dgvtxtstockpostId,
            this.dgvtxtCheck,
            this.dgvtxtamount,
            this.dgvtxtunit,
            this.dgvtxtProductCode,
            this.dgvCmbProject,
            this.dgvCmbCategory});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProductCreation.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvProductCreation.EnableHeadersVisualStyles = false;
            this.dgvProductCreation.GridColor = System.Drawing.Color.DimGray;
            this.dgvProductCreation.Location = new System.Drawing.Point(24, 240);
            this.dgvProductCreation.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dgvProductCreation.Name = "dgvProductCreation";
            this.dgvProductCreation.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProductCreation.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvProductCreation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvProductCreation.Size = new System.Drawing.Size(1035, 194);
            this.dgvProductCreation.TabIndex = 30;
            this.dgvProductCreation.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductCreation_CellEndEdit);
            this.dgvProductCreation.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductCreation_CellEnter);
            this.dgvProductCreation.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductCreation_CellValueChanged);
            this.dgvProductCreation.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvProductCreation_DataError);
            this.dgvProductCreation.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvProductCreation_EditingControlShowing);
            this.dgvProductCreation.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvProductCreation_RowsAdded);
            this.dgvProductCreation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProductCreation_KeyDown);
            // 
            // dgvtxtslno
            // 
            this.dgvtxtslno.DataPropertyName = "SlNo";
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dgvtxtslno.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtslno.FillWeight = 35.53299F;
            this.dgvtxtslno.HeaderText = "Sl No";
            this.dgvtxtslno.Name = "dgvtxtslno";
            this.dgvtxtslno.ReadOnly = true;
            this.dgvtxtslno.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtslno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvcmbtgodown
            // 
            this.dgvcmbtgodown.FillWeight = 110.7445F;
            this.dgvcmbtgodown.HeaderText = "Store";
            this.dgvcmbtgodown.Name = "dgvcmbtgodown";
            this.dgvcmbtgodown.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dgvcmbrack
            // 
            this.dgvcmbrack.FillWeight = 110.7445F;
            this.dgvcmbrack.HeaderText = "Rack";
            this.dgvcmbrack.Name = "dgvcmbrack";
            this.dgvcmbrack.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dgvtxtbatch
            // 
            this.dgvtxtbatch.DataPropertyName = "batchNo";
            this.dgvtxtbatch.HeaderText = "Batch";
            this.dgvtxtbatch.Name = "dgvtxtbatch";
            this.dgvtxtbatch.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtbatch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtbatch.Visible = false;
            // 
            // dgvtxManfDate
            // 
            this.dgvtxManfDate.HeaderText = "MFD";
            this.dgvtxManfDate.Name = "dgvtxManfDate";
            this.dgvtxManfDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxManfDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxManfDate.Visible = false;
            // 
            // dgvtxtExpDate
            // 
            this.dgvtxtExpDate.HeaderText = "EXPD";
            this.dgvtxtExpDate.Name = "dgvtxtExpDate";
            this.dgvtxtExpDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtExpDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtExpDate.Visible = false;
            // 
            // dgvtxtqty
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.dgvtxtqty.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtqty.FillWeight = 110.7445F;
            this.dgvtxtqty.HeaderText = "Qty.";
            this.dgvtxtqty.MaxInputLength = 13;
            this.dgvtxtqty.Name = "dgvtxtqty";
            this.dgvtxtqty.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtqty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtrate
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtrate.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvtxtrate.FillWeight = 110.7445F;
            this.dgvtxtrate.HeaderText = "Rate";
            this.dgvtxtrate.MaxInputLength = 13;
            this.dgvtxtrate.Name = "dgvtxtrate";
            this.dgvtxtrate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtrate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvcmbUnit
            // 
            this.dgvcmbUnit.FillWeight = 110.7445F;
            this.dgvcmbUnit.HeaderText = "Unit";
            this.dgvcmbUnit.Name = "dgvcmbUnit";
            this.dgvcmbUnit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dgvtxtbatchId
            // 
            dataGridViewCellStyle5.NullValue = "0";
            this.dgvtxtbatchId.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvtxtbatchId.HeaderText = "BatchId";
            this.dgvtxtbatchId.Name = "dgvtxtbatchId";
            this.dgvtxtbatchId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtbatchId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtbatchId.Visible = false;
            // 
            // dgvtxtstockpostId
            // 
            dataGridViewCellStyle6.NullValue = "0";
            this.dgvtxtstockpostId.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvtxtstockpostId.HeaderText = "StockId";
            this.dgvtxtstockpostId.Name = "dgvtxtstockpostId";
            this.dgvtxtstockpostId.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtstockpostId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtstockpostId.Visible = false;
            // 
            // dgvtxtCheck
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(98)))), ((int)(((byte)(54)))));
            this.dgvtxtCheck.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvtxtCheck.FillWeight = 20F;
            this.dgvtxtCheck.HeaderText = "";
            this.dgvtxtCheck.Name = "dgvtxtCheck";
            this.dgvtxtCheck.ReadOnly = true;
            this.dgvtxtCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtCheck.Visible = false;
            // 
            // dgvtxtamount
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtamount.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvtxtamount.FillWeight = 110.7445F;
            this.dgvtxtamount.HeaderText = "Amount";
            this.dgvtxtamount.Name = "dgvtxtamount";
            this.dgvtxtamount.ReadOnly = true;
            this.dgvtxtamount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtamount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtunit
            // 
            this.dgvtxtunit.HeaderText = "Unit";
            this.dgvtxtunit.Name = "dgvtxtunit";
            this.dgvtxtunit.ReadOnly = true;
            this.dgvtxtunit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtunit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtunit.Visible = false;
            // 
            // dgvtxtProductCode
            // 
            this.dgvtxtProductCode.HeaderText = "Code";
            this.dgvtxtProductCode.Name = "dgvtxtProductCode";
            this.dgvtxtProductCode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvtxtProductCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtProductCode.Visible = false;
            // 
            // dgvCmbProject
            // 
            this.dgvCmbProject.HeaderText = "Project";
            this.dgvCmbProject.Name = "dgvCmbProject";
            // 
            // dgvCmbCategory
            // 
            this.dgvCmbCategory.HeaderText = "Category";
            this.dgvCmbCategory.Name = "dgvCmbCategory";
            // 
            // cmbTax
            // 
            this.cmbTax.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTax.FormattingEnabled = true;
            this.cmbTax.Location = new System.Drawing.Point(493, 161);
            this.cmbTax.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbTax.Name = "cmbTax";
            this.cmbTax.Size = new System.Drawing.Size(180, 21);
            this.cmbTax.TabIndex = 15;
            this.cmbTax.SelectedIndexChanged += new System.EventHandler(this.cmbTax_SelectedIndexChanged);
            this.cmbTax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTax_KeyDown);
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.BackColor = System.Drawing.Color.Transparent;
            this.lblTax.ForeColor = System.Drawing.Color.Black;
            this.lblTax.Location = new System.Drawing.Point(457, 164);
            this.lblTax.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(25, 13);
            this.lblTax.TabIndex = 426;
            this.lblTax.Text = "Tax";
            this.lblTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaximumStock
            // 
            this.txtMaximumStock.Location = new System.Drawing.Point(493, 55);
            this.txtMaximumStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtMaximumStock.MaxLength = 13;
            this.txtMaximumStock.Name = "txtMaximumStock";
            this.txtMaximumStock.Size = new System.Drawing.Size(180, 20);
            this.txtMaximumStock.TabIndex = 20;
            this.txtMaximumStock.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMaximumStock_MouseClick);
            this.txtMaximumStock.TextChanged += new System.EventHandler(this.txtMaximumStock_TextChanged);
            this.txtMaximumStock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMaximumStock_KeyDown);
            this.txtMaximumStock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaximumStock_KeyPress);
            this.txtMaximumStock.Leave += new System.EventHandler(this.txtMaximumStock_Leave);
            // 
            // lblMaximumStock
            // 
            this.lblMaximumStock.AutoSize = true;
            this.lblMaximumStock.BackColor = System.Drawing.Color.Transparent;
            this.lblMaximumStock.ForeColor = System.Drawing.Color.Black;
            this.lblMaximumStock.Location = new System.Drawing.Point(402, 61);
            this.lblMaximumStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMaximumStock.Name = "lblMaximumStock";
            this.lblMaximumStock.Size = new System.Drawing.Size(82, 13);
            this.lblMaximumStock.TabIndex = 428;
            this.lblMaximumStock.Text = "Maximum Stock";
            this.lblMaximumStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbAllowBatch
            // 
            this.cmbAllowBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAllowBatch.FormattingEnabled = true;
            this.cmbAllowBatch.Location = new System.Drawing.Point(950, 140);
            this.cmbAllowBatch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbAllowBatch.Name = "cmbAllowBatch";
            this.cmbAllowBatch.Size = new System.Drawing.Size(54, 21);
            this.cmbAllowBatch.TabIndex = 26;
            this.cmbAllowBatch.SelectedValueChanged += new System.EventHandler(this.cmbAllowBatch_SelectedValueChanged);
            this.cmbAllowBatch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbAllowBatch_KeyDown);
            // 
            // lblAllowBatch
            // 
            this.lblAllowBatch.AutoSize = true;
            this.lblAllowBatch.BackColor = System.Drawing.Color.Transparent;
            this.lblAllowBatch.ForeColor = System.Drawing.Color.Black;
            this.lblAllowBatch.Location = new System.Drawing.Point(884, 142);
            this.lblAllowBatch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblAllowBatch.Name = "lblAllowBatch";
            this.lblAllowBatch.Size = new System.Drawing.Size(63, 13);
            this.lblAllowBatch.TabIndex = 432;
            this.lblAllowBatch.Text = "Allow Batch";
            this.lblAllowBatch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbDefaultGodown
            // 
            this.cmbDefaultGodown.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefaultGodown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultGodown.FormattingEnabled = true;
            this.cmbDefaultGodown.Location = new System.Drawing.Point(493, 134);
            this.cmbDefaultGodown.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbDefaultGodown.Name = "cmbDefaultGodown";
            this.cmbDefaultGodown.Size = new System.Drawing.Size(180, 21);
            this.cmbDefaultGodown.TabIndex = 22;
            this.cmbDefaultGodown.SelectedValueChanged += new System.EventHandler(this.cmbDefaultGodown_SelectedValueChanged);
            this.cmbDefaultGodown.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDefaultGodown_KeyDown);
            // 
            // lblDefaultGodown
            // 
            this.lblDefaultGodown.AutoSize = true;
            this.lblDefaultGodown.BackColor = System.Drawing.Color.Transparent;
            this.lblDefaultGodown.ForeColor = System.Drawing.Color.Black;
            this.lblDefaultGodown.Location = new System.Drawing.Point(415, 140);
            this.lblDefaultGodown.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblDefaultGodown.Name = "lblDefaultGodown";
            this.lblDefaultGodown.Size = new System.Drawing.Size(69, 13);
            this.lblDefaultGodown.TabIndex = 430;
            this.lblDefaultGodown.Text = "Default Store";
            this.lblDefaultGodown.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbBom
            // 
            this.cmbBom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBom.FormattingEnabled = true;
            this.cmbBom.Location = new System.Drawing.Point(824, 107);
            this.cmbBom.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbBom.Name = "cmbBom";
            this.cmbBom.Size = new System.Drawing.Size(180, 21);
            this.cmbBom.TabIndex = 18;
            this.cmbBom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBom_KeyDown);
            this.cmbBom.Leave += new System.EventHandler(this.cmbBom_Leave);
            // 
            // lblBom
            // 
            this.lblBom.AutoSize = true;
            this.lblBom.BackColor = System.Drawing.Color.Transparent;
            this.lblBom.ForeColor = System.Drawing.Color.Black;
            this.lblBom.Location = new System.Drawing.Point(782, 112);
            this.lblBom.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBom.Name = "lblBom";
            this.lblBom.Size = new System.Drawing.Size(31, 13);
            this.lblBom.TabIndex = 434;
            this.lblBom.Text = "BOM";
            this.lblBom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBrandAdd
            // 
            this.btnBrandAdd.BackColor = System.Drawing.Color.Gray;
            this.btnBrandAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBrandAdd.FlatAppearance.BorderSize = 0;
            this.btnBrandAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrandAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrandAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnBrandAdd.Location = new System.Drawing.Point(1010, 29);
            this.btnBrandAdd.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.btnBrandAdd.Name = "btnBrandAdd";
            this.btnBrandAdd.Size = new System.Drawing.Size(21, 19);
            this.btnBrandAdd.TabIndex = 7;
            this.btnBrandAdd.Text = "+";
            this.btnBrandAdd.UseVisualStyleBackColor = false;
            this.btnBrandAdd.Click += new System.EventHandler(this.btnBrandAdd_Click);
            // 
            // btnSizeAdd
            // 
            this.btnSizeAdd.BackColor = System.Drawing.Color.Gray;
            this.btnSizeAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSizeAdd.FlatAppearance.BorderSize = 0;
            this.btnSizeAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSizeAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSizeAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnSizeAdd.Location = new System.Drawing.Point(1010, 55);
            this.btnSizeAdd.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.btnSizeAdd.Name = "btnSizeAdd";
            this.btnSizeAdd.Size = new System.Drawing.Size(21, 19);
            this.btnSizeAdd.TabIndex = 11;
            this.btnSizeAdd.Text = "+";
            this.btnSizeAdd.UseVisualStyleBackColor = false;
            this.btnSizeAdd.Click += new System.EventHandler(this.btnSizeAdd_Click);
            // 
            // btnTaxAdd
            // 
            this.btnTaxAdd.BackColor = System.Drawing.Color.Gray;
            this.btnTaxAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnTaxAdd.FlatAppearance.BorderSize = 0;
            this.btnTaxAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTaxAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTaxAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnTaxAdd.Location = new System.Drawing.Point(684, 163);
            this.btnTaxAdd.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.btnTaxAdd.Name = "btnTaxAdd";
            this.btnTaxAdd.Size = new System.Drawing.Size(21, 19);
            this.btnTaxAdd.TabIndex = 16;
            this.btnTaxAdd.Text = "+";
            this.btnTaxAdd.UseVisualStyleBackColor = false;
            this.btnTaxAdd.Click += new System.EventHandler(this.btnTaxAdd_Click);
            // 
            // btnDefaultGodownAdd
            // 
            this.btnDefaultGodownAdd.BackColor = System.Drawing.Color.Gray;
            this.btnDefaultGodownAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDefaultGodownAdd.FlatAppearance.BorderSize = 0;
            this.btnDefaultGodownAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDefaultGodownAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefaultGodownAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnDefaultGodownAdd.Location = new System.Drawing.Point(684, 136);
            this.btnDefaultGodownAdd.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.btnDefaultGodownAdd.Name = "btnDefaultGodownAdd";
            this.btnDefaultGodownAdd.Size = new System.Drawing.Size(21, 19);
            this.btnDefaultGodownAdd.TabIndex = 23;
            this.btnDefaultGodownAdd.Text = "+";
            this.btnDefaultGodownAdd.UseVisualStyleBackColor = false;
            this.btnDefaultGodownAdd.Click += new System.EventHandler(this.btnDefaultGodownAdd_Click);
            // 
            // btnUnitAdd
            // 
            this.btnUnitAdd.BackColor = System.Drawing.Color.Gray;
            this.btnUnitAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnUnitAdd.FlatAppearance.BorderSize = 0;
            this.btnUnitAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnitAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnitAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnUnitAdd.Location = new System.Drawing.Point(348, 128);
            this.btnUnitAdd.Name = "btnUnitAdd";
            this.btnUnitAdd.Size = new System.Drawing.Size(21, 19);
            this.btnUnitAdd.TabIndex = 5;
            this.btnUnitAdd.Text = "+";
            this.btnUnitAdd.UseVisualStyleBackColor = false;
            this.btnUnitAdd.Click += new System.EventHandler(this.btnUnitAdd_Click);
            // 
            // btnGroupAdd
            // 
            this.btnGroupAdd.BackColor = System.Drawing.Color.Gray;
            this.btnGroupAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGroupAdd.FlatAppearance.BorderSize = 0;
            this.btnGroupAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGroupAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnGroupAdd.Location = new System.Drawing.Point(684, 8);
            this.btnGroupAdd.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.btnGroupAdd.Name = "btnGroupAdd";
            this.btnGroupAdd.Size = new System.Drawing.Size(21, 19);
            this.btnGroupAdd.TabIndex = 3;
            this.btnGroupAdd.Text = "+";
            this.btnGroupAdd.UseVisualStyleBackColor = false;
            this.btnGroupAdd.Click += new System.EventHandler(this.btnGroupAdd_Click);
            // 
            // cmbUnit
            // 
            this.cmbUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnit.FormattingEnabled = true;
            this.cmbUnit.Location = new System.Drawing.Point(155, 126);
            this.cmbUnit.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbUnit.Name = "cmbUnit";
            this.cmbUnit.Size = new System.Drawing.Size(180, 21);
            this.cmbUnit.TabIndex = 4;
            this.cmbUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUnit_KeyDown);
            this.cmbUnit.Leave += new System.EventHandler(this.cmbUnit_Leave);
            // 
            // lblUnit
            // 
            this.lblUnit.AutoSize = true;
            this.lblUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblUnit.ForeColor = System.Drawing.Color.Black;
            this.lblUnit.Location = new System.Drawing.Point(114, 131);
            this.lblUnit.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblUnit.Name = "lblUnit";
            this.lblUnit.Size = new System.Drawing.Size(26, 13);
            this.lblUnit.TabIndex = 442;
            this.lblUnit.Text = "Unit";
            this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.BackColor = System.Drawing.Color.Transparent;
            this.lblGroup.ForeColor = System.Drawing.Color.Black;
            this.lblGroup.Location = new System.Drawing.Point(448, 9);
            this.lblGroup.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(36, 13);
            this.lblGroup.TabIndex = 440;
            this.lblGroup.Text = "Group";
            this.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbModalNo
            // 
            this.cmbModalNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbModalNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModalNo.FormattingEnabled = true;
            this.cmbModalNo.Location = new System.Drawing.Point(824, 2);
            this.cmbModalNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbModalNo.Name = "cmbModalNo";
            this.cmbModalNo.Size = new System.Drawing.Size(180, 21);
            this.cmbModalNo.TabIndex = 8;
            this.cmbModalNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbModalNo_KeyDown);
            // 
            // lblModalNo
            // 
            this.lblModalNo.AutoSize = true;
            this.lblModalNo.BackColor = System.Drawing.Color.Transparent;
            this.lblModalNo.ForeColor = System.Drawing.Color.Black;
            this.lblModalNo.Location = new System.Drawing.Point(757, 7);
            this.lblModalNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblModalNo.Name = "lblModalNo";
            this.lblModalNo.Size = new System.Drawing.Size(56, 13);
            this.lblModalNo.TabIndex = 446;
            this.lblModalNo.Text = "Model No.";
            this.lblModalNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnModalNo
            // 
            this.btnModalNo.BackColor = System.Drawing.Color.Gray;
            this.btnModalNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnModalNo.FlatAppearance.BorderSize = 0;
            this.btnModalNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModalNo.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModalNo.ForeColor = System.Drawing.Color.Maroon;
            this.btnModalNo.Location = new System.Drawing.Point(1009, 3);
            this.btnModalNo.Name = "btnModalNo";
            this.btnModalNo.Size = new System.Drawing.Size(21, 19);
            this.btnModalNo.TabIndex = 9;
            this.btnModalNo.Text = "+";
            this.btnModalNo.UseVisualStyleBackColor = false;
            this.btnModalNo.Click += new System.EventHandler(this.btnModalNo_Click);
            // 
            // txtReorderLevel
            // 
            this.txtReorderLevel.Location = new System.Drawing.Point(493, 80);
            this.txtReorderLevel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtReorderLevel.MaxLength = 13;
            this.txtReorderLevel.Name = "txtReorderLevel";
            this.txtReorderLevel.Size = new System.Drawing.Size(180, 20);
            this.txtReorderLevel.TabIndex = 21;
            this.txtReorderLevel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtReorderLevel_MouseClick);
            this.txtReorderLevel.TextChanged += new System.EventHandler(this.txtReorderLevel_TextChanged);
            this.txtReorderLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReorderLevel_KeyDown);
            this.txtReorderLevel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReorderLevel_KeyPress);
            this.txtReorderLevel.Leave += new System.EventHandler(this.txtReorderLevel_Leave);
            // 
            // lblReorderLevel
            // 
            this.lblReorderLevel.AutoSize = true;
            this.lblReorderLevel.BackColor = System.Drawing.Color.Transparent;
            this.lblReorderLevel.ForeColor = System.Drawing.Color.Black;
            this.lblReorderLevel.Location = new System.Drawing.Point(407, 83);
            this.lblReorderLevel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblReorderLevel.Name = "lblReorderLevel";
            this.lblReorderLevel.Size = new System.Drawing.Size(77, 13);
            this.lblReorderLevel.TabIndex = 453;
            this.lblReorderLevel.Text = "Re-order Level";
            this.lblReorderLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMinimumStock
            // 
            this.txtMinimumStock.Location = new System.Drawing.Point(493, 32);
            this.txtMinimumStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtMinimumStock.MaxLength = 13;
            this.txtMinimumStock.Name = "txtMinimumStock";
            this.txtMinimumStock.Size = new System.Drawing.Size(180, 20);
            this.txtMinimumStock.TabIndex = 19;
            this.txtMinimumStock.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMinimumStock_MouseClick);
            this.txtMinimumStock.TextChanged += new System.EventHandler(this.txtMinimumStock_TextChanged);
            this.txtMinimumStock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMinimumStock_KeyDown);
            this.txtMinimumStock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMinimumStock_KeyPress);
            this.txtMinimumStock.Leave += new System.EventHandler(this.txtMinimumStock_Leave);
            // 
            // txtSalesRate
            // 
            this.txtSalesRate.Location = new System.Drawing.Point(155, 102);
            this.txtSalesRate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtSalesRate.MaxLength = 10;
            this.txtSalesRate.Name = "txtSalesRate";
            this.txtSalesRate.Size = new System.Drawing.Size(180, 20);
            this.txtSalesRate.TabIndex = 14;
            this.txtSalesRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSalesRate.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtSalesRate_MouseClick);
            this.txtSalesRate.TextChanged += new System.EventHandler(this.txtSalesRate_TextChanged);
            this.txtSalesRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSalesRate_KeyDown);
            this.txtSalesRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalesRate_KeyPress);
            this.txtSalesRate.Leave += new System.EventHandler(this.txtSalesRate_Leave);
            // 
            // lblMinimumStock
            // 
            this.lblMinimumStock.AutoSize = true;
            this.lblMinimumStock.BackColor = System.Drawing.Color.Transparent;
            this.lblMinimumStock.ForeColor = System.Drawing.Color.Black;
            this.lblMinimumStock.Location = new System.Drawing.Point(405, 35);
            this.lblMinimumStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMinimumStock.Name = "lblMinimumStock";
            this.lblMinimumStock.Size = new System.Drawing.Size(79, 13);
            this.lblMinimumStock.TabIndex = 450;
            this.lblMinimumStock.Text = "Minimum Stock";
            this.lblMinimumStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSalesRate
            // 
            this.lblSalesRate.AutoSize = true;
            this.lblSalesRate.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesRate.ForeColor = System.Drawing.Color.Black;
            this.lblSalesRate.Location = new System.Drawing.Point(81, 105);
            this.lblSalesRate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblSalesRate.Name = "lblSalesRate";
            this.lblSalesRate.Size = new System.Drawing.Size(59, 13);
            this.lblSalesRate.TabIndex = 449;
            this.lblSalesRate.Text = "Sales Rate";
            this.lblSalesRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbTaxApplicableOn
            // 
            this.cmbTaxApplicableOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTaxApplicableOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTaxApplicableOn.FormattingEnabled = true;
            this.cmbTaxApplicableOn.Location = new System.Drawing.Point(493, 188);
            this.cmbTaxApplicableOn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbTaxApplicableOn.Name = "cmbTaxApplicableOn";
            this.cmbTaxApplicableOn.Size = new System.Drawing.Size(180, 21);
            this.cmbTaxApplicableOn.TabIndex = 17;
            this.cmbTaxApplicableOn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTaxApplicableOn_KeyDown);
            // 
            // lblTaxApplicableOn
            // 
            this.lblTaxApplicableOn.AutoSize = true;
            this.lblTaxApplicableOn.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxApplicableOn.ForeColor = System.Drawing.Color.Black;
            this.lblTaxApplicableOn.Location = new System.Drawing.Point(387, 192);
            this.lblTaxApplicableOn.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblTaxApplicableOn.Name = "lblTaxApplicableOn";
            this.lblTaxApplicableOn.Size = new System.Drawing.Size(97, 13);
            this.lblTaxApplicableOn.TabIndex = 455;
            this.lblTaxApplicableOn.Text = "Tax Applicable On.";
            this.lblTaxApplicableOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDefaultAdd
            // 
            this.btnDefaultAdd.BackColor = System.Drawing.Color.Gray;
            this.btnDefaultAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDefaultAdd.FlatAppearance.BorderSize = 0;
            this.btnDefaultAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDefaultAdd.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefaultAdd.ForeColor = System.Drawing.Color.Maroon;
            this.btnDefaultAdd.Location = new System.Drawing.Point(683, 105);
            this.btnDefaultAdd.Name = "btnDefaultAdd";
            this.btnDefaultAdd.Size = new System.Drawing.Size(21, 19);
            this.btnDefaultAdd.TabIndex = 25;
            this.btnDefaultAdd.Text = "+";
            this.btnDefaultAdd.UseVisualStyleBackColor = false;
            this.btnDefaultAdd.Click += new System.EventHandler(this.btnDefaultAdd_Click);
            // 
            // cmbDefaultRack
            // 
            this.cmbDefaultRack.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDefaultRack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultRack.FormattingEnabled = true;
            this.cmbDefaultRack.Location = new System.Drawing.Point(493, 105);
            this.cmbDefaultRack.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbDefaultRack.Name = "cmbDefaultRack";
            this.cmbDefaultRack.Size = new System.Drawing.Size(180, 21);
            this.cmbDefaultRack.TabIndex = 24;
            this.cmbDefaultRack.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDefaultRack_KeyDown);
            // 
            // lblDefaultRate
            // 
            this.lblDefaultRate.AutoSize = true;
            this.lblDefaultRate.BackColor = System.Drawing.Color.Transparent;
            this.lblDefaultRate.ForeColor = System.Drawing.Color.Black;
            this.lblDefaultRate.Location = new System.Drawing.Point(414, 108);
            this.lblDefaultRate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblDefaultRate.Name = "lblDefaultRate";
            this.lblDefaultRate.Size = new System.Drawing.Size(70, 13);
            this.lblDefaultRate.TabIndex = 457;
            this.lblDefaultRate.Text = "Default Rack";
            this.lblDefaultRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbMultipleUnit
            // 
            this.cmbMultipleUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMultipleUnit.FormattingEnabled = true;
            this.cmbMultipleUnit.Location = new System.Drawing.Point(824, 139);
            this.cmbMultipleUnit.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbMultipleUnit.Name = "cmbMultipleUnit";
            this.cmbMultipleUnit.Size = new System.Drawing.Size(50, 21);
            this.cmbMultipleUnit.TabIndex = 27;
            this.cmbMultipleUnit.SelectedIndexChanged += new System.EventHandler(this.cmbMultipleUnit_SelectedIndexChanged);
            this.cmbMultipleUnit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbMultipleUnit_KeyDown);
            this.cmbMultipleUnit.Leave += new System.EventHandler(this.cmbMultipleUnit_Leave);
            // 
            // lblMultipleUnit
            // 
            this.lblMultipleUnit.AutoSize = true;
            this.lblMultipleUnit.BackColor = System.Drawing.Color.Transparent;
            this.lblMultipleUnit.ForeColor = System.Drawing.Color.Black;
            this.lblMultipleUnit.Location = new System.Drawing.Point(748, 142);
            this.lblMultipleUnit.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblMultipleUnit.Name = "lblMultipleUnit";
            this.lblMultipleUnit.Size = new System.Drawing.Size(65, 13);
            this.lblMultipleUnit.TabIndex = 460;
            this.lblMultipleUnit.Text = "Multiple Unit";
            this.lblMultipleUnit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbOpeningStock
            // 
            this.cmbOpeningStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOpeningStock.FormattingEnabled = true;
            this.cmbOpeningStock.Location = new System.Drawing.Point(155, 205);
            this.cmbOpeningStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbOpeningStock.Name = "cmbOpeningStock";
            this.cmbOpeningStock.Size = new System.Drawing.Size(180, 21);
            this.cmbOpeningStock.TabIndex = 29;
            this.cmbOpeningStock.SelectedValueChanged += new System.EventHandler(this.cmbOpeningStock_SelectedValueChanged);
            this.cmbOpeningStock.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOpeningStock_KeyDown);
            this.cmbOpeningStock.Leave += new System.EventHandler(this.cmbOpeningStock_Leave);
            // 
            // lblOpeningStock
            // 
            this.lblOpeningStock.AutoSize = true;
            this.lblOpeningStock.BackColor = System.Drawing.Color.Transparent;
            this.lblOpeningStock.ForeColor = System.Drawing.Color.Black;
            this.lblOpeningStock.Location = new System.Drawing.Point(62, 208);
            this.lblOpeningStock.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblOpeningStock.Name = "lblOpeningStock";
            this.lblOpeningStock.Size = new System.Drawing.Size(78, 13);
            this.lblOpeningStock.TabIndex = 462;
            this.lblOpeningStock.Text = "Opening Stock";
            this.lblOpeningStock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(93, 452);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(200, 42);
            this.txtNarration.TabIndex = 31;
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            // 
            // cbxActive
            // 
            this.cbxActive.AutoSize = true;
            this.cbxActive.BackColor = System.Drawing.Color.Transparent;
            this.cbxActive.Checked = true;
            this.cbxActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxActive.ForeColor = System.Drawing.Color.Black;
            this.cbxActive.Location = new System.Drawing.Point(313, 457);
            this.cbxActive.Name = "cbxActive";
            this.cbxActive.Size = new System.Drawing.Size(56, 17);
            this.cbxActive.TabIndex = 32;
            this.cbxActive.Text = "Active";
            this.cbxActive.UseVisualStyleBackColor = false;
            this.cbxActive.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxActive_KeyDown);
            // 
            // cbxReminder
            // 
            this.cbxReminder.AutoSize = true;
            this.cbxReminder.BackColor = System.Drawing.Color.Transparent;
            this.cbxReminder.ForeColor = System.Drawing.Color.Black;
            this.cbxReminder.Location = new System.Drawing.Point(390, 456);
            this.cbxReminder.Name = "cbxReminder";
            this.cbxReminder.Size = new System.Drawing.Size(112, 17);
            this.cbxReminder.TabIndex = 33;
            this.cbxReminder.Text = "Show in Reminder";
            this.cbxReminder.UseVisualStyleBackColor = false;
            this.cbxReminder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxReminder_KeyDown);
            // 
            // cmbBrand
            // 
            this.cmbBrand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBrand.FormattingEnabled = true;
            this.cmbBrand.Location = new System.Drawing.Point(824, 28);
            this.cmbBrand.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbBrand.Name = "cmbBrand";
            this.cmbBrand.Size = new System.Drawing.Size(180, 21);
            this.cmbBrand.TabIndex = 6;
            this.cmbBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbBrand_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(338, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 470;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(338, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 470;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(673, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 470;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(338, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 470;
            this.label4.Text = "*";
            // 
            // cmbGroup
            // 
            this.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(493, 7);
            this.cmbGroup.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(180, 21);
            this.cmbGroup.TabIndex = 2;
            this.cmbGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbGroup_KeyDown);
            // 
            // txtPartNo
            // 
            this.txtPartNo.Enabled = false;
            this.txtPartNo.Location = new System.Drawing.Point(824, 166);
            this.txtPartNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtPartNo.Multiline = true;
            this.txtPartNo.Name = "txtPartNo";
            this.txtPartNo.Size = new System.Drawing.Size(180, 21);
            this.txtPartNo.TabIndex = 28;
            this.txtPartNo.Visible = false;
            this.txtPartNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartNo_KeyDown);
            // 
            // lblPartNo
            // 
            this.lblPartNo.AutoSize = true;
            this.lblPartNo.BackColor = System.Drawing.Color.Transparent;
            this.lblPartNo.Enabled = false;
            this.lblPartNo.ForeColor = System.Drawing.Color.Black;
            this.lblPartNo.Location = new System.Drawing.Point(770, 172);
            this.lblPartNo.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblPartNo.Name = "lblPartNo";
            this.lblPartNo.Size = new System.Drawing.Size(43, 13);
            this.lblPartNo.TabIndex = 472;
            this.lblPartNo.Text = "Part No";
            this.lblPartNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPartNo.Visible = false;
            // 
            // lnklblRemove
            // 
            this.lnklblRemove.ActiveLinkColor = System.Drawing.Color.Maroon;
            this.lnklblRemove.AutoSize = true;
            this.lnklblRemove.LinkColor = System.Drawing.Color.Maroon;
            this.lnklblRemove.Location = new System.Drawing.Point(987, 459);
            this.lnklblRemove.Margin = new System.Windows.Forms.Padding(5);
            this.lnklblRemove.Name = "lnklblRemove";
            this.lnklblRemove.Size = new System.Drawing.Size(47, 13);
            this.lnklblRemove.TabIndex = 473;
            this.lnklblRemove.TabStop = true;
            this.lnklblRemove.Text = "Remove";
            this.lnklblRemove.VisitedLinkColor = System.Drawing.Color.Maroon;
            this.lnklblRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnklblRemove_LinkClicked);
            // 
            // radProduct
            // 
            this.radProduct.AutoSize = true;
            this.radProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProduct.Location = new System.Drawing.Point(154, 5);
            this.radProduct.Name = "radProduct";
            this.radProduct.Size = new System.Drawing.Size(58, 17);
            this.radProduct.TabIndex = 474;
            this.radProduct.Text = "Stock";
            this.radProduct.UseVisualStyleBackColor = true;
            // 
            // radService
            // 
            this.radService.AutoSize = true;
            this.radService.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radService.Location = new System.Drawing.Point(215, 5);
            this.radService.Name = "radService";
            this.radService.Size = new System.Drawing.Size(134, 17);
            this.radService.TabIndex = 475;
            this.radService.Text = "Service/Non Stock";
            this.radService.UseVisualStyleBackColor = true;
            this.radService.CheckedChanged += new System.EventHandler(this.radService_CheckedChanged);
            // 
            // cmbSalesAccount
            // 
            this.cmbSalesAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesAccount.FormattingEnabled = true;
            this.cmbSalesAccount.Location = new System.Drawing.Point(155, 179);
            this.cmbSalesAccount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbSalesAccount.Name = "cmbSalesAccount";
            this.cmbSalesAccount.Size = new System.Drawing.Size(180, 21);
            this.cmbSalesAccount.TabIndex = 732;
            // 
            // lblSalesAccount
            // 
            this.lblSalesAccount.AutoSize = true;
            this.lblSalesAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblSalesAccount.ForeColor = System.Drawing.Color.Black;
            this.lblSalesAccount.Location = new System.Drawing.Point(62, 182);
            this.lblSalesAccount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblSalesAccount.Name = "lblSalesAccount";
            this.lblSalesAccount.Size = new System.Drawing.Size(76, 13);
            this.lblSalesAccount.TabIndex = 733;
            this.lblSalesAccount.Text = "Sales Account";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(338, 187);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 734;
            this.label5.Text = "*";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "yyyy/mm/dd";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(672, 214);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(17, 20);
            this.dtpDate.TabIndex = 771;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged_1);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(493, 214);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(180, 20);
            this.txtDate.TabIndex = 769;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(403, 217);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(75, 13);
            this.lblDate.TabIndex = 770;
            this.lblDate.Text = "Effective Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(49, 156);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 772;
            this.label6.Text = "Expense Account";
            // 
            // cmbExpenseAccount
            // 
            this.cmbExpenseAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbExpenseAccount.FormattingEnabled = true;
            this.cmbExpenseAccount.Location = new System.Drawing.Point(155, 153);
            this.cmbExpenseAccount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbExpenseAccount.Name = "cmbExpenseAccount";
            this.cmbExpenseAccount.Size = new System.Drawing.Size(180, 21);
            this.cmbExpenseAccount.TabIndex = 773;
            // 
            // chkAutomaticBarcode
            // 
            this.chkAutomaticBarcode.AutoSize = true;
            this.chkAutomaticBarcode.Location = new System.Drawing.Point(824, 193);
            this.chkAutomaticBarcode.Name = "chkAutomaticBarcode";
            this.chkAutomaticBarcode.Size = new System.Drawing.Size(116, 17);
            this.chkAutomaticBarcode.TabIndex = 774;
            this.chkAutomaticBarcode.Text = "Automatic Barcode";
            this.chkAutomaticBarcode.UseVisualStyleBackColor = true;
            this.chkAutomaticBarcode.CheckedChanged += new System.EventHandler(this.chkAutomaticBarcode_CheckedChanged);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(824, 213);
            this.txtBarcode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtBarcode.MaxLength = 13;
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(180, 20);
            this.txtBarcode.TabIndex = 775;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblBarcode.Enabled = false;
            this.lblBarcode.ForeColor = System.Drawing.Color.Black;
            this.lblBarcode.Location = new System.Drawing.Point(766, 215);
            this.lblBarcode.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(47, 13);
            this.lblBarcode.TabIndex = 776;
            this.lblBarcode.Text = "Barcode";
            this.lblBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNarration
            // 
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(24, 456);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(5);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(78, 20);
            this.lblNarration.TabIndex = 464;
            this.lblNarration.Text = "Description";
            this.lblNarration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbltotalInventoryValue
            // 
            this.lbltotalInventoryValue.AutoSize = true;
            this.lbltotalInventoryValue.Location = new System.Drawing.Point(793, 440);
            this.lbltotalInventoryValue.Name = "lbltotalInventoryValue";
            this.lbltotalInventoryValue.Size = new System.Drawing.Size(108, 13);
            this.lbltotalInventoryValue.TabIndex = 777;
            this.lbltotalInventoryValue.Text = "Total Inventory Value";
            // 
            // txtTotalInventoryValue
            // 
            this.txtTotalInventoryValue.Enabled = false;
            this.txtTotalInventoryValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInventoryValue.Location = new System.Drawing.Point(919, 437);
            this.txtTotalInventoryValue.Name = "txtTotalInventoryValue";
            this.txtTotalInventoryValue.Size = new System.Drawing.Size(140, 21);
            this.txtTotalInventoryValue.TabIndex = 778;
            // 
            // btnCreateSalesAccount
            // 
            this.btnCreateSalesAccount.BackColor = System.Drawing.Color.Gray;
            this.btnCreateSalesAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCreateSalesAccount.FlatAppearance.BorderSize = 0;
            this.btnCreateSalesAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateSalesAccount.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateSalesAccount.ForeColor = System.Drawing.Color.Maroon;
            this.btnCreateSalesAccount.Location = new System.Drawing.Point(348, 181);
            this.btnCreateSalesAccount.Name = "btnCreateSalesAccount";
            this.btnCreateSalesAccount.Size = new System.Drawing.Size(21, 19);
            this.btnCreateSalesAccount.TabIndex = 779;
            this.btnCreateSalesAccount.Text = "+";
            this.btnCreateSalesAccount.UseVisualStyleBackColor = false;
            this.btnCreateSalesAccount.Click += new System.EventHandler(this.btnCreateSalesAccount_Click);
            // 
            // btnCreateExpenseAccount
            // 
            this.btnCreateExpenseAccount.BackColor = System.Drawing.Color.Gray;
            this.btnCreateExpenseAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCreateExpenseAccount.FlatAppearance.BorderSize = 0;
            this.btnCreateExpenseAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateExpenseAccount.Font = new System.Drawing.Font("Bauhaus 93", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateExpenseAccount.ForeColor = System.Drawing.Color.Maroon;
            this.btnCreateExpenseAccount.Location = new System.Drawing.Point(348, 155);
            this.btnCreateExpenseAccount.Name = "btnCreateExpenseAccount";
            this.btnCreateExpenseAccount.Size = new System.Drawing.Size(21, 19);
            this.btnCreateExpenseAccount.TabIndex = 780;
            this.btnCreateExpenseAccount.Text = "+";
            this.btnCreateExpenseAccount.UseVisualStyleBackColor = false;
            this.btnCreateExpenseAccount.Click += new System.EventHandler(this.btnCreateExpenseAccount_Click);
            // 
            // frmProductCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.btnCreateExpenseAccount);
            this.Controls.Add(this.btnCreateSalesAccount);
            this.Controls.Add(this.txtTotalInventoryValue);
            this.Controls.Add(this.lbltotalInventoryValue);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.chkAutomaticBarcode);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.cmbExpenseAccount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbSalesAccount);
            this.Controls.Add(this.lblSalesAccount);
            this.Controls.Add(this.radService);
            this.Controls.Add(this.radProduct);
            this.Controls.Add(this.lnklblRemove);
            this.Controls.Add(this.lblPartNo);
            this.Controls.Add(this.txtPartNo);
            this.Controls.Add(this.cbxReminder);
            this.Controls.Add(this.cmbBrand);
            this.Controls.Add(this.cbxActive);
            this.Controls.Add(this.txtNarration);
            this.Controls.Add(this.lblNarration);
            this.Controls.Add(this.cmbOpeningStock);
            this.Controls.Add(this.lblOpeningStock);
            this.Controls.Add(this.cmbMultipleUnit);
            this.Controls.Add(this.lblMultipleUnit);
            this.Controls.Add(this.btnDefaultAdd);
            this.Controls.Add(this.cmbDefaultRack);
            this.Controls.Add(this.lblDefaultRate);
            this.Controls.Add(this.cmbTaxApplicableOn);
            this.Controls.Add(this.lblTaxApplicableOn);
            this.Controls.Add(this.txtReorderLevel);
            this.Controls.Add(this.lblReorderLevel);
            this.Controls.Add(this.txtMinimumStock);
            this.Controls.Add(this.txtSalesRate);
            this.Controls.Add(this.lblMinimumStock);
            this.Controls.Add(this.lblSalesRate);
            this.Controls.Add(this.btnModalNo);
            this.Controls.Add(this.cmbModalNo);
            this.Controls.Add(this.lblModalNo);
            this.Controls.Add(this.btnUnitAdd);
            this.Controls.Add(this.btnGroupAdd);
            this.Controls.Add(this.cmbUnit);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.lblGroup);
            this.Controls.Add(this.btnDefaultGodownAdd);
            this.Controls.Add(this.btnTaxAdd);
            this.Controls.Add(this.btnSizeAdd);
            this.Controls.Add(this.btnBrandAdd);
            this.Controls.Add(this.cmbBom);
            this.Controls.Add(this.lblBom);
            this.Controls.Add(this.cmbAllowBatch);
            this.Controls.Add(this.lblAllowBatch);
            this.Controls.Add(this.cmbDefaultGodown);
            this.Controls.Add(this.lblDefaultGodown);
            this.Controls.Add(this.txtMaximumStock);
            this.Controls.Add(this.lblMaximumStock);
            this.Controls.Add(this.cmbTax);
            this.Controls.Add(this.lblTax);
            this.Controls.Add(this.cmbSize);
            this.Controls.Add(this.lblSize);
            this.Controls.Add(this.txtMrp);
            this.Controls.Add(this.txtPurchaseRate);
            this.Controls.Add(this.lblMrp);
            this.Controls.Add(this.lblPurchaseRate);
            this.Controls.Add(this.lblBrand);
            this.Controls.Add(this.txtProductCode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblProductCode);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.dgvProductCreation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmProductCreation";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item Creation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProductCreation_FormClosing);
            this.Load += new System.EventHandler(this.frmProductCreation_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProductCreation_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductCreation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSize;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.TextBox txtMrp;
        private System.Windows.Forms.TextBox txtPurchaseRate;
        private System.Windows.Forms.Label lblMrp;
        private System.Windows.Forms.Label lblPurchaseRate;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.TextBox txtProductCode;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblProductCode;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cmbTax;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.TextBox txtMaximumStock;
        private System.Windows.Forms.Label lblMaximumStock;
        private System.Windows.Forms.ComboBox cmbAllowBatch;
        private System.Windows.Forms.Label lblAllowBatch;
        private System.Windows.Forms.ComboBox cmbDefaultGodown;
        private System.Windows.Forms.Label lblDefaultGodown;
        private System.Windows.Forms.ComboBox cmbBom;
        private System.Windows.Forms.Label lblBom;
        private System.Windows.Forms.Button btnBrandAdd;
        private System.Windows.Forms.Button btnSizeAdd;
        private System.Windows.Forms.Button btnTaxAdd;
        private System.Windows.Forms.Button btnDefaultGodownAdd;
        private System.Windows.Forms.Button btnUnitAdd;
        private System.Windows.Forms.Button btnGroupAdd;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.ComboBox cmbModalNo;
        private System.Windows.Forms.Label lblModalNo;
        private System.Windows.Forms.Button btnModalNo;
        private System.Windows.Forms.TextBox txtReorderLevel;
        private System.Windows.Forms.Label lblReorderLevel;
        private System.Windows.Forms.TextBox txtMinimumStock;
        private System.Windows.Forms.TextBox txtSalesRate;
        private System.Windows.Forms.Label lblMinimumStock;
        private System.Windows.Forms.Label lblSalesRate;
        private System.Windows.Forms.ComboBox cmbTaxApplicableOn;
        private System.Windows.Forms.Label lblTaxApplicableOn;
        private System.Windows.Forms.Button btnDefaultAdd;
        private System.Windows.Forms.ComboBox cmbDefaultRack;
        private System.Windows.Forms.Label lblDefaultRate;
        private System.Windows.Forms.ComboBox cmbMultipleUnit;
        private System.Windows.Forms.Label lblMultipleUnit;
        private System.Windows.Forms.ComboBox cmbOpeningStock;
        private System.Windows.Forms.Label lblOpeningStock;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.CheckBox cbxActive;
        private System.Windows.Forms.CheckBox cbxReminder;
        private System.Windows.Forms.ComboBox cmbBrand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.TextBox txtPartNo;
        private System.Windows.Forms.Label lblPartNo;
        public System.Windows.Forms.ComboBox cmbUnit;
        private System.Windows.Forms.LinkLabel lnklblRemove;
        private dgv.DataGridViewEnter dgvProductCreation;
        private System.Windows.Forms.RadioButton radProduct;
        private System.Windows.Forms.RadioButton radService;
        private System.Windows.Forms.ComboBox cmbSalesAccount;
        private System.Windows.Forms.Label lblSalesAccount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbExpenseAccount;
        private System.Windows.Forms.CheckBox chkAutomaticBarcode;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtslno;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbtgodown;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbrack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtbatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxManfDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtExpDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtqty;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtrate;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcmbUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtbatchId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtstockpostId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtamount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtunit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtProductCode;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbProject;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvCmbCategory;
        private System.Windows.Forms.Label lbltotalInventoryValue;
        private System.Windows.Forms.TextBox txtTotalInventoryValue;
        private System.Windows.Forms.Button btnCreateSalesAccount;
        private System.Windows.Forms.Button btnCreateExpenseAccount;
    }
}