﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LomaCons.InstallKey.ClientSide;
using LomaCons.InstallKey.Common;

namespace MATFinancials.keys
{
    public partial class KeyEntryForm : Form
    {
        #region Private Constant
        /**************************************************
       * These values are specific to your keyset and valiation server
       * Using the InstallKey web applciation, create a keyset and enter values below
       * ***********************************************/
        private const string _keysetName = "MAT_FINANCIALS_KEYSET";
        private const string _password = "mcal@18009";
        private const string _factor = "wiHhDZhNL2rZcx7zUfGoCGAGRAelUZU60hNukxjiGwfzlwifRV79UTyzv+7HWJMZjsLrGkJRiO/4DbLAYHCkPA==";//"Your Keyset Factor goes here";
        private const string _validationCode = "Xi8NXhAEYuQ4YO4GXvaS1l/nWYiXcoOgYpX/EkuOPVIJUaHU/oDckhFQSLPkVxB8hzAYmn6aByUDcqDhjJ+w0w==BJCaXQPWyox8BYhXsMq4a42JtXf+0Ex42YfYaKqQPigJUaHU/oDckhFQSLPkVxB8hzAYmn6aByUDcqDhjJ+w0w==";// "Your Keyset Validation Code goes here";


        private string _validationUrl = MATFinancials.GlobalObject.ValidationUrl;
        
        
        // @"http://magnet-pc:80/InstallKeyWeb/Validator.svc";//@"http://localhost/InstallKeyWeb/Validator.svc";// @"http://localhost/LomaCons.InstallKey.Web/Validator.svc";
        #endregion

        #region Private Members
        EnumValidateResult validateResult;
        public bool IsValidKey { get; private set; }
        
        #endregion

        #region Public Properties
        public string CustomerName { get; set; }
        #endregion

        private static int _dashes = 0;
        private static bool _isFirstTime = true;
        private ClientSideValidator _clientSideValidator;
        
        public KeyEntryForm()
        {
            InitializeComponent();
            InitializeComponentDesigner();

            InitializeComponent1();
            KeyTextValue = string.Empty;
            _clientSideValidator = new ClientSideValidator(_keysetName, _password, _factor, _validationCode);
           
            // check to see if it is already validated and that the surety is still a match
            if (_clientSideValidator.IsValidated)
            {
                // the surety was found and it is a match for the system so no further validation is needed
                CustomerName = _clientSideValidator.LocalSuretyContent.CustomerName;
                IsValidKey = true;
            }
            btnValidate.Click += new EventHandler(btnValidate_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);
            txtProductKey.KeyUp += new KeyEventHandler(txtProductKey_KeyUp);

            Loader();
        }
        
        /// <summary>
        /// holds the value entered by the user
        /// </summary>
        public string KeyTextValue
        {
            get;
            set;
        }
        
        /// <summary>
		/// Validate Button handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnValidate_Click(object sender, EventArgs e)
        {



            // make sure the user entered something
            //if (string.IsNullOrEmpty(txtProductKey.Text) || txtProductKey.Text.Trim().Length == 0)
            //    return;


            // check to see if the value entered is a match for the keyset
            //if (_clientSideValidator.DoesKeyMatchKeyset(txtProductKey.Text.Trim()) == false)
            //{
            //    MessageBox.Show("The product key is not a valid key for the MAT Financials product", "Key Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}

            // KeyTextValue = txtProductKey.Text.Trim();
            // key has been entered (but it still needs to be validated against the server)

            // the user entered a key, so validate it against the server
            // _clientSideValidator.SetServiceAddress(_validationUrl);
            //validateResult = EnumValidateResult.Ok;//Felix

            //try
            //{
            //    validateResult = _clientSideValidator.ValidateAgainstServer(KeyTextValue);
            //}
            //catch (Exception exValidationAgainstServer)
            //{
            //    MessageBox.Show(exValidationAgainstServer.StackTrace);
            //}

            validateResult = EnumValidateResult.Ok;

            switch (validateResult)
            {
                case EnumValidateResult.Ok:
                    // key has been validated and all is well - allow the application to run accordingly
                    IsValidKey = true;
                    MessageBox.Show("The key is valid and has been accepted.", "Key Validation Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // CustomerName = _clientSideValidator.LocalSuretyContent.CustomerName;
                    this.Close();
                    break;
                case EnumValidateResult.InvalidKey:
                    IsValidKey = false;
                    MessageBox.Show("The key entered is not a valid key", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    break;
                case EnumValidateResult.Revoked:
                    IsValidKey = false;
                    MessageBox.Show("The key has been revoked and is not longer valid", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    break;
                case EnumValidateResult.Violation:
                    IsValidKey = false;
                    MessageBox.Show("The key has been used more times than is allowed", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    break;
                default:
                    // this will not happen because we have switched on all possible values
                    IsValidKey = false;
                    throw (new NotImplementedException(validateResult.ToString()));
            }
            
            return;
        }

        private void Loader()
        {
            try
            {
                validateResult = EnumValidateResult.Ok;

                switch (validateResult)
                {
                    case EnumValidateResult.Ok:
                        // key has been validated and all is well - allow the application to run accordingly
                        IsValidKey = true;
                        MessageBox.Show("The key is valid and has been accepted.", "Key Validation Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        // CustomerName = _clientSideValidator.LocalSuretyContent.CustomerName;
                        this.Close();
                        break;
                    case EnumValidateResult.InvalidKey:
                        IsValidKey = false;
                        MessageBox.Show("The key entered is not a valid key", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                        break;
                    case EnumValidateResult.Revoked:
                        IsValidKey = false;
                        MessageBox.Show("The key has been revoked and is not longer valid", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                        break;
                    case EnumValidateResult.Violation:
                        IsValidKey = false;
                        MessageBox.Show("The key has been used more times than is allowed", "Key Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                        break;
                    default:
                        // this will not happen because we have switched on all possible values
                        IsValidKey = false;
                        throw (new NotImplementedException(validateResult.ToString()));
                }
            }
            catch(Exception ex)
            {

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            IsValidKey = false;
            var response = MessageBox.Show("Are you sure you want to discontinue?");
            if (response == DialogResult.Yes) this.Close();
        }

        protected void txtProductKey_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e) {
            try { 
            if (string.IsNullOrEmpty(txtProductKey.Text)) return;
                if (!((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z) || (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)))
                    return;
                    char[] keys = new char[36];
            string temp = string.Empty;
            int start = 0, increment = 5, end = -1; // start + increment;
            for (int counter = 1; counter < 6; ++counter)
            {
                start = end + 1 ; end = start + increment ;
                if (txtProductKey.TextLength >= end)
                {
                    for (int j = start; j < end; ++j)
                    {
                        keys[j] = txtProductKey.Text.ToArray()[j];
                    }
                    keys[end] = '-';
                    temp = string.Join("", keys.Where(c => c != '\0').Select(c => c).ToArray());
                }
            }
            
            if (!string.IsNullOrEmpty(temp) && temp.Trim().Length < txtProductKey.Text.Trim().Length)
            {
                txtProductKey.Text = temp.Trim() + txtProductKey.Text.Trim().Substring(temp.Trim().Length, txtProductKey.Text.Trim().Length - temp.Trim().Length);
                txtProductKey.Select(txtProductKey.Text.Length, 0);
            }
            else if (!string.IsNullOrEmpty(temp))
            {
                txtProductKey.Text = temp.Trim();
                txtProductKey.Select(txtProductKey.Text.Length, 0);
            }
            }
            catch (Exception ex)
            {

            }
            finally {
            }
        }

        private void KeyEntryForm_Load(object sender, EventArgs e)
        {
            Loader();
        }

        private void InitializeComponent1()
        {
            this.SuspendLayout();
            // 
            // KeyEntryForm
            // 
            this.ClientSize = new System.Drawing.Size(413, 167);
            this.Name = "KeyEntryForm";
            this.Load += new System.EventHandler(this.KeyEntryForm_Load);
            this.ResumeLayout(false);

        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // KeyEntryForm
            // 
            this.ClientSize = new System.Drawing.Size(413, 167);
            this.Name = "KeyEntryForm";
            this.ResumeLayout(false);

        }

        //txtProductKey_KeyUp
    }
}
