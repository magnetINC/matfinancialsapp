﻿namespace MATFinancials
{
    partial class formMDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMDI));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.Home = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createCompanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SelectCompanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalJournalsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.runGeneralJournalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editCompanyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCompanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mastersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountGroupToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleAcctLedgerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountCodesRangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountCodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chartOfAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyListToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exchangeRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetSettingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetVarianceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCurrentDateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.roleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rolePrivilegeSettingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userCreationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newFinancialYearToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFinancialYearToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodeSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodePrintingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.surfixPrefixSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeProductTaxToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dotmatrixPrintDesignerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rebuildIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BackUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RestoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOSToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesQuotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionInToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesInvoiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCReceivableToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditNoteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesQuotationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryNoteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionInToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesInvoiceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCReceivableToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.creditNoteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pOSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingSalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mastersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountLedgerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountGroupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleAcctLedgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chartOfAccountToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exchangeRateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesManToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.counterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetVarianceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.areaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.routeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierCenterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.materialReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionOutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseInvoiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCPayableToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.debitNoteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.registersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.materialReceiptToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionOutToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseInvoiceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCPayableToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.debitNoteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemCreationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleItemCreationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemComponentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.brandToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modelNoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.unitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.storeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rackToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.priceListToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pricingLevelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.priceListToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.standardRateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.physicalStockToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockJournalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.physicalStockToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockJournalToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contraVoucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankReconciliationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalVoucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.bankTransfersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalJournalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billAllocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financialStatementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trialBalanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profitAndLossToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profitAndLossByProjectAndCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceSheetToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashFlowToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fundFlowToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oldProfitAndLossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oldBalanceSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pLByCostOfSalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vATReturnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalJournalsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionDetailsByLedgersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPAgeingSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierBalanceSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materialReceiptToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCPayablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debitNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aRAgeingSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerBalanceSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesQuotationToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryNoteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCReceivableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.creditNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shiftReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockJournalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockVarianceReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryMovementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.physicalStockReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReport2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockDetainsReportNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transfersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashBankBookToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeCashDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bankReconciliationToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountLedgersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierCustomerListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountListToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyAttendanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyAttendanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailySalaryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalaryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.payheadToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPackageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.advancePaymentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.bonusDeductionToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeAddressBookToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionUnitReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.contraReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCPayableReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCReceivableReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDCClearanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materialReceiptReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionOutReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseInvoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesQuotationReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryNoteReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectionInReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesInvoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.physicalStockReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditNoteReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debitNoteReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockJournalReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlyAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailySalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payheadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancePaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bonusDeductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeAddressBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayBookToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.PartyAddressBooktoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StockReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShortExpiryReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductStaticstoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PriceListReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TaxReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VatReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChequeReporttoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.freeSaleReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productVsBatchReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockVarianceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherWiseProductSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payrollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.payHeadToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPackageCreationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeCreationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.holidaySettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statutoryComplianceSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalarySettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mmAttendance = new System.Windows.Forms.ToolStripMenuItem();
            this.advancePaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bonusDeductionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.frmMonthlySalaryVoucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DailySalaryVouchertoolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.paySlipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registersToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPackageRegisterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeRegisterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bonusDeductionRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalaryRegisterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailySalaryRegisterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reminderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.personalReminderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overduePurchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overdueSalesOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shortExpiryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miracleSkateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miracleIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactUsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.BackgrndWrkrLoading = new System.ComponentModel.BackgroundWorker();
            this.ucCalculator1 = new MATFinancials.ucCalculator();
            this.ucQuickLaunch1 = new MATFinancials.ucQuickLaunch();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Home,
            this.companyToolStripMenuItem,
            this.pOSToolStripMenuItem2,
            this.customerStripMenuItem,
            this.mastersToolStripMenuItem,
            this.supplierStripMenuItem1,
            this.inventoryStripMenuItem,
            this.transactionToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.payrollToolStripMenuItem,
            this.reminderToolStripMenuItem,
            this.utilitiesToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.dateToolStripMenuItem,
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(1354, 27);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // Home
            // 
            this.Home.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.Home.Image = ((System.Drawing.Image)(resources.GetObject("Home.Image")));
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(71, 21);
            this.Home.Text = "Home";
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCompanyToolStripMenuItem,
            this.SelectCompanyToolStripMenuItem,
            this.generalJournalsToolStripMenuItem2,
            this.editCompanyToolStripMenuItem1,
            this.deleteCompanyToolStripMenuItem,
            this.mastersToolStripMenuItem1,
            this.settingsToolStripMenuItem2,
            this.BackUpToolStripMenuItem,
            this.RestoreToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.companyToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.companyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("companyToolStripMenuItem.Image")));
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(91, 21);
            this.companyToolStripMenuItem.Text = "&Company";
            // 
            // createCompanyToolStripMenuItem
            // 
            this.createCompanyToolStripMenuItem.Name = "createCompanyToolStripMenuItem";
            this.createCompanyToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.createCompanyToolStripMenuItem.Text = "Create Company";
            this.createCompanyToolStripMenuItem.Click += new System.EventHandler(this.createCompanyToolStripMenuItem_Click);
            // 
            // SelectCompanyToolStripMenuItem
            // 
            this.SelectCompanyToolStripMenuItem.Name = "SelectCompanyToolStripMenuItem";
            this.SelectCompanyToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.SelectCompanyToolStripMenuItem.Text = "Select Company";
            this.SelectCompanyToolStripMenuItem.Click += new System.EventHandler(this.SelectCompanyToolStripMenuItem_Click);
            // 
            // generalJournalsToolStripMenuItem2
            // 
            this.generalJournalsToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runGeneralJournalToolStripMenuItem,
            this.journalRegisterToolStripMenuItem});
            this.generalJournalsToolStripMenuItem2.Name = "generalJournalsToolStripMenuItem2";
            this.generalJournalsToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.generalJournalsToolStripMenuItem2.Text = "General Journals";
            this.generalJournalsToolStripMenuItem2.Click += new System.EventHandler(this.generalJournalsToolStripMenuItem2_Click);
            // 
            // runGeneralJournalToolStripMenuItem
            // 
            this.runGeneralJournalToolStripMenuItem.Name = "runGeneralJournalToolStripMenuItem";
            this.runGeneralJournalToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.runGeneralJournalToolStripMenuItem.Text = "Run General Journal";
            this.runGeneralJournalToolStripMenuItem.Click += new System.EventHandler(this.runGeneralJournalToolStripMenuItem_Click);
            // 
            // journalRegisterToolStripMenuItem
            // 
            this.journalRegisterToolStripMenuItem.Name = "journalRegisterToolStripMenuItem";
            this.journalRegisterToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.journalRegisterToolStripMenuItem.Text = "Journal Register";
            this.journalRegisterToolStripMenuItem.Click += new System.EventHandler(this.journalRegisterToolStripMenuItem_Click);
            // 
            // editCompanyToolStripMenuItem1
            // 
            this.editCompanyToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.editCompanyToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.editCompanyToolStripMenuItem1.Name = "editCompanyToolStripMenuItem1";
            this.editCompanyToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.editCompanyToolStripMenuItem1.Text = "Edit Company";
            this.editCompanyToolStripMenuItem1.Click += new System.EventHandler(this.editCompanyToolStripMenuItem1_Click);
            // 
            // deleteCompanyToolStripMenuItem
            // 
            this.deleteCompanyToolStripMenuItem.Name = "deleteCompanyToolStripMenuItem";
            this.deleteCompanyToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.deleteCompanyToolStripMenuItem.Text = "Delete This Company";
            this.deleteCompanyToolStripMenuItem.Click += new System.EventHandler(this.deleteCompanyToolStripMenuItem_Click);
            // 
            // mastersToolStripMenuItem1
            // 
            this.mastersToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountToolStripMenuItem1,
            this.taxToolStripMenuItem2,
            this.currencyToolStripMenuItem,
            this.formTypeToolStripMenuItem,
            this.projectsToolStripMenuItem,
            this.categoryToolStripMenuItem1,
            this.salesmanToolStripMenuItem,
            this.counterToolStripMenuItem1,
            this.budgetToolStripMenuItem,
            this.stateToolStripMenuItem,
            this.cityToolStripMenuItem});
            this.mastersToolStripMenuItem1.Name = "mastersToolStripMenuItem1";
            this.mastersToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.mastersToolStripMenuItem1.Text = "Masters";
            // 
            // accountToolStripMenuItem1
            // 
            this.accountToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountLedgerToolStripMenuItem,
            this.accountGroupToolStripMenuItem2,
            this.multipleAcctLedgerToolStripMenuItem1,
            this.accountCodesRangeToolStripMenuItem,
            this.accountCodesToolStripMenuItem,
            this.chartOfAccountToolStripMenuItem});
            this.accountToolStripMenuItem1.Name = "accountToolStripMenuItem1";
            this.accountToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.accountToolStripMenuItem1.Text = "Account";
            // 
            // accountLedgerToolStripMenuItem
            // 
            this.accountLedgerToolStripMenuItem.Name = "accountLedgerToolStripMenuItem";
            this.accountLedgerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.accountLedgerToolStripMenuItem.Text = "Account Ledger";
            this.accountLedgerToolStripMenuItem.Click += new System.EventHandler(this.accountLedgerToolStripMenuItem_Click_1);
            // 
            // accountGroupToolStripMenuItem2
            // 
            this.accountGroupToolStripMenuItem2.Name = "accountGroupToolStripMenuItem2";
            this.accountGroupToolStripMenuItem2.Size = new System.Drawing.Size(204, 22);
            this.accountGroupToolStripMenuItem2.Text = "Account group";
            this.accountGroupToolStripMenuItem2.Click += new System.EventHandler(this.accountGroupToolStripMenuItem2_Click);
            // 
            // multipleAcctLedgerToolStripMenuItem1
            // 
            this.multipleAcctLedgerToolStripMenuItem1.Enabled = false;
            this.multipleAcctLedgerToolStripMenuItem1.Name = "multipleAcctLedgerToolStripMenuItem1";
            this.multipleAcctLedgerToolStripMenuItem1.Size = new System.Drawing.Size(204, 22);
            this.multipleAcctLedgerToolStripMenuItem1.Text = "Multiple Acct Ledger";
            this.multipleAcctLedgerToolStripMenuItem1.Visible = false;
            this.multipleAcctLedgerToolStripMenuItem1.Click += new System.EventHandler(this.multipleAcctLedgerToolStripMenuItem1_Click);
            // 
            // accountCodesRangeToolStripMenuItem
            // 
            this.accountCodesRangeToolStripMenuItem.Name = "accountCodesRangeToolStripMenuItem";
            this.accountCodesRangeToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.accountCodesRangeToolStripMenuItem.Text = "Account Codes Range";
            this.accountCodesRangeToolStripMenuItem.Visible = false;
            this.accountCodesRangeToolStripMenuItem.Click += new System.EventHandler(this.accountCodesRangeToolStripMenuItem_Click);
            // 
            // accountCodesToolStripMenuItem
            // 
            this.accountCodesToolStripMenuItem.Name = "accountCodesToolStripMenuItem";
            this.accountCodesToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.accountCodesToolStripMenuItem.Text = "Account Codes";
            this.accountCodesToolStripMenuItem.Visible = false;
            this.accountCodesToolStripMenuItem.Click += new System.EventHandler(this.accountCodesToolStripMenuItem_Click);
            // 
            // chartOfAccountToolStripMenuItem
            // 
            this.chartOfAccountToolStripMenuItem.Name = "chartOfAccountToolStripMenuItem";
            this.chartOfAccountToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.chartOfAccountToolStripMenuItem.Text = "Chart Of Account";
            this.chartOfAccountToolStripMenuItem.Click += new System.EventHandler(this.chartOfAccountToolStripMenuItem_Click_1);
            // 
            // taxToolStripMenuItem2
            // 
            this.taxToolStripMenuItem2.Name = "taxToolStripMenuItem2";
            this.taxToolStripMenuItem2.Size = new System.Drawing.Size(139, 22);
            this.taxToolStripMenuItem2.Text = "Tax";
            this.taxToolStripMenuItem2.Click += new System.EventHandler(this.taxToolStripMenuItem2_Click);
            // 
            // currencyToolStripMenuItem
            // 
            this.currencyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currencyListToolStripMenuItem1,
            this.exchangeRateToolStripMenuItem});
            this.currencyToolStripMenuItem.Name = "currencyToolStripMenuItem";
            this.currencyToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.currencyToolStripMenuItem.Text = "Currency";
            // 
            // currencyListToolStripMenuItem1
            // 
            this.currencyListToolStripMenuItem1.Name = "currencyListToolStripMenuItem1";
            this.currencyListToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.currencyListToolStripMenuItem1.Text = "Currency List";
            this.currencyListToolStripMenuItem1.Click += new System.EventHandler(this.currencyListToolStripMenuItem1_Click);
            // 
            // exchangeRateToolStripMenuItem
            // 
            this.exchangeRateToolStripMenuItem.Name = "exchangeRateToolStripMenuItem";
            this.exchangeRateToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.exchangeRateToolStripMenuItem.Text = "Exchange Rate";
            this.exchangeRateToolStripMenuItem.Click += new System.EventHandler(this.exchangeRateToolStripMenuItem_Click_1);
            // 
            // formTypeToolStripMenuItem
            // 
            this.formTypeToolStripMenuItem.Name = "formTypeToolStripMenuItem";
            this.formTypeToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.formTypeToolStripMenuItem.Text = "Form Type";
            this.formTypeToolStripMenuItem.Click += new System.EventHandler(this.formTypeToolStripMenuItem_Click);
            // 
            // projectsToolStripMenuItem
            // 
            this.projectsToolStripMenuItem.Name = "projectsToolStripMenuItem";
            this.projectsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.projectsToolStripMenuItem.Text = "Projects";
            this.projectsToolStripMenuItem.Click += new System.EventHandler(this.projectsToolStripMenuItem_Click);
            // 
            // categoryToolStripMenuItem1
            // 
            this.categoryToolStripMenuItem1.Name = "categoryToolStripMenuItem1";
            this.categoryToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.categoryToolStripMenuItem1.Text = "Category";
            this.categoryToolStripMenuItem1.Click += new System.EventHandler(this.categoryToolStripMenuItem1_Click);
            // 
            // salesmanToolStripMenuItem
            // 
            this.salesmanToolStripMenuItem.Name = "salesmanToolStripMenuItem";
            this.salesmanToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.salesmanToolStripMenuItem.Text = "Salesman";
            this.salesmanToolStripMenuItem.Click += new System.EventHandler(this.salesmanToolStripMenuItem_Click_1);
            // 
            // counterToolStripMenuItem1
            // 
            this.counterToolStripMenuItem1.Name = "counterToolStripMenuItem1";
            this.counterToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.counterToolStripMenuItem1.Text = "Sales Point";
            this.counterToolStripMenuItem1.Click += new System.EventHandler(this.counterToolStripMenuItem1_Click);
            // 
            // budgetToolStripMenuItem
            // 
            this.budgetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.budgetSettingsToolStripMenuItem1,
            this.budgetVarianceToolStripMenuItem});
            this.budgetToolStripMenuItem.Name = "budgetToolStripMenuItem";
            this.budgetToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.budgetToolStripMenuItem.Text = "Budget";
            // 
            // budgetSettingsToolStripMenuItem1
            // 
            this.budgetSettingsToolStripMenuItem1.Name = "budgetSettingsToolStripMenuItem1";
            this.budgetSettingsToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.budgetSettingsToolStripMenuItem1.Text = "Budget Settings";
            this.budgetSettingsToolStripMenuItem1.Click += new System.EventHandler(this.budgetSettingsToolStripMenuItem1_Click);
            // 
            // budgetVarianceToolStripMenuItem
            // 
            this.budgetVarianceToolStripMenuItem.Name = "budgetVarianceToolStripMenuItem";
            this.budgetVarianceToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.budgetVarianceToolStripMenuItem.Text = "Budget Variance";
            this.budgetVarianceToolStripMenuItem.Click += new System.EventHandler(this.budgetVarianceToolStripMenuItem_Click_1);
            // 
            // stateToolStripMenuItem
            // 
            this.stateToolStripMenuItem.Name = "stateToolStripMenuItem";
            this.stateToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.stateToolStripMenuItem.Text = "State";
            this.stateToolStripMenuItem.Click += new System.EventHandler(this.stateToolStripMenuItem_Click);
            // 
            // cityToolStripMenuItem
            // 
            this.cityToolStripMenuItem.Name = "cityToolStripMenuItem";
            this.cityToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.cityToolStripMenuItem.Text = "City";
            this.cityToolStripMenuItem.Click += new System.EventHandler(this.cityToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem2
            // 
            this.settingsToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeCurrentDateToolStripMenuItem1,
            this.settingsToolStripMenuItem3,
            this.roleToolStripMenuItem1,
            this.rolePrivilegeSettingToolStripMenuItem,
            this.userCreationToolStripMenuItem1,
            this.changePasswordToolStripMenuItem1,
            this.newFinancialYearToolStripMenuItem1,
            this.changeFinancialYearToolStripMenuItem1,
            this.barcodeSettingsToolStripMenuItem,
            this.barcodePrintingToolStripMenuItem1,
            this.surfixPrefixSettingsToolStripMenuItem,
            this.changeProductTaxToolStripMenuItem1,
            this.dotmatrixPrintDesignerToolStripMenuItem1,
            this.rebuildIndexToolStripMenuItem,
            this.emailConfigurationToolStripMenuItem});
            this.settingsToolStripMenuItem2.Image = global::MATFinancials.Properties.Resources.Setting;
            this.settingsToolStripMenuItem2.Name = "settingsToolStripMenuItem2";
            this.settingsToolStripMenuItem2.Size = new System.Drawing.Size(199, 22);
            this.settingsToolStripMenuItem2.Text = "&Settings";
            // 
            // changeCurrentDateToolStripMenuItem1
            // 
            this.changeCurrentDateToolStripMenuItem1.Name = "changeCurrentDateToolStripMenuItem1";
            this.changeCurrentDateToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.changeCurrentDateToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.changeCurrentDateToolStripMenuItem1.Text = "Change Current Date";
            this.changeCurrentDateToolStripMenuItem1.Click += new System.EventHandler(this.changeCurrentDateToolStripMenuItem1_Click);
            // 
            // settingsToolStripMenuItem3
            // 
            this.settingsToolStripMenuItem3.Name = "settingsToolStripMenuItem3";
            this.settingsToolStripMenuItem3.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.settingsToolStripMenuItem3.Size = new System.Drawing.Size(219, 22);
            this.settingsToolStripMenuItem3.Text = "Settings";
            this.settingsToolStripMenuItem3.Click += new System.EventHandler(this.settingsToolStripMenuItem3_Click);
            // 
            // roleToolStripMenuItem1
            // 
            this.roleToolStripMenuItem1.Name = "roleToolStripMenuItem1";
            this.roleToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.roleToolStripMenuItem1.Text = "Role";
            this.roleToolStripMenuItem1.Click += new System.EventHandler(this.roleToolStripMenuItem1_Click);
            // 
            // rolePrivilegeSettingToolStripMenuItem
            // 
            this.rolePrivilegeSettingToolStripMenuItem.Name = "rolePrivilegeSettingToolStripMenuItem";
            this.rolePrivilegeSettingToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.rolePrivilegeSettingToolStripMenuItem.Text = "Role Privilege Settings";
            this.rolePrivilegeSettingToolStripMenuItem.Click += new System.EventHandler(this.rolePrivilegeSettingToolStripMenuItem_Click);
            // 
            // userCreationToolStripMenuItem1
            // 
            this.userCreationToolStripMenuItem1.Name = "userCreationToolStripMenuItem1";
            this.userCreationToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.userCreationToolStripMenuItem1.Text = "User Creation";
            this.userCreationToolStripMenuItem1.Click += new System.EventHandler(this.userCreationToolStripMenuItem1_Click);
            // 
            // changePasswordToolStripMenuItem1
            // 
            this.changePasswordToolStripMenuItem1.Name = "changePasswordToolStripMenuItem1";
            this.changePasswordToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.changePasswordToolStripMenuItem1.Text = "Change Password";
            this.changePasswordToolStripMenuItem1.Click += new System.EventHandler(this.changePasswordToolStripMenuItem1_Click);
            // 
            // newFinancialYearToolStripMenuItem1
            // 
            this.newFinancialYearToolStripMenuItem1.Name = "newFinancialYearToolStripMenuItem1";
            this.newFinancialYearToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.newFinancialYearToolStripMenuItem1.Text = "New Financial Year";
            this.newFinancialYearToolStripMenuItem1.Click += new System.EventHandler(this.newFinancialYearToolStripMenuItem1_Click);
            // 
            // changeFinancialYearToolStripMenuItem1
            // 
            this.changeFinancialYearToolStripMenuItem1.Name = "changeFinancialYearToolStripMenuItem1";
            this.changeFinancialYearToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.changeFinancialYearToolStripMenuItem1.Text = "Change Financial Year";
            this.changeFinancialYearToolStripMenuItem1.Click += new System.EventHandler(this.changeFinancialYearToolStripMenuItem1_Click);
            // 
            // barcodeSettingsToolStripMenuItem
            // 
            this.barcodeSettingsToolStripMenuItem.Name = "barcodeSettingsToolStripMenuItem";
            this.barcodeSettingsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.barcodeSettingsToolStripMenuItem.Text = "Barcode Settings";
            this.barcodeSettingsToolStripMenuItem.Click += new System.EventHandler(this.barcodeSettingsToolStripMenuItem_Click);
            // 
            // barcodePrintingToolStripMenuItem1
            // 
            this.barcodePrintingToolStripMenuItem1.Name = "barcodePrintingToolStripMenuItem1";
            this.barcodePrintingToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.barcodePrintingToolStripMenuItem1.Text = "Barcode Printing";
            this.barcodePrintingToolStripMenuItem1.Click += new System.EventHandler(this.barcodePrintingToolStripMenuItem1_Click);
            // 
            // surfixPrefixSettingsToolStripMenuItem
            // 
            this.surfixPrefixSettingsToolStripMenuItem.Name = "surfixPrefixSettingsToolStripMenuItem";
            this.surfixPrefixSettingsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.surfixPrefixSettingsToolStripMenuItem.Text = "Surfix Prefix Settings";
            this.surfixPrefixSettingsToolStripMenuItem.Click += new System.EventHandler(this.surfixPrefixSettingsToolStripMenuItem_Click);
            // 
            // changeProductTaxToolStripMenuItem1
            // 
            this.changeProductTaxToolStripMenuItem1.Name = "changeProductTaxToolStripMenuItem1";
            this.changeProductTaxToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.changeProductTaxToolStripMenuItem1.Text = "Change Product Tax";
            this.changeProductTaxToolStripMenuItem1.Click += new System.EventHandler(this.changeProductTaxToolStripMenuItem1_Click);
            // 
            // dotmatrixPrintDesignerToolStripMenuItem1
            // 
            this.dotmatrixPrintDesignerToolStripMenuItem1.Name = "dotmatrixPrintDesignerToolStripMenuItem1";
            this.dotmatrixPrintDesignerToolStripMenuItem1.Size = new System.Drawing.Size(219, 22);
            this.dotmatrixPrintDesignerToolStripMenuItem1.Text = "Dotmatrix Print Designer";
            this.dotmatrixPrintDesignerToolStripMenuItem1.Click += new System.EventHandler(this.dotmatrixPrintDesignerToolStripMenuItem1_Click);
            // 
            // rebuildIndexToolStripMenuItem
            // 
            this.rebuildIndexToolStripMenuItem.Name = "rebuildIndexToolStripMenuItem";
            this.rebuildIndexToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.rebuildIndexToolStripMenuItem.Text = "Rebuild Index";
            this.rebuildIndexToolStripMenuItem.Click += new System.EventHandler(this.rebuildIndexToolStripMenuItem_Click);
            // 
            // emailConfigurationToolStripMenuItem
            // 
            this.emailConfigurationToolStripMenuItem.Name = "emailConfigurationToolStripMenuItem";
            this.emailConfigurationToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.emailConfigurationToolStripMenuItem.Text = "Email Configuration";
            this.emailConfigurationToolStripMenuItem.Click += new System.EventHandler(this.emailConfigurationToolStripMenuItem_Click);
            // 
            // BackUpToolStripMenuItem
            // 
            this.BackUpToolStripMenuItem.Name = "BackUpToolStripMenuItem";
            this.BackUpToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.BackUpToolStripMenuItem.Text = "BackUp";
            this.BackUpToolStripMenuItem.Click += new System.EventHandler(this.BackUpToolStripMenuItem_Click);
            // 
            // RestoreToolStripMenuItem
            // 
            this.RestoreToolStripMenuItem.Name = "RestoreToolStripMenuItem";
            this.RestoreToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.RestoreToolStripMenuItem.Text = "Restore";
            this.RestoreToolStripMenuItem.Click += new System.EventHandler(this.RestoreToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.logoutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click_1);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.closeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // pOSToolStripMenuItem2
            // 
            this.pOSToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("pOSToolStripMenuItem2.Image")));
            this.pOSToolStripMenuItem2.Name = "pOSToolStripMenuItem2";
            this.pOSToolStripMenuItem2.Size = new System.Drawing.Size(57, 21);
            this.pOSToolStripMenuItem2.Text = "POS";
            this.pOSToolStripMenuItem2.Visible = false;
            this.pOSToolStripMenuItem2.Click += new System.EventHandler(this.pOSToolStripMenuItem2_Click);
            // 
            // customerStripMenuItem
            // 
            this.customerStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem1,
            this.salesQuotationToolStripMenuItem,
            this.salesOrderToolStripMenuItem1,
            this.deliveryNoteToolStripMenuItem,
            this.rejectionInToolStripMenuItem1,
            this.salesInvoiceToolStripMenuItem1,
            this.salesReturnToolStripMenuItem1,
            this.pDCReceivableToolStripMenuItem1,
            this.pDCClearanceToolStripMenuItem2,
            this.receiptsToolStripMenuItem,
            this.creditNoteToolStripMenuItem1,
            this.registersToolStripMenuItem,
            this.pOSToolStripMenuItem1,
            this.pendingSalesToolStripMenuItem});
            this.customerStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.customerStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("customerStripMenuItem.Image")));
            this.customerStripMenuItem.Name = "customerStripMenuItem";
            this.customerStripMenuItem.Size = new System.Drawing.Size(98, 21);
            this.customerStripMenuItem.Text = "&Customers";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.customerToolStripMenuItem1.Text = "Customer Center";
            this.customerToolStripMenuItem1.Click += new System.EventHandler(this.customerToolStripMenuItem1_Click);
            // 
            // salesQuotationToolStripMenuItem
            // 
            this.salesQuotationToolStripMenuItem.Name = "salesQuotationToolStripMenuItem";
            this.salesQuotationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Q)));
            this.salesQuotationToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.salesQuotationToolStripMenuItem.Text = "Sales Quotation";
            this.salesQuotationToolStripMenuItem.Click += new System.EventHandler(this.salesQuotationToolStripMenuItem_Click);
            // 
            // salesOrderToolStripMenuItem1
            // 
            this.salesOrderToolStripMenuItem1.Name = "salesOrderToolStripMenuItem1";
            this.salesOrderToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F8)));
            this.salesOrderToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.salesOrderToolStripMenuItem1.Text = "Sales Order";
            this.salesOrderToolStripMenuItem1.Click += new System.EventHandler(this.salesOrderToolStripMenuItem1_Click);
            // 
            // deliveryNoteToolStripMenuItem
            // 
            this.deliveryNoteToolStripMenuItem.Name = "deliveryNoteToolStripMenuItem";
            this.deliveryNoteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F8)));
            this.deliveryNoteToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.deliveryNoteToolStripMenuItem.Text = "Delivery Note";
            this.deliveryNoteToolStripMenuItem.Click += new System.EventHandler(this.deliveryNoteToolStripMenuItem_Click);
            // 
            // rejectionInToolStripMenuItem1
            // 
            this.rejectionInToolStripMenuItem1.Name = "rejectionInToolStripMenuItem1";
            this.rejectionInToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F6)));
            this.rejectionInToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.rejectionInToolStripMenuItem1.Text = "Rejection In";
            this.rejectionInToolStripMenuItem1.Click += new System.EventHandler(this.rejectionInToolStripMenuItem1_Click);
            // 
            // salesInvoiceToolStripMenuItem1
            // 
            this.salesInvoiceToolStripMenuItem1.Name = "salesInvoiceToolStripMenuItem1";
            this.salesInvoiceToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.salesInvoiceToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.salesInvoiceToolStripMenuItem1.Text = "Sales Invoice";
            this.salesInvoiceToolStripMenuItem1.Click += new System.EventHandler(this.salesInvoiceToolStripMenuItem1_Click);
            // 
            // salesReturnToolStripMenuItem1
            // 
            this.salesReturnToolStripMenuItem1.Name = "salesReturnToolStripMenuItem1";
            this.salesReturnToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.salesReturnToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.salesReturnToolStripMenuItem1.Text = "Sales Return";
            this.salesReturnToolStripMenuItem1.Click += new System.EventHandler(this.salesReturnToolStripMenuItem1_Click);
            // 
            // pDCReceivableToolStripMenuItem1
            // 
            this.pDCReceivableToolStripMenuItem1.Name = "pDCReceivableToolStripMenuItem1";
            this.pDCReceivableToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.pDCReceivableToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.pDCReceivableToolStripMenuItem1.Text = "PDC Receivable";
            this.pDCReceivableToolStripMenuItem1.Click += new System.EventHandler(this.pDCReceivableToolStripMenuItem1_Click);
            // 
            // pDCClearanceToolStripMenuItem2
            // 
            this.pDCClearanceToolStripMenuItem2.Name = "pDCClearanceToolStripMenuItem2";
            this.pDCClearanceToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.pDCClearanceToolStripMenuItem2.Size = new System.Drawing.Size(210, 22);
            this.pDCClearanceToolStripMenuItem2.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem2.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem2_Click);
            // 
            // receiptsToolStripMenuItem
            // 
            this.receiptsToolStripMenuItem.Name = "receiptsToolStripMenuItem";
            this.receiptsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.receiptsToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.receiptsToolStripMenuItem.Text = "Receipts";
            this.receiptsToolStripMenuItem.Click += new System.EventHandler(this.receiptsToolStripMenuItem_Click);
            // 
            // creditNoteToolStripMenuItem1
            // 
            this.creditNoteToolStripMenuItem1.Name = "creditNoteToolStripMenuItem1";
            this.creditNoteToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F8)));
            this.creditNoteToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.creditNoteToolStripMenuItem1.Text = "Credit Note";
            this.creditNoteToolStripMenuItem1.Click += new System.EventHandler(this.creditNoteToolStripMenuItem1_Click);
            // 
            // registersToolStripMenuItem
            // 
            this.registersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesQuotationToolStripMenuItem1,
            this.salesOrderToolStripMenuItem2,
            this.deliveryNoteToolStripMenuItem1,
            this.rejectionInToolStripMenuItem2,
            this.salesInvoiceToolStripMenuItem2,
            this.salesReturnsToolStripMenuItem,
            this.pDCReceivableToolStripMenuItem2,
            this.pDCClearanceToolStripMenuItem3,
            this.creditNoteToolStripMenuItem2,
            this.receiptsToolStripMenuItem1});
            this.registersToolStripMenuItem.Name = "registersToolStripMenuItem";
            this.registersToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.registersToolStripMenuItem.Text = "Registers";
            // 
            // salesQuotationToolStripMenuItem1
            // 
            this.salesQuotationToolStripMenuItem1.Name = "salesQuotationToolStripMenuItem1";
            this.salesQuotationToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.salesQuotationToolStripMenuItem1.Text = "Sales Quotation";
            this.salesQuotationToolStripMenuItem1.Click += new System.EventHandler(this.salesQuotationToolStripMenuItem1_Click);
            // 
            // salesOrderToolStripMenuItem2
            // 
            this.salesOrderToolStripMenuItem2.Name = "salesOrderToolStripMenuItem2";
            this.salesOrderToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.salesOrderToolStripMenuItem2.Text = "Sales Order";
            this.salesOrderToolStripMenuItem2.Click += new System.EventHandler(this.salesOrderToolStripMenuItem2_Click);
            // 
            // deliveryNoteToolStripMenuItem1
            // 
            this.deliveryNoteToolStripMenuItem1.Name = "deliveryNoteToolStripMenuItem1";
            this.deliveryNoteToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.deliveryNoteToolStripMenuItem1.Text = "Delivery Note";
            this.deliveryNoteToolStripMenuItem1.Click += new System.EventHandler(this.deliveryNoteToolStripMenuItem1_Click);
            // 
            // rejectionInToolStripMenuItem2
            // 
            this.rejectionInToolStripMenuItem2.Name = "rejectionInToolStripMenuItem2";
            this.rejectionInToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.rejectionInToolStripMenuItem2.Text = "Rejection In";
            this.rejectionInToolStripMenuItem2.Click += new System.EventHandler(this.rejectionInToolStripMenuItem2_Click);
            // 
            // salesInvoiceToolStripMenuItem2
            // 
            this.salesInvoiceToolStripMenuItem2.Name = "salesInvoiceToolStripMenuItem2";
            this.salesInvoiceToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.salesInvoiceToolStripMenuItem2.Text = "Sales Invoice";
            this.salesInvoiceToolStripMenuItem2.Click += new System.EventHandler(this.salesInvoiceToolStripMenuItem2_Click);
            // 
            // salesReturnsToolStripMenuItem
            // 
            this.salesReturnsToolStripMenuItem.Name = "salesReturnsToolStripMenuItem";
            this.salesReturnsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.salesReturnsToolStripMenuItem.Text = "Sales Returns";
            this.salesReturnsToolStripMenuItem.Click += new System.EventHandler(this.salesReturnsToolStripMenuItem_Click);
            // 
            // pDCReceivableToolStripMenuItem2
            // 
            this.pDCReceivableToolStripMenuItem2.Name = "pDCReceivableToolStripMenuItem2";
            this.pDCReceivableToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.pDCReceivableToolStripMenuItem2.Text = "PDC Receivable";
            this.pDCReceivableToolStripMenuItem2.Click += new System.EventHandler(this.pDCReceivableToolStripMenuItem2_Click);
            // 
            // pDCClearanceToolStripMenuItem3
            // 
            this.pDCClearanceToolStripMenuItem3.Name = "pDCClearanceToolStripMenuItem3";
            this.pDCClearanceToolStripMenuItem3.Size = new System.Drawing.Size(168, 22);
            this.pDCClearanceToolStripMenuItem3.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem3.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem3_Click);
            // 
            // creditNoteToolStripMenuItem2
            // 
            this.creditNoteToolStripMenuItem2.Name = "creditNoteToolStripMenuItem2";
            this.creditNoteToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.creditNoteToolStripMenuItem2.Text = "Receipts";
            this.creditNoteToolStripMenuItem2.Click += new System.EventHandler(this.creditNoteToolStripMenuItem2_Click);
            // 
            // receiptsToolStripMenuItem1
            // 
            this.receiptsToolStripMenuItem1.Name = "receiptsToolStripMenuItem1";
            this.receiptsToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.receiptsToolStripMenuItem1.Text = "Credit Note";
            this.receiptsToolStripMenuItem1.Click += new System.EventHandler(this.receiptsToolStripMenuItem1_Click);
            // 
            // pOSToolStripMenuItem1
            // 
            this.pOSToolStripMenuItem1.Name = "pOSToolStripMenuItem1";
            this.pOSToolStripMenuItem1.Size = new System.Drawing.Size(210, 22);
            this.pOSToolStripMenuItem1.Text = "POS";
            this.pOSToolStripMenuItem1.Click += new System.EventHandler(this.pOSToolStripMenuItem1_Click);
            // 
            // pendingSalesToolStripMenuItem
            // 
            this.pendingSalesToolStripMenuItem.Name = "pendingSalesToolStripMenuItem";
            this.pendingSalesToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.pendingSalesToolStripMenuItem.Text = "Pending Sales";
            this.pendingSalesToolStripMenuItem.Click += new System.EventHandler(this.pendingSalesToolStripMenuItem_Click);
            // 
            // mastersToolStripMenuItem
            // 
            this.mastersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountToolStripMenuItem,
            this.taxToolStripMenuItem,
            this.currencyToolStripMenuItem1,
            this.voucherTypeToolStripMenuItem,
            this.projectToolStripMenuItem,
            this.categoryToolStripMenuItem,
            this.salesManToolStripMenuItem1,
            this.counterToolStripMenuItem,
            this.budgetToolStripMenuItem1,
            this.areaToolStripMenuItem,
            this.routeToolStripMenuItem});
            this.mastersToolStripMenuItem.Enabled = false;
            this.mastersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("mastersToolStripMenuItem.Image")));
            this.mastersToolStripMenuItem.Name = "mastersToolStripMenuItem";
            this.mastersToolStripMenuItem.Size = new System.Drawing.Size(76, 21);
            this.mastersToolStripMenuItem.Text = "&Masters";
            this.mastersToolStripMenuItem.Visible = false;
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountLedgerToolStripMenuItem1,
            this.accountGroupToolStripMenuItem1,
            this.multipleAcctLedgerToolStripMenuItem,
            this.chartOfAccountToolStripMenuItem1});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.accountToolStripMenuItem.Text = "Account";
            // 
            // accountLedgerToolStripMenuItem1
            // 
            this.accountLedgerToolStripMenuItem1.Name = "accountLedgerToolStripMenuItem1";
            this.accountLedgerToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.accountLedgerToolStripMenuItem1.Text = "Account Ledger";
            this.accountLedgerToolStripMenuItem1.Click += new System.EventHandler(this.accountLedgerToolStripMenuItem1_Click);
            // 
            // accountGroupToolStripMenuItem1
            // 
            this.accountGroupToolStripMenuItem1.Name = "accountGroupToolStripMenuItem1";
            this.accountGroupToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.accountGroupToolStripMenuItem1.Text = "Account Group";
            this.accountGroupToolStripMenuItem1.Click += new System.EventHandler(this.accountGroupToolStripMenuItem1_Click);
            // 
            // multipleAcctLedgerToolStripMenuItem
            // 
            this.multipleAcctLedgerToolStripMenuItem.Name = "multipleAcctLedgerToolStripMenuItem";
            this.multipleAcctLedgerToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.multipleAcctLedgerToolStripMenuItem.Text = "Multiple Acct Ledgers";
            this.multipleAcctLedgerToolStripMenuItem.Click += new System.EventHandler(this.multipleAcctLedgerToolStripMenuItem_Click);
            // 
            // chartOfAccountToolStripMenuItem1
            // 
            this.chartOfAccountToolStripMenuItem1.Name = "chartOfAccountToolStripMenuItem1";
            this.chartOfAccountToolStripMenuItem1.Size = new System.Drawing.Size(189, 22);
            this.chartOfAccountToolStripMenuItem1.Text = "Chart Of Account";
            this.chartOfAccountToolStripMenuItem1.Click += new System.EventHandler(this.chartOfAccountToolStripMenuItem1_Click);
            // 
            // taxToolStripMenuItem
            // 
            this.taxToolStripMenuItem.Name = "taxToolStripMenuItem";
            this.taxToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.taxToolStripMenuItem.Text = "Tax";
            this.taxToolStripMenuItem.Click += new System.EventHandler(this.taxToolStripMenuItem_Click);
            // 
            // currencyToolStripMenuItem1
            // 
            this.currencyToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.currencyListToolStripMenuItem,
            this.exchangeRateToolStripMenuItem1});
            this.currencyToolStripMenuItem1.Name = "currencyToolStripMenuItem1";
            this.currencyToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.currencyToolStripMenuItem1.Text = "Currency";
            // 
            // currencyListToolStripMenuItem
            // 
            this.currencyListToolStripMenuItem.Name = "currencyListToolStripMenuItem";
            this.currencyListToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.currencyListToolStripMenuItem.Text = "Currency List";
            this.currencyListToolStripMenuItem.Click += new System.EventHandler(this.currencyListToolStripMenuItem_Click);
            // 
            // exchangeRateToolStripMenuItem1
            // 
            this.exchangeRateToolStripMenuItem1.Name = "exchangeRateToolStripMenuItem1";
            this.exchangeRateToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.exchangeRateToolStripMenuItem1.Text = "Exchange Rate";
            this.exchangeRateToolStripMenuItem1.Click += new System.EventHandler(this.exchangeRateToolStripMenuItem1_Click);
            // 
            // voucherTypeToolStripMenuItem
            // 
            this.voucherTypeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.voucherTypeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.voucherTypeToolStripMenuItem.Name = "voucherTypeToolStripMenuItem";
            this.voucherTypeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.voucherTypeToolStripMenuItem.Text = "Form Type";
            this.voucherTypeToolStripMenuItem.Click += new System.EventHandler(this.voucherTypeToolStripMenuItem_Click);
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.projectToolStripMenuItem.Text = "Projects";
            this.projectToolStripMenuItem.Click += new System.EventHandler(this.projectToolStripMenuItem_Click);
            // 
            // categoryToolStripMenuItem
            // 
            this.categoryToolStripMenuItem.Name = "categoryToolStripMenuItem";
            this.categoryToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.categoryToolStripMenuItem.Text = "Category";
            this.categoryToolStripMenuItem.Click += new System.EventHandler(this.categoryToolStripMenuItem_Click);
            // 
            // salesManToolStripMenuItem1
            // 
            this.salesManToolStripMenuItem1.Name = "salesManToolStripMenuItem1";
            this.salesManToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.salesManToolStripMenuItem1.Text = "Salesman";
            this.salesManToolStripMenuItem1.Click += new System.EventHandler(this.salesmanToolStripMenuItem_Click);
            // 
            // counterToolStripMenuItem
            // 
            this.counterToolStripMenuItem.Name = "counterToolStripMenuItem";
            this.counterToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.counterToolStripMenuItem.Text = "Counter";
            this.counterToolStripMenuItem.Click += new System.EventHandler(this.counterToolStripMenuItem_Click);
            // 
            // budgetToolStripMenuItem1
            // 
            this.budgetToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.budgetSettingsToolStripMenuItem,
            this.budgetVarianceToolStripMenuItem1});
            this.budgetToolStripMenuItem1.Name = "budgetToolStripMenuItem1";
            this.budgetToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.budgetToolStripMenuItem1.Text = "Budget";
            // 
            // budgetSettingsToolStripMenuItem
            // 
            this.budgetSettingsToolStripMenuItem.Name = "budgetSettingsToolStripMenuItem";
            this.budgetSettingsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.budgetSettingsToolStripMenuItem.Text = "Budget Settings";
            this.budgetSettingsToolStripMenuItem.Click += new System.EventHandler(this.budgetSettingsToolStripMenuItem_Click);
            // 
            // budgetVarianceToolStripMenuItem1
            // 
            this.budgetVarianceToolStripMenuItem1.Name = "budgetVarianceToolStripMenuItem1";
            this.budgetVarianceToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.budgetVarianceToolStripMenuItem1.Text = "Budget Variance";
            this.budgetVarianceToolStripMenuItem1.Click += new System.EventHandler(this.budgetVarianceToolStripMenuItem1_Click);
            // 
            // areaToolStripMenuItem
            // 
            this.areaToolStripMenuItem.Name = "areaToolStripMenuItem";
            this.areaToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.areaToolStripMenuItem.Text = "State";
            this.areaToolStripMenuItem.Click += new System.EventHandler(this.areaToolStripMenuItem_Click);
            // 
            // routeToolStripMenuItem
            // 
            this.routeToolStripMenuItem.Name = "routeToolStripMenuItem";
            this.routeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.routeToolStripMenuItem.Text = "City";
            this.routeToolStripMenuItem.Click += new System.EventHandler(this.routeToolStripMenuItem_Click);
            // 
            // supplierStripMenuItem1
            // 
            this.supplierStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supplierCenterToolStripMenuItem,
            this.purchaseOrderToolStripMenuItem1,
            this.materialReceiptToolStripMenuItem,
            this.rejectionOutToolStripMenuItem1,
            this.purchaseInvoiceToolStripMenuItem1,
            this.purchaseReturnToolStripMenuItem1,
            this.pDCPayableToolStripMenuItem2,
            this.pDCClearanceToolStripMenuItem5,
            this.paymentsToolStripMenuItem1,
            this.debitNoteToolStripMenuItem2,
            this.registersToolStripMenuItem1});
            this.supplierStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.supplierStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("supplierStripMenuItem1.Image")));
            this.supplierStripMenuItem1.Name = "supplierStripMenuItem1";
            this.supplierStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.supplierStripMenuItem1.Size = new System.Drawing.Size(90, 21);
            this.supplierStripMenuItem1.Text = "&Suppliers";
            // 
            // supplierCenterToolStripMenuItem
            // 
            this.supplierCenterToolStripMenuItem.Name = "supplierCenterToolStripMenuItem";
            this.supplierCenterToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.supplierCenterToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.supplierCenterToolStripMenuItem.Text = "Supplier Center";
            this.supplierCenterToolStripMenuItem.Click += new System.EventHandler(this.supplierCenterToolStripMenuItem_Click);
            // 
            // purchaseOrderToolStripMenuItem1
            // 
            this.purchaseOrderToolStripMenuItem1.Name = "purchaseOrderToolStripMenuItem1";
            this.purchaseOrderToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F9)));
            this.purchaseOrderToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.purchaseOrderToolStripMenuItem1.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem1.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem1_Click);
            // 
            // materialReceiptToolStripMenuItem
            // 
            this.materialReceiptToolStripMenuItem.Name = "materialReceiptToolStripMenuItem";
            this.materialReceiptToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F9)));
            this.materialReceiptToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.materialReceiptToolStripMenuItem.Text = "Material Receipt";
            this.materialReceiptToolStripMenuItem.Click += new System.EventHandler(this.materialReceiptToolStripMenuItem_Click);
            // 
            // rejectionOutToolStripMenuItem1
            // 
            this.rejectionOutToolStripMenuItem1.Name = "rejectionOutToolStripMenuItem1";
            this.rejectionOutToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F6)));
            this.rejectionOutToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.rejectionOutToolStripMenuItem1.Text = "Rejection Out";
            this.rejectionOutToolStripMenuItem1.Click += new System.EventHandler(this.rejectionOutToolStripMenuItem1_Click);
            // 
            // purchaseInvoiceToolStripMenuItem1
            // 
            this.purchaseInvoiceToolStripMenuItem1.Name = "purchaseInvoiceToolStripMenuItem1";
            this.purchaseInvoiceToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.purchaseInvoiceToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.purchaseInvoiceToolStripMenuItem1.Text = "Purchase Invoice";
            this.purchaseInvoiceToolStripMenuItem1.Click += new System.EventHandler(this.purchaseInvoiceToolStripMenuItem1_Click);
            // 
            // purchaseReturnToolStripMenuItem1
            // 
            this.purchaseReturnToolStripMenuItem1.Name = "purchaseReturnToolStripMenuItem1";
            this.purchaseReturnToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.purchaseReturnToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.purchaseReturnToolStripMenuItem1.Text = "Purchase Return";
            this.purchaseReturnToolStripMenuItem1.Click += new System.EventHandler(this.purchaseReturnToolStripMenuItem1_Click);
            // 
            // pDCPayableToolStripMenuItem2
            // 
            this.pDCPayableToolStripMenuItem2.Name = "pDCPayableToolStripMenuItem2";
            this.pDCPayableToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.pDCPayableToolStripMenuItem2.Size = new System.Drawing.Size(222, 22);
            this.pDCPayableToolStripMenuItem2.Text = "PDC Payable";
            this.pDCPayableToolStripMenuItem2.Click += new System.EventHandler(this.pDCPayableToolStripMenuItem2_Click);
            // 
            // pDCClearanceToolStripMenuItem5
            // 
            this.pDCClearanceToolStripMenuItem5.Name = "pDCClearanceToolStripMenuItem5";
            this.pDCClearanceToolStripMenuItem5.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.pDCClearanceToolStripMenuItem5.Size = new System.Drawing.Size(222, 22);
            this.pDCClearanceToolStripMenuItem5.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem5.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem5_Click);
            // 
            // paymentsToolStripMenuItem1
            // 
            this.paymentsToolStripMenuItem1.Name = "paymentsToolStripMenuItem1";
            this.paymentsToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.paymentsToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.paymentsToolStripMenuItem1.Text = "Payments";
            this.paymentsToolStripMenuItem1.Click += new System.EventHandler(this.paymentsToolStripMenuItem1_Click);
            // 
            // debitNoteToolStripMenuItem2
            // 
            this.debitNoteToolStripMenuItem2.Name = "debitNoteToolStripMenuItem2";
            this.debitNoteToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F9)));
            this.debitNoteToolStripMenuItem2.Size = new System.Drawing.Size(222, 22);
            this.debitNoteToolStripMenuItem2.Text = "Debit Note";
            this.debitNoteToolStripMenuItem2.Click += new System.EventHandler(this.debitNoteToolStripMenuItem2_Click);
            // 
            // registersToolStripMenuItem1
            // 
            this.registersToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderToolStripMenuItem2,
            this.materialReceiptToolStripMenuItem1,
            this.rejectionOutToolStripMenuItem2,
            this.purchaseInvoiceToolStripMenuItem2,
            this.purchaseReturnToolStripMenuItem2,
            this.pDCPayableToolStripMenuItem1,
            this.pDCClearanceToolStripMenuItem4,
            this.debitNoteToolStripMenuItem1,
            this.paymentsToolStripMenuItem});
            this.registersToolStripMenuItem1.Name = "registersToolStripMenuItem1";
            this.registersToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.registersToolStripMenuItem1.Text = "&Registers";
            // 
            // purchaseOrderToolStripMenuItem2
            // 
            this.purchaseOrderToolStripMenuItem2.Name = "purchaseOrderToolStripMenuItem2";
            this.purchaseOrderToolStripMenuItem2.Size = new System.Drawing.Size(172, 22);
            this.purchaseOrderToolStripMenuItem2.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem2.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem2_Click);
            // 
            // materialReceiptToolStripMenuItem1
            // 
            this.materialReceiptToolStripMenuItem1.Name = "materialReceiptToolStripMenuItem1";
            this.materialReceiptToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.materialReceiptToolStripMenuItem1.Text = "Material Receipt";
            this.materialReceiptToolStripMenuItem1.Click += new System.EventHandler(this.materialReceiptToolStripMenuItem1_Click);
            // 
            // rejectionOutToolStripMenuItem2
            // 
            this.rejectionOutToolStripMenuItem2.Name = "rejectionOutToolStripMenuItem2";
            this.rejectionOutToolStripMenuItem2.Size = new System.Drawing.Size(172, 22);
            this.rejectionOutToolStripMenuItem2.Text = "Rejection Out";
            this.rejectionOutToolStripMenuItem2.Click += new System.EventHandler(this.rejectionOutToolStripMenuItem2_Click);
            // 
            // purchaseInvoiceToolStripMenuItem2
            // 
            this.purchaseInvoiceToolStripMenuItem2.Name = "purchaseInvoiceToolStripMenuItem2";
            this.purchaseInvoiceToolStripMenuItem2.Size = new System.Drawing.Size(172, 22);
            this.purchaseInvoiceToolStripMenuItem2.Text = "Purchase Invoice";
            this.purchaseInvoiceToolStripMenuItem2.Click += new System.EventHandler(this.purchaseInvoiceToolStripMenuItem2_Click);
            // 
            // purchaseReturnToolStripMenuItem2
            // 
            this.purchaseReturnToolStripMenuItem2.Name = "purchaseReturnToolStripMenuItem2";
            this.purchaseReturnToolStripMenuItem2.Size = new System.Drawing.Size(172, 22);
            this.purchaseReturnToolStripMenuItem2.Text = "Purchase Return";
            this.purchaseReturnToolStripMenuItem2.Click += new System.EventHandler(this.purchaseReturnToolStripMenuItem2_Click);
            // 
            // pDCPayableToolStripMenuItem1
            // 
            this.pDCPayableToolStripMenuItem1.Name = "pDCPayableToolStripMenuItem1";
            this.pDCPayableToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.pDCPayableToolStripMenuItem1.Text = "PDC Payable";
            this.pDCPayableToolStripMenuItem1.Click += new System.EventHandler(this.pDCPayableToolStripMenuItem1_Click);
            // 
            // pDCClearanceToolStripMenuItem4
            // 
            this.pDCClearanceToolStripMenuItem4.Name = "pDCClearanceToolStripMenuItem4";
            this.pDCClearanceToolStripMenuItem4.Size = new System.Drawing.Size(172, 22);
            this.pDCClearanceToolStripMenuItem4.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem4.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem4_Click);
            // 
            // debitNoteToolStripMenuItem1
            // 
            this.debitNoteToolStripMenuItem1.Name = "debitNoteToolStripMenuItem1";
            this.debitNoteToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.debitNoteToolStripMenuItem1.Text = "Debit Note";
            this.debitNoteToolStripMenuItem1.Click += new System.EventHandler(this.debitNoteToolStripMenuItem1_Click);
            // 
            // paymentsToolStripMenuItem
            // 
            this.paymentsToolStripMenuItem.Name = "paymentsToolStripMenuItem";
            this.paymentsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.paymentsToolStripMenuItem.Text = "Payments";
            this.paymentsToolStripMenuItem.Click += new System.EventHandler(this.paymentsToolStripMenuItem_Click);
            // 
            // inventoryStripMenuItem
            // 
            this.inventoryStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemGroupToolStripMenuItem,
            this.itemCreationToolStripMenuItem,
            this.multipleItemCreationToolStripMenuItem,
            this.itemComponentsToolStripMenuItem,
            this.priceListToolStripMenuItem1,
            this.physicalStockToolStripMenuItem1,
            this.stockJournalToolStripMenuItem1,
            this.registerToolStripMenuItem1});
            this.inventoryStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.inventoryStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("inventoryStripMenuItem.Image")));
            this.inventoryStripMenuItem.Name = "inventoryStripMenuItem";
            this.inventoryStripMenuItem.Size = new System.Drawing.Size(89, 21);
            this.inventoryStripMenuItem.Text = "&Inventory";
            // 
            // itemGroupToolStripMenuItem
            // 
            this.itemGroupToolStripMenuItem.Name = "itemGroupToolStripMenuItem";
            this.itemGroupToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.itemGroupToolStripMenuItem.Text = "Item Group";
            this.itemGroupToolStripMenuItem.Click += new System.EventHandler(this.itemGroupToolStripMenuItem_Click);
            // 
            // itemCreationToolStripMenuItem
            // 
            this.itemCreationToolStripMenuItem.Name = "itemCreationToolStripMenuItem";
            this.itemCreationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.itemCreationToolStripMenuItem.Text = "Item Creation";
            this.itemCreationToolStripMenuItem.Click += new System.EventHandler(this.itemCreationToolStripMenuItem_Click);
            // 
            // multipleItemCreationToolStripMenuItem
            // 
            this.multipleItemCreationToolStripMenuItem.Enabled = false;
            this.multipleItemCreationToolStripMenuItem.Name = "multipleItemCreationToolStripMenuItem";
            this.multipleItemCreationToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.multipleItemCreationToolStripMenuItem.Text = "Multiple Item Creation";
            this.multipleItemCreationToolStripMenuItem.Visible = false;
            this.multipleItemCreationToolStripMenuItem.Click += new System.EventHandler(this.multipleItemCreationToolStripMenuItem_Click);
            // 
            // itemComponentsToolStripMenuItem
            // 
            this.itemComponentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.batchToolStripMenuItem1,
            this.brandToolStripMenuItem1,
            this.modelNoToolStripMenuItem,
            this.sizeToolStripMenuItem1,
            this.unitToolStripMenuItem1,
            this.storeToolStripMenuItem,
            this.rackToolStripMenuItem1});
            this.itemComponentsToolStripMenuItem.Name = "itemComponentsToolStripMenuItem";
            this.itemComponentsToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.itemComponentsToolStripMenuItem.Text = "Item Components";
            // 
            // batchToolStripMenuItem1
            // 
            this.batchToolStripMenuItem1.Name = "batchToolStripMenuItem1";
            this.batchToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.batchToolStripMenuItem1.Text = "Batch";
            this.batchToolStripMenuItem1.Click += new System.EventHandler(this.batchToolStripMenuItem1_Click);
            // 
            // brandToolStripMenuItem1
            // 
            this.brandToolStripMenuItem1.Name = "brandToolStripMenuItem1";
            this.brandToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.brandToolStripMenuItem1.Text = "Brand";
            this.brandToolStripMenuItem1.Click += new System.EventHandler(this.brandToolStripMenuItem1_Click);
            // 
            // modelNoToolStripMenuItem
            // 
            this.modelNoToolStripMenuItem.Name = "modelNoToolStripMenuItem";
            this.modelNoToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.modelNoToolStripMenuItem.Text = "Model No";
            this.modelNoToolStripMenuItem.Click += new System.EventHandler(this.modelNoToolStripMenuItem_Click);
            // 
            // sizeToolStripMenuItem1
            // 
            this.sizeToolStripMenuItem1.Name = "sizeToolStripMenuItem1";
            this.sizeToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.sizeToolStripMenuItem1.Text = "Size";
            this.sizeToolStripMenuItem1.Click += new System.EventHandler(this.sizeToolStripMenuItem1_Click);
            // 
            // unitToolStripMenuItem1
            // 
            this.unitToolStripMenuItem1.Name = "unitToolStripMenuItem1";
            this.unitToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.unitToolStripMenuItem1.Text = "Unit";
            this.unitToolStripMenuItem1.Click += new System.EventHandler(this.unitToolStripMenuItem1_Click);
            // 
            // storeToolStripMenuItem
            // 
            this.storeToolStripMenuItem.Name = "storeToolStripMenuItem";
            this.storeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.storeToolStripMenuItem.Text = "Store";
            this.storeToolStripMenuItem.Click += new System.EventHandler(this.storeToolStripMenuItem_Click);
            // 
            // rackToolStripMenuItem1
            // 
            this.rackToolStripMenuItem1.Name = "rackToolStripMenuItem1";
            this.rackToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.rackToolStripMenuItem1.Text = "Rack";
            this.rackToolStripMenuItem1.Click += new System.EventHandler(this.rackToolStripMenuItem1_Click);
            // 
            // priceListToolStripMenuItem1
            // 
            this.priceListToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pricingLevelToolStripMenuItem1,
            this.priceListToolStripMenuItem2,
            this.standardRateToolStripMenuItem1});
            this.priceListToolStripMenuItem1.Name = "priceListToolStripMenuItem1";
            this.priceListToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
            this.priceListToolStripMenuItem1.Text = "Price List";
            // 
            // pricingLevelToolStripMenuItem1
            // 
            this.pricingLevelToolStripMenuItem1.Name = "pricingLevelToolStripMenuItem1";
            this.pricingLevelToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.pricingLevelToolStripMenuItem1.Text = "Pricing Level";
            this.pricingLevelToolStripMenuItem1.Click += new System.EventHandler(this.pricingLevelToolStripMenuItem1_Click);
            // 
            // priceListToolStripMenuItem2
            // 
            this.priceListToolStripMenuItem2.Name = "priceListToolStripMenuItem2";
            this.priceListToolStripMenuItem2.Size = new System.Drawing.Size(159, 22);
            this.priceListToolStripMenuItem2.Text = "Price List";
            this.priceListToolStripMenuItem2.Click += new System.EventHandler(this.priceListToolStripMenuItem2_Click);
            // 
            // standardRateToolStripMenuItem1
            // 
            this.standardRateToolStripMenuItem1.Name = "standardRateToolStripMenuItem1";
            this.standardRateToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.standardRateToolStripMenuItem1.Text = "Standard Rate";
            this.standardRateToolStripMenuItem1.Click += new System.EventHandler(this.standardRateToolStripMenuItem1_Click);
            // 
            // physicalStockToolStripMenuItem1
            // 
            this.physicalStockToolStripMenuItem1.Name = "physicalStockToolStripMenuItem1";
            this.physicalStockToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
            this.physicalStockToolStripMenuItem1.Text = "Physical Stock";
            this.physicalStockToolStripMenuItem1.Click += new System.EventHandler(this.physicalStockToolStripMenuItem1_Click);
            // 
            // stockJournalToolStripMenuItem1
            // 
            this.stockJournalToolStripMenuItem1.Name = "stockJournalToolStripMenuItem1";
            this.stockJournalToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
            this.stockJournalToolStripMenuItem1.Text = "Stock Journal";
            this.stockJournalToolStripMenuItem1.Click += new System.EventHandler(this.stockJournalToolStripMenuItem1_Click);
            // 
            // registerToolStripMenuItem1
            // 
            this.registerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemRegisterToolStripMenuItem,
            this.physicalStockToolStripMenuItem2,
            this.stockJournalToolStripMenuItem2});
            this.registerToolStripMenuItem1.Name = "registerToolStripMenuItem1";
            this.registerToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
            this.registerToolStripMenuItem1.Text = "Register";
            // 
            // itemRegisterToolStripMenuItem
            // 
            this.itemRegisterToolStripMenuItem.Name = "itemRegisterToolStripMenuItem";
            this.itemRegisterToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.itemRegisterToolStripMenuItem.Text = "Item Register";
            this.itemRegisterToolStripMenuItem.Click += new System.EventHandler(this.itemRegisterToolStripMenuItem_Click);
            // 
            // physicalStockToolStripMenuItem2
            // 
            this.physicalStockToolStripMenuItem2.Name = "physicalStockToolStripMenuItem2";
            this.physicalStockToolStripMenuItem2.Size = new System.Drawing.Size(156, 22);
            this.physicalStockToolStripMenuItem2.Text = "Physical Stock";
            this.physicalStockToolStripMenuItem2.Click += new System.EventHandler(this.physicalStockToolStripMenuItem2_Click);
            // 
            // stockJournalToolStripMenuItem2
            // 
            this.stockJournalToolStripMenuItem2.Name = "stockJournalToolStripMenuItem2";
            this.stockJournalToolStripMenuItem2.Size = new System.Drawing.Size(156, 22);
            this.stockJournalToolStripMenuItem2.Text = "Stock Journal";
            this.stockJournalToolStripMenuItem2.Click += new System.EventHandler(this.stockJournalToolStripMenuItem2_Click);
            // 
            // transactionToolStripMenuItem
            // 
            this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contraVoucherToolStripMenuItem,
            this.bankReconciliationToolStripMenuItem,
            this.expensesToolStripMenuItem,
            this.journalVoucherToolStripMenuItem,
            this.registersToolStripMenuItem2,
            this.pOSToolStripMenuItem,
            this.billAllocationToolStripMenuItem});
            this.transactionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.transactionToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("transactionToolStripMenuItem.Image")));
            this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
            this.transactionToolStripMenuItem.Size = new System.Drawing.Size(81, 21);
            this.transactionToolStripMenuItem.Text = "&Banking";
            this.transactionToolStripMenuItem.Click += new System.EventHandler(this.transactionToolStripMenuItem_Click);
            // 
            // contraVoucherToolStripMenuItem
            // 
            this.contraVoucherToolStripMenuItem.Name = "contraVoucherToolStripMenuItem";
            this.contraVoucherToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.contraVoucherToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.contraVoucherToolStripMenuItem.Text = "Bank Transfers";
            this.contraVoucherToolStripMenuItem.Click += new System.EventHandler(this.contraVoucherToolStripMenuItem_Click);
            // 
            // bankReconciliationToolStripMenuItem
            // 
            this.bankReconciliationToolStripMenuItem.Name = "bankReconciliationToolStripMenuItem";
            this.bankReconciliationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.bankReconciliationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.bankReconciliationToolStripMenuItem.Text = "Bank Reconciliation";
            this.bankReconciliationToolStripMenuItem.Click += new System.EventHandler(this.bankReconciliationToolStripMenuItem_Click);
            // 
            // expensesToolStripMenuItem
            // 
            this.expensesToolStripMenuItem.Name = "expensesToolStripMenuItem";
            this.expensesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.expensesToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.expensesToolStripMenuItem.Text = "Pay Expenses";
            this.expensesToolStripMenuItem.Click += new System.EventHandler(this.expensesToolStripMenuItem_Click);
            // 
            // journalVoucherToolStripMenuItem
            // 
            this.journalVoucherToolStripMenuItem.Name = "journalVoucherToolStripMenuItem";
            this.journalVoucherToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.journalVoucherToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.journalVoucherToolStripMenuItem.Text = "General Journals";
            this.journalVoucherToolStripMenuItem.Visible = false;
            this.journalVoucherToolStripMenuItem.Click += new System.EventHandler(this.journalVoucherToolStripMenuItem_Click);
            // 
            // registersToolStripMenuItem2
            // 
            this.registersToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bankTransfersToolStripMenuItem,
            this.bankRegisterToolStripMenuItem,
            this.cashRegisterToolStripMenuItem,
            this.otherRegisterToolStripMenuItem,
            this.generalJournalsToolStripMenuItem});
            this.registersToolStripMenuItem2.Name = "registersToolStripMenuItem2";
            this.registersToolStripMenuItem2.Size = new System.Drawing.Size(226, 22);
            this.registersToolStripMenuItem2.Text = "Registers";
            // 
            // bankTransfersToolStripMenuItem
            // 
            this.bankTransfersToolStripMenuItem.Name = "bankTransfersToolStripMenuItem";
            this.bankTransfersToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.bankTransfersToolStripMenuItem.Text = "Bank Transfers";
            this.bankTransfersToolStripMenuItem.Click += new System.EventHandler(this.bankTransfersToolStripMenuItem_Click);
            // 
            // bankRegisterToolStripMenuItem
            // 
            this.bankRegisterToolStripMenuItem.Name = "bankRegisterToolStripMenuItem";
            this.bankRegisterToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.bankRegisterToolStripMenuItem.Text = "Bank Register";
            this.bankRegisterToolStripMenuItem.Click += new System.EventHandler(this.bankRegisterToolStripMenuItem_Click);
            // 
            // cashRegisterToolStripMenuItem
            // 
            this.cashRegisterToolStripMenuItem.Name = "cashRegisterToolStripMenuItem";
            this.cashRegisterToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.cashRegisterToolStripMenuItem.Text = "Cash Register";
            this.cashRegisterToolStripMenuItem.Click += new System.EventHandler(this.cashRegisterToolStripMenuItem_Click);
            // 
            // otherRegisterToolStripMenuItem
            // 
            this.otherRegisterToolStripMenuItem.Name = "otherRegisterToolStripMenuItem";
            this.otherRegisterToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.otherRegisterToolStripMenuItem.Text = "Other Register";
            this.otherRegisterToolStripMenuItem.Click += new System.EventHandler(this.otherRegisterToolStripMenuItem_Click);
            // 
            // generalJournalsToolStripMenuItem
            // 
            this.generalJournalsToolStripMenuItem.Enabled = false;
            this.generalJournalsToolStripMenuItem.Name = "generalJournalsToolStripMenuItem";
            this.generalJournalsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.generalJournalsToolStripMenuItem.Text = "General Journals";
            this.generalJournalsToolStripMenuItem.Visible = false;
            this.generalJournalsToolStripMenuItem.Click += new System.EventHandler(this.generalJournalsToolStripMenuItem_Click);
            // 
            // pOSToolStripMenuItem
            // 
            this.pOSToolStripMenuItem.Enabled = false;
            this.pOSToolStripMenuItem.Name = "pOSToolStripMenuItem";
            this.pOSToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.pOSToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.pOSToolStripMenuItem.Text = "POS";
            this.pOSToolStripMenuItem.Visible = false;
            this.pOSToolStripMenuItem.Click += new System.EventHandler(this.pOSToolStripMenuItem_Click);
            // 
            // billAllocationToolStripMenuItem
            // 
            this.billAllocationToolStripMenuItem.Enabled = false;
            this.billAllocationToolStripMenuItem.Name = "billAllocationToolStripMenuItem";
            this.billAllocationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.billAllocationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.billAllocationToolStripMenuItem.Text = "Bill Allocation";
            this.billAllocationToolStripMenuItem.Visible = false;
            this.billAllocationToolStripMenuItem.Click += new System.EventHandler(this.billAllocationToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.financialStatementsToolStripMenuItem,
            this.otherReportsToolStripMenuItem,
            this.toolStripMenuItem18,
            this.payrollToolStripMenuItem1,
            this.dayBookToolStripMenuItem1,
            this.cashBToolStripMenuItem,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.PartyAddressBooktoolStripMenuItem,
            this.StockReporttoolStripMenuItem,
            this.ShortExpiryReporttoolStripMenuItem,
            this.ProductStaticstoolStripMenuItem,
            this.PriceListReporttoolStripMenuItem,
            this.TaxReporttoolStripMenuItem,
            this.VatReporttoolStripMenuItem,
            this.ChequeReporttoolStripMenuItem,
            this.freeSaleReportToolStripMenuItem,
            this.productVsBatchReportToolStripMenuItem,
            this.stockReportDetailsToolStripMenuItem,
            this.stockVarianceReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.reportsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reportsToolStripMenuItem.Image")));
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(82, 21);
            this.reportsToolStripMenuItem.Text = "&Reports";
            // 
            // financialStatementsToolStripMenuItem
            // 
            this.financialStatementsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trialBalanceToolStripMenuItem1,
            this.profitAndLossToolStripMenuItem1,
            this.profitAndLossByProjectAndCategoryToolStripMenuItem,
            this.balanceSheetToolStripMenuItem1,
            this.cashFlowToolStripMenuItem1,
            this.fundFlowToolStripMenuItem1,
            this.oldProfitAndLossToolStripMenuItem,
            this.oldBalanceSheetToolStripMenuItem,
            this.pLByCostOfSalesToolStripMenuItem});
            this.financialStatementsToolStripMenuItem.Enabled = false;
            this.financialStatementsToolStripMenuItem.Name = "financialStatementsToolStripMenuItem";
            this.financialStatementsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.financialStatementsToolStripMenuItem.Text = "Financial Statements";
            // 
            // trialBalanceToolStripMenuItem1
            // 
            this.trialBalanceToolStripMenuItem1.Name = "trialBalanceToolStripMenuItem1";
            this.trialBalanceToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.trialBalanceToolStripMenuItem1.Text = "Trial Balance";
            this.trialBalanceToolStripMenuItem1.Click += new System.EventHandler(this.trialBalanceToolStripMenuItem1_Click);
            // 
            // profitAndLossToolStripMenuItem1
            // 
            this.profitAndLossToolStripMenuItem1.Name = "profitAndLossToolStripMenuItem1";
            this.profitAndLossToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.profitAndLossToolStripMenuItem1.Text = "Profit And Loss (Conventional)";
            this.profitAndLossToolStripMenuItem1.Click += new System.EventHandler(this.profitAndLossToolStripMenuItem1_Click);
            // 
            // profitAndLossByProjectAndCategoryToolStripMenuItem
            // 
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Enabled = false;
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Name = "profitAndLossByProjectAndCategoryToolStripMenuItem";
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Text = "P/L by Project and Category";
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Visible = false;
            this.profitAndLossByProjectAndCategoryToolStripMenuItem.Click += new System.EventHandler(this.profitAndLossByProjectAndCategoryToolStripMenuItem_Click);
            // 
            // balanceSheetToolStripMenuItem1
            // 
            this.balanceSheetToolStripMenuItem1.Name = "balanceSheetToolStripMenuItem1";
            this.balanceSheetToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.balanceSheetToolStripMenuItem1.Text = "Balance Sheet";
            this.balanceSheetToolStripMenuItem1.Click += new System.EventHandler(this.balanceSheetToolStripMenuItem1_Click);
            // 
            // cashFlowToolStripMenuItem1
            // 
            this.cashFlowToolStripMenuItem1.Enabled = false;
            this.cashFlowToolStripMenuItem1.Name = "cashFlowToolStripMenuItem1";
            this.cashFlowToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.cashFlowToolStripMenuItem1.Text = "Cash Flow";
            this.cashFlowToolStripMenuItem1.Visible = false;
            this.cashFlowToolStripMenuItem1.Click += new System.EventHandler(this.cashFlowToolStripMenuItem1_Click);
            // 
            // fundFlowToolStripMenuItem1
            // 
            this.fundFlowToolStripMenuItem1.Enabled = false;
            this.fundFlowToolStripMenuItem1.Name = "fundFlowToolStripMenuItem1";
            this.fundFlowToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.fundFlowToolStripMenuItem1.Text = "Fund Flow";
            this.fundFlowToolStripMenuItem1.Visible = false;
            this.fundFlowToolStripMenuItem1.Click += new System.EventHandler(this.fundFlowToolStripMenuItem1_Click);
            // 
            // oldProfitAndLossToolStripMenuItem
            // 
            this.oldProfitAndLossToolStripMenuItem.Enabled = false;
            this.oldProfitAndLossToolStripMenuItem.Name = "oldProfitAndLossToolStripMenuItem";
            this.oldProfitAndLossToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.oldProfitAndLossToolStripMenuItem.Text = "Old Profit and Loss";
            this.oldProfitAndLossToolStripMenuItem.Visible = false;
            this.oldProfitAndLossToolStripMenuItem.Click += new System.EventHandler(this.profitAndLossToolStripMenuItem_Click);
            // 
            // oldBalanceSheetToolStripMenuItem
            // 
            this.oldBalanceSheetToolStripMenuItem.Enabled = false;
            this.oldBalanceSheetToolStripMenuItem.Name = "oldBalanceSheetToolStripMenuItem";
            this.oldBalanceSheetToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.oldBalanceSheetToolStripMenuItem.Text = "Old Balance Sheet";
            this.oldBalanceSheetToolStripMenuItem.Visible = false;
            this.oldBalanceSheetToolStripMenuItem.Click += new System.EventHandler(this.balanceSheetToolStripMenuItem_Click);
            // 
            // pLByCostOfSalesToolStripMenuItem
            // 
            this.pLByCostOfSalesToolStripMenuItem.Name = "pLByCostOfSalesToolStripMenuItem";
            this.pLByCostOfSalesToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.pLByCostOfSalesToolStripMenuItem.Text = "Profit And Loss (Standard)";
            this.pLByCostOfSalesToolStripMenuItem.Click += new System.EventHandler(this.pLByCostOfSalesToolStripMenuItem_Click);
            // 
            // otherReportsToolStripMenuItem
            // 
            this.otherReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalToolStripMenuItem,
            this.suppliersToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.bankToolStripMenuItem1,
            this.accountsToolStripMenuItem,
            this.listToolStripMenuItem,
            this.payrollToolStripMenuItem2,
            this.transactionUnitReportToolStripMenuItem});
            this.otherReportsToolStripMenuItem.Name = "otherReportsToolStripMenuItem";
            this.otherReportsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.otherReportsToolStripMenuItem.Text = "Other Reports";
            // 
            // generalToolStripMenuItem
            // 
            this.generalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.taxToolStripMenuItem1,
            this.vATReturnsToolStripMenuItem,
            this.generalJournalsToolStripMenuItem1,
            this.transactionDetailsByLedgersToolStripMenuItem});
            this.generalToolStripMenuItem.Name = "generalToolStripMenuItem";
            this.generalToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.generalToolStripMenuItem.Text = "General";
            // 
            // taxToolStripMenuItem1
            // 
            this.taxToolStripMenuItem1.Name = "taxToolStripMenuItem1";
            this.taxToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.taxToolStripMenuItem1.Text = "Tax";
            this.taxToolStripMenuItem1.Click += new System.EventHandler(this.taxToolStripMenuItem1_Click);
            // 
            // vATReturnsToolStripMenuItem
            // 
            this.vATReturnsToolStripMenuItem.Name = "vATReturnsToolStripMenuItem";
            this.vATReturnsToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.vATReturnsToolStripMenuItem.Text = "VAT Returns";
            this.vATReturnsToolStripMenuItem.Click += new System.EventHandler(this.vATReturnsToolStripMenuItem_Click);
            // 
            // generalJournalsToolStripMenuItem1
            // 
            this.generalJournalsToolStripMenuItem1.Name = "generalJournalsToolStripMenuItem1";
            this.generalJournalsToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.generalJournalsToolStripMenuItem1.Text = "General Journals";
            this.generalJournalsToolStripMenuItem1.Click += new System.EventHandler(this.generalJournalsToolStripMenuItem1_Click);
            // 
            // transactionDetailsByLedgersToolStripMenuItem
            // 
            this.transactionDetailsByLedgersToolStripMenuItem.Name = "transactionDetailsByLedgersToolStripMenuItem";
            this.transactionDetailsByLedgersToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.transactionDetailsByLedgersToolStripMenuItem.Text = "Transaction Details By ledgers";
            this.transactionDetailsByLedgersToolStripMenuItem.Click += new System.EventHandler(this.transactionDetailsByLedgersToolStripMenuItem_Click);
            // 
            // suppliersToolStripMenuItem
            // 
            this.suppliersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aPAgeingSummaryToolStripMenuItem,
            this.supplierBalanceSummaryToolStripMenuItem,
            this.purchaseSummaryToolStripMenuItem,
            this.transactionsToolStripMenuItem});
            this.suppliersToolStripMenuItem.Name = "suppliersToolStripMenuItem";
            this.suppliersToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.suppliersToolStripMenuItem.Text = "Suppliers";
            // 
            // aPAgeingSummaryToolStripMenuItem
            // 
            this.aPAgeingSummaryToolStripMenuItem.Name = "aPAgeingSummaryToolStripMenuItem";
            this.aPAgeingSummaryToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.aPAgeingSummaryToolStripMenuItem.Text = "A/P Ageing Summary";
            this.aPAgeingSummaryToolStripMenuItem.Click += new System.EventHandler(this.aPAgeingSummaryToolStripMenuItem_Click);
            // 
            // supplierBalanceSummaryToolStripMenuItem
            // 
            this.supplierBalanceSummaryToolStripMenuItem.Name = "supplierBalanceSummaryToolStripMenuItem";
            this.supplierBalanceSummaryToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.supplierBalanceSummaryToolStripMenuItem.Text = "Supplier Balance Summary";
            this.supplierBalanceSummaryToolStripMenuItem.Click += new System.EventHandler(this.supplierBalanceSummaryToolStripMenuItem_Click_1);
            // 
            // purchaseSummaryToolStripMenuItem
            // 
            this.purchaseSummaryToolStripMenuItem.Name = "purchaseSummaryToolStripMenuItem";
            this.purchaseSummaryToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.purchaseSummaryToolStripMenuItem.Text = "Purchase Summary";
            this.purchaseSummaryToolStripMenuItem.Click += new System.EventHandler(this.purchaseSummaryToolStripMenuItem_Click);
            // 
            // transactionsToolStripMenuItem
            // 
            this.transactionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderToolStripMenuItem,
            this.materialReceiptToolStripMenuItem2,
            this.rejectionOutToolStripMenuItem,
            this.purchaseReturnsToolStripMenuItem,
            this.paymentsToolStripMenuItem2,
            this.pDCPayablesToolStripMenuItem,
            this.pDCClearanceToolStripMenuItem,
            this.debitNoteToolStripMenuItem});
            this.transactionsToolStripMenuItem.Name = "transactionsToolStripMenuItem";
            this.transactionsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.transactionsToolStripMenuItem.Text = "Transactions";
            // 
            // purchaseOrderToolStripMenuItem
            // 
            this.purchaseOrderToolStripMenuItem.Name = "purchaseOrderToolStripMenuItem";
            this.purchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.purchaseOrderToolStripMenuItem.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem_Click_1);
            // 
            // materialReceiptToolStripMenuItem2
            // 
            this.materialReceiptToolStripMenuItem2.Name = "materialReceiptToolStripMenuItem2";
            this.materialReceiptToolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.materialReceiptToolStripMenuItem2.Text = "Material Receipt";
            this.materialReceiptToolStripMenuItem2.Click += new System.EventHandler(this.materialReceiptToolStripMenuItem2_Click);
            // 
            // rejectionOutToolStripMenuItem
            // 
            this.rejectionOutToolStripMenuItem.Name = "rejectionOutToolStripMenuItem";
            this.rejectionOutToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.rejectionOutToolStripMenuItem.Text = "Rejection Out";
            this.rejectionOutToolStripMenuItem.Click += new System.EventHandler(this.rejectionOutToolStripMenuItem_Click_1);
            // 
            // purchaseReturnsToolStripMenuItem
            // 
            this.purchaseReturnsToolStripMenuItem.Name = "purchaseReturnsToolStripMenuItem";
            this.purchaseReturnsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.purchaseReturnsToolStripMenuItem.Text = "Purchase Returns";
            this.purchaseReturnsToolStripMenuItem.Click += new System.EventHandler(this.purchaseReturnsToolStripMenuItem_Click);
            // 
            // paymentsToolStripMenuItem2
            // 
            this.paymentsToolStripMenuItem2.Name = "paymentsToolStripMenuItem2";
            this.paymentsToolStripMenuItem2.Size = new System.Drawing.Size(176, 22);
            this.paymentsToolStripMenuItem2.Text = "Payments";
            this.paymentsToolStripMenuItem2.Click += new System.EventHandler(this.paymentsToolStripMenuItem2_Click);
            // 
            // pDCPayablesToolStripMenuItem
            // 
            this.pDCPayablesToolStripMenuItem.Name = "pDCPayablesToolStripMenuItem";
            this.pDCPayablesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.pDCPayablesToolStripMenuItem.Text = "PDC Payables";
            this.pDCPayablesToolStripMenuItem.Click += new System.EventHandler(this.pDCPayablesToolStripMenuItem_Click);
            // 
            // pDCClearanceToolStripMenuItem
            // 
            this.pDCClearanceToolStripMenuItem.Name = "pDCClearanceToolStripMenuItem";
            this.pDCClearanceToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.pDCClearanceToolStripMenuItem.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem_Click_1);
            // 
            // debitNoteToolStripMenuItem
            // 
            this.debitNoteToolStripMenuItem.Name = "debitNoteToolStripMenuItem";
            this.debitNoteToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.debitNoteToolStripMenuItem.Text = "Debit Note";
            this.debitNoteToolStripMenuItem.Click += new System.EventHandler(this.debitNoteToolStripMenuItem_Click_1);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aRAgeingSummaryToolStripMenuItem,
            this.customerBalanceSummaryToolStripMenuItem,
            this.salesSummaryToolStripMenuItem,
            this.transactionsToolStripMenuItem1,
            this.shiftReportToolStripMenuItem});
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.customersToolStripMenuItem.Text = "Customers";
            // 
            // aRAgeingSummaryToolStripMenuItem
            // 
            this.aRAgeingSummaryToolStripMenuItem.Name = "aRAgeingSummaryToolStripMenuItem";
            this.aRAgeingSummaryToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.aRAgeingSummaryToolStripMenuItem.Text = "A/R Ageing Summary";
            this.aRAgeingSummaryToolStripMenuItem.Click += new System.EventHandler(this.aRAgeingSummaryToolStripMenuItem_Click);
            // 
            // customerBalanceSummaryToolStripMenuItem
            // 
            this.customerBalanceSummaryToolStripMenuItem.Name = "customerBalanceSummaryToolStripMenuItem";
            this.customerBalanceSummaryToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.customerBalanceSummaryToolStripMenuItem.Text = "Customer Balance Summary";
            this.customerBalanceSummaryToolStripMenuItem.Click += new System.EventHandler(this.customerBalanceSummaryToolStripMenuItem_Click_1);
            // 
            // salesSummaryToolStripMenuItem
            // 
            this.salesSummaryToolStripMenuItem.Name = "salesSummaryToolStripMenuItem";
            this.salesSummaryToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.salesSummaryToolStripMenuItem.Text = "Sales Summary";
            this.salesSummaryToolStripMenuItem.Click += new System.EventHandler(this.salesSummaryToolStripMenuItem_Click);
            // 
            // transactionsToolStripMenuItem1
            // 
            this.transactionsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesQuotationToolStripMenuItem2,
            this.salesOrderToolStripMenuItem,
            this.deliveryNoteToolStripMenuItem2,
            this.rejectionInToolStripMenuItem,
            this.salesReturnsToolStripMenuItem1,
            this.receiptsToolStripMenuItem2,
            this.pDCReceivableToolStripMenuItem,
            this.pDCClearanceToolStripMenuItem6,
            this.creditNoteToolStripMenuItem});
            this.transactionsToolStripMenuItem1.Name = "transactionsToolStripMenuItem1";
            this.transactionsToolStripMenuItem1.Size = new System.Drawing.Size(238, 22);
            this.transactionsToolStripMenuItem1.Text = "Transactions";
            // 
            // salesQuotationToolStripMenuItem2
            // 
            this.salesQuotationToolStripMenuItem2.Name = "salesQuotationToolStripMenuItem2";
            this.salesQuotationToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.salesQuotationToolStripMenuItem2.Text = "Sales Quotation";
            this.salesQuotationToolStripMenuItem2.Click += new System.EventHandler(this.salesQuotationToolStripMenuItem2_Click);
            // 
            // salesOrderToolStripMenuItem
            // 
            this.salesOrderToolStripMenuItem.Name = "salesOrderToolStripMenuItem";
            this.salesOrderToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.salesOrderToolStripMenuItem.Text = "Sales Order";
            this.salesOrderToolStripMenuItem.Click += new System.EventHandler(this.salesOrderToolStripMenuItem_Click_1);
            // 
            // deliveryNoteToolStripMenuItem2
            // 
            this.deliveryNoteToolStripMenuItem2.Name = "deliveryNoteToolStripMenuItem2";
            this.deliveryNoteToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.deliveryNoteToolStripMenuItem2.Text = "Delivery Note";
            this.deliveryNoteToolStripMenuItem2.Click += new System.EventHandler(this.deliveryNoteToolStripMenuItem2_Click);
            // 
            // rejectionInToolStripMenuItem
            // 
            this.rejectionInToolStripMenuItem.Name = "rejectionInToolStripMenuItem";
            this.rejectionInToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.rejectionInToolStripMenuItem.Text = "Rejection In";
            this.rejectionInToolStripMenuItem.Click += new System.EventHandler(this.rejectionInToolStripMenuItem_Click_1);
            // 
            // salesReturnsToolStripMenuItem1
            // 
            this.salesReturnsToolStripMenuItem1.Name = "salesReturnsToolStripMenuItem1";
            this.salesReturnsToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.salesReturnsToolStripMenuItem1.Text = "Sales Returns";
            this.salesReturnsToolStripMenuItem1.Click += new System.EventHandler(this.salesReturnsToolStripMenuItem1_Click);
            // 
            // receiptsToolStripMenuItem2
            // 
            this.receiptsToolStripMenuItem2.Name = "receiptsToolStripMenuItem2";
            this.receiptsToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.receiptsToolStripMenuItem2.Text = "Receipts";
            this.receiptsToolStripMenuItem2.Click += new System.EventHandler(this.receiptsToolStripMenuItem2_Click);
            // 
            // pDCReceivableToolStripMenuItem
            // 
            this.pDCReceivableToolStripMenuItem.Name = "pDCReceivableToolStripMenuItem";
            this.pDCReceivableToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.pDCReceivableToolStripMenuItem.Text = "PDC Receivable";
            this.pDCReceivableToolStripMenuItem.Click += new System.EventHandler(this.pDCReceivableToolStripMenuItem_Click_1);
            // 
            // pDCClearanceToolStripMenuItem6
            // 
            this.pDCClearanceToolStripMenuItem6.Name = "pDCClearanceToolStripMenuItem6";
            this.pDCClearanceToolStripMenuItem6.Size = new System.Drawing.Size(168, 22);
            this.pDCClearanceToolStripMenuItem6.Text = "PDC Clearance";
            this.pDCClearanceToolStripMenuItem6.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem6_Click);
            // 
            // creditNoteToolStripMenuItem
            // 
            this.creditNoteToolStripMenuItem.Name = "creditNoteToolStripMenuItem";
            this.creditNoteToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.creditNoteToolStripMenuItem.Text = "Credit Note";
            this.creditNoteToolStripMenuItem.Click += new System.EventHandler(this.creditNoteToolStripMenuItem_Click_1);
            // 
            // shiftReportToolStripMenuItem
            // 
            this.shiftReportToolStripMenuItem.Name = "shiftReportToolStripMenuItem";
            this.shiftReportToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.shiftReportToolStripMenuItem.Text = "Shift Report";
            this.shiftReportToolStripMenuItem.Click += new System.EventHandler(this.shiftReportToolStripMenuItem_Click);
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockReportToolStripMenuItem1,
            this.stockJournalsToolStripMenuItem,
            this.stockVarianceReportToolStripMenuItem1,
            this.inventoryMovementToolStripMenuItem,
            this.physicalStockReportToolStripMenuItem1,
            this.inventoryStatisticsToolStripMenuItem,
            this.stockReportDetailsToolStripMenuItem1,
            this.stockReport2ToolStripMenuItem,
            this.stockDetainsReportNewToolStripMenuItem});
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.inventoryToolStripMenuItem.Text = "Inventory";
            // 
            // stockReportToolStripMenuItem1
            // 
            this.stockReportToolStripMenuItem1.Name = "stockReportToolStripMenuItem1";
            this.stockReportToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockReportToolStripMenuItem1.Text = "Stock Report";
            this.stockReportToolStripMenuItem1.Visible = false;
            this.stockReportToolStripMenuItem1.Click += new System.EventHandler(this.stockReportToolStripMenuItem1_Click);
            // 
            // stockJournalsToolStripMenuItem
            // 
            this.stockJournalsToolStripMenuItem.Name = "stockJournalsToolStripMenuItem";
            this.stockJournalsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockJournalsToolStripMenuItem.Text = "Stock Journals";
            this.stockJournalsToolStripMenuItem.Click += new System.EventHandler(this.stockJournalsToolStripMenuItem_Click);
            // 
            // stockVarianceReportToolStripMenuItem1
            // 
            this.stockVarianceReportToolStripMenuItem1.Name = "stockVarianceReportToolStripMenuItem1";
            this.stockVarianceReportToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockVarianceReportToolStripMenuItem1.Text = "Stock Variance Report";
            this.stockVarianceReportToolStripMenuItem1.Click += new System.EventHandler(this.stockVarianceReportToolStripMenuItem1_Click);
            // 
            // inventoryMovementToolStripMenuItem
            // 
            this.inventoryMovementToolStripMenuItem.Name = "inventoryMovementToolStripMenuItem";
            this.inventoryMovementToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.inventoryMovementToolStripMenuItem.Text = "Inventory Movement";
            this.inventoryMovementToolStripMenuItem.Click += new System.EventHandler(this.inventoryMovementToolStripMenuItem_Click);
            // 
            // physicalStockReportToolStripMenuItem1
            // 
            this.physicalStockReportToolStripMenuItem1.Name = "physicalStockReportToolStripMenuItem1";
            this.physicalStockReportToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.physicalStockReportToolStripMenuItem1.Text = "Physical Stock Report";
            this.physicalStockReportToolStripMenuItem1.Click += new System.EventHandler(this.physicalStockReportToolStripMenuItem1_Click);
            // 
            // inventoryStatisticsToolStripMenuItem
            // 
            this.inventoryStatisticsToolStripMenuItem.Name = "inventoryStatisticsToolStripMenuItem";
            this.inventoryStatisticsToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.inventoryStatisticsToolStripMenuItem.Text = "Inventory Statistics";
            this.inventoryStatisticsToolStripMenuItem.Click += new System.EventHandler(this.inventoryStatisticsToolStripMenuItem_Click);
            // 
            // stockReportDetailsToolStripMenuItem1
            // 
            this.stockReportDetailsToolStripMenuItem1.Name = "stockReportDetailsToolStripMenuItem1";
            this.stockReportDetailsToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.stockReportDetailsToolStripMenuItem1.Text = "Stock Details Report";
            this.stockReportDetailsToolStripMenuItem1.Click += new System.EventHandler(this.stockReportDetailsToolStripMenuItem1_Click);
            // 
            // stockReport2ToolStripMenuItem
            // 
            this.stockReport2ToolStripMenuItem.Name = "stockReport2ToolStripMenuItem";
            this.stockReport2ToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockReport2ToolStripMenuItem.Text = "Stock Summary Report";
            this.stockReport2ToolStripMenuItem.Click += new System.EventHandler(this.stockReport2ToolStripMenuItem_Click);
            // 
            // stockDetainsReportNewToolStripMenuItem
            // 
            this.stockDetainsReportNewToolStripMenuItem.Name = "stockDetainsReportNewToolStripMenuItem";
            this.stockDetainsReportNewToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.stockDetainsReportNewToolStripMenuItem.Text = "Stock Details Report New";
            this.stockDetainsReportNewToolStripMenuItem.Click += new System.EventHandler(this.stockDetailsReportNewToolStripMenuItem_Click);
            // 
            // bankToolStripMenuItem1
            // 
            this.bankToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transfersToolStripMenuItem1,
            this.cashBankBookToolStripMenuItem1,
            this.chequeCashDetailsToolStripMenuItem1,
            this.bankReconciliationToolStripMenuItem2});
            this.bankToolStripMenuItem1.Name = "bankToolStripMenuItem1";
            this.bankToolStripMenuItem1.Size = new System.Drawing.Size(213, 22);
            this.bankToolStripMenuItem1.Text = "Banking";
            // 
            // transfersToolStripMenuItem1
            // 
            this.transfersToolStripMenuItem1.Name = "transfersToolStripMenuItem1";
            this.transfersToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.transfersToolStripMenuItem1.Text = "Transfers";
            this.transfersToolStripMenuItem1.Click += new System.EventHandler(this.transfersToolStripMenuItem_Click);
            // 
            // cashBankBookToolStripMenuItem1
            // 
            this.cashBankBookToolStripMenuItem1.Name = "cashBankBookToolStripMenuItem1";
            this.cashBankBookToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.cashBankBookToolStripMenuItem1.Text = "Cash/Bank Book";
            this.cashBankBookToolStripMenuItem1.Click += new System.EventHandler(this.cashBankBookToolStripMenuItem_Click);
            // 
            // chequeCashDetailsToolStripMenuItem1
            // 
            this.chequeCashDetailsToolStripMenuItem1.Name = "chequeCashDetailsToolStripMenuItem1";
            this.chequeCashDetailsToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.chequeCashDetailsToolStripMenuItem1.Text = "Cheque/Cash Details";
            this.chequeCashDetailsToolStripMenuItem1.Click += new System.EventHandler(this.chequeCashDetailsToolStripMenuItem_Click);
            // 
            // bankReconciliationToolStripMenuItem2
            // 
            this.bankReconciliationToolStripMenuItem2.Name = "bankReconciliationToolStripMenuItem2";
            this.bankReconciliationToolStripMenuItem2.Size = new System.Drawing.Size(196, 22);
            this.bankReconciliationToolStripMenuItem2.Text = "Bank Reconciliation";
            this.bankReconciliationToolStripMenuItem2.Click += new System.EventHandler(this.bankReconciliationToolStripMenuItem1_Click);
            // 
            // accountsToolStripMenuItem
            // 
            this.accountsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountLedgersToolStripMenuItem,
            this.accountGroupToolStripMenuItem});
            this.accountsToolStripMenuItem.Name = "accountsToolStripMenuItem";
            this.accountsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.accountsToolStripMenuItem.Text = "Accounts";
            // 
            // accountLedgersToolStripMenuItem
            // 
            this.accountLedgersToolStripMenuItem.Name = "accountLedgersToolStripMenuItem";
            this.accountLedgersToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.accountLedgersToolStripMenuItem.Text = "Account Ledgers";
            this.accountLedgersToolStripMenuItem.Click += new System.EventHandler(this.accountLedgersToolStripMenuItem_Click);
            // 
            // accountGroupToolStripMenuItem
            // 
            this.accountGroupToolStripMenuItem.Name = "accountGroupToolStripMenuItem";
            this.accountGroupToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.accountGroupToolStripMenuItem.Text = "Account Group";
            this.accountGroupToolStripMenuItem.Click += new System.EventHandler(this.accountGroupToolStripMenuItem_Click_1);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supplierCustomerListToolStripMenuItem,
            this.priceListToolStripMenuItem,
            this.accountListToolStripMenuItem1});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.listToolStripMenuItem.Text = "List";
            // 
            // supplierCustomerListToolStripMenuItem
            // 
            this.supplierCustomerListToolStripMenuItem.Name = "supplierCustomerListToolStripMenuItem";
            this.supplierCustomerListToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.supplierCustomerListToolStripMenuItem.Text = "Supplier/Customer List";
            this.supplierCustomerListToolStripMenuItem.Click += new System.EventHandler(this.supplierCustomerListToolStripMenuItem_Click);
            // 
            // priceListToolStripMenuItem
            // 
            this.priceListToolStripMenuItem.Name = "priceListToolStripMenuItem";
            this.priceListToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.priceListToolStripMenuItem.Text = "Price List";
            this.priceListToolStripMenuItem.Click += new System.EventHandler(this.priceListToolStripMenuItem_Click_1);
            // 
            // accountListToolStripMenuItem1
            // 
            this.accountListToolStripMenuItem1.Name = "accountListToolStripMenuItem1";
            this.accountListToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.accountListToolStripMenuItem1.Text = "Account List";
            this.accountListToolStripMenuItem1.Click += new System.EventHandler(this.accountListToolStripMenuItem1_Click);
            // 
            // payrollToolStripMenuItem2
            // 
            this.payrollToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeToolStripMenuItem1,
            this.dailyAttendanceToolStripMenuItem1,
            this.monthlyAttendanceToolStripMenuItem1,
            this.dailySalaryToolStripMenuItem1,
            this.monthlySalaryToolStripMenuItem1,
            this.payheadToolStripMenuItem2,
            this.salaryPackageToolStripMenuItem1,
            this.advancePaymentToolStripMenuItem2,
            this.bonusDeductionToolStripMenuItem2,
            this.employeeAddressBookToolStripMenuItem1});
            this.payrollToolStripMenuItem2.Name = "payrollToolStripMenuItem2";
            this.payrollToolStripMenuItem2.Size = new System.Drawing.Size(213, 22);
            this.payrollToolStripMenuItem2.Text = "Payroll";
            // 
            // employeeToolStripMenuItem1
            // 
            this.employeeToolStripMenuItem1.Name = "employeeToolStripMenuItem1";
            this.employeeToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.employeeToolStripMenuItem1.Text = "Employee";
            this.employeeToolStripMenuItem1.Click += new System.EventHandler(this.employeeToolStripMenuItem1_Click);
            // 
            // dailyAttendanceToolStripMenuItem1
            // 
            this.dailyAttendanceToolStripMenuItem1.Name = "dailyAttendanceToolStripMenuItem1";
            this.dailyAttendanceToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.dailyAttendanceToolStripMenuItem1.Text = "Daily Attendance";
            this.dailyAttendanceToolStripMenuItem1.Click += new System.EventHandler(this.dailyAttendanceToolStripMenuItem1_Click);
            // 
            // monthlyAttendanceToolStripMenuItem1
            // 
            this.monthlyAttendanceToolStripMenuItem1.Name = "monthlyAttendanceToolStripMenuItem1";
            this.monthlyAttendanceToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.monthlyAttendanceToolStripMenuItem1.Text = "Monthly Attendance";
            this.monthlyAttendanceToolStripMenuItem1.Click += new System.EventHandler(this.monthlyAttendanceToolStripMenuItem1_Click);
            // 
            // dailySalaryToolStripMenuItem1
            // 
            this.dailySalaryToolStripMenuItem1.Name = "dailySalaryToolStripMenuItem1";
            this.dailySalaryToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.dailySalaryToolStripMenuItem1.Text = "Daily Salary";
            this.dailySalaryToolStripMenuItem1.Click += new System.EventHandler(this.dailySalaryToolStripMenuItem1_Click);
            // 
            // monthlySalaryToolStripMenuItem1
            // 
            this.monthlySalaryToolStripMenuItem1.Name = "monthlySalaryToolStripMenuItem1";
            this.monthlySalaryToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.monthlySalaryToolStripMenuItem1.Text = "Monthly Salary";
            this.monthlySalaryToolStripMenuItem1.Click += new System.EventHandler(this.monthlySalaryToolStripMenuItem1_Click);
            // 
            // payheadToolStripMenuItem2
            // 
            this.payheadToolStripMenuItem2.Name = "payheadToolStripMenuItem2";
            this.payheadToolStripMenuItem2.Size = new System.Drawing.Size(192, 22);
            this.payheadToolStripMenuItem2.Text = "Pay Element";
            this.payheadToolStripMenuItem2.Click += new System.EventHandler(this.payheadToolStripMenuItem2_Click);
            // 
            // salaryPackageToolStripMenuItem1
            // 
            this.salaryPackageToolStripMenuItem1.Name = "salaryPackageToolStripMenuItem1";
            this.salaryPackageToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.salaryPackageToolStripMenuItem1.Text = "Salary Package";
            this.salaryPackageToolStripMenuItem1.Click += new System.EventHandler(this.salaryPackageToolStripMenuItem1_Click);
            // 
            // advancePaymentToolStripMenuItem2
            // 
            this.advancePaymentToolStripMenuItem2.Name = "advancePaymentToolStripMenuItem2";
            this.advancePaymentToolStripMenuItem2.Size = new System.Drawing.Size(192, 22);
            this.advancePaymentToolStripMenuItem2.Text = "Advance Payment";
            this.advancePaymentToolStripMenuItem2.Click += new System.EventHandler(this.advancePaymentToolStripMenuItem2_Click);
            // 
            // bonusDeductionToolStripMenuItem2
            // 
            this.bonusDeductionToolStripMenuItem2.Name = "bonusDeductionToolStripMenuItem2";
            this.bonusDeductionToolStripMenuItem2.Size = new System.Drawing.Size(192, 22);
            this.bonusDeductionToolStripMenuItem2.Text = "Bonus Deduction";
            this.bonusDeductionToolStripMenuItem2.Click += new System.EventHandler(this.bonusDeductionToolStripMenuItem2_Click);
            // 
            // employeeAddressBookToolStripMenuItem1
            // 
            this.employeeAddressBookToolStripMenuItem1.Name = "employeeAddressBookToolStripMenuItem1";
            this.employeeAddressBookToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.employeeAddressBookToolStripMenuItem1.Text = "Employee List";
            this.employeeAddressBookToolStripMenuItem1.Click += new System.EventHandler(this.employeeAddressBookToolStripMenuItem1_Click);
            // 
            // transactionUnitReportToolStripMenuItem
            // 
            this.transactionUnitReportToolStripMenuItem.Name = "transactionUnitReportToolStripMenuItem";
            this.transactionUnitReportToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.transactionUnitReportToolStripMenuItem.Text = "Transaction Unit Report";
            this.transactionUnitReportToolStripMenuItem.Click += new System.EventHandler(this.transactionUnitReportToolStripMenuItem_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contraReportToolStripMenuItem,
            this.paymentReportToolStripMenuItem,
            this.receiptReportToolStripMenuItem,
            this.journalReportToolStripMenuItem,
            this.pDCPayableReportToolStripMenuItem,
            this.pDCReceivableReportToolStripMenuItem,
            this.pDCClearanceToolStripMenuItem1,
            this.purchaseOrderReportToolStripMenuItem,
            this.materialReceiptReportToolStripMenuItem,
            this.rejectionOutReportToolStripMenuItem,
            this.purchaseInvoiceReportToolStripMenuItem,
            this.purchaseReturnReportToolStripMenuItem,
            this.salesQuotationReportToolStripMenuItem,
            this.salesOrderReportToolStripMenuItem,
            this.deliveryNoteReportToolStripMenuItem,
            this.rejectionInReportToolStripMenuItem,
            this.salesInvoiceReportToolStripMenuItem,
            this.salesReturnReportToolStripMenuItem,
            this.physicalStockReportToolStripMenuItem,
            this.creditNoteReportToolStripMenuItem,
            this.debitNoteReportToolStripMenuItem,
            this.stockJournalReportToolStripMenuItem});
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem18.Text = "Transactions";
            this.toolStripMenuItem18.Visible = false;
            // 
            // contraReportToolStripMenuItem
            // 
            this.contraReportToolStripMenuItem.Enabled = false;
            this.contraReportToolStripMenuItem.Name = "contraReportToolStripMenuItem";
            this.contraReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.contraReportToolStripMenuItem.Text = "Bank Transfer Report";
            this.contraReportToolStripMenuItem.Click += new System.EventHandler(this.contraReportToolStripMenuItem_Click);
            // 
            // paymentReportToolStripMenuItem
            // 
            this.paymentReportToolStripMenuItem.Enabled = false;
            this.paymentReportToolStripMenuItem.Name = "paymentReportToolStripMenuItem";
            this.paymentReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.paymentReportToolStripMenuItem.Text = "Payment Report";
            this.paymentReportToolStripMenuItem.Click += new System.EventHandler(this.paymentReportToolStripMenuItem_Click);
            // 
            // receiptReportToolStripMenuItem
            // 
            this.receiptReportToolStripMenuItem.Enabled = false;
            this.receiptReportToolStripMenuItem.Name = "receiptReportToolStripMenuItem";
            this.receiptReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.receiptReportToolStripMenuItem.Text = "Receipt Report";
            this.receiptReportToolStripMenuItem.Click += new System.EventHandler(this.receiptReportToolStripMenuItem_Click);
            // 
            // journalReportToolStripMenuItem
            // 
            this.journalReportToolStripMenuItem.Enabled = false;
            this.journalReportToolStripMenuItem.Name = "journalReportToolStripMenuItem";
            this.journalReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.journalReportToolStripMenuItem.Text = "Journal Report";
            this.journalReportToolStripMenuItem.Click += new System.EventHandler(this.journalReportToolStripMenuItem_Click);
            // 
            // pDCPayableReportToolStripMenuItem
            // 
            this.pDCPayableReportToolStripMenuItem.Enabled = false;
            this.pDCPayableReportToolStripMenuItem.Name = "pDCPayableReportToolStripMenuItem";
            this.pDCPayableReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.pDCPayableReportToolStripMenuItem.Text = "PDC Payable Report";
            this.pDCPayableReportToolStripMenuItem.Click += new System.EventHandler(this.pDCPayableReportToolStripMenuItem_Click);
            // 
            // pDCReceivableReportToolStripMenuItem
            // 
            this.pDCReceivableReportToolStripMenuItem.Enabled = false;
            this.pDCReceivableReportToolStripMenuItem.Name = "pDCReceivableReportToolStripMenuItem";
            this.pDCReceivableReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.pDCReceivableReportToolStripMenuItem.Text = "PDC Receivable Report";
            this.pDCReceivableReportToolStripMenuItem.Click += new System.EventHandler(this.pDCReceivableReportToolStripMenuItem_Click);
            // 
            // pDCClearanceToolStripMenuItem1
            // 
            this.pDCClearanceToolStripMenuItem1.Enabled = false;
            this.pDCClearanceToolStripMenuItem1.Name = "pDCClearanceToolStripMenuItem1";
            this.pDCClearanceToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.pDCClearanceToolStripMenuItem1.Text = "PDC Clearance Report";
            this.pDCClearanceToolStripMenuItem1.Click += new System.EventHandler(this.pDCClearanceToolStripMenuItem1_Click);
            // 
            // purchaseOrderReportToolStripMenuItem
            // 
            this.purchaseOrderReportToolStripMenuItem.Enabled = false;
            this.purchaseOrderReportToolStripMenuItem.Name = "purchaseOrderReportToolStripMenuItem";
            this.purchaseOrderReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.purchaseOrderReportToolStripMenuItem.Text = "Purchase Order Report";
            this.purchaseOrderReportToolStripMenuItem.Click += new System.EventHandler(this.purchaseOrderReportToolStripMenuItem_Click);
            // 
            // materialReceiptReportToolStripMenuItem
            // 
            this.materialReceiptReportToolStripMenuItem.Enabled = false;
            this.materialReceiptReportToolStripMenuItem.Name = "materialReceiptReportToolStripMenuItem";
            this.materialReceiptReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.materialReceiptReportToolStripMenuItem.Text = "Material Receipt Report";
            this.materialReceiptReportToolStripMenuItem.Click += new System.EventHandler(this.materialReceiptReportToolStripMenuItem_Click);
            // 
            // rejectionOutReportToolStripMenuItem
            // 
            this.rejectionOutReportToolStripMenuItem.Enabled = false;
            this.rejectionOutReportToolStripMenuItem.Name = "rejectionOutReportToolStripMenuItem";
            this.rejectionOutReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.rejectionOutReportToolStripMenuItem.Text = "Rejection Out Report";
            this.rejectionOutReportToolStripMenuItem.Click += new System.EventHandler(this.rejectionOutReportToolStripMenuItem_Click);
            // 
            // purchaseInvoiceReportToolStripMenuItem
            // 
            this.purchaseInvoiceReportToolStripMenuItem.Enabled = false;
            this.purchaseInvoiceReportToolStripMenuItem.Name = "purchaseInvoiceReportToolStripMenuItem";
            this.purchaseInvoiceReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.purchaseInvoiceReportToolStripMenuItem.Text = "Purchase Invoice Report";
            this.purchaseInvoiceReportToolStripMenuItem.Click += new System.EventHandler(this.purchaseInvoiceReportToolStripMenuItem_Click);
            // 
            // purchaseReturnReportToolStripMenuItem
            // 
            this.purchaseReturnReportToolStripMenuItem.Enabled = false;
            this.purchaseReturnReportToolStripMenuItem.Name = "purchaseReturnReportToolStripMenuItem";
            this.purchaseReturnReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.purchaseReturnReportToolStripMenuItem.Text = "Purchase Return Report";
            this.purchaseReturnReportToolStripMenuItem.Click += new System.EventHandler(this.purchaseReturnReportToolStripMenuItem_Click);
            // 
            // salesQuotationReportToolStripMenuItem
            // 
            this.salesQuotationReportToolStripMenuItem.Enabled = false;
            this.salesQuotationReportToolStripMenuItem.Name = "salesQuotationReportToolStripMenuItem";
            this.salesQuotationReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.salesQuotationReportToolStripMenuItem.Text = "Sales Quotation Report";
            this.salesQuotationReportToolStripMenuItem.Click += new System.EventHandler(this.salesQuotationReportToolStripMenuItem_Click);
            // 
            // salesOrderReportToolStripMenuItem
            // 
            this.salesOrderReportToolStripMenuItem.Enabled = false;
            this.salesOrderReportToolStripMenuItem.Name = "salesOrderReportToolStripMenuItem";
            this.salesOrderReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.salesOrderReportToolStripMenuItem.Text = "Sales Order Report";
            this.salesOrderReportToolStripMenuItem.Click += new System.EventHandler(this.salesOrderReportToolStripMenuItem_Click);
            // 
            // deliveryNoteReportToolStripMenuItem
            // 
            this.deliveryNoteReportToolStripMenuItem.Enabled = false;
            this.deliveryNoteReportToolStripMenuItem.Name = "deliveryNoteReportToolStripMenuItem";
            this.deliveryNoteReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.deliveryNoteReportToolStripMenuItem.Text = "Delivery Note Report";
            this.deliveryNoteReportToolStripMenuItem.Click += new System.EventHandler(this.deliveryNoteReportToolStripMenuItem_Click);
            // 
            // rejectionInReportToolStripMenuItem
            // 
            this.rejectionInReportToolStripMenuItem.Enabled = false;
            this.rejectionInReportToolStripMenuItem.Name = "rejectionInReportToolStripMenuItem";
            this.rejectionInReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.rejectionInReportToolStripMenuItem.Text = "Rejection In Report";
            this.rejectionInReportToolStripMenuItem.Click += new System.EventHandler(this.rejectionInReportToolStripMenuItem_Click);
            // 
            // salesInvoiceReportToolStripMenuItem
            // 
            this.salesInvoiceReportToolStripMenuItem.Enabled = false;
            this.salesInvoiceReportToolStripMenuItem.Name = "salesInvoiceReportToolStripMenuItem";
            this.salesInvoiceReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.salesInvoiceReportToolStripMenuItem.Text = "Sales Invoice Report";
            this.salesInvoiceReportToolStripMenuItem.Click += new System.EventHandler(this.salesInvoiceReportToolStripMenuItem_Click);
            // 
            // salesReturnReportToolStripMenuItem
            // 
            this.salesReturnReportToolStripMenuItem.Enabled = false;
            this.salesReturnReportToolStripMenuItem.Name = "salesReturnReportToolStripMenuItem";
            this.salesReturnReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.salesReturnReportToolStripMenuItem.Text = "Sales Return Report";
            this.salesReturnReportToolStripMenuItem.Click += new System.EventHandler(this.salesReturnReportToolStripMenuItem_Click);
            // 
            // physicalStockReportToolStripMenuItem
            // 
            this.physicalStockReportToolStripMenuItem.Enabled = false;
            this.physicalStockReportToolStripMenuItem.Name = "physicalStockReportToolStripMenuItem";
            this.physicalStockReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.physicalStockReportToolStripMenuItem.Text = "Physical Stock Report";
            this.physicalStockReportToolStripMenuItem.Click += new System.EventHandler(this.physicalStockReportToolStripMenuItem_Click);
            // 
            // creditNoteReportToolStripMenuItem
            // 
            this.creditNoteReportToolStripMenuItem.Enabled = false;
            this.creditNoteReportToolStripMenuItem.Name = "creditNoteReportToolStripMenuItem";
            this.creditNoteReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.creditNoteReportToolStripMenuItem.Text = "Credit Note Report";
            this.creditNoteReportToolStripMenuItem.Click += new System.EventHandler(this.creditNoteReportToolStripMenuItem_Click);
            // 
            // debitNoteReportToolStripMenuItem
            // 
            this.debitNoteReportToolStripMenuItem.Enabled = false;
            this.debitNoteReportToolStripMenuItem.Name = "debitNoteReportToolStripMenuItem";
            this.debitNoteReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.debitNoteReportToolStripMenuItem.Text = "Debit Note Report";
            this.debitNoteReportToolStripMenuItem.Click += new System.EventHandler(this.debitNoteReportToolStripMenuItem_Click);
            // 
            // stockJournalReportToolStripMenuItem
            // 
            this.stockJournalReportToolStripMenuItem.Enabled = false;
            this.stockJournalReportToolStripMenuItem.Name = "stockJournalReportToolStripMenuItem";
            this.stockJournalReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.stockJournalReportToolStripMenuItem.Text = "Stock Journal Report";
            this.stockJournalReportToolStripMenuItem.Click += new System.EventHandler(this.stockJournalReportToolStripMenuItem_Click);
            // 
            // payrollToolStripMenuItem1
            // 
            this.payrollToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeToolStripMenuItem,
            this.dailyAttendanceToolStripMenuItem,
            this.monthlyAttendanceToolStripMenuItem,
            this.dailySalaryToolStripMenuItem,
            this.monthlySalaryToolStripMenuItem,
            this.payheadToolStripMenuItem,
            this.salaryPackageToolStripMenuItem,
            this.advancePaymentToolStripMenuItem1,
            this.bonusDeductionToolStripMenuItem,
            this.employeeAddressBookToolStripMenuItem});
            this.payrollToolStripMenuItem1.Enabled = false;
            this.payrollToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.payrollToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.payrollToolStripMenuItem1.Name = "payrollToolStripMenuItem1";
            this.payrollToolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.payrollToolStripMenuItem1.Text = "Payroll";
            this.payrollToolStripMenuItem1.Visible = false;
            // 
            // employeeToolStripMenuItem
            // 
            this.employeeToolStripMenuItem.Enabled = false;
            this.employeeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.employeeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.employeeToolStripMenuItem.Name = "employeeToolStripMenuItem";
            this.employeeToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.employeeToolStripMenuItem.Text = "Employee";
            this.employeeToolStripMenuItem.Click += new System.EventHandler(this.employeeToolStripMenuItem_Click);
            // 
            // dailyAttendanceToolStripMenuItem
            // 
            this.dailyAttendanceToolStripMenuItem.Enabled = false;
            this.dailyAttendanceToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.dailyAttendanceToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dailyAttendanceToolStripMenuItem.Name = "dailyAttendanceToolStripMenuItem";
            this.dailyAttendanceToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.dailyAttendanceToolStripMenuItem.Text = "Daily Attendance";
            this.dailyAttendanceToolStripMenuItem.Click += new System.EventHandler(this.dailyAttendanceToolStripMenuItem_Click);
            // 
            // monthlyAttendanceToolStripMenuItem
            // 
            this.monthlyAttendanceToolStripMenuItem.Enabled = false;
            this.monthlyAttendanceToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.monthlyAttendanceToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monthlyAttendanceToolStripMenuItem.Name = "monthlyAttendanceToolStripMenuItem";
            this.monthlyAttendanceToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.monthlyAttendanceToolStripMenuItem.Text = "Monthly Attendance";
            this.monthlyAttendanceToolStripMenuItem.Click += new System.EventHandler(this.monthlyAttendanceToolStripMenuItem_Click);
            // 
            // dailySalaryToolStripMenuItem
            // 
            this.dailySalaryToolStripMenuItem.Enabled = false;
            this.dailySalaryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.dailySalaryToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dailySalaryToolStripMenuItem.Name = "dailySalaryToolStripMenuItem";
            this.dailySalaryToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.dailySalaryToolStripMenuItem.Text = "Daily Salary";
            this.dailySalaryToolStripMenuItem.Click += new System.EventHandler(this.dailySalaryToolStripMenuItem_Click);
            // 
            // monthlySalaryToolStripMenuItem
            // 
            this.monthlySalaryToolStripMenuItem.Enabled = false;
            this.monthlySalaryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.monthlySalaryToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monthlySalaryToolStripMenuItem.Name = "monthlySalaryToolStripMenuItem";
            this.monthlySalaryToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.monthlySalaryToolStripMenuItem.Text = "Monthly Salary";
            this.monthlySalaryToolStripMenuItem.Click += new System.EventHandler(this.monthlySalaryToolStripMenuItem_Click);
            // 
            // payheadToolStripMenuItem
            // 
            this.payheadToolStripMenuItem.Enabled = false;
            this.payheadToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.payheadToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.payheadToolStripMenuItem.Name = "payheadToolStripMenuItem";
            this.payheadToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.payheadToolStripMenuItem.Text = "Payhead";
            this.payheadToolStripMenuItem.Click += new System.EventHandler(this.payheadToolStripMenuItem_Click);
            // 
            // salaryPackageToolStripMenuItem
            // 
            this.salaryPackageToolStripMenuItem.Enabled = false;
            this.salaryPackageToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.salaryPackageToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.salaryPackageToolStripMenuItem.Name = "salaryPackageToolStripMenuItem";
            this.salaryPackageToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.salaryPackageToolStripMenuItem.Text = "Salary Package";
            this.salaryPackageToolStripMenuItem.Click += new System.EventHandler(this.salaryPackageToolStripMenuItem_Click);
            // 
            // advancePaymentToolStripMenuItem1
            // 
            this.advancePaymentToolStripMenuItem1.Enabled = false;
            this.advancePaymentToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.advancePaymentToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advancePaymentToolStripMenuItem1.Name = "advancePaymentToolStripMenuItem1";
            this.advancePaymentToolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.advancePaymentToolStripMenuItem1.Text = "Advance Payment";
            this.advancePaymentToolStripMenuItem1.Click += new System.EventHandler(this.advancePaymentToolStripMenuItem1_Click);
            // 
            // bonusDeductionToolStripMenuItem
            // 
            this.bonusDeductionToolStripMenuItem.Enabled = false;
            this.bonusDeductionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.bonusDeductionToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bonusDeductionToolStripMenuItem.Name = "bonusDeductionToolStripMenuItem";
            this.bonusDeductionToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.bonusDeductionToolStripMenuItem.Text = "Bonus Deduction";
            this.bonusDeductionToolStripMenuItem.Click += new System.EventHandler(this.bonusDeductionToolStripMenuItem_Click);
            // 
            // employeeAddressBookToolStripMenuItem
            // 
            this.employeeAddressBookToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.employeeAddressBookToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.employeeAddressBookToolStripMenuItem.Name = "employeeAddressBookToolStripMenuItem";
            this.employeeAddressBookToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.employeeAddressBookToolStripMenuItem.Text = "Employee Address Book";
            this.employeeAddressBookToolStripMenuItem.Click += new System.EventHandler(this.employeeAddressBookToolStripMenuItem_Click);
            // 
            // dayBookToolStripMenuItem1
            // 
            this.dayBookToolStripMenuItem1.Enabled = false;
            this.dayBookToolStripMenuItem1.Name = "dayBookToolStripMenuItem1";
            this.dayBookToolStripMenuItem1.Size = new System.Drawing.Size(218, 22);
            this.dayBookToolStripMenuItem1.Text = "Day Book";
            this.dayBookToolStripMenuItem1.Visible = false;
            this.dayBookToolStripMenuItem1.Click += new System.EventHandler(this.dayBookToolStripMenuItem1_Click);
            // 
            // cashBToolStripMenuItem
            // 
            this.cashBToolStripMenuItem.Enabled = false;
            this.cashBToolStripMenuItem.Name = "cashBToolStripMenuItem";
            this.cashBToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.cashBToolStripMenuItem.Text = "Cash/Bank Book";
            this.cashBToolStripMenuItem.Visible = false;
            this.cashBToolStripMenuItem.Click += new System.EventHandler(this.cashBToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Enabled = false;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem6.Text = "Account Group Report";
            this.toolStripMenuItem6.Visible = false;
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Enabled = false;
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem7.Text = "Account Ledger Report";
            this.toolStripMenuItem7.Visible = false;
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Enabled = false;
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem8.Text = "Outstanding Report";
            this.toolStripMenuItem8.Visible = false;
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Enabled = false;
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItem9.Text = "Ageing Report";
            this.toolStripMenuItem9.Visible = false;
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // PartyAddressBooktoolStripMenuItem
            // 
            this.PartyAddressBooktoolStripMenuItem.Enabled = false;
            this.PartyAddressBooktoolStripMenuItem.Name = "PartyAddressBooktoolStripMenuItem";
            this.PartyAddressBooktoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.PartyAddressBooktoolStripMenuItem.Text = "Party\'s Address Book";
            this.PartyAddressBooktoolStripMenuItem.Visible = false;
            this.PartyAddressBooktoolStripMenuItem.Click += new System.EventHandler(this.PartyAddressBooktoolStripMenuItem_Click);
            // 
            // StockReporttoolStripMenuItem
            // 
            this.StockReporttoolStripMenuItem.Enabled = false;
            this.StockReporttoolStripMenuItem.Name = "StockReporttoolStripMenuItem";
            this.StockReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.StockReporttoolStripMenuItem.Text = "Stock Report";
            this.StockReporttoolStripMenuItem.Visible = false;
            this.StockReporttoolStripMenuItem.Click += new System.EventHandler(this.StockReporttoolStripMenuItem_Click);
            // 
            // ShortExpiryReporttoolStripMenuItem
            // 
            this.ShortExpiryReporttoolStripMenuItem.Enabled = false;
            this.ShortExpiryReporttoolStripMenuItem.Name = "ShortExpiryReporttoolStripMenuItem";
            this.ShortExpiryReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.ShortExpiryReporttoolStripMenuItem.Text = "Short Expiry Report";
            this.ShortExpiryReporttoolStripMenuItem.Visible = false;
            this.ShortExpiryReporttoolStripMenuItem.Click += new System.EventHandler(this.ShortExpiryReporttoolStripMenuItem_Click);
            // 
            // ProductStaticstoolStripMenuItem
            // 
            this.ProductStaticstoolStripMenuItem.Enabled = false;
            this.ProductStaticstoolStripMenuItem.Name = "ProductStaticstoolStripMenuItem";
            this.ProductStaticstoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.ProductStaticstoolStripMenuItem.Text = "Product Statistics";
            this.ProductStaticstoolStripMenuItem.Visible = false;
            this.ProductStaticstoolStripMenuItem.Click += new System.EventHandler(this.ProductStaticstoolStripMenuItem_Click);
            // 
            // PriceListReporttoolStripMenuItem
            // 
            this.PriceListReporttoolStripMenuItem.Enabled = false;
            this.PriceListReporttoolStripMenuItem.Name = "PriceListReporttoolStripMenuItem";
            this.PriceListReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.PriceListReporttoolStripMenuItem.Text = "Price List Report";
            this.PriceListReporttoolStripMenuItem.Visible = false;
            this.PriceListReporttoolStripMenuItem.Click += new System.EventHandler(this.PriceListReporttoolStripMenuItem_Click);
            // 
            // TaxReporttoolStripMenuItem
            // 
            this.TaxReporttoolStripMenuItem.Enabled = false;
            this.TaxReporttoolStripMenuItem.Name = "TaxReporttoolStripMenuItem";
            this.TaxReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.TaxReporttoolStripMenuItem.Text = "Tax Report";
            this.TaxReporttoolStripMenuItem.Visible = false;
            this.TaxReporttoolStripMenuItem.Click += new System.EventHandler(this.TaxReporttoolStripMenuItem_Click);
            // 
            // VatReporttoolStripMenuItem
            // 
            this.VatReporttoolStripMenuItem.Enabled = false;
            this.VatReporttoolStripMenuItem.Name = "VatReporttoolStripMenuItem";
            this.VatReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.VatReporttoolStripMenuItem.Text = "VAT Return Report";
            this.VatReporttoolStripMenuItem.Visible = false;
            this.VatReporttoolStripMenuItem.Click += new System.EventHandler(this.VatReporttoolStripMenuItem_Click);
            // 
            // ChequeReporttoolStripMenuItem
            // 
            this.ChequeReporttoolStripMenuItem.Enabled = false;
            this.ChequeReporttoolStripMenuItem.Name = "ChequeReporttoolStripMenuItem";
            this.ChequeReporttoolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.ChequeReporttoolStripMenuItem.Text = "Cheque Report";
            this.ChequeReporttoolStripMenuItem.Visible = false;
            this.ChequeReporttoolStripMenuItem.Click += new System.EventHandler(this.ChequeReporttoolStripMenuItem_Click);
            // 
            // freeSaleReportToolStripMenuItem
            // 
            this.freeSaleReportToolStripMenuItem.Enabled = false;
            this.freeSaleReportToolStripMenuItem.Name = "freeSaleReportToolStripMenuItem";
            this.freeSaleReportToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.freeSaleReportToolStripMenuItem.Text = "Free Sale Report";
            this.freeSaleReportToolStripMenuItem.Visible = false;
            this.freeSaleReportToolStripMenuItem.Click += new System.EventHandler(this.freeSaleReportToolStripMenuItem_Click);
            // 
            // productVsBatchReportToolStripMenuItem
            // 
            this.productVsBatchReportToolStripMenuItem.Enabled = false;
            this.productVsBatchReportToolStripMenuItem.Name = "productVsBatchReportToolStripMenuItem";
            this.productVsBatchReportToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.productVsBatchReportToolStripMenuItem.Text = "Product Vs Batch Report";
            this.productVsBatchReportToolStripMenuItem.Visible = false;
            this.productVsBatchReportToolStripMenuItem.Click += new System.EventHandler(this.productVsBatchReportToolStripMenuItem_Click);
            // 
            // stockReportDetailsToolStripMenuItem
            // 
            this.stockReportDetailsToolStripMenuItem.Enabled = false;
            this.stockReportDetailsToolStripMenuItem.Name = "stockReportDetailsToolStripMenuItem";
            this.stockReportDetailsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.stockReportDetailsToolStripMenuItem.Text = "Stock Report Details";
            this.stockReportDetailsToolStripMenuItem.Visible = false;
            this.stockReportDetailsToolStripMenuItem.Click += new System.EventHandler(this.stockReportDetailsToolStripMenuItem_Click);
            // 
            // stockVarianceReportToolStripMenuItem
            // 
            this.stockVarianceReportToolStripMenuItem.Enabled = false;
            this.stockVarianceReportToolStripMenuItem.Name = "stockVarianceReportToolStripMenuItem";
            this.stockVarianceReportToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.stockVarianceReportToolStripMenuItem.Text = "Stock Variance Report";
            this.stockVarianceReportToolStripMenuItem.Visible = false;
            this.stockVarianceReportToolStripMenuItem.Click += new System.EventHandler(this.stockVarianceReportToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productSearchToolStripMenuItem,
            this.voucherSearchToolStripMenuItem,
            this.voucherWiseProductSearchToolStripMenuItem});
            this.searchToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(75, 21);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // productSearchToolStripMenuItem
            // 
            this.productSearchToolStripMenuItem.Name = "productSearchToolStripMenuItem";
            this.productSearchToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.productSearchToolStripMenuItem.Text = "Product Search";
            this.productSearchToolStripMenuItem.Click += new System.EventHandler(this.productSearchToolStripMenuItem_Click);
            // 
            // voucherSearchToolStripMenuItem
            // 
            this.voucherSearchToolStripMenuItem.Name = "voucherSearchToolStripMenuItem";
            this.voucherSearchToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.voucherSearchToolStripMenuItem.Text = "Form Search";
            this.voucherSearchToolStripMenuItem.Click += new System.EventHandler(this.voucherSearchToolStripMenuItem_Click);
            // 
            // voucherWiseProductSearchToolStripMenuItem
            // 
            this.voucherWiseProductSearchToolStripMenuItem.Name = "voucherWiseProductSearchToolStripMenuItem";
            this.voucherWiseProductSearchToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.voucherWiseProductSearchToolStripMenuItem.Text = "Form Wise Product Search";
            this.voucherWiseProductSearchToolStripMenuItem.Click += new System.EventHandler(this.voucherWiseProductSearchToolStripMenuItem_Click);
            // 
            // payrollToolStripMenuItem
            // 
            this.payrollToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.designationToolStripMenuItem1,
            this.payHeadToolStripMenuItem1,
            this.salaryPackageCreationToolStripMenuItem,
            this.employeeCreationToolStripMenuItem,
            this.holidaySettingsToolStripMenuItem,
            this.statutoryComplianceSettingsToolStripMenuItem,
            this.monthlySalarySettingsToolStripMenuItem,
            this.mmAttendance,
            this.advancePaymentToolStripMenuItem,
            this.bonusDeductionToolStripMenuItem1,
            this.frmMonthlySalaryVoucherToolStripMenuItem,
            this.DailySalaryVouchertoolStripMenuItem1,
            this.paySlipToolStripMenuItem,
            this.registersToolStripMenuItem3});
            this.payrollToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("payrollToolStripMenuItem.Image")));
            this.payrollToolStripMenuItem.Name = "payrollToolStripMenuItem";
            this.payrollToolStripMenuItem.Size = new System.Drawing.Size(71, 21);
            this.payrollToolStripMenuItem.Text = "&Payroll";
            // 
            // designationToolStripMenuItem1
            // 
            this.designationToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.designationToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.designationToolStripMenuItem1.Name = "designationToolStripMenuItem1";
            this.designationToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.designationToolStripMenuItem1.Text = "Designation";
            this.designationToolStripMenuItem1.Click += new System.EventHandler(this.designationToolStripMenuItem1_Click);
            // 
            // payHeadToolStripMenuItem1
            // 
            this.payHeadToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payHeadToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.payHeadToolStripMenuItem1.Name = "payHeadToolStripMenuItem1";
            this.payHeadToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.payHeadToolStripMenuItem1.Text = "Pay Element";
            this.payHeadToolStripMenuItem1.Click += new System.EventHandler(this.payHeadToolStripMenuItem1_Click);
            // 
            // salaryPackageCreationToolStripMenuItem
            // 
            this.salaryPackageCreationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salaryPackageCreationToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.salaryPackageCreationToolStripMenuItem.Name = "salaryPackageCreationToolStripMenuItem";
            this.salaryPackageCreationToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.salaryPackageCreationToolStripMenuItem.Text = "Salary Package Creation";
            this.salaryPackageCreationToolStripMenuItem.Click += new System.EventHandler(this.salaryPackageCreationToolStripMenuItem_Click);
            // 
            // employeeCreationToolStripMenuItem
            // 
            this.employeeCreationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeCreationToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.employeeCreationToolStripMenuItem.Name = "employeeCreationToolStripMenuItem";
            this.employeeCreationToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.employeeCreationToolStripMenuItem.Text = "Employee Creation";
            this.employeeCreationToolStripMenuItem.Click += new System.EventHandler(this.employeeCreationToolStripMenuItem_Click);
            // 
            // holidaySettingsToolStripMenuItem
            // 
            this.holidaySettingsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.holidaySettingsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.holidaySettingsToolStripMenuItem.Name = "holidaySettingsToolStripMenuItem";
            this.holidaySettingsToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.holidaySettingsToolStripMenuItem.Text = "Holiday Settings";
            this.holidaySettingsToolStripMenuItem.Click += new System.EventHandler(this.holidaySettingsToolStripMenuItem_Click);
            // 
            // statutoryComplianceSettingsToolStripMenuItem
            // 
            this.statutoryComplianceSettingsToolStripMenuItem.Name = "statutoryComplianceSettingsToolStripMenuItem";
            this.statutoryComplianceSettingsToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.statutoryComplianceSettingsToolStripMenuItem.Text = "Statutory Compliance Settings";
            this.statutoryComplianceSettingsToolStripMenuItem.Click += new System.EventHandler(this.statutoryComplianceSettingsToolStripMenuItem_Click);
            // 
            // monthlySalarySettingsToolStripMenuItem
            // 
            this.monthlySalarySettingsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monthlySalarySettingsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.monthlySalarySettingsToolStripMenuItem.Name = "monthlySalarySettingsToolStripMenuItem";
            this.monthlySalarySettingsToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.monthlySalarySettingsToolStripMenuItem.Text = "Monthly Salary Settings";
            this.monthlySalarySettingsToolStripMenuItem.Click += new System.EventHandler(this.monthlySalarySettingsToolStripMenuItem_Click);
            // 
            // mmAttendance
            // 
            this.mmAttendance.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mmAttendance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.mmAttendance.Name = "mmAttendance";
            this.mmAttendance.Size = new System.Drawing.Size(234, 22);
            this.mmAttendance.Text = "Attendance";
            this.mmAttendance.Click += new System.EventHandler(this.mmAttendance_Click);
            // 
            // advancePaymentToolStripMenuItem
            // 
            this.advancePaymentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advancePaymentToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.advancePaymentToolStripMenuItem.Name = "advancePaymentToolStripMenuItem";
            this.advancePaymentToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.advancePaymentToolStripMenuItem.Text = "Advance Payment";
            this.advancePaymentToolStripMenuItem.Click += new System.EventHandler(this.advancePaymentToolStripMenuItem_Click);
            // 
            // bonusDeductionToolStripMenuItem1
            // 
            this.bonusDeductionToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bonusDeductionToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bonusDeductionToolStripMenuItem1.Name = "bonusDeductionToolStripMenuItem1";
            this.bonusDeductionToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.bonusDeductionToolStripMenuItem1.Text = "Bonus/Deduction";
            this.bonusDeductionToolStripMenuItem1.Click += new System.EventHandler(this.bonusDeductionToolStripMenuItem1_Click);
            // 
            // frmMonthlySalaryVoucherToolStripMenuItem
            // 
            this.frmMonthlySalaryVoucherToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmMonthlySalaryVoucherToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frmMonthlySalaryVoucherToolStripMenuItem.Name = "frmMonthlySalaryVoucherToolStripMenuItem";
            this.frmMonthlySalaryVoucherToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.frmMonthlySalaryVoucherToolStripMenuItem.Text = "Monthly Salary Form";
            this.frmMonthlySalaryVoucherToolStripMenuItem.Click += new System.EventHandler(this.frmMonthlySalaryVoucherToolStripMenuItem_Click);
            // 
            // DailySalaryVouchertoolStripMenuItem1
            // 
            this.DailySalaryVouchertoolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DailySalaryVouchertoolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DailySalaryVouchertoolStripMenuItem1.Name = "DailySalaryVouchertoolStripMenuItem1";
            this.DailySalaryVouchertoolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.DailySalaryVouchertoolStripMenuItem1.Text = "Daily Salary Form";
            this.DailySalaryVouchertoolStripMenuItem1.Click += new System.EventHandler(this.DailySalaryVouchertoolStripMenuItem1_Click);
            // 
            // paySlipToolStripMenuItem
            // 
            this.paySlipToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paySlipToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.paySlipToolStripMenuItem.Name = "paySlipToolStripMenuItem";
            this.paySlipToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.paySlipToolStripMenuItem.Text = "Pay Slip";
            this.paySlipToolStripMenuItem.Click += new System.EventHandler(this.paySlipToolStripMenuItem_Click);
            // 
            // registersToolStripMenuItem3
            // 
            this.registersToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salaryPackageRegisterToolStripMenuItem1,
            this.employeeRegisterToolStripMenuItem1,
            this.advanceRegisterToolStripMenuItem,
            this.bonusDeductionRegisterToolStripMenuItem,
            this.monthlySalaryRegisterToolStripMenuItem1,
            this.dailySalaryRegisterToolStripMenuItem1});
            this.registersToolStripMenuItem3.Name = "registersToolStripMenuItem3";
            this.registersToolStripMenuItem3.Size = new System.Drawing.Size(234, 22);
            this.registersToolStripMenuItem3.Text = "Registers";
            // 
            // salaryPackageRegisterToolStripMenuItem1
            // 
            this.salaryPackageRegisterToolStripMenuItem1.Name = "salaryPackageRegisterToolStripMenuItem1";
            this.salaryPackageRegisterToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.salaryPackageRegisterToolStripMenuItem1.Text = "Salary Package Register";
            this.salaryPackageRegisterToolStripMenuItem1.Click += new System.EventHandler(this.salaryPackageRegisterToolStripMenuItem_Click);
            // 
            // employeeRegisterToolStripMenuItem1
            // 
            this.employeeRegisterToolStripMenuItem1.Name = "employeeRegisterToolStripMenuItem1";
            this.employeeRegisterToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.employeeRegisterToolStripMenuItem1.Text = "Employee Register";
            this.employeeRegisterToolStripMenuItem1.Click += new System.EventHandler(this.employeeRegisterToolStripMenuItem_Click);
            // 
            // advanceRegisterToolStripMenuItem
            // 
            this.advanceRegisterToolStripMenuItem.Name = "advanceRegisterToolStripMenuItem";
            this.advanceRegisterToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.advanceRegisterToolStripMenuItem.Text = "Advance Register";
            this.advanceRegisterToolStripMenuItem.Click += new System.EventHandler(this.mmadvanceRegisterToolStripMenuItem_Click);
            // 
            // bonusDeductionRegisterToolStripMenuItem
            // 
            this.bonusDeductionRegisterToolStripMenuItem.Name = "bonusDeductionRegisterToolStripMenuItem";
            this.bonusDeductionRegisterToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.bonusDeductionRegisterToolStripMenuItem.Text = "Bonus/Deduction Register";
            this.bonusDeductionRegisterToolStripMenuItem.Click += new System.EventHandler(this.bonusDeductionRegisterToolStripMenuItem1_Click);
            // 
            // monthlySalaryRegisterToolStripMenuItem1
            // 
            this.monthlySalaryRegisterToolStripMenuItem1.Name = "monthlySalaryRegisterToolStripMenuItem1";
            this.monthlySalaryRegisterToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.monthlySalaryRegisterToolStripMenuItem1.Text = "Monthly Salary Register";
            this.monthlySalaryRegisterToolStripMenuItem1.Click += new System.EventHandler(this.monthlySalaryRegisterToolStripMenuItem_Click);
            // 
            // dailySalaryRegisterToolStripMenuItem1
            // 
            this.dailySalaryRegisterToolStripMenuItem1.Name = "dailySalaryRegisterToolStripMenuItem1";
            this.dailySalaryRegisterToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.dailySalaryRegisterToolStripMenuItem1.Text = "Daily Salary Register";
            this.dailySalaryRegisterToolStripMenuItem1.Click += new System.EventHandler(this.dailySalaryRegisterToolStripMenuItem_Click);
            // 
            // reminderToolStripMenuItem
            // 
            this.reminderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.personalReminderToolStripMenuItem,
            this.overduePurchaseOrderToolStripMenuItem,
            this.overdueSalesOrderToolStripMenuItem,
            this.shortExpiryToolStripMenuItem,
            this.stockToolStripMenuItem});
            this.reminderToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.reminderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("reminderToolStripMenuItem.Image")));
            this.reminderToolStripMenuItem.Name = "reminderToolStripMenuItem";
            this.reminderToolStripMenuItem.Size = new System.Drawing.Size(92, 21);
            this.reminderToolStripMenuItem.Text = "&Reminder";
            // 
            // personalReminderToolStripMenuItem
            // 
            this.personalReminderToolStripMenuItem.Name = "personalReminderToolStripMenuItem";
            this.personalReminderToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.personalReminderToolStripMenuItem.Text = "Personal Reminder";
            this.personalReminderToolStripMenuItem.Click += new System.EventHandler(this.personalReminderToolStripMenuItem_Click);
            // 
            // overduePurchaseOrderToolStripMenuItem
            // 
            this.overduePurchaseOrderToolStripMenuItem.Name = "overduePurchaseOrderToolStripMenuItem";
            this.overduePurchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.overduePurchaseOrderToolStripMenuItem.Text = "Overdue Purchase Order";
            this.overduePurchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.overduePurchaseOrderToolStripMenuItem_Click);
            // 
            // overdueSalesOrderToolStripMenuItem
            // 
            this.overdueSalesOrderToolStripMenuItem.Name = "overdueSalesOrderToolStripMenuItem";
            this.overdueSalesOrderToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.overdueSalesOrderToolStripMenuItem.Text = "Overdue Sales Order";
            this.overdueSalesOrderToolStripMenuItem.Click += new System.EventHandler(this.overdueSalesOrderToolStripMenuItem_Click);
            // 
            // shortExpiryToolStripMenuItem
            // 
            this.shortExpiryToolStripMenuItem.Name = "shortExpiryToolStripMenuItem";
            this.shortExpiryToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.shortExpiryToolStripMenuItem.Text = "Short Expiry";
            this.shortExpiryToolStripMenuItem.Click += new System.EventHandler(this.shortExpiryToolStripMenuItem_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.stockToolStripMenuItem.Text = "Stock";
            this.stockToolStripMenuItem.Click += new System.EventHandler(this.stockToolStripMenuItem_Click);
            // 
            // utilitiesToolStripMenuItem
            // 
            this.utilitiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miracleSkateToolStripMenuItem,
            this.miracleIToolStripMenuItem});
            this.utilitiesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.utilitiesToolStripMenuItem.Name = "utilitiesToolStripMenuItem";
            this.utilitiesToolStripMenuItem.Size = new System.Drawing.Size(62, 21);
            this.utilitiesToolStripMenuItem.Text = "Utilities";
            this.utilitiesToolStripMenuItem.Visible = false;
            // 
            // miracleSkateToolStripMenuItem
            // 
            this.miracleSkateToolStripMenuItem.Name = "miracleSkateToolStripMenuItem";
            this.miracleSkateToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.miracleSkateToolStripMenuItem.Text = "MATFinancials Integrator";
            this.miracleSkateToolStripMenuItem.Click += new System.EventHandler(this.miracleSkateToolStripMenuItem_Click);
            // 
            // miracleIToolStripMenuItem
            // 
            this.miracleIToolStripMenuItem.Name = "miracleIToolStripMenuItem";
            this.miracleIToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.miracleIToolStripMenuItem.Text = "MAT Financials BI";
            this.miracleIToolStripMenuItem.Click += new System.EventHandler(this.miracleIToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutUsToolStripMenuItem,
            this.contentsToolStripMenuItem,
            this.contactUsToolStripMenuItem1});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutUsToolStripMenuItem
            // 
            this.aboutUsToolStripMenuItem.Name = "aboutUsToolStripMenuItem";
            this.aboutUsToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.aboutUsToolStripMenuItem.Text = "About Us";
            this.aboutUsToolStripMenuItem.Click += new System.EventHandler(this.aboutUsToolStripMenuItem_Click);
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.contentsToolStripMenuItem.Text = "Contents";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // contactUsToolStripMenuItem1
            // 
            this.contactUsToolStripMenuItem1.Name = "contactUsToolStripMenuItem1";
            this.contactUsToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
            this.contactUsToolStripMenuItem1.Text = "Send Mail";
            this.contactUsToolStripMenuItem1.Click += new System.EventHandler(this.contactUsToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(40, 21);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // dateToolStripMenuItem
            // 
            this.dateToolStripMenuItem.BackColor = System.Drawing.Color.Gainsboro;
            this.dateToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F, System.Drawing.FontStyle.Bold);
            this.dateToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.dateToolStripMenuItem.Name = "dateToolStripMenuItem";
            this.dateToolStripMenuItem.Size = new System.Drawing.Size(12, 21);
            this.dateToolStripMenuItem.Click += new System.EventHandler(this.dateToolStripMenuItem_Click);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 21);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.Visible = false;
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(152, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(152, 6);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem1.Text = "E&xit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(42, 21);
            this.editToolStripMenuItem.Text = "&Edit";
            this.editToolStripMenuItem.Visible = false;
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.25F);
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(51, 21);
            this.toolsToolStripMenuItem.Text = "&Tools";
            this.toolsToolStripMenuItem.Visible = false;
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem1,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem1,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(44, 21);
            this.helpToolStripMenuItem1.Text = "&Help";
            this.helpToolStripMenuItem1.Visible = false;
            // 
            // contentsToolStripMenuItem1
            // 
            this.contentsToolStripMenuItem1.Name = "contentsToolStripMenuItem1";
            this.contentsToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem1.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem1
            // 
            this.searchToolStripMenuItem1.Name = "searchToolStripMenuItem1";
            this.searchToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.searchToolStripMenuItem1.Text = "&Search";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 616);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(1161, 19);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            this.statusStrip.Visible = false;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 14);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // BackgrndWrkrLoading
            // 
            this.BackgrndWrkrLoading.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgrndWrkrLoading_DoWork);
            this.BackgrndWrkrLoading.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgrndWrkrLoading_RunWorkerCompleted);
            // 
            // ucCalculator1
            // 
            this.ucCalculator1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ucCalculator1.BackColor = System.Drawing.Color.DimGray;
            this.ucCalculator1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucCalculator1.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.ucCalculator1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucCalculator1.Location = new System.Drawing.Point(1100, 381);
            this.ucCalculator1.Margin = new System.Windows.Forms.Padding(4);
            this.ucCalculator1.Name = "ucCalculator1";
            this.ucCalculator1.Size = new System.Drawing.Size(222, 271);
            this.ucCalculator1.TabIndex = 8;
            this.ucCalculator1.Visible = false;
            // 
            // ucQuickLaunch1
            // 
            this.ucQuickLaunch1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ucQuickLaunch1.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ucQuickLaunch1.BackColor = System.Drawing.Color.Gray;
            this.ucQuickLaunch1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucQuickLaunch1.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.ucQuickLaunch1.ForeColor = System.Drawing.Color.Maroon;
            this.ucQuickLaunch1.Location = new System.Drawing.Point(1100, 31);
            this.ucQuickLaunch1.Margin = new System.Windows.Forms.Padding(4);
            this.ucQuickLaunch1.Name = "ucQuickLaunch1";
            this.ucQuickLaunch1.Size = new System.Drawing.Size(222, 689);
            this.ucQuickLaunch1.TabIndex = 4;
            this.ucQuickLaunch1.Visible = false;
            this.ucQuickLaunch1.Load += new System.EventHandler(this.ucQuickLaunch1_Load);
            // 
            // formMDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.ucCalculator1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.ucQuickLaunch1);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "formMDI";
            this.Text = "MAT Financials";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.formMDI_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.formMDI_KeyDown);
            this.Resize += new System.EventHandler(this.formMDI_Resize);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem employeeCreationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salaryPackageCreationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancePaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mmAttendance;
        private System.Windows.Forms.ToolStripMenuItem monthlySalarySettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frmMonthlySalaryVoucherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem holidaySettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bonusDeductionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mastersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DailySalaryVouchertoolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem areaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem counterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem routeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalVoucherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createCompanyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SelectCompanyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem designationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem payHeadToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bankReconciliationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paySlipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reminderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherWiseProductSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overduePurchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overdueSalesOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem personalReminderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shortExpiryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editCompanyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dayBookToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlyAttendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailySalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payheadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salaryPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancePaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bonusDeductionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeAddressBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BackUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RestoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billAllocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem contraReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiptReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCPayableReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCReceivableReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materialReceiptReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectionOutReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseInvoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesQuotationReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesOrderReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryNoteReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectionInReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesInvoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesReturnReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockJournalReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem physicalStockReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditNoteReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debitNoteReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem PartyAddressBooktoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StockReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ShortExpiryReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProductStaticstoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PriceListReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TaxReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VatReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ChequeReporttoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem freeSaleReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productVsBatchReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesManToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contraVoucherToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem taxToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem payrollToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem payrollToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem dateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutUsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker BackgrndWrkrLoading;
        private System.Windows.Forms.ToolStripMenuItem utilitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miracleSkateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miracleIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Home;
        private System.Windows.Forms.ToolStripMenuItem statutoryComplianceSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockVarianceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem changeCurrentDateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem roleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rolePrivilegeSettingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userCreationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changeFinancialYearToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem barcodeSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barcodePrintingToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newFinancialYearToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem surfixPrefixSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeProductTaxToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dotmatrixPrintDesignerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rebuildIndexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesQuotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesOrderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deliveryNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectionInToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesInvoiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesReturnToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pDCReceivableToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem receiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditNoteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesQuotationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesOrderToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deliveryNoteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rejectionInToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesInvoiceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesReturnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCReceivableToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem creditNoteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem receiptsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem supplierStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem supplierCenterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem materialReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectionOutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseInvoiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pDCPayableToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem debitNoteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem registersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem materialReceiptToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rejectionOutToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem purchaseInvoiceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pDCPayableToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem debitNoteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemCreationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipleItemCreationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemComponentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem brandToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modelNoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem unitToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem storeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rackToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem priceListToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pricingLevelToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem priceListToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem standardRateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem physicalStockToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stockJournalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem itemRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem physicalStockToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem stockJournalToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountLedgerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountGroupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem multipleAcctLedgerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chartOfAccountToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem currencyToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem currencyListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exchangeRateToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem budgetToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem budgetSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem budgetVarianceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contactUsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem expensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem bankTransfersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalJournalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financialStatementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trialBalanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profitAndLossToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem balanceSheetToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashFlowToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fundFlowToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem generalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vATReturnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalJournalsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transactionDetailsByLedgersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suppliersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aPAgeingSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierBalanceSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materialReceiptToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem rejectionOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pDCPayablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debitNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aRAgeingSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerBalanceSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesQuotationToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryNoteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem rejectionInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesReturnsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem receiptsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pDCReceivableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDCClearanceToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem creditNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stockJournalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportDetailsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stockVarianceReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inventoryMovementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountLedgersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierCustomerListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payrollToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem physicalStockReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inventoryStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dailyAttendanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem monthlyAttendanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dailySalaryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem monthlySalaryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem payheadToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salaryPackageToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem advancePaymentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem bonusDeductionToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem employeeAddressBookToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem generalJournalsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem runGeneralJournalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mastersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem currencyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountLedgerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountGroupToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem multipleAcctLedgerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem chartOfAccountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem currencyListToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exchangeRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem counterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem budgetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem budgetSettingsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem budgetVarianceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountCodesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountCodesRangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pOSToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profitAndLossByProjectAndCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oldProfitAndLossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oldBalanceSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReport2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pOSToolStripMenuItem2;
        private ucQuickLaunch ucQuickLaunch1;
        private ucCalculator ucCalculator1;
        private System.Windows.Forms.ToolStripMenuItem shiftReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pLByCostOfSalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pendingSalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountListToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transactionUnitReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockDetainsReportNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registersToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem employeeRegisterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem advanceRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bonusDeductionRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalaryRegisterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dailySalaryRegisterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bankToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transfersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashBankBookToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem chequeCashDetailsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bankReconciliationToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salaryPackageRegisterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteCompanyToolStripMenuItem;
    }
}



