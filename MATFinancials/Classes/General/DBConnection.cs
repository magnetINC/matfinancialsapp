using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.ServiceProcess;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Diagnostics;
using System.Configuration;

//<summary>    
//Summary description for DBConnection    
//</summary>    
namespace MATFinancials
{
    public class DBConnection
    {
        protected SqlConnection sqlcon;
        protected string serverName = (ConfigurationManager.AppSettings["MsSqlServer"] == null || ConfigurationManager.AppSettings["MsSqlServer"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlServer"].ToString();
        protected string userId = (ConfigurationManager.AppSettings["MsSqlUserId"] == null || ConfigurationManager.AppSettings["MsSqlUserId"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlUserId"].ToString();
        protected string password = (ConfigurationManager.AppSettings["MsSqlPassword"] == null || ConfigurationManager.AppSettings["MsSqlPassword"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlPassword"].ToString();
        protected string ApplicationPath = (ConfigurationManager.AppSettings["ApplicationPath"] == null || ConfigurationManager.AppSettings["ApplicationPath"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["ApplicationPath"].ToString();
        //protected string ApplicationPath = (ConfigurationManager.AppSettings["magnetConnectionString"] == null || ConfigurationManager.AppSettings["magnetConnectionString"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["magnetConnectionString"].ToString();
        protected string isSqlServer = (ConfigurationManager.AppSettings["isServerConnection"] == null || ConfigurationManager.AppSettings["isServerConnection"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["isServerConnection"].ToString();
        public DBConnection()
        {
            SqlConnection.ClearAllPools();
            string path = string.Empty;
            if (PublicVariables._decCurrentCompanyId > 0)
            {
                path = ApplicationPath + "\\Data\\" + PublicVariables._decCurrentCompanyId + "\\DBMATAccounting.mdf";
                sqlcon = new SqlConnection(@"Data Source=.\SQLEXPRESS; AttachDbFilename=" + path + "; Integrated Security = true; Connect Timeout = 120; User Instance=true");
            }
            else if (PublicVariables._decCurrentCompanyId == 0)
            {
                path = ApplicationPath + "\\Data\\DBMATAccounting.mdf";
                //TODO: test for first login
                sqlcon = new SqlConnection();
                //sqlcon.ConnectionString = @"Data Source=S45-40-135-165;Initial Catalog=DBMATAccounting_Magnet; user id=sa; Password=sa@12"; 
                sqlcon.ConnectionString = @"Data Source=iii;Initial Catalog=DBMATAccounting_USL; user id=sa; Password=sa@12";
                //sqlcon.ConnectionString = @"Data Source=.;Initial Catalog=DBMATAccounting_Magnet; integrated security=True;MultipleActiveResultSets=True";

                //sqlcon.ConnectionString = @"Data Source=.;Initial Catalog=DBMATAccounting_USL;integrated security=True;MultipleActiveResultSets=True";
                //connection string="data source=.;initial catalog=DBMATAccounting_Magnet;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework
                //end
            }
            else
            {
                path = ApplicationPath + "\\Data\\COMP\\DBMATAccounting.mdf";
            }

            if (serverName != null)
            {
                if (isSqlServer != null)
                {
                    if (userId == null || password == null)
                    {
                        //1.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";Integrated Security=True;Connect Timeout=120");

                    }
                    else if (Convert.ToBoolean(isSqlServer))
                    {
                        sqlcon = new SqlConnection(this.MagnetConnectionString);//Felix 20161021 For server connection
                    }
                    else
                    {
                        //2.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=120");
                        // attach dbfile here

                    }
                }
                else
                {
                    if (userId == null || password == null)
                    {
                        //3.sqlcon = new SqlConnection(this.MagnetConnectionString);   // goes through with this connection string
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";Integrated Security=True;Connect Timeout=120;User Instance=True");

                    }
                    else
                    {
                        //4.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + "; Connect Timeout=120; User Instance=False");

                    }
                }
                try
                {
                    sqlcon.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        public string MagnetConnectionString
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["magnetConnectionString"]);
            }
        }

        public string MagnetConnectionString2
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["magnetConnectionString2"]);
            }
        }
    }
}
