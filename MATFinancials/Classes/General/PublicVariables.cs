﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
namespace MATFinancials
{
    class PublicVariables
    {
        public static decimal _decCurrentUserId = 1;
        public static decimal _decCurrentCompanyId = 0;
        public static DateTime _dtCurrentDate;
        public static DateTime _dtFromDate;//financial year starting    
        public static DateTime _dtToDate;//financial year ending         
        public static decimal _decCurrentFinancialYearId = 1;
        public static bool isMessageAdd = true;
        public static bool isMessageEdit = true;
        public static bool isMessageDelete = true;
        public static bool isMessageClose = true;
        public static decimal _decCurrencyId = 1;
        public static int _inNoOfDecimalPlaces = 2;
        public static string MessageToShow = string.Empty;
        public static string MessageHeadear = string.Empty;
        public static decimal PartyBalance = 0;
        public static DateTime paymentDate = _dtCurrentDate;
        public static decimal productId = 0;      

        // defined by precious 20/07/2016 to set default colour for skin
        public static Color myBackColour()
        {
            //return Color.DarkOliveGreen;
            //return Color.FromArgb(170, 184, 131);
            return Color.White;//.Gray;// (170, 184, 131);
        }
        public static Color myForeColour()
        {
            //return Color.WhiteSmoke;
            return Color.Black;//.White;
        }
    }
}
