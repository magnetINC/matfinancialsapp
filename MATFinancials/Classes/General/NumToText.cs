﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MATFinancials
{
    public class NumToText
    {
        /// <summary>
        /// Function to return amount in words
        /// </summary>
        /// <param name="decAmount"></param>
        /// <param name="decCurrId"></param>
        /// <returns></returns>
        public string AmountWords(decimal decAmount, decimal decCurrId)
        {
            string AountInWords = string.Empty; // To return the amount in words
            CurrencyInfo currencyInfo = new CurrencySP().CurrencyView(decCurrId);
            decAmount = Math.Round(decAmount, currencyInfo.NoOfDecimalPlaces); //Rounding according to decimal places of currency
            string strAmount = decAmount.ToString(); // Just keeping the whole amount as string for performing split operation on it
            string strAmountinwordsOfIntiger = string.Empty;  // To hold amount in words of intiger
            string strAmountInWordsOfDecimal = string.Empty; // To hold amoutn in words of decimal part
            string[] strPartsArray = strAmount.Split('.'); // Splitting with "." to get intiger part and decimal part seperately
            string strDecimaPart = string.Empty;                     // To hold decimal part
            if (strPartsArray.Length > 1)
                if (strPartsArray[1] != null)
                    strDecimaPart = strPartsArray[1]; // Holding decimal portion if any
            if (strPartsArray[0] != null)
                strAmount = strPartsArray[0];    // Holding intiger part of amount
            else
                strAmount = string.Empty; ;
            if (strAmount.Trim() != string.Empty && decimal.Parse(strAmount) != 0)
                //strAmountinwordsOfIntiger = NumberToText(long.Parse(strAmount));
                strAmountinwordsOfIntiger = NumberToText2(long.Parse(strAmount));
            if (strDecimaPart.Trim() != string.Empty && decimal.Parse(strDecimaPart) != 0)
                //strAmountInWordsOfDecimal = NumberToText(long.Parse(strDecimaPart));
                strAmountInWordsOfDecimal = NumberToText2(long.Parse(strDecimaPart));
            SettingsSP spSettings = new SettingsSP();
            if (spSettings.SettingsStatusCheck("ShowCurrencySymbol") != "Yes")
            {
                // Showing currency as suffix
                if (strAmountinwordsOfIntiger != string.Empty)
                    AountInWords = strAmountinwordsOfIntiger + " " + currencyInfo.CurrencyName;
                if (strAmountInWordsOfDecimal != string.Empty)
                    AountInWords = AountInWords + " and " + strAmountInWordsOfDecimal + " " + currencyInfo.SubunitName;
                AountInWords = AountInWords + " only";
            }
            else
            {
                // Showing currency as prefix
                if (strAmountinwordsOfIntiger != string.Empty)
                    AountInWords = /*currencyInfo.CurrencyName + " " +*/ strAmountinwordsOfIntiger + " " + currencyInfo.CurrencyName;
                if (strAmountInWordsOfDecimal != string.Empty)
                    AountInWords = AountInWords + " and " + strAmountInWordsOfDecimal + " " + currencyInfo.SubunitName;
                AountInWords = AountInWords + " only";
            }
            return AountInWords;
        }
        /// <summary>
        /// Function to return number in text
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string NumberToText(int number)
        {
            // Converting the number to words //old
            if (number == 0) return "Zero";
            if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";
            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units 
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands 
            num[3] = number / 10000000; // crores 
            num[2] = num[2] - 100 * num[3]; // lakhs 
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10; // ones 
                t = num[i] / 10;
                h = num[i] / 100; // hundreds 
                t = t - 10 * h; // tens 
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append(" ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }
        /// <summary>
        /// Function to return number in text
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string NumberToText(long number)
        {
            // Converting the number to words //old
            if (number == 0) return "Zero";
            if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";
            long[] num = new long[4];
            long first = 0;
            long u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            //string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            string[] words3 = { "Thousand ", "Hundred ", "Million " };
            num[0] = number % 1000; // units 
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands 
            num[3] = number / 1000000; // Millions
            num[2] = num[2] - 10 * num[3]; // Hundreds 

            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (long i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10; // ones 
                t = num[i] / 10;
                h = num[i] / 100; // hundreds 
                t = t - 10 * h; // tens 
                try
                {
                    if (h > 0) sb.Append(words0[h] + "Hundred ");
                }
                catch
                {
                    //index out of range
                }
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append(" ");//if (h > 0 || i == 0) sb.Append(" and");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }
        public static string NumberToText2(long number)
        {
            // Converting the number to words // New 20170327
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToText2(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToText2(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToText2(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToText2(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }
            return words;
        }
    }
}
