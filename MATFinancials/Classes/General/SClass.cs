﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using Microsoft.Win32;
using System.Configuration;
using System.IO;

namespace MATFinancials
{
    class GetConnection : DBConnection
    {
        internal string GetCurrentConnctionString()
        {
            string ss = sqlcon.ConnectionString;
            if (ss.Contains("user id=\'"))
            {
                ss = ss  + ";password='" + password + "'";
            }
            return ss;
        }
    }

    public class SClass
    {
        public string MagnetConnectionString
        {
            get
            {
                var Rq = Convert.ToString(ConfigurationManager.AppSettings["magnetConnectionString"]);
                return Rq;
            }
        }
        public bool CheckFilesExists(string FileNameWithFullPath)
        {
            if (File.Exists(FileNameWithFullPath))
            {
                return true;
            }
            return false;
        }
        public bool CheckMsSqlConnection(string serverName, string userId, string password, string ApplicationPath, bool isServerConnection = false)
        {
            //Felix Logging 20170125
            GlobalObject.WriteLine(GlobalObject.LogType.Information, string.Empty, "======= Started sql connection =========");
            GlobalObject.WriteLine(GlobalObject.LogType.Information, "at CheckMsSqlConnection()", "\r\n" + string.Format("Input Parameters \r\n\tServer Name: {0}\r\n\tUser ID: {1}\r\n\tPassword: {2}\r\n\tIs ServerConnection: {3} \r\n\tApplication Path: {4}", serverName, userId, password, isServerConnection.ToString(), ApplicationPath));

            bool isTrue = false;
            SqlConnection sqlcon;
            if (serverName != null)
            {
                if (userId == null || password == null)
                {
                    //start pravin
                    sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + ApplicationPath + "\\Data\\DBMATAccounting.mdf" + ";Integrated Security=True;Connect Timeout=30;User Instance=True");
                    //end pravin
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, "sqlcon 7", sqlcon.ConnectionString);
                }
                else if (isServerConnection)
                {
                    sqlcon = new SqlConnection(this.MagnetConnectionString);//Felix 20161021 For server connection
                }
                else
                {
                    //8.   sqlcon = new SqlConnection(this.MagnetConnectionString);
                    sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + ApplicationPath + "\\Data\\DBMATAccounting.mdf" + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=30; User Instance=False");
                    // attach dbfile here
                    GlobalObject.WriteLine(GlobalObject.LogType.Information, "sqlcon 8", sqlcon.ConnectionString);
                }
                try
                {
                    sqlcon.Open();
                    isTrue = true;
                }

                //-------modified by me 08/07/2016
                catch (SqlException ex)
                {
                    GlobalObject.WriteLine(GlobalObject.LogType.Error, "CheckMsSqlConnection First Exception (ex)", "\n\r" + ex.StackTrace);

                    if (ex.Number == 18493)
                    {
                        if (userId == null || password == null)
                        {
                            //9.  sqlcon = new SqlConnection(this.MagnetConnectionString);
                            sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + ApplicationPath + "\\Data\\DBMATAccounting.mdf" + ";Integrated Security=True;Connect Timeout=30");
                            GlobalObject.WriteLine(GlobalObject.LogType.Information, "sqlcon 9", sqlcon.ConnectionString);
                        }
                        else
                        {
                            //10.  sqlcon = new SqlConnection(this.MagnetConnectionString);
                            sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + ApplicationPath + "\\Data\\DBMATAccounting.mdf" + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=30");
                            GlobalObject.WriteLine(GlobalObject.LogType.Information, "sqlcon 10", sqlcon.ConnectionString);
                        }
                        try
                        {
                            sqlcon.Open();
                            UpdateAppConfig("isServerConnection", "true");
                            isTrue = true;
                        }
                        catch (SqlException exa)
                        {
                            GlobalObject.WriteLine(GlobalObject.LogType.Error, "CheckMsSqlConnection Second Exception (exa)", "\n\r" + exa.StackTrace);

                            MessageBox.Show(exa.Message);
                            isTrue = false;

                        }
                        finally
                        {
                            if (sqlcon.State == System.Data.ConnectionState.Open)
                            {
                                sqlcon.Close();
                            }
                        }
                    }
                    else
                    {
                        GlobalObject.WriteLine(GlobalObject.LogType.Error, "CheckMsSqlConnection First Exception Message (ex)", "\n\r" + ex.Message);
                        MessageBox.Show(ex.Message);
                        isTrue = false;
                    }
                }
                finally
                {
                    if (sqlcon.State == System.Data.ConnectionState.Open)
                    {
                        sqlcon.Close();
                    }
                }
            }
            return isTrue;
        }
        public DataTable GetLocalInstance()
        {
            DataTable LocalInstanceNames = new DataTable();
            LocalInstanceNames.Columns.Add("Server", typeof(string));
            LocalInstanceNames.Columns.Add("Instance", typeof(string));
            RegistryView registryView = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
            using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView))
            {
                RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);
                if (instanceKey != null)
                {
                    foreach (string instanceName in instanceKey.GetValueNames())
                    {
                        LocalInstanceNames.Rows.Add(Environment.MachineName, instanceName);
                    }
                }
                else
                {
                    LocalInstanceNames.Rows.Add(Environment.MachineName, "");
                }
            }
            return LocalInstanceNames;
        }

        public void UpdateAppConfig(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch { }

        }

        internal DataTable GetOmPath(string serverName, string userId, string password)
        {
            //-------modified by me 08/07/2016
            SqlConnection sqlcon;
            DataTable dtbl;
            if (serverName != null)
            {
                if (userId == null || password == null)
                {
                 //11.   sqlcon = new SqlConnection(this.MagnetConnectionString);
                    sqlcon = new SqlConnection(@"Data Source=" + serverName + ";Integrated Security=True;Connect Timeout=30;User Instance=True");
                }
                else
                {
                  //12.  sqlcon = new SqlConnection(this.MagnetConnectionString);
                    sqlcon = new SqlConnection(@"Data Source=" + serverName + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=30; User Instance=False");
                }
                try
                {
                    // this place connects to a db on a physical path. havent modified it yet.
                    sqlcon.Open();
                    SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT name, cast(replace(physical_name, '\\Data\\DBMATFinancials.mdf', '') as varchar(max)) AS [location] FROM sys.master_files WHERE physical_name like '%'+'\\Data\\DBMATFinancials.mdf'", sqlcon);
                    sqlDa.Fill(dtbl = new DataTable());
                    if (dtbl.Rows.Count == 0 && (serverName.Split('\\'))[0].ToString() == Environment.MachineName)
                    {
                        dtbl.Rows.Add("MyPath", Application.StartupPath);
                    }
                    return dtbl;
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 18493)
                    {
                        if (userId == null || password == null)
                        {
                            // modified by me ------14/07/2016
                           //13. sqlcon = new SqlConnection(this.MagnetConnectionString);
                           sqlcon = new SqlConnection(@"Data Source=" + serverName + ";Integrated Security=True;Connect Timeout=30");
                        }
                        else
                        {
                          //14.  sqlcon = new SqlConnection(this.MagnetConnectionString);
                            sqlcon = new SqlConnection(@"Data Source=" + serverName + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=30");
                        }
                        try
                        {
                            sqlcon.Open();
                            SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT name, cast(replace(physical_name, '\\Data\\DBMATFinancials.mdf', '') as varchar(max)) AS [location] FROM sys.master_files WHERE physical_name like '%'+'\\Data\\DBMATFinancials.mdf'", sqlcon);
                            sqlDa.Fill(dtbl = new DataTable());
                            if (dtbl.Rows.Count == 0 && (serverName.Split('\\'))[0].ToString() == Environment.MachineName)
                            {
                                dtbl.Rows.Add("MyPath", Application.StartupPath);
                            }
                            return dtbl;
                        }
                        catch (SqlException exa)
                        {

                            MessageBox.Show(exa.Message);

                        }
                        finally
                        {
                            if (sqlcon.State == System.Data.ConnectionState.Open)
                            {
                                sqlcon.Close();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                finally
                {
                    if (sqlcon.State == System.Data.ConnectionState.Open)
                    {
                        sqlcon.Close();
                    }
                }

            }
            return dtbl = new DataTable();
        }
    }
}
