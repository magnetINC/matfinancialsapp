﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace MATFinancials
{
    public class StatutoryComplianceSP : DBConnection
    {
        DataSet ds;
        SqlCommand comm;
        SqlDataAdapter da;
        public StatutoryComplianceSP()
        {
            ds = new DataSet();
            comm = new SqlCommand();
            da = new SqlDataAdapter();
        }
        public bool StatutoryComplianceAdd(StatutoryComplianceInfo StatutoryComplianceInfo)
        {
            int RowsAffected = 0;
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_StatutoryCompliance_Insert", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@IsPAYEApplicable", SqlDbType.Bit).Value = StatutoryComplianceInfo.IsPAYEApplicable;
                comm.Parameters.Add("@IsPensionApplicable", SqlDbType.Bit).Value = StatutoryComplianceInfo.IsPensionApplicable;
                comm.Parameters.Add("@SalaryPackageID", SqlDbType.Int).Value = StatutoryComplianceInfo.SalaryPackageID;
                comm.Parameters.Add("@PAYE", SqlDbType.Decimal).Value = StatutoryComplianceInfo.PAYE;
                comm.Parameters.Add("@Pension", SqlDbType.Decimal).Value = StatutoryComplianceInfo.Pension;
                comm.Parameters.Add("@PAYEReceivingLedgerId", SqlDbType.Int).Value = StatutoryComplianceInfo.PAYEReceivingLedgerId;
                comm.Parameters.Add("@PensionReceivingLedgerId", SqlDbType.Int).Value = StatutoryComplianceInfo.PensionReceivingLedgerId;
                RowsAffected = comm.ExecuteNonQuery();
            }
            catch(Exception ex) { }
            finally { sqlcon.Close(); }
            if(RowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool StatutoryComplianceUpdate(StatutoryComplianceInfo StatutoryComplianceInfo)
        {
            int RowsAffected = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_StatutoryCompliance_Update", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add("@IsPAYEApplicable", SqlDbType.Bit).Value = StatutoryComplianceInfo.IsPAYEApplicable;
                comm.Parameters.Add("@IsPensionApplicable", SqlDbType.Bit).Value = StatutoryComplianceInfo.IsPensionApplicable;
                comm.Parameters.Add("@SalaryPackageID", SqlDbType.Int).Value = StatutoryComplianceInfo.SalaryPackageID;
                comm.Parameters.Add("@PAYE", SqlDbType.Decimal).Value = StatutoryComplianceInfo.PAYE;
                comm.Parameters.Add("@Pension", SqlDbType.Decimal).Value = StatutoryComplianceInfo.Pension;
                comm.Parameters.Add("@PAYEReceivingLedgerId", SqlDbType.Int).Value = StatutoryComplianceInfo.PAYEReceivingLedgerId;
                comm.Parameters.Add("@PensionReceivingLedgerId", SqlDbType.Int).Value = StatutoryComplianceInfo.PensionReceivingLedgerId;
                RowsAffected = comm.ExecuteNonQuery();
            }
            catch (Exception ex) { }
            finally { sqlcon.Close(); }
            if (RowsAffected > 0)
            {
                return true;
            }
            return false;
        }
        public DataSet StatutoryComplianceSelect()
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_StatutoryCompliance_Select", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
            }
            catch (Exception ex) { }
            finally { sqlcon.Close(); }
            return ds;
        }
    }
}
