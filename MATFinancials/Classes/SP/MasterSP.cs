﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
namespace MATFinancials
{
    class MasterSP:DBConnection
    {
        #region Function
        /// <summary>
        /// Function to get DotMatrxPrinter Format ComboFill For VoucherType
        /// </summary>
        /// <returns></returns>
        public DataTable DotMatrxPrinterFormatComboFillForVoucherType()
        {
            DataTable dtbl = new DataTable();
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("DotMatrxPrinterFormatComboFillForVoucherType", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.Message);
            }
            return dtbl;
        }
        #endregion
    }
}
