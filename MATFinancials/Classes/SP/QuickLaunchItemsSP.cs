﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace MATFinancials
{
    class QuickLaunchItemsSP:DBConnection
    {
        #region Function
        /// <summary>
        /// Function to get all the Non Selected  values from tbl_QuickLaunchItems Table
        /// </summary>
        /// <param name="isSelected"></param>
        /// <returns></returns>
        public DataTable QuickLaunchNonSelectedViewAll(bool isSelected)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("QuickLaunchNonSelectedViewAll", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.Add("@selected", SqlDbType.Bit).Value = isSelected;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to Update values in tbl_QuickLaunchItems Table
        /// </summary>
        /// <param name="infoQuickLaunchItems"></param>
        public void QuickLaunchItemsEdit(QuickLaunchItemsInfo infoQuickLaunchItems)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("QuickLaunchItemsEdit", sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@quickLaunchItemsId", SqlDbType.Decimal).Value = infoQuickLaunchItems.QuickLaunchItemsId;
                sqlcmd.Parameters.Add("@itemsName", SqlDbType.VarChar).Value = infoQuickLaunchItems.ItemsName;
                sqlcmd.Parameters.Add("@status", SqlDbType.Bit).Value = infoQuickLaunchItems.Status;
                sqlcmd.Parameters.Add("@extra1", SqlDbType.VarChar).Value = infoQuickLaunchItems.Extra1;
                sqlcmd.Parameters.Add("@extra2", SqlDbType.VarChar).Value = infoQuickLaunchItems.Extra2;
                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
        }
        #endregion
    }
}
