﻿using MATFinancials.Classes.Info;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.SP
{
    public class ProjectSP : DBConnection
    {
        SqlCommand comm;
        SqlParameter param;
        DataTable dt;
        DataSet ds;
        SqlDataAdapter da;
        int RowsAffected = 0;
        bool IsTransactionSuccessfull = false;
        public ProjectSP()
        {

        }
        public bool ProjectAdd(ProjectInfo ProjectInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Project_Insert", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                //param = new SqlParameter();
                comm.Parameters.AddWithValue("@ProjectName", ProjectInfo.ProjectName);
                comm.Parameters.AddWithValue("@CreatedBy", ProjectInfo.CreatedBy);
                comm.Parameters.AddWithValue("@CreatedOn", ProjectInfo.CreatedOn);
                comm.Parameters.AddWithValue("@ModifiedBy", ProjectInfo.ModifiedBy);
                comm.Parameters.AddWithValue("@ModifiedOn", ProjectInfo.ModifiedOn);
                RowsAffected = comm.ExecuteNonQuery();
                if (RowsAffected < 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public bool ProjectUpdate(ProjectInfo ProjectInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Project_Update", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("ProjectId", ProjectInfo.ProjectId);
                comm.Parameters.AddWithValue("ProjectName", ProjectInfo.ProjectName);
                comm.Parameters.AddWithValue("@CreatedBy", ProjectInfo.CreatedBy);
                comm.Parameters.AddWithValue("@CreatedOn", ProjectInfo.CreatedOn);
                comm.Parameters.AddWithValue("@ModifiedBy", ProjectInfo.ModifiedBy);
                comm.Parameters.AddWithValue("@ModifiedOn", ProjectInfo.ModifiedOn);
                RowsAffected = comm.ExecuteNonQuery();
                if (RowsAffected > 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public bool ProjectDelete(ProjectInfo ProjectInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Project_Delete", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("ProjectId", ProjectInfo.ProjectId);
                RowsAffected = comm.ExecuteNonQuery();
                if (RowsAffected > 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public DataTable ProjectView(ProjectInfo ProjectInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                da = new SqlDataAdapter("SP_Project_View", sqlcon);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }

        public DataTable ProjectViewAll()
        {
            try
            {
                dt = new DataTable(); da = new SqlDataAdapter();
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                da = new SqlDataAdapter("SP_Project_ViewAll", sqlcon);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }
    }
}
