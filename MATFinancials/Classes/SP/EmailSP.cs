﻿using MATFinancials.Classes.Info;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Classes.SP
{
    class EmailSP: DBConnection
    {
        public void EmailConfigAdd(EmailInfo emailInfo)
        {
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("emailConfiguration", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = cmd.Parameters.Add("emailId", SqlDbType.NVarChar);
                param.Value = emailInfo.EmailId;
                param = cmd.Parameters.Add("logonPassword", SqlDbType.NVarChar);
                param.Value = emailInfo.LogonPassword;
                param = cmd.Parameters.Add("IsDefault", SqlDbType.NVarChar);
                param.Value = emailInfo.IsDefault;
                param = cmd.Parameters.Add("@smtp", SqlDbType.VarChar);
                param.Value = emailInfo.smtp;
                param = cmd.Parameters.Add("@portNumber", SqlDbType.VarChar);
                param.Value = emailInfo.portNumber;
                param = cmd.Parameters.Add("@enableSSL", SqlDbType.Bit);
                param.Value = emailInfo.enableSSL;

                cmd.ExecuteNonQuery();              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                sqlcon.Close();
            }
        }

        /// <summary>
        /// Call to Update Email
        /// </summary>
        /// <param name="emailInfo"></param>
        public void EmailConfigUpdate(EmailInfo emailInfo)
        {
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("EmailConfigEdit", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = cmd.Parameters.Add("emailId", SqlDbType.NVarChar);
                param.Value = emailInfo.EmailId;
                param = cmd.Parameters.Add("emailConfigId", SqlDbType.Decimal);
                param.Value = emailInfo.emailConfigurationId;
                param = cmd.Parameters.Add("IsDefault", SqlDbType.NVarChar);
                param.Value = emailInfo.IsDefault;
                param = cmd.Parameters.Add("@LogonPassword", SqlDbType.NVarChar);
                param.Value = emailInfo.LogonPassword;
                param = cmd.Parameters.Add("@smtp", SqlDbType.VarChar);
                param.Value = emailInfo.smtp;
                param = cmd.Parameters.Add("@portNumber", SqlDbType.VarChar);
                param.Value = emailInfo.portNumber;
                param = cmd.Parameters.Add("@enableSSL", SqlDbType.Bit);
                param.Value = emailInfo.enableSSL;
                cmd.ExecuteNonQuery();

            }catch(Exception ex)
            {
                string log = ex.Message;
            }
            finally
            {
                sqlcon.Close();
            }
        }

        public DataTable EmailConfigViewAll()
        {
            EmailInfo infoEmail = new EmailInfo();
            DataTable dtbl = new DataTable();
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }                
                SqlDataAdapter sqlda = new SqlDataAdapter("EmailViewAll", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                string log = ex.Message;
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        
        /// <summary>
        /// Function to view EmailAddress
        /// </summary>
        /// <param name="configID"></param>
        /// <returns></returns>

        public EmailInfo EmailConfigView(decimal configID)
        {
            EmailInfo infoEmail = new EmailInfo();
            SqlDataReader sdr = null;
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("EmailView", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = cmd.Parameters.Add("@emailConfigId", SqlDbType.Decimal);
                param.Value = configID;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    infoEmail.emailConfigurationId = int.Parse(sdr[0].ToString());
                    infoEmail.EmailId = sdr[1].ToString();
                    infoEmail.LogonPassword = sdr[2].ToString();
                    infoEmail.IsDefault = sdr[3].ToString();
                    infoEmail.smtp = sdr[4].ToString();
                    infoEmail.portNumber = sdr[5].ToString();
                    infoEmail.enableSSL = Convert.ToBoolean(sdr[6]);
                }
            }catch(Exception ex)
            {
                string log = ex.Message;
            }
            finally
            {
                sqlcon.Close();
                sdr.Close();
            }
            return infoEmail;
        }

        /// <summary>
        /// Function to delete an Email Address
        /// </summary>
        /// <param name="emailConfigID"></param>
        public void DeleteEmail(decimal emailConfigID)
        {
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("emailConfigDelete", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = cmd.Parameters.Add("@EmailConfigID", SqlDbType.Decimal);
                param.Value = emailConfigID;
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                string log = ex.Message;
            }
        }
        
    }
}
