﻿using MATFinancials.Classes.Info;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials
{
    class salesManTransactionDaySP : DBConnection
    {
        int rowAffected = 0;
        public bool salesManTransactionDayAdd(salesManTransactionDayInfo info)
        {
            bool isInserted = false;
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand comm = new SqlCommand("salesManTransactionDayAdd", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = comm.Parameters.AddWithValue("@dayCloseDate", info.dayCloseDate);
                param = comm.Parameters.AddWithValue("@dayOpenDate", info.dayOpenDate);
                param = comm.Parameters.AddWithValue("@Extra1", info.Extra1);
                param = comm.Parameters.AddWithValue("@Extra2", info.Extra2);
                param = comm.Parameters.AddWithValue("@ExtraDate", info.ExtraDate);
                param = comm.Parameters.AddWithValue("@salesManId", info.salesManId);
                param = comm.Parameters.AddWithValue("@transactionDay", info.transactionDay);
                rowAffected = comm.ExecuteNonQuery();
                if(rowAffected > 0)
                {
                    isInserted = true;
                }
            }
            catch (Exception ex)
            {

            }
            return isInserted;
        }
    }
}
