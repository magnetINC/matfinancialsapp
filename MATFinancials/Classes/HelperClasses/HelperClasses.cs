﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Other
{
    public class HelperClasses : DBConnection
    {
        #region public variables
        SqlCommand comm;
        SqlParameter param;
        SqlDataAdapter da;
        DataSet ds;
        SqlTransaction transaction;
        int rowAffected = 0;
        #endregion
        public HelperClasses()
        {
            comm = new SqlCommand();
            param = new SqlParameter();
            da = new SqlDataAdapter();
            ds = new DataSet();
        }

        public string returnValue;
        public DateTime returnedDate;
        public string VoucherNo;
        public decimal id;

        public string GetReturnValue(string CommStr)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                // string CommStr = "select * from tbl_"
                SqlCommand Comm = new SqlCommand(CommStr, sqlcon);
                returnValue = Convert.ToString(Comm.ExecuteScalar());
            }
            catch (Exception ex) { }
            return returnValue;
        }
        public decimal CreditNoteMasterId(string voucherNo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string comStr = string.Format("SELECT creditNoteMasterId FROM tbl_CreditNoteMaster WHERE " + 
                    "voucherNo = '{0}' ", voucherNo.ToString());
                SqlCommand Comm = new SqlCommand(comStr, sqlcon);
                SqlDataAdapter sdaadapter = new SqlDataAdapter();
                id = Convert.ToInt32(Comm.ExecuteScalar());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return id;
        }
        public decimal GetMasterId(string comStr)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand Comm = new SqlCommand(comStr, sqlcon);
                SqlDataAdapter sdaadapter = new SqlDataAdapter();
                id = Convert.ToInt32(Comm.ExecuteScalar());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return id;
        }
        public decimal GetCurrentDay(decimal salesManId)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string comStr = string.Format("select top 1 transactionDay from tbl_salesManTransactionDay where salesmanId = {0} order by salesManTransactionDayId desc", salesManId);
                SqlCommand comm = new SqlCommand(comStr, sqlcon);
                id = Convert.ToDecimal(comm.ExecuteScalar());
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlcon.Close();
            }
            return id;
        }
        public DateTime GetOpenDay(decimal salesManId)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string comStr = string.Format("select top 1 dayCloseDate from tbl_salesManTransactionDay where salesmanId = {0} order by salesManTransactionDayId desc", salesManId);
                SqlCommand comm = new SqlCommand(comStr, sqlcon);
                returnedDate = Convert.ToDateTime(comm.ExecuteScalar());
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlcon.Close();
            }
            return returnedDate;
        }
        public decimal DebitNoteMasterId(string voucherNo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string comStr = string.Format("SELECT debitNoteMasterId FROM tbl_DebitNoteMaster WHERE " +
                    "voucherNo = '{0}' ", voucherNo.ToString());
                SqlCommand Comm = new SqlCommand(comStr, sqlcon);
                SqlDataAdapter sdaadapter = new SqlDataAdapter();
                id = Convert.ToInt32(Comm.ExecuteScalar());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return id;
        }
        public string GetVoucherNo(string CommStr)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                // string CommStr = "select * from tbl_"
                SqlCommand Comm = new SqlCommand(CommStr, sqlcon);
                VoucherNo = Convert.ToString(Comm.ExecuteScalar());
            }
            catch (Exception ex) { }
            return VoucherNo;
        }
        public DataSet GetDataSet(string queryStr)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand(queryStr, sqlcon);
                comm.CommandType = CommandType.Text;
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sqlcon.Close();
            }
            return ds;
        }
        public bool ExecuteNonQuery(string queryStr)
        {
            bool isQueryExecuted = false;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }

                SqlCommand Comm = new SqlCommand(queryStr, sqlcon);
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = queryStr;
                rowAffected = Comm.ExecuteNonQuery();
            }
            catch (Exception ex) { }
            if (rowAffected > 0)
            {
                isQueryExecuted = true;
            }
            return isQueryExecuted;
        }
    }
}
