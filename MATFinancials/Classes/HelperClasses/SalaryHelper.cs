﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.HelperClasses
{
    public class SalaryHelper
    {
        public static DataTable LedgerPostingMirrorTable()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("date", typeof(DateTime));
                dt.Columns.Add("voucherTypeId", typeof(decimal));
                dt.Columns.Add("voucherNo", typeof(string));
                dt.Columns.Add("ledgerId", typeof(decimal));
                dt.Columns.Add("debit", typeof(decimal));
                dt.Columns.Add("credit", typeof(decimal));
                dt.Columns.Add("detailsId", typeof(decimal));
                dt.Columns.Add("yearId", typeof(decimal));
                dt.Columns.Add("invoiceNo", typeof(string));
                dt.Columns.Add("chequeNo", typeof(string));
                dt.Columns.Add("chequeDate", typeof(DateTime));
                dt.Columns.Add("extraDate", typeof(DateTime));
                dt.Columns.Add("extra1", typeof(string));
                dt.Columns.Add("extra2", typeof(string));
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable SalaryVoucherDetailsMirrorTable()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("salaryVoucherMasterId", typeof(decimal));
                dt.Columns.Add("employeeId", typeof(decimal));
                dt.Columns.Add("bonus", typeof(decimal));
                dt.Columns.Add("deduction", typeof(decimal));
                dt.Columns.Add("advance", typeof(decimal));
                dt.Columns.Add("lop", typeof(decimal));
                dt.Columns.Add("PAYE", typeof(decimal));
                dt.Columns.Add("Pension", typeof(decimal));
                dt.Columns.Add("salary", typeof(decimal));
                dt.Columns.Add("status", typeof(string));
                dt.Columns.Add("extraDate", typeof(DateTime));
                dt.Columns.Add("extra1", typeof(string));
                dt.Columns.Add("extra2", typeof(string));
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable salesOrderDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("salesOrderMasterId", typeof(decimal));
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        public static DataTable purchaseOrderDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("purchaseOrderMasterId", typeof(decimal));
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
    }
}
