﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.HelperClasses
{
    public class GeneralClasses
    {
    }
    public class invoiceEmailDetails
    {
        public string mailTo { get; set; }
        public string MailFrom { get; set; }
        public string MailPassword { get; set; }
        public string smtpServer { get; set; }
        public string message { get; set; }
        public string mailTitle { get; set; }
        public string attachment { get; set; }
    }
}
