﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancials
{
    public class StatutoryComplianceInfo
    {
        private bool _IsPAYEApplicable;
        private bool _IsPensionApplicable;
        private int _SalaryPackageID;
        private decimal _PAYE;
        private decimal _Pension;
        private int _PAYEReceivingLedgerId;
        private int _PensionReceivingLedgerId;

        public bool IsPAYEApplicable
        {
            get { return _IsPAYEApplicable; }
            set { _IsPAYEApplicable = value; }
        }
        public bool IsPensionApplicable
        {
            get { return _IsPensionApplicable; }
            set { _IsPensionApplicable = value; }
        }
        public int SalaryPackageID
        {
            get { return _SalaryPackageID; }
            set { _SalaryPackageID = value; }
        }
        public decimal PAYE
        {
            get { return _PAYE; }
            set { _PAYE = value; }
        }
        public decimal Pension
        {
            get { return _Pension; }
            set { _Pension = value; }
        }
        public int PAYEReceivingLedgerId
        {
            get { return _PAYEReceivingLedgerId; }
            set { _PAYEReceivingLedgerId = value; }
        }
        public int PensionReceivingLedgerId
        {
            get { return _PensionReceivingLedgerId; }
            set { _PensionReceivingLedgerId = value; }
        }

    }
}
