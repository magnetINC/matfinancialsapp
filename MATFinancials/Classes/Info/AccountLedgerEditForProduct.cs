﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials
{
    class AccountLedgerEditForProduct
    {
       public decimal productId { get; set; }
       public decimal expenseAccount { get; set; }
       public decimal newLedgerId { get; set; }
    }
}
