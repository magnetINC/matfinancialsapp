


using System;
using System.Collections.Generic;
using System.Text;
//<summary>    
//Summary description for PaymentDetailsInfo    
//</summary>    
namespace MATFinancials
{
    public class PaymentDetailsInfo
    {
        private decimal _paymentDetailsId;
        private decimal _paymentMasterId;
        private decimal _ledgerId;
        private decimal _amount;
        private decimal _exchangeRateId;
        private string _chequeNo;
        private DateTime _chequeDate;
        private DateTime _extraDate;
        private string _extra1;
        private string _extra2;
        private int _ProjectId;
        private int _CategoryId;
        private string _memo;
        private decimal _withholdingTaxId;
        private decimal _withholdingTaxAmount;
        private decimal _grossAmount;

        public decimal PaymentDetailsId
        {
            get { return _paymentDetailsId; }
            set { _paymentDetailsId = value; }
        }
        public decimal PaymentMasterId
        {
            get { return _paymentMasterId; }
            set { _paymentMasterId = value; }
        }
        public decimal LedgerId
        {
            get { return _ledgerId; }
            set { _ledgerId = value; }
        }
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        public decimal ExchangeRateId
        {
            get { return _exchangeRateId; }
            set { _exchangeRateId = value; }
        }
        public string ChequeNo
        {
            get { return _chequeNo; }
            set { _chequeNo = value; }
        }
        public DateTime ChequeDate
        {
            get { return _chequeDate; }
            set { _chequeDate = value; }
        }
        public DateTime ExtraDate
        {
            get { return _extraDate; }
            set { _extraDate = value; }
        }
        public string Extra1
        {
            get { return _extra1; }
            set { _extra1 = value; }
        }
        public string Extra2
        {
            get { return _extra2; }
            set { _extra2 = value; }
        }
        public int ProjectId
        {
            get { return _ProjectId; }
            set { _ProjectId = value; }
        }
        public int CategoryId
        {
            get { return _CategoryId; }
            set { _CategoryId = value; }
        }
        public string Memo
        {
            get { return _memo; }
            set { _memo = value; }
        }
        public  decimal withholdingTaxId
        {
            get { return _withholdingTaxId; }
            set { _withholdingTaxId = value; }
        }
        public decimal withholdingTaxAmount
        {
            get { return _withholdingTaxAmount; }
            set { _withholdingTaxAmount = value; }
        }
        public decimal grossAmount
        {
            get { return _grossAmount; }
            set { _grossAmount = value; }
        }
    }
}
