﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials
{
    class salesManTransactionDayInfo
    {
        private decimal _transactionDay;
        private decimal _salesManId;
        private DateTime _dayOpenDate;
        private DateTime? _dayCloseDate;
        private DateTime _extraDate;
        private string _extra1;
        private string _extra2;

        public decimal transactionDay
        {
            get { return _transactionDay; }
            set { _transactionDay = value; }
        }
        public decimal salesManId
        {
            get { return _salesManId; }
            set { _salesManId = value; }
        }
        public DateTime dayOpenDate
        {
            get { return _dayOpenDate; }
            set { _dayOpenDate = value; }
        }
        public DateTime? dayCloseDate
        {
            get { return _dayCloseDate; }
            set { _dayCloseDate = value; }
        }
        public DateTime ExtraDate
        {
            get { return _extraDate; }
            set { _extraDate = value; }
        }
        public string Extra1
        {
            get { return _extra1; }
            set { _extra1 = value; }
        }
        public string Extra2
        {
            get { return _extra2; }
            set { _extra2 = value; }
        }
    }
}
