﻿namespace MATFinancials.FinancialStatements
{
    partial class frmTrialBalance2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTrialBalance2));
            this.txtTodate = new System.Windows.Forms.TextBox();
            this.ddtpTrialToDate = new System.Windows.Forms.DateTimePicker();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpTrialFromDate = new System.Windows.Forms.DateTimePicker();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnprint = new System.Windows.Forms.Button();
            this.dgvTrailBalance = new System.Windows.Forms.DataGridView();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbldebitSum = new System.Windows.Forms.Label();
            this.lblcreditSum = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.accountGroupId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ledgerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.debitDifference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creditDifference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrailBalance)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTodate
            // 
            this.txtTodate.Location = new System.Drawing.Point(205, 23);
            this.txtTodate.Margin = new System.Windows.Forms.Padding(5);
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Size = new System.Drawing.Size(178, 22);
            this.txtTodate.TabIndex = 1171;
            this.txtTodate.Leave += new System.EventHandler(this.txtTodate_Leave);
            // 
            // ddtpTrialToDate
            // 
            this.ddtpTrialToDate.CustomFormat = "dd-MMM-yyyy";
            this.ddtpTrialToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ddtpTrialToDate.Location = new System.Drawing.Point(379, 23);
            this.ddtpTrialToDate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ddtpTrialToDate.Name = "ddtpTrialToDate";
            this.ddtpTrialToDate.Size = new System.Drawing.Size(22, 22);
            this.ddtpTrialToDate.TabIndex = 1172;
            this.ddtpTrialToDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(204, 24);
            this.txtFromDate.Margin = new System.Windows.Forms.Padding(5);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Size = new System.Drawing.Size(178, 22);
            this.txtFromDate.TabIndex = 1169;
            this.txtFromDate.Visible = false;
            this.txtFromDate.Leave += new System.EventHandler(this.txtFromDate_Leave);
            // 
            // dtpTrialFromDate
            // 
            this.dtpTrialFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTrialFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTrialFromDate.Location = new System.Drawing.Point(390, 24);
            this.dtpTrialFromDate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dtpTrialFromDate.Name = "dtpTrialFromDate";
            this.dtpTrialFromDate.Size = new System.Drawing.Size(12, 22);
            this.dtpTrialFromDate.TabIndex = 1170;
            this.dtpTrialFromDate.Visible = false;
            this.dtpTrialFromDate.ValueChanged += new System.EventHandler(this.dtpTrialFromDate_ValueChanged);
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.DimGray;
            this.btnclear.FlatAppearance.BorderSize = 0;
            this.btnclear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.ForeColor = System.Drawing.Color.Maroon;
            this.btnclear.Location = new System.Drawing.Point(824, 24);
            this.btnclear.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(85, 20);
            this.btnclear.TabIndex = 1168;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(738, 24);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 20);
            this.btnSearch.TabIndex = 1167;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(202, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 16);
            this.label3.TabIndex = 1166;
            this.label3.Text = "To date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(202, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 16);
            this.label2.TabIndex = 1165;
            this.label2.Text = "From dat";
            this.label2.Visible = false;
            // 
            // btnprint
            // 
            this.btnprint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnprint.FlatAppearance.BorderSize = 0;
            this.btnprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnprint.ForeColor = System.Drawing.Color.Black;
            this.btnprint.Location = new System.Drawing.Point(409, 473);
            this.btnprint.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(85, 27);
            this.btnprint.TabIndex = 1173;
            this.btnprint.Text = "Print";
            this.btnprint.UseVisualStyleBackColor = false;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // dgvTrailBalance
            // 
            this.dgvTrailBalance.AllowUserToAddRows = false;
            this.dgvTrailBalance.AllowUserToDeleteRows = false;
            this.dgvTrailBalance.AllowUserToResizeColumns = false;
            this.dgvTrailBalance.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvTrailBalance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTrailBalance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTrailBalance.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvTrailBalance.BackgroundColor = System.Drawing.Color.White;
            this.dgvTrailBalance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTrailBalance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTrailBalance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTrailBalance.ColumnHeadersHeight = 31;
            this.dgvTrailBalance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.accountGroupId,
            this.ledgerName,
            this.debitDifference,
            this.creditDifference});
            this.dgvTrailBalance.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvTrailBalance.EnableHeadersVisualStyles = false;
            this.dgvTrailBalance.GridColor = System.Drawing.Color.White;
            this.dgvTrailBalance.Location = new System.Drawing.Point(205, 51);
            this.dgvTrailBalance.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgvTrailBalance.Name = "dgvTrailBalance";
            this.dgvTrailBalance.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTrailBalance.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvTrailBalance.RowHeadersVisible = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvTrailBalance.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvTrailBalance.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dgvTrailBalance.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvTrailBalance.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvTrailBalance.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            this.dgvTrailBalance.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvTrailBalance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTrailBalance.Size = new System.Drawing.Size(714, 378);
            this.dgvTrailBalance.TabIndex = 1174;
            this.dgvTrailBalance.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTrailBalance_CellContentDoubleClick);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Cooper Black", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(200, 444);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(50, 14);
            this.lblTotal.TabIndex = 1175;
            this.lblTotal.Text = "TOTAL";
            this.lblTotal.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(712, 435);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 1176;
            this.label4.Text = "==========";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(811, 435);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 1177;
            this.label5.Text = "==========";
            this.label5.Visible = false;
            // 
            // lbldebitSum
            // 
            this.lbldebitSum.AutoSize = true;
            this.lbldebitSum.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldebitSum.Location = new System.Drawing.Point(725, 462);
            this.lbldebitSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldebitSum.Name = "lbldebitSum";
            this.lbldebitSum.Size = new System.Drawing.Size(76, 17);
            this.lbldebitSum.TabIndex = 1178;
            this.lbldebitSum.Text = "[debitsum]";
            this.lbldebitSum.Visible = false;
            // 
            // lblcreditSum
            // 
            this.lblcreditSum.AutoSize = true;
            this.lblcreditSum.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcreditSum.Location = new System.Drawing.Point(821, 462);
            this.lblcreditSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcreditSum.Name = "lblcreditSum";
            this.lblcreditSum.Size = new System.Drawing.Size(81, 17);
            this.lblcreditSum.TabIndex = 1179;
            this.lblcreditSum.Text = "[creditsum]";
            this.lblcreditSum.Visible = false;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(205, 473);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(198, 27);
            this.btnExport.TabIndex = 1180;
            this.btnExport.Text = "Export To Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // accountGroupId
            // 
            this.accountGroupId.DataPropertyName = "accountGroupId";
            this.accountGroupId.HeaderText = "";
            this.accountGroupId.Name = "accountGroupId";
            this.accountGroupId.ReadOnly = true;
            this.accountGroupId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.accountGroupId.Visible = false;
            // 
            // ledgerName
            // 
            this.ledgerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ledgerName.DataPropertyName = "ledgerName";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.ledgerName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ledgerName.FillWeight = 284.7716F;
            this.ledgerName.HeaderText = "PARTICULARS";
            this.ledgerName.Name = "ledgerName";
            this.ledgerName.ReadOnly = true;
            this.ledgerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ledgerName.Width = 300;
            // 
            // debitDifference
            // 
            this.debitDifference.DataPropertyName = "debitDifference";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.debitDifference.DefaultCellStyle = dataGridViewCellStyle4;
            this.debitDifference.FillWeight = 9.17233F;
            this.debitDifference.HeaderText = "DEBIT";
            this.debitDifference.Name = "debitDifference";
            this.debitDifference.ReadOnly = true;
            this.debitDifference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // creditDifference
            // 
            this.creditDifference.DataPropertyName = "creditDifference";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.creditDifference.DefaultCellStyle = dataGridViewCellStyle5;
            this.creditDifference.FillWeight = 6.056094F;
            this.creditDifference.HeaderText = "CREDIT";
            this.creditDifference.Name = "creditDifference";
            this.creditDifference.ReadOnly = true;
            this.creditDifference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // frmTrialBalance2
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1022, 511);
            this.Controls.Add(this.txtTodate);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lblcreditSum);
            this.Controls.Add(this.lbldebitSum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.dgvTrailBalance);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.ddtpTrialToDate);
            this.Controls.Add(this.txtFromDate);
            this.Controls.Add(this.dtpTrialFromDate);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frmTrialBalance2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trial Balance ";
            this.Load += new System.EventHandler(this.frmTrialBalance2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrailBalance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtTodate;
        private System.Windows.Forms.DateTimePicker ddtpTrialToDate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpTrialFromDate;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.DataGridView dgvTrailBalance;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbldebitSum;
        private System.Windows.Forms.Label lblcreditSum;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.DataGridViewTextBoxColumn accountGroupId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ledgerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn debitDifference;
        private System.Windows.Forms.DataGridViewTextBoxColumn creditDifference;
    }
}