﻿using MATFinancials.DAL;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MATFinancials;
namespace MATFinancials.FinancialStatements
{
    public partial class frmDayReport : Form
    {
        #region public variables
        DBMatConnection conn = new DBMatConnection();
        HelperClasses dbHelperClass = new HelperClasses();
        Padding newPadding = new Padding(50, 0, 0, 0);
        bool isMultipleShiftInADay = false;
        decimal checkedDayShift;
        decimal RoleId;
        decimal salesManId;
        bool isPreviousReport = false;
        TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
        #endregion

        public frmDayReport()
        {
            InitializeComponent();
        }

        private void frmDayReport_Load(object sender, EventArgs e)
        {
            dtpDate.Value = PublicVariables._dtCurrentDate;
            dtpDate.Text = dtpDate.Value.ToString("dd-MMM-yyyy");
            cmbSalesmanFill();
            fillGrid();
            showPreviousReports();
        }

        private void cmbSalesmanFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                dtbl = TransactionGeneralFillObj.SalesmanViewAllForComboFill(cmbSalesman, true);

                dtbl.Rows.RemoveAt(0);

                cmbSalesman.DataSource = dtbl;
                cmbSalesman.ValueMember = "userId";
                cmbSalesman.DisplayMember = "employeeName";
                cmbSalesman.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "POS9:" + ex.Message;
            }
        }

        public void fillGrid()
        {
            dbHelperClass = new HelperClasses();
            DataTable dtbl1 = new DataTable();
            DataTable dtbl2 = new DataTable();
            DataTable dtbl3 = new DataTable();
            DataTable dtbl4 = new DataTable();
            DataSet dsDayReport = new DataSet();
            DataSet dsUser = new DataSet();
            conn = new DBMatConnection();
            decimal netTotal = 0;
            int gridRow = 0;
            DateTime transactionDay = Convert.ToDateTime(txtDate.Text.ToString());
            transactionDay = transactionDay.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            try
            {
                btnPrint.Enabled = false;
                chkCloseDay.Enabled = true;
                Font newFont = new Font(dgvDayReport.Font, FontStyle.Bold);
                dgvDayReport.Rows.Clear();
                string queryStr = string.Format("select employeeName from tbl_Employee e inner join tbl_User u on e.employeeCode = u.userName where u.userId = {0}", PublicVariables._decCurrentUserId);
                dsUser = conn.GetDataSet(queryStr);

                #region global report region
                string queryString = string.Format("select roleId from tbl_User where userId = {0}", PublicVariables._decCurrentUserId);
                RoleId = Convert.ToDecimal(dbHelperClass.GetReturnValue(queryString));

                decimal salesManId = Convert.ToDecimal(cmbSalesman.SelectedValue);
                if (RoleId == 1)
                {
                    lblFilterBySalesman.Visible = true;
                    cmbSalesman.Visible = true;
                    if (isPreviousReport || isMultipleShiftInADay)
                    {
                        conn = new DBMatConnection();
                        transactionDay = transactionDay.Date.AddHours(23).AddMinutes(58);
                        conn.AddParameter("@DayDate", transactionDay.ToString());
                        conn.AddParameter("@shift", checkedDayShift);
                        if (cmbSalesman.SelectedIndex > -1)
                        {
                            conn.AddParameter("@salesManId", salesManId);
                        }
                        else
                        {
                            conn.AddParameter("@salesManId", PublicVariables._decCurrentUserId);
                        }
                        dsDayReport = new DataSet();
                        dsDayReport = conn.getDataSet("spPreviousShiftReport");
                        btnPrint.Enabled = true;
                        chkCloseDay.Enabled = false;
                    }
                    else
                    {
                        conn = new DBMatConnection();
                        conn.AddParameter("@EndofDayDate", transactionDay.ToString());
                        if (cmbSalesman.SelectedIndex > -1)
                        {
                            conn.AddParameter("@salesManId", salesManId);
                        }
                        else
                        {
                            conn.AddParameter("@salesManId", PublicVariables._decCurrentUserId);
                        }
                        dsDayReport = new DataSet();
                        dsDayReport = conn.getDataSet("spDayReport");
                    }
                    if (dsDayReport.Tables.Count > -1 && dsDayReport.Tables[0].Rows.Count > 0)
                    {
                        dtbl1 = dsDayReport.Tables[0];
                        dtbl2 = dsDayReport.Tables[1];
                        dtbl3 = dsDayReport.Tables[2];
                        dtbl4 = dsDayReport.Tables[3];

                        dgvDayReport.Rows.Add();
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "DAY REPORT     BY     " + cmbSalesman.SelectedText.ToString().ToUpper();
                        //dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dsUser.Tables[0].Rows[0]["employeeName"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "First Order";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl1.Rows[0]["date"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Last Order";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl2.Rows[0]["date"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "First Invoice";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl1.Rows[0]["invoiceNo"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Last Invoice";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl2.Rows[0]["invoiceNo"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Grand Total";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["grandTotal"]).ToString("N2");
                        netTotal = Convert.ToDecimal(dtbl3.Rows[0]["grandTotal"].ToString());
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Discount";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"]).ToString("N2");
                        netTotal = netTotal - (Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"].ToString()));
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Total Refund";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["refund"]).ToString("N2");
                        netTotal = netTotal - (Convert.ToDecimal(dtbl3.Rows[0]["refund"].ToString()));
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        //dgvDayReport.Rows[gridRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Padding = newPadding;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;

                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "NET TOTAL";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = netTotal.ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "SUMMARY OF PAYMENT";
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CASH NAIRA";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl3.Rows[0]["cashAmount"] != DBNull.Value ?
                            (Convert.ToDecimal(dtbl3.Rows[0]["cashAmount"]) - Convert.ToDecimal(dtbl3.Rows[0]["refund"])).ToString("N2") : Convert.ToDecimal(dtbl3.Rows[0]["refund"]).ToString("N2"); //- Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"])).ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "ATM / PREPAID CARD";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["cardAmount"]).ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CHEQUE PAYMENT";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["chequeAmount"]).ToString("N2");

                        // -------------------- credit list ----------------------------- //
                        if (dtbl3.Rows[0]["creditSales"] != DBNull.Value)
                        {
                            dgvDayReport.Rows.Add();
                            gridRow++;
                            dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CREDIT SALES";
                            dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["creditSales"]).ToString("N2");
                            dgvDayReport.Rows.Add();
                            gridRow++;
                            if (dtbl4.Rows.Count > 0)
                            {
                                dgvDayReport.Rows.Add();
                                gridRow++;
                                dgvDayReport.Rows[gridRow].DefaultCellStyle.Padding = newPadding;
                                dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;
                                dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CREDIT LIST";
                                for (int i = 0; i < dtbl4.Rows.Count; i++)
                                {
                                    dgvDayReport.Rows.Add();
                                    gridRow++;
                                    dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = dtbl4.Rows[i]["invoiceNo"].ToString() + "    " + dtbl4.Rows[i]["customerName"].ToString();
                                    dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl4.Rows[i]["totalAmount"]).ToString("N2");
                                }
                            }
                        }
                    }
                }
                #endregion

                #region day report region
                else
                {
                    if (isPreviousReport)
                    {
                        conn = new DBMatConnection();
                        transactionDay = transactionDay.Date.AddHours(23).AddMinutes(58);
                        conn.AddParameter("@DayDate", transactionDay.ToString());
                        conn.AddParameter("@shift", checkedDayShift);
                        if (cmbSalesman.SelectedIndex > -1)
                        {
                            conn.AddParameter("@salesManId", salesManId);
                        }
                        else
                        {
                            conn.AddParameter("@salesManId", PublicVariables._decCurrentUserId);
                        }
                        dsDayReport = new DataSet();
                        dsDayReport = conn.getDataSet("spPreviousShiftReport");
                        btnPrint.Enabled = true;
                        chkCloseDay.Enabled = false;
                    }
                    else
                    {
                        conn = new DBMatConnection();
                        conn.AddParameter("@EndofDayDate", transactionDay.ToString());
                        conn.AddParameter("@salesManId", PublicVariables._decCurrentUserId);
                        dsDayReport = conn.getDataSet("spDayReport");
                    }
                    if (dsDayReport.Tables.Count > -1 && dsDayReport.Tables[0].Rows.Count > 0)
                    {
                        dtbl1 = dsDayReport.Tables[0];
                        dtbl2 = dsDayReport.Tables[1];
                        dtbl3 = dsDayReport.Tables[2];

                        dgvDayReport.Rows.Add();
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "DAY REPORT     BY     " + dsUser.Tables[0].Rows[0]["employeeName"].ToString().ToUpper();
                        //dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dsUser.Tables[0].Rows[0]["employeeName"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "First Order";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl1.Rows[0]["date"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Last Order";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl2.Rows[0]["date"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "First Invoice";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl1.Rows[0]["invoiceNo"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Last Invoice";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = dtbl2.Rows[0]["invoiceNo"].ToString();
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Grand Total";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["grandTotal"]).ToString("N2");
                        netTotal = Convert.ToDecimal(dtbl3.Rows[0]["grandTotal"].ToString());
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Discount";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"]).ToString("N2");
                        netTotal = netTotal - (Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"].ToString()));
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "Total Refund";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["refund"]).ToString("N2");
                        netTotal = netTotal - (Convert.ToDecimal(dtbl3.Rows[0]["refund"].ToString()));
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        //dgvDayReport.Rows[gridRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Padding = newPadding;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;

                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "NET TOTAL";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = netTotal.ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "SUMMARY OF PAYMENT";
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CASH NAIRA";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = (Convert.ToDecimal(dtbl3.Rows[0]["cashAmount"]) - Convert.ToDecimal(dtbl3.Rows[0]["refund"])).ToString("N2"); // - Convert.ToDecimal(dtbl3.Rows[0]["billDiscount"])).ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "ATM / PREPAID CARD";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["cardAmount"]).ToString("N2");
                        dgvDayReport.Rows.Add();
                        gridRow++;
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CHEQUE PAYMENT";
                        dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["chequeAmount"]).ToString("N2");

                        // ---------------------------- credit list -------------------------- //
                        if (dtbl3.Rows[0]["creditSales"] != DBNull.Value)
                        {
                            dgvDayReport.Rows.Add();
                            gridRow++;
                            dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CREDIT SALES";
                            dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl3.Rows[0]["creditSales"]).ToString("N2");
                            dgvDayReport.Rows.Add();
                            gridRow++;
                            if (dtbl4.Rows.Count > 0)
                            {
                                dgvDayReport.Rows.Add();
                                gridRow++;
                                dgvDayReport.Rows[gridRow].DefaultCellStyle.Padding = newPadding;
                                dgvDayReport.Rows[gridRow].DefaultCellStyle.Font = newFont;
                                dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = "CREDIT LIST";
                                for (int i = 0; i < dtbl4.Rows.Count; i++)
                                {
                                    dgvDayReport.Rows.Add();
                                    gridRow++;
                                    dgvDayReport.Rows[gridRow].Cells["dgvtxtParticulars"].Value = dtbl4.Rows[i]["invoiceNo"].ToString() + "    " + dtbl4.Rows[i]["customerName"].ToString();
                                    dgvDayReport.Rows[gridRow].Cells["dgvtxtValue"].Value = Convert.ToDecimal(dtbl4.Rows[i]["totalAmount"]).ToString("N2");
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkCloseDay.Checked == true)
                {
                    if (Messages.CloseDayMessage())
                    {
                        EnterNextDay(PublicVariables._decCurrentUserId);
                        if (!isPreviousReport)
                        {
                            print(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                        }
                        btnClear_Click(e, e);
                    }
                }
                else
                {
                    //if(Messages.OpenDayMessage())
                    if (isPreviousReport)
                    {
                        print(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void print(DateTime fromDate, DateTime toDate)
        {
            try
            {
                DataSet dsDayReport = GetDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.DayReportPrinting(dsDayReport);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL4:" + ex.Message;
            }
        }

        private DataSet GetDataSet()
        {
            DataSet dsDayReport = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblDayReport = GetDataTable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.ProfitAndLossReportPrintCompany(PublicVariables._decCurrentCompanyId);//(PublicVariables._decCurrentCompanyId);
                dsDayReport.Tables.Add(dtblDayReport);
                dsDayReport.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL3:" + ex.Message;
            }
            return dsDayReport;
        }

        private DataTable GetDataTable()
        {
            DataTable dtblDayReport = new DataTable();
            try
            {
                dtblDayReport.Columns.Add("Particulars");
                dtblDayReport.Columns.Add("Value");
                //dtblProfit.Columns.Add("Amount2");
                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvDayReport.Rows)
                {
                    drow = dtblDayReport.NewRow();
                    drow["Particulars"] = dr.Cells["dgvtxtParticulars"].Value;
                    drow["Value"] = dr.Cells["dgvtxtValue"].Value;
                    //drow["Amount2"] = dr.Cells["dgvtxtAmount2"].Value;
                    dtblDayReport.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL2:" + ex.Message;
            }
            return dtblDayReport;
        }

        public void EnterNextDay(decimal decUseId)
        {
            string queryStr = "";
            DataSet ds = new DataSet();
            salesManTransactionDayInfo infoTransactionDay = new salesManTransactionDayInfo();
            salesManTransactionDaySP SPTransactionDay = new salesManTransactionDaySP();
            HelperClasses helperClasses = new HelperClasses();
            DBMatConnection conn = new DBMatConnection();

            // close previous day 

            infoTransactionDay = new salesManTransactionDayInfo();
            SPTransactionDay = new salesManTransactionDaySP();
            DateTime closinsDay = Convert.ToDateTime(DateTime.Now);     // shift must close at the current time

            queryStr = string.Format("update tbl_salesManTransactionDay set dayCloseDate = '{0}' where transactionDay = " +
                        "(select top 1 transactionDay from tbl_salesManTransactionDay where salesmanId = {1} order by salesManTransactionDayId desc)" +
                        " AND salesManId = '{2}'", closinsDay.ToString(), PublicVariables._decCurrentUserId, decUseId);
            conn.ExecuteNonQuery2(queryStr);

            //  leave current day open

            infoTransactionDay = new salesManTransactionDayInfo();
            SPTransactionDay = new salesManTransactionDaySP();

            infoTransactionDay.dayCloseDate = null;
            conn = new DBMatConnection();
            queryStr = string.Format("select * from tbl_salesManTransactionDay s where s.salesManId = {0}", decUseId);
            ds = conn.ExecuteQuery(queryStr);
            //infoTransactionDay.dayCloseDate = Convert.ToDateTime(txtDate.Text.ToString());
            //infoTransactionDay.dayCloseDate = infoTransactionDay.dayCloseDate.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                infoTransactionDay.dayOpenDate = helperClasses.GetOpenDay(decUseId);
                infoTransactionDay.dayOpenDate = infoTransactionDay.dayOpenDate.AddMinutes(1);
                infoTransactionDay.transactionDay = helperClasses.GetCurrentDay(decUseId) + 1;
            }
            else
            {
                infoTransactionDay.dayOpenDate = DateTime.Now;
                infoTransactionDay.dayCloseDate = DateTime.Now;
                infoTransactionDay.transactionDay = 0;
            }
            infoTransactionDay.Extra1 = string.Empty;
            infoTransactionDay.Extra2 = string.Empty;
            infoTransactionDay.ExtraDate = DateTime.Now;
            infoTransactionDay.salesManId = decUseId;
            if (SPTransactionDay.salesManTransactionDayAdd(infoTransactionDay))
            {
                Messages.SavedMessage();
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtDate.Text = dtpDate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL7:" + ex.Message;
            }
        }

        private void txtDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (txtDate.SelectionLength != 11)
                    {
                        if (txtDate.Text == string.Empty || txtDate.SelectionStart == 0)
                        {
                            txtDate.Focus();
                            txtDate.SelectionStart = 0;
                            txtDate.SelectionLength = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL17:" + ex.Message;
            }
        }

        private void txtDate_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtDate.Text == string.Empty)
                {
                    txtDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtDate);
                if (txtDate.Text == string.Empty)
                {
                    txtDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL13:" + ex.Message;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            isPreviousReport = false;
            isMultipleShiftInADay = false;
            dgvDayReport.Rows.Clear();
            fillGrid();
        }

        private void Clear()
        {
            isPreviousReport = false;
            ((ListBox)chkDaysList).DataSource = null;
            //chkDaysList.Visible = false;
            dtpDate.Value = PublicVariables._dtToDate;
            dtpDate.Text = dtpDate.Value.ToString("dd-MMM-yyyy");
            txtDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
            cmbCounter.SelectedIndex = -1;
            cmbSalesman.SelectedIndex = -1;
            showPreviousReports();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime currentDay = DateTime.Now.Date.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute);
                DateTime transactionDay = Convert.ToDateTime(txtDate.Text.ToString());
                transactionDay = transactionDay.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute); //.AddSeconds(DateTime.Now.Second);
                checkedDayShift = -1;
                if (transactionDay < currentDay)
                {
                    isMultipleShiftInADay = false;
                    isPreviousReport = true;
                }
                else
                {
                    isPreviousReport = false;
                }
                fillGrid();
                showPreviousReports();
            }
            catch (Exception ex)
            {

            }
        }

        private void chkCloseDay_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCloseDay.Checked == true)
            {
                btnPrint.Enabled = true;
            }
            else
            {
                btnPrint.Enabled = false;
            }
        }

        private void chkDaysList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                isMultipleShiftInADay = true;
                isPreviousReport = true;
                //DataRow row;
                //row = ((DataRowView)this.chkDaysList.Items[0]).Row;
                checkedDayShift = Convert.ToInt64(chkDaysList.SelectedValue.ToString());
                //checkedDayShift = Convert.ToInt64((row[this.chkDaysList.SelectedValue]).ToString());
                fillGrid();
            }
            catch (Exception ex)
            {

            }
        }
        public void showPreviousReports()
        {
            //if (isPreviousReport)
            //{
            try
            {
                ((ListBox)chkDaysList).DataSource = null;
                chkDaysList.Visible = false;
                DBMatConnection conn = new DBMatConnection();
                dbHelperClass = new HelperClasses();
                salesManId = cmbSalesman.SelectedValue == null ? PublicVariables._decCurrentUserId : Convert.ToDecimal(cmbSalesman.SelectedValue);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                DateTime dayRange = Convert.ToDateTime(txtDate.Text.ToString());
                DateTime startDate = dayRange.AddHours(0).AddMinutes(0).AddSeconds(0);
                DateTime endDate = dayRange.AddHours(23).AddMinutes(59).AddSeconds(59);
                conn.AddParameter("@startDate", startDate);
                conn.AddParameter("@endDate", endDate);
                conn.AddParameter("@salesManId", salesManId);
                ds = conn.getDataSet("getPreviousShifts");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    for(int i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["Extra1"] = "Shift  " + dt.Rows[i]["Sl No"].ToString();
                    }
                    ((ListBox)chkDaysList).DataSource = dt;
                    ((ListBox)chkDaysList).DisplayMember = "Extra1";
                    ((ListBox)chkDaysList).ValueMember = "transactionDay";
                    chkDaysList.Visible = true;
                }
            }
            catch (Exception ex)
            {

            }
            //}
        }
    }
}
