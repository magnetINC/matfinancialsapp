﻿
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.FinancialStatements
{
    public partial class frmTrialBalance2 : Form
    {
        public frmTrialBalance2()
        {
            InitializeComponent();
        }

        bool isFormLoad = false;

        private void frmTrialBalance2_Load(object sender, EventArgs e)
        {
            try
            {
                isFormLoad = true;
                dtpTrialFromDate.MinDate = PublicVariables._dtFromDate;
                dtpTrialFromDate.MaxDate = PublicVariables._dtToDate;
                dtpTrialFromDate.Value = PublicVariables._dtFromDate;
                ddtpTrialToDate.MinDate = PublicVariables._dtFromDate;
                ddtpTrialToDate.MaxDate = PublicVariables._dtToDate;
                ddtpTrialToDate.Value = PublicVariables._dtToDate;
                dtpTrialFromDate.Text = dtpTrialFromDate.Value.ToString("dd-MMM-yyyy");
                ddtpTrialToDate.Text = ddtpTrialToDate.Value.ToString("dd-MMM-yyyy");
                isFormLoad = false;
                GridFill();
                txtFromDate.Focus();

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB7:" + ex.Message;
            }
        }


        // method implementation Precious
        public void GridFill()
        {
            // Method Precious
            try
            {
                decimal dcOpeningStockForRollOver = 0;
                if (!isFormLoad)
                {
                    string calculationMethod = string.Empty;
                    SettingsInfo InfoSettings = new SettingsInfo();
                    SettingsSP SpSettings = new SettingsSP();
                    GeneralQueries queryForRetainedEarning = new GeneralQueries();
                    //--------------- Selection Of Calculation Method According To Settings ------------------// 
                    if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                    {
                        calculationMethod = "FIFO";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                    {
                        calculationMethod = "Average Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                    {
                        calculationMethod = "High Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                    {
                        calculationMethod = "Low Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                    {
                        calculationMethod = "Last Purchase Rate";
                    }
                    FinancialStatementSP SpFinance1 = new FinancialStatementSP();
                    CurrencyInfo InfoCurrency = new CurrencyInfo();
                    CurrencySP SpCurrency = new CurrencySP();
                    GeneralQueries methodForStockValue = new GeneralQueries();
                    DBMatConnection conn = new DBMatConnection();
                    int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
                    decimal dcClosingStock = SpFinance1.StockValueGetOnDate(Convert.ToDateTime(txtTodate.Text), calculationMethod, false, false);
                    dcClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
                    //---------------------Opening Stock-----------------------
                    //decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(PublicVariables._dtFromDate, calculationMethod, true, true);           
                    string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                    string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                    decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(Convert.ToDateTime(txtTodate.Text), calculationMethod, true, true);
                    DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                    DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                    if (conn.getSingleValue(queryStr) != string.Empty)
                    {
                        lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                    }
                    else
                    {
                        lastFinYearEnd = Convert.ToDateTime(txtFromDate.Text).AddDays(-1);
                    }
                    //dcOpeningStockForRollOver = SpFinance1.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                    dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, 0, false, DateTime.Now);
                    dcOpeninggStock = dcOpeninggStock + dcOpeningStockForRollOver;

                    DateValidation objValidation = new DateValidation();
                    objValidation.DateValidationFunction(txtTodate);
                    if (txtTodate.Text == string.Empty)
                        txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");

                    DateValidation objvalidation = new DateValidation();
                    objvalidation.DateValidationFunction(txtFromDate);
                    if (txtFromDate.Text == string.Empty)
                    {
                        txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    }
                    objvalidation.DateValidationFunction(txtTodate);
                    if (txtTodate.Text == string.Empty)
                    {
                        txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    }
                    /// -----------------------section -----------------
                    FinancialStatementSP financialStatement = new FinancialStatementSP();
                    DataTable dtTrial = new DataTable();
                    dtTrial = financialStatement.TrialBalanceGridFill(Convert.ToDateTime(txtTodate.Text));
                    var querySumCr = 0m;
                    var querySumDr = 0m;

                    // ----------------- calculate and replace retained earning in the datatable ---------------- //
                    decimal retainedEarning = queryForRetainedEarning.retainedEarnings(Convert.ToDateTime(txtTodate.Text));
                    string retainedEarnings = retainedEarning > 0 ? retainedEarning.ToString() : "(" + (retainedEarning * -1).ToString() + ")";
                    if (dtTrial.AsEnumerable().Where(m => m.Field<string>("accountGroupName").ToLower() == "retained earning").Any())
                    {
                        foreach (DataRow row in dtTrial.Rows)
                        {
                            if (row["accountGroupName"].ToString().Trim().ToLower() == "retained earning".ToLower())
                            {
                                if (retainedEarning > 0)
                                {
                                    row["credit"] = retainedEarning;
                                    row["creditDifference"] = retainedEarning;
                                }
                                else
                                {
                                    retainedEarning = retainedEarning * -1;
                                    row["debit"] = retainedEarning;
                                    row["debitDifference"] = retainedEarning;
                                }
                                row["accountGroupName"] = "Retained Earning";
                            }
                        }
                    }
                    else if (retainedEarning != 0)
                    {
                        if (retainedEarning > 0)
                        {
                            DataRow dRE1 = dtTrial.NewRow();
                            dRE1["accountGroupId"] = 0;
                            dRE1["credit"] = retainedEarning;
                            dRE1["creditDifference"] = retainedEarning;
                            dRE1["debit"] = 0.0;
                            dRE1["debitDifference"] = 0.00;
                            dRE1["accountGroupName"] = "Retained Earning";
                            dtTrial.Rows.Add(dRE1);
                        }
                        else if (retainedEarning < 0)
                        {
                            retainedEarning = retainedEarning * -1;
                            DataRow dRE2 = dtTrial.NewRow();
                            dRE2["accountGroupId"] = 0;
                            dRE2["credit"] = 0.0;
                            dRE2["creditDifference"] = 0.0;
                            dRE2["debit"] = retainedEarning;
                            dRE2["debitDifference"] = retainedEarning;
                            dRE2["accountGroupName"] = "Retained Earning";
                            dtTrial.Rows.Add(dRE2);
                        }
                    }
                    DataRow dr = dtTrial.NewRow();  // to add new row
                    dr["accountGroupId"] = -4;
                    dr["debit"] = 0.0;
                    dr["credit"] = 0.0;
                    dr["creditDifference"] = 0.0;
                    dr["debitDifference"] = dcOpeninggStock;
                    dr["accountGroupName"] = "Stock";
                    dtTrial.Rows.Add(dr);

                    querySumCr = (from c in dtTrial.AsEnumerable() select c.Field<decimal>("creditDifference")).Sum();
                    querySumDr = (from c in dtTrial.AsEnumerable() select c.Field<decimal>("debitDifference")).Sum();

                    DataRow drSum = dtTrial.NewRow();  // to add new row                  
                    drSum["accountGroupId"] = -2;
                    drSum["debit"] = 0.0;
                    drSum["credit"] = 0.0;
                    drSum["creditDifference"] = querySumCr;
                    drSum["debitDifference"] = querySumDr;
                    drSum["accountGroupName"] = "TOTAL";

                    //dtTrial.Rows.Add(drNewLine);    
                    dtTrial.Rows.Add(drSum);

                    //
                    //Commented Out By Precious, devising another means to doing this, in-order to add lines beneat and above Balance.
                    //var query = dtTrial.AsEnumerable()                       
                    //.Select(trialBalance => new
                    //{                    
                    //    ledgerName = trialBalance.Field<string>("accountGroupName"),                      
                    //    debitDifference = trialBalance.Field<decimal>("debitDifference").ToString("N2"),
                    //    creditDifference = trialBalance.Field<decimal>("creditDifference").ToString("N2"),
                    //    accountGroupId = trialBalance.Field<decimal>("accountGroupId")
                    //}).ToList();

                    //dgvTrailBalance.DataSource = query;            
                    dgvTrailBalance.Rows.Clear();   // clears the previous record before adding new ones, based on filter.
                    foreach (DataRow trialRows in dtTrial.Rows)
                    {
                        if (Convert.ToDecimal(trialRows["accountGroupId"].ToString()) == -2)
                        {
                            dgvTrailBalance.Rows.Add();
                            dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = "";
                            dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = "_______________";
                            dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = "_______________";
                        }

                        decimal debitDiff = Convert.ToDecimal(trialRows["debitDifference"]);
                        decimal ceditDiff = Convert.ToDecimal(trialRows["creditDifference"]);

                        FinancialStatementSP SpFinance = new FinancialStatementSP();
                        decimal direct = SpFinance.GetDirectPostingIntoRetainedEanings(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                        if (trialRows["accountGroupName"].ToString().ToLower() == "retained earning".ToLower()) {
                            debitDiff = debitDiff - direct;
                            ceditDiff = ceditDiff - direct;
                        }
                        
                        dgvTrailBalance.Rows.Add();
                        dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = trialRows["accountGroupName"];
                        dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = debitDiff.ToString("N2");
                        dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = ceditDiff.ToString("N2");
                        dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["accountGroupId"].Value = trialRows["accountGroupId"];
                    }
                    dgvTrailBalance.Rows.Add();
                    dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = "";
                    dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = "===============";
                    dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = "===============";
                    lblcreditSum.Text = querySumCr.ToString("N2");
                    lbldebitSum.Text = querySumDr.ToString("N2");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }

        public DataTable Getdatatable()
        {
            DataTable dtblTrail = new DataTable();
            try
            {
                dtblTrail.Columns.Add("ledgerName");
                dtblTrail.Columns.Add("debitDifference");
                dtblTrail.Columns.Add("creditDifference");

                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvTrailBalance.Rows)
                {
                    drow = dtblTrail.NewRow();
                    drow["ledgerName"] = dr.Cells["ledgerName"].Value;
                    drow["debitDifference"] = dr.Cells["debitDifference"].Value;
                    drow["creditDifference"] = dr.Cells["creditDifference"].Value;
                    dtblTrail.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB4:" + ex.Message;
            }

            return dtblTrail;
        }
        public DataSet getdataset()
        {
            DataSet dsTrail = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblTrail = Getdatatable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.FundFlowReportPrintCompany(1);    // removed PublicVariable.CurrenCompanyID
                dsTrail.Tables.Add(dtblTrail);
                dsTrail.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB5:" + ex.Message;
            }
            return dsTrail;
        }

        public void Print(DateTime todate)
        {
            try
            {
                DateTime printedDate = Convert.ToDateTime(txtTodate.Text);

                FinancialStatementSP spFinance = new FinancialStatementSP();
                DataSet dsTrail = getdataset();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.TrailBalanceReportPrinting(dsTrail, Convert.ToDateTime(txtTodate.Text));
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB6:" + ex.Message;
            }
        }

        private void dgvTrailBalance_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    if (dgvTrailBalance.Rows[e.RowIndex].Cells["accountGroupId"].Value != null && dgvTrailBalance.Rows[e.RowIndex].Cells["accountGroupId"].Value.ToString() != string.Empty)
                    {
                        decimal decAccountGroupId = Convert.ToDecimal(dgvTrailBalance.Rows[e.RowIndex].Cells["accountGroupId"].Value.ToString());
                        frmLedgerDetails frmLedgerDetailsObjt = new frmLedgerDetails();
                        frmAccountLedgerReport led = new frmAccountLedgerReport();
                        frmAccountGroupwiseReport frmAccountGroupwiseReportObjt = new frmAccountGroupwiseReport();

                        frmAccountGroupwiseReportObjt.WindowState = FormWindowState.Normal;
                        frmAccountGroupwiseReportObjt.MdiParent = formMDI.MDIObj;

                        if (dgvTrailBalance.Rows[e.RowIndex].Cells["debitDifference"].Value.ToString() != "0.00" && dgvTrailBalance.Rows[e.RowIndex].Cells["creditDifference"].Value.ToString() == "0.00" && (decAccountGroupId == 22 || decAccountGroupId == 26)
                            || dgvTrailBalance.Rows[e.RowIndex].Cells["creditDifference"].Value.ToString() != "0.00" && dgvTrailBalance.Rows[e.RowIndex].Cells["debitDifference"].Value.ToString() == "0.00" && (decAccountGroupId == 22 || decAccountGroupId == 26))
                        {
                            frmAccountGroupwiseReportObjt.CallFromTrailBalance2(dtpTrialFromDate.Value.ToString(), ddtpTrialToDate.Value.ToString(), decAccountGroupId, this);
                        }
                        else if ((dgvTrailBalance.Rows[e.RowIndex].Cells["debitDifference"].Value.ToString() != "0.00" && dgvTrailBalance.Rows[e.RowIndex].Cells["creditDifference"].Value.ToString() == "0.00") && (decAccountGroupId != 22 || decAccountGroupId != 26)
                            || (dgvTrailBalance.Rows[e.RowIndex].Cells["creditDifference"].Value.ToString() != "0.00" && dgvTrailBalance.Rows[e.RowIndex].Cells["debitDifference"].Value.ToString() == "0.00") && (decAccountGroupId != 22 || decAccountGroupId != 26))
                        {
                            frmLedgerDetailsObjt.WindowState = FormWindowState.Normal;
                            frmLedgerDetailsObjt.MdiParent = formMDI.MDIObj;
                            frmLedgerDetailsObjt.CallFromAccountLedgerReport(led, decAccountGroupId, dtpTrialFromDate.Value, ddtpTrialToDate.Value);
                        }

                        // Ladt Modified by Precious _26_09_2016
                        // code to implement if not using accountGroupID
                    }
                }
            }
            catch (Exception ex)
            {
                this.Enabled = true;
                formMDI.infoError.ErrorString = "TB12:" + ex.Message;
            }
        }

        private void dtpTrialFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpTrialFromDate.Value;
                this.txtFromDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB10:" + ex.Message;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.ddtpTrialToDate.Value;
                this.txtTodate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB9:" + ex.Message;
            }
        }

        public void Clear()
        {
            try
            {
                isFormLoad = true;
                dtpTrialFromDate.MinDate = PublicVariables._dtFromDate;
                dtpTrialFromDate.MaxDate = PublicVariables._dtToDate;
                dtpTrialFromDate.Value = PublicVariables._dtFromDate;
                ddtpTrialToDate.MinDate = PublicVariables._dtFromDate;
                ddtpTrialToDate.MaxDate = PublicVariables._dtToDate;
                ddtpTrialToDate.Value = PublicVariables._dtToDate;
                dtpTrialFromDate.Text = dtpTrialFromDate.Value.ToString("dd-MMM-yyyy");
                ddtpTrialToDate.Text = ddtpTrialToDate.Value.ToString("dd-MMM-yyyy");
                txtTodate.Text = ddtpTrialToDate.Value.ToString("dd-MMM-yyyy");
                isFormLoad = false;
                GridFill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB1:" + ex.Message;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DateValidation ObjValidation = new DateValidation();
                ObjValidation.DateValidationFunction(txtTodate);
                if (Convert.ToDateTime(txtTodate.Text) < Convert.ToDateTime(txtFromDate.Text))
                {
                    MessageBox.Show("Todate should be greater than Fromdate", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    GridFill();
                }
                else
                {

                    GridFill();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB14:" + ex.Message;
            }
        }

        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtFromDate.Text, out dt);
                dtpTrialFromDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB15:" + ex.Message;
            }
        }

        private void txtTodate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtTodate);
                if (txtTodate.Text == string.Empty)
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB16:" + ex.Message;
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvTrailBalance.Rows.Count < 0)
                {
                    MessageBox.Show("No Row To Print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    Print(PublicVariables._dtToDate);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB13:" + ex.Message;
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime selectedDate = Convert.ToDateTime(txtTodate.Text);
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvTrailBalance, "TRIAL BALANCE AS OF: " + txtTodate.Text, 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }
    }
}
