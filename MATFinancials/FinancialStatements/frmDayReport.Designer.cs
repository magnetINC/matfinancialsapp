﻿namespace MATFinancials.FinancialStatements
{
    partial class frmDayReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDayReport));
            this.dgvDayReport = new System.Windows.Forms.DataGridView();
            this.dgvtxtParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.chkCloseDay = new System.Windows.Forms.CheckBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cmbCounter = new System.Windows.Forms.ComboBox();
            this.cmbSalesman = new System.Windows.Forms.ComboBox();
            this.lblFilterByCounter = new System.Windows.Forms.Label();
            this.lblFilterBySalesman = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblDate = new System.Windows.Forms.Label();
            this.chkDaysList = new System.Windows.Forms.CheckedListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDayReport)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDayReport
            // 
            this.dgvDayReport.AllowUserToResizeRows = false;
            this.dgvDayReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDayReport.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvDayReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDayReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDayReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDayReport.ColumnHeadersVisible = false;
            this.dgvDayReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtParticulars,
            this.dgvtxtValue});
            this.dgvDayReport.EnableHeadersVisualStyles = false;
            this.dgvDayReport.GridColor = System.Drawing.Color.White;
            this.dgvDayReport.Location = new System.Drawing.Point(255, 68);
            this.dgvDayReport.Name = "dgvDayReport";
            this.dgvDayReport.RowHeadersVisible = false;
            this.dgvDayReport.Size = new System.Drawing.Size(538, 384);
            this.dgvDayReport.TabIndex = 0;
            // 
            // dgvtxtParticulars
            // 
            this.dgvtxtParticulars.HeaderText = "Particulars";
            this.dgvtxtParticulars.Name = "dgvtxtParticulars";
            // 
            // dgvtxtValue
            // 
            this.dgvtxtValue.HeaderText = "Value";
            this.dgvtxtValue.Name = "dgvtxtValue";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(136, 68);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(425, 28);
            this.panel1.TabIndex = 1186;
            this.panel1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DimGray;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "DAY REPORT";
            // 
            // chkCloseDay
            // 
            this.chkCloseDay.AutoSize = true;
            this.chkCloseDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCloseDay.Location = new System.Drawing.Point(634, 473);
            this.chkCloseDay.Name = "chkCloseDay";
            this.chkCloseDay.Size = new System.Drawing.Size(90, 20);
            this.chkCloseDay.TabIndex = 1187;
            this.chkCloseDay.Text = "Close Shift";
            this.chkCloseDay.UseVisualStyleBackColor = true;
            this.chkCloseDay.CheckedChanged += new System.EventHandler(this.chkCloseDay_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.Enabled = false;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(752, 467);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(85, 27);
            this.btnPrint.TabIndex = 1188;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // cmbCounter
            // 
            this.cmbCounter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCounter.FormattingEnabled = true;
            this.cmbCounter.Location = new System.Drawing.Point(128, 31);
            this.cmbCounter.Name = "cmbCounter";
            this.cmbCounter.Size = new System.Drawing.Size(121, 21);
            this.cmbCounter.TabIndex = 1189;
            this.cmbCounter.Visible = false;
            // 
            // cmbSalesman
            // 
            this.cmbSalesman.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSalesman.FormattingEnabled = true;
            this.cmbSalesman.Location = new System.Drawing.Point(255, 31);
            this.cmbSalesman.Name = "cmbSalesman";
            this.cmbSalesman.Size = new System.Drawing.Size(121, 21);
            this.cmbSalesman.TabIndex = 1190;
            this.cmbSalesman.Visible = false;
            // 
            // lblFilterByCounter
            // 
            this.lblFilterByCounter.AutoSize = true;
            this.lblFilterByCounter.Location = new System.Drawing.Point(128, 12);
            this.lblFilterByCounter.Name = "lblFilterByCounter";
            this.lblFilterByCounter.Size = new System.Drawing.Size(44, 13);
            this.lblFilterByCounter.TabIndex = 1191;
            this.lblFilterByCounter.Text = "Counter";
            this.lblFilterByCounter.Visible = false;
            // 
            // lblFilterBySalesman
            // 
            this.lblFilterBySalesman.AutoSize = true;
            this.lblFilterBySalesman.Location = new System.Drawing.Point(252, 11);
            this.lblFilterBySalesman.Name = "lblFilterBySalesman";
            this.lblFilterBySalesman.Size = new System.Drawing.Size(53, 13);
            this.lblFilterBySalesman.TabIndex = 1192;
            this.lblFilterBySalesman.Text = "Salesman";
            this.lblFilterBySalesman.Visible = false;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(591, 32);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(188, 20);
            this.txtDate.TabIndex = 1193;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(775, 32);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(18, 20);
            this.dtpDate.TabIndex = 1196;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.DimGray;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Maroon;
            this.btnClear.Location = new System.Drawing.Point(917, 31);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 21);
            this.btnClear.TabIndex = 1195;
            this.btnClear.Text = "Reset";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(819, 31);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 21);
            this.btnSearch.TabIndex = 1194;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(539, 35);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 1197;
            this.lblDate.Text = "Date";
            // 
            // chkDaysList
            // 
            this.chkDaysList.CheckOnClick = true;
            this.chkDaysList.FormattingEnabled = true;
            this.chkDaysList.Location = new System.Drawing.Point(819, 95);
            this.chkDaysList.Name = "chkDaysList";
            this.chkDaysList.Size = new System.Drawing.Size(120, 94);
            this.chkDaysList.TabIndex = 1198;
            this.chkDaysList.Visible = false;
            this.chkDaysList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chkDaysList_ItemCheck);
            // 
            // frmDayReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.dgvDayReport);
            this.Controls.Add(this.chkDaysList);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblFilterBySalesman);
            this.Controls.Add(this.lblFilterByCounter);
            this.Controls.Add(this.cmbSalesman);
            this.Controls.Add(this.cmbCounter);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.chkCloseDay);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDayReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Day Report";
            this.Load += new System.EventHandler(this.frmDayReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDayReport)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDayReport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkCloseDay;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ComboBox cmbCounter;
        private System.Windows.Forms.ComboBox cmbSalesman;
        private System.Windows.Forms.Label lblFilterByCounter;
        private System.Windows.Forms.Label lblFilterBySalesman;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtValue;
        private System.Windows.Forms.CheckedListBox chkDaysList;
    }
}