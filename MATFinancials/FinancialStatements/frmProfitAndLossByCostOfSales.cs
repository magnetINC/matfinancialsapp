﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MATFinancials.Classes.SP;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;

namespace MATFinancials
{
    public partial class frmProfitAndLossByCostOfSales : Form
    {
        #region Public Variables
        /// <summary>
        /// Public variable Declaration part
        /// </summary>
        string calculationMethod = string.Empty;
        bool isFormLoad = false;
        string strGroupId = string.Empty;
        string strLedgerId = string.Empty;
        int inCurrenRowIndex = 0;// To keep row index while returning from voucher 
        int inCurrentColunIndex = 0;// To keep column index while returning from voucher 
        string strformName = string.Empty;
        decimal decgranExTotal = 0;
        decimal decgranIncTotal = 0;
        Padding newPadding = new Padding(50, 0, 0, 0);
        #endregion
        #region Functions
        /// <summary>
        /// Creates an instance of frmProfitAndLossByCostOfSales class
        /// </summary>
        public frmProfitAndLossByCostOfSales()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Function to fill Datagridview
        /// </summary>
        public void Gridfill()
        {
            try
            {
                decimal TotalRevenue = 0;
                decimal totalCostofSales = 0;
                decimal TotalOpeningStockAndExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal OperatingExpenses = 0;
                decimal OperatingProfitOrLos = 0;
                decimal dcOpeningStockForRollOver = 0;
                decimal dcOpeningStock = 0;

                if (!isFormLoad)
                {
                    DateValidation objValidation = new DateValidation();
                    objValidation.DateValidationFunction(txtFromDate);
                    if (txtFromDate.Text == string.Empty)
                        txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    objValidation.DateValidationFunction(txtToDate);
                    if (txtToDate.Text == string.Empty)
                        txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    Font newFont = new Font(dgvProfitAndLoss.Font, FontStyle.Bold);
                    CurrencyInfo InfoCurrency = new CurrencyInfo();
                    CurrencySP SpCurrency = new CurrencySP();
                    InfoCurrency = SpCurrency.CurrencyView(1);
                    int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
                    dgvProfitAndLoss.Rows.Clear();
                    FinancialStatementSP spFinancial = new FinancialStatementSP();
                    DataTable dtblFinancial = new DataTable();
                    DataSet DsetProfitAndLoss = new DataSet();
                    SettingsInfo infoSettings = new SettingsInfo();
                    SettingsSP SpSettings = new SettingsSP();
                    GeneralQueries methodForStockValue = new GeneralQueries();
                    DBMatConnection conn = new DBMatConnection();

                    //---------check  calculation method
                    if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                    {
                        calculationMethod = "FIFO";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                    {
                        calculationMethod = "Average Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                    {
                        calculationMethod = "High Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                    {
                        calculationMethod = "Low Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                    {
                        calculationMethod = "Last Purchase Rate";
                    }
                    DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysisByCostOfSales(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text));
                    DataTable dtOpeningStock = new DataTable();
                    DataTable dtOpeningStockForRollOver = new DataTable();
                    dtOpeningStock = spFinancial.StockValueOnDateByAVCOForOpeningStockByCostOfSales(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false, false);
                    dtOpeningStockForRollOver = spFinancial.StockValueOnDateByAVCOForOpeningStockByCostOfSales(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false, true);

                    if (dtOpeningStock != null && dtOpeningStock.Rows.Count > 0 && dtOpeningStockForRollOver != null && dtOpeningStockForRollOver.Rows.Count > 0)
                    {
                        var rollOverList = (from i in dtOpeningStock.AsEnumerable()
                                            join j in dtOpeningStockForRollOver.AsEnumerable() on i.Field<decimal>("ledgerId") equals j.Field<decimal>("ledgerId") //into newTable
                                            select new
                                            {
                                                ledgerId = j.Field<decimal>("ledgerId"),
                                                ledgerName = j.Field<string>("ledgerName"),
                                                stockValue = j.Field<decimal>("stockValue") + i.Field<decimal>("stockValue")

                                            }).ToList();

                        dtOpeningStock.Rows.Clear();
                        foreach (var item in rollOverList)
                        {
                            DataRow dr = dtOpeningStock.NewRow();
                            dr["ledgerId"] = item.ledgerId;
                            dr["ledgerName"] = item.ledgerName;
                            dr["stockValue"] = item.stockValue;
                            dtOpeningStock.Rows.Add(dr);
                        }

                    }

                    else if ((dtOpeningStock == null || dtOpeningStock.Rows.Count < 1) && (dtOpeningStockForRollOver != null || dtOpeningStockForRollOver.Rows.Count > 0))
                    {
                        //dtOpeningStock = dtOpeningStockForRollOver; since we now use the stock formular in GeneralQueries.CurrentStock
                    }

                    // ---------------------------------- REVENUE ----------------------------------//
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "REVENUE";
                    dgvProfitAndLoss.Rows.Add();
                    //---Sales Account  -Credit
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[1];
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                            TotalRevenue += Convert.ToDecimal(rw["Credit"]);
                            dgvProfitAndLoss.Rows.Add();
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    }

                    //----Direct Income 
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[3];
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                            TotalRevenue += Convert.ToDecimal(rw["Credit"]);
                            dgvProfitAndLoss.Rows.Add();
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";

                    // ---------------------------------- Total Revenue ------------------------------------------------------------ //
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "TOTAL REVENUE";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2");

                    // ---------------------------------- COST OF SALES ----------------------------------//

                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "COST OF SALES";
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                    ///// ---Purchases (cost of sales) - Debit
                    dtblFinancial = new DataTable();

                    dtblFinancial = DsetProfitAndLoss.Tables[0];
                    DataTable dtClosingStock = spFinancial.StockValueGetOnDateByCostOfSales(DateTime.Parse(txtToDate.Text), calculationMethod, false, false);
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                    // check for opening and closing stock without sales
                    List<decimal> ledgersNotInLedgerPosting = new List<decimal>();
                    List<decimal> ledgersinLedgerPosting = new List<decimal>();
                    if (dtClosingStock.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtClosingStock.Rows.Count; i++)
                        {
                            decimal ledgerId = Convert.ToDecimal((dtClosingStock.Rows[i]["ledgerId"]).ToString());
                            ledgersNotInLedgerPosting.Add(ledgerId);
                        }
                    }

                    if (dtblFinancial.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtblFinancial.Rows.Count; i++)
                        {
                            decimal ledgerId = Convert.ToDecimal((dtblFinancial.Rows[i]["ledgerId"]).ToString());
                            ledgersinLedgerPosting.Add(ledgerId);
                        }
                    }

                    List<decimal> unCommonLedgers = ledgersNotInLedgerPosting.Except(ledgersinLedgerPosting).ToList();

                    string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                    string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                    DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                    DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                    if (conn.getSingleValue(queryStr) != string.Empty)
                    {
                        lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                    }
                    else
                    {
                        lastFinYearEnd = Convert.ToDateTime(txtFromDate.Text).AddDays(-1);
                    }
                    decimal dcPurchaseAccount = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal ledgerId = Convert.ToDecimal(rw["ledgerId"].ToString());
                            decimal openingStock = (from d in dtOpeningStock.AsEnumerable()
                                                    where d.Field<decimal>("ledgerId") == ledgerId
                                                    select d.Field<decimal>("stockValue")).FirstOrDefault();

                            decimal openingStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, ledgerId, false, DateTime.Now); // new opening stock method by average cost 20170120
                            openingStock = openingStock + openingStockForRollOver;
                            //decimal closingStock = (from e in dtClosingStock.AsEnumerable()
                            //                        where e.Field<decimal>("ledgerId") == ledgerId
                            //                        select e.Field<decimal>("stockValue")).Sum();

                            decimal closingStock = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(txtToDate.Text), true, ledgerId, false, lastFinYearEnd); //this should work instaed   //

                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                            dcPurchaseAccount += dcBalance;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                            decimal costOfSales = Convert.ToDecimal(rw["Debit"]) + openingStock - closingStock; 
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = costOfSales > 0 ? costOfSales.ToString("N2") : "(" + (costOfSales * -1).ToString("N2") + ")";
                            totalCostofSales += Convert.ToDecimal(rw["Debit"]) + openingStock - closingStock;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = rw["ledgerId"].ToString();
                            dgvProfitAndLoss.Rows.Add();
                        }
                    }

                    if (unCommonLedgers.Any())
                    {
                        for (int j = 0; j < unCommonLedgers.Count; j++)
                        {
                            //decimal value = Convert.ToDecimal(dtClosingStock.AsEnumerable().Where(d => d.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())).Select(e => e.Field<decimal>("ledgerId")).ToString());
                            decimal openingStock = (from f in dtOpeningStock.AsEnumerable()
                                                    where f.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                                                    select f.Field<decimal>("stockValue")).FirstOrDefault();

                            decimal openingStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, Convert.ToDecimal(unCommonLedgers[j].ToString()), false, DateTime.Now); // new opening stock method by average cost 20170120
                            openingStock = openingStock + openingStockForRollOver;
                            //decimal closingStock = (from g in dtClosingStock.AsEnumerable()
                            //                        where g.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                            //                        select g.Field<decimal>("stockValue")).FirstOrDefault();

                            decimal closingStock = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(txtToDate.Text), true, Convert.ToDecimal(unCommonLedgers[j].ToString()), false, DateTime.Now);  // new closing stock method by average cost 20170120

                            string ledgerName = (from g in dtClosingStock.AsEnumerable()
                                                 where g.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                                                 select g.Field<string>("ledgerName")).FirstOrDefault();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            decimal dcBalance = decimal.Parse(dtClosingStock.Rows[j]["stockValue"].ToString().ToString());
                            dcPurchaseAccount += dcBalance;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = ledgerName.ToUpper();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = (openingStock - closingStock).ToString("N2");
                            totalCostofSales += (openingStock - closingStock);
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = dtClosingStock.Rows[j]["ledgerId"].ToString();
                            dgvProfitAndLoss.Rows.Add();
                        }
                    }

                    decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;
                    ////---Direct Expense
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[2];
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        decimal directExpenses = 0;
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            directExpenses += Convert.ToDecimal(rw["Debit"]);
                            totalCostofSales += Convert.ToDecimal(rw["Debit"]);
                        }
                        if(directExpenses != 0)
                        {
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "DIRECT EXPENSES";
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = directExpenses.ToString("N2");
                            //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                            dgvProfitAndLoss.Rows.Add();
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = totalCostofSales > 0 ? totalCostofSales.ToString("N2") : "(" + (totalCostofSales * -1).ToString("N2") + ")";
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = "______________";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtGroupId1"].Value = "11";

                    // ------------------------------- Gross profit or loss --------------------------------------------------- //
                    GrossProfitOrLoss = (TotalRevenue - totalCostofSales);
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "GROSS PROFIT/(LOSS)";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = GrossProfitOrLoss < 0 ? "(" + (GrossProfitOrLoss * -1).ToString("N2") : GrossProfitOrLoss.ToString("N2");

                    decgranIncTotal = GrossProfitOrLoss;

                    ///---Other Income 
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OTHER INCOME";
                    dgvProfitAndLoss.Rows.Add();
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[5];
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                            GrossProfitOrLoss += Convert.ToDecimal(rw["Credit"]);
                            dgvProfitAndLoss.Rows.Add();
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                        // --------------------------------------------- sum other income with gross profit (loss) ------------------------------- //
                        dgvProfitAndLoss.Rows.Add();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = GrossProfitOrLoss.ToString("N2");
                    }

                    // ----------------------------------------------- Operating Expenses ------------------------------------------- //
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING EXPENSES";

                    ///------Indirect Expense 
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[4];
                    dgvProfitAndLoss.Rows.Add();
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = rw["ledgerId"].ToString();
                            dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = Convert.ToDecimal(rw["Debit"]) > 0 ?
                                Convert.ToDecimal(rw["Debit"]).ToString("N2") : "(" + (Convert.ToDecimal(rw["Debit"]) * -1).ToString("N2") + ")";
                            OperatingExpenses += Convert.ToDecimal(rw["Debit"]);
                            dgvProfitAndLoss.Rows.Add();
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    }
                    // ----------------------------------------- Total operating expenses ------------------------------------- //
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = OperatingExpenses < 0 ? "(" + (OperatingExpenses * -1).ToString("N2") : OperatingExpenses.ToString("N2");

                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                    // ------------------------------------------ Operating profit (loss) ----------------------------------- //
                    dgvProfitAndLoss.Rows.Add();
                    OperatingProfitOrLos = GrossProfitOrLoss - OperatingExpenses;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING PROFIT (LOSS)";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = OperatingProfitOrLos < 0 ?
                        "(" + (OperatingProfitOrLos * -1).ToString("N2") : OperatingProfitOrLos.ToString("N2");

                    decgranExTotal = OperatingProfitOrLos;

                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                    //---- Calculating Grand total
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL1:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to convert datagridview data to a datatable
        /// </summary>
        /// <returns></returns>
        public DataTable GetDataTable()
        {
            DataTable dtblProfit = new DataTable();
            try
            {
                dtblProfit.Columns.Add("Income");
                dtblProfit.Columns.Add("Amount1");
                dtblProfit.Columns.Add("Amount2");
                dtblProfit.Columns.Add("frmDate");
                dtblProfit.Columns.Add("toDate");
                //dtblProfit.Columns.Add("Amount2");
                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvProfitAndLoss.Rows)
                {
                    drow = dtblProfit.NewRow();
                    drow["Income"] = dr.Cells["dgvtxtIncome"].Value;
                    drow["Amount1"] = dr.Cells["dgvtxtAmount1"].Value;
                    drow["Amount2"] = dr.Cells["dgvtxtAmount2"].Value;
                    drow["frmDate"] = txtFromDate.Text.ToString();
                    drow["toDate"] = txtToDate.Text.ToString();
                    //drow["Amount2"] = dr.Cells["dgvtxtAmount2"].Value;
                    dtblProfit.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL2:" + ex.Message;
            }
            return dtblProfit;
        }
        /// <summary>
        /// Function to convert datatable to dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            DataSet dsProfit = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblProfit = GetDataTable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.ProfitAndLossReportPrintCompany(PublicVariables._decCurrentCompanyId);//(PublicVariables._decCurrentCompanyId);
                dsProfit.Tables.Add(dtblProfit);
                dsProfit.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL3:" + ex.Message;
            }
            return dsProfit;
        }
        /// <summary>
        /// Function for Print
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        public void Print(DateTime fromDate, DateTime toDate)
        {
            try
            {
                DataSet dsProfit = GetDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.ProfitAndLossReportPrinting2(dsProfit);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL4:" + ex.Message;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Form Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmProfitAndLossByCostOfSales_Load(object sender, EventArgs e)
        {
            try
            {
                isFormLoad = true;
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                isFormLoad = false;
                txtFromDate.Focus();
                Gridfill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL5:" + ex.Message;
            }
        }
        /// <summary>
        /// Fills txtFromDate textbox on dtpContraVoucherDate DatetimePicker ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        /// Fills txtToDate textbox on dtpTodate DatetimePicker ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpTodate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpTodate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL7:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Search' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpTodate.Value >= dtpFromDate.Value)
                {
                    Gridfill();
                }
                else
                {
                    Messages.InformationMessage("To date less than from date");
                    dtpTodate.Value = PublicVariables._dtToDate;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL8:" + ex.Message;
            }
        }
        /// <summary>
        /// Calls corresponding Ledgers AccountGroupwise Report on cell double click in Datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvProfitAndLoss_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            #region by ledger
            try
            {
                if (e.RowIndex != -1)
                {
                    decimal decLedgerId = Convert.ToDecimal(dgvProfitAndLoss.Rows[e.RowIndex].Cells["dgvtxtLedgerId"].Value.ToString());
                    if (Convert.ToInt32(dgvProfitAndLoss.Rows[e.RowIndex].Cells["dgvtxtLedgerId"].Value.ToString()) != 0)
                    {
                        frmLedgerDetails frmLedgerDetailsObj = new frmLedgerDetails();
                        frmLedgerDetailsObj.MdiParent = formMDI.MDIObj;

                        frmLedgerDetailsObj.CallFromProfitAndLossByCostOfSales(this, decLedgerId, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AGWREP21:" + ex.Message;
            }
            #endregion
        }
        /// <summary>
        /// On 'Clear' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString();
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString();
                Gridfill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Print' button click to print
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (decgranExTotal == 0 && decgranIncTotal == 0)
                {
                    MessageBox.Show("No row to print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Print(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL11:" + ex.Message;
            }
        }
        /// <summary>
        /// Date validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL12:" + ex.Message;
            }
        }
        /// <summary>
        /// Date validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                }
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL13:" + ex.Message;
            }
        }
        #endregion
        #region Navigation
        /// <summary>
        /// Escape key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmProfitAndLossByCostOfSales_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (PublicVariables.isMessageClose)
                    {
                        Messages.CloseMessage(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL14:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtToDate.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL15:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    dgvProfitAndLoss.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    btnClear.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL16:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (txtToDate.SelectionLength != 11)
                    {
                        if (txtToDate.Text == string.Empty || txtToDate.SelectionStart == 0)
                        {
                            txtFromDate.Focus();
                            txtFromDate.SelectionStart = 0;
                            txtFromDate.SelectionLength = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL17:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnClear.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    txtToDate.Focus();
                    txtToDate.SelectionStart = 0;
                    txtToDate.SelectionLength = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL18:" + ex.Message;
            }
        }
        #endregion

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvProfitAndLoss, "STATEMENT OF PROFIT/LOSS AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }

        private void dtpTodate_ValueChanged_1(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpTodate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PLS1:" + ex.Message;
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = dtpFromDate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL6:" + ex.Message;
            }
        }
    }
}
