﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using MATFinancials.Classes.SP;

namespace MATFinancials.FinancialStatements
{
    public partial class frmProfitAndLossByProjectAndCategory : Form
    {
        #region Public Variables
        /// <summary>
        /// Public variable Declaration part
        /// </summary>
        string calculationMethod = string.Empty;
        bool isFormLoad = false;
        string strGroupId = string.Empty;
        string strLedgerId = string.Empty;
        int inCurrenRowIndex = 0;// To keep row index while returning from voucher 
        int inCurrentColunIndex = 0;// To keep column index while returning from voucher 
        string strformName = string.Empty;
        decimal decgranExTotal = 0;
        decimal decgranIncTotal = 0;
        Padding newPadding = new Padding(50, 0, 0, 0);
        #endregion
        #region Functions
        /// <summary>
        /// Creates an instance of frmProfitAndLossByProjectAndCategory class
        /// </summary>
        public frmProfitAndLossByProjectAndCategory()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Function to fill Datagridview
        /// </summary>
        public void Gridfill()
        {
            try
            {
                decimal TotalRevenue = 0;
                decimal TotalOpeningStockAndExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal OperatingExpenses = 0;
                decimal OperatingProfitOrLos = 0;
                decimal dcOpeningStock = 0;
                decimal dcClosingStock = 0;
                if (!isFormLoad)
                {
                    DateValidation objValidation = new DateValidation();
                    objValidation.DateValidationFunction(txtFromDate);
                    if (txtFromDate.Text == string.Empty)
                        txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    objValidation.DateValidationFunction(txtToDate);
                    if (txtToDate.Text == string.Empty)
                        txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    Font newFont = new Font(dgvProfitAndLoss.Font, FontStyle.Bold);
                    CurrencyInfo InfoCurrency = new CurrencyInfo();
                    CurrencySP SpCurrency = new CurrencySP();
                    InfoCurrency = SpCurrency.CurrencyView(1);
                    int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
                    dgvProfitAndLoss.Rows.Clear();
                    FinancialStatementSP spFinancial = new FinancialStatementSP();
                    DataTable dtblFinancial = new DataTable();
                    DataSet DsetProfitAndLoss = new DataSet();
                    SettingsInfo infoSettings = new SettingsInfo();
                    SettingsSP SpSettings = new SettingsSP();
                    //---------check  calculation method
                    if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                    {
                        calculationMethod = "FIFO";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                    {
                        calculationMethod = "Average Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                    {
                        calculationMethod = "High Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                    {
                        calculationMethod = "Low Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                    {
                        calculationMethod = "Last Purchase Rate";
                    }
                    if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0)
                    {
                        DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysis(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text));
                        dcOpeningStock = spFinancial.StockValueGetOnDate(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false);
                        dcClosingStock = spFinancial.StockValueGetOnDate(DateTime.Parse(txtToDate.Text), calculationMethod, false, false);
                    }
                    if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex > 0)
                    {
                        int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                        int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                        DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysisByProjectAndCategory(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), ProjectId, CategoryId);
                        dcOpeningStock = spFinancial.StockValueGetOnDateByProjectAndCategory(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false, ProjectId, CategoryId);
                        dcClosingStock = spFinancial.StockValueGetOnDateByProjectAndCategory(DateTime.Parse(txtToDate.Text), calculationMethod, false, false, ProjectId, CategoryId);
                    }
                    if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex == 0)
                    {
                        int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                        DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysisByProject(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), ProjectId);
                        dcOpeningStock = spFinancial.StockValueGetOnDateByProject(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false, ProjectId);
                        dcClosingStock = spFinancial.StockValueGetOnDateByProject(DateTime.Parse(txtToDate.Text), calculationMethod, false, false, ProjectId);
                    }
                    if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex > 0)
                    {
                        int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                        DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysisByCategory(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), CategoryId);
                        dcOpeningStock = spFinancial.StockValueGetOnDateByCategory(DateTime.Parse(txtFromDate.Text), DateTime.Parse(txtToDate.Text), calculationMethod, true, false, CategoryId);
                        dcClosingStock = spFinancial.StockValueGetOnDateByCategory(DateTime.Parse(txtToDate.Text), calculationMethod, false, false, CategoryId);
                    }
                    // ---------------------------------- REVENUE ----------------------------------//
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "REVENUE";
                    //---Sales Account  -Credit
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[1];
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Sales Accounts";
                    decimal dcSalesAccount = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = decimal.Parse(rw["Credit"].ToString().ToString());
                            dcSalesAccount += dcBalance;
                            TotalRevenue += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId2"].Value = "10";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcSalesAccount < 0 ? "(" + (dcSalesAccount * -1).ToString("N2") + ")" : dcSalesAccount.ToString("N2");
                    dgvProfitAndLoss.Rows.Add();
                    //----Direct Income 
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[3];
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Direct Incomes";
                    decimal dcDirectIncoome = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                            dcDirectIncoome += dcBalance;
                            TotalRevenue += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId2"].Value = "12";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcDirectIncoome.ToString();
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";

                    // ---------------------------------- Total Revenue ------------------------------------------------------------ //
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "TOTAL REVENUE";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2");

                    // ---------------------------------- COST OF SALES ----------------------------------//
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "COST OF SALES";
                    //---- Opening Stock
                    dgvProfitAndLoss.Rows.Add();
                    //this.dgvProfitAndLoss.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                    //this.dgvProfitAndLoss.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();

                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Opening Stock";
                    if (dcOpeningStock > 0)
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = Math.Round(dcOpeningStock, inDecimalPlaces).ToString("N2");
                    }
                    else
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = (Math.Round(dcOpeningStock * -1, inDecimalPlaces)).ToString("N2");
                    }
                    TotalOpeningStockAndExpenses += dcOpeningStock;
                    /// ---Purchases - Debit
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[0];
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Purchases";
                    decimal dcPurchaseAccount = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                            dcPurchaseAccount += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcPurchaseAccount < 0 ? "(" + (dcPurchaseAccount * -1).ToString("N2") : dcPurchaseAccount.ToString("N2");
                    TotalOpeningStockAndExpenses += dcPurchaseAccount;
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";

                    // ----------------------------- Sum opening stock and purchases -------------------------------------------------//
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = TotalOpeningStockAndExpenses < 0 ? "(" + (TotalOpeningStockAndExpenses * -1).ToString("N2") + ")" : TotalOpeningStockAndExpenses.ToString("N2");
                    decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;
                    //---Direct Expense
                    dgvProfitAndLoss.Rows.Add();
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[2];
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Direct Expenses";
                    decimal dcDirectExpense = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                            dcDirectExpense += dcBalance;
                            PlusDirectExpenses += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "13";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcDirectExpense < 0 ? "(" + (dcDirectExpense * -1).ToString("N2") + ")" : dcDirectExpense.ToString("N2");
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";

                    // ------------------------ add direct expenses to opening stock and purchases -----------------------------------------------//
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = PlusDirectExpenses < 0 ? "(" + (PlusDirectExpenses * -1).ToString("N2") + ")" : PlusDirectExpenses.ToString("N2");

                    //Closing Stock 
                    dgvProfitAndLoss.Rows.Add();

                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Closing Stock";
                    //dcClosingStock = spFinancial.StockValueGetOnDate(DateTime.Parse(txtToDate.Text), calculationMethod, false, false);
                    decimal ClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
                    decimal ClosingStockPlusDirectExpenses = Math.Round(PlusDirectExpenses - dcClosingStock, inDecimalPlaces);
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = ClosingStock < 0 ? "(" + (ClosingStock * -1).ToString("N2") + ")" : "(" + ClosingStock.ToString("N2") + ")";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = ClosingStockPlusDirectExpenses < 0 ? "(" + (ClosingStockPlusDirectExpenses * -1).ToString("N2") + ")" : ClosingStockPlusDirectExpenses.ToString("N2");
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";

                    // ------------------------------- Gross profit or loss --------------------------------------------------- //
                    GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - dcClosingStock);
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "GROSS PROFIT/(LOSS)";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = GrossProfitOrLoss < 0 ? "(" + (GrossProfitOrLoss * -1).ToString("N2") : GrossProfitOrLoss.ToString("N2");

                    dgvProfitAndLoss.Rows.Add();
                    ///---Other Income 
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[5];
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Other Income";
                    decimal dcIndirectIncome = 0m;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                            dcIndirectIncome += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId2"].Value = "14";
                    }
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcIndirectIncome < 0 ? "(" + dcIndirectIncome.ToString("N2") + ")" : dcIndirectIncome.ToString("N2");
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";

                    // --------------------------------------------- sum other income with gross profit (loss) ------------------------------- //
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = (dcIndirectIncome + GrossProfitOrLoss) < 0 ?
                        "(" + (dcIndirectIncome + GrossProfitOrLoss * -1).ToString("N2") + ")" : (dcIndirectIncome + GrossProfitOrLoss).ToString("N2");
                    OperatingProfitOrLos = OperatingProfitOrLos + (dcIndirectIncome + GrossProfitOrLoss);

                    #region Old Code
                    /*
                    decimal dcTotalExpense = 0;
                    decimal dcTotalIncome = 0;
                    dcTotalExpense = dcOpeningStock + dcPurchaseAccount + dcDirectExpense;
                    dcTotalIncome = dcClosingStock + dcSalesAccount + dcDirectIncoome;
                    dcTotalExpense = Math.Round(dcTotalExpense, inDecimalPlaces);
                    dcTotalIncome = Math.Round(dcTotalIncome, inDecimalPlaces);
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtExpenses"].Value = "Total";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Total";
                    decimal dcGrossProfit = 0;
                    decimal dcGrossLoss = 0;
                    if (dcTotalExpense > dcTotalIncome)
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcTotalExpense.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcTotalExpense.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtIncome"].Value = "Gross Loss b/d ";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount2"].Value = dcTotalExpense - dcTotalIncome;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount2"].Style.ForeColor = Color.Red;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtIncome"].Style.ForeColor = Color.Red;
                        dgvProfitAndLoss.Rows.Add();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtExpenses"].Value = "Gross Loss b/d ";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcTotalExpense - dcTotalIncome;
                        dcGrossLoss = dcTotalExpense - dcTotalIncome;
                    }
                    else
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcTotalIncome.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcTotalIncome.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtExpenses"].Value = "Gross Profit c/d ";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount1"].Value = dcTotalIncome - dcTotalExpense;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount1"].Style.ForeColor = Color.Green;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtExpenses"].Style.ForeColor = Color.Green;
                        dgvProfitAndLoss.Rows.Add();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Gross Profit c/d ";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcTotalIncome - dcTotalExpense;
                        dcGrossProfit = dcTotalIncome - dcTotalExpense;
                    }
                    dgvProfitAndLoss.Rows.Add();
                    */
                    #endregion
                    // ----------------------------------------------- Operating Expenses ------------------------------------------- //
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING EXPENSES";

                    ///------Indirect Expense 
                    dtblFinancial = new DataTable();
                    dtblFinancial = DsetProfitAndLoss.Tables[4];
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Indirect Expenses";
                    decimal dcIndirectExpense = 0;
                    if (dtblFinancial.Rows.Count > 0)
                    {
                        foreach (DataRow rw in dtblFinancial.Rows)
                        {
                            decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                            dcIndirectExpense += dcBalance;
                        }
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "15";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcIndirectExpense < 0 ? "(" + (dcIndirectExpense * -1).ToString("N2") : dcIndirectExpense.ToString("N2");
                        OperatingExpenses += dcIndirectExpense;
                    }
                    // ----------------------------------------- Total operating expenses ------------------------------------- //
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = OperatingExpenses < 0 ? "(" + (OperatingExpenses * -1).ToString("N2") : OperatingExpenses.ToString("N2");

                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                    // ------------------------------------------ Operating profit (loss) ----------------------------------- //
                    dgvProfitAndLoss.Rows.Add();
                    OperatingProfitOrLos = OperatingProfitOrLos - OperatingExpenses;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING PROFIT (LOSS)";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = OperatingProfitOrLos < 0 ?
                        "(" + (OperatingProfitOrLos * -1).ToString("N2") : OperatingProfitOrLos.ToString("N2");
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows.Add();
                    //---- Calculating Grand total
                    #region Old Code
                    /*
                    decimal dcGrandTotalExpense = dcGrossLoss + dcIndirectExpense;
                    decimal dcGrandTotalIncome = dcGrossProfit + dcIndirectIncome;
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "_______________________";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "_______________________";
                    dgvProfitAndLoss.Rows.Add();
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtExpenses"].Value = "Grand Total";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "Grand Total";
                    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                    if (dcGrandTotalExpense > dcGrandTotalIncome)
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcGrandTotalExpense.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcGrandTotalExpense.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtIncome"].Value = "Net Loss ";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount2"].Value = dcGrandTotalExpense - dcGrandTotalIncome;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtIncome"].Style.ForeColor = Color.Red;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount2"].Style.ForeColor = Color.Red;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].DefaultCellStyle.Font = newFont;
                        decgranExTotal = dcGrandTotalExpense;
                    }
                    else
                    {
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = dcGrandTotalIncome.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = dcGrandTotalIncome.ToString();
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtExpenses"].Value = "Net Profit";
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount1"].Value = dcGrandTotalIncome - dcGrandTotalExpense;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtExpenses"].Style.ForeColor = Color.Green;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].Cells["dgvtxtAmount1"].Style.ForeColor = Color.Green;
                        dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 3].DefaultCellStyle.Font = newFont;
                        decgranIncTotal = dcGrandTotalIncome;
                    }
                    if (dgvProfitAndLoss.Columns.Count > 0)
                    {
                        dgvProfitAndLoss.Columns["dgvtxtAmount1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvProfitAndLoss.Columns["dgvtxtAmount2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    }
                    if (inCurrenRowIndex >= 0 && dgvProfitAndLoss.Rows.Count > 0 && inCurrenRowIndex < dgvProfitAndLoss.Rows.Count)
                    {
                        if (dgvProfitAndLoss.Rows[inCurrenRowIndex].Cells[inCurrentColunIndex].Visible)
                        {
                            dgvProfitAndLoss.CurrentCell = dgvProfitAndLoss.Rows[inCurrenRowIndex].Cells[inCurrentColunIndex];
                        }
                        else
                        {
                            dgvProfitAndLoss.CurrentCell = dgvProfitAndLoss.Rows[inCurrenRowIndex].Cells["dgvtxtExpenses"];
                        }
                        dgvProfitAndLoss.CurrentCell.Selected = true;
                    }
                    inCurrenRowIndex = 0;
                    */
                    #endregion
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL1:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to convert datagridview data to a datatable
        /// </summary>
        /// <returns></returns>
        public DataTable GetDataTable()
        {
            DataTable dtblProfit = new DataTable();
            try
            {
                dtblProfit.Columns.Add("Expenses");
                dtblProfit.Columns.Add("Amount1");
                dtblProfit.Columns.Add("Income");
                dtblProfit.Columns.Add("Amount2");
                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvProfitAndLoss.Rows)
                {
                    drow = dtblProfit.NewRow();
                    drow["Expenses"] = dr.Cells["dgvtxtExpenses"].Value;
                    drow["Amount1"] = dr.Cells["dgvtxtAmount1"].Value;
                    drow["Income"] = dr.Cells["dgvtxtIncome"].Value;
                    drow["Amount2"] = dr.Cells["dgvtxtAmount2"].Value;
                    dtblProfit.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL2:" + ex.Message;
            }
            return dtblProfit;
        }
        /// <summary>
        /// Function to convert datatable to dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            DataSet dsProfit = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblProfit = GetDataTable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.ProfitAndLossReportPrintCompany(PublicVariables._decCurrentCompanyId);//(PublicVariables._decCurrentCompanyId);
                dsProfit.Tables.Add(dtblProfit);
                dsProfit.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL3:" + ex.Message;
            }
            return dsProfit;
        }
        /// <summary>
        /// Function for Print
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        public void Print(DateTime fromDate, DateTime toDate)
        {
            try
            {
                DataSet dsProfit = GetDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.ProfitAndLossReportPrinting(dsProfit);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL4:" + ex.Message;
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Form Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmProfitAndLossByProjectAndCategory_Load(object sender, EventArgs e)
        {
            try
            {
                isFormLoad = true;
                dtpContraVoucherDate.MinDate = PublicVariables._dtFromDate;
                dtpContraVoucherDate.MaxDate = PublicVariables._dtToDate;
                dtpContraVoucherDate.Value = PublicVariables._dtFromDate;
                dtpContraVoucherDate.Text = dtpContraVoucherDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                isFormLoad = false;
                txtFromDate.Focus();
                ProjectComboFill();
                CategoryComboFill();
                Gridfill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL5:" + ex.Message;
            }
        }
        private void CategoryComboFill()
        {
            try
            {
                CategorySP categorySP = new CategorySP();
                DataTable dt = categorySP.CategoryViewAll();
                DataRow dr = dt.NewRow();
                dr["CategoryId"] = "0";
                dr["CategoryName"] = "NA";
                dt.Rows.InsertAt(dr, 0);

                cmbCategory.DataSource = dt;
                cmbCategory.DisplayMember = "CategoryName";
                cmbCategory.ValueMember = "CategoryId";
            }

            catch (Exception ex)
            {

            }
        }

        private void ProjectComboFill()
        {
            try
            {
                ProjectSP ProjectSP = new ProjectSP();
                DataTable dt = ProjectSP.ProjectViewAll();
                DataRow dr = dt.NewRow();
                dr["ProjectId"] = "0";
                dr["ProjectName"] = "NA";
                dt.Rows.InsertAt(dr, 0);

                cmbProject.DataSource = dt;
                cmbProject.DisplayMember = "ProjectName";
                cmbProject.ValueMember = "ProjectId";
            }

            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// Fills txtFromDate textbox on dtpContraVoucherDate DatetimePicker ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpContraVoucherDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = dtpContraVoucherDate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL6:" + ex.Message;
            }
        }
        /// <summary>
        /// Fills txtToDate textbox on dtpTodate DatetimePicker ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpTodate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpTodate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL7:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Search' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtpTodate.Value >= dtpContraVoucherDate.Value)
                {
                    Gridfill();
                }
                else
                {
                    Messages.InformationMessage("To date less than from date");
                    dtpTodate.Value = PublicVariables._dtToDate;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL8:" + ex.Message;
            }
        }
        /// <summary>
        /// Calls corresponding Ledgers AccountGroupwise Report on cell double click in Datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvProfitAndLoss_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvProfitAndLoss.CurrentRow.Index != -1 && dgvProfitAndLoss.CurrentCell.ColumnIndex != -1)
                {
                    if (dgvProfitAndLoss.CurrentCell != null)
                    {
                        if (dgvProfitAndLoss.CurrentCell.Value != null && dgvProfitAndLoss.CurrentCell.Value.ToString().Trim() != string.Empty)
                        {
                            int inRowIndex = dgvProfitAndLoss.CurrentCell.RowIndex;
                            int inColIndex = dgvProfitAndLoss.CurrentCell.ColumnIndex;
                            string strGroupId = string.Empty;
                            if (dgvProfitAndLoss.Columns[inColIndex].Name == "dgvtxtExpenses" || dgvProfitAndLoss.Columns[inColIndex].Name == "dgvtxtAmount1")
                            {
                                try
                                {
                                    if (dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId1"].Value != null && dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId1"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId1"].Value.ToString();
                                    }
                                }
                                catch
                                {
                                    strGroupId = string.Empty;
                                }
                            }
                            else if (dgvProfitAndLoss.Columns[inColIndex].Name == "dgvtxtIncome" || dgvProfitAndLoss.Columns[inColIndex].Name == "dgvtxtAmount2")
                            {
                                try
                                {
                                    if (dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId2"].Value != null && dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId2"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvProfitAndLoss.Rows[inRowIndex].Cells["dgvtxtGroupId2"].Value.ToString();
                                    }
                                }
                                catch
                                {
                                    strGroupId = string.Empty;
                                }
                            }
                            if (strGroupId != string.Empty)
                            {
                                inCurrenRowIndex = inRowIndex;
                                inCurrentColunIndex = inColIndex;
                                frmAccountGroupwiseReport objForm = new frmAccountGroupwiseReport();
                                objForm.WindowState = FormWindowState.Normal;
                                objForm.MdiParent = formMDI.MDIObj;
                                objForm.CallFromProfitAndLossByProjectAndCategory(txtFromDate.Text, txtToDate.Text, strGroupId, this);
                                this.Enabled = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL9:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Clear' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                dtpContraVoucherDate.MinDate = PublicVariables._dtFromDate;
                dtpContraVoucherDate.MaxDate = PublicVariables._dtToDate;
                dtpContraVoucherDate.Value = PublicVariables._dtFromDate;
                dtpContraVoucherDate.Text = dtpContraVoucherDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString();
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString();
                Gridfill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Print' button click to print
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (decgranExTotal == 0 && decgranIncTotal == 0)
                {
                    MessageBox.Show("No row to print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Print(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL11:" + ex.Message;
            }
        }
        /// <summary>
        /// Date validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL12:" + ex.Message;
            }
        }
        /// <summary>
        /// Date validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                }
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL13:" + ex.Message;
            }
        }
        #endregion
        #region Navigation
        /// <summary>
        /// Escape key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmProfitAndLossByProjectAndCategory_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (PublicVariables.isMessageClose)
                    {
                        Messages.CloseMessage(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL14:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtToDate.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL15:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    dgvProfitAndLoss.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    btnClear.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL16:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (txtToDate.SelectionLength != 11)
                    {
                        if (txtToDate.Text == string.Empty || txtToDate.SelectionStart == 0)
                        {
                            txtFromDate.Focus();
                            txtFromDate.SelectionStart = 0;
                            txtFromDate.SelectionLength = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL17:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnClear.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    txtToDate.Focus();
                    txtToDate.SelectionStart = 0;
                    txtToDate.SelectionLength = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL18:" + ex.Message;
            }
        }
        #endregion

        private void btnClearProjectAndCategory_Click(object sender, EventArgs e)
        {
            cmbProject.SelectedIndex = 0;
            cmbCategory.SelectedIndex = 0;
            Gridfill();
        }

        private void btnSearchProjectAndCategory_Click(object sender, EventArgs e)
        {
            Gridfill();
        }
    }
}
