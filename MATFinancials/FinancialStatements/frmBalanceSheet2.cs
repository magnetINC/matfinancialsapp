﻿using MATFinancials.Classes.HelperClasses;
using MATFinancials.Classes.SP;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.FinancialStatements
{
    public partial class frmBalanceSheet2 : Form
    {
        public frmBalanceSheet2()
        {
            InitializeComponent();
        }

        private void frmBalanceSheet2_Load(object sender, EventArgs e)
        {
            try
            {
                ProjectComboFill();
                CategoryComboFill();
                FillGrid();
                txtToDate.Text = PublicVariables._dtCurrentDate.ToString();
                isFormLoad = true;
                dtpdate.MinDate = PublicVariables._dtFromDate;
                dtpdate.MaxDate = PublicVariables._dtToDate;
                dtpdate.Value = PublicVariables._dtToDate;
                txtToDate.Text = dtpdate.Value.ToString("dd-MMM-yyyy");
                isFormLoad = false;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS8:" + ex.Message;
            }
        }
        private void CategoryComboFill()
        {
            try
            {
                CategorySP categorySP = new CategorySP();
                DataTable dt = categorySP.CategoryViewAll();
                DataRow dr = dt.NewRow();
                dr["CategoryId"] = "0";
                dr["CategoryName"] = "All";
                dt.Rows.InsertAt(dr, 0);

                cmbCategory.DataSource = dt;
                cmbCategory.DisplayMember = "CategoryName";
                cmbCategory.ValueMember = "CategoryId";
            }
            catch (Exception ex)
            {

            }
        }

        private void ProjectComboFill()
        {
            try
            {
                ProjectSP ProjectSP = new ProjectSP();
                DataTable dt = ProjectSP.ProjectViewAll();
                DataRow dr = dt.NewRow();
                dr["ProjectId"] = "0";
                dr["ProjectName"] = "All";
                dt.Rows.InsertAt(dr, 0);

                cmbProject.DataSource = dt;
                cmbProject.DisplayMember = "ProjectName";
                cmbProject.ValueMember = "ProjectId";
            }
            catch (Exception ex)
            {

            }
        }
        #region Public Variables
        /// <summary>
        /// public variable declaration part
        /// </summary>
        bool isFormLoad = false;
        int inCurrenRowIndex = 0;
        int inCurentcolIndex = 0;
        string calculationMethod = string.Empty;
        decimal decPrintOrNot = 0;
        decimal decPrintOrNot1 = 0;
        DateTime dtfromdate = PublicVariables._dtFromDate;
        Padding newPadding = new Padding(50, 0, 0, 0);
        #endregion
        #region FUNCTIONS

        /// <summary>
        /// Function to fill Datagridview
        /// </summary>
        public void FillGrid()
        {
            try
            {
                bool ShowInventory = false;
                bool ShowProfitAndLoss = false;
                decimal Cost = 0;
                decimal AccDep = 0;
                decimal TotalEquitiesAndLiabilities = 0;
                decimal Total = 0;
                decimal TotalELSubGroup = 0;
                int OrderNumber = 0;
                string PropertyPlantsAndEquipment = "";
                decimal ClosingStock = 0;
                decimal ClosingStockForRollOver = 0;
                DataSet dsetProfitAndLoss = null;
                DataSet dsetProfitAndLossRollOver = null;
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                //DateTime lastFinYearEnd = DateTime.Parse(txtFromDate.Text); // conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(PublicVariables._dtFromDate.AddDays(-1));
                }
                if (!isFormLoad)
                {
                    GeneralQueries methodForStockValue = new GeneralQueries();
                    DateValidation objValidation = new DateValidation();
                    objValidation.DateValidationFunction(txtToDate);
                    if (txtToDate.Text == string.Empty)
                        txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    Font newFont = new Font(dgvReport.Font, FontStyle.Bold);
                    CurrencyInfo InfoCurrency = new CurrencyInfo();
                    CurrencySP SpCurrency = new CurrencySP();
                    InfoCurrency = SpCurrency.CurrencyView(1);
                    int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
                    dgvReport.Rows.Clear();
                    FinancialStatementSP SpFinance = new FinancialStatementSP();
                    DataSet DsetBalanceSheet = new DataSet();
                    DataSet DsetBalanceSheetForRollOver = new DataSet();
                    DataTable dtbl = new DataTable();
                    DataTable dtblGroup = new DataTable();
                    DataTable dtblSubGroups = new DataTable();
                    SettingsInfo InfoSettings = new SettingsInfo();
                    SettingsSP SpSettings = new SettingsSP();
                    AccountGroupSP spAccountGroup = new AccountGroupSP();
                    //--------------- Selection Of Calculation Method According To Settings ------------------// 
                    if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                    {
                        calculationMethod = "FIFO";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                    {
                        calculationMethod = "Average Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                    {
                        calculationMethod = "High Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                    {
                        calculationMethod = "Low Cost";
                    }
                    else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                    {
                        calculationMethod = "Last Purchase Rate";
                    }

                    int LiabilityRow = 0;
                    int AssetRow = 0;
                    decimal TotalAssets = 0;
                    decimal TotalLiabilities = 0;
                    decimal TotalPPE = 0;
                    decimal TotalNBV = 0;
                    decimal TotalEL = 0;
                    decimal TotalInventory = 0;
                    decimal dcOpeningStockForRollOver = 0;
                    decimal dcOpeningStockForBalanceSheetRollOver = 0;
                    decimal TotalCost = 0;
                    decimal TotalDep = 0;
                    decimal OpeningStock = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true);
                    OpeningStock = Math.Round(OpeningStock, inDecimalPlaces);
                    dcOpeningStockForBalanceSheetRollOver = SpFinance.StockValueGetOnDateForRollOver(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true, true);
                    dcOpeningStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                    OpeningStock = OpeningStock + dcOpeningStockForRollOver;
                    DsetBalanceSheet = SpFinance.BalanceSheet(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text));
                    DsetBalanceSheetForRollOver = SpFinance.BalanceSheetForRollOver(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text));

                    //------------------- Asset -------------------------------// 
                    //------------------- Non Current Asset -------------------------------// 
                    dtbl = DsetBalanceSheet.Tables[0];
                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "NON CURRENT ASSETS".ToUpper();

                    dgvReport.Rows.Add();
                    AssetRow++;

                    dgvReport.Rows[AssetRow].Cells["Amount1"].Value = "Cost";
                    dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "Acc. Depreciation";
                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "NBV";

                    AssetRow++;

                    foreach (DataRow rw in dtbl.Rows)
                    {
                        int id = Convert.ToInt32(rw["ID"].ToString());
                        dgvReport.Rows.Add();
                        // Remove alternating cell style formatting 
                        //this.dgvReport.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                        //this.dgvReport.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();
                        decimal balance = (rw["ID"].ToString() == "6") ? Convert.ToDecimal(rw["Balance"].ToString()) + OpeningStock : Convert.ToDecimal(rw["Balance"].ToString());
                        PropertyPlantsAndEquipment = rw["Name"].ToString().ToLower();

                        if (balance > 0)
                        {
                            dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = rw["Name"].ToString().ToUpper();
                            dgvReport.Rows[AssetRow].Cells["GroupId1"].Value = rw["ID"].ToString();
                            AssetRow++;
                        }
                        else
                        {
                            balance = balance * -1;
                            dgvReport.Rows.Add();
                            dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = rw["Name"].ToString().ToUpper();
                            dgvReport.Rows[AssetRow].Cells["GroupId2"].Value = rw["ID"].ToString();
                            TotalLiabilities += balance;
                            AssetRow++;
                        }

                        // -------------------- place inventories in current assets --------------------------------------- //
                        if (rw["Name"].ToString().ToLower() == "current assets")
                        {
                            ShowInventory = true;
                        }
                        //----------------- get ledgers under the groups with the current id add them to gridview -------------------//
                        if (Convert.ToInt32(rw["ID"].ToString()) == id)
                        {
                            //----------------- Inventories -------------------//
                            if (ShowInventory)
                            {
                                decimal Inventories = 0;
                                //Inventories = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false); // old implementation 20171801
                                Inventories = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(txtToDate.Text), false, 0, false, DateTime.Now);

                                #region For Balance Sheet by project and category
                                //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0)
                                //{
                                //    Inventories = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false);
                                //    ClosingStock = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false);
                                //    dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheet(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text));
                                //    OpeningStock = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true);
                                //}
                                //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex > 0)
                                //{
                                //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                                //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                                //    Inventories = SpFinance.StockValueGetOnDateByProjectAndCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, ProjectId, CategoryId);
                                //    ClosingStock = SpFinance.StockValueGetOnDateByProjectAndCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, ProjectId, CategoryId);
                                //    dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheetByProjectAndCategory(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text), ProjectId, CategoryId);
                                //    OpeningStock = SpFinance.StockValueGetOnDateByProjectAndCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true, ProjectId, CategoryId);
                                //}
                                //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex == 0)
                                //{
                                //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                                //    Inventories = SpFinance.StockValueGetOnDateByProject(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, ProjectId);
                                //    ClosingStock = SpFinance.StockValueGetOnDateByProject(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, ProjectId);
                                //    dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheetByProject(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text), ProjectId);
                                //    OpeningStock = SpFinance.StockValueGetOnDateByProject(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true, ProjectId);
                                //}
                                //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex > 0)
                                //{
                                //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                                //    Inventories = SpFinance.StockValueGetOnDateByCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, CategoryId);
                                //    ClosingStock = SpFinance.StockValueGetOnDateByCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false, CategoryId);
                                //    dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheetByCategory(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text), CategoryId);
                                //    OpeningStock = SpFinance.StockValueGetOnDateByCategory(Convert.ToDateTime(txtToDate.Text), calculationMethod, true, true, CategoryId);
                                //}
                                #endregion
                                Inventories = Math.Round(Inventories, inDecimalPlaces); // - OpeningStock;
                                if (Inventories != 0)
                                {
                                    TotalInventory += Inventories;
                                    if (Inventories >= 0)
                                    {
                                        dgvReport.Rows.Add();
                                        dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                        dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Inventories";
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = Inventories.ToString("N2");
                                        //dgvReport.Rows[AssetRow].Cells["Amount3"].Value = Inventories.ToString("N2");
                                        AssetRow++;
                                    }
                                    else
                                    {
                                        Inventories = Inventories * -1;
                                        dgvReport.Rows.Add();
                                        dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                        //dgvReport.Rows[AssetRow].Cells["Amount2"].Style.ForeColor = Color.Red;
                                        dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Inventories";
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + Inventories.ToString("N2") + ")";
                                        //dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "(" + Inventories.ToString("N2") + ")";
                                        //TotalLiabilities += Inventories;
                                        AssetRow++;
                                    }
                                }
                                ShowInventory = false;
                            }
                            dtblGroup = new DataTable();

                            if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0)
                            {
                                dtblGroup = spAccountGroup.AccountGroupWiseReportViewAll(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text));
                            }
                            if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex > 0)
                            {
                                int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                                int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                                dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProjectAndCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId, CategoryId);
                            }
                            if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex == 0)
                            {
                                int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                                dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProject(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId);
                            }
                            if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex > 0)
                            {
                                int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                                dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), CategoryId);
                            }
                            if (dtblGroup != null && dtblGroup.Rows.Count > 0)
                            {
                                TotalPPE = 0;
                                Total = 0;
                                TotalDep = 0 + TotalInventory;
                                TotalCost = 0;
                                TotalNBV = 0;

                                foreach (DataRow row in dtblGroup.Rows)
                                {
                                    //decimal PPEbalance = Convert.ToDecimal(row["balance1"].ToString());
                                    decimal NBV = Convert.ToDecimal(row["balance1"].ToString());
                                    decimal debit = Convert.ToDecimal(row["debit"]);
                                    decimal credit = Convert.ToDecimal(row["credit"]);
                                    decimal PPEbalance = debit - credit;
                                    dgvReport.Rows.Add();
                                    dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = row["name"].ToString();
                                    //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = NBV < 0 ? "(" + (NBV * -1).ToString("N2") + ")" : NBV.ToString("N2"); //Convert.ToDecimal(row["NBV"]).ToString("N2");
                                    dgvReport.Rows[AssetRow].Cells["Amount2"].Value = PPEbalance < 0 ? "(" + (PPEbalance * -1).ToString("N2") + ")" : PPEbalance.ToString("N2"); //Convert.ToDecimal(row["PPEbalance"]).ToString("N2");
                                    if (PropertyPlantsAndEquipment == "property plant & equipment")
                                    {
                                        dgvReport.Rows[AssetRow].Cells["Amount1"].Value = debit < 0 ? "(" + (debit * -1).ToString("N2") + ")" : debit.ToString("N2"); // Convert.ToDecimal(row["debit"]).ToString("N2");
                                        //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = credit < 0 ? "(" + (credit * -1).ToString("N2") + ")" : credit.ToString("N2"); //Convert.ToDecimal(row["credit"]).ToString("N2");
                                        if (credit < 0)
                                        {
                                            credit = credit * -1;
                                        }
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + (credit).ToString("N2") + ")";
                                        dgvReport.Rows[AssetRow].Cells["Amount3"].Value = PPEbalance < 0 ? "(" + (PPEbalance * -1).ToString("N2") + ")" : PPEbalance.ToString("N2");
                                    }
                                    TotalCost += debit;
                                    TotalDep += credit;
                                    TotalPPE += PPEbalance;
                                    TotalNBV += NBV;

                                    AssetRow++;
                                }
                                dgvReport.Rows.Add();
                                if (PropertyPlantsAndEquipment == "property plant & equipment")
                                {
                                    dgvReport.Rows[AssetRow].Cells["Amount1"].Value = "__________";
                                }
                                dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "__________";
                                dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "__________";
                                AssetRow++;
                                dgvReport.Rows.Add();
                                if (PropertyPlantsAndEquipment == "property plant & equipment")
                                {
                                    dgvReport.Rows[AssetRow].Cells["Amount1"].Value = TotalCost < 0 ? "(" + (TotalCost * -1).ToString("N2") + ")" : TotalCost.ToString("N2");
                                    //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = TotalDep < 0 ? "(" + (TotalDep * -1).ToString("N2") + ")" : TotalDep.ToString("N2");
                                    if (TotalDep < 0)
                                    {
                                        TotalDep = TotalDep * -1;
                                    }
                                    dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + (TotalDep).ToString("N2") + ")";
                                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = TotalPPE < 0 ? "(" + (TotalPPE * -1).ToString("N2") + ")" : TotalPPE.ToString("N2");
                                    Total += TotalPPE;
                                    dgvReport.Rows[AssetRow].Cells["GroupId1"].Value = rw["ID"].ToString();
                                }
                                else
                                {
                                    //decimal SummedValue = (TotalNBV + TotalInventory);
                                    decimal SummedValue = (TotalPPE + TotalInventory);
                                    dgvReport.Rows[AssetRow - 2].Cells["Amount3"].Value = SummedValue < 0 ? "(" + (SummedValue * -1).ToString("N2") + ")" : SummedValue.ToString("N2");
                                    dgvReport.Rows[AssetRow - 2].Cells["GroupId1"].Value = rw["ID"].ToString();
                                    //Total += (TotalNBV + TotalInventory);
                                    Total += (TotalPPE + TotalInventory);
                                    AssetRow = AssetRow - 2;
                                }
                                TotalAssets += Total;
                                AssetRow++;
                                dgvReport.Rows.Add();
                                dgvReport.Rows[AssetRow].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                                //dgvReport.Rows[AssetRow].DefaultCellStyle.Font.Underline = DataGridViewContentAlignment.TopRight;
                                if (PropertyPlantsAndEquipment == "property plant & equipment")
                                {
                                    dgvReport.Rows[AssetRow].Cells["Amount1"].Value = "__________";
                                    dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "__________";
                                }

                                // ------------------ pass the group id into the current row with the summed value -------------------- //
                                dgvReport.Rows[AssetRow].Cells["GroupId1"].Value = rw["ID"].ToString();
                                // ----------------------------------------------------------------------------------------------//
                                AssetRow++;
                            }
                        }
                    }
                    // --------------------------------------------------- TOTAL ASSETS ---------------------------------------------------- //
                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "TOTAL ASSETS";
                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = TotalAssets < 0 ? "(" + (TotalAssets * -1).ToString("N2") + ")" : (TotalAssets).ToString("N2");

                    decPrintOrNot = TotalAssets;

                    AssetRow++;
                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "____________";
                    AssetRow++;

                    //------------------------------------------ EQUITIES AND LIABILITIES ------------------------------------------//
                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "EQUITIES AND LIABILITIES";
                    AssetRow++;

                    dtbl = DsetBalanceSheet.Tables[1];
                    foreach (DataRow rw in dtbl.Rows)
                    {
                        int id = Convert.ToInt32(rw["ID"].ToString());
                        decimal balance = Convert.ToDecimal(rw["Balance"].ToString());
                        if (balance < 0)
                        {
                            dgvReport.Rows.Add();
                            dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = rw["name"].ToString();
                            dgvReport.Rows[AssetRow].Cells["GroupId2"].Value = Convert.ToDecimal(rw["ID"]).ToString();
                            AssetRow++;
                        }
                        else
                        {
                            dgvReport.Rows.Add();
                            dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = rw["Name"].ToString();
                            dgvReport.Rows[AssetRow].Cells["GroupId2"].Value = Convert.ToDecimal(rw["ID"]).ToString();
                            TotalLiabilities += balance;
                            AssetRow++;
                        }
                        if (rw["Name"].ToString().ToLower() == "equity and reserves")
                        {
                            OrderNumber = 1;
                        }
                        if (Convert.ToInt32(rw["ID"].ToString()) == 1)
                        {
                            ShowProfitAndLoss = true;
                        }
                        //----------------- get ledgers of groups with the current id but position Non-Current Liabilities ontop Current Liabilities with order number 2 as the second to come -------------------//
                        if (Convert.ToInt32(rw["ID"].ToString()) == id)
                        {
                            OrderNumber++;
                            dtblGroup = new DataTable();
                            dtblGroup = spAccountGroup.AccountGroupWiseReportViewAll(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text));


                            #region for balance sheet by project and catgory
                            //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0)
                            //{
                            //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAll(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text));
                            //}
                            //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex > 0)
                            //{
                            //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                            //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                            //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProjectAndCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId, CategoryId);
                            //}
                            //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex == 0)
                            //{
                            //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                            //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProject(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId);
                            //}
                            //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex > 0)
                            //{
                            //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                            //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), CategoryId);
                            //}
                            #endregion

                            if (dtblGroup != null && dtblGroup.Rows.Count > 0)
                            {
                                TotalELSubGroup = 0;
                                foreach (DataRow row in dtblGroup.Rows)
                                {
                                    decimal Balance1 = Convert.ToDecimal(row["balance1"].ToString());
                                    decimal debit = Convert.ToDecimal(row["debit"].ToString());
                                    decimal credit = Convert.ToDecimal(row["credit"].ToString());
                                    decimal Balance2 = credit - debit;
                                    dgvReport.Rows.Add();
                                    dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = row["name"].ToString();
                                    dgvReport.Rows[AssetRow].Cells["Amount2"].Value = Balance2.ToString("N2");
                                    if (Balance2 > 0)
                                    {
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = (Balance2).ToString("N2");
                                    }
                                    if (Balance2 < 0)
                                    {
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + (Balance2 * -1).ToString("N2") + ")";
                                    }
                                    TotalELSubGroup += Balance2;
                                    AssetRow++;
                                }

                                //----------------------------- Profit or loss Account ---------------------------//
                                #region p/l here
                                //----------------------------- Closing Stock--------------------------------------------------- //
                                
                                if (ShowProfitAndLoss)
                                {
                                    #region OLD
                                    /*
                                        //ClosingStock = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false);
                                        ClosingStock = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, Convert.ToDateTime(txtToDate.Text), false, 0, false, DateTime.Now);
                                        ClosingStock = Math.Round(ClosingStock, inDecimalPlaces) - OpeningStock;

                                        ClosingStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                                        ClosingStockForRollOver = Math.Round(ClosingStockForRollOver, inDecimalPlaces) - dcOpeningStockForBalanceSheetRollOver;

                                        dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheet(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text));

                                        decimal dcProfit = 0;
                                        for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
                                        {
                                            dtbl = dsetProfitAndLoss.Tables[i];
                                            decimal dcSum = 0;
                                            if (i == 0 || (i % 2) == 0)
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                                                    dcProfit = dcProfit - dcSum;
                                                }
                                            }
                                            else
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                                                    dcProfit = dcProfit + dcSum;
                                                }
                                            }
                                        }

                                        DataSet dsetProfitAndLossOpening = SpFinance.ProfitAndLossAnalysisUpToaDateForPreviousYears(PublicVariables._dtFromDate);
                                        decimal dcProfitOpening = 0;
                                        for (int i = 0; i < dsetProfitAndLossOpening.Tables.Count; ++i)
                                        {
                                            dtbl = dsetProfitAndLossOpening.Tables[i];
                                            decimal dcSum = 0;
                                            if (i == 0 || (i % 2) == 0)
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                                                    dcProfitOpening = dcProfitOpening - dcSum;
                                                }
                                            }
                                            else
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                                                    dcProfitOpening = dcProfitOpening + dcSum;
                                                }
                                            }
                                        }

                                        dsetProfitAndLossRollOver = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheetForRollOver(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text));

                                        decimal dcProfitRollOver = 0;
                                        for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
                                        {
                                            dtbl = dsetProfitAndLossRollOver.Tables[i];
                                            decimal dcSum = 0;
                                            if (i == 0 || (i % 2) == 0)
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                                                    dcProfitRollOver = dcProfitRollOver - dcSum;
                                                }
                                            }
                                            else
                                            {
                                                if (dtbl.Rows.Count > 0)
                                                {
                                                    dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                                                    dcProfitRollOver = dcProfitRollOver + dcSum;
                                                }
                                            }
                                        }

                                        decimal decProfitLedgerOpening = decimal.Parse(DsetBalanceSheet.Tables[3].Compute("Sum(Balance)", string.Empty).ToString());
                                        decimal decTotalProfitAndLoss = decimal.Parse(DsetBalanceSheet.Tables[2].Compute("Sum(Balance)", string.Empty).ToString());
                                        decimal decRetainedEarning = decimal.Parse(DsetBalanceSheetForRollOver.Tables[2].Compute("Sum(Balance)", string.Empty).ToString());

                                        //Old implementation commented by urefe to balance P/L figure with Balance sheet P/L value 20160906
                                        decimal decCurrentProfitLoss = dcProfit + ClosingStock;
                                        decimal decCurrentProfitLossRollover = dcProfitRollOver + ClosingStockForRollOver;
                                        //decimal decCurrentProfitLoss = (dcProfit > 0 ? dcProfit * -1 : dcProfit) - (ClosingStock > 0 ? ClosingStock * -1 : (ClosingStock));
                                        //decimal decCurrentProfitLoss = dcProfit + (ClosingStock > 0 ? ClosingStock * -1 : (ClosingStock));
                                        decimal decOpeningOfProfitAndLoss = decProfitLedgerOpening + dcProfitOpening;
                                        //decTotalProfitAndLoss = (decTotalProfitAndLoss < 0) ? decTotalProfitAndLoss * -1 : decTotalProfitAndLoss;
                                        //decRetainedEarning = (decRetainedEarning < 0) ? decRetainedEarning * -1 : decRetainedEarning;
                                        decOpeningOfProfitAndLoss = (decOpeningOfProfitAndLoss < 0) ? decOpeningOfProfitAndLoss * -1 : decOpeningOfProfitAndLoss;
                                        //decCurrentProfitLoss = (decCurrentProfitLoss < 0) ? decCurrentProfitLoss * -1 : decCurrentProfitLoss;
                                        */
                                    #endregion
                                    decimal profitOrLoss = methodForStockValue.profitOrLoss(Convert.ToDateTime(txtToDate.Text), calculationMethod, false);
                                    if (profitOrLoss >= 0) //(decCurrentProfitLoss >= 0)
                                    {
                                        foreach (DataRow dRow in DsetBalanceSheet.Tables[2].Rows)
                                        {
                                            if (dRow["Name"].ToString() == "Profit And Loss Account")
                                            {
                                                dgvReport.Rows.Add();
                                                dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                                dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Profit/Loss for the Year";
                                                dgvReport.Rows[AssetRow].Cells["Amount2"].Value = Math.Round(profitOrLoss, PublicVariables._inNoOfDecimalPlaces).ToString("N2");
                                                dgvReport.Rows[AssetRow].Cells["GroupId1"].Value = dRow["ID"].ToString();
                                                TotalELSubGroup = TotalELSubGroup + Math.Round(profitOrLoss, PublicVariables._inNoOfDecimalPlaces);
                                                AssetRow++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        profitOrLoss = profitOrLoss * -1;
                                        foreach (DataRow dRow in DsetBalanceSheet.Tables[2].Rows)
                                        {
                                            if (dRow["Name"].ToString() == "Profit And Loss Account")
                                            {
                                                dgvReport.Rows.Add();
                                                dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                                //dgvReport.Rows[AssetRow].Cells["Amount2"].Style.ForeColor = Color.Red;
                                                dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Profit/Loss for the Year";
                                                dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + Math.Round(profitOrLoss, PublicVariables._inNoOfDecimalPlaces).ToString("N2") + ")";
                                                dgvReport.Rows[AssetRow].Cells["GroupId2"].Value = dRow["ID"].ToString();
                                                //TotalELSubGroup = TotalELSubGroup + Math.Round(decTotalProfitAndLoss + decCurrentProfitLoss * -1, PublicVariables._inNoOfDecimalPlaces);
                                                TotalELSubGroup = TotalELSubGroup - Math.Round(profitOrLoss, PublicVariables._inNoOfDecimalPlaces);
                                                AssetRow++;
                                            }
                                        }
                                    }

                                    // ---------------------------- Asset-------------------------------------------- //
                                    dgvReport.Rows.Add();
                                    //dgvReport.Rows[AssetRow].DefaultCellStyle.Font = newFont;
                                    dgvReport.Rows[AssetRow].DefaultCellStyle.Padding = newPadding;
                                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Retained Earnings";


                                    // ------------------------- Retained Earnings ---------------------- //
                                    decimal retainedEarnings = methodForStockValue.retainedEarnings(Convert.ToDateTime(txtToDate.Text)); //decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning;                                                                                              //added by evans:
                                    decimal direct = SpFinance.GetDirectPostingIntoRetainedEanings(PublicVariables._dtFromDate, PublicVariables._dtToDate);
                                    retainedEarnings = retainedEarnings - direct;
                                    if (retainedEarnings >= 0)
                                    {
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = Math.Round(retainedEarnings, PublicVariables._inNoOfDecimalPlaces).ToString("N2");
                                        TotalELSubGroup = TotalELSubGroup + retainedEarnings; // (decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning);
                                    }
                                    else
                                    {
                                        retainedEarnings = retainedEarnings * -1;
                                        //dgvReport.Rows[AssetRow].Cells["Amount2"].Style.ForeColor = Color.Red;
                                        dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "(" + Math.Round(retainedEarnings, PublicVariables._inNoOfDecimalPlaces).ToString("N2") + ")";
                                        TotalELSubGroup = TotalELSubGroup - retainedEarnings; // (decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning);
                                    }
                                    AssetRow++;
                                    ShowProfitAndLoss = false;
                                }
                                #endregion
                                dgvReport.Rows.Add();
                                dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "_____________";
                                AssetRow++;
                            }
                            dgvReport.Rows[AssetRow - 2].Cells["Amount3"].Value = TotalELSubGroup.ToString("N2");
                            dgvReport.Rows[AssetRow - 2].Cells["GroupId2"].Value = rw["ID"].ToString();
                            TotalEquitiesAndLiabilities += TotalELSubGroup;
                            if (TotalELSubGroup < 0)
                            {
                                TotalELSubGroup = TotalELSubGroup * -1;
                                dgvReport.Rows[AssetRow - 2].Cells["Amount3"].Value = "(" + TotalELSubGroup.ToString("N2") + ")";
                                dgvReport.Rows[AssetRow - 2].Cells["GroupId2"].Value = rw["ID"].ToString();
                            }
                        }
                    }

                    dgvReport.Rows.Add();
                    //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = "_____________";
                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "_____________";
                    AssetRow++;

                    // -------------------- Get total equities and liabilities ---------------------------------------------------- //
                    //AssetRow++;

                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["dgvtxtAsset"].Value = "Total Equity and Liabilities".ToUpper();
                    //dgvReport.Rows[AssetRow].Cells["Amount3"].Style = new Font(dgvReport.DefaultCellStyle.Font, FontStyle.Underline);
                    if (TotalEquitiesAndLiabilities > 0)
                    {
                        dgvReport.Rows[AssetRow].Cells["Amount3"].Value = (TotalEquitiesAndLiabilities).ToString("N2");
                    }
                    else
                    {
                        dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "(" + (TotalEquitiesAndLiabilities * -1).ToString("N2") + ")";
                    }

                    decPrintOrNot1 = TotalEquitiesAndLiabilities;

                    AssetRow++;
                    dgvReport.Rows.Add();
                    dgvReport.Rows[AssetRow].Cells["Amount3"].Value = "_____________";
                    AssetRow++;

                    //---------------------Profit And Loss---------------------------------------------------------------------------------------------------------------
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS1:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to create Datatables
        /// </summary>
        /// <returns></returns>

        public DataTable dtblBalanceSheet2()
        {
            DataTable dtLocalC = new DataTable();
            try
            {
                dtLocalC.Columns.Add("Asset");
                dtLocalC.Columns.Add("Amount1");
                dtLocalC.Columns.Add("Amount2");
                dtLocalC.Columns.Add("Amount3");
                dtLocalC.Columns.Add("date");
                DataRow drLocal = null;
                foreach (DataGridViewRow dr in dgvReport.Rows)
                {
                    drLocal = dtLocalC.NewRow();
                    drLocal["Asset"] = dr.Cells["dgvtxtAsset"].Value;
                    drLocal["Amount1"] = dr.Cells["Amount1"].Value;
                    drLocal["Amount2"] = dr.Cells["Amount2"].Value;
                    drLocal["Amount3"] = dr.Cells["Amount3"].Value;
                    drLocal["date"] = txtToDate.Text.ToString();
                    dtLocalC.Rows.Add(drLocal);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS2:" + ex.Message;
            }
            return dtLocalC;
        }
        /// <summary>
        /// Function to get dataset
        /// </summary>
        /// <returns></returns>
        public DataSet getdataset2()
        {
            DataSet dsFundFlow2 = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblFund2 = dtblBalanceSheet2();
                DataTable dtblCompany2 = new DataTable();
                dtblCompany2 = spfinancial.FundFlowReportPrintCompany(PublicVariables._decCurrentCompanyId);
                dsFundFlow2.Tables.Add(dtblFund2);
                dsFundFlow2.Tables.Add(dtblCompany2);
                return dsFundFlow2;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS3:" + ex.Message;
            }
            return dsFundFlow2;
        }
        /// <summary>
        /// Function for Print
        /// </summary>
        /// <param name="toDate"></param>
        public void Print(DateTime toDate)
        {
            try
            {
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataSet destBalance2 = getdataset2();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.BalanceSheetReportPrint2(destBalance2);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS4:" + ex.Message;
            }
        }
        #endregion
        #region EVENTS
        /// <summary>
        /// On 'Search' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnsearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtToDate.Text != string.Empty)
                {
                    FillGrid();
                }
                else if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtCurrentDate.ToString();
                    FillGrid();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS5:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Clear' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnclear_Click(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = PublicVariables._dtCurrentDate.ToString();
                dtpdate.MinDate = PublicVariables._dtFromDate;
                dtpdate.MaxDate = PublicVariables._dtToDate;
                dtpdate.Value = PublicVariables._dtToDate;
                txtToDate.Text = dtpdate.Value.ToString("dd-MMM-yyyy");
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS6:" + ex.Message;
            }
        }
        /// <summary>
        /// Date Validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpdate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtToDate.Text, out dt);
                dtpdate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS7:" + ex.Message;
            }
        }
        /// <summary>
        /// Form Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <summary>
        /// Shows details of corresponding Ledgers on Cell double click in Datagirdview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvReport_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvReport.CurrentRow.Index == e.RowIndex)
                {
                    if (dgvReport.CurrentCell != null)
                    {
                        if (dgvReport.CurrentCell.Value != null && dgvReport.CurrentCell.Value.ToString().Trim() != string.Empty)
                        {
                            int inRowIndex = dgvReport.CurrentCell.RowIndex;
                            int inColIndex = dgvReport.CurrentCell.ColumnIndex;
                            string strGroupId = string.Empty;
                            string strLedgerId = string.Empty;
                            if (dgvReport.Columns[inColIndex].Name == "dgvtxtAsset" || dgvReport.Columns[inColIndex].Name == "Amount1")
                            {
                                try
                                {
                                    // Urefe comment the belo line since negative values that may arise sometimes with brackets around it cannot be converted to decimal
                                    //if (Convert.ToDecimal(dgvReport.Rows[e.RowIndex].Cells["Amount1"].Value.ToString()) != 0)
                                    if (dgvReport.Rows[inRowIndex].Cells["ID1"].Value != null && dgvReport.Rows[inRowIndex].Cells["ID1"].Value.ToString() != string.Empty)
                                    {
                                        strLedgerId = dgvReport.Rows[inRowIndex].Cells["ID1"].Value.ToString();
                                        strGroupId = string.Empty;
                                    }
                                    else if (dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value != null && dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString();
                                        strLedgerId = string.Empty;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strGroupId = string.Empty;
                                    strLedgerId = string.Empty;
                                }
                            }
                            if (dgvReport.Columns[inColIndex].Name == "dgvtxtAsset" || dgvReport.Columns[inColIndex].Name == "Amount3")
                            {
                                try
                                {
                                    // Urefe comment the belo line since negative values that may arise sometimes with brackets around it cannot be converted to decimal
                                    //if (Convert.ToDecimal(dgvReport.Rows[e.RowIndex].Cells["Amount3"].Value.ToString()) != 0)
                                    if (dgvReport.Rows[inRowIndex].Cells["ID1"].Value != null && dgvReport.Rows[inRowIndex].Cells["ID1"].Value.ToString() != string.Empty)
                                    {
                                        strLedgerId = dgvReport.Rows[inRowIndex].Cells["ID1"].Value.ToString();
                                        strGroupId = string.Empty;
                                    }
                                    else if (dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value != null && dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString();
                                        strLedgerId = string.Empty;
                                    }
                                    else if (dgvReport.Rows[inRowIndex].Cells["GroupId2"].Value != null && dgvReport.Rows[inRowIndex].Cells["GroupId2"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvReport.Rows[inRowIndex].Cells["GroupId2"].Value.ToString();
                                        strLedgerId = string.Empty;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strGroupId = string.Empty;
                                    strLedgerId = string.Empty;
                                }
                            }
                            else if (dgvReport.Columns[inColIndex].Name == "dgvtxtAsset" || dgvReport.Columns[inColIndex].Name == "Amount2")
                            {
                                try
                                {
                                    // Urefe comment the belo line since negative values that may arise sometimes with brackets around it cannot be converted to decimal
                                    //if (Convert.ToDecimal(dgvReport.Rows[e.RowIndex].Cells["Amount2"].Value.ToString()) != 0)
                                    if (dgvReport.Rows[inRowIndex].Cells["ID2"].Value != null && dgvReport.Rows[inRowIndex].Cells["ID2"].Value.ToString() != string.Empty)
                                    {
                                        strLedgerId = dgvReport.Rows[inRowIndex].Cells["ID2"].Value.ToString();
                                        strGroupId = string.Empty;
                                    }
                                    else if (dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value != null && dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString() != string.Empty)
                                    {
                                        strGroupId = dgvReport.Rows[inRowIndex].Cells["GroupId1"].Value.ToString();
                                        strLedgerId = string.Empty;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strGroupId = string.Empty;
                                    strLedgerId = string.Empty;
                                }
                            }
                            if (strLedgerId != string.Empty || strGroupId != string.Empty)
                            {
                                inCurrenRowIndex = inRowIndex;
                                frmAccountGroupwiseReport objForm1 = new frmAccountGroupwiseReport();
                                objForm1.WindowState = FormWindowState.Normal;
                                objForm1.MdiParent = formMDI.MDIObj;
                                objForm1.CallFromBalancesheet2(dtfromdate.ToString(), txtToDate.Text, strGroupId, this);
                                this.Enabled = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Enabled = true;

                formMDI.infoError.ErrorString = "BS9:" + ex.Message;
            }
        }
        /// <summary>
        /// Checks on Form Closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBalanceSheet_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                isFormLoad = true;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS10:" + ex.Message;
            }
        }
        /// <summary>
        /// On 'Print' button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (decPrintOrNot == 0 && decPrintOrNot1 == 0)
                {
                    MessageBox.Show("No Row To Print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    Print(PublicVariables._dtToDate);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS11:" + ex.Message;
            }
        }
        /// <summary>
        /// Date validation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                }
                else
                {
                    DateValidation objValidation = new DateValidation();
                    objValidation.DateValidationFunction(txtToDate);
                    if (txtToDate.Text == string.Empty)
                        txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    DateTime dt;
                    DateTime.TryParse(txtToDate.Text, out dt);
                    dtpdate.Value = dt;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS12:" + ex.Message;
            }
        }
        /// <summary>
        /// Fills txtToDate textbox on dtpdate datetimepicker ValueChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpdate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpdate.Value;
                this.txtToDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS13:" + ex.Message;
            }
        }
        #endregion
        #region NAVIGATION
        /// <summary>
        /// Escape key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmBalanceSheet_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (PublicVariables.isMessageClose)
                    {
                        Messages.CloseMessage(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS14:" + ex.Message;
            }
        }
        /// <summary>
        /// Back space navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_KeyDown(object sender, KeyEventArgs e)
        {

        }
        /// <summary>
        /// Enter key navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_KeyDown(object sender, KeyEventArgs e)
        {

        }
        /// <summary>
        ///  Enter key and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnsearch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnsearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    txtToDate.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS17:" + ex.Message;
            }
        }
        /// <summary>
        /// Enter key and backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnclear_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    //btnPrint.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    btnsearch.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS18:" + ex.Message;
            }
        }
        /// <summary>
        /// Backspace navigation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvReport_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Back)
                {
                    btnclear.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS19:" + ex.Message;
            }
        }
        #endregion

        private void btnSearchProjectAndCategory_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void btnClearProjectAndCategory_Click(object sender, EventArgs e)
        {
            cmbProject.SelectedIndex = 0;
            cmbCategory.SelectedIndex = 0;
            FillGrid();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvReport, "BALANCE SHEET AS AT:", 0, 0, "Excel", null, Convert.ToDateTime(txtToDate.Text), "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }
    }
}
