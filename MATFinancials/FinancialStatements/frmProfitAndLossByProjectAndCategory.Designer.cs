﻿namespace MATFinancials.FinancialStatements
{
    partial class frmProfitAndLossByProjectAndCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfitAndLossByProjectAndCategory));
            this.btnClearProjectAndCategory = new System.Windows.Forms.Button();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbProject = new System.Windows.Forms.ComboBox();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvtxtGroupId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGroupId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearchProjectAndCategory = new System.Windows.Forms.Button();
            this.dgvtxtAmount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.dtpTodate = new System.Windows.Forms.DateTimePicker();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpContraVoucherDate = new System.Windows.Forms.DateTimePicker();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvtxtIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvProfitAndLoss = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProfitAndLoss)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClearProjectAndCategory
            // 
            this.btnClearProjectAndCategory.BackColor = System.Drawing.Color.DimGray;
            this.btnClearProjectAndCategory.FlatAppearance.BorderSize = 0;
            this.btnClearProjectAndCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearProjectAndCategory.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearProjectAndCategory.ForeColor = System.Drawing.Color.Maroon;
            this.btnClearProjectAndCategory.Location = new System.Drawing.Point(719, 53);
            this.btnClearProjectAndCategory.Name = "btnClearProjectAndCategory";
            this.btnClearProjectAndCategory.Size = new System.Drawing.Size(85, 21);
            this.btnClearProjectAndCategory.TabIndex = 1208;
            this.btnClearProjectAndCategory.Text = "Clear";
            this.btnClearProjectAndCategory.UseVisualStyleBackColor = false;
            this.btnClearProjectAndCategory.Click += new System.EventHandler(this.btnClearProjectAndCategory_Click);
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(393, 54);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(206, 21);
            this.cmbCategory.TabIndex = 1206;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(339, 58);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(49, 13);
            this.lblCategory.TabIndex = 1205;
            this.lblCategory.Text = "Category";
            // 
            // cmbProject
            // 
            this.cmbProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProject.FormattingEnabled = true;
            this.cmbProject.Location = new System.Drawing.Point(111, 54);
            this.cmbProject.Name = "cmbProject";
            this.cmbProject.Size = new System.Drawing.Size(206, 21);
            this.cmbProject.TabIndex = 1204;
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(45, 58);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(40, 13);
            this.lblProject.TabIndex = 1203;
            this.lblProject.Text = "Project";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.BackColor = System.Drawing.Color.DimGray;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblHeading.Location = new System.Drawing.Point(3, 4);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(199, 20);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Statement of Profit or Loss";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.lblHeading);
            this.panel1.Location = new System.Drawing.Point(47, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(437, 28);
            this.panel1.TabIndex = 1202;
            // 
            // dgvtxtGroupId2
            // 
            this.dgvtxtGroupId2.HeaderText = "GroupId2";
            this.dgvtxtGroupId2.Name = "dgvtxtGroupId2";
            this.dgvtxtGroupId2.ReadOnly = true;
            this.dgvtxtGroupId2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId2.Visible = false;
            // 
            // dgvtxtGroupId1
            // 
            this.dgvtxtGroupId1.HeaderText = "GroupId1";
            this.dgvtxtGroupId1.Name = "dgvtxtGroupId1";
            this.dgvtxtGroupId1.ReadOnly = true;
            this.dgvtxtGroupId1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId1.Visible = false;
            // 
            // dgvtxtAmount2
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount2.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvtxtAmount2.HeaderText = "Amount2";
            this.dgvtxtAmount2.Name = "dgvtxtAmount2";
            this.dgvtxtAmount2.ReadOnly = true;
            this.dgvtxtAmount2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnSearchProjectAndCategory
            // 
            this.btnSearchProjectAndCategory.BackColor = System.Drawing.Color.DimGray;
            this.btnSearchProjectAndCategory.FlatAppearance.BorderSize = 0;
            this.btnSearchProjectAndCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchProjectAndCategory.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchProjectAndCategory.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearchProjectAndCategory.Location = new System.Drawing.Point(621, 54);
            this.btnSearchProjectAndCategory.Name = "btnSearchProjectAndCategory";
            this.btnSearchProjectAndCategory.Size = new System.Drawing.Size(85, 21);
            this.btnSearchProjectAndCategory.TabIndex = 1207;
            this.btnSearchProjectAndCategory.Text = "Search";
            this.btnSearchProjectAndCategory.UseVisualStyleBackColor = false;
            this.btnSearchProjectAndCategory.Click += new System.EventHandler(this.btnSearchProjectAndCategory_Click);
            // 
            // dgvtxtAmount1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtAmount1.HeaderText = "Amount";
            this.dgvtxtAmount1.Name = "dgvtxtAmount1";
            this.dgvtxtAmount1.ReadOnly = true;
            this.dgvtxtAmount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(393, 20);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.Size = new System.Drawing.Size(188, 20);
            this.txtToDate.TabIndex = 1193;
            // 
            // dtpTodate
            // 
            this.dtpTodate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTodate.Location = new System.Drawing.Point(577, 20);
            this.dtpTodate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpTodate.Name = "dtpTodate";
            this.dtpTodate.Size = new System.Drawing.Size(22, 20);
            this.dtpTodate.TabIndex = 1200;
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(111, 20);
            this.txtFromDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Size = new System.Drawing.Size(188, 20);
            this.txtFromDate.TabIndex = 1192;
            // 
            // dtpContraVoucherDate
            // 
            this.dtpContraVoucherDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpContraVoucherDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpContraVoucherDate.Location = new System.Drawing.Point(285, 20);
            this.dtpContraVoucherDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpContraVoucherDate.Name = "dtpContraVoucherDate";
            this.dtpContraVoucherDate.Size = new System.Drawing.Size(32, 20);
            this.dtpContraVoucherDate.TabIndex = 1199;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.DimGray;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Maroon;
            this.btnClear.Location = new System.Drawing.Point(719, 19);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 21);
            this.btnClear.TabIndex = 1195;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(621, 19);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 21);
            this.btnSearch.TabIndex = 1194;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(337, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 1198;
            this.label3.Text = "To date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(46, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1197;
            this.label2.Text = "From date";
            // 
            // dgvtxtIncome
            // 
            this.dgvtxtIncome.HeaderText = "Income";
            this.dgvtxtIncome.Name = "dgvtxtIncome";
            this.dgvtxtIncome.ReadOnly = true;
            this.dgvtxtIncome.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvProfitAndLoss
            // 
            this.dgvProfitAndLoss.AllowUserToAddRows = false;
            this.dgvProfitAndLoss.AllowUserToDeleteRows = false;
            this.dgvProfitAndLoss.AllowUserToResizeColumns = false;
            this.dgvProfitAndLoss.AllowUserToResizeRows = false;
            this.dgvProfitAndLoss.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProfitAndLoss.BackgroundColor = System.Drawing.Color.White;
            this.dgvProfitAndLoss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProfitAndLoss.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProfitAndLoss.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProfitAndLoss.ColumnHeadersHeight = 35;
            this.dgvProfitAndLoss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProfitAndLoss.ColumnHeadersVisible = false;
            this.dgvProfitAndLoss.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtIncome,
            this.dgvtxtAmount1,
            this.dgvtxtAmount2,
            this.dgvtxtGroupId1,
            this.dgvtxtGroupId2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProfitAndLoss.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProfitAndLoss.EnableHeadersVisualStyles = false;
            this.dgvProfitAndLoss.GridColor = System.Drawing.Color.White;
            this.dgvProfitAndLoss.Location = new System.Drawing.Point(48, 121);
            this.dgvProfitAndLoss.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.dgvProfitAndLoss.MultiSelect = false;
            this.dgvProfitAndLoss.Name = "dgvProfitAndLoss";
            this.dgvProfitAndLoss.ReadOnly = true;
            this.dgvProfitAndLoss.RowHeadersVisible = false;
            this.dgvProfitAndLoss.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProfitAndLoss.Size = new System.Drawing.Size(986, 358);
            this.dgvProfitAndLoss.TabIndex = 1201;
            // 
            // frmProfitAndLossByProjectAndCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.btnClearProjectAndCategory);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.cmbProject);
            this.Controls.Add(this.lblProject);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSearchProjectAndCategory);
            this.Controls.Add(this.txtToDate);
            this.Controls.Add(this.dtpTodate);
            this.Controls.Add(this.txtFromDate);
            this.Controls.Add(this.dtpContraVoucherDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvProfitAndLoss);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmProfitAndLossByProjectAndCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmProfitAndLossByProjectAndCategory";
            this.Load += new System.EventHandler(this.frmProfitAndLossByProjectAndCategory_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProfitAndLoss)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClearProjectAndCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbProject;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount2;
        private System.Windows.Forms.Button btnSearchProjectAndCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount1;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.DateTimePicker dtpTodate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpContraVoucherDate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtIncome;
        private System.Windows.Forms.DataGridView dgvProfitAndLoss;
    }
}