﻿namespace MATFinancials
{
    partial class frmProfitAndLoss2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProfitAndLoss2));
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.dtpTodate = new System.Windows.Forms.DateTimePicker();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpContraVoucherDate = new System.Windows.Forms.DateTimePicker();
            this.dgvProfitAndLoss = new System.Windows.Forms.DataGridView();
            this.dgvtxtIncome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGroupId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGroupId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblHeading = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProfitAndLoss)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.DimGray;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Maroon;
            this.btnClear.Location = new System.Drawing.Point(723, 13);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 21);
            this.btnClear.TabIndex = 1177;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(625, 13);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 21);
            this.btnSearch.TabIndex = 1176;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(341, 18);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 1181;
            this.label3.Text = "To date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(50, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1180;
            this.label2.Text = "From date";
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(953, 473);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(85, 27);
            this.btnPrint.TabIndex = 1179;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(394, 14);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.Size = new System.Drawing.Size(188, 20);
            this.txtToDate.TabIndex = 1175;
            this.txtToDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtToDate_KeyDown);
            this.txtToDate.Leave += new System.EventHandler(this.txtToDate_Leave);
            // 
            // dtpTodate
            // 
            this.dtpTodate.CustomFormat = "dd-MMM-yyyy";
            this.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTodate.Location = new System.Drawing.Point(581, 14);
            this.dtpTodate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpTodate.Name = "dtpTodate";
            this.dtpTodate.Size = new System.Drawing.Size(18, 20);
            this.dtpTodate.TabIndex = 1182;
            this.dtpTodate.ValueChanged += new System.EventHandler(this.dtpTodate_ValueChanged);
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(115, 14);
            this.txtFromDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Size = new System.Drawing.Size(188, 20);
            this.txtFromDate.TabIndex = 1174;
            this.txtFromDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFromDate_KeyDown);
            this.txtFromDate.Leave += new System.EventHandler(this.txtFromDate_Leave);
            // 
            // dtpContraVoucherDate
            // 
            this.dtpContraVoucherDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpContraVoucherDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpContraVoucherDate.Location = new System.Drawing.Point(301, 14);
            this.dtpContraVoucherDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpContraVoucherDate.Name = "dtpContraVoucherDate";
            this.dtpContraVoucherDate.Size = new System.Drawing.Size(18, 20);
            this.dtpContraVoucherDate.TabIndex = 1182;
            this.dtpContraVoucherDate.ValueChanged += new System.EventHandler(this.dtpContraVoucherDate_ValueChanged);
            // 
            // dgvProfitAndLoss
            // 
            this.dgvProfitAndLoss.AllowUserToAddRows = false;
            this.dgvProfitAndLoss.AllowUserToDeleteRows = false;
            this.dgvProfitAndLoss.AllowUserToResizeColumns = false;
            this.dgvProfitAndLoss.AllowUserToResizeRows = false;
            this.dgvProfitAndLoss.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProfitAndLoss.BackgroundColor = System.Drawing.Color.White;
            this.dgvProfitAndLoss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProfitAndLoss.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProfitAndLoss.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProfitAndLoss.ColumnHeadersHeight = 35;
            this.dgvProfitAndLoss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProfitAndLoss.ColumnHeadersVisible = false;
            this.dgvProfitAndLoss.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtIncome,
            this.dgvtxtAmount1,
            this.dgvtxtAmount2,
            this.dgvtxtGroupId1,
            this.dgvtxtGroupId2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProfitAndLoss.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProfitAndLoss.EnableHeadersVisualStyles = false;
            this.dgvProfitAndLoss.GridColor = System.Drawing.Color.White;
            this.dgvProfitAndLoss.Location = new System.Drawing.Point(52, 81);
            this.dgvProfitAndLoss.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.dgvProfitAndLoss.MultiSelect = false;
            this.dgvProfitAndLoss.Name = "dgvProfitAndLoss";
            this.dgvProfitAndLoss.ReadOnly = true;
            this.dgvProfitAndLoss.RowHeadersVisible = false;
            this.dgvProfitAndLoss.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProfitAndLoss.Size = new System.Drawing.Size(986, 384);
            this.dgvProfitAndLoss.TabIndex = 1184;
            this.dgvProfitAndLoss.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProfitAndLoss_CellDoubleClick);
            // 
            // dgvtxtIncome
            // 
            this.dgvtxtIncome.HeaderText = "Income";
            this.dgvtxtIncome.Name = "dgvtxtIncome";
            this.dgvtxtIncome.ReadOnly = true;
            this.dgvtxtIncome.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtAmount1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtAmount1.HeaderText = "Amount";
            this.dgvtxtAmount1.Name = "dgvtxtAmount1";
            this.dgvtxtAmount1.ReadOnly = true;
            this.dgvtxtAmount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtAmount2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtAmount2.HeaderText = "Amount2";
            this.dgvtxtAmount2.Name = "dgvtxtAmount2";
            this.dgvtxtAmount2.ReadOnly = true;
            this.dgvtxtAmount2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtGroupId1
            // 
            this.dgvtxtGroupId1.HeaderText = "GroupId1";
            this.dgvtxtGroupId1.Name = "dgvtxtGroupId1";
            this.dgvtxtGroupId1.ReadOnly = true;
            this.dgvtxtGroupId1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId1.Visible = false;
            // 
            // dgvtxtGroupId2
            // 
            this.dgvtxtGroupId2.HeaderText = "GroupId2";
            this.dgvtxtGroupId2.Name = "dgvtxtGroupId2";
            this.dgvtxtGroupId2.ReadOnly = true;
            this.dgvtxtGroupId2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId2.Visible = false;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.BackColor = System.Drawing.Color.DimGray;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblHeading.Location = new System.Drawing.Point(3, 4);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(199, 20);
            this.lblHeading.TabIndex = 0;
            this.lblHeading.Text = "Statement of Profit or Loss";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.lblHeading);
            this.panel1.Location = new System.Drawing.Point(53, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(437, 28);
            this.panel1.TabIndex = 1185;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(807, 473);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(130, 27);
            this.btnExport.TabIndex = 1300;
            this.btnExport.Text = "Export To Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // frmProfitAndLoss2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvProfitAndLoss);
            this.Controls.Add(this.txtToDate);
            this.Controls.Add(this.dtpTodate);
            this.Controls.Add(this.txtFromDate);
            this.Controls.Add(this.dtpContraVoucherDate);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPrint);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmProfitAndLoss2";
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Profit And Loss (Conventional)";
            this.Load += new System.EventHandler(this.frmProfitAndLoss2_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProfitAndLoss2_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProfitAndLoss)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.DateTimePicker dtpTodate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpContraVoucherDate;
        private System.Windows.Forms.DataGridView dgvProfitAndLoss;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtIncome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId2;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExport;
    }
}