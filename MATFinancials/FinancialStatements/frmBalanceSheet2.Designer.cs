﻿namespace MATFinancials.FinancialStatements
{
    partial class frmBalanceSheet2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBalanceSheet2));
            this.lblasset = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnsearch = new System.Windows.Forms.Button();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.dgvtxtAsset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupId3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtLiability = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtThird = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClearProjectAndCategory = new System.Windows.Forms.Button();
            this.btnSearchProjectAndCategory = new System.Windows.Forms.Button();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbProject = new System.Windows.Forms.ComboBox();
            this.lblProject = new System.Windows.Forms.Label();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.dtpdate = new System.Windows.Forms.DateTimePicker();
            this.lbltodate = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // lblasset
            // 
            this.lblasset.AutoSize = true;
            this.lblasset.BackColor = System.Drawing.Color.Transparent;
            this.lblasset.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblasset.ForeColor = System.Drawing.Color.White;
            this.lblasset.Location = new System.Drawing.Point(4, 5);
            this.lblasset.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblasset.Name = "lblasset";
            this.lblasset.Size = new System.Drawing.Size(52, 19);
            this.lblasset.TabIndex = 1184;
            this.lblasset.Text = "Asset";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Controls.Add(this.lblasset);
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(91, 52);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 32);
            this.panel2.TabIndex = 1197;
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.DimGray;
            this.btnclear.FlatAppearance.BorderSize = 0;
            this.btnclear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.ForeColor = System.Drawing.Color.Maroon;
            this.btnclear.Location = new System.Drawing.Point(881, 27);
            this.btnclear.Margin = new System.Windows.Forms.Padding(4);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(113, 26);
            this.btnclear.TabIndex = 1191;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnsearch
            // 
            this.btnsearch.BackColor = System.Drawing.Color.DimGray;
            this.btnsearch.FlatAppearance.BorderSize = 0;
            this.btnsearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnsearch.Location = new System.Drawing.Point(751, 27);
            this.btnsearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnsearch.Name = "btnsearch";
            this.btnsearch.Size = new System.Drawing.Size(113, 26);
            this.btnsearch.TabIndex = 1190;
            this.btnsearch.Text = "Search";
            this.btnsearch.UseVisualStyleBackColor = false;
            this.btnsearch.Click += new System.EventHandler(this.btnsearch_Click);
            // 
            // dgvReport
            // 
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AllowUserToResizeColumns = false;
            this.dgvReport.AllowUserToResizeRows = false;
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvReport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvReport.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.ColumnHeadersVisible = false;
            this.dgvReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtAsset,
            this.Amount1,
            this.ID1,
            this.GroupId1,
            this.Amount2,
            this.ID2,
            this.GroupId2,
            this.Amount3,
            this.ID3,
            this.GroupId3,
            this.dgvtxtLiability,
            this.dgvtxtThird});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReport.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvReport.EnableHeadersVisualStyles = false;
            this.dgvReport.GridColor = System.Drawing.Color.LightGray;
            this.dgvReport.Location = new System.Drawing.Point(91, 84);
            this.dgvReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 6);
            this.dgvReport.MultiSelect = false;
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvReport.RowHeadersVisible = false;
            this.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReport.Size = new System.Drawing.Size(903, 371);
            this.dgvReport.TabIndex = 1194;
            this.dgvReport.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport_CellDoubleClick);
            // 
            // dgvtxtAsset
            // 
            this.dgvtxtAsset.HeaderText = "Asset";
            this.dgvtxtAsset.Name = "dgvtxtAsset";
            this.dgvtxtAsset.ReadOnly = true;
            // 
            // Amount1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Amount1.HeaderText = "Amount1";
            this.Amount1.Name = "Amount1";
            this.Amount1.ReadOnly = true;
            this.Amount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ID1
            // 
            this.ID1.HeaderText = "ID1";
            this.ID1.Name = "ID1";
            this.ID1.ReadOnly = true;
            this.ID1.Visible = false;
            // 
            // GroupId1
            // 
            this.GroupId1.HeaderText = "GroupId1";
            this.GroupId1.Name = "GroupId1";
            this.GroupId1.ReadOnly = true;
            this.GroupId1.Visible = false;
            // 
            // Amount2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Amount2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Amount2.HeaderText = "Amount2";
            this.Amount2.Name = "Amount2";
            this.Amount2.ReadOnly = true;
            this.Amount2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ID2
            // 
            this.ID2.HeaderText = "ID2";
            this.ID2.Name = "ID2";
            this.ID2.ReadOnly = true;
            this.ID2.Visible = false;
            // 
            // GroupId2
            // 
            this.GroupId2.HeaderText = "GroupId2";
            this.GroupId2.Name = "GroupId2";
            this.GroupId2.ReadOnly = true;
            this.GroupId2.Visible = false;
            // 
            // Amount3
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight;
            this.Amount3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Amount3.HeaderText = "Amount3";
            this.Amount3.Name = "Amount3";
            this.Amount3.ReadOnly = true;
            // 
            // ID3
            // 
            this.ID3.HeaderText = "ID3";
            this.ID3.Name = "ID3";
            this.ID3.ReadOnly = true;
            this.ID3.Visible = false;
            // 
            // GroupId3
            // 
            this.GroupId3.HeaderText = "GroupId3";
            this.GroupId3.Name = "GroupId3";
            this.GroupId3.ReadOnly = true;
            this.GroupId3.Visible = false;
            // 
            // dgvtxtLiability
            // 
            this.dgvtxtLiability.HeaderText = "Liability";
            this.dgvtxtLiability.Name = "dgvtxtLiability";
            this.dgvtxtLiability.ReadOnly = true;
            this.dgvtxtLiability.Visible = false;
            // 
            // dgvtxtThird
            // 
            this.dgvtxtThird.HeaderText = "Third";
            this.dgvtxtThird.Name = "dgvtxtThird";
            this.dgvtxtThird.ReadOnly = true;
            this.dgvtxtThird.Visible = false;
            // 
            // btnClearProjectAndCategory
            // 
            this.btnClearProjectAndCategory.BackColor = System.Drawing.Color.DimGray;
            this.btnClearProjectAndCategory.FlatAppearance.BorderSize = 0;
            this.btnClearProjectAndCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearProjectAndCategory.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearProjectAndCategory.ForeColor = System.Drawing.Color.Maroon;
            this.btnClearProjectAndCategory.Location = new System.Drawing.Point(595, 40);
            this.btnClearProjectAndCategory.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearProjectAndCategory.Name = "btnClearProjectAndCategory";
            this.btnClearProjectAndCategory.Size = new System.Drawing.Size(113, 26);
            this.btnClearProjectAndCategory.TabIndex = 1203;
            this.btnClearProjectAndCategory.Text = "Clear";
            this.btnClearProjectAndCategory.UseVisualStyleBackColor = false;
            this.btnClearProjectAndCategory.Visible = false;
            this.btnClearProjectAndCategory.Click += new System.EventHandler(this.btnClearProjectAndCategory_Click);
            // 
            // btnSearchProjectAndCategory
            // 
            this.btnSearchProjectAndCategory.BackColor = System.Drawing.Color.DimGray;
            this.btnSearchProjectAndCategory.FlatAppearance.BorderSize = 0;
            this.btnSearchProjectAndCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchProjectAndCategory.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchProjectAndCategory.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearchProjectAndCategory.Location = new System.Drawing.Point(595, 9);
            this.btnSearchProjectAndCategory.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearchProjectAndCategory.Name = "btnSearchProjectAndCategory";
            this.btnSearchProjectAndCategory.Size = new System.Drawing.Size(113, 26);
            this.btnSearchProjectAndCategory.TabIndex = 1202;
            this.btnSearchProjectAndCategory.Text = "Search";
            this.btnSearchProjectAndCategory.UseVisualStyleBackColor = false;
            this.btnSearchProjectAndCategory.Visible = false;
            this.btnSearchProjectAndCategory.Click += new System.EventHandler(this.btnSearchProjectAndCategory_Click);
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(522, 36);
            this.cmbCategory.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(65, 24);
            this.cmbCategory.TabIndex = 1201;
            this.cmbCategory.Visible = false;
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(430, 40);
            this.lblCategory.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(63, 16);
            this.lblCategory.TabIndex = 1200;
            this.lblCategory.Text = "Category";
            this.lblCategory.Visible = false;
            // 
            // cmbProject
            // 
            this.cmbProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProject.FormattingEnabled = true;
            this.cmbProject.Location = new System.Drawing.Point(527, 4);
            this.cmbProject.Margin = new System.Windows.Forms.Padding(4);
            this.cmbProject.Name = "cmbProject";
            this.cmbProject.Size = new System.Drawing.Size(60, 24);
            this.cmbProject.TabIndex = 1199;
            this.cmbProject.Visible = false;
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(446, 9);
            this.lblProject.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(50, 16);
            this.lblProject.TabIndex = 1198;
            this.lblProject.Text = "Project";
            this.lblProject.Visible = false;
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(166, 22);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.Size = new System.Drawing.Size(136, 22);
            this.txtToDate.TabIndex = 1204;
            // 
            // dtpdate
            // 
            this.dtpdate.CustomFormat = "dd-MMM-yyyy";
            this.dtpdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpdate.Location = new System.Drawing.Point(306, 22);
            this.dtpdate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpdate.Name = "dtpdate";
            this.dtpdate.Size = new System.Drawing.Size(21, 22);
            this.dtpdate.TabIndex = 1205;
            this.dtpdate.ValueChanged += new System.EventHandler(this.dtpdate_ValueChanged);
            this.dtpdate.Leave += new System.EventHandler(this.dtpdate_Leave);
            // 
            // lbltodate
            // 
            this.lbltodate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltodate.ForeColor = System.Drawing.Color.Black;
            this.lbltodate.Location = new System.Drawing.Point(97, 24);
            this.lbltodate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbltodate.Name = "lbltodate";
            this.lbltodate.Size = new System.Drawing.Size(64, 21);
            this.lbltodate.TabIndex = 1206;
            this.lbltodate.Text = "To Date";
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(876, 465);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(115, 33);
            this.btnPrint.TabIndex = 1207;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(691, 465);
            this.btnExport.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(173, 33);
            this.btnExport.TabIndex = 1300;
            this.btnExport.Text = "Export To Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // frmBalanceSheet2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1080, 511);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.txtToDate);
            this.Controls.Add(this.dtpdate);
            this.Controls.Add(this.lbltodate);
            this.Controls.Add(this.btnClearProjectAndCategory);
            this.Controls.Add(this.btnSearchProjectAndCategory);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.cmbProject);
            this.Controls.Add(this.lblProject);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnsearch);
            this.Controls.Add(this.dgvReport);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmBalanceSheet2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Balance Sheet";
            this.Load += new System.EventHandler(this.frmBalanceSheet2_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblasset;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnsearch;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAsset;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID1;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupId2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID3;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupId3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtLiability;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtThird;
        private System.Windows.Forms.Button btnClearProjectAndCategory;
        private System.Windows.Forms.Button btnSearchProjectAndCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbProject;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.DateTimePicker dtpdate;
        private System.Windows.Forms.Label lbltodate;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnExport;
    }
}