﻿namespace MATFinancials.Reports
{
    partial class StockDetailsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockDetailsReport));
            this.lblBatch = new System.Windows.Forms.Label();
            this.cmbBatch = new System.Windows.Forms.ComboBox();
            this.cmbProduct = new System.Windows.Forms.ComboBox();
            this.lblProduct = new System.Windows.Forms.Label();
            this.txtProductCode = new System.Windows.Forms.TextBox();
            this.lblProductCode = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.cmbGodown = new System.Windows.Forms.ComboBox();
            this.txtTodate = new System.Windows.Forms.TextBox();
            this.ddtpStockToDate = new System.Windows.Forms.DateTimePicker();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.dtpStockFromDate = new System.Windows.Forms.DateTimePicker();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblToDate = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.dgvStockDetailsReport = new System.Windows.Forms.DataGridView();
            this.productId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.godownName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.voucherTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Batch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inwardQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.outwardQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyBalalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAverageCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockDetailsReport)).BeginInit();
            this.SuspendLayout();
            // 
            // lblBatch
            // 
            this.lblBatch.AutoSize = true;
            this.lblBatch.ForeColor = System.Drawing.Color.Black;
            this.lblBatch.Location = new System.Drawing.Point(290, 37);
            this.lblBatch.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBatch.Name = "lblBatch";
            this.lblBatch.Size = new System.Drawing.Size(35, 13);
            this.lblBatch.TabIndex = 1338;
            this.lblBatch.Text = "Batch";
            // 
            // cmbBatch
            // 
            this.cmbBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBatch.FormattingEnabled = true;
            this.cmbBatch.Location = new System.Drawing.Point(333, 33);
            this.cmbBatch.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbBatch.Name = "cmbBatch";
            this.cmbBatch.Size = new System.Drawing.Size(151, 21);
            this.cmbBatch.TabIndex = 1337;
            // 
            // cmbProduct
            // 
            this.cmbProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.Location = new System.Drawing.Point(576, 34);
            this.cmbProduct.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Size = new System.Drawing.Size(202, 21);
            this.cmbProduct.TabIndex = 1335;
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.BackColor = System.Drawing.Color.Transparent;
            this.lblProduct.ForeColor = System.Drawing.Color.Black;
            this.lblProduct.Location = new System.Drawing.Point(495, 38);
            this.lblProduct.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(77, 13);
            this.lblProduct.TabIndex = 1336;
            this.lblProduct.Text = "Select Product";
            // 
            // txtProductCode
            // 
            this.txtProductCode.Location = new System.Drawing.Point(576, 8);
            this.txtProductCode.Name = "txtProductCode";
            this.txtProductCode.Size = new System.Drawing.Size(202, 20);
            this.txtProductCode.TabIndex = 1332;
            // 
            // lblProductCode
            // 
            this.lblProductCode.AutoSize = true;
            this.lblProductCode.ForeColor = System.Drawing.Color.Black;
            this.lblProductCode.Location = new System.Drawing.Point(492, 10);
            this.lblProductCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblProductCode.Name = "lblProductCode";
            this.lblProductCode.Size = new System.Drawing.Size(72, 13);
            this.lblProductCode.TabIndex = 1334;
            this.lblProductCode.Text = "Product Code";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.ForeColor = System.Drawing.Color.Black;
            this.lblStore.Location = new System.Drawing.Point(63, 38);
            this.lblStore.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(32, 13);
            this.lblStore.TabIndex = 1333;
            this.lblStore.Text = "Store";
            // 
            // cmbGodown
            // 
            this.cmbGodown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGodown.FormattingEnabled = true;
            this.cmbGodown.Location = new System.Drawing.Point(100, 34);
            this.cmbGodown.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbGodown.Name = "cmbGodown";
            this.cmbGodown.Size = new System.Drawing.Size(170, 21);
            this.cmbGodown.TabIndex = 1331;
            // 
            // txtTodate
            // 
            this.txtTodate.Location = new System.Drawing.Point(333, 8);
            this.txtTodate.Margin = new System.Windows.Forms.Padding(5);
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Size = new System.Drawing.Size(130, 20);
            this.txtTodate.TabIndex = 1329;
            this.txtTodate.Leave += new System.EventHandler(this.txtTodate_Leave);
            // 
            // ddtpStockToDate
            // 
            this.ddtpStockToDate.CustomFormat = "dd-MMM-yyyy";
            this.ddtpStockToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ddtpStockToDate.Location = new System.Drawing.Point(462, 8);
            this.ddtpStockToDate.Name = "ddtpStockToDate";
            this.ddtpStockToDate.Size = new System.Drawing.Size(22, 20);
            this.ddtpStockToDate.TabIndex = 1330;
            this.ddtpStockToDate.ValueChanged += new System.EventHandler(this.ddtpStockToDate_ValueChanged);
            // 
            // txtFromDate
            // 
            this.txtFromDate.Location = new System.Drawing.Point(100, 7);
            this.txtFromDate.Margin = new System.Windows.Forms.Padding(5);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Size = new System.Drawing.Size(149, 20);
            this.txtFromDate.TabIndex = 1327;
            this.txtFromDate.Leave += new System.EventHandler(this.txtFromDate_Leave);
            // 
            // dtpStockFromDate
            // 
            this.dtpStockFromDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpStockFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStockFromDate.Location = new System.Drawing.Point(248, 7);
            this.dtpStockFromDate.Name = "dtpStockFromDate";
            this.dtpStockFromDate.Size = new System.Drawing.Size(22, 20);
            this.dtpStockFromDate.TabIndex = 1328;
            this.dtpStockFromDate.ValueChanged += new System.EventHandler(this.dtpStockFromDate_ValueChanged);
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.DimGray;
            this.btnclear.FlatAppearance.BorderSize = 0;
            this.btnclear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclear.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.ForeColor = System.Drawing.Color.Maroon;
            this.btnclear.Location = new System.Drawing.Point(877, 36);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(85, 20);
            this.btnclear.TabIndex = 1326;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(786, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 20);
            this.btnSearch.TabIndex = 1325;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.ForeColor = System.Drawing.Color.Black;
            this.lblToDate.Location = new System.Drawing.Point(281, 12);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(44, 13);
            this.lblToDate.TabIndex = 1324;
            this.lblToDate.Text = "To date";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.ForeColor = System.Drawing.Color.Black;
            this.lblFromDate.Location = new System.Drawing.Point(38, 7);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(54, 13);
            this.lblFromDate.TabIndex = 1323;
            this.lblFromDate.Text = "From date";
            // 
            // dgvStockDetailsReport
            // 
            this.dgvStockDetailsReport.AllowUserToAddRows = false;
            this.dgvStockDetailsReport.AllowUserToDeleteRows = false;
            this.dgvStockDetailsReport.AllowUserToOrderColumns = true;
            this.dgvStockDetailsReport.AllowUserToResizeRows = false;
            this.dgvStockDetailsReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvStockDetailsReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStockDetailsReport.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvStockDetailsReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvStockDetailsReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvStockDetailsReport.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvStockDetailsReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockDetailsReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvStockDetailsReport.ColumnHeadersHeight = 31;
            this.dgvStockDetailsReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productId,
            this.godownName,
            this.productName,
            this.productCode,
            this.refNo,
            this.voucherTypeName,
            this.Batch,
            this.date,
            this.rate,
            this.inwardQty,
            this.outwardQty,
            this.qtyBalalance,
            this.dgvtxtAverageCost,
            this.currentValue,
            this.stockValue});
            this.dgvStockDetailsReport.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStockDetailsReport.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgvStockDetailsReport.EnableHeadersVisualStyles = false;
            this.dgvStockDetailsReport.GridColor = System.Drawing.Color.White;
            this.dgvStockDetailsReport.Location = new System.Drawing.Point(3, 62);
            this.dgvStockDetailsReport.MultiSelect = false;
            this.dgvStockDetailsReport.Name = "dgvStockDetailsReport";
            this.dgvStockDetailsReport.ReadOnly = true;
            this.dgvStockDetailsReport.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStockDetailsReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvStockDetailsReport.RowHeadersVisible = false;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvStockDetailsReport.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvStockDetailsReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStockDetailsReport.ShowEditingIcon = false;
            this.dgvStockDetailsReport.Size = new System.Drawing.Size(1229, 465);
            this.dgvStockDetailsReport.TabIndex = 1340;
            // 
            // productId
            // 
            this.productId.DataPropertyName = "productId";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.productId.DefaultCellStyle = dataGridViewCellStyle2;
            this.productId.HeaderText = "ID";
            this.productId.Name = "productId";
            this.productId.ReadOnly = true;
            this.productId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.productId.Visible = false;
            // 
            // godownName
            // 
            this.godownName.DataPropertyName = "godownName";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.godownName.DefaultCellStyle = dataGridViewCellStyle3;
            this.godownName.HeaderText = "Store";
            this.godownName.Name = "godownName";
            this.godownName.ReadOnly = true;
            this.godownName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // productName
            // 
            this.productName.DataPropertyName = "productName";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.productName.DefaultCellStyle = dataGridViewCellStyle4;
            this.productName.HeaderText = "Item";
            this.productName.Name = "productName";
            this.productName.ReadOnly = true;
            this.productName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // productCode
            // 
            this.productCode.DataPropertyName = "productCode";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.productCode.DefaultCellStyle = dataGridViewCellStyle5;
            this.productCode.HeaderText = "Code";
            this.productCode.Name = "productCode";
            this.productCode.ReadOnly = true;
            this.productCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // refNo
            // 
            this.refNo.DataPropertyName = "refNo";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.refNo.DefaultCellStyle = dataGridViewCellStyle6;
            this.refNo.HeaderText = "Ref No";
            this.refNo.Name = "refNo";
            this.refNo.ReadOnly = true;
            this.refNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // voucherTypeName
            // 
            this.voucherTypeName.DataPropertyName = "voucherTypeName";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.voucherTypeName.DefaultCellStyle = dataGridViewCellStyle7;
            this.voucherTypeName.HeaderText = "Transaction";
            this.voucherTypeName.Name = "voucherTypeName";
            this.voucherTypeName.ReadOnly = true;
            this.voucherTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Batch
            // 
            this.Batch.DataPropertyName = "Batch";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Batch.DefaultCellStyle = dataGridViewCellStyle8;
            this.Batch.HeaderText = "Batch";
            this.Batch.Name = "Batch";
            this.Batch.ReadOnly = true;
            this.Batch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // date
            // 
            this.date.DataPropertyName = "date";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.date.DefaultCellStyle = dataGridViewCellStyle9;
            this.date.HeaderText = "Date";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            this.date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rate
            // 
            this.rate.DataPropertyName = "rate";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.rate.DefaultCellStyle = dataGridViewCellStyle10;
            this.rate.HeaderText = "Rate";
            this.rate.Name = "rate";
            this.rate.ReadOnly = true;
            this.rate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // inwardQty
            // 
            this.inwardQty.DataPropertyName = "inwardQty";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.inwardQty.DefaultCellStyle = dataGridViewCellStyle11;
            this.inwardQty.HeaderText = "Qty-In";
            this.inwardQty.Name = "inwardQty";
            this.inwardQty.ReadOnly = true;
            this.inwardQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // outwardQty
            // 
            this.outwardQty.DataPropertyName = "outwardQty";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.outwardQty.DefaultCellStyle = dataGridViewCellStyle12;
            this.outwardQty.HeaderText = "Qty-Out";
            this.outwardQty.Name = "outwardQty";
            this.outwardQty.ReadOnly = true;
            this.outwardQty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // qtyBalalance
            // 
            this.qtyBalalance.DataPropertyName = "qtyBalalance";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.qtyBalalance.DefaultCellStyle = dataGridViewCellStyle13;
            this.qtyBalalance.HeaderText = "Qty Bal";
            this.qtyBalalance.Name = "qtyBalalance";
            this.qtyBalalance.ReadOnly = true;
            this.qtyBalalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvtxtAverageCost
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAverageCost.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvtxtAverageCost.HeaderText = "Avg Cost";
            this.dgvtxtAverageCost.Name = "dgvtxtAverageCost";
            this.dgvtxtAverageCost.ReadOnly = true;
            this.dgvtxtAverageCost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // currentValue
            // 
            this.currentValue.DataPropertyName = "currentValue";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.currentValue.DefaultCellStyle = dataGridViewCellStyle15;
            this.currentValue.HeaderText = "Current Value";
            this.currentValue.Name = "currentValue";
            this.currentValue.ReadOnly = true;
            this.currentValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.currentValue.Visible = false;
            // 
            // stockValue
            // 
            this.stockValue.DataPropertyName = "stockValue";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Format = "N2";
            dataGridViewCellStyle16.NullValue = null;
            this.stockValue.DefaultCellStyle = dataGridViewCellStyle16;
            this.stockValue.HeaderText = "Stock Value";
            this.stockValue.Name = "stockValue";
            this.stockValue.ReadOnly = true;
            this.stockValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(29, 531);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(135, 27);
            this.btnExport.TabIndex = 1339;
            this.btnExport.Text = "Export To Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtRefNo
            // 
            this.txtRefNo.Location = new System.Drawing.Point(992, 34);
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Size = new System.Drawing.Size(127, 20);
            this.txtRefNo.TabIndex = 1342;
            this.txtRefNo.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(1019, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 1341;
            this.label5.Text = "Ref No";
            this.label5.Visible = false;
            // 
            // StockDetailsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 561);
            this.Controls.Add(this.txtRefNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dgvStockDetailsReport);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lblBatch);
            this.Controls.Add(this.cmbBatch);
            this.Controls.Add(this.cmbProduct);
            this.Controls.Add(this.lblProduct);
            this.Controls.Add(this.txtProductCode);
            this.Controls.Add(this.lblProductCode);
            this.Controls.Add(this.lblStore);
            this.Controls.Add(this.cmbGodown);
            this.Controls.Add(this.txtTodate);
            this.Controls.Add(this.ddtpStockToDate);
            this.Controls.Add(this.txtFromDate);
            this.Controls.Add(this.dtpStockFromDate);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblToDate);
            this.Controls.Add(this.lblFromDate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StockDetailsReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stock Details Report";
            this.Load += new System.EventHandler(this.StockDetailsReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStockDetailsReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBatch;
        private System.Windows.Forms.ComboBox cmbBatch;
        private System.Windows.Forms.ComboBox cmbProduct;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.TextBox txtProductCode;
        private System.Windows.Forms.Label lblProductCode;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.ComboBox cmbGodown;
        private System.Windows.Forms.TextBox txtTodate;
        private System.Windows.Forms.DateTimePicker ddtpStockToDate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.DateTimePicker dtpStockFromDate;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.DataGridView dgvStockDetailsReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn productId;
        private System.Windows.Forms.DataGridViewTextBoxColumn godownName;
        private System.Windows.Forms.DataGridViewTextBoxColumn productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn productCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn refNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn voucherTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Batch;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn rate;
        private System.Windows.Forms.DataGridViewTextBoxColumn inwardQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn outwardQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyBalalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAverageCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockValue;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.TextBox txtRefNo;
        private System.Windows.Forms.Label label5;
    }
}