﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class StockSummaryReport : Form
    {
        TransactionsGeneralFill TransactionGenericFillObj = new TransactionsGeneralFill();//used to fill product combofill
        public StockSummaryReport()
        {
            InitializeComponent();
        }
        bool isFormLoad = false;
        DateTime fromDate = PublicVariables._dtFromDate;
        DateTime toDate = PublicVariables._dtToDate;
        private void StockSummaryReport_Load(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR10:" + ex.Message;
            }
        }

        //public void clear()
        //{
        //    isFormLoad = true;                    
        //    GridFill();
        //}

        /// <summary>
        /// Fill Batch ComboBox
        /// </summary>
        public void BatchComboFill()
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                DataTable dtbl = new DataTable();
                dtbl = spBatch.BatchViewAllDistinct();
                DataRow dr = dtbl.NewRow();
                dr[0] = "All";
                //dr[1] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbBatch.DataSource = dtbl;
                cmbBatch.DisplayMember = "batchNo";
                cmbBatch.ValueMember = "batchNo";

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = ex.Message;
            }
        }
        public void GridFill()
        {
            try
            {
                #region NEW CODE
                dgvStockSummaryReport.Rows.Clear();
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    decimal productId = Convert.ToDecimal(cmbProduct.SelectedValue);
                    string pCode = txtProductCode.Text.ToString();
                    decimal storeId = Convert.ToDecimal(cmbGodown.SelectedValue);
                    string batch = Convert.ToString(cmbBatch.SelectedValue);

                    string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                    DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                    //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                    dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, PublicVariables._dtToDate, pCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        //if (againstVoucherType != "NA")
                        //{
                        //    voucherType = againstVoucherType;
                        //}
                        //else
                        //{
                        //    voucherType = dr["typeOfVoucher"].ToString();
                        //}
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                            //stockValue = inwardQty2 * rate2;
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            totalAssetValue += value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            dgvStockSummaryReport.Rows.Add();
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["pName"].Value = dr["productName"].ToString();
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["gName"].Value = dr["godownName"].ToString(); ;
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["pRate"].Value = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["srate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["qtyBal"].Value = inwardqt - outwardqt;
                            if (value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["dgvtxtAvgCostValue"].Value = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["dgvtxtAvgCostValue"].Value = "0.00";
                            }
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["stkVal"].Value = value1.ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["productCode"].Value = dr["productCode"].ToString();
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            dgvStockSummaryReport.Rows.Add();
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["pName"].Value = dr["productName"].ToString();
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["gName"].Value = dr["godownName"].ToString(); ;
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["pRate"].Value = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["srate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["qtyBal"].Value = inwardqt - outwardqt;
                            if(value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["dgvtxtAvgCostValue"].Value = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["dgvtxtAvgCostValue"].Value = "0.00";
                            }
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["stkVal"].Value = value1.ToString("N2");
                            dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["productCode"].Value = dr["productCode"].ToString();
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    decimal totalStockValue = 0;
                    for (int h = 0; h < dgvStockSummaryReport.Rows.Count; h++)
                    {
                        totalStockValue += Convert.ToDecimal(dgvStockSummaryReport.Rows[h].Cells["stkVal"].Value);
                    }
                    txtTotal.Text = totalStockValue.ToString("N2");
                    dgvStockSummaryReport.Rows.Add();
                    dgvStockSummaryReport.Rows.Add();
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["stkVal"].Value = totalStockValue.ToString("N2"); // Convert.ToDecimal(totalAssetValue + stockValue).ToString("N2");
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["dgvtxtAvgCostValue"].Value = (totalStockValue / qtyBalance).ToString("N2");
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["qtyBal"].Value = qtyBalance.ToString("N2");
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["gName"].Value = "Total Stock Value:";
                    dgvStockSummaryReport.Rows.Add();
                    dgvStockSummaryReport.Rows[dgvStockSummaryReport.Rows.Count - 1].Cells["stkVal"].Value = "=============";
                    //txtTotal.Text = Convert.ToDecimal(totalAssetValue + stockValue).ToString("N2");

                }
                catch (Exception ex)
                {
                    formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
                }
                #endregion
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKSR2:" + ex.Message;
            }
        }
        public void RackComboFill()
        {
            try
            {

                RackSP spRack = new RackSP();
                DataTable dtbl = new DataTable();
                if (cmbGodown.SelectedValue.ToString() != "System.Data.DataRowView")
                {
                    dtbl = spRack.RackViewAllByGodown(Convert.ToDecimal(cmbGodown.SelectedValue.ToString()));

                    DataRow drowSelect = dtbl.NewRow();
                    drowSelect[0] = 0;
                    drowSelect["rackName"] = "All";
                    cmbRack.DataSource = dtbl;
                    cmbRack.DisplayMember = "rackName";
                    cmbRack.ValueMember = "rackId";
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR6:" + ex.Message;
            }
        }
        public void TaxComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                TaxSP spTax = new TaxSP();
                dtbl = spTax.TaxViewAllForProduct();
                DataRow drowSelect = dtbl.NewRow();
                drowSelect[0] = 0;
                drowSelect["taxName"] = "All";
                dtbl.Rows.InsertAt(drowSelect, 0);
                cmbTax.DataSource = dtbl;
                cmbTax.ValueMember = "taxId";
                cmbTax.DisplayMember = "taxName";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR7:" + ex.Message;
            }
        }
        public void GodownComboFill()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtbl = new DataTable();
                dtbl = spGodown.GodownViewAll();
                DataRow drowSelect = dtbl.NewRow();
                drowSelect[0] = 0;
                drowSelect["godownName"] = "All";
                dtbl.Rows.InsertAt(drowSelect, 0);
                cmbGodown.DataSource = dtbl;
                cmbGodown.DisplayMember = "godownName";
                cmbGodown.ValueMember = "godownId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR5:" + ex.Message;
            }
        }
        public void BrandComboFill()
        {

            try
            {

                DataTable dtbl = new DataTable();
                BrandSP spBrand = new BrandSP();
                dtbl = spBrand.BrandViewAll();
                DataRow dr = dtbl.NewRow();
                dr["brandName"] = "All";
                dr["brandId"] = 0;
                dtbl.Rows.InsertAt(dr, 0);
                cmbBrand.DataSource = dtbl;
                cmbBrand.DisplayMember = "brandName";
                cmbBrand.ValueMember = "brandId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR4:" + ex.Message;
            }
        }
        public void SizeComboFill()
        {
            try
            {
                DataTable datbl = TransactionGenericFillObj.SizeViewAll(cmbSize, true);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR3:" + ex.Message;
            }
        }
        public void ModelNoComboFill()
        {
            try
            {
                DataTable dtbl = TransactionGenericFillObj.ModelNoViewAll(cmbModel, true);

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR2:" + ex.Message;
            }
        }
        public void ProductGroupComboFill()
        {
            try
            {
                DataTable dtbl = TransactionGenericFillObj.ProductGroupViewAll(cmbProductgroup, true);

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR1:" + ex.Message;
            }
        }
        public void Clear()
        {
            try
            {
                BrandComboFill();
                ProductGroupComboFill();
                ModelNoComboFill();
                SizeComboFill();
                GodownComboFill();
                TaxComboFill();
                ProductComboFill();
                txtproductName.Text = string.Empty;
                BatchComboFill();
                GridFill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR9:" + ex.Message;
            }
        }
        private void cmbGodown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbGodown.SelectedIndex > 0)
                {
                    RackComboFill();
                    decimal decGodownId = Convert.ToDecimal(cmbGodown.SelectedValue.ToString());
                }
                else
                {
                    RackComboFill();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR11:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            // code to implement
            GridFill();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvStockSummaryReport, "STOCK SUMMARY REPORT AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKSR01:" + ex.Message;
            }
        }

        /// <summary>
        /// Function to fill the product combobox
        /// </summary>
        public void ProductComboFill()
        {
            try
            {
                ProductSP spproduct = new ProductSP();
                DataTable dtbl = spproduct.ProductViewAll();
                cmbProduct.DataSource = dtbl;
                cmbProduct.DisplayMember = "productName";
                cmbProduct.ValueMember = "productId";
                DataRow dr = dtbl.NewRow();
                dr["productId"] = 0;
                dr["productName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "RPL4:" + ex.Message;
            }
        }

        private void btnreset_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {
            txtTotal.Text = string.Format("{0:#,##0.00}", double.Parse(txtTotal.Text));
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

        }
    }
}
