﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MATFinancials.DAL;

namespace MATFinancials.Reports
{
    public partial class frmTransactionUnitReport : Form
    {
        public frmTransactionUnitReport()
        {
            InitializeComponent();
        }

        private void TransactionUnitReport_Load(object sender, EventArgs e)
        {
            clear();
            fillGrid();
        }

        private void fillGrid()
        {
            try
            {
                dgvUnitReport.DataSource = null; //.Rows.Clear();
                DBMatConnection conn = new DBMatConnection();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                string querryStr = " SELECT l.voucherTypeId, v.voucherTypeName, l.voucherNo, SUM(l.Debit) AS Debit, SUM(l.Credit)AS Credit, (SUM(l.Debit) - SUM(l.Credit)) AS Balance " +
                    " from tbl_LedgerPosting l INNER JOIN tbl_VoucherType v on l.voucherTypeId = v.voucherTypeId where l.voucherTypeId <> 2 group by l.voucherNo, l.voucherTypeId, v.voucherTypeName ";
                ds = conn.ExecuteQuery(querryStr);
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        var requiredData = (from i in dt.AsEnumerable()
                                            where i.Field<decimal>("Balance") != 0
                                            select i).CopyToDataTable();
                        dgvUnitReport.DataSource = requiredData;
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void clear()
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                fillGrid();

            }
            catch (Exception ex)
            {

            }
        }
    }
}
