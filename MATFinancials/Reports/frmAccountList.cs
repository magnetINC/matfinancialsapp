﻿using MATFinancials.Classes.HelperClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmAccountList : Form
    {
        public frmAccountList()
        {
            InitializeComponent();
        }

        private void frmAccountList_Load(object sender, EventArgs e)
        {
            AccountGroupComboFill();
            GridFill();
        }

        public void GridFill()
        {
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            GeneralQueries queryForRetainedEarning = new GeneralQueries();
            DataTable dtbl = new DataTable();
            dtbl = spAccountLedger.AccountListViewAll(Convert.ToDecimal(cmbAccountGroup.SelectedValue), PublicVariables._dtFromDate, PublicVariables._dtToDate);
            // ----------------- calculate and replace retained earning in the datatable ---------------- //
            decimal retainedEarning = queryForRetainedEarning.retainedEarnings(PublicVariables._dtToDate);
            //string retainedEarnings = retainedEarning > 0 ? retainedEarning.ToString("N2") : "(" + (retainedEarning * -1).ToString("N2") + ")";
            if (dtbl.AsEnumerable().Where(m => m.Field<string>("ledgerName").ToLower() == "retained earning").Any())
            {
                foreach(DataRow row in dtbl.Rows)
                {
                    if(row["ledgerName"].ToString().ToLower() == "retained earning")
                    {
                        row["Balance"] = retainedEarning;
                        row["CD"] = Convert.ToDecimal(retainedEarning) > 0 ? "Cr" : "Dr";
                    } 
                }
            }
                dgvAccountList.DataSource = dtbl;
        }

        /// <summary>
        /// Account Ledger Combo Fill
        /// </summary>
        public void AccountGroupComboFill()
        {
            try
            {
                AccountGroupSP spAccountGroup = new AccountGroupSP();
                DataTable dtblAccountGroupSearch = new DataTable();
                dtblAccountGroupSearch = spAccountGroup.AccountGroupViewAllComboFill();
                DataRow dr = dtblAccountGroupSearch.NewRow();
                dr["accountGroupId"] = 0;
                dr["accountGroupName"] = "All";
                dtblAccountGroupSearch.Rows.InsertAt(dr, 0);
                cmbAccountGroup.DataSource = dtblAccountGroupSearch;
                cmbAccountGroup.ValueMember = "accountGroupId";
                cmbAccountGroup.DisplayMember = "accountGroupName";
                cmbAccountGroup.SelectedValue = 0;                
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AL8:" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvAccountList, "ACCOUNT LIST:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AL01:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            GridFill();
        }
    }
}
