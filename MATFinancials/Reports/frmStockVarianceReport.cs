﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmStockVarianceReport : Form
    {
        public frmStockVarianceReport()
        {
            InitializeComponent();
        }

        private void frmStockVarianceReport_Load(object sender, EventArgs e)
        {
            // GridFill();
            clear();
        }

        public void GridFill()
        {
            StockPostingSP spstock = new StockPostingSP();
            DataTable dtbl = new DataTable();
            try
            {
                string itemCode = "";
                if(!string.IsNullOrWhiteSpace(txtProductCode.Text))
                {
                    itemCode = txtProductCode.Text.ToString().Trim();
                }
                dtbl = spstock.StockVarianceReportViewAll(Convert.ToDecimal(cmbProduct.SelectedValue), Convert.ToDecimal(cmbGodown.SelectedValue), itemCode);
                var query = dtbl.AsEnumerable().Select(variance => new
                {
                    SLNO = variance.Field<decimal>("SL.NO"),
                    productId = variance.Field<decimal>("productId"),
                    productCode = variance.Field<string>("productCode"),
                    productName = variance.Field<string>("productName"),
                    qty = variance.Field<decimal>("qty"),
                    godownName = variance.Field<string>("godownName"),
                    SystemStock = variance.Field<string>("SystemStock"),
                    Batch = variance.Field<string>("Batch"),
                    rate = variance.Field<decimal>("rate"),
                    //stockDifference = Convert.ToDecimal( variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")  - variance.Field<decimal>("qty"),        
                    stockDifference = variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0"),
                    stockValue = variance.Field<decimal>("rate") * (variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")),
                    //stockDifference = Convert.ToDecimal(SystemStock) - Convert.ToDecimal(qty),
                    extraDate = variance.Field<DateTime>("extraDate").ToShortDateString()
                }).ToList();
                dgvStockVarianceReport.DataSource = query;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SVR02" + ex.Message;
            }

        }

        public void GodownComboFill()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtbl = new DataTable();
                dtbl = spGodown.GodownViewAll();
                DataRow drowSelect = dtbl.NewRow();
                drowSelect[0] = 0;
                drowSelect["godownName"] = "All";
                dtbl.Rows.InsertAt(drowSelect, 0);
                cmbGodown.DataSource = dtbl;
                cmbGodown.DisplayMember = "godownName";
                cmbGodown.ValueMember = "godownId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKR5:" + ex.Message;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (Messages.CloseConfirmation())
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SVR01:" + ex.Message;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //GridFill();
            clear();
        }
        public void ProductComboFill()
        {
            try
            {
                ProductSP spproduct = new ProductSP();
                DataTable dtbl = spproduct.ProductViewAll();
                cmbProduct.DataSource = dtbl;
                cmbProduct.DisplayMember = "productName";
                cmbProduct.ValueMember = "productId";
                DataRow dr = dtbl.NewRow();
                dr["productId"] = 0;
                dr["productName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "RPL4:" + ex.Message;
            }
        }

        public void clear()
        {
            txtProductCode.Text = string.Empty;
            ProductComboFill();
            GodownComboFill();
            GridFill();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridFill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STVR02" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvStockVarianceReport, "STOCK VARIANCE REPORT AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STVR01:" + ex.Message;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvStockVarianceReport.Rows.Count > 0)
                {
                    Print();
                }
                else
                {
                    Messages.InformationMessage("No Data Found");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Handles Print Function
        /// </summary>
        public void Print()
        {
            ProductSP spProduct = new ProductSP();
            try
            {
                frmReport frmReportObj = new frmReport();          
                DataSet ds = spProduct.StockVarianceReportPrinting(Convert.ToDecimal(cmbProduct.SelectedValue),Convert.ToDecimal(cmbGodown.SelectedValue), PublicVariables._decCurrentCompanyId);
                frmReportObj.MdiParent = formMDI.MDIObj;
                //frmReportObj.LedgerDetailsPrinting(ds);
                frmReportObj.StockVariancekReportPrint(ds);

            }
            catch (Exception ex)
            {
                Messages.InformationMessage(ex.Message);
            }
        }
    }
}
