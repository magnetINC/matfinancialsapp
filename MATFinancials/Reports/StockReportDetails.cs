﻿using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class StockReportDetails : Form
    {
        #region public Variables
        string calculationMethod = string.Empty;
        List<string> AllItems = new List<string> { };
        ProductSP spproduct = new ProductSP();
        #endregion
        public StockReportDetails()
        {
            InitializeComponent();
        }

        bool isFormLoad = false;
        private void StockReportDetails_Load(object sender, EventArgs e)
        {
            try
            {

                //isFormLoad = true;                
                GodownComboFill();
                ProductComboFill();
                BatchComboFill();

                dtpStockFromDate.MinDate = PublicVariables._dtFromDate;
                dtpStockFromDate.MaxDate = PublicVariables._dtToDate;
                dtpStockFromDate.Value = PublicVariables._dtFromDate;
                ddtpStockToDate.MinDate = PublicVariables._dtFromDate;
                ddtpStockToDate.MaxDate = PublicVariables._dtToDate;
                ddtpStockToDate.Value = PublicVariables._dtToDate;
                dtpStockFromDate.Text = dtpStockFromDate.Value.ToString("dd-MMM-yyyy");
                ddtpStockToDate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Focus();
                GridFill();
                //isFormLoad = false;

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD2:" + ex.Message;
            }
        }

        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtFromDate.Text, out dt);
                dtpStockFromDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD3:" + ex.Message;
            }
        }

        private void txtTodate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtTodate);
                if (txtTodate.Text == string.Empty)
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD4:" + ex.Message;
            }
        }

        public void GodownComboFill()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtbl = new DataTable();
                dtbl = spGodown.GodownViewAll();
                DataRow drowSelect = dtbl.NewRow();
                drowSelect[0] = 0;
                drowSelect[1] = "All";
                dtbl.Rows.InsertAt(drowSelect, 0);
                cmbGodown.DataSource = dtbl;
                cmbGodown.DisplayMember = "godownName";
                cmbGodown.ValueMember = "godownId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD5:" + ex.Message;
            }
        }

        public void BatchComboFill()
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                DataTable dtbl = new DataTable();
                dtbl = spBatch.BatchViewAllDistinct();
                DataRow dr = dtbl.NewRow();
                dr[0] = "All";
                //dr[1] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbBatch.DataSource = dtbl;
                cmbBatch.DisplayMember = "batchNo";
                cmbBatch.ValueMember = "batchNo";

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD13" + ex.Message;
            }
        }
        public void GridFill()
        {
            try
            {
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(txtFromDate.Text).AddDays(-1);
                }
                //if (!isFormLoad)
                //{
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtTodate);
                if (txtTodate.Text == string.Empty)
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");

                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                try
                {
                    dtbl = spstock.StockReportDetailsGridFill(Convert.ToDecimal(cmbProduct.SelectedValue),
                                Convert.ToDecimal(cmbGodown.SelectedValue), Convert.ToString(cmbBatch.SelectedValue),
                                Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text),
                                txtProductCode.Text, txtRefNo.Text);
                }
                catch (Exception ex)
                {
                    formMDI.infoError.ErrorString = "STKRD13" + ex.Message;
                }
                // when there is no transaction during the period selected but there is transaction before the period selected,
                // invoke this addition (dtbl2) and it is per store 20170406
                DataTable dtbl2 = new DataTable();
                try
                {
                    dtbl2 = spstock.StockReportDetailsGridFill(Convert.ToDecimal(cmbProduct.SelectedValue),
                                Convert.ToDecimal(cmbGodown.SelectedValue), Convert.ToString(cmbBatch.SelectedValue),
                                Convert.ToDateTime(PublicVariables._dtFromDate), Convert.ToDateTime(txtFromDate.Text),
                                txtProductCode.Text, txtRefNo.Text);
                }
                catch (Exception ex)
                {
                    formMDI.infoError.ErrorString = "STKRD13" + ex.Message;
                }
                List<decimal> storesInDtbl = new List<decimal>();
                List<decimal> storesInDtbl2 = new List<decimal>();
                List<decimal> storesWithoutTransactions = new List<decimal>();

                if ((dtbl != null && dtbl.Rows.Count > 0) && (dtbl2 != null && dtbl2.Rows.Count > 0))
                {
                    storesInDtbl = dtbl.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    storesInDtbl2 = dtbl2.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    // select stores without transaction before the period
                    storesWithoutTransactions = storesInDtbl2.Except(storesInDtbl).ToList();
                    if (dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).Any())
                    {
                        dtbl2 = dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).CopyToDataTable();
                    }
                }

                // the invoked method ends here
                dgvStockreportDetails.Rows.Clear();
                fillAllItems();
                var ItemsWithRecords = (dtbl.AsEnumerable().Select(j => j.Field<string>("productCode"))).ToList();
                List<string> itemsWithoutRecords = AllItems.Except(ItemsWithRecords).ToList();

                decimal currentStore = 0, previousStore = 0;
                decimal currentProductId = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0, inwardQty = 0, outwardQty = 0, totalQuantityIn = 0, totalQuantityOut = 0;
                decimal qtyBal = 0, stockValue = 0, stockValue2 = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    productCode = dr["productCode"].ToString();
                    currentStore = Convert.ToDecimal(dr["godownId"]);
                    string voucherType = "", refNo = "";

                    inwardQty = (from p in dtbl.AsEnumerable()
                                 where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                 select p.Field<decimal>("inwardQty")).Sum();
                    outwardQty = (from p in dtbl.AsEnumerable()
                                  where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                  select p.Field<decimal>("outwardQty")).Sum();
                    //totalQuantityIn += inwardQty;
                    //totalQuantityOut += outwardQty;
                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;
                        //againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END  ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                //computedAverageRate = (stockValue / 1); // old changed by Urefe 2017-02-21
                                //qtyBal += inwardQty2 - outwardQty2; // old changed by Urefe 2017-02-21
                                //stockValue = computedAverageRate * 1; // old changed by Urefe 2017-02-21
                                //computedAverageRate = Convert.ToDecimal(dr["rate"]);  // old changed by Urefe 2017-02-27
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                //if(computedAverageRate == 0)
                                //{
                                //    stockValue = qtyBal * rate2;
                                //}
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }
                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                            || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }

                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage;// (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    // insert subtotal per item per store and calculate the final subtotal per item (all stores together) totalAssetValue += 
                    if ((currentProductId != prevProductId && i != 0) || (currentStore != previousStore && i != 0))
                    {
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }

                        dgvStockreportDetails.Rows.Add();
                        dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                        dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (inwardQty + openingQuantityIn).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + (outwardQty + openingQuantityOut).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                        dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = ""; // computedAverageRate.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                        dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                        totalAssetValue += Convert.ToDecimal(dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value);
                        totalQuantityIn += inwardQty;
                        totalQuantityOut += outwardQty;
                        i++;
                        openingQuantityIn = 0;
                        openingQuantityOut = 0;
                    }
                    //calculation end
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (currentStore != previousStore))
                    {
                        // ---------------------- PUT THE OPENING STOCK HERE I.E AFTER THE SUBTOTAL -------------------------- //
                        computedAverageRate = rate2;
                        //decimal openingStock = methodForStockValue.currentStockValue3(lastFinYearStart, lastFinYearEnd, false, 0, false, DateTime.Now, productCode);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, productCode, currentStore);
                        dcOpeningStockForRollOver = itemParams.Item1;
                        openingQuantity = itemParams.Item2;
                        openingQuantityIn = itemParams.Item3;
                        openingQuantityOut = itemParams.Item4;
                        //dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        //openingQuantity = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        qtyBal += openingQuantity;
                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                        {
                            computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                            stockValue = qtyBal * computedAverageRate;
                        }

                        dgvStockreportDetails.Rows.Add();
                        dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["rate"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                        //totalAssetValue += Convert.ToDecimal(dcOpeningStockForRollOver);
                        totalQuantityIn += openingQuantityIn;
                        totalQuantityOut += openingQuantityOut;
                        //totalQuantityOut += outwardQty;
                        i++;

                    }
                    // ------------------------------------------------------ //
                    dgvStockreportDetails.Rows.Add();
                    dgvStockreportDetails.Rows[i].Cells["productId"].Value = dr["productId"];
                    dgvStockreportDetails.Rows[i].Cells["productName"].Value = dr["productName"];
                    dgvStockreportDetails.Rows[i].Cells["godownName"].Value = dr["godownName"];
                    dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = Convert.ToDecimal(dr["inwardQty"]).ToString("N2");
                    dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2");
                    dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = isAgainstVoucher == true ? againstVoucherType : voucherType;
                    isAgainstVoucher = false;
                    dgvStockreportDetails.Rows[i].Cells["rate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value =  computedAverageRate.ToString("N2"); 
                    if (stockValue != 0 && qtyBal != 0)
                    {
                        dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (stockValue / qtyBal).ToString("N2");
                    }
                    else
                    {
                        dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = computedAverageRate.ToString("N2");
                    }
                    dgvStockreportDetails.Rows[i].Cells["date"].Value = Convert.ToDateTime(dr["date"]).ToShortDateString();
                    dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                    dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                    dgvStockreportDetails.Rows[i].Cells["Batch"].Value = dr["Batch"].ToString();
                    dgvStockreportDetails.Rows[i].Cells["productCode"].Value = dr["productCode"];
                    dgvStockreportDetails.Rows[i].Cells["refNo"].Value = refNo; // dr["refNo"];
                    prevProductId = currentProductId;
                    previousStore = currentStore;
                    previousRunningAverage = Convert.ToDecimal(dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value);
                    i++;
                }
                //Add the sub total for the last item in the table
                dgvStockreportDetails.Rows.Add();
                if (i == 0 && i < dgvStockreportDetails.Rows.Count) { i = dgvStockreportDetails.Rows.Count; }
                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = inwardQty.ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + outwardQty.ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                totalQuantityIn += inwardQty;
                totalQuantityOut += outwardQty;
                i++;
                //Add the sub total for the last item in the table end

                // ================== Add closing stock as opening stock if transactions have not been ran on that item in that store for that period ============== //
                if (storesWithoutTransactions.Any())
                {
                    foreach (var item in storesWithoutTransactions)
                    {
                        string productCode2 = (dtbl2.AsEnumerable().Where(m => m.Field<decimal>("godownId") == item).
                            Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        var itemParams = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), true, productCode2, item);
                        decimal itemStockValue = itemParams.Item1;
                        decimal itemStock = itemParams.Item2;
                        dgvStockreportDetails.Rows.Add();
                        dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["godownName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("godownName"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                        }
                        dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }

                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for all items)============== //
                if (itemsWithoutRecords.Any() && cmbProduct.SelectedIndex == 0)
                {
                    spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        dgvStockreportDetails.Rows.Add();
                        dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                        }
                        dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for a single item) ============== //
                else if (itemsWithoutRecords.Any() && cmbProduct.SelectedIndex != 0 && (dtbl == null || dtbl.Rows.Count < 1))
                {
                    spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    itemsWithoutRecords.Clear(); itemsWithoutRecords.AddRange(dt.AsEnumerable().Where(k => k.Field<decimal>("productId") == Convert.ToDecimal(cmbProduct.SelectedValue))
                        .Select(k => k.Field<string>("productCode")).ToList());
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        dgvStockreportDetails.Rows.Add();
                        dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                        }
                        dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================================== end region ================================ //
                //decimal productID = Convert.ToDecimal(dgvStockreportDetails.Rows[i - 1].Cells["productId"].Value);
                #region what is this block of code meant for
                /*
                decimal inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == prevProductId
                                    select p.Field<decimal>("inwardQty")).Sum();
                decimal outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == prevProductId
                                     select p.Field<decimal>("outwardQty")).Sum();

                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[dgvStockreportDetails.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (Convert.ToDecimal(inwardqt) + openingQuantityIn).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = (Convert.ToDecimal(outwardqt) + openingQuantityOut).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = (stockValue + dcOpeningStockForRollOver).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i != 0 ? i - 1 : i].Cells["productName"].Value; //+ " In " + dgvStockreportDetails.Rows[i !=0 ? i - 1: i].Cells["godownName"].Value;
                */
                #endregion
                //decimal totalAsset = totalAssetValue + stockValue;
                //total inward and outward quantity and total stock value
                decimal totalAsset = totalAssetValue + stockValue + stockValue2;// - dcOpeningStockForRollOver;
                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[i + 1].Cells["stockValue"].Value = "==========";
                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvStockreportDetails.Rows[i + 2].Cells["qtyBalalance"].Value = (totalQuantityIn - totalQuantityOut).ToString("N2");
                if (totalQuantityIn - totalQuantityOut != 0)
                {
                    dgvStockreportDetails.Rows[i + 2].Cells["dgvtxtAverageCost"].Value = (totalAsset / (totalQuantityIn - totalQuantityOut)).ToString("N2");
                }
                dgvStockreportDetails.Rows[i + 2].Cells["stockValue"].Value = totalAsset.ToString("N2");
                dgvStockreportDetails.Rows[i + 2].Cells["voucherTypeName"].Value = "Total Stock Value:";
                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[i + 3].Cells["stockValue"].Value = "==========";
                //}
            }

            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
        }
        private void dtpStockFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpStockFromDate.Value;
                this.txtFromDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD1:" + ex.Message;
            }
        }
        private void ddtpStockToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.ddtpStockToDate.Value;
                this.txtTodate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD9:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                DateValidation ObjValidation = new DateValidation();
                ObjValidation.DateValidationFunction(txtTodate);
                if (Convert.ToDateTime(txtTodate.Text) < Convert.ToDateTime(txtFromDate.Text))
                {
                    MessageBox.Show("Todate should be greater than Fromdate", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    GridFill();
                }
                else
                {

                    GridFill();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD10:" + ex.Message;
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            try
            {
                clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD11:" + ex.Message;
            }
        }

        public void clear()
        {
            isFormLoad = true;

            dtpStockFromDate.MinDate = PublicVariables._dtFromDate;
            dtpStockFromDate.MaxDate = PublicVariables._dtToDate;
            dtpStockFromDate.Value = PublicVariables._dtFromDate;

            ddtpStockToDate.MinDate = PublicVariables._dtFromDate;
            ddtpStockToDate.MaxDate = PublicVariables._dtToDate;
            ddtpStockToDate.Value = PublicVariables._dtToDate;
            dtpStockFromDate.Text = dtpStockFromDate.Value.ToString("dd-MMM-yyyy");
            ddtpStockToDate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
            txtTodate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
            txtProductCode.Text = string.Empty;
            isFormLoad = false;
            ProductComboFill();
            GodownComboFill();
            GridFill();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {

            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvStockreportDetails, "STOCK DETAILS AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD12:" + ex.Message;
            }
        }
        public void ProductComboFill()
        {
            try
            {

                spproduct = new ProductSP();
                DataTable dtbl = new DataTable();
                dtbl = spproduct.ProductViewAll();
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    AllItems.Add(dtbl.Rows[i]["productCode"].ToString());
                }
                cmbProduct.DataSource = dtbl;
                cmbProduct.DisplayMember = "productName";
                cmbProduct.ValueMember = "productId";
                DataRow dr = dtbl.NewRow();
                dr["productId"] = 0;
                dr["productName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "RPL4:" + ex.Message;
            }
        }
        private void fillAllItems()
        {
            if (txtProductCode.Text != string.Empty || cmbBatch.SelectedIndex > 0)
            {
                try
                {
                    spproduct = new ProductSP();
                    AllItems.Clear();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    if (dt.AsEnumerable().Where(k => k.Field<string>("productCode") == txtProductCode.Text.ToString()).Any())
                    {
                        // filter by product code only
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("productCode") == txtProductCode.Text.ToString()).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    if (dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == cmbBatch.Text.ToString()).Any())
                    {
                        // filter by batch only
                        AllItems.Clear();
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == cmbBatch.Text.ToString()).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    // add filter by batch and product code combined below here
                }
                catch (Exception ex)
                {
                    formMDI.infoError.ErrorString = "SDR1:" + ex.Message;
                }
            }

        }
    }
}
