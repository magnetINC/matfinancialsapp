﻿namespace MATFinancials.Reports
{
    partial class frmBankReconciliationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBankReconciliationReport));
            this.dgvBankReconciliation = new System.Windows.Forms.DataGridView();
            this.dgvtxtParticulars = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtAmount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGroupId1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtGroupId2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpStatrmentTo = new System.Windows.Forms.DateTimePicker();
            this.txtStatementTo = new System.Windows.Forms.TextBox();
            this.dtpStatementFrom = new System.Windows.Forms.DateTimePicker();
            this.txtStatementFrom = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbBankAccount = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankReconciliation)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBankReconciliation
            // 
            this.dgvBankReconciliation.AllowUserToAddRows = false;
            this.dgvBankReconciliation.AllowUserToDeleteRows = false;
            this.dgvBankReconciliation.AllowUserToOrderColumns = true;
            this.dgvBankReconciliation.AllowUserToResizeRows = false;
            this.dgvBankReconciliation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBankReconciliation.BackgroundColor = System.Drawing.Color.White;
            this.dgvBankReconciliation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBankReconciliation.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBankReconciliation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBankReconciliation.ColumnHeadersHeight = 35;
            this.dgvBankReconciliation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvBankReconciliation.ColumnHeadersVisible = false;
            this.dgvBankReconciliation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtParticulars,
            this.dgvtxtValue,
            this.dgvtxtAmount1,
            this.dgvtxtAmount2,
            this.dgvtxtGroupId1,
            this.dgvtxtGroupId2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBankReconciliation.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvBankReconciliation.EnableHeadersVisualStyles = false;
            this.dgvBankReconciliation.GridColor = System.Drawing.Color.White;
            this.dgvBankReconciliation.Location = new System.Drawing.Point(148, 119);
            this.dgvBankReconciliation.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.dgvBankReconciliation.MultiSelect = false;
            this.dgvBankReconciliation.Name = "dgvBankReconciliation";
            this.dgvBankReconciliation.ReadOnly = true;
            this.dgvBankReconciliation.RowHeadersVisible = false;
            this.dgvBankReconciliation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBankReconciliation.Size = new System.Drawing.Size(758, 340);
            this.dgvBankReconciliation.TabIndex = 1185;
            // 
            // dgvtxtParticulars
            // 
            this.dgvtxtParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgvtxtParticulars.FillWeight = 131.3362F;
            this.dgvtxtParticulars.HeaderText = "Particulars";
            this.dgvtxtParticulars.Name = "dgvtxtParticulars";
            this.dgvtxtParticulars.ReadOnly = true;
            this.dgvtxtParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtParticulars.Width = 260;
            // 
            // dgvtxtValue
            // 
            this.dgvtxtValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvtxtValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvtxtValue.FillWeight = 131.3362F;
            this.dgvtxtValue.HeaderText = "Value";
            this.dgvtxtValue.Name = "dgvtxtValue";
            this.dgvtxtValue.ReadOnly = true;
            this.dgvtxtValue.Width = 170;
            // 
            // dgvtxtAmount1
            // 
            this.dgvtxtAmount1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvtxtAmount1.FillWeight = 76.4138F;
            this.dgvtxtAmount1.HeaderText = "Amount";
            this.dgvtxtAmount1.Name = "dgvtxtAmount1";
            this.dgvtxtAmount1.ReadOnly = true;
            this.dgvtxtAmount1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtAmount1.Width = 155;
            // 
            // dgvtxtAmount2
            // 
            this.dgvtxtAmount2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dgvtxtAmount2.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvtxtAmount2.FillWeight = 60.9137F;
            this.dgvtxtAmount2.HeaderText = "Amount2";
            this.dgvtxtAmount2.Name = "dgvtxtAmount2";
            this.dgvtxtAmount2.ReadOnly = true;
            this.dgvtxtAmount2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtAmount2.Width = 155;
            // 
            // dgvtxtGroupId1
            // 
            this.dgvtxtGroupId1.HeaderText = "GroupId1";
            this.dgvtxtGroupId1.Name = "dgvtxtGroupId1";
            this.dgvtxtGroupId1.ReadOnly = true;
            this.dgvtxtGroupId1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId1.Visible = false;
            // 
            // dgvtxtGroupId2
            // 
            this.dgvtxtGroupId2.HeaderText = "GroupId2";
            this.dgvtxtGroupId2.Name = "dgvtxtGroupId2";
            this.dgvtxtGroupId2.ReadOnly = true;
            this.dgvtxtGroupId2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dgvtxtGroupId2.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(845, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 16);
            this.label12.TabIndex = 1196;
            this.label12.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(475, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 16);
            this.label9.TabIndex = 1195;
            this.label9.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(514, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 16);
            this.label2.TabIndex = 1194;
            this.label2.Text = "*";
            // 
            // dtpStatrmentTo
            // 
            this.dtpStatrmentTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpStatrmentTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStatrmentTo.Location = new System.Drawing.Point(815, 54);
            this.dtpStatrmentTo.Name = "dtpStatrmentTo";
            this.dtpStatrmentTo.Size = new System.Drawing.Size(23, 22);
            this.dtpStatrmentTo.TabIndex = 1193;
            this.dtpStatrmentTo.ValueChanged += new System.EventHandler(this.dtpStatrmentTo_ValueChanged);
            // 
            // txtStatementTo
            // 
            this.txtStatementTo.Location = new System.Drawing.Point(664, 54);
            this.txtStatementTo.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.txtStatementTo.Name = "txtStatementTo";
            this.txtStatementTo.Size = new System.Drawing.Size(151, 22);
            this.txtStatementTo.TabIndex = 1188;
            this.txtStatementTo.TextChanged += new System.EventHandler(this.txtStatementTo_TextChanged);
            this.txtStatementTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatementTo_KeyDown);
            this.txtStatementTo.Leave += new System.EventHandler(this.txtStatementTo_Leave);
            // 
            // dtpStatementFrom
            // 
            this.dtpStatementFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpStatementFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStatementFrom.Location = new System.Drawing.Point(442, 52);
            this.dtpStatementFrom.Name = "dtpStatementFrom";
            this.dtpStatementFrom.Size = new System.Drawing.Size(23, 22);
            this.dtpStatementFrom.TabIndex = 1192;
            this.dtpStatementFrom.ValueChanged += new System.EventHandler(this.dtpStatementFrom_ValueChanged);
            // 
            // txtStatementFrom
            // 
            this.txtStatementFrom.Location = new System.Drawing.Point(291, 52);
            this.txtStatementFrom.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.txtStatementFrom.Name = "txtStatementFrom";
            this.txtStatementFrom.Size = new System.Drawing.Size(151, 22);
            this.txtStatementFrom.TabIndex = 1187;
            this.txtStatementFrom.TextChanged += new System.EventHandler(this.txtStatementFrom_TextChanged);
            this.txtStatementFrom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatementFrom_KeyDown);
            this.txtStatementFrom.Leave += new System.EventHandler(this.txtStatementFrom_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(530, 57);
            this.label13.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 16);
            this.label13.TabIndex = 1191;
            this.label13.Text = "Statement Date To";
            // 
            // cmbBankAccount
            // 
            this.cmbBankAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBankAccount.FormattingEnabled = true;
            this.cmbBankAccount.Location = new System.Drawing.Point(291, 20);
            this.cmbBankAccount.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.cmbBankAccount.Name = "cmbBankAccount";
            this.cmbBankAccount.Size = new System.Drawing.Size(220, 24);
            this.cmbBankAccount.TabIndex = 1186;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(145, 56);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 16);
            this.label10.TabIndex = 1190;
            this.label10.Text = "Statement Date From";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(145, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 6, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 1189;
            this.label7.Text = "Bank Account";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(793, 86);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(113, 27);
            this.btnSearch.TabIndex = 1197;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(615, 468);
            this.btnExport.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(174, 33);
            this.btnExport.TabIndex = 1300;
            this.btnExport.Text = "Export To Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(796, 468);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(114, 33);
            this.btnPrint.TabIndex = 1299;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // frmBankReconciliationReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpStatrmentTo);
            this.Controls.Add(this.txtStatementTo);
            this.Controls.Add(this.dtpStatementFrom);
            this.Controls.Add(this.txtStatementFrom);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cmbBankAccount);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvBankReconciliation);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBankReconciliationReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bank Reconciliation Report";
            this.Load += new System.EventHandler(this.frmBankReconciliationReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankReconciliation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBankReconciliation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpStatrmentTo;
        private System.Windows.Forms.TextBox txtStatementTo;
        private System.Windows.Forms.DateTimePicker dtpStatementFrom;
        private System.Windows.Forms.TextBox txtStatementFrom;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbBankAccount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtParticulars;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtAmount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtGroupId2;
    }
}