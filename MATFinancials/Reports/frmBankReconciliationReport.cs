﻿using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmBankReconciliationReport : Form
    {
        #region public variables

        Padding newPadding = new Padding(50, 0, 0, 0);
        #endregion
        public frmBankReconciliationReport()
        {
            InitializeComponent();
        }

        private void frmBankReconciliationReport_Load(object sender, EventArgs e)
        {
            clear();
            fillGrid();
        }

        private void fillGrid()
        {
            try
            {
                GeneralQueries bankReconciliation = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();
                BankReconciliationSP spBankReconciliation = new BankReconciliationSP();
                dgvBankReconciliation.Rows.Clear();
                DataSet ds = new DataSet();
                DataTable dtblBankReconciled = new DataTable();
                DataTable dtblBankUnreconciled = new DataTable();
                decimal totalWithdrawal = 0;
                decimal totalDeposit = 0;
                decimal bankBalance = 0;
                DateTime dateFrom = Convert.ToDateTime(txtStatementFrom.Text).AddDays(-2);
                DateTime dateTo = Convert.ToDateTime(txtStatementFrom.Text).AddDays(-1);
                if(Convert.ToDateTime(txtStatementFrom.Text) > Convert.ToDateTime(txtStatementTo.Text))
                {
                    MessageBox.Show("Statement to date should be greater than statement from date", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (cmbBankAccount.SelectedIndex > -1)
                {
                    string queryStr = string.Format("select distinct top 1 (b.ReconStartDate), b.ReconEndDate from tbl_BankReconciliation b " +
                        " INNER JOIN tbl_LedgerPosting l on b.ledgerPostingId = l.ledgerPostingId where l.ledgerId = '{0}' AND " +
                        " b.ReconEndDate < '{1}' order by b.ReconEndDate desc ", cmbBankAccount.SelectedValue, Convert.ToDateTime(txtStatementFrom.Text));
                    ds = conn.ExecuteQuery(queryStr); // (queryStr); // != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate;
                    //decimal lastReconciledBalance = bankReconciliation.lastBankReconciliation(Convert.ToDecimal(cmbBankAccount.SelectedValue.ToString()), lastReconciliation, lastReconciliation);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        dateFrom = Convert.ToDateTime(ds.Tables[0].Rows[0]["ReconStartDate"]);
                        dateTo = Convert.ToDateTime(ds.Tables[0].Rows[0]["ReconEndDate"]);
                    }
                    decimal lastReconciledBalance = bankReconciliation.lastBankReconciliation(Convert.ToDecimal(cmbBankAccount.SelectedValue.ToString()), dateFrom, dateTo);

                    dtblBankReconciled = spBankReconciliation.BankReconciliationFillReconcile(Convert.ToDecimal(cmbBankAccount.SelectedValue.ToString()), Convert.ToDateTime(txtStatementFrom.Text), Convert.ToDateTime(txtStatementTo.Text));
                    dtblBankUnreconciled = spBankReconciliation.BankReconciliationUnrecocile(Convert.ToDecimal(cmbBankAccount.SelectedValue.ToString()), Convert.ToDateTime(txtStatementFrom.Text), Convert.ToDateTime(txtStatementTo.Text), true);

                    // ===================== LAST RECONCILED BALANCE ============================ //
                    dgvBankReconciliation.Rows.Add();
                    //dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Last Reconciled Balance";
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = lastReconciledBalance >= 0 ? lastReconciledBalance.ToString("N2") : "(" + (lastReconciledBalance * -1).ToString("N2") + ")";
                    dgvBankReconciliation.Rows.Add();

                    if (dtblBankReconciled.Rows.Count > 0)
                    {
                        // ===================== TOTAL DEPOSIT AS PER COMPANY BOOK ======================= //
                        var deposits = (from i in dtblBankReconciled.AsEnumerable()
                                        select i.Field<decimal>("debit")).Sum();
                        deposits += (from i in dtblBankUnreconciled.AsEnumerable()
                                     select i.Field<decimal>("debit")).Sum();
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Total Deposit as per Company Book";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = deposits >= 0 ? deposits.ToString("N2") : "(" + (deposits * -1).ToString("N2") + ")";
                        dgvBankReconciliation.Rows.Add();

                        // ==================== TOTAL WITHDRAWAL AS PER COMPANY BOOK ================ //
                        var withDrawals = (from i in dtblBankReconciled.AsEnumerable()
                                           select i.Field<decimal>("credit")).Sum();
                        withDrawals += (from i in dtblBankUnreconciled.AsEnumerable()
                                        select i.Field<decimal>("credit")).Sum();
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Total Withdrawal as per Company Book";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "(" + withDrawals.ToString("N2") + ")";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Style.Font = new Font(Font, FontStyle.Underline);
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = (deposits - withDrawals) >= 0 ? (deposits - withDrawals).ToString("N2") : "(" + ((deposits - withDrawals) * -1).ToString("N2") + ")"; ;

                        // ====================== NET CASH BOOK BALANCE ============================ //
                        dgvBankReconciliation.Rows.Add();
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "NET Cash Book Balance";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = (deposits - withDrawals + lastReconciledBalance) >= 0 ? 
                            (deposits - withDrawals + lastReconciledBalance).ToString("N2") : "(" + ((deposits - withDrawals + lastReconciledBalance) * -1).ToString("N2") + ")";
                        dgvBankReconciliation.Rows.Add();

                        bankBalance += (deposits - withDrawals) + lastReconciledBalance;
                    }

                    // ==================== UNCLEARED TRANSACTIONS ============================== //
                    dgvBankReconciliation.Rows.Add();
                    if (dtblBankUnreconciled.Rows.Count > 0)
                    {
                        var zeroValue = new List<decimal> { 0 };
                        var unpresentedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("debit"))).CopyToDataTable();
                        var uncreditedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("credit"))).CopyToDataTable();
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Uncleared Transactions".ToUpper();
                        dgvBankReconciliation.Rows.Add();

                        //-------------------- uncredited cheques (uncleared deposits)-------------------------------------//
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Unpresented Cheques (Withdrawals)";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtValue"].Value = "Cheque Number";
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "Amount";
                        dgvBankReconciliation.Rows.Add();

                        totalWithdrawal = 0;
                        for (int j = 0; j < uncreditedCheques.Rows.Count; j++)
                        {
                            totalWithdrawal += Convert.ToDecimal(uncreditedCheques.Rows[j]["credit"].ToString());
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtValue"].Value = uncreditedCheques.Rows[j]["chequeNo"].ToString();
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = uncreditedCheques.Rows[j]["voucherTypeName"].ToString();
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = Convert.ToDecimal(uncreditedCheques.Rows[j]["credit"]).ToString("N2");
                            dgvBankReconciliation.Rows.Add();
                        }
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 2].Cells["dgvtxtAmount2"].Style.Font = new Font(Font, FontStyle.Underline);
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = totalWithdrawal >= 0 ? totalWithdrawal.ToString("N2") : "(" + (totalWithdrawal * -1).ToString("N2") + ")";
                        bankBalance = bankBalance + totalWithdrawal;
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = bankBalance >= 0 ? bankBalance.ToString("N2") : "(" + (bankBalance * -1).ToString("N2") + ")";

                        //-------------------- unpresented cheques (uncleared withdrawals)-------------------------------------//
                        dgvBankReconciliation.Rows.Add();
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Font = new Font(Font, FontStyle.Underline);
                        dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Uncredited Cheques (Deposits)";
                        dgvBankReconciliation.Rows.Add();

                        totalDeposit = 0;
                        for (int j = 0; j < unpresentedCheques.Rows.Count; j++)
                        {
                            totalDeposit += Convert.ToDecimal(unpresentedCheques.Rows[j]["debit"].ToString());
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtValue"].Value = unpresentedCheques.Rows[j]["chequeNo"].ToString();
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = unpresentedCheques.Rows[j]["voucherTypeName"].ToString();
                            dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = Convert.ToDecimal(unpresentedCheques.Rows[j]["debit"]).ToString("N2");
                            dgvBankReconciliation.Rows.Add();
                        }
                    }
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 2].Cells["dgvtxtAmount2"].Style.Font = new Font(Font, FontStyle.Underline);
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = totalDeposit >= 0 ? totalDeposit.ToString("N2") : "(" + (totalDeposit * -1).ToString("N2") + ")";
                    bankBalance = bankBalance - totalDeposit;

                    // ======================== BALANCE AS PER BANK STATEMENT ======================= //
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtParticulars"].Value = "Balance as per Bank Statement";
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = bankBalance >= 0 ? bankBalance.ToString("N2") : "(" + (bankBalance * -1).ToString("N2") + ")";
                    dgvBankReconciliation.Rows.Add();
                    dgvBankReconciliation.Rows[dgvBankReconciliation.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "==============";
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void clear()
        {
            bankAccountComboFill();
            /*-------date setting at the time of loading--------*/
            dtpStatementFrom.MinDate = PublicVariables._dtFromDate;
            dtpStatementFrom.MaxDate = PublicVariables._dtToDate;
            dtpStatementFrom.Value = PublicVariables._dtCurrentDate;
            dtpStatrmentTo.MinDate = PublicVariables._dtFromDate;
            dtpStatrmentTo.MaxDate = PublicVariables._dtToDate;
            dtpStatrmentTo.Value = PublicVariables._dtCurrentDate;
            dgvBankReconciliation.Rows.Clear();
            /*--------------------------------------------------*/
            fillGrid();
        }
        public void bankAccountComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                dtbl = obj.BankComboFill();
                cmbBankAccount.DataSource = dtbl;
                cmbBankAccount.ValueMember = "ledgerId";
                cmbBankAccount.DisplayMember = "ledgerName";
                cmbBankAccount.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR2:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                fillGrid();
            }
            catch (Exception ex)
            {

            }
        }

        private void dtpStatrmentTo_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpStatrmentTo.Value;
                this.txtStatementTo.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR12:" + ex.Message;
            }
        }

        private void dtpStatementFrom_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpStatementFrom.Value;
                this.txtStatementFrom.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR10:" + ex.Message;
            }
        }

        private void txtStatementFrom_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation obj = new DateValidation();
                obj.DateValidationFunction(txtStatementFrom);
                if (txtStatementFrom.Text == string.Empty)
                {
                    txtStatementFrom.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
                dtpStatementFrom.Value = Convert.ToDateTime(txtStatementFrom.Text);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR11:" + ex.Message;
            }
        }

        private void txtStatementTo_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation obj = new DateValidation();
                obj.DateValidationFunction(txtStatementTo);
                if (txtStatementTo.Text == string.Empty)
                {
                    txtStatementTo.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
                dtpStatrmentTo.Value = Convert.ToDateTime(txtStatementTo.Text);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR13:" + ex.Message;
            }
        }

        private void txtStatementFrom_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtStatementTo.Focus();
                    txtStatementTo.SelectionStart = 0;
                    txtStatementTo.SelectionLength = 0;
                }
                if (txtStatementFrom.Text == string.Empty || txtStatementFrom.SelectionStart == 0)
                {
                    if (e.KeyCode == Keys.Back)
                    {
                        cmbBankAccount.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR21:" + ex.Message;
            }
        }

        private void txtStatementTo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (txtStatementTo.Text == string.Empty || txtStatementTo.SelectionStart == 0)
                    {
                        txtStatementFrom.Focus();
                        txtStatementFrom.SelectionLength = 0;
                        txtStatementTo.SelectionStart = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR22:" + ex.Message;
            }
        }

        private void txtStatementFrom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStatementTo_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            exportFunction();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printFunction();
            }
            catch (Exception ex)
            {

            }
        }

        private void printFunction()
        {
            try
            {
                if (dgvBankReconciliation.Rows.Count == 0)
                {
                    MessageBox.Show("No Row To Print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    Print(PublicVariables._dtToDate);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS11:" + ex.Message;
            }
        }
        public void Print(DateTime toDate)
        {
            try
            {
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataSet dsBankReconciliation = getDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.bankReconciliationReportPrint(dsBankReconciliation);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS4:" + ex.Message;
            }
        }
        public DataSet getDataSet()
        {
            DataSet dsBankRecon = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtBankRecon = dtblBankRecon();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.FundFlowReportPrintCompany(PublicVariables._decCurrentCompanyId);
                dsBankRecon.Tables.Add(dtBankRecon);
                dsBankRecon.Tables.Add(dtblCompany);
                return dsBankRecon;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS3:" + ex.Message;
            }
            return dsBankRecon;
        }
        public DataTable dtblBankRecon()
        {
            DataTable dtBankReconcile = new DataTable();
            try
            {
                dtBankReconcile.Columns.Add("Particulars");
                dtBankReconcile.Columns.Add("Reference");
                dtBankReconcile.Columns.Add("Amount1");
                dtBankReconcile.Columns.Add("Amount2");
                dtBankReconcile.Columns.Add("Bank");
                dtBankReconcile.Columns.Add("fromDate");
                dtBankReconcile.Columns.Add("toDate");
                DataRow drBankReconcile = null;
                foreach (DataGridViewRow dr in dgvBankReconciliation.Rows)
                {
                    drBankReconcile = dtBankReconcile.NewRow();
                    drBankReconcile["Particulars"] = dr.Cells["dgvtxtParticulars"].Value;
                    drBankReconcile["Reference"] = dr.Cells["dgvtxtValue"].Value;
                    drBankReconcile["Amount1"] = dr.Cells["dgvtxtAmount1"].Value;
                    drBankReconcile["Amount2"] = dr.Cells["dgvtxtAmount2"].Value;
                    drBankReconcile["Bank"] = cmbBankAccount.Text.ToString();
                    drBankReconcile["fromDate"] = txtStatementFrom.Text.ToString();
                    drBankReconcile["toDate"] = txtStatementTo.Text.ToString();
                    dtBankReconcile.Rows.Add(drBankReconcile);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BS2:" + ex.Message;
            }
            return dtBankReconcile;
        }

        private void exportFunction()
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvBankReconciliation, "Bank Reconciliation for the Period:", 0, 0, "Excel", Convert.ToDateTime(txtStatementFrom.Text), Convert.ToDateTime(txtStatementTo.Text), "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }
    }
}

