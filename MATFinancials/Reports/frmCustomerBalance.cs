﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmCustomerBalance : Form
    {
        Padding newPadding = new Padding(50, 0, 0, 0);
        int intNoOfRowsToPrint = 0;
        public frmCustomerBalance()
        {
            InitializeComponent();
        }

        private void frmCustomerBalance_Load(object sender, EventArgs e)
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString("dd-MMM-yyyy");
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString("dd-MMM-yyyy");
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }

        private void FillGrid()
        {
            try
            {
                int gridRow = 0;
                decimal totalCredit = 0;
                DataSet dsCustomer = new DataSet();
                DataTable dtblCustomer = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();

                DBMatConnection connection = new DBMatConnection();
                string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                    "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                    "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 26 AND lp.date <= '{0}'" +
                    "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", txtToDate.Text.ToString());
                dsCustomer = connection.ExecuteQuery(query);

                dgvCustomerBalance.Rows.Clear();
                dtblCustomer = dsCustomer.Tables[0];

                dtblCustomer = (from d in dtblCustomer.AsEnumerable()
                                where d.Field<decimal>("openingBalance") != 0
                                select d).CopyToDataTable();

                this.dgvCustomerBalance.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                this.dgvCustomerBalance.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();

                int gridRowNumber = 0;
                for (int i = 0; i < dtblCustomer.Rows.Count; i++)
                {
                    dgvCustomerBalance.Rows.Add();
                    gridRowNumber++;
                    dgvCustomerBalance.Rows[i].Cells["dgvtxtSNo"].Value = gridRowNumber;
                    dgvCustomerBalance.Rows[i].Cells["dgvtxtLedgerName"].Value = dtblCustomer.Rows[i]["ledgerName"].ToString();
                    dgvCustomerBalance.Rows[i].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(dtblCustomer.Rows[i]["openingBalance"]).ToString("N2");
                    totalCredit += Convert.ToDecimal(dtblCustomer.Rows[i]["openingBalance"]);
                }
                gridRow = dgvCustomerBalance.Rows.Count - 1;
                dgvCustomerBalance.Rows.Add();
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "____________________";
                dgvCustomerBalance.Rows.Add();
                gridRow++;
                dgvCustomerBalance.Rows[gridRow].DefaultCellStyle.Font = new Font(dgvCustomerBalance.Font, FontStyle.Bold);
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "Total";
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(totalCredit).ToString("N2");
                dgvCustomerBalance.Rows.Add();
                gridRow++;
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "====================";
                intNoOfRowsToPrint = dtblCustomer.Rows.Count;
            }
            catch (Exception ex)
            {

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (intNoOfRowsToPrint == 0)
                {
                    MessageBox.Show("No row to print", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Print();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL11:" + ex.Message;
            }
        }

        private void Print()
        {
            try
            {
                DataSet dsCustomerBalance = GetDataSet();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.CustomerBalancePrinting(dsCustomerBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL4:" + ex.Message;
            }
        }
        private DataSet GetDataSet()
        {
            DataSet dsCustomerBalance = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblCustomerBalance = GetDataTable();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.ProfitAndLossReportPrintCompany(PublicVariables._decCurrentCompanyId);
                dsCustomerBalance.Tables.Add(dtblCustomerBalance);
                dsCustomerBalance.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL3:" + ex.Message;
            }
            return dsCustomerBalance;
        }

        private DataTable GetDataTable()
        {
            DataTable dtblCustomerBalance = new DataTable();
            try
            {
                dtblCustomerBalance.Columns.Add("S/No");
                dtblCustomerBalance.Columns.Add("Customer");
                dtblCustomerBalance.Columns.Add("Balance");
                DataRow drow = null;
                foreach (DataGridViewRow dr in dgvCustomerBalance.Rows)
                {
                    drow = dtblCustomerBalance.NewRow();
                    drow["S/No"] = dr.Cells["dgvtxtSNo"].Value;
                    drow["Customer"] = dr.Cells["dgvtxtLedgerName"].Value;
                    drow["Balance"] = dr.Cells["dgvtxtBalance"].Value;
                    dtblCustomerBalance.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL2:" + ex.Message;
            }
            return dtblCustomerBalance;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {

            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = dtpFromDate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL6:" + ex.Message;
            }
        }

        private void dtpTodate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpTodate.Text;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL7:" + ex.Message;
            }
        }
        public void Clear()
        {
            try
            {
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpTodate.Value = PublicVariables._dtToDate;
                dtpTodate.Text = dtpTodate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Text = Convert.ToDateTime(PublicVariables._dtFromDate).ToString("dd-MMM-yyyy");
                txtToDate.Text = Convert.ToDateTime(PublicVariables._dtToDate).ToString("dd-MMM-yyyy");
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "PAL10:" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvCustomerBalance, "CUSTOMER BALANCE AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB2:" + ex.Message;
            }
        }
    }
}
