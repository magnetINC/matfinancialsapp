﻿using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class StockDetailsReport : Form
    {
        public StockDetailsReport()
        {
            InitializeComponent();
        }
        #region public Variables
        string calculationMethod = string.Empty;
        List<string> AllItems = new List<string> { };
        List<decimal> AllStores = new List<decimal> { };
        ProductSP spproduct = new ProductSP();
        #endregion
        bool isFormLoad = false, isOpeningRate = false;
        private void StockDetailsReport_Load(object sender, EventArgs e)
        {
            try
            {
                //isFormLoad = true;                
                GodownComboFill();
                ProductComboFill();
                BatchComboFill();

                dtpStockFromDate.MinDate = PublicVariables._dtFromDate;
                dtpStockFromDate.MaxDate = PublicVariables._dtToDate;
                dtpStockFromDate.Value = PublicVariables._dtFromDate;
                ddtpStockToDate.MinDate = PublicVariables._dtFromDate;
                ddtpStockToDate.MaxDate = PublicVariables._dtToDate;
                ddtpStockToDate.Value = PublicVariables._dtToDate;
                dtpStockFromDate.Text = dtpStockFromDate.Value.ToString("dd-MMM-yyyy");
                ddtpStockToDate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
                txtFromDate.Focus();
                GridFill();
                //isFormLoad = false;

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD2:" + ex.Message;
            }
        }
        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtFromDate.Text, out dt);
                dtpStockFromDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD3:" + ex.Message;
            }
        }
        private void txtTodate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtTodate);
                if (txtTodate.Text == string.Empty)
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD4:" + ex.Message;
            }
        }
        public void GodownComboFill()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtbl = new DataTable();
                dtbl = spGodown.GodownViewAll();
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    AllStores.Add(Convert.ToDecimal(dtbl.Rows[i]["godownId"]));
                }
                DataRow drowSelect = dtbl.NewRow();
                drowSelect[0] = 0;
                drowSelect[1] = "All";
                dtbl.Rows.InsertAt(drowSelect, 0);
                cmbGodown.DataSource = dtbl;
                cmbGodown.DisplayMember = "godownName";
                cmbGodown.ValueMember = "godownId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD5:" + ex.Message;
            }
        }
        public void BatchComboFill()
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                DataTable dtbl = new DataTable();
                dtbl = spBatch.BatchViewAllDistinct();
                DataRow dr = dtbl.NewRow();
                dr[0] = "All";
                //dr[1] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbBatch.DataSource = dtbl;
                cmbBatch.DisplayMember = "batchNo";
                cmbBatch.ValueMember = "batchNo";

            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD13" + ex.Message;
            }
        }
        public void GridFill()
        {
            try
            {
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(txtFromDate.Text).AddDays(-1);
                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                StockPostingSP spstock = new StockPostingSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtTodate);
                if (txtTodate.Text == string.Empty)
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                decimal productId = 0, storeId = 0 ;
                string produceCode = "", batch = "All", referenceNo = "";
                if (cmbProduct.SelectedIndex != 0)
                {
                    productId = Convert.ToDecimal(cmbProduct.SelectedValue);
                }
                if (cmbGodown.SelectedIndex != 0)
                {
                    storeId = Convert.ToDecimal(cmbGodown.SelectedValue);
                }
                if (cmbBatch.SelectedIndex != 0)
                {
                    batch = cmbBatch.SelectedValue.ToString();
                }
                if (txtProductCode.Text.ToString().Trim() != string.Empty)
                {
                    produceCode = txtProductCode.Text.ToString();
                }
                if (txtRefNo.Text.ToString().Trim() != string.Empty)
                {
                    referenceNo = txtRefNo.Text.ToString();
                }
                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), produceCode, referenceNo);
                dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), produceCode, referenceNo);

                if (AllItems.Any())
                {
                    dgvStockDetailsReport.Rows.Clear();
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();
                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();
                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }
                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //
                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = openingQuantity; // itemParams.Item3;
                                            openingQuantityOut = 0; // itemParams.Item4;
                                            //qtyBal += openingQuantity;
                                            qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            dgvStockDetailsReport.Rows.Add();
                                            //dgvStockDetailsReport.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                                            dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                            totalQtyPerStore += openingQuantity;
                                            i++;
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }
                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());
                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            if(!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}
                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }
                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }
                                            refNo = dr["refNo"].ToString();
                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                            voucherType = dr["typeOfVoucher"].ToString();
                                            if (voucherType == "Stock Journal")
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = {0} ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }
                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                                                    || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }

                                                else if (outwardQty2 > 0 && voucherType == "Sales Order")
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType == "Sales Return" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Rejection In" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Delivery Note")
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                            }

                                            // ------------------------------------------------------ //
                                            dgvStockDetailsReport.Rows.Add();
                                            dgvStockDetailsReport.Rows[i].Cells["productId"].Value = dr["productId"];
                                            dgvStockDetailsReport.Rows[i].Cells["productName"].Value = dr["productName"];
                                            dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = dr["godownName"];
                                            dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = Convert.ToDecimal(dr["inwardQty"]).ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = isAgainstVoucher == true ? againstVoucherType : voucherType;
                                            isAgainstVoucher = false;
                                            dgvStockDetailsReport.Rows[i].Cells["rate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");

                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = (stockValue / qtyBal).ToString("N2");
                                            }
                                            else
                                            {
                                                dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = computedAverageRate.ToString("N2");
                                            }
                                            dgvStockDetailsReport.Rows[i].Cells["date"].Value = Convert.ToDateTime(dr["date"]).ToShortDateString();
                                            dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                                            dgvStockDetailsReport.Rows[i].Cells["Batch"].Value = dr["Batch"].ToString();
                                            dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = dr["productCode"];
                                            dgvStockDetailsReport.Rows[i].Cells["refNo"].Value = refNo;
                                            stockValuePerItemPerStore = Convert.ToDecimal(dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        dgvStockDetailsReport.Rows.Add();
                                        dgvStockDetailsReport.Rows[i].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                                        dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                        dgvStockDetailsReport.Rows[i].Cells["productId"].Value = "";
                                        dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                        dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = storeName;
                                        dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = (inwardQtyPerStore + openingQuantityIn).ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = ("-") + (outwardQtyPerStore + openingQuantityOut).ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Sub total: ";
                                        dgvStockDetailsReport.Rows[i].Cells["date"].Value = "";
                                        dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = (inwardQtyPerStore - outwardQtyPerStore + openingQuantity).ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = stockValuePerItemPerStore.ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["Batch"].Value = "";
                                        dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                        dgvStockDetailsReport.Rows[i].Cells["refNo"].Value = "";
                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += inwardQtyPerStore - outwardQtyPerStore + openingQuantity;
                                        i++;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }
                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the opening stock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();

                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();
                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = openingQuantity; // itemParams.Item3;
                                        openingQuantityOut = 0; // itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }

                                        dgvStockDetailsReport.Rows.Add();
                                        //dgvStockDetailsReport.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                                        dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                        dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                        dgvStockDetailsReport.Rows[i].Cells["productName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("productName")).FirstOrDefault()).ToString();
                                        dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString();
                                        dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                        dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                        dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                        dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                dgvStockDetailsReport.Rows.Add();
                dgvStockDetailsReport.Rows.Add();
                dgvStockDetailsReport.Rows[i + 1].Cells["stockValue"].Value = "==========";
                dgvStockDetailsReport.Rows.Add();
                dgvStockDetailsReport.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvStockDetailsReport.Rows[i + 2].Cells["qtyBalalance"].Value = totalQtyPerStore.ToString("N2");
                if (totalQtyPerStore != 0)
                {
                    dgvStockDetailsReport.Rows[i + 2].Cells["dgvtxtAverageCost"].Value = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                dgvStockDetailsReport.Rows[i + 2].Cells["stockValue"].Value = totalStockValue.ToString("N2");
                dgvStockDetailsReport.Rows[i + 2].Cells["voucherTypeName"].Value = "Total Stock Value:";
                dgvStockDetailsReport.Rows.Add();
                dgvStockDetailsReport.Rows[i + 3].Cells["stockValue"].Value = "==========";
            }

            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
        }
        private void dtpStockFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpStockFromDate.Value;
                this.txtFromDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD1:" + ex.Message;
            }
        }
        private void ddtpStockToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.ddtpStockToDate.Value;
                this.txtTodate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD9:" + ex.Message;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                DateValidation ObjValidation = new DateValidation();
                ObjValidation.DateValidationFunction(txtTodate);
                if (Convert.ToDateTime(txtTodate.Text) < Convert.ToDateTime(txtFromDate.Text))
                {
                    MessageBox.Show("Todate should be greater than Fromdate", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                    GridFill();
                }
                else
                {

                    GridFill();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD10:" + ex.Message;
            }
        }
        private void btnclear_Click(object sender, EventArgs e)
        {
            try
            {
                clear();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD11:" + ex.Message;
            }
        }
        public void clear()
        {
            isFormLoad = true;

            dtpStockFromDate.MinDate = PublicVariables._dtFromDate;
            dtpStockFromDate.MaxDate = PublicVariables._dtToDate;
            dtpStockFromDate.Value = PublicVariables._dtFromDate;

            ddtpStockToDate.MinDate = PublicVariables._dtFromDate;
            ddtpStockToDate.MaxDate = PublicVariables._dtToDate;
            ddtpStockToDate.Value = PublicVariables._dtToDate;
            dtpStockFromDate.Text = dtpStockFromDate.Value.ToString("dd-MMM-yyyy");
            ddtpStockToDate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
            txtTodate.Text = ddtpStockToDate.Value.ToString("dd-MMM-yyyy");
            txtProductCode.Text = string.Empty;
            isFormLoad = false;
            ProductComboFill();
            GodownComboFill();
            GridFill();
        }
        private void btnExport_Click(object sender, EventArgs e)
        {

            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvStockDetailsReport, "STOCK DETAILS AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD12:" + ex.Message;
            }
        }
        public void ProductComboFill()
        {
            try
            {

                spproduct = new ProductSP();
                DataTable dtbl = new DataTable();
                dtbl = spproduct.ProductViewAll();
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    AllItems.Add(dtbl.Rows[i]["productCode"].ToString());
                }
                cmbProduct.DataSource = dtbl;
                cmbProduct.DisplayMember = "productName";
                cmbProduct.ValueMember = "productId";
                DataRow dr = dtbl.NewRow();
                dr["productId"] = 0;
                dr["productName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "RPL4:" + ex.Message;
            }
        }

    }
}
