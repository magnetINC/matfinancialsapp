﻿
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace MATFinancials
{
    public partial class frmAccountLedgerReport : Form
    {
        
        
        #region PUBLIC VARIABLES
        /// <summary>
        /// Public variable declaration part
        /// </summary>
        DateTime dtFromDate, dtTodate;
        decimal decAccountGroupId, decLedgerId;
        #endregion

        #region FUNCTIOS
        /// <summary>
        /// Create an Instance of a frmAccountLedgerReport class
        /// </summary>
        public frmAccountLedgerReport()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Function to do on form load
        /// </summary>
        public void FormLoad()
        {
            try
            {
                txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                txtToDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                dtFromDate = Convert.ToDateTime(txtFromDate.Text);
                dtTodate = Convert.ToDateTime(txtToDate.Text);
                txtFromDate.Focus();
                AccountGroupComboFill();
                decAccountGroupId = Convert.ToDecimal(cmbAccountGroup.SelectedValue.ToString());
                decLedgerId = Convert.ToDecimal(cmbAccountLedger.SelectedValue.ToString());
                FinancialYearDate();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP1:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to fill the accountgroup combo box
        /// </summary>
        public void AccountGroupComboFill()
        {
            try
            {
                AccountGroupSP spAccountGroup = new AccountGroupSP();
                DataTable dtblAccountGroup = new DataTable();
                dtblAccountGroup = spAccountGroup.AccountGroupViewAllComboFillForAccountLedger();
                DataRow dr = dtblAccountGroup.NewRow();
                dr["accountGroupName"] = "All";
                dr["accountGroupId"] = -1;
                dtblAccountGroup.Rows.InsertAt(dr, 0);
                cmbAccountGroup.DataSource = dtblAccountGroup;
                cmbAccountGroup.ValueMember = "accountGroupId";
                cmbAccountGroup.DisplayMember = "accountGroupName";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP2:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to fill the accountledger combbox 
        /// </summary>
        public void AccountLedgerComboFill()
        {
            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                DataTable dtblAccountLedger = new DataTable();
                dtblAccountLedger = spAccountLedger.AccountLedgerViewByAccountGroup(Convert.ToDecimal(cmbAccountGroup.SelectedValue.ToString()));
                DataRow dr = dtblAccountLedger.NewRow();
                dr["ledgerName"] = "All";
                dr["ledgerId"] = 0;
                dtblAccountLedger.Rows.InsertAt(dr, 0);
                cmbAccountLedger.DataSource = dtblAccountLedger;
                cmbAccountLedger.ValueMember = "ledgerId";
                cmbAccountLedger.DisplayMember = "ledgerName";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP3:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to fill the grid
        /// </summary>
        public void AccountLedgerReportFill()
        {
            decimal decTotalClosing = 0;
            AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
            try
            {
                this.dgvAccountLedgerReport.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                this.dgvAccountLedgerReport.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();
                GeneralQueries methodForRetainedEarning = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();
                decimal retainedEarning = methodForRetainedEarning.retainedEarnings(PublicVariables._dtToDate);
                retainedEarning = Math.Round(retainedEarning, 2);
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                conn.AddParameter("@fromDate", Convert.ToDateTime(txtFromDate.Text));
                conn.AddParameter("@toDate", Convert.ToDateTime(txtToDate.Text));
                conn.AddParameter("@accountGroupId", Convert.ToDecimal(cmbAccountGroup.SelectedValue.ToString()));
                conn.AddParameter("@ledgerId", Convert.ToDecimal(cmbAccountLedger.SelectedValue.ToString()));
                ds = conn.getDataSet("AccountLedgerReportFill");

                //dt = SpAccountLedger.AccountLedgerReportFill(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), Convert.ToDecimal(cmbAccountGroup.SelectedValue.ToString()), Convert.ToDecimal(cmbAccountLedger.SelectedValue.ToString())); // old does not show opening balances
                //var profitCarryOver = dt.AsEnumerable().Where(i => i.Field<string>("ledgerName") == "retained earning").Select(i => i).ToList();
                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[1]; 
                    foreach(DataRow row in dt.Rows)
                    {
                        decimal debitBalance = 0;
                        decimal creditBalance = 0;
                        string balanceToUse = "";
                        var openingBalances = (from i in ds.Tables[0].AsEnumerable()
                                               where i.Field<decimal>("ledgerId") == Convert.ToDecimal(row["ledgerId"])
                                               select new
                                               {
                                                   debit = i.Field<decimal>("OpeningDebit"),
                                                   credit = i.Field<decimal>("OpeningCredit")
                                               }).ToList();
                        foreach(var balance in openingBalances)
                        {
                            debitBalance = balance.debit;
                            creditBalance = balance.credit;
                        }
                        if(debitBalance > 0)
                        {
                            balanceToUse = Convert.ToString(debitBalance) + "Dr";
                        }
                        else if(creditBalance > 0)
                        {
                            balanceToUse = Convert.ToString(creditBalance) + "Cr";
                        }
                        row["opening"] = balanceToUse;
                        if (balanceToUse.Contains("Cr"))
                        {
                            if(row["Closing"].ToString().EndsWith("Cr"))
                            {
                                row["Closing"] = row["Closing"].ToString().Substring(0, row["Closing"].ToString().Length - 2);
                                row["Closing"] = Convert.ToDecimal(row["Closing"]) + creditBalance;
                                row["Closing"] = Convert.ToDecimal(row["Closing"]).ToString("N2") + " Cr";
                            }
                            if (row["Closing"].ToString().EndsWith("Dr"))
                            {
                                row["Closing"] = row["Closing"].ToString().Substring(0, row["Closing"].ToString().Length - 2);
                                row["Closing"] = Convert.ToDecimal(row["Closing"]) - creditBalance;
                                row["Closing"] = Convert.ToDecimal(row["Closing"]).ToString("N2") + " Dr";
                            }
                        }
                        if (balanceToUse.Contains("Dr"))
                        {
                            if (row["Closing"].ToString().EndsWith("Dr"))
                            {
                                row["Closing"] = row["Closing"].ToString().Substring(0, row["Closing"].ToString().Length - 2);
                                row["Closing"] = Convert.ToDecimal(row["Closing"]) + debitBalance;
                                row["Closing"] = Convert.ToDecimal(row["Closing"]).ToString("N2") + " Dr";
                            }
                            if (row["Closing"].ToString().EndsWith("Cr"))
                            {
                                row["Closing"] = row["Closing"].ToString().Substring(0, row["Closing"].ToString().Length - 2);
                                row["Closing"] = Convert.ToDecimal(row["Closing"]) - debitBalance;
                                row["Closing"] = Convert.ToDecimal(row["Closing"]).ToString("N2") + " Cr";
                            }
                        }
                    }
                    List<decimal> ledgersWithTransactions = dt.AsEnumerable().Select(i => i.Field<decimal>("ledgerId")).ToList();
                    List<decimal> ledgersWithoutTransactions = ds.Tables[0].AsEnumerable().Where(j => j.Field<decimal>("OpeningCredit") != j.Field<decimal>("OpeningDebit"))
                        .Select(j => j.Field<decimal>("ledgerId")).ToList()
                        .Except(ledgersWithTransactions).ToList();
                    DataTable dt2 = new DataTable();
                    if (ledgersWithoutTransactions.Any())
                    {
                        var openingBalanceDetails = (from k in ds.Tables[0].AsEnumerable()
                                         where ledgersWithoutTransactions.Contains(k.Field<decimal>("ledgerId")) select k).ToList();
                        dt2 = openingBalanceDetails.CopyToDataTable();
                        if (Convert.ToDecimal(cmbAccountLedger.SelectedValue) != 0)
                        {
                            if (openingBalanceDetails.AsEnumerable().Where(i => i.Field<decimal>("ledgerId") == Convert.ToDecimal(cmbAccountLedger.SelectedValue)).Any())
                            {
                                dt2 = openingBalanceDetails.AsEnumerable().Where(i => i.Field<decimal>("ledgerId") == Convert.ToDecimal(cmbAccountLedger.SelectedValue)).CopyToDataTable();
                            }
                            else
                            {
                                //goto emptyGrid;
                            }
                        }
                            foreach (DataRow rowItem in dt2.Rows)
                        {
                            DataRow row = dt.NewRow();
                            row["slNO"] = "1";
                            row["ledgerId"] = rowItem["ledgerId"];
                            row["ledgerName"] = rowItem["ledgerName"];
                            row["Opening"] = Convert.ToDecimal(rowItem["OpeningCredit"]) != 0 ? rowItem["OpeningCredit"] : rowItem["OpeningDebit"];
                            row["op"] = "0.00";
                            row["Credit"] = "0.00";
                            row["Debit"] = "0.00";
                            row["Closing"] = Convert.ToDecimal(rowItem["OpeningCredit"]) != 0 ? rowItem["OpeningCredit"]+"Cr" : rowItem["OpeningDebit"]+"Dr";
                            row["Closing1"] = Convert.ToDecimal(row["Opening"]).ToString("N2");
                            dt.Rows.InsertAt(row, 0); 
                        }
                    }
                }
                var profitCarryOver = dt.AsEnumerable().Where(i => string.Compare(i.Field<string>("ledgerName"), "retained earning", true) == 0).Select(i => i).ToList();

                if(profitCarryOver.Any())
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["ledgerName"].ToString().Trim().ToLower() == "retained earning".ToLower())
                        {
                            if (retainedEarning > 0)
                            {
                                row["credit"] = retainedEarning;
                                //row["creditDifference"] = retainedEarning;
                            }
                            else
                            {
                                retainedEarning = retainedEarning * -1;
                                row["debit"] = retainedEarning;
                                //row["debitDifference"] = retainedEarning;
                            }
                            row["closing"] = (Convert.ToDecimal(row["credit"]) - Convert.ToDecimal(row["debit"])).ToString("N2") + "Cr"; // retainedEarning;
                            row["closing1"] = (Convert.ToDecimal(row["debit"]) - Convert.ToDecimal(row["credit"])).ToString("N2"); // retainedEarning;
                        }
                    }
                }
                else
                {
                    DataRow row = dt.NewRow();
                    row["slNO"] = "1";
                    row["ledgerId"] = "2";
                    row["ledgerName"] = "Retained Earning";
                    row["Opening"] = "0.00Dr";
                    row["op"] = "0.00";
                    row["Credit"] = retainedEarning > 0 ? retainedEarning.ToString() : "0.00";
                    row["Debit"] = retainedEarning > 0 ? "0.00" : retainedEarning.ToString();
                    row["Closing"] = retainedEarning > 0 ? retainedEarning + "Cr" : retainedEarning + "Dr"; 
                    row["Closing1"] = retainedEarning.ToString("N2");
                    dt.Rows.InsertAt(row, 0);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["slNO"] = (i + 1).ToString();
                }

                dgvAccountLedgerReport.DataSource = dt;
                if (dgvAccountLedgerReport.RowCount > 0)
                {
                    for (int i = 0; i < dgvAccountLedgerReport.RowCount; i++)
                    {
                        decTotalClosing = decTotalClosing + Convert.ToDecimal(dgvAccountLedgerReport.Rows[i].Cells["dgvtxtClosing1"].Value.ToString());
                    }
                }
                if (decTotalClosing < 0)
                {
                    decTotalClosing = -1 * decTotalClosing;
                    lblTotalAmount.Text = decTotalClosing.ToString() + "Cr";
                }
                else
                {
                    lblTotalAmount.Text = decTotalClosing.ToString() + "Dr";
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP4:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to print the report
        /// </summary>
        public void Print()
        {
            AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
            try
            {
                DataSet dsAccountLedgerReport = SpAccountLedger.AccountLedgerReportPrinting(PublicVariables._decCurrentCompanyId, dtFromDate, dtTodate, decAccountGroupId, decLedgerId);
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.AccountLedgerReportPrinting(dsAccountLedgerReport);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP5:" + ex.Message;
            }
        }
        /// <summary>
        /// Function to set the financial year date
        /// </summary>
        public void FinancialYearDate()
        {
            try
            {
                //-----For FromDate----//
                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                CompanyInfo infoComapany = new CompanyInfo();
                CompanySP spCompany = new CompanySP();
                infoComapany = spCompany.CompanyView(1);
                DateTime dtFromDate = infoComapany.CurrentDate;
                dtpFromDate.Value = dtFromDate;
                dtpFromDate.Text = dtFromDate.ToString("dd-MMM-yyyy");
                dtpFromDate.Value = Convert.ToDateTime(txtFromDate.Text);
                txtFromDate.Focus();
                txtFromDate.SelectAll();
                //==============================//
                //-----For ToDate-----------------//
                dtpToDate.MinDate = PublicVariables._dtFromDate;
                dtpToDate.MaxDate = PublicVariables._dtToDate;
                infoComapany = spCompany.CompanyView(1);
                DateTime dtToDate = infoComapany.CurrentDate;
                dtpToDate.Value = dtToDate;
                dtpToDate.Text = dtToDate.ToString("dd-MMM-yyyy");
                dtpToDate.Value = Convert.ToDateTime(txtToDate.Text);
                txtToDate.Focus();
                txtToDate.SelectAll();
                //=====================//
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP6:" + ex.Message;
            }
        }
        #endregion
        
        #region EVENTS
        /// <summary>
        /// On form load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmAccountLedgerReport_Load(object sender, EventArgs e)
        {
            try
            {
                FormLoad();
                AccountLedgerReportFill();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP7:" + ex.Message;
            }
        }
        /// <summary>
        /// On selected value change of accountGroup combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAccountGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAccountGroup.SelectedValue != null)
                {
                    if (cmbAccountGroup.SelectedValue.ToString() != "System.Data.DataRowView" && cmbAccountGroup.Text != "System.Data.DataRowView")
                    {
                        AccountLedgerComboFill();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP8:" + ex.Message;
            }
        }
        /// <summary>
        /// On value change of dtpFromDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP9:" + ex.Message;
            }
        }
        /// <summary>
        /// On value change of dtpToDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpToDate.Value.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP10:" + ex.Message;
            }
        }
        /// <summary>
        /// On leave from txtFromDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation obj = new DateValidation();
                obj.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }
                //---for change date in Date time picker----//
                string strdate = txtFromDate.Text;
                dtpFromDate.Value = Convert.ToDateTime(strdate.ToString());
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP11:" + ex.Message;
            }
        }
        /// <summary>
        /// On leave from txtToDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation obj = new DateValidation();
                obj.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtCurrentDate.ToString("dd-MMM-yyyy");
                }
                //---for change date in Date time picker----//
                string strdate = txtToDate.Text;
                dtpToDate.Value = Convert.ToDateTime(strdate.ToString());
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP12:" + ex.Message;
            }
        }
        /// <summary>
        /// On leave from txtToDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dtFromDate = Convert.ToDateTime(txtFromDate.Text);//Used When double click on grid
                dtTodate = Convert.ToDateTime(txtToDate.Text);//Used When double click on grid
                decAccountGroupId= Convert.ToDecimal(cmbAccountGroup.SelectedValue.ToString());
                decLedgerId = Convert.ToDecimal(cmbAccountLedger.SelectedValue.ToString());
                AccountLedgerReportFill();
                txtFromDate.Focus();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP13:" + ex.Message;
            }
        }
        /// <summary>
        /// On reset button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                frmAccountLedgerReport_Load(sender, e);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP14:" + ex.Message;
            }
        }
        /// <summary>
        /// When doubleclicking on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvAccountLedgerReport_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    if (dgvAccountLedgerReport.CurrentRow != null)
                    {
                        decimal decLedgerId = Convert.ToDecimal(dgvAccountLedgerReport.CurrentRow.Cells["dgvtxLedgerId"].Value.ToString());
                        frmLedgerDetails frmLedgerDetailsObj = new frmLedgerDetails();
                        frmLedgerDetailsObj.MdiParent = formMDI.MDIObj;
                        frmLedgerDetailsObj.CallFromAccountLedgerReport(this, decLedgerId, dtFromDate, dtTodate);
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP15:" + ex.Message;
            }
        }
        /// <summary>
        /// On print button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvAccountLedgerReport.Rows.Count > 0)
                {
                    Print();
                }
                else
                {
                    Messages.InformationMessage("No data found");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP16:" + ex.Message;
            }
        }

        /// <summary>
        /// On 'Export' button click to export the report to Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvAccountLedgerReport, "Account Ledger Report", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP17:" + ex.Message;
            }
        }
       
        #endregion
        
        #region NAVIGATION
        /// <summary>
        /// Escape for form close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmAccountLedgerReport_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (PublicVariables.isMessageClose)
                    {
                        Messages.CloseMessage(this);
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP18:" + ex.Message;
            }
        }
        /// <summary>
        /// Enterkey navigation of txtFromDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFromDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtToDate.Focus();
                    txtToDate.SelectionStart = txtToDate.Text.Length;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP19:" + ex.Message;
            }
        }
        /// <summary>
        /// Enterkey and backspace navigation of txtToDate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtToDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cmbAccountGroup.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    if (txtToDate.SelectionStart == 0)
                    {
                        txtFromDate.Focus();
                        txtFromDate.SelectionStart = txtFromDate.Text.Length;
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP20:" + ex.Message;
            }
        }
        /// <summary>
        /// Enterkey and backspace navigation of cmbAccountGroup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAccountGroup_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cmbAccountLedger.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    txtToDate.Focus();
                    txtToDate.SelectionLength = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP21:" + ex.Message;
            }
        }
        /// <summary>
        /// Enterkey and backspace navigation of cmbAccountLedger
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAccountLedger_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSearch.Focus();
                }
                if (e.KeyCode == Keys.Back)
                {
                    cmbAccountGroup.Focus();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ALREP22:" + ex.Message;
            }
        }
        #endregion
        
    }
}
