﻿namespace MATFinancials.Reports
{
    partial class frmAgeingReportForCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAgeingReportForCustomer));
            this.btnExport = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.dtpAgeingDate = new System.Windows.Forms.DateTimePicker();
            this.lblTotFour = new System.Windows.Forms.Label();
            this.lblTotThree = new System.Windows.Forms.Label();
            this.lblTotTwo = new System.Windows.Forms.Label();
            this.lblTotOne = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbtnLedgerWise = new System.Windows.Forms.RadioButton();
            this.rbtnVoucher = new System.Windows.Forms.RadioButton();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtnReceivable = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnPayable = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbLedger = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SlNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VoucherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.ForeColor = System.Drawing.Color.Black;
            this.btnExport.Location = new System.Drawing.Point(861, 531);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(85, 26);
            this.btnExport.TabIndex = 4575;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(770, 530);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(85, 27);
            this.btnPrint.TabIndex = 4574;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // dtpAgeingDate
            // 
            this.dtpAgeingDate.Location = new System.Drawing.Point(465, 7);
            this.dtpAgeingDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.dtpAgeingDate.Name = "dtpAgeingDate";
            this.dtpAgeingDate.Size = new System.Drawing.Size(22, 20);
            this.dtpAgeingDate.TabIndex = 4583;
            this.dtpAgeingDate.CloseUp += new System.EventHandler(this.dtpAgeingDate_CloseUp);
            this.dtpAgeingDate.Leave += new System.EventHandler(this.dtpAgeingDate_Leave);
            // 
            // lblTotFour
            // 
            this.lblTotFour.AutoSize = true;
            this.lblTotFour.ForeColor = System.Drawing.Color.Brown;
            this.lblTotFour.Location = new System.Drawing.Point(851, 514);
            this.lblTotFour.Name = "lblTotFour";
            this.lblTotFour.Size = new System.Drawing.Size(28, 13);
            this.lblTotFour.TabIndex = 4582;
            this.lblTotFour.Text = "0.00";
            // 
            // lblTotThree
            // 
            this.lblTotThree.AutoSize = true;
            this.lblTotThree.ForeColor = System.Drawing.Color.Brown;
            this.lblTotThree.Location = new System.Drawing.Point(628, 514);
            this.lblTotThree.Name = "lblTotThree";
            this.lblTotThree.Size = new System.Drawing.Size(28, 13);
            this.lblTotThree.TabIndex = 4581;
            this.lblTotThree.Text = "0.00";
            // 
            // lblTotTwo
            // 
            this.lblTotTwo.AutoSize = true;
            this.lblTotTwo.ForeColor = System.Drawing.Color.Brown;
            this.lblTotTwo.Location = new System.Drawing.Point(419, 514);
            this.lblTotTwo.Name = "lblTotTwo";
            this.lblTotTwo.Size = new System.Drawing.Size(28, 13);
            this.lblTotTwo.TabIndex = 4580;
            this.lblTotTwo.Text = "0.00";
            // 
            // lblTotOne
            // 
            this.lblTotOne.AutoSize = true;
            this.lblTotOne.ForeColor = System.Drawing.Color.Brown;
            this.lblTotOne.Location = new System.Drawing.Point(212, 514);
            this.lblTotOne.Name = "lblTotOne";
            this.lblTotOne.Size = new System.Drawing.Size(28, 13);
            this.lblTotOne.TabIndex = 4579;
            this.lblTotOne.Text = "0.00";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(10, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(80, 1);
            this.groupBox4.TabIndex = 4569;
            this.groupBox4.TabStop = false;
            // 
            // rbtnLedgerWise
            // 
            this.rbtnLedgerWise.AutoSize = true;
            this.rbtnLedgerWise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnLedgerWise.ForeColor = System.Drawing.Color.Black;
            this.rbtnLedgerWise.Location = new System.Drawing.Point(115, 19);
            this.rbtnLedgerWise.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.rbtnLedgerWise.Name = "rbtnLedgerWise";
            this.rbtnLedgerWise.Size = new System.Drawing.Size(85, 17);
            this.rbtnLedgerWise.TabIndex = 1;
            this.rbtnLedgerWise.TabStop = true;
            this.rbtnLedgerWise.Text = "Ledger Wise";
            this.rbtnLedgerWise.UseVisualStyleBackColor = true;
            // 
            // rbtnVoucher
            // 
            this.rbtnVoucher.AutoSize = true;
            this.rbtnVoucher.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnVoucher.ForeColor = System.Drawing.Color.Black;
            this.rbtnVoucher.Location = new System.Drawing.Point(7, 20);
            this.rbtnVoucher.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.rbtnVoucher.Name = "rbtnVoucher";
            this.rbtnVoucher.Size = new System.Drawing.Size(92, 17);
            this.rbtnVoucher.TabIndex = 0;
            this.rbtnVoucher.TabStop = true;
            this.rbtnVoucher.Text = "Voucher Wise";
            this.rbtnVoucher.UseVisualStyleBackColor = true;
            // 
            // txtToDate
            // 
            this.txtToDate.Location = new System.Drawing.Point(287, 7);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.Size = new System.Drawing.Size(181, 20);
            this.txtToDate.TabIndex = 4568;
            this.txtToDate.Leave += new System.EventHandler(this.txtToDate_Leave);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.rbtnLedgerWise);
            this.groupBox2.Controls.Add(this.rbtnVoucher);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox2.Location = new System.Drawing.Point(613, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 44);
            this.groupBox2.TabIndex = 4571;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Details";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(10, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(80, 1);
            this.groupBox3.TabIndex = 4568;
            this.groupBox3.TabStop = false;
            // 
            // rbtnReceivable
            // 
            this.rbtnReceivable.AutoSize = true;
            this.rbtnReceivable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnReceivable.ForeColor = System.Drawing.Color.Black;
            this.rbtnReceivable.Location = new System.Drawing.Point(11, 19);
            this.rbtnReceivable.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.rbtnReceivable.Name = "rbtnReceivable";
            this.rbtnReceivable.Size = new System.Drawing.Size(79, 17);
            this.rbtnReceivable.TabIndex = 1;
            this.rbtnReceivable.Text = "Receivable";
            this.rbtnReceivable.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.rbtnReceivable);
            this.groupBox1.Controls.Add(this.rbtnPayable);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox1.Location = new System.Drawing.Point(287, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 44);
            this.groupBox1.TabIndex = 4570;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Details";
            // 
            // rbtnPayable
            // 
            this.rbtnPayable.AutoSize = true;
            this.rbtnPayable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPayable.ForeColor = System.Drawing.Color.Black;
            this.rbtnPayable.Location = new System.Drawing.Point(165, 19);
            this.rbtnPayable.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.rbtnPayable.Name = "rbtnPayable";
            this.rbtnPayable.Size = new System.Drawing.Size(63, 17);
            this.rbtnPayable.TabIndex = 0;
            this.rbtnPayable.Text = "Payable";
            this.rbtnPayable.UseVisualStyleBackColor = true;
            this.rbtnPayable.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DimGray;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.Maroon;
            this.btnSearch.Location = new System.Drawing.Point(769, 103);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(85, 22);
            this.btnSearch.TabIndex = 4572;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.DimGray;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.Maroon;
            this.btnReset.Location = new System.Drawing.Point(860, 103);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(85, 22);
            this.btnReset.TabIndex = 4573;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // dgvReport
            // 
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AllowUserToResizeRows = false;
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvReport.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvReport.ColumnHeadersHeight = 35;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SlNo,
            this.VoucherType,
            this.VoucherNo});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReport.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvReport.EnableHeadersVisualStyles = false;
            this.dgvReport.GridColor = System.Drawing.Color.DimGray;
            this.dgvReport.Location = new System.Drawing.Point(184, 127);
            this.dgvReport.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.RowHeadersVisible = false;
            this.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReport.Size = new System.Drawing.Size(761, 384);
            this.dgvReport.TabIndex = 4578;
            this.dgvReport.TabStop = false;
            this.dgvReport.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport_CellDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(184, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 4577;
            this.label2.Text = "Account Ledger";
            // 
            // cmbLedger
            // 
            this.cmbLedger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLedger.FormattingEnabled = true;
            this.cmbLedger.Location = new System.Drawing.Point(287, 31);
            this.cmbLedger.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.cmbLedger.Name = "cmbLedger";
            this.cmbLedger.Size = new System.Drawing.Size(200, 21);
            this.cmbLedger.TabIndex = 4569;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(184, 11);
            this.label8.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 4576;
            this.label8.Text = "Ageing Date";
            // 
            // SlNo
            // 
            this.SlNo.DataPropertyName = "Sl No";
            this.SlNo.HeaderText = "S|No";
            this.SlNo.Name = "SlNo";
            this.SlNo.ReadOnly = true;
            // 
            // VoucherType
            // 
            this.VoucherType.DataPropertyName = "VoucherType";
            this.VoucherType.HeaderText = "Form Type";
            this.VoucherType.Name = "VoucherType";
            this.VoucherType.ReadOnly = true;
            // 
            // VoucherNo
            // 
            this.VoucherNo.DataPropertyName = "VoucherNo";
            this.VoucherNo.HeaderText = "Form No";
            this.VoucherNo.Name = "VoucherNo";
            this.VoucherNo.ReadOnly = true;
            // 
            // frmAgeingReportForCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 561);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.dtpAgeingDate);
            this.Controls.Add(this.lblTotFour);
            this.Controls.Add(this.lblTotThree);
            this.Controls.Add(this.lblTotTwo);
            this.Controls.Add(this.lblTotOne);
            this.Controls.Add(this.txtToDate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.dgvReport);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbLedger);
            this.Controls.Add(this.label8);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAgeingReportForCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ageing Report For Customer";
            this.Load += new System.EventHandler(this.frmAgeingReportForCustomer_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DateTimePicker dtpAgeingDate;
        private System.Windows.Forms.Label lblTotFour;
        private System.Windows.Forms.Label lblTotThree;
        private System.Windows.Forms.Label lblTotTwo;
        private System.Windows.Forms.Label lblTotOne;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbtnLedgerWise;
        private System.Windows.Forms.RadioButton rbtnVoucher;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtnReceivable;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnPayable;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbLedger;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn SlNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherType;
        private System.Windows.Forms.DataGridViewTextBoxColumn VoucherNo;
    }
}