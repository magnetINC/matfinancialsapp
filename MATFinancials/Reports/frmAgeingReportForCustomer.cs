﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Reports
{
    public partial class frmAgeingReportForCustomer : Form
    {
        #region Public variables
        /// <summary>
        /// Public variable declaration part
        /// </summary>
        public string strVoucherType;
        public string strVoucherNo;
        public decimal decMasterId;
        public decimal decledgerId;
        public decimal decVoucherTypeId;
        int inCurrenRowIndex = 0;
        string fillby = string.Empty;
        string ledgerId = string.Empty;
        bool isFormLoad = false;
        #endregion
        #region Functions
        /// <summary>
        /// Create an Instance of a frmAgeingReport class
        /// </summary>
        public frmAgeingReportForCustomer()
        {
            InitializeComponent();
        }

        public void GridColumn()
        {
            try
            {
                if (rbtnLedgerWise.Checked)
                {
                    dgvReport.Columns["ledgerId"].Visible = false;
                    dgvReport.Columns["masterId"].Visible = false;
                    dgvReport.Columns["voucherTypeId"].Visible = false;
                    dgvReport.Columns["typeOfVoucher"].Visible = false;
                }
                else if (rbtnVoucher.Checked)
                {
                    dgvReport.Columns["ledgerId"].Visible = false;
                    dgvReport.Columns["masterId"].Visible = false;
                    dgvReport.Columns["voucherTypeId"].Visible = false;
                    dgvReport.Columns["typeOfVoucher"].Visible = false;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR1:" + ex.Message;
            }
        }
        /// <summary>
        /// To fill combo with all ledgers to whome interest enabled is true
        /// </summary>
        public void FillInterestEnabledLedgersCombo()
        {
            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                //PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                //DataTable dtblCashOrParty = SpPartyBalance.AccountLedgerGetByDebtorAndCreditorWithBalance();
                DataTable dtblCashOrParty = spAccountLedger.AccountLedgerViewCustomerOnly();
                DataRow dr1 = dtblCashOrParty.NewRow();
                dr1["ledgerName"] = "All";
                dr1["ledgerId"] = 0;
                dtblCashOrParty.Rows.InsertAt(dr1, 0);
                cmbLedger.DataSource = dtblCashOrParty;
                cmbLedger.DisplayMember = "ledgerName";
                cmbLedger.ValueMember = "ledgerId";
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR2:" + ex.Message;
            }
        }
        public void FillGrid()
        {
            try
            {
                if (!isFormLoad)
                {
                    PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                    DataTable dtbl = new DataTable();
                    cmbLedger.Enabled = true;
                    string p = string.Empty;
                    if (cmbLedger.SelectedValue != null)
                        p = cmbLedger.SelectedValue.ToString().ToString();
                    if (cmbLedger.SelectedValue != null)
                    {
                        if (rbtnVoucher.Checked)
                        {
                            fillby = "Voucher";
                        }
                        else if (rbtnLedgerWise.Checked)
                        {
                            fillby = "Ledger";
                        }
                        if (rbtnReceivable.Checked && rbtnLedgerWise.Checked)
                            dtbl = SpPartyBalance.AgeingReportLedgerReceivable(DateTime.Parse(dtpAgeingDate.Text), Convert.ToDecimal(cmbLedger.SelectedValue.ToString()));
                        else if (rbtnReceivable.Checked && rbtnVoucher.Checked)
                            dtbl = SpPartyBalance.AgeingReportVoucherReceivable(DateTime.Parse(dtpAgeingDate.Text), Convert.ToDecimal(cmbLedger.SelectedValue.ToString()));
                        else if (rbtnPayable.Checked && rbtnLedgerWise.Checked)
                            dtbl = SpPartyBalance.AgeingReportLedgerPayable(DateTime.Parse(dtpAgeingDate.Text), Convert.ToDecimal(cmbLedger.SelectedValue.ToString()));
                        else if (rbtnPayable.Checked && rbtnVoucher.Checked)
                            dtbl = SpPartyBalance.AgeingReportVoucherPayable(DateTime.Parse(dtpAgeingDate.Text), Convert.ToDecimal(cmbLedger.SelectedValue.ToString()));
                    }
                    decimal dcTotOne = 0m;
                    decimal dcTotTwo = 0m;
                    decimal dcTotThree = 0m;
                    decimal dcTotFour = 0m;
                    if (dtbl.Rows.Count > 0)
                    {
                        dcTotOne = decimal.Parse(dtbl.Compute("Sum([1 to 30])", string.Empty).ToString());
                        dcTotTwo = decimal.Parse(dtbl.Compute("Sum([31 to 60])", string.Empty).ToString());
                        dcTotThree = decimal.Parse(dtbl.Compute("Sum([61 to 90])", string.Empty).ToString());
                        dcTotFour = decimal.Parse(dtbl.Compute("Sum([90 above])", string.Empty).ToString());
                    }
                    dtbl.Rows.Add();
                    dtbl.Rows[dtbl.Rows.Count - 1]["Account Ledger"] = "Total :";
                    dtbl.Rows[dtbl.Rows.Count - 1]["1 to 30"] = dcTotOne;
                    dtbl.Rows[dtbl.Rows.Count - 1]["31 to 60"] = dcTotTwo;
                    dtbl.Rows[dtbl.Rows.Count - 1]["61 to 90"] = dcTotThree;
                    dtbl.Rows[dtbl.Rows.Count - 1]["90 above"] = dcTotFour;
                    this.dgvReport.AlternatingRowsDefaultCellStyle.BackColor = PublicVariables.myBackColour();
                    this.dgvReport.AlternatingRowsDefaultCellStyle.ForeColor = PublicVariables.myForeColour();
                    dgvReport.DataSource = dtbl;
                    if (dgvReport.Columns.Count > 0)
                    {
                        if (rbtnLedgerWise.Checked == true)
                        {
                            dgvReport.Columns["ledgerId"].Visible = false;
                            dgvReport.Columns["masterId"].Visible = false;
                            dgvReport.Columns["voucherTypeId"].Visible = false;
                            dgvReport.Columns["VoucherType"].Visible = false;
                            dgvReport.Columns["VoucherNo"].Visible = false;
                            dgvReport.Columns["Date"].Visible = true;
                            dgvReport.Columns["Account Ledger"].Visible = true;
                        }
                        else
                        {
                            dgvReport.Columns["masterId"].Visible = false;
                            dgvReport.Columns["ledgerId"].Visible = false;
                            dgvReport.Columns["voucherTypeId"].Visible = false;
                            dgvReport.Columns["VoucherType"].Visible = true;
                            dgvReport.Columns["VoucherNo"].Visible = true;
                            dgvReport.Columns["Date"].Visible = true;
                            dgvReport.Columns["Account Ledger"].Visible = false;
                        }
                    }
                    dgvReport.Columns["1 to 30"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvReport.Columns["31 to 60"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvReport.Columns["61 to 90"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    dgvReport.Columns["90 above"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    lblTotOne.Text = "1 to 30: " + dcTotOne.ToString();
                    lblTotTwo.Text = "31 to 60: " + dcTotTwo.ToString();
                    lblTotThree.Text = "61 to 90: " + dcTotThree.ToString();
                    lblTotFour.Text = "90 above: " + dcTotFour.ToString();
                    ledgerId = string.Empty;
                    if (inCurrenRowIndex >= 0 && dgvReport.Rows.Count > 0 && inCurrenRowIndex < dgvReport.Rows.Count)
                    {
                        dgvReport.CurrentCell = dgvReport.Rows[inCurrenRowIndex].Cells["1 to 30"];
                        dgvReport.CurrentCell.Selected = true;
                    }
                    inCurrenRowIndex = 0;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR3:" + ex.Message;
            }
        }

        public DataTable dtblageing()
        {
            DataTable dtLocalC = new DataTable();
            try
            {
                dtLocalC.Columns.Add("Account Ledger");
                dtLocalC.Columns.Add("Date");
                dtLocalC.Columns.Add("1 to 30");
                dtLocalC.Columns.Add("31 to 60");
                dtLocalC.Columns.Add("61 to 90");
                dtLocalC.Columns.Add("90 above");
                dtLocalC.Columns.Add("SlNo");
                DataRow drLocal = null;
                foreach (DataGridViewRow dr in dgvReport.Rows)
                {
                    drLocal = dtLocalC.NewRow();
                    drLocal["Account Ledger"] = dr.Cells["Account Ledger"].Value;
                    drLocal["Date"] = dr.Cells["Date"].Value;
                    drLocal["1 to 30"] = dr.Cells["1 to 30"].Value;
                    drLocal["31 to 60"] = dr.Cells["31 to 60"].Value;
                    drLocal["61 to 90"] = dr.Cells["61 to 90"].Value;
                    drLocal["90 above"] = dr.Cells["90 above"].Value;
                    drLocal["SlNo"] = dr.Cells["Sl No"].Value;
                    dtLocalC.Rows.Add(drLocal);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR4:" + ex.Message;
            }
            return dtLocalC;
        }

        /// <summary>
        /// Creation of Ageingtable1
        /// </summary>
        /// <returns></returns>
        public DataTable dtblageing1()
        {
            DataTable dtLocalC = new DataTable();
            try
            {
                dtLocalC.Columns.Add("1 to 30");
                dtLocalC.Columns.Add("31 to 60");
                dtLocalC.Columns.Add("61 to 90");
                dtLocalC.Columns.Add("90 above");
                dtLocalC.Columns.Add("Date");
                dtLocalC.Columns.Add("Form No");
                dtLocalC.Columns.Add("Form Type");
                dtLocalC.Columns.Add("SlNo");
                DataRow drLocal = null;
                foreach (DataGridViewRow dr in dgvReport.Rows)
                {
                    drLocal = dtLocalC.NewRow();
                    drLocal["Voucher Type"] = dr.Cells["VoucherType"].Value;
                    drLocal["Voucher No"] = dr.Cells["VoucherNo"].Value;
                    drLocal["Date"] = dr.Cells["Date"].Value;
                    drLocal["1 to 30"] = dr.Cells["1 to 30"].Value;
                    drLocal["31 to 60"] = dr.Cells["31 to 60"].Value;
                    drLocal["61 to 90"] = dr.Cells["61 to 90"].Value;
                    drLocal["90 above"] = dr.Cells["90 above"].Value;
                    drLocal["SlNo"] = dr.Cells["Sl No"].Value;
                    dtLocalC.Rows.Add(drLocal);
                    strVoucherType = dr.Cells["VoucherType"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR5:" + ex.Message;
            }
            return dtLocalC;
        }

        /// <summary>
        /// Function to get dataset for printing the report
        /// </summary>
        /// <returns></returns>
        public DataSet getdataset()
        {
            DataSet dsFundFlow = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblFund = dtblageing();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.FundFlowReportPrintCompany(PublicVariables._decCurrentCompanyId);//(PublicVariables._decCurrentCompanyId);
                dsFundFlow.Tables.Add(dtblFund);
                dsFundFlow.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR6:" + ex.Message;
            }
            return dsFundFlow;
        }

        /// <summary>
        /// Function to get dataset for printing the report
        /// </summary>
        /// <returns></returns>
        public DataSet getdataset1()
        {
            DataSet dsFundFlow = new DataSet();
            try
            {
                FinancialStatementSP spfinancial = new FinancialStatementSP();
                DataTable dtblFund = dtblageing1();
                DataTable dtblCompany = new DataTable();
                dtblCompany = spfinancial.FundFlowReportPrintCompany(PublicVariables._decCurrentCompanyId);//(PublicVariables._decCurrentCompanyId);
                dsFundFlow.Tables.Add(dtblFund);
                dsFundFlow.Tables.Add(dtblCompany);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR7:" + ex.Message;
            }
            return dsFundFlow;
        }
        /// <summary>
        /// Function to print the report
        /// </summary>
        /// <param name="toDate"></param>
        public void Print(DateTime toDate)
        {
            try
            {
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataSet destBalance = getdataset();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.AgeingReportPrint(destBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR8:" + ex.Message;
            }
        }

        /// <summary>
        /// Function to print the report
        /// </summary>
        /// <param name="toDate"></param>
        public void Print1(DateTime toDate)
        {
            try
            {
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataSet destBalance = getdataset1();
                frmReport frmReport = new frmReport();
                frmReport.MdiParent = formMDI.MDIObj;
                frmReport.AgeingReportPrint1(destBalance);
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR9:" + ex.Message;
            }
        }

        public void InitialDateSettings()
        {
            try
            {
                dtpAgeingDate.Value = PublicVariables._dtToDate;
                dtpAgeingDate.MinDate = PublicVariables._dtFromDate;
                dtpAgeingDate.MaxDate = PublicVariables._dtToDate;
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR10:" + ex.Message;
            }
        }

        public void NotSortable()
        {
            try
            {
                dgvReport.Columns["1 to 30"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["31 to 60"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["61 to 90"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["90 above"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["Date"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["Voucher Type"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["Voucher No"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["Account Ledger"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns["Narration"].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReport.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR11:" + ex.Message;
            }
        }
        private void frmAgeingReportForCustomer_Load(object sender, EventArgs e)
        {
            try
            {
                InitialDateSettings();
                FillInterestEnabledLedgersCombo();
                //rbtnPayable.Checked = true;
                rbtnReceivable.Checked = true;
                rbtnVoucher.Checked = true;
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR12:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR13:" + ex.Message;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                isFormLoad = true;
                InitialDateSettings();
                rbtnReceivable.Checked = true;
                rbtnLedgerWise.Checked = true;
                isFormLoad = false;
                FillGrid();
                FillInterestEnabledLedgersCombo();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR14:" + ex.Message;
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvReport.Rows.Count - 1 <= 0)
                {
                    MessageBox.Show("No data found", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (rbtnLedgerWise.Checked)
                    {
                        Print(PublicVariables._dtToDate);
                    }
                    else if (rbtnVoucher.Checked)
                    {
                        Print1(PublicVariables._dtToDate);
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR16:" + ex.Message;
            }
        }

        private void dgvReport_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          
            decimal decVouchertypeId = 0;
            string strVoucherNo = string.Empty;
            try
            {
                if (dgvReport.CurrentRow.Index == e.RowIndex)
                {
                    if ((dgvReport.CurrentRow.Cells["voucherTypeId"].Value != null && dgvReport.CurrentRow.Cells["voucherTypeId"].Value.ToString() != string.Empty))
                    {
                        int inI = dgvReport.CurrentCell.RowIndex;
                        foreach (DataGridViewRow dgv in dgvReport.Rows)
                        {
                            if (dgv.Cells["VoucherNo"].Value != null && dgv.Cells["VoucherNo"].Value.ToString() != string.Empty &&
                                   dgv.Cells["voucherTypeId"].Value != null && dgv.Cells["voucherTypeId"].Value.ToString() != string.Empty)
                            {
                                strVoucherType = dgv.Cells["VoucherType"].Value.ToString();
                                decVouchertypeId = Convert.ToDecimal(dgv.Cells["voucherTypeId"].Value.ToString());
                                strVoucherNo = dgv.Cells["VoucherNo"].Value.ToString();
                            }
                            else
                            {
                                if (dgv.Cells["ledgerId"].Value.ToString() != string.Empty)
                                {
                                    decledgerId = decimal.Parse(dgv.Cells["ledgerId"].Value.ToString());
                                    strVoucherType = dgv.Cells["Account Ledger"].Value.ToString();
                                    frmLedgerDetails frmLedger = new frmLedgerDetails();
                                    frmLedger = Application.OpenForms["frmLedgerDetails"] as frmLedgerDetails;
                                    if (frmLedger == null)
                                    {
                                        frmLedger = new frmLedgerDetails();
                                        frmLedger.MdiParent = formMDI.MDIObj;
                                        frmLedger.callFromAgeingForCustomer(this, decledgerId); // precious
                                        this.Enabled = false;
                                    }
                                }
                            }
                            if (dgv.Index == inI)
                            {
                                break;
                            }
                        }
                        if (strVoucherType == "Payment Voucher")
                        {
                            PaymentMasterSP spPaymentMaster = new PaymentMasterSP();
                            decMasterId = spPaymentMaster.paymentMasterIdView(decVouchertypeId, strVoucherNo);
                            frmPaymentVoucher frmPaymentVoucher = new frmPaymentVoucher();
                            frmPaymentVoucher = Application.OpenForms["frmPaymentVoucher"] as frmPaymentVoucher;
                            if (frmPaymentVoucher == null)
                            {
                                frmPaymentVoucher = new frmPaymentVoucher();
                                frmPaymentVoucher.MdiParent = formMDI.MDIObj;
                                frmPaymentVoucher.callFromAgeingForCustomer(this, decMasterId); // precious
                                this.Enabled = false;
                            }
                        }
                        else if (strVoucherType == "Receipt Voucher")
                        {
                            ReceiptMasterSP spRecieptMaster = new ReceiptMasterSP();
                            decMasterId = spRecieptMaster.ReceiptMasterIdView(decVouchertypeId, strVoucherNo);
                            frmReceiptVoucher frmReceiptVoucher = new frmReceiptVoucher();
                            frmReceiptVoucher = Application.OpenForms["frmReceiptVoucher"] as frmReceiptVoucher;
                            if (frmReceiptVoucher == null)
                            {
                                frmReceiptVoucher = new frmReceiptVoucher();
                                frmReceiptVoucher.MdiParent = formMDI.MDIObj;
                                frmReceiptVoucher.callFromAgeingForCustomer(this, decMasterId); // Precious
                                this.Enabled = false;
                            }
                        }
                        else if (strVoucherType == "Journal Voucher")
                        {
                            JournalMasterSP spJournalMaster = new JournalMasterSP();
                            decMasterId = spJournalMaster.JournalMasterIdView(decVouchertypeId, strVoucherNo);
                            frmJournalVoucher frmJournalVoucher = new frmJournalVoucher();
                            frmJournalVoucher = Application.OpenForms["frmJournalVoucher"] as frmJournalVoucher;
                            if (frmJournalVoucher == null)
                            {
                                frmJournalVoucher = new frmJournalVoucher();
                                frmJournalVoucher.MdiParent = formMDI.MDIObj;
                                frmJournalVoucher.callFromAgeingCustomer(this, decMasterId);    // Precious
                                this.Enabled = false;
                            }
                        }
                        else if (strVoucherType == "PDC Receivable")
                        {
                            PDCReceivableMasterSP spPdcRecievabl = new PDCReceivableMasterSP();
                            decMasterId = spPdcRecievabl.PdcReceivableMasterIdView(decVouchertypeId, strVoucherNo);
                            frmPdcReceivable frmPdcReceivable = new frmPdcReceivable();
                            frmPdcReceivable = Application.OpenForms["frmPdcReceivable"] as frmPdcReceivable;
                            if (frmPdcReceivable == null)
                            {
                                frmPdcReceivable = new frmPdcReceivable();
                                frmPdcReceivable.MdiParent = formMDI.MDIObj;
                                frmPdcReceivable.callFromAgeingCustomer(this, decMasterId);     // Precious
                                this.Enabled = false;
                            }
                        }
                        else if (strVoucherType == "PDC Payable")
                        {
                            PDCPayableMasterSP spPdcPayable = new PDCPayableMasterSP();
                            decMasterId = spPdcPayable.PdcPayableMasterIdView(decVouchertypeId, strVoucherNo);
                            frmPdcPayable frmPdcPayable = new frmPdcPayable();
                            frmPdcPayable = Application.OpenForms["frmPdcPayable"] as frmPdcPayable;
                            if (frmPdcPayable == null)
                            {
                                frmPdcPayable = new frmPdcPayable();
                                frmPdcPayable.MdiParent = formMDI.MDIObj;
                                frmPdcPayable.callFromAgeingCustomer(this, decMasterId);    // Precious
                                this.Enabled = false;
                            }
                        }
                        else if (strVoucherType == "Sales Invoice")
                        {
                            SalesMasterSP spMaster = new SalesMasterSP();
                            decMasterId = spMaster.SalesMasterIdViewByvoucherNoAndVoucherType(decVouchertypeId, strVoucherNo);
                            SalesMasterSP spSalesMaster = new SalesMasterSP();
                            bool blPOS = spSalesMaster.DayBookSalesInvoiceOrPOS(decMasterId, decVouchertypeId);
                            frmSalesInvoice frmSalesInvoice = new frmSalesInvoice();
                            frmPOS frmPOS = new frmPOS();
                            if (blPOS == true)
                            {
                                frmPOS = Application.OpenForms["frmPOS"] as frmPOS;
                                if (frmPOS == null)
                                {
                                    frmPOS = new frmPOS();
                                    frmPOS.MdiParent = formMDI.MDIObj;
                                    frmPOS.callFromAgeingCustomer(this, decMasterId);       // Precious
                                    this.Enabled = false;
                                }
                            }
                            else
                            {
                                frmSalesInvoice = Application.OpenForms["frmSalesInvoice"] as frmSalesInvoice;
                                if (frmSalesInvoice == null) 
                                {
                                    frmSalesInvoice = new frmSalesInvoice();
                                    frmSalesInvoice.MdiParent = formMDI.MDIObj;
                                    frmSalesInvoice.callFromAgeingCustomer(this, decMasterId);  // Precious
                                    this.Enabled = false;
                                }
                            }
                        }
                        else if (strVoucherType == "Purchase Invoice")
                        {
                            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
                            decMasterId = spPurchaseMaster.PurchaseMasterIdViewByvoucherNoAndVoucherType(decVouchertypeId, strVoucherNo);
                            frmPurchaseInvoice objpurchase = new frmPurchaseInvoice();
                            objpurchase.WindowState = FormWindowState.Normal;
                            objpurchase.MdiParent = formMDI.MDIObj;
                            objpurchase.callFromAgeingCustomer(this, decMasterId);  // Precious
                            this.Enabled = true;
                        }
                        else if (strVoucherType == "Credit Note")
                        {
                            CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
                            decMasterId = spCreditNoteMaster.CreditNoteMasterIdView(decVouchertypeId, strVoucherNo);
                            frmCreditNote objpurchase = new frmCreditNote();
                            objpurchase.WindowState = FormWindowState.Normal;
                            objpurchase.MdiParent = formMDI.MDIObj;
                            objpurchase.callFromAgeingCustomer(this, decMasterId);  // Precious
                            this.Enabled = false;
                        }
                        else if (strVoucherType == "Debit Note")
                        {
                            DebitNoteMasterSP spDebitNote = new DebitNoteMasterSP();
                            decMasterId = spDebitNote.DebitNoteMasterIdView(decVouchertypeId, strVoucherNo);
                            frmDebitNote objpurchase = new frmDebitNote();
                            objpurchase.WindowState = FormWindowState.Normal;
                            objpurchase.MdiParent = formMDI.MDIObj;
                            objpurchase.callFromAgeingCustomer(this, decMasterId);  // Precious
                            this.Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR17:" + ex.Message;
            }
        }

        private void dtpAgeingDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtToDate.Text, out dt);
                dtpAgeingDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR18:" + ex.Message;
            }
        }

        private void dtpAgeingDate_CloseUp(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = dtpAgeingDate.Value.ToString("dd-MMM-yyyy");
                txtToDate.SelectAll();
                txtToDate.Focus();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR19:" + ex.Message;
            }
        }

        private void txtToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation DateValidationObj = new DateValidation();
                DateValidationObj.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                {
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR20:" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvReport, "Ageing Report", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "AR21:" + ex.Message;
            }
        }
    }
}
#endregion