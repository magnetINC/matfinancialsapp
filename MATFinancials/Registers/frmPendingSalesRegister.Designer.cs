﻿namespace MATFinancials
{
    partial class frmPendingSalesRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPendingSalesRegister));
            this.dgvPendingSales = new System.Windows.Forms.DataGridView();
            this.dgvtxtSINO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtPendingSalesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtxtNaration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnViewDetails = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPendingSales)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPendingSales
            // 
            this.dgvPendingSales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPendingSales.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvPendingSales.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPendingSales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPendingSales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtxtSINO,
            this.dgvtxtPendingSalesId,
            this.dgvtxtDate,
            this.dgvtxtNaration});
            this.dgvPendingSales.GridColor = System.Drawing.SystemColors.ButtonShadow;
            this.dgvPendingSales.Location = new System.Drawing.Point(200, 70);
            this.dgvPendingSales.Name = "dgvPendingSales";
            this.dgvPendingSales.Size = new System.Drawing.Size(700, 303);
            this.dgvPendingSales.TabIndex = 1;
            this.dgvPendingSales.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPendingSales_CellDoubleClick);
            // 
            // dgvtxtSINO
            // 
            this.dgvtxtSINO.HeaderText = "SI/NO";
            this.dgvtxtSINO.Name = "dgvtxtSINO";
            // 
            // dgvtxtPendingSalesId
            // 
            this.dgvtxtPendingSalesId.HeaderText = "Transaction Id";
            this.dgvtxtPendingSalesId.Name = "dgvtxtPendingSalesId";
            // 
            // dgvtxtDate
            // 
            this.dgvtxtDate.HeaderText = "Date";
            this.dgvtxtDate.Name = "dgvtxtDate";
            // 
            // dgvtxtNaration
            // 
            this.dgvtxtNaration.HeaderText = "Naration";
            this.dgvtxtNaration.Name = "dgvtxtNaration";
            this.dgvtxtNaration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnViewDetails
            // 
            this.btnViewDetails.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnViewDetails.FlatAppearance.BorderSize = 0;
            this.btnViewDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewDetails.ForeColor = System.Drawing.Color.Black;
            this.btnViewDetails.Location = new System.Drawing.Point(633, 393);
            this.btnViewDetails.Name = "btnViewDetails";
            this.btnViewDetails.Size = new System.Drawing.Size(85, 27);
            this.btnViewDetails.TabIndex = 10;
            this.btnViewDetails.Text = "View Details";
            this.btnViewDetails.UseVisualStyleBackColor = false;
            this.btnViewDetails.Click += new System.EventHandler(this.btnViewDetails_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(815, 393);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(724, 393);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 23;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmPendingSalesRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1084, 511);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnViewDetails);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvPendingSales);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPendingSalesRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pending Sales Register";
            this.Load += new System.EventHandler(this.frmPendingSalesRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPendingSales)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPendingSales;
        private System.Windows.Forms.Button btnViewDetails;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtSINO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtPendingSalesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtxtNaration;
    }
}