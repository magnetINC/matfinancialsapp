﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Registers
{
    public partial class frmCashRegister : Form
    {
        public decimal ClosingBalance = 0;
        public frmCashRegister()
        {
            InitializeComponent();
        }

        public void OtherAccount()
        {
            try
            {
                AccountLedgerSP ledger = new AccountLedgerSP();
                DataTable dtbl = new DataTable();
                dtbl = ledger.CashComboFill();
                cmbOtherAccount.DataSource = dtbl;
                cmbOtherAccount.ValueMember = "ledgerId";
                cmbOtherAccount.DisplayMember = "ledgerName";
                //cmbOtherAccount.SelectedIndex = 0;
                DataRow dr = dtbl.NewRow();
                dr["ledgerId"] = 0;
                dr["ledgerName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbOtherAccount.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmCashRegister_Load(object sender, EventArgs e)
        {
            try
            {

                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpToDate.MinDate = PublicVariables._dtFromDate;
                dtpToDate.MaxDate = PublicVariables._dtToDate;
                //dtpToDate.Value = DateTime.Now.Date; //PublicVariables._dtToDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpToDate.Text = dtpToDate.Value.ToString("dd-MMM-yyyy");

                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = DateTime.Now.ToString(); //PublicVariables._dtToDate.ToString("dd-MMM-yyyy");

                DateValidation objvalidation = new DateValidation();
                objvalidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }

                GetCurrentClosingBalance(Convert.ToDateTime(txtFromDate.Text));
                OtherAccount();
                GridFill();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// FillGrid function
        /// </summary>
        public void GridFill()
        {
            try
            {
                FinancialStatementSP spFinance = new FinancialStatementSP();
                AccountLedgerSP ledger = new AccountLedgerSP();
                DataTable dtbl = new DataTable();
                //dtbl = ledger.CashRegister(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), Convert.ToDecimal(cmbOtherAccount.SelectedValue));
                dtbl = spFinance.CashRegister(Convert.ToDateTime(dtpFromDate.Value.ToString("dd/MMM/yyyy")), Convert.ToDateTime(dtpToDate.Value.ToString("dd/MMM/yyyy")), Convert.ToDecimal(cmbOtherAccount.SelectedValue), "cash register".ToLower(), cmbOtherAccount.Text);
                if (Convert.ToDecimal(cmbOtherAccount.SelectedValue) != 0)
                {
                    GetCurrentClosingBalance(Convert.ToDateTime(txtFromDate.Text), Convert.ToDecimal(cmbOtherAccount.SelectedValue));
                }
                else
                {
                    GetCurrentClosingBalance(Convert.ToDateTime(txtFromDate.Text));
                }
                dgvCashRegister.Rows.Clear();
                decimal currentLedgerId = 0;
                decimal previousLedgerId = 0;
                decimal currentBalance = 0; // ClosingBalance ;   // assigns the current balance to the cosing balance of previous transaction
                decimal Credit = 0;
                decimal Debit = 0;
                decimal totalAsset = 0;
                string status = "";

                int i = 1;
                dgvCashRegister.Rows.Add();
                dgvCashRegister.Rows[i - 1].Cells["tBalance"].Value = Convert.ToDecimal(ClosingBalance).ToString("N2");
                dgvCashRegister.Rows[i - 1].Cells["ledgerName"].Value = "Opening Balance:";
                currentBalance += ClosingBalance;

                foreach (DataRow dr in dtbl.Rows)
                {                    
                    //currentLedgerId = Convert.ToDecimal(dr["ledgerId"]);
                    //status = Convert.ToString(dr["CrOrDr"]).ToString();
                    Credit = Convert.ToDecimal(dr["Credit"]);
                    Debit = Convert.ToDecimal(dr["Debit"]);

                    if (Credit == 0)
                    {
                        currentBalance += Debit;
                    }
                    else
                    {
                        currentBalance = currentBalance - Credit;
                    }
                    //dgvCashRegister.Rows.Add();
                    //dgvCashRegister.Rows[i].Cells["ledgerName"].Value = dr["ledgerName"];
                    //dgvCashRegister.Rows[i].Cells["eDate"].Value = Convert.ToDateTime(dr["eDate"]).ToShortDateString();
                    //dgvCashRegister.Rows[i].Cells["Memo"].Value = dr["Memo"];
                    //dgvCashRegister.Rows[i].Cells["voucherTypeName"].Value = dr["voucherTypeName"];
                    //dgvCashRegister.Rows[i].Cells["dgvtxtVoucherNo"].Value = dr["invoiceNo"];
                    //dgvCashRegister.Rows[i].Cells["Credit"].Value = Convert.ToDecimal(dr["Credit"]).ToString("N2");
                    //dgvCashRegister.Rows[i].Cells["Debit"].Value = Convert.ToDecimal(dr["Debit"]).ToString("N2");
                    //dgvCashRegister.Rows[i].Cells["tBalance"].Value = currentBalance.ToString("N2");
                    //dgvCashRegister.Rows[i].Cells["masterId"].Value = Convert.ToDecimal(dr["masterId"]);
                    //dgvCashRegister.Rows[i].Cells["POS"].Value = Convert.ToDecimal(dr["POS"]);
                    //previousLedgerId = currentLedgerId;
                    //i++;

                    dgvCashRegister.Rows.Add();
                    dgvCashRegister.Rows[i].Cells["ledgerName"].Value = dr["Ledger"];
                    dgvCashRegister.Rows[i].Cells["eDate"].Value = Convert.ToDateTime(dr["Date"]).ToShortDateString();
                    dgvCashRegister.Rows[i].Cells["Memo"].Value = dr["Narration"];
                    dgvCashRegister.Rows[i].Cells["voucherTypeName"].Value = dr["Form Type"];
                    dgvCashRegister.Rows[i].Cells["dgvtxtVoucherNo"].Value = dr["voucherNo"];
                    dgvCashRegister.Rows[i].Cells["Credit"].Value = Convert.ToDecimal(dr["Credit"]).ToString("N2");
                    dgvCashRegister.Rows[i].Cells["Debit"].Value = Convert.ToDecimal(dr["Debit"]).ToString("N2");
                    dgvCashRegister.Rows[i].Cells["tBalance"].Value = currentBalance.ToString("N2");
                    dgvCashRegister.Rows[i].Cells["masterId"].Value = Convert.ToDecimal(dr["masterId"]);
                    //dgvCashRegister.Rows[i].Cells["POS"].Value = Convert.ToDecimal(dr["POS"]);
                    previousLedgerId = currentLedgerId;
                    i++;
                }
                //dgvCashRegister.Rows.Add();
                ////dgvCashRegister.Rows[dgvCashRegister.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //dgvCashRegister.Rows[i].Cells["Memo"].Value = "Sub Total";
                //dgvCashRegister.Rows[i].Cells["tBalance"].Value = dgvCashRegister.Rows[i != 0 ? i - 1 : i].Cells["tBalance"].Value;

                totalAsset += Convert.ToDecimal(dgvCashRegister.Rows[i != 0 ? i - 1 : i].Cells["tBalance"].Value);

                decimal Asset = totalAsset + currentBalance;
                dgvCashRegister.Rows.Add();
                dgvCashRegister.Rows[i].Cells["tBalance"].Value = "==========";

                dgvCashRegister.Rows.Add();
               // dgvCashRegister.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvCashRegister.Rows[i + 1].Cells["tBalance"].Value = totalAsset.ToString("N2");
                dgvCashRegister.Rows[i + 1].Cells["ledgerName"].Value = "Current Balance:";

                dgvCashRegister.Rows.Add();
                dgvCashRegister.Rows[i + 2].Cells["tBalance"].Value = "==========";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetCurrentClosingBalance(DateTime currentDate)
        {
            DAL.DBMatConnection _db = new DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("select ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting inner join tbl_AccountLedger al on al.ledgerId = tbl_LedgerPosting.ledgerId where al.accountGroupId = 27 and Date <= '"+ currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if(ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
        }
        public void GetCurrentClosingBalance(DateTime currentDate, decimal ledgerId)
        {
            DAL.DBMatConnection _db = new DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("SELECT ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='" + ledgerId +"' and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
        }

        /// <summary>
        /// Call to Drill-Down
        /// </summary>
        /// <param name="strTypeofVoucher"></param>
        /// <param name="decMasterId"></param>
        public void CallToCorrespondingForm(string strTypeofVoucher, decimal decMasterId, decimal ledgerId, string voucherNo)
        {
            frmLedgerDetails ld = new frmLedgerDetails();
            try
            {
                switch (strTypeofVoucher)
                {
                    case "Contra Voucher":
                        frmContraVoucher frmContraVoucherObj = new frmContraVoucher();
                        frmContraVoucher frmContraVoucherOpen = Application.OpenForms["frmContraVoucher"] as frmContraVoucher;
                        if (frmContraVoucherOpen == null)
                        {
                            frmContraVoucherObj.MdiParent = this.MdiParent;
                            frmContraVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmContraVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmContraVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmContraVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Payment Voucher":
                        frmPaymentVoucher frmPaymentVoucherObj = new frmPaymentVoucher();
                        frmPaymentVoucher frmPaymentVoucherOpen = Application.OpenForms["frmPaymentVoucher"] as frmPaymentVoucher;
                        if (frmPaymentVoucherOpen == null)
                        {

                            frmPaymentVoucherObj.MdiParent = this.MdiParent;
                            frmPaymentVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPaymentVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPaymentVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPaymentVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Receipt Voucher":
                        frmReceiptVoucher frmReceiptVoucherObj = new frmReceiptVoucher();
                        frmReceiptVoucher frmReceiptVoucherOpen = Application.OpenForms["frmReceiptVoucher"] as frmReceiptVoucher;
                        if (frmReceiptVoucherOpen == null)
                        {
                            frmReceiptVoucherObj.MdiParent = this.MdiParent;
                            frmReceiptVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmReceiptVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmReceiptVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmReceiptVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Journal Voucher":
                        frmJournalVoucher frmJournalVoucherObj = new frmJournalVoucher();
                        frmJournalVoucher frmJournalVoucherOpen = Application.OpenForms["frmJournalVoucher"] as frmJournalVoucher;
                        if (frmJournalVoucherOpen == null)
                        {
                            frmJournalVoucherObj.MdiParent = this.MdiParent;
                            frmJournalVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmJournalVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmJournalVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmJournalVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "PDC Clearance":
                        frmPdcClearance frmPdcClearanceObj = new frmPdcClearance();
                        frmPdcClearance frmPdcClearanceOpen = Application.OpenForms["frmPdcClearance"] as frmPdcClearance;
                        if (frmPdcClearanceOpen == null)
                        {

                            frmPdcClearanceObj.MdiParent = this.MdiParent;
                            frmPdcClearanceObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPdcClearanceOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPdcClearanceOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPdcClearanceOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "PDC Payable":
                        frmPdcPayable frmPDCPayableObj = new frmPdcPayable();
                        frmPdcPayable frmPDCPayableOpen = Application.OpenForms["frmPdcPayable"] as frmPdcPayable;
                        if (frmPDCPayableOpen == null)
                        {

                            frmPDCPayableObj.MdiParent = this.MdiParent;
                            frmPDCPayableObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPDCPayableOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPDCPayableOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPDCPayableOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "PDC Receivable":
                        frmPdcReceivable frmPdcReceivableObj = new frmPdcReceivable();
                        frmPdcReceivable frmPdcReceivableOpen = Application.OpenForms["frmPdcReceivable"] as frmPdcReceivable;
                        if (frmPdcReceivableOpen == null)
                        {
                            frmPdcReceivableObj.MdiParent = this.MdiParent;
                            frmPdcReceivableObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPdcReceivableOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPdcReceivableOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPdcReceivableOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Purchase Invoice":
                        frmPurchaseInvoice frmPurchaseInvoiceObj = new frmPurchaseInvoice();
                        frmPurchaseInvoice frmPurchaseInvoiveOpen = Application.OpenForms["frmPurchaseInvoice"] as frmPurchaseInvoice;
                        if (frmPurchaseInvoiveOpen == null)
                        {
                            frmPurchaseInvoiceObj.MdiParent = this.MdiParent;
                            frmPurchaseInvoiceObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPurchaseInvoiveOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPurchaseInvoiveOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPurchaseInvoiveOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Purchase Return":
                        frmPurchaseReturn frmPurchaseReturnObj = new frmPurchaseReturn();
                        frmPurchaseReturn frmPurchaseReturnOpen = Application.OpenForms["frmPurchaseReturn"] as frmPurchaseReturn;
                        if (frmPurchaseReturnOpen == null)
                        {
                            frmPurchaseReturnObj.MdiParent = this.MdiParent;
                            frmPurchaseReturnObj.CallFromLedgerDetails(ld, decMasterId, true);
                        }
                        else
                        {
                            frmPurchaseReturnOpen.CallFromLedgerDetails(ld, decMasterId, true);
                            if (frmPurchaseReturnOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPurchaseReturnOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "Sales Invoice":
                        if (dgvCashRegister.CurrentRow.Cells["POS"].Value.ToString() == "0")
                        {

                            frmSalesInvoice frmSalesInvoiceObj = new frmSalesInvoice();
                            frmSalesInvoice frmSalesInvoiveOpen = Application.OpenForms["frmSalesInvoice"] as frmSalesInvoice;
                            if (frmSalesInvoiveOpen == null)
                            {
                                frmSalesInvoiceObj.MdiParent = this.MdiParent;
                                frmSalesInvoiceObj.CallFromLedgerDetails(ld, decMasterId, ledgerId, voucherNo);
                            }
                            else
                            {
                                frmSalesInvoiveOpen.CallFromLedgerDetails(ld, decMasterId, ledgerId, voucherNo);
                                if (frmSalesInvoiveOpen.WindowState == FormWindowState.Minimized)
                                {
                                    frmSalesInvoiveOpen.WindowState = FormWindowState.Normal;
                                }
                            }
                        }
                        else
                        {
                            frmPOS frmposObj = new frmPOS();
                            frmPOS frmposOpen = Application.OpenForms["frmPOS"] as frmPOS;
                            if (frmposOpen == null)
                            {
                                frmposObj.MdiParent = this.MdiParent;
                                frmposObj.CallFromLedgerDetails(ld, decMasterId);
                            }
                            else
                            {
                                frmposOpen.CallFromLedgerDetails(ld, decMasterId);
                                if (frmposOpen.WindowState == FormWindowState.Minimized)
                                {
                                    frmposOpen.WindowState = FormWindowState.Normal;
                                }
                            }
                        }
                        break;
                    case "Sales Return":
                        frmSalesReturn frmSalesReturnObj = new frmSalesReturn();
                        frmSalesReturn frmSalesReturnOpen = Application.OpenForms["frmSalesReturn"] as frmSalesReturn;
                        if (frmSalesReturnOpen == null)
                        {
                            frmSalesReturnObj.MdiParent = this.MdiParent;
                            frmSalesReturnObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmSalesReturnOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmSalesReturnOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmSalesReturnOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "Service Voucher":
                        frmServiceVoucher frmServiceVoucherObj = new frmServiceVoucher();
                        frmServiceVoucher frmServiceVoucherOpen = Application.OpenForms["frmServiceVoucher"] as frmServiceVoucher;
                        if (frmServiceVoucherOpen == null)
                        {
                            frmServiceVoucherObj.MdiParent = this.MdiParent;
                            frmServiceVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmServiceVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmServiceVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmServiceVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Credit Note":
                        frmCreditNote frmCreditNoteobj = new frmCreditNote();
                        frmCreditNote frmCreditNoteOpen = Application.OpenForms["frmCreditNote"] as frmCreditNote;
                        if (frmCreditNoteOpen == null)
                        {
                            frmCreditNoteobj.MdiParent = this.MdiParent;
                            frmCreditNoteobj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmCreditNoteOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmCreditNoteOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmCreditNoteOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Debit Note":
                        frmDebitNote frmDebitNoteObj = new frmDebitNote();
                        frmDebitNote frmDebitNoteOpen = Application.OpenForms["frmDebitNote"] as frmDebitNote;
                        if (frmDebitNoteOpen == null)
                        {
                            frmDebitNoteObj.MdiParent = this.MdiParent;
                            frmDebitNoteObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmDebitNoteOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmDebitNoteOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmDebitNoteOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Advance Payment":
                        frmAdvancePayment frmAdvancePaymentObj = new frmAdvancePayment();
                        frmAdvancePayment frmAdvancePaymentopen = Application.OpenForms["frmAdvancePayment"] as frmAdvancePayment;
                        if (frmAdvancePaymentopen == null)
                        {
                            frmAdvancePaymentObj.MdiParent = this.MdiParent;
                            frmAdvancePaymentObj.CallFromLedgerDetails(ld, decMasterId, 0);
                        }
                        else
                        {
                            frmAdvancePaymentopen.CallFromLedgerDetails(ld, decMasterId, 0);
                            if (frmAdvancePaymentopen.WindowState == FormWindowState.Minimized)
                            {
                                frmAdvancePaymentopen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Monthly Salary Voucher":
                        frmMonthlySalaryVoucher frmMonthlySalaryVoucherObj = new frmMonthlySalaryVoucher();
                        frmMonthlySalaryVoucher frmMonthlySalaryVoucheropen = Application.OpenForms["frmMonthlySalaryVoucher"] as frmMonthlySalaryVoucher;
                        if (frmMonthlySalaryVoucheropen == null)
                        {
                            frmMonthlySalaryVoucherObj.MdiParent = this.MdiParent;
                            frmMonthlySalaryVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmMonthlySalaryVoucheropen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmMonthlySalaryVoucheropen.WindowState == FormWindowState.Minimized)
                            {
                                frmMonthlySalaryVoucheropen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ACCL02:" + ex.Message;
            }
        }

        private void dgvCashRegister_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmLedgerDetails ld = new frmLedgerDetails();
            try
            {
                if (e.RowIndex != -1)
                {
                    if (dgvCashRegister.Rows[e.RowIndex].Cells["masterId"].Value != null && Convert.ToDecimal(dgvCashRegister.Rows[e.RowIndex].Cells["masterId"].Value) != 0)
                    {
                        decimal decMasterId = Convert.ToDecimal(dgvCashRegister.Rows[e.RowIndex].Cells["masterId"].Value);
                        decimal ledgerId = Convert.ToDecimal(dgvCashRegister.Rows[e.RowIndex].Cells["dgvtxtLedgerId"].Value);
                        string voucherNo = dgvCashRegister.Rows[e.RowIndex].Cells["dgvtxtVoucherNo"].Value.ToString();
                        string strTypeofVoucher = Convert.ToString(dgvCashRegister.Rows[e.RowIndex].Cells["voucherTypeName"].Value);
                        ld.WindowState = FormWindowState.Normal;
                        ld.MdiParent = formMDI.MDIObj;
                        CallToCorrespondingForm(strTypeofVoucher, decMasterId, ledgerId, voucherNo);

                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BE02:" + ex.Message;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            GridFill();
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpFromDate.Value;
                this.txtFromDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR10:" + ex.Message;
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpToDate.Value;
                this.txtToDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR9:" + ex.Message;
            }
        }

        private void dtpFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtFromDate.Text, out dt);
                dtpFromDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB15:" + ex.Message;
            }
        }

        private void dtpToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR6:" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvCashRegister, "CASH REGISTER AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "STKRD12:" + ex.Message;
            }
        }
    }
}
