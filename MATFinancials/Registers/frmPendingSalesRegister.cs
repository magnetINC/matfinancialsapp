﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MATFinancials.Classes;
using MATFinancials.DAL;

namespace MATFinancials
{
    public partial class frmPendingSalesRegister : Form
    {
        int inCurrenRowIndex = 0;
        public frmPendingSalesRegister()
        {
            InitializeComponent();
        }

        private void frmPendingSalesRegister_Load(object sender, EventArgs e)
        {
            Clear();
            FillGrid();
        }

        private void Clear()
        {

        }

        private void FillGrid()
        {
            dgvPendingSales.Rows.Clear();
            DataSet ds = new DataSet();
            DBMatConnection connection = new DBMatConnection();
            try
            {
                ds = connection.getDataSet("PendingSalesRegisterGridfillAll");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dgvPendingSales.Rows.Add();
                    int rowNumber = 0;
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        dgvPendingSales.Rows[rowNumber].Cells["dgvtxtPendingSalesId"].Value = row["PendingSalesMasterId"].ToString();
                        dgvPendingSales.Rows[rowNumber].Cells["dgvtxtDate"].Value = row["date"].ToString();
                        dgvPendingSales.Rows[rowNumber].Cells["dgvtxtNaration"].Value = row["narration"].ToString();
                        dgvPendingSales.Rows.Add();
                        rowNumber++;
                    }
                    dgvPendingSales.Rows.RemoveAt(rowNumber++);
                }
            }
            catch (Exception EX)
            {

            }
        }

        private void dgvPendingSales_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1 && e.ColumnIndex > -1)
                {
                    btnViewDetails_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SIREG14:" + ex.Message;
            }
        }

        private void btnViewDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPendingSales.CurrentRow != null)
                {
                    inCurrenRowIndex = dgvPendingSales.CurrentRow.Index;
                    //if (bool.Parse(dgvPendingSales.CurrentRow.Cells["dgvtxtPos"].Value.ToString()))           //check if sales is via pos or sales invoice
                    //{
                    frmPOS objfrmpos = new frmPOS();
                    decimal dcRegister = Convert.ToDecimal(dgvPendingSales.CurrentRow.Cells["dgvtxtPendingSalesId"].Value.ToString());
                    frmPOS openpos = Application.OpenForms["frmPOS"] as frmPOS;
                    if (openpos == null)
                    {
                        objfrmpos.WindowState = FormWindowState.Normal;
                        objfrmpos.MdiParent = formMDI.MDIObj;
                        objfrmpos.Show();
                        objfrmpos.CallFromPendingSalesRegister(dcRegister, this, dcRegister);
                        this.Close();
                    }
                    else
                    {
                        openpos.MdiParent = formMDI.MDIObj;
                        openpos.BringToFront();
                        openpos.CallFromPendingSalesRegister(dcRegister, this, dcRegister);
                        if (openpos.WindowState == FormWindowState.Minimized)
                        {
                            openpos.WindowState = FormWindowState.Normal;
                        }
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SIREG13:" + ex.Message;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvPendingSales.CurrentRow != null)
                {
                    decimal pendingSalesId = Convert.ToDecimal(dgvPendingSales.CurrentRow.Cells["dgvtxtPendingSalesId"].Value.ToString());
                    DeleteFunction(pendingSalesId);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void DeleteFunction(decimal pendingSalesId)
        {
            try
            {
                DBMatConnection conn = new DBMatConnection();
                string queryStr1 = string.Format("delete from tbl_pendingSalesMaster WHERE PendingSalesMasterId = {0}", pendingSalesId);
                string queryStr2 = string.Format("delete from tbl_pendingSalesDetails WHERE PendingSalesMasterId IN ({0})", pendingSalesId);
                conn.ExecuteQuery(queryStr1);
                conn.ExecuteQuery(queryStr2);
                FillGrid();
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "SIREG13:" + ex.Message;
            }
        }
    }
}
