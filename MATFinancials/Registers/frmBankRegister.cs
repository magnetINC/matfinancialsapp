﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Registers
{
    public partial class frmBankRegister : Form
    {
        public frmBankRegister()
        {
            InitializeComponent();
        }

        private void frmBankRegister_Load(object sender, EventArgs e)
        {
            try
            {

                dtpFromDate.MinDate = PublicVariables._dtFromDate;
                dtpFromDate.MaxDate = PublicVariables._dtToDate;
                dtpFromDate.Value = PublicVariables._dtFromDate;
                dtpToDate.MinDate = PublicVariables._dtFromDate;
                dtpToDate.MaxDate = PublicVariables._dtToDate;
                dtpToDate.Value = PublicVariables._dtToDate;
                dtpFromDate.Text = dtpFromDate.Value.ToString("dd-MMM-yyyy");
                dtpToDate.Text = dtpToDate.Value.ToString("dd-MMM-yyyy");

                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");

                DateValidation objvalidation = new DateValidation();
                objvalidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                {
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                }
                BankledgerComboFill();
                GridFill();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        decimal ClosingBalance = 0;
        public void GridFill()
        {
            try
            {
                FinancialStatementSP spFinance = new FinancialStatementSP();
                AccountLedgerSP ledger = new AccountLedgerSP();
                DataTable dtbl = new DataTable();
                //dtbl = ledger.BankRegister(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), Convert.ToDecimal(cmbBank.SelectedValue));
                dtbl = spFinance.CashRegister(Convert.ToDateTime(dtpFromDate.Value.ToString("dd/MMM/yyyy")), Convert.ToDateTime(dtpToDate.Value.ToString("dd/MMM/yyyy")), Convert.ToDecimal(cmbBank.SelectedValue), "bank register".ToLower(), cmbBank.Text);
                if (Convert.ToDecimal(cmbBank.SelectedValue) != 0)
                {
                    GetCurrentClosingBalance(Convert.ToDateTime(txtFromDate.Text), Convert.ToDecimal(cmbBank.SelectedValue));
                }
                else
                {
                    GetCurrentClosingBalance(Convert.ToDateTime(txtFromDate.Text));
                }
                dgvBankRegister.Rows.Clear();
                decimal currentBalance = 0;
                decimal Credit = 0;
                decimal Debit = 0;
                decimal totalAsset = 0;
               

                int i = 1;
                dgvBankRegister.Rows.Add();
                dgvBankRegister.Rows[i - 1].Cells["tBalance"].Value = Convert.ToDecimal(ClosingBalance).ToString("N2");
                dgvBankRegister.Rows[i - 1].Cells["ledgerName"].Value = "Opening Balance:";
                currentBalance += ClosingBalance;
                foreach (DataRow dr in dtbl.Rows)
                {
                    Credit = Convert.ToDecimal(dr["Credit"]);
                    Debit = Convert.ToDecimal(dr["Debit"]);

                    if(Debit != 0)
                    {
                        currentBalance += Debit - Credit;
                    }
                    else
                    {
                        currentBalance -= Credit - Debit;
                    }

                    //if (currentLedgerId != previousLedgerId && i != 0)
                    //{
                    //    currentBalance = Debit - Credit;
                    //    dgvBankRegister.Rows.Add();
                    //    dgvBankRegister.Rows[dgvBankRegister.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                    //    //dgvBankRegister.Rows[i].Cells["eDate"].Value = "Sub Total";
                    //    dgvBankRegister.Rows[i].Cells["tBalance"].Value = dgvBankRegister.Rows[i != 0 ? i - 1 : i].Cells["tBalance"].Value;
                    //    totalAsset += Convert.ToDecimal(dgvBankRegister.Rows[i != 0 ? i - 1 : i].Cells["tBalance"].Value);
                    //    i++;
                    //}

                    dgvBankRegister.Rows.Add();
                    dgvBankRegister.Rows[i].Cells["ledgerName"].Value = dr["Ledger"];
                    dgvBankRegister.Rows[i].Cells["eDate"].Value = Convert.ToDateTime(dr["Date"]).ToShortDateString();
                    dgvBankRegister.Rows[i].Cells["Memo"].Value = dr["Narration"];
                    dgvBankRegister.Rows[i].Cells["voucherTypeName"].Value = dr["Form Type"];
                    dgvBankRegister.Rows[i].Cells["dgvtxtVoucherNo"].Value = dr["voucherNo"];
                    dgvBankRegister.Rows[i].Cells["Credit"].Value = Convert.ToDecimal(dr["Credit"]).ToString("N2");
                    dgvBankRegister.Rows[i].Cells["Debit"].Value = Convert.ToDecimal(dr["Debit"]).ToString("N2");
                    dgvBankRegister.Rows[i].Cells["tBalance"].Value = currentBalance.ToString("N2");
                    dgvBankRegister.Rows[i].Cells["masterId"].Value = Convert.ToDecimal(dr["masterId"]);
                    i++;
                }
                dgvBankRegister.Rows.Add();
                dgvBankRegister.Rows[dgvBankRegister.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //dgvBankRegister.Rows[i].Cells["Memo"].Value = "Sub Total";
                dgvBankRegister.Rows[i].Cells["tBalance"].Value = dgvBankRegister.Rows[i != 0 ? i - 1 : i].Cells["tBalance"].Value;

                totalAsset += Convert.ToDecimal(dgvBankRegister.Rows[i !=0 ? i - 1 : i].Cells["tBalance"].Value);

                decimal Asset = totalAsset + currentBalance;
                dgvBankRegister.Rows.Add();
                dgvBankRegister.Rows[i + 1].Cells["tBalance"].Value = "==========";

                dgvBankRegister.Rows.Add();
                dgvBankRegister.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvBankRegister.Rows[i + 2].Cells["tBalance"].Value = totalAsset.ToString("N2");
                dgvBankRegister.Rows[i + 2].Cells["Memo"].Value = "Total Bank Value:";

                dgvBankRegister.Rows.Add();
                dgvBankRegister.Rows[i + 3].Cells["tBalance"].Value = "==========";
            
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void BankledgerComboFill()
        {
            try
            {
                AccountLedgerSP ledger = new AccountLedgerSP();
                DataTable dtbl = new DataTable();
                dtbl = ledger.BankledgerComboFill();             
                cmbBank.DataSource = dtbl;
                cmbBank.ValueMember = "ledgerId";
                cmbBank.DisplayMember = "ledgerName";
               // cmbBank.SelectedIndex = 0;
                DataRow dr = dtbl.NewRow();
                dr["ledgerId"] = 0;
                dr["ledgerName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
                cmbBank.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetCurrentClosingBalance(DateTime currentDate)
        {
            DAL.DBMatConnection _db = new DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("select ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting inner join tbl_AccountLedger al on al.ledgerId = tbl_LedgerPosting.ledgerId where al.accountGroupId = 28 and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
        }
        public void GetCurrentClosingBalance(DateTime currentDate, decimal ledgerId)
        {
            DAL.DBMatConnection _db = new DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("SELECT ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='" + ledgerId + "' and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            GridFill();
        }

        private void dtpFromDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtFromDate);
                if (txtFromDate.Text == string.Empty)
                    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                DateTime dt;
                DateTime.TryParse(txtFromDate.Text, out dt);
                dtpFromDate.Value = dt;
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "TB15:" + ex.Message;
            }
        }

        private void dtpToDate_Leave(object sender, EventArgs e)
        {
            try
            {
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(txtToDate);
                if (txtToDate.Text == string.Empty)
                    txtToDate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR6:" + ex.Message;
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpFromDate.Value;
                this.txtFromDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR10:" + ex.Message;
            }
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime date = this.dtpToDate.Value;
                this.txtToDate.Text = date.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR9:" + ex.Message;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                ExportNew ex = new ExportNew();
                ex.ExportExcel(dgvBankRegister, "BANK REGISTER AS AT:", 0, 0, "Excel", null, null, "");
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BR01:" + ex.Message;
            }
        }

        /// <summary>
        /// Precious
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBankRegister_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
  
        }

        /// <summary>
        /// for Drill Down Precious
        /// </summary>
        /// <param name="strTypeofVoucher"></param>
        /// <param name="decMasterId"></param>
        public void CallToCorrespondingForm(string strTypeofVoucher, decimal decMasterId)
        {
            frmLedgerDetails ld = new frmLedgerDetails();
            try
            {
                switch (strTypeofVoucher)
                {
                    case "Contra Voucher":
                        frmContraVoucher frmContraVoucherObj = new frmContraVoucher();
                        frmContraVoucher frmContraVoucherOpen = Application.OpenForms["frmContraVoucher"] as frmContraVoucher;
                        if (frmContraVoucherOpen == null)
                        {
                            frmContraVoucherObj.MdiParent = this.MdiParent;
                            frmContraVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmContraVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmContraVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmContraVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Payment Voucher":
                        frmPaymentVoucher frmPaymentVoucherObj = new frmPaymentVoucher();
                        frmPaymentVoucher frmPaymentVoucherOpen = Application.OpenForms["frmPaymentVoucher"] as frmPaymentVoucher;
                        if (frmPaymentVoucherOpen == null)
                        {

                            frmPaymentVoucherObj.MdiParent = this.MdiParent;
                            frmPaymentVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPaymentVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPaymentVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPaymentVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Receipt Voucher":
                        frmReceiptVoucher frmReceiptVoucherObj = new frmReceiptVoucher();
                        frmReceiptVoucher frmReceiptVoucherOpen = Application.OpenForms["frmReceiptVoucher"] as frmReceiptVoucher;
                        if (frmReceiptVoucherOpen == null)
                        {
                            frmReceiptVoucherObj.MdiParent = this.MdiParent;
                            frmReceiptVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmReceiptVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmReceiptVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmReceiptVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Journal Voucher":
                        frmJournalVoucher frmJournalVoucherObj = new frmJournalVoucher();
                        frmJournalVoucher frmJournalVoucherOpen = Application.OpenForms["frmJournalVoucher"] as frmJournalVoucher;
                        if (frmJournalVoucherOpen == null)
                        {
                            frmJournalVoucherObj.MdiParent = this.MdiParent;
                            frmJournalVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmJournalVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmJournalVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmJournalVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "PDC Clearance":
                        frmPdcClearance frmPdcClearanceObj = new frmPdcClearance();
                        frmPdcClearance frmPdcClearanceOpen = Application.OpenForms["frmPdcClearance"] as frmPdcClearance;
                        if (frmPdcClearanceOpen == null)
                        {

                            frmPdcClearanceObj.MdiParent = this.MdiParent;
                            frmPdcClearanceObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPdcClearanceOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPdcClearanceOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPdcClearanceOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "PDC Payable":
                        frmPdcPayable frmPDCPayableObj = new frmPdcPayable();
                        frmPdcPayable frmPDCPayableOpen = Application.OpenForms["frmPdcPayable"] as frmPdcPayable;
                        if (frmPDCPayableOpen == null)
                        {

                            frmPDCPayableObj.MdiParent = this.MdiParent;
                            frmPDCPayableObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPDCPayableOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPDCPayableOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPDCPayableOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "PDC Receivable":
                        frmPdcReceivable frmPdcReceivableObj = new frmPdcReceivable();
                        frmPdcReceivable frmPdcReceivableOpen = Application.OpenForms["frmPdcReceivable"] as frmPdcReceivable;
                        if (frmPdcReceivableOpen == null)
                        {
                            frmPdcReceivableObj.MdiParent = this.MdiParent;
                            frmPdcReceivableObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPdcReceivableOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPdcReceivableOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPdcReceivableOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Purchase Invoice":
                        frmPurchaseInvoice frmPurchaseInvoiceObj = new frmPurchaseInvoice();
                        frmPurchaseInvoice frmPurchaseInvoiveOpen = Application.OpenForms["frmPurchaseInvoice"] as frmPurchaseInvoice;
                        if (frmPurchaseInvoiveOpen == null)
                        {
                            frmPurchaseInvoiceObj.MdiParent = this.MdiParent;
                            frmPurchaseInvoiceObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmPurchaseInvoiveOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmPurchaseInvoiveOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPurchaseInvoiveOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Purchase Return":
                        frmPurchaseReturn frmPurchaseReturnObj = new frmPurchaseReturn();
                        frmPurchaseReturn frmPurchaseReturnOpen = Application.OpenForms["frmPurchaseReturn"] as frmPurchaseReturn;
                        if (frmPurchaseReturnOpen == null)
                        {
                            frmPurchaseReturnObj.MdiParent = this.MdiParent;
                            frmPurchaseReturnObj.CallFromLedgerDetails(ld, decMasterId, true);
                        }
                        else
                        {
                            frmPurchaseReturnOpen.CallFromLedgerDetails(ld, decMasterId, true);
                            if (frmPurchaseReturnOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmPurchaseReturnOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "Sales Return":
                        frmSalesReturn frmSalesReturnObj = new frmSalesReturn();
                        frmSalesReturn frmSalesReturnOpen = Application.OpenForms["frmSalesReturn"] as frmSalesReturn;
                        if (frmSalesReturnOpen == null)
                        {
                            frmSalesReturnObj.MdiParent = this.MdiParent;
                            frmSalesReturnObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmSalesReturnOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmSalesReturnOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmSalesReturnOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;

                    case "Service Voucher":
                        frmServiceVoucher frmServiceVoucherObj = new frmServiceVoucher();
                        frmServiceVoucher frmServiceVoucherOpen = Application.OpenForms["frmServiceVoucher"] as frmServiceVoucher;
                        if (frmServiceVoucherOpen == null)
                        {
                            frmServiceVoucherObj.MdiParent = this.MdiParent;
                            frmServiceVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmServiceVoucherOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmServiceVoucherOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmServiceVoucherOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Credit Note":
                        frmCreditNote frmCreditNoteobj = new frmCreditNote();
                        frmCreditNote frmCreditNoteOpen = Application.OpenForms["frmCreditNote"] as frmCreditNote;
                        if (frmCreditNoteOpen == null)
                        {
                            frmCreditNoteobj.MdiParent = this.MdiParent;
                            frmCreditNoteobj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmCreditNoteOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmCreditNoteOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmCreditNoteOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Debit Note":
                        frmDebitNote frmDebitNoteObj = new frmDebitNote();
                        frmDebitNote frmDebitNoteOpen = Application.OpenForms["frmDebitNote"] as frmDebitNote;
                        if (frmDebitNoteOpen == null)
                        {
                            frmDebitNoteObj.MdiParent = this.MdiParent;
                            frmDebitNoteObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmDebitNoteOpen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmDebitNoteOpen.WindowState == FormWindowState.Minimized)
                            {
                                frmDebitNoteOpen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Advance Payment":
                        frmAdvancePayment frmAdvancePaymentObj = new frmAdvancePayment();
                        frmAdvancePayment frmAdvancePaymentopen = Application.OpenForms["frmAdvancePayment"] as frmAdvancePayment;
                        if (frmAdvancePaymentopen == null)
                        {
                            frmAdvancePaymentObj.MdiParent = this.MdiParent;
                            frmAdvancePaymentObj.CallFromLedgerDetails(ld, decMasterId, 0);
                        }
                        else
                        {
                            frmAdvancePaymentopen.CallFromLedgerDetails(ld, decMasterId, 0);
                            if (frmAdvancePaymentopen.WindowState == FormWindowState.Minimized)
                            {
                                frmAdvancePaymentopen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    case "Monthly Salary Voucher":
                        frmMonthlySalaryVoucher frmMonthlySalaryVoucherObj = new frmMonthlySalaryVoucher();
                        frmMonthlySalaryVoucher frmMonthlySalaryVoucheropen = Application.OpenForms["frmMonthlySalaryVoucher"] as frmMonthlySalaryVoucher;
                        if (frmMonthlySalaryVoucheropen == null)
                        {
                            frmMonthlySalaryVoucherObj.MdiParent = this.MdiParent;
                            frmMonthlySalaryVoucherObj.CallFromLedgerDetails(ld, decMasterId);
                        }
                        else
                        {
                            frmMonthlySalaryVoucheropen.CallFromLedgerDetails(ld, decMasterId);
                            if (frmMonthlySalaryVoucheropen.WindowState == FormWindowState.Minimized)
                            {
                                frmMonthlySalaryVoucheropen.WindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "ACCL02:" + ex.Message;
            }
        }

        private void dgvBankRegister_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmLedgerDetails ld = new frmLedgerDetails();
            try
            {
                if (e.RowIndex != -1)
                {
                    if (dgvBankRegister.Rows[e.RowIndex].Cells["masterId"].Value != null && Convert.ToDecimal(dgvBankRegister.Rows[e.RowIndex].Cells["masterId"].Value) != 0)
                    {
                        decimal decMasterId = Convert.ToDecimal(dgvBankRegister.Rows[e.RowIndex].Cells["masterId"].Value);
                        string strTypeofVoucher = Convert.ToString(dgvBankRegister.Rows[e.RowIndex].Cells["voucherTypeName"].Value);
                        ld.WindowState = FormWindowState.Normal;
                        ld.MdiParent = formMDI.MDIObj;
                        CallToCorrespondingForm(strTypeofVoucher, decMasterId);

                    }
                }
            }
            catch (Exception ex)
            {
                formMDI.infoError.ErrorString = "BE02:" + ex.Message;
            }
        }
    }
}
