﻿using System;

namespace MATFinancials
{
    partial class frmBonusDeduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBonusDeduction));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtDeductionAmount = new System.Windows.Forms.TextBox();
            this.lblNarration = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDeductionAmount = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtNarration = new System.Windows.Forms.TextBox();
            this.cmbEmployeeCode = new System.Windows.Forms.ComboBox();
            this.lblEmployeeCode = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.txtBonusAmount = new System.Windows.Forms.TextBox();
            this.lblBonusAmount = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblDateMandatory = new System.Windows.Forms.Label();
            this.lblEmployeeCodeMandatory = new System.Windows.Forms.Label();
            this.lblDeductionAmountMandatory = new System.Windows.Forms.Label();
            this.lblBonusAmountMandatory = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Month = new System.Windows.Forms.TextBox();
            this.lblReceivingLedger = new System.Windows.Forms.Label();
            this.cmbReceivingLedger = new System.Windows.Forms.ComboBox();
            this.lblVoucherNoValidator = new System.Windows.Forms.Label();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.lblVoucherNo = new System.Windows.Forms.Label();
            this.lblCashOrBankValidator = new System.Windows.Forms.Label();
            this.cmbCashOrBank = new System.Windows.Forms.ComboBox();
            this.lblCashOrBank = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(621, 391);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 27);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(530, 391);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtDeductionAmount
            // 
            this.txtDeductionAmount.Location = new System.Drawing.Point(152, 104);
            this.txtDeductionAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDeductionAmount.MaxLength = 15;
            this.txtDeductionAmount.Name = "txtDeductionAmount";
            this.txtDeductionAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDeductionAmount.Size = new System.Drawing.Size(187, 20);
            this.txtDeductionAmount.TabIndex = 4;
            this.txtDeductionAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDeductionAmount_KeyDown);
            this.txtDeductionAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeductionAmount_KeyPress);
            this.txtDeductionAmount.Leave += new System.EventHandler(this.txtDeductionAmount_Leave);
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.BackColor = System.Drawing.Color.Transparent;
            this.lblNarration.ForeColor = System.Drawing.Color.Black;
            this.lblNarration.Location = new System.Drawing.Point(47, 178);
            this.lblNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(50, 13);
            this.lblNarration.TabIndex = 48;
            this.lblNarration.Text = "Narration";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(47, 51);
            this.lblDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 47;
            this.lblDate.Text = "Date";
            // 
            // lblDeductionAmount
            // 
            this.lblDeductionAmount.AutoSize = true;
            this.lblDeductionAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblDeductionAmount.ForeColor = System.Drawing.Color.Black;
            this.lblDeductionAmount.Location = new System.Drawing.Point(47, 106);
            this.lblDeductionAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblDeductionAmount.Name = "lblDeductionAmount";
            this.lblDeductionAmount.Size = new System.Drawing.Size(95, 13);
            this.lblDeductionAmount.TabIndex = 45;
            this.lblDeductionAmount.Text = "Deduction Amount";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(348, 391);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnSave_KeyDown);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(439, 391);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(85, 27);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtNarration
            // 
            this.txtNarration.Location = new System.Drawing.Point(50, 196);
            this.txtNarration.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtNarration.MaxLength = 5000;
            this.txtNarration.Multiline = true;
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.Size = new System.Drawing.Size(349, 128);
            this.txtNarration.TabIndex = 5;
            this.txtNarration.Enter += new System.EventHandler(this.txtNarration_Enter);
            this.txtNarration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNarration_KeyDown);
            this.txtNarration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNarration_KeyPress);
            // 
            // cmbEmployeeCode
            // 
            this.cmbEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmployeeCode.FormattingEnabled = true;
            this.cmbEmployeeCode.Location = new System.Drawing.Point(501, 47);
            this.cmbEmployeeCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbEmployeeCode.Name = "cmbEmployeeCode";
            this.cmbEmployeeCode.Size = new System.Drawing.Size(200, 21);
            this.cmbEmployeeCode.TabIndex = 1;
            this.cmbEmployeeCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEmployeeCode_KeyDown);
            // 
            // lblEmployeeCode
            // 
            this.lblEmployeeCode.AutoSize = true;
            this.lblEmployeeCode.BackColor = System.Drawing.Color.Transparent;
            this.lblEmployeeCode.ForeColor = System.Drawing.Color.Black;
            this.lblEmployeeCode.Location = new System.Drawing.Point(396, 51);
            this.lblEmployeeCode.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblEmployeeCode.Name = "lblEmployeeCode";
            this.lblEmployeeCode.Size = new System.Drawing.Size(81, 13);
            this.lblEmployeeCode.TabIndex = 57;
            this.lblEmployeeCode.Text = "Employee Code";
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.BackColor = System.Drawing.Color.Transparent;
            this.lblMonth.ForeColor = System.Drawing.Color.Black;
            this.lblMonth.Location = new System.Drawing.Point(47, 83);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(37, 13);
            this.lblMonth.TabIndex = 59;
            this.lblMonth.Text = "Month";
            // 
            // txtBonusAmount
            // 
            this.txtBonusAmount.Location = new System.Drawing.Point(501, 73);
            this.txtBonusAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtBonusAmount.MaxLength = 15;
            this.txtBonusAmount.Name = "txtBonusAmount";
            this.txtBonusAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBonusAmount.Size = new System.Drawing.Size(200, 20);
            this.txtBonusAmount.TabIndex = 3;
            this.txtBonusAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBonusAmount_KeyDown);
            this.txtBonusAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBonusAmount_KeyPress);
            this.txtBonusAmount.Leave += new System.EventHandler(this.txtBonusAmount_Leave);
            // 
            // lblBonusAmount
            // 
            this.lblBonusAmount.AutoSize = true;
            this.lblBonusAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblBonusAmount.ForeColor = System.Drawing.Color.Black;
            this.lblBonusAmount.Location = new System.Drawing.Point(396, 77);
            this.lblBonusAmount.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.lblBonusAmount.Name = "lblBonusAmount";
            this.lblBonusAmount.Size = new System.Drawing.Size(76, 13);
            this.lblBonusAmount.TabIndex = 65;
            this.lblBonusAmount.Text = "Bonus Amount";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd-MMM-yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(310, 48);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(23, 20);
            this.dtpDate.TabIndex = 1000;
            this.dtpDate.TabStop = false;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // dtpMonth
            // 
            this.dtpMonth.Checked = false;
            this.dtpMonth.CustomFormat = "MMMMyyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(310, 77);
            this.dtpMonth.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(23, 20);
            this.dtpMonth.TabIndex = 2;
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            this.dtpMonth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpMonth_KeyDown);
            // 
            // lblDateMandatory
            // 
            this.lblDateMandatory.AutoSize = true;
            this.lblDateMandatory.ForeColor = System.Drawing.Color.Red;
            this.lblDateMandatory.Location = new System.Drawing.Point(343, 51);
            this.lblDateMandatory.Name = "lblDateMandatory";
            this.lblDateMandatory.Size = new System.Drawing.Size(11, 13);
            this.lblDateMandatory.TabIndex = 66;
            this.lblDateMandatory.Text = "*";
            // 
            // lblEmployeeCodeMandatory
            // 
            this.lblEmployeeCodeMandatory.AutoSize = true;
            this.lblEmployeeCodeMandatory.ForeColor = System.Drawing.Color.Red;
            this.lblEmployeeCodeMandatory.Location = new System.Drawing.Point(705, 80);
            this.lblEmployeeCodeMandatory.Name = "lblEmployeeCodeMandatory";
            this.lblEmployeeCodeMandatory.Size = new System.Drawing.Size(11, 13);
            this.lblEmployeeCodeMandatory.TabIndex = 66;
            this.lblEmployeeCodeMandatory.Text = "*";
            // 
            // lblDeductionAmountMandatory
            // 
            this.lblDeductionAmountMandatory.AutoSize = true;
            this.lblDeductionAmountMandatory.ForeColor = System.Drawing.Color.Red;
            this.lblDeductionAmountMandatory.Location = new System.Drawing.Point(343, 111);
            this.lblDeductionAmountMandatory.Name = "lblDeductionAmountMandatory";
            this.lblDeductionAmountMandatory.Size = new System.Drawing.Size(11, 13);
            this.lblDeductionAmountMandatory.TabIndex = 66;
            this.lblDeductionAmountMandatory.Text = "*";
            // 
            // lblBonusAmountMandatory
            // 
            this.lblBonusAmountMandatory.AutoSize = true;
            this.lblBonusAmountMandatory.ForeColor = System.Drawing.Color.Red;
            this.lblBonusAmountMandatory.Location = new System.Drawing.Point(343, 83);
            this.lblBonusAmountMandatory.Name = "lblBonusAmountMandatory";
            this.lblBonusAmountMandatory.Size = new System.Drawing.Size(11, 13);
            this.lblBonusAmountMandatory.TabIndex = 66;
            this.lblBonusAmountMandatory.Text = "*";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(152, 48);
            this.txtDate.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtDate.MaxLength = 15;
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(161, 20);
            this.txtDate.TabIndex = 0;
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.Leave += new System.EventHandler(this.txtDate_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(705, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 1001;
            this.label1.Text = "*";
            // 
            // txt_Month
            // 
            this.txt_Month.Location = new System.Drawing.Point(152, 77);
            this.txt_Month.Margin = new System.Windows.Forms.Padding(5, 4, 5, 0);
            this.txt_Month.MaxLength = 15;
            this.txt_Month.Name = "txt_Month";
            this.txt_Month.Size = new System.Drawing.Size(161, 20);
            this.txt_Month.TabIndex = 1002;
            // 
            // lblReceivingLedger
            // 
            this.lblReceivingLedger.AutoSize = true;
            this.lblReceivingLedger.Location = new System.Drawing.Point(396, 133);
            this.lblReceivingLedger.Name = "lblReceivingLedger";
            this.lblReceivingLedger.Size = new System.Drawing.Size(91, 13);
            this.lblReceivingLedger.TabIndex = 1003;
            this.lblReceivingLedger.Text = "Receiving Ledger";
            // 
            // cmbReceivingLedger
            // 
            this.cmbReceivingLedger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReceivingLedger.FormattingEnabled = true;
            this.cmbReceivingLedger.Location = new System.Drawing.Point(501, 128);
            this.cmbReceivingLedger.Name = "cmbReceivingLedger";
            this.cmbReceivingLedger.Size = new System.Drawing.Size(200, 21);
            this.cmbReceivingLedger.TabIndex = 1004;
            // 
            // lblVoucherNoValidator
            // 
            this.lblVoucherNoValidator.AutoSize = true;
            this.lblVoucherNoValidator.ForeColor = System.Drawing.Color.Red;
            this.lblVoucherNoValidator.Location = new System.Drawing.Point(343, 28);
            this.lblVoucherNoValidator.Name = "lblVoucherNoValidator";
            this.lblVoucherNoValidator.Size = new System.Drawing.Size(11, 13);
            this.lblVoucherNoValidator.TabIndex = 1007;
            this.lblVoucherNoValidator.Text = "*";
            this.lblVoucherNoValidator.Visible = false;
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.Location = new System.Drawing.Point(152, 21);
            this.txtVoucherNo.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(181, 20);
            this.txtVoucherNo.TabIndex = 1005;
            this.txtVoucherNo.Visible = false;
            this.txtVoucherNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVoucherNo_KeyDown);
            // 
            // lblVoucherNo
            // 
            this.lblVoucherNo.AutoSize = true;
            this.lblVoucherNo.BackColor = System.Drawing.Color.Transparent;
            this.lblVoucherNo.ForeColor = System.Drawing.Color.Black;
            this.lblVoucherNo.Location = new System.Drawing.Point(47, 24);
            this.lblVoucherNo.Margin = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.lblVoucherNo.Name = "lblVoucherNo";
            this.lblVoucherNo.Size = new System.Drawing.Size(47, 13);
            this.lblVoucherNo.TabIndex = 1006;
            this.lblVoucherNo.Text = "Form No";
            this.lblVoucherNo.Visible = false;
            // 
            // lblCashOrBankValidator
            // 
            this.lblCashOrBankValidator.AutoSize = true;
            this.lblCashOrBankValidator.ForeColor = System.Drawing.Color.Red;
            this.lblCashOrBankValidator.Location = new System.Drawing.Point(705, 106);
            this.lblCashOrBankValidator.Name = "lblCashOrBankValidator";
            this.lblCashOrBankValidator.Size = new System.Drawing.Size(11, 13);
            this.lblCashOrBankValidator.TabIndex = 1010;
            this.lblCashOrBankValidator.Text = "*";
            this.lblCashOrBankValidator.Visible = false;
            // 
            // cmbCashOrBank
            // 
            this.cmbCashOrBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCashOrBank.FormattingEnabled = true;
            this.cmbCashOrBank.Location = new System.Drawing.Point(501, 99);
            this.cmbCashOrBank.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.cmbCashOrBank.Name = "cmbCashOrBank";
            this.cmbCashOrBank.Size = new System.Drawing.Size(200, 21);
            this.cmbCashOrBank.TabIndex = 1008;
            this.cmbCashOrBank.Visible = false;
            this.cmbCashOrBank.SelectedValueChanged += new System.EventHandler(this.cmbCashOrBank_SelectedValueChanged);
            this.cmbCashOrBank.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCashOrBank_KeyDown);
            // 
            // lblCashOrBank
            // 
            this.lblCashOrBank.AutoSize = true;
            this.lblCashOrBank.BackColor = System.Drawing.Color.Transparent;
            this.lblCashOrBank.ForeColor = System.Drawing.Color.Black;
            this.lblCashOrBank.Location = new System.Drawing.Point(396, 106);
            this.lblCashOrBank.Margin = new System.Windows.Forms.Padding(0, 5, 5, 0);
            this.lblCashOrBank.Name = "lblCashOrBank";
            this.lblCashOrBank.Size = new System.Drawing.Size(87, 13);
            this.lblCashOrBank.TabIndex = 1009;
            this.lblCashOrBank.Text = "Cash / Bank a/c";
            this.lblCashOrBank.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(705, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 1011;
            this.label2.Text = "*";
            // 
            // frmBonusDeduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(753, 441);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCashOrBankValidator);
            this.Controls.Add(this.cmbCashOrBank);
            this.Controls.Add(this.lblCashOrBank);
            this.Controls.Add(this.lblVoucherNoValidator);
            this.Controls.Add(this.txtVoucherNo);
            this.Controls.Add(this.lblVoucherNo);
            this.Controls.Add(this.cmbReceivingLedger);
            this.Controls.Add(this.lblReceivingLedger);
            this.Controls.Add(this.txt_Month);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.lblBonusAmountMandatory);
            this.Controls.Add(this.lblDeductionAmountMandatory);
            this.Controls.Add(this.lblEmployeeCodeMandatory);
            this.Controls.Add(this.lblDateMandatory);
            this.Controls.Add(this.dtpMonth);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.txtBonusAmount);
            this.Controls.Add(this.lblBonusAmount);
            this.Controls.Add(this.lblMonth);
            this.Controls.Add(this.cmbEmployeeCode);
            this.Controls.Add(this.lblEmployeeCode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtDeductionAmount);
            this.Controls.Add(this.lblNarration);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblDeductionAmount);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtNarration);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmBonusDeduction";
            this.Opacity = 0.85D;
            this.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bonus / Deduction ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBonusDeduction_FormClosing);
            this.Load += new System.EventHandler(this.frmBonusDeduction_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBonusDeduction_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtDeductionAmount;
        private System.Windows.Forms.Label lblNarration;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDeductionAmount;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtNarration;
        private System.Windows.Forms.ComboBox cmbEmployeeCode;
        private System.Windows.Forms.Label lblEmployeeCode;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.TextBox txtBonusAmount;
        private System.Windows.Forms.Label lblBonusAmount;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DateTimePicker dtpMonth;
        private System.Windows.Forms.Label lblDateMandatory;
        private System.Windows.Forms.Label lblEmployeeCodeMandatory;
        private System.Windows.Forms.Label lblDeductionAmountMandatory;
        private System.Windows.Forms.Label lblBonusAmountMandatory;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Month;
        private System.Windows.Forms.Label lblReceivingLedger;
        private System.Windows.Forms.ComboBox cmbReceivingLedger;
        private System.Windows.Forms.Label lblVoucherNoValidator;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private System.Windows.Forms.Label lblVoucherNo;
        private System.Windows.Forms.Label lblCashOrBankValidator;
        private System.Windows.Forms.ComboBox cmbCashOrBank;
        private System.Windows.Forms.Label lblCashOrBank;
        private System.Windows.Forms.Label label2;
    }
}