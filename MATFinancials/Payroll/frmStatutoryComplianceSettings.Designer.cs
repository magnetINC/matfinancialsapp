﻿namespace MATFinancials.Payroll
{
    partial class frmStatutoryComplianceSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStatutoryComplianceSettings));
            this.tabPAYESlab = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.tabStatutorySetup = new System.Windows.Forms.TabPage();
            this.chkAllowNHF = new System.Windows.Forms.CheckBox();
            this.lblPensionRecieivingLedger = new System.Windows.Forms.Label();
            this.cmbPensionReceivingLedger = new System.Windows.Forms.ComboBox();
            this.lblPayeReceivingLedger = new System.Windows.Forms.Label();
            this.cmbPAYEReceivingLedger = new System.Windows.Forms.ComboBox();
            this.lblSalaryPackages = new System.Windows.Forms.Label();
            this.ChkSalaryPackages = new System.Windows.Forms.CheckedListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.chkApplyPension = new System.Windows.Forms.CheckBox();
            this.chkApplyPAYE = new System.Windows.Forms.CheckBox();
            this.lblApplyPension = new System.Windows.Forms.Label();
            this.lblApplyPAYE = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPAYESlab.SuspendLayout();
            this.tabStatutorySetup.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPAYESlab
            // 
            this.tabPAYESlab.Controls.Add(this.label3);
            this.tabPAYESlab.Location = new System.Drawing.Point(4, 22);
            this.tabPAYESlab.Name = "tabPAYESlab";
            this.tabPAYESlab.Padding = new System.Windows.Forms.Padding(3);
            this.tabPAYESlab.Size = new System.Drawing.Size(607, 355);
            this.tabPAYESlab.TabIndex = 1;
            this.tabPAYESlab.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(294, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // tabStatutorySetup
            // 
            this.tabStatutorySetup.Controls.Add(this.chkAllowNHF);
            this.tabStatutorySetup.Controls.Add(this.lblPensionRecieivingLedger);
            this.tabStatutorySetup.Controls.Add(this.cmbPensionReceivingLedger);
            this.tabStatutorySetup.Controls.Add(this.lblPayeReceivingLedger);
            this.tabStatutorySetup.Controls.Add(this.cmbPAYEReceivingLedger);
            this.tabStatutorySetup.Controls.Add(this.lblSalaryPackages);
            this.tabStatutorySetup.Controls.Add(this.ChkSalaryPackages);
            this.tabStatutorySetup.Controls.Add(this.btnSave);
            this.tabStatutorySetup.Controls.Add(this.chkApplyPension);
            this.tabStatutorySetup.Controls.Add(this.chkApplyPAYE);
            this.tabStatutorySetup.Controls.Add(this.lblApplyPension);
            this.tabStatutorySetup.Controls.Add(this.lblApplyPAYE);
            this.tabStatutorySetup.Location = new System.Drawing.Point(4, 22);
            this.tabStatutorySetup.Name = "tabStatutorySetup";
            this.tabStatutorySetup.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatutorySetup.Size = new System.Drawing.Size(607, 355);
            this.tabStatutorySetup.TabIndex = 0;
            this.tabStatutorySetup.Text = "Statutory Setup";
            this.tabStatutorySetup.UseVisualStyleBackColor = true;
            // 
            // chkAllowNHF
            // 
            this.chkAllowNHF.AutoSize = true;
            this.chkAllowNHF.Location = new System.Drawing.Point(263, 102);
            this.chkAllowNHF.Name = "chkAllowNHF";
            this.chkAllowNHF.Size = new System.Drawing.Size(142, 30);
            this.chkAllowNHF.TabIndex = 1009;
            this.chkAllowNHF.Text = "Tick To Inlude NHF\r\n(i.e 2.5% of Basic Salary)";
            this.chkAllowNHF.UseVisualStyleBackColor = true;
            this.chkAllowNHF.Visible = false;
            // 
            // lblPensionRecieivingLedger
            // 
            this.lblPensionRecieivingLedger.AutoSize = true;
            this.lblPensionRecieivingLedger.Location = new System.Drawing.Point(154, 68);
            this.lblPensionRecieivingLedger.Name = "lblPensionRecieivingLedger";
            this.lblPensionRecieivingLedger.Size = new System.Drawing.Size(91, 13);
            this.lblPensionRecieivingLedger.TabIndex = 1008;
            this.lblPensionRecieivingLedger.Text = "Receiving Ledger";
            // 
            // cmbPensionReceivingLedger
            // 
            this.cmbPensionReceivingLedger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPensionReceivingLedger.FormattingEnabled = true;
            this.cmbPensionReceivingLedger.Location = new System.Drawing.Point(263, 65);
            this.cmbPensionReceivingLedger.Name = "cmbPensionReceivingLedger";
            this.cmbPensionReceivingLedger.Size = new System.Drawing.Size(174, 21);
            this.cmbPensionReceivingLedger.TabIndex = 1007;
            // 
            // lblPayeReceivingLedger
            // 
            this.lblPayeReceivingLedger.AutoSize = true;
            this.lblPayeReceivingLedger.Location = new System.Drawing.Point(154, 30);
            this.lblPayeReceivingLedger.Name = "lblPayeReceivingLedger";
            this.lblPayeReceivingLedger.Size = new System.Drawing.Size(91, 13);
            this.lblPayeReceivingLedger.TabIndex = 1006;
            this.lblPayeReceivingLedger.Text = "Receiving Ledger";
            // 
            // cmbPAYEReceivingLedger
            // 
            this.cmbPAYEReceivingLedger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPAYEReceivingLedger.FormattingEnabled = true;
            this.cmbPAYEReceivingLedger.Location = new System.Drawing.Point(263, 27);
            this.cmbPAYEReceivingLedger.Name = "cmbPAYEReceivingLedger";
            this.cmbPAYEReceivingLedger.Size = new System.Drawing.Size(174, 21);
            this.cmbPAYEReceivingLedger.TabIndex = 1005;
            // 
            // lblSalaryPackages
            // 
            this.lblSalaryPackages.AutoSize = true;
            this.lblSalaryPackages.Location = new System.Drawing.Point(19, 189);
            this.lblSalaryPackages.Name = "lblSalaryPackages";
            this.lblSalaryPackages.Size = new System.Drawing.Size(120, 13);
            this.lblSalaryPackages.TabIndex = 6;
            this.lblSalaryPackages.Text = "Select Salary Packages";
            this.lblSalaryPackages.Visible = false;
            // 
            // ChkSalaryPackages
            // 
            this.ChkSalaryPackages.FormattingEnabled = true;
            this.ChkSalaryPackages.Location = new System.Drawing.Point(22, 224);
            this.ChkSalaryPackages.MultiColumn = true;
            this.ChkSalaryPackages.Name = "ChkSalaryPackages";
            this.ChkSalaryPackages.Size = new System.Drawing.Size(120, 94);
            this.ChkSalaryPackages.TabIndex = 5;
            this.ChkSalaryPackages.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.Location = new System.Drawing.Point(497, 307);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 29);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // chkApplyPension
            // 
            this.chkApplyPension.AutoSize = true;
            this.chkApplyPension.Location = new System.Drawing.Point(114, 68);
            this.chkApplyPension.Name = "chkApplyPension";
            this.chkApplyPension.Size = new System.Drawing.Size(15, 14);
            this.chkApplyPension.TabIndex = 3;
            this.chkApplyPension.UseVisualStyleBackColor = true;
            // 
            // chkApplyPAYE
            // 
            this.chkApplyPAYE.AutoSize = true;
            this.chkApplyPAYE.Location = new System.Drawing.Point(114, 30);
            this.chkApplyPAYE.Name = "chkApplyPAYE";
            this.chkApplyPAYE.Size = new System.Drawing.Size(15, 14);
            this.chkApplyPAYE.TabIndex = 2;
            this.chkApplyPAYE.UseVisualStyleBackColor = true;
            // 
            // lblApplyPension
            // 
            this.lblApplyPension.AutoSize = true;
            this.lblApplyPension.Location = new System.Drawing.Point(19, 68);
            this.lblApplyPension.Name = "lblApplyPension";
            this.lblApplyPension.Size = new System.Drawing.Size(74, 13);
            this.lblApplyPension.TabIndex = 1;
            this.lblApplyPension.Text = "Apply Pension";
            // 
            // lblApplyPAYE
            // 
            this.lblApplyPAYE.AutoSize = true;
            this.lblApplyPAYE.Location = new System.Drawing.Point(19, 31);
            this.lblApplyPAYE.Name = "lblApplyPAYE";
            this.lblApplyPAYE.Size = new System.Drawing.Size(64, 13);
            this.lblApplyPAYE.TabIndex = 0;
            this.lblApplyPAYE.Text = "Apply PAYE";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabStatutorySetup);
            this.tabControl1.Controls.Add(this.tabPAYESlab);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(615, 381);
            this.tabControl1.TabIndex = 0;
            // 
            // frmStatutoryComplianceSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 439);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmStatutoryComplianceSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Statutory Compliance Settings";
            this.Load += new System.EventHandler(this.frmStatutorySetup_Load);
            this.tabPAYESlab.ResumeLayout(false);
            this.tabPAYESlab.PerformLayout();
            this.tabStatutorySetup.ResumeLayout(false);
            this.tabStatutorySetup.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPAYESlab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabStatutorySetup;
        private System.Windows.Forms.CheckBox chkApplyPension;
        private System.Windows.Forms.CheckBox chkApplyPAYE;
        private System.Windows.Forms.Label lblApplyPension;
        private System.Windows.Forms.Label lblApplyPAYE;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSalaryPackages;
        private System.Windows.Forms.CheckedListBox ChkSalaryPackages;
        private System.Windows.Forms.Label lblPensionRecieivingLedger;
        private System.Windows.Forms.ComboBox cmbPensionReceivingLedger;
        private System.Windows.Forms.Label lblPayeReceivingLedger;
        private System.Windows.Forms.ComboBox cmbPAYEReceivingLedger;
        private System.Windows.Forms.CheckBox chkAllowNHF;
    }
}