﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class LedgerInfo
    {
        public string Name { get; set; }
        public string Account_Group { get; set; }
        public string Opening_Balance { get; set; }
        public string Narration { get; set; }
        public string CrorDr { get; set; }
        
    }
}
