﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class voucherTypesInfo
    {
        public string name { get; set; }
        public string typeOfVoucher { get; set; }
        public string methodOfVoucherNumbering { get; set; }
        public string narration { get; set; }
        public string declaration { get; set; }
        public string active { get; set; }
        public string dotMatrixPrintFormat { get; set; }
    }
}
