﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate 
{
    class accountGroupInfo
    {
        private string name = "";
        private string under = "";
        private string narration = "";
        private string nature = "";
        private string affect_gross_profit = "";

        public string Name { get { return name; } set { name = value; } }
        public string Under { get { return under; } set { under = value; } }
        public string Narration { get { return narration; } set { narration = value; } }
        public string Nature { get { return nature; } set { nature = value; } }
        public string Affect_Gross_Profit { get { return affect_gross_profit; } set { affect_gross_profit = value; } }
        
    }
}
