﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class pricingLevelInfo
    {
        private string priceLevel = "";
        private string narration = "";

        public string PricingLevelName { get { return priceLevel; } set { priceLevel = value; } }
        public string Narration { get { return narration; } set { narration = value; } }
        
    }
}
