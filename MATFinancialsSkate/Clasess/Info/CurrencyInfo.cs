﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class currencyInfo
    {
        private string currencysymbol = "";
        private string narration = "";
        private string subunitname = "";

        public string CurrencyName { get; set; }
        public string CurrencySymbol { get { return currencysymbol; } set { currencysymbol = value; } }
        public string NoOfDecimalPlaces { get; set; }
        public string Narration { get { return narration; } set { narration = value; } }
        public string SubUnitName { get { return subunitname; } set { subunitname = value; } }
        
    }
}
