﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class batchesInfo
    {
        private string productname;
        //private DateTime mfgdate = DateTime.Today;
        //private DateTime expirydate = DateTime.Today.AddDays(10);.
        private string mfgdate;
        private string expirydate;
        private string batchname = "NA";
        private string narration = "";

        public string ProductName { get { return productname; } set { productname = value; } }
        //public DateTime MfgDate { get { return mfgdate; } set { mfgdate = value; } }
        //public DateTime ExpiryDate { get { return expirydate; } set { expirydate = value; } }

        public string MfgDate { get { return mfgdate; } set { mfgdate = value; } }
        public string ExpiryDate { get { return expirydate; } set { expirydate = value; } }

        public string BatchName { get { return batchname; } set { batchname = value; } }
        public string Narration { get { return narration; } set { narration = value; } }
    }
}
