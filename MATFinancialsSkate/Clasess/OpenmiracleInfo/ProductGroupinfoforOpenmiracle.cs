﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class ProductGroupinfoforOpenmiracle
    {
        public decimal GroupUnder { get; set; }
        public string GroupName { get; set; }
        public string Narration { get; set; }
    }
}
