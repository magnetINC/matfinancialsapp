﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class UnitinfoforOpenmiracle
    {
        public string UnitName { get; set; }
        public string FormalName { get; set; }
        public decimal noOfDecimalPlaces { get; set; }
        public string Narration { get; set; }
    }
}
