﻿

 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MATFinancialsI.DAL
{
    public class FinancialDate : DBConnection
    {
        public DateTime GetFromDate()
        {
            if (sqlCon.State == System.Data.ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("MiracleI_Financial_FROM_Date", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            return Convert.ToDateTime((sqlCmd.ExecuteScalar() != null) ? sqlCmd.ExecuteScalar() : DateTime.Today);
        }
        public DateTime GetToDate()
        {
            if (sqlCon.State == System.Data.ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("MiracleI_Financial_TO_Date", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            return Convert.ToDateTime((sqlCmd.ExecuteScalar() != null) ? sqlCmd.ExecuteScalar() : DateTime.Today);
        }
    }
}
