﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MATFinancialsI.DAL
{
    public class LoginCheck : DBConnection
    {
        public bool CheckUserAndPassord(string strUserName, string strPassword)
        {
            bool isTrue = false;
            if (sqlCon.State == System.Data.ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("CheckUserAndPassword", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@userName", System.Data.SqlDbType.VarChar).Value = strUserName;
            sqlCmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = strPassword;
            object Result = sqlCmd.ExecuteScalar();
            if (Result!=null)
            {
                if (int.Parse(Result.ToString())==1)
                {
                    isTrue = true;
                }
            }
            return isTrue;
        }
    }
}
