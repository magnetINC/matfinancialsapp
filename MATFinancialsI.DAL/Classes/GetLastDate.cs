﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MATFinancialsI.DAL
{
    public class GetLastDate:DBConnection
    {
        public DateTime GetLastDay()
        {
            if (sqlCon.State == System.Data.ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("MiracleI_MaxDate", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            return Convert.ToDateTime((sqlCmd.ExecuteScalar() != null) ? sqlCmd.ExecuteScalar() : DateTime.Today);
        }
    }
}
