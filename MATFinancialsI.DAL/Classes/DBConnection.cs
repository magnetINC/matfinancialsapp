﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Win32;
using Entity;
using System.IO;
using System.Xml;

namespace MATFinancialsI.DAL
{
    public class DBConnection
    {
        protected SqlConnection sqlCon;
        public DBConnection()
        {
            try
            {
                if (GeneralInfo.strCon == null)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load("Connection.xml");
                    GeneralInfo.strCon = xDoc.DocumentElement.SelectSingleNode("conection").InnerText;
                    sqlCon = new SqlConnection(GeneralInfo.strCon);
                    if (sqlCon.State == System.Data.ConnectionState.Closed)
                        sqlCon.Open();
                    ConnectionString.IsConnectionTrue = true;
                    xDoc.DocumentElement.RemoveChild(xDoc.DocumentElement.ChildNodes[0]);
                    FileStream WRITER = new FileStream("Connection.xml", FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite);
                    xDoc.Save(WRITER);
                    WRITER.Close();

                }
                else
                {
                    sqlCon = new SqlConnection(GeneralInfo.strCon);
                    if (sqlCon.State == System.Data.ConnectionState.Closed)
                        sqlCon.Open();
                    ConnectionString.IsConnectionTrue = true;
                }
            }
            catch (Exception)
            { }

        }
        public class GeneralInfo
        {
            public static string strCon { get; set; }
        }
    }
}
