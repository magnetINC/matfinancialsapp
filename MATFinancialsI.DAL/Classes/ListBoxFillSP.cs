﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Entity;

namespace MATFinancialsI.DAL
{
  public class ListBoxFillSP : DBConnection
    {
      public List<DataTable> GetList(CategoryInfo infoCategory)
        {
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_ListBoxFill", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.Add("@module", SqlDbType.VarChar).Value = (infoCategory.ModuleName != string.Empty) ? infoCategory.ModuleName : "Overall Statistics";
            sqlDa.SelectCommand.Parameters.Add("@fromDate", SqlDbType.DateTime).Value = infoCategory.DateFrom;
            sqlDa.SelectCommand.Parameters.Add("@toDate", SqlDbType.DateTime).Value = infoCategory.DateTo;
            sqlDa.SelectCommand.Parameters.Add("@type", SqlDbType.VarChar).Value = infoCategory.TypeName;
            sqlDa.SelectCommand.Parameters.Add("@category", SqlDbType.VarChar).Value = (infoCategory.CatagoryName != string.Empty) ? infoCategory.CatagoryName : "Overall Statistics";
            sqlDa.SelectCommand.Parameters.Add("@subCategory", SqlDbType.VarChar).Value = (infoCategory.SubCatagoryName != string.Empty) ? infoCategory.SubCatagoryName : "Overall Statistics";
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
    }
}
