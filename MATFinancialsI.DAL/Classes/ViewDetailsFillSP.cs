﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace MATFinancialsI.DAL
{
    public class ViewDetailsFillSP:DBConnection
    {
        public List<DataTable> GetViewDetailsData()
        {
            string strStoredProcedure = null;
            if (ViewDetailsInfo.ModuleName == "Finance" || ViewDetailsInfo.ModuleName == null || ViewDetailsInfo.ModuleName == "Overall Statistics")
            {
                if ((ViewDetailsInfo.Catagory == "Overall Statistics") || (ViewDetailsInfo.Catagory == null))
                {
                    strStoredProcedure = ("MiracleI_ProfitAndLoss_View_Details");
                }
                else
                {
                    strStoredProcedure = "MiracleI_" + ViewDetailsInfo.Catagory.Trim() + "_View_Details";
                }
            }
            else
            {
                strStoredProcedure = "MiracleI_" + ViewDetailsInfo.ModuleName.Trim() + "_View_Details";
            }
            List<char> resutl = strStoredProcedure.ToList();
            resutl.RemoveAll(c => c == ' ');
            strStoredProcedure = new string(resutl.ToArray());
            List<DataTable> lstDataTable = new List<DataTable>();
            DataTable dtbl = new DataTable();
            SqlDataAdapter sqlDa = new SqlDataAdapter(strStoredProcedure, sqlCon);
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.Add("@type", SqlDbType.VarChar).Value = (ViewDetailsInfo.Type != string.Empty && ViewDetailsInfo.Type != null) ? ViewDetailsInfo.Type : "Month";
            sqlDa.SelectCommand.Parameters.Add("@onDate", SqlDbType.DateTime).Value = ViewDetailsInfo.OnDate;
            sqlDa.SelectCommand.Parameters.Add("@checkValue", SqlDbType.VarChar).Value = (ViewDetailsInfo.CheckValue != string.Empty && ViewDetailsInfo.CheckValue !=  null) ? ViewDetailsInfo.CheckValue : "Overall Statistics";
            sqlDa.SelectCommand.Parameters.Add("@subCategory", SqlDbType.VarChar).Value = (ViewDetailsInfo.SubCatagory != string.Empty && ViewDetailsInfo.SubCatagory != null) ? ViewDetailsInfo.SubCatagory : "Overall Statistics";
            sqlDa.SelectCommand.Parameters.Add("@Category", SqlDbType.VarChar).Value = (ViewDetailsInfo.Catagory != string.Empty && ViewDetailsInfo.Catagory != null) ? ViewDetailsInfo.Catagory : "Overall Statistics";
            if (ViewDetailsInfo.ModuleName == "Finance" && ViewDetailsInfo.Catagory == "Tax Details")
            {
                sqlDa.SelectCommand.Parameters.Add("@sortBy", SqlDbType.VarChar).Value =(ViewDetailsInfo.SortBy != null && ViewDetailsInfo.SortBy != string.Empty) ? ViewDetailsInfo.SortBy:"Amount";
            }
            else if (ViewDetailsInfo.ModuleName == "Finance" && ViewDetailsInfo.Catagory == "Budget")
            {
                sqlDa.SelectCommand.Parameters.Add("@budgetName", SqlDbType.VarChar).Value = (ViewDetailsInfo.SubCatagory != null && ViewDetailsInfo.SubCatagory != string.Empty) ? ViewDetailsInfo.SubCatagory : "Overall Statistics";
            }


            dtbl.Columns.Add("Sl No", typeof(int));
            dtbl.Columns["Sl No"].AutoIncrement = true;
            dtbl.Columns["Sl No"].AutoIncrementSeed = 1;
            dtbl.Columns["Sl No"].AutoIncrementStep = 1;
            sqlDa.Fill(dtbl);
            lstDataTable.Add(dtbl);
            if (dtbl.Columns.Contains("ledgerId"))
            {
                dtbl.Columns.Remove("ledgerId");
            }
            return lstDataTable;
        }
    }
}
