﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace MATFinancialsI.DAL
{
    public class BarChartFillSP : DBConnection
    {
        public List<DataTable> GetBarTopChart()
        {
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_OverallGraph_Top", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
        public List<DataTable> GetBarAccountGroupChart()
        {
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_FinancialStatiticsGraph_AccountGroups", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
        public List<DataTable> GetBarColor(CategoryInfo CategoryInfo)
        {
            OpeningAndClosingStockDetails objOpeningAndClosingStockDetails = new OpeningAndClosingStockDetails();
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_TopGraph_COLOR_Check", sqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.SelectCommand.Parameters.Add("@OpeningStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.OpeningStockValueGetOnDate(CategoryInfo);
            sqlDa.SelectCommand.Parameters.Add("@ClosingStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.ClosingStockValueGetOnDate(CategoryInfo);
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
    }
}
