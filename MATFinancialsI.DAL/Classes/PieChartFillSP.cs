﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace MATFinancialsI.DAL
{
   public class PieChartFillSP:DBConnection
    {
       OpeningAndClosingStockDetails objOpeningAndClosingStockDetails = new OpeningAndClosingStockDetails();
       public List<DataTable> GetPieChartTable(CategoryInfo CategoryInfo)
        {
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_PieChart_ValueForTable", sqlCon);
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.Add("@OpeningStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.OpeningStockValueGetOnDate(CategoryInfo);
            sqlDa.SelectCommand.Parameters.Add("@ClosingStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.ClosingStockValueGetOnDate(CategoryInfo);
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
       public List<DataTable> GetPieChart(CategoryInfo CategoryInfo)
        {
            DataTable dtbl = new DataTable();
            List<DataTable> dataTableList = new List<DataTable>();
            SqlDataAdapter sqlDa = new SqlDataAdapter("MiracleI_ValueForPieChart", sqlCon);
            sqlDa.SelectCommand.CommandTimeout = 300;
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDa.SelectCommand.Parameters.Add("@OpeningStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.OpeningStockValueGetOnDate(CategoryInfo);
            sqlDa.SelectCommand.Parameters.Add("@ClosingStock", SqlDbType.Decimal).Value = objOpeningAndClosingStockDetails.ClosingStockValueGetOnDate(CategoryInfo);
            sqlDa.Fill(dtbl);
            dataTableList.Add(dtbl);
            return dataTableList;
        }
    }
}
