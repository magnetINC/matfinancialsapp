﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Entity;

namespace MATFinancialsI.DAL
{
    class OpeningAndClosingStockDetails : DBConnection
    {
        public string GetProfitAndLossSettings()
        {
            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("SettingsStatusCheck", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@settingsName", SqlDbType.VarChar).Value = "StockValueCalculationMethod";
            return Convert.ToString(sqlCmd.ExecuteScalar().ToString());
        }

        public decimal ClosingStockValueGetOnDate(CategoryInfo infoCatagory)
        {
            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            string calculationMethod = GetProfitAndLossSettings();
            SqlCommand sqlCmd = new SqlCommand();
            if (calculationMethod == "FIFO")
            {

                sqlCmd = new SqlCommand("StockValueOnDateByFIFO", sqlCon);

            }
            else if (calculationMethod == "Average Cost")
            {

                sqlCmd = new SqlCommand("StockValueOnDateByAVCO", sqlCon);

            }
            else if (calculationMethod == "High Cost")
            {

                sqlCmd = new SqlCommand("StockValueOnDateByHighCost", sqlCon);

            }
            else if (calculationMethod == "Low Cost")
            {

                sqlCmd = new SqlCommand("StockValueOnDateByLowCost", sqlCon);

            }
            else if (calculationMethod == "Last Purchase Rate")
            {

                sqlCmd = new SqlCommand("StockValueOnDateByLastPurchaseRate", sqlCon);

            }
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@date", SqlDbType.DateTime).Value = infoCatagory.DateTo;
            decimal outVariable = 0;
            object obj = sqlCmd.ExecuteScalar().ToString();
            if (obj != null)
            {
                decimal.TryParse(obj.ToString(), out outVariable);
            }
            return outVariable;
        }

        public decimal OpeningStockValueGetOnDate(CategoryInfo infoCatagory)
        {
                if (sqlCon.State == ConnectionState.Closed)
                {
                    sqlCon.Open();
                }
                string calculationMethod = GetProfitAndLossSettings();
                SqlCommand sqlCmd = new SqlCommand();
                if (calculationMethod == "FIFO")
                {
                    sqlCmd = new SqlCommand("StockValueOnDateByFIFOForOpeningStockForBalancesheet", sqlCon);
                }
                else if (calculationMethod == "Average Cost")
                {
                    sqlCmd = new SqlCommand("StockValueOnDateByAVCOForOpeningStockForBalanceSheet", sqlCon);

                }
                else if (calculationMethod == "High Cost")
                {
                    sqlCmd = new SqlCommand("StockValueOnDateByHighCostForOpeningStockBlncSheet", sqlCon);
                }
                else if (calculationMethod == "Low Cost")
                {
                    sqlCmd = new SqlCommand("StockValueOnDateByLowCostForOpeningStockForBlncSheet", sqlCon);
                }
                else if (calculationMethod == "Last Purchase Rate")
                {
                    sqlCmd = new SqlCommand("StockValueOnDateByLastPurchaseRateForOpeningStockBlncSheet", sqlCon);
                }
                decimal dcstockValue = 0;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@date", SqlDbType.DateTime).Value = infoCatagory.DateFrom;
                sqlCmd.Parameters.Add("@fromDate", SqlDbType.DateTime).Value = infoCatagory.DateTo;
                object obj = sqlCmd.ExecuteScalar();
                if (obj != null)
                {
                    decimal.TryParse(obj.ToString(), out dcstockValue);
                }
            return dcstockValue;
        }


    }
}
