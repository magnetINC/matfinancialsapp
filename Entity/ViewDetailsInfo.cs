﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public static class ViewDetailsInfo
    {
        public static string ModuleName { get; set; }
        public static string Type { get; set; }
        public static DateTime OnDate { get; set; }
        public static string Catagory { get; set; }
        public static string SubCatagory { get; set; }
        public static string CheckValue { get; set; }
        public static string SortBy { get; set; }
    }
}
