﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using ScottLogic.ScottLogic.PieChart;
using System.Windows;

namespace ScottLogic.Controls.PieChart
{
    /// <summary>
    /// Selects a colour purely based on its location within a collection.
    /// </summary>
    public class IndexedColourSelector : DependencyObject, IColorSelector
    {
        /// <summary>
        /// An array of brushes.
        /// </summary>
        public Brush[] Brushes
        {
            get { return (Brush[])GetValue(BrushesProperty); }
            set { SetValue(BrushesProperty, value); }
        }

        public static readonly DependencyProperty BrushesProperty = 
                       DependencyProperty.Register("BrushesProperty", typeof(Brush[]), typeof(IndexedColourSelector), new UIPropertyMetadata(null));


        public Brush SelectBrush(object item, int index)
        {
            SolidColorBrush myBrush = new SolidColorBrush();
            BrushConverter conv = new BrushConverter();
            if ( (item as WPFPieChart.AssetClass).Class.ToString() == "Asset")
            {
                myBrush = conv.ConvertFromString("#FFFF9600") as SolidColorBrush;
                return myBrush;
            }
            else if ((item as WPFPieChart.AssetClass).Class.ToString() == "Expenses")
            {
                myBrush = conv.ConvertFromString("#FF8A00D1") as SolidColorBrush;
                return myBrush;
            }
            else if ((item as WPFPieChart.AssetClass).Class.ToString() == "Income")
            {
                myBrush = conv.ConvertFromString("#FF5AB401") as SolidColorBrush;
                return myBrush;
            }
            else if ((item as WPFPieChart.AssetClass).Class.ToString() == "Liability")
            {
                myBrush = conv.ConvertFromString("#FFDA0077") as SolidColorBrush;
                return myBrush;
            }
           
            return System.Windows.Media.Brushes.Black;
        }
    }
}
