﻿
 
using System.Timers;
using System.Windows;
using System.Windows.Controls;

namespace MATFinancialsI
{
    /// <summary>
    /// Interaction logic for Progress.xaml
    /// </summary>
    public partial class Progress : UserControl
    {
        private delegate void VoidDelegete();
        private Timer timer;
        private int progress;

        public Progress()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }
        void OnLoaded(object sender, RoutedEventArgs e)
        {
            timer = new Timer(80);
            timer.Elapsed += OnTimerElapsed;
            timer.Start();
        }

        void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            rotationCanvas.Dispatcher.Invoke
                (
                new VoidDelegete(
                    delegate
                    {
                        SpinnerRotate.Angle += 25;
                        if (SpinnerRotate.Angle == 360)
                        {
                            SpinnerRotate.Angle = 5;
                        }
                    }
                    ),
                null
                );

        }



        public int Progresss
        {
            get { return progress; }
            set
            {
                progress = value;
            }
        }
    }
}
