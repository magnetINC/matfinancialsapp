﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using MATFinancialsI.BLL;
using Entity;

namespace WPFPieChart
{
    public class AssetClass : INotifyPropertyChanged
    {
        private String myClass;

        public String Class
        {
            get { return myClass; }
            set {
                myClass = value;
                RaisePropertyChangeEvent("Class");
            }
        }

        private double dblValue;

        public double Value
        {
            get { return dblValue; }
            set {
                dblValue = value;
                RaisePropertyChangeEvent("Value");
            }
        }

        public static List<AssetClass> ConstructTestData(CategoryInfo infoCatagory)
        {
            CallSP objCallSP = new CallSP();
            List<DataTable> objList = objCallSP.GetPieChartDetails(infoCatagory);

            List<AssetClass> assetClasses = new List<AssetClass>();
            for (int i = 0; i < objList[0].Rows.Count; i++)
            {
                assetClasses.Add(new AssetClass() { Class = objList[0].Rows[i][0].ToString(), Value = Convert.ToDouble(objList[0].Rows[i][1]) });               
            }

            return assetClasses;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChangeEvent(String propertyName)
        {
            if (PropertyChanged!=null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            
        }

        #endregion
    }
}
