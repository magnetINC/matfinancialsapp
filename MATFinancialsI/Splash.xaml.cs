﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;
using Entity;
using MATFinancialsI.BLL;
using System.Diagnostics;
using System.Data;


namespace MATFinancialsI
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : Window
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();
        public Splash()
        {
            InitializeComponent();
            grdLogIn.Visibility = System.Windows.Visibility.Hidden;
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();

        }
        DataTable dtbl = new DataTable();
        public void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(2000);
            ConnectionString.IsConnectionTrue = false;
            ConnectionString.Connection = Environment.CurrentDirectory + @"\Data\DBMATAccounting.mdf";
            CallSP objCallSp = new CallSP();
            if (objCallSp.GetCompanyName().Count > 0)
            {
                dtbl = objCallSp.GetCompanyName()[0];
            }
            else
            {
                string fullpath = string.Empty;
                List<Process> taskBarProcesses = Process.GetProcesses().Where(p => !string.IsNullOrEmpty(p.MainWindowTitle)).ToList();
                foreach (Process proc in taskBarProcesses)
                {
                    if (proc.ProcessName.ToLower() == "matfinancials")
                    {
                        fullpath = proc.MainModule.FileName;
                        fullpath = fullpath.Remove(fullpath.Length - 16);
                        ConnectionString.Connection = fullpath + @"Data\DBMATAccounting.mdf";
                        break;
                    }
                }
                if (objCallSp.GetCompanyName().Count > 0)
                {
                    dtbl = objCallSp.GetCompanyName()[0];
                }
            }
        }

        public void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (dtbl != null && dtbl.Rows.Count > 0)
            {
                grdSplash.Visibility = System.Windows.Visibility.Hidden;
                grdLogIn.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                label1.Content = "Sorry, Cannot access database!.";
                label1.Foreground = Brushes.DarkKhaki;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            //string encryptedComingPassword = new Security().base64Encode(txtPassword.Password);
            if (objCallSp.GetLoginCheck(txtUseName.Text, txtPassword.Password))
            {
                MainWindow objMainWindow = new MainWindow();
                this.Close();
                objMainWindow.Show();
            }
            else
            {
                label4.Visibility = System.Windows.Visibility.Visible;
                txtUseName.Text = "";
                txtPassword.Password = "";
                txtUseName.Focus();
            }

        }


        private void CompanyPickPopUp_Opened(object sender, EventArgs e)
        {
            button1.Visibility = System.Windows.Visibility.Hidden;
        }

        private void CompanyPickPopUp_Closed(object sender, EventArgs e)
        {
            button1.Visibility = System.Windows.Visibility.Visible;
        }

        CallSP objCallSp = new CallSP();

        private void txtUseName_KeyUp(object sender, KeyEventArgs e)
        {
            label4.Visibility = System.Windows.Visibility.Hidden;
        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            label4.Visibility = System.Windows.Visibility.Hidden;
        }

        private void button_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Button).Foreground = Brushes.Black;
        }

        private void button_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Button).Foreground = Brushes.White;
        }
    }
}
