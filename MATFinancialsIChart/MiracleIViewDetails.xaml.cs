﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MATFinancialsI.BLL;
using System.Threading;

namespace MATFinancials.MATFinancialsIChart
{
    /// <summary>
    /// Interaction logic for MiracleIViewDetails.xaml
    /// </summary>
    public partial class MiracleIViewDetails : Window
    {
        public MiracleIViewDetails()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(new ThreadStart(FunctionViewDetail));
            t.Start();
        }
        public void FunctionViewDetail()
        {
            dataGrid1.Dispatcher.Invoke(
          new Action(
           () =>
           {
               CallSP SPCall = new CallSP();
               dataGrid1.ItemsSource = SPCall.GetViewDetailsFill()[0].DefaultView;
           }));
        }

        private void dataGrid1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            lblLoading.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
