﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancials.MATFinancialsIChart.Charts
{
	public class HorizontalAxis : NumericAxis
	{
		public HorizontalAxis()
		{
			Placement = AxisPlacement.Bottom;
		}

		protected override void ValidatePlacement(AxisPlacement newPlacement)
		{
			if (newPlacement == AxisPlacement.Left || newPlacement == AxisPlacement.Right)
				throw new ArgumentException(Properties.Resources.HorizontalAxisCannotBeVertical);
		}
	}
}
