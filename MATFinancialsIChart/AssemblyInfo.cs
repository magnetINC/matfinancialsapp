﻿
 
using System;
using System.Resources;
using System.Windows.Markup;
using System.Runtime.CompilerServices;
using MATFinancials.MATFinancialsIChart;
using System.Diagnostics.CodeAnalysis;

[module: SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]

[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Navigation")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Charts.Navigation")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Charts")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.DataSources")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Common.Palettes")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Charts.Axes")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.PointMarkers")]
[assembly: XmlnsDefinition(D3AssemblyConstants.DefaultXmlNamespace, "MATFinancials.MATFinancialsIChart.Charts.Shapes")]

[assembly: XmlnsPrefix(D3AssemblyConstants.DefaultXmlNamespace, "StinoKJames")]

[assembly: CLSCompliant(true)]
[assembly: InternalsVisibleTo("MATFinancialsIChart.Test, PublicKey=002400000480000094000000060200000024000052534131000400000100010039f88065585acdedaac491218a8836c4c54070b4b0f85bc909bd002856b509349f95fc845d1d4c664ea6b93045f2ada3b4fe70c6cd9b3fb615f94b8b5f67e4ea8ea5decb233e2e3c0ce84b78dc3ca0cd9fd2260792ece12224fca5813f03c7ad57b1faa07e3ca8fafb278fa23976fc7a35b8b4ae4efedacd1e193d89738ac2aa")]
[assembly: InternalsVisibleTo("MATFinancialsIChart.Maps, PublicKey=002400000480000094000000060200000024000052534131000400000100010039f88065585acdedaac491218a8836c4c54070b4b0f85bc909bd002856b509349f95fc845d1d4c664ea6b93045f2ada3b4fe70c6cd9b3fb615f94b8b5f67e4ea8ea5decb233e2e3c0ce84b78dc3ca0cd9fd2260792ece12224fca5813f03c7ad57b1faa07e3ca8fafb278fa23976fc7a35b8b4ae4efedacd1e193d89738ac2aa")]

namespace MATFinancials.MATFinancialsIChart
{
	public static class D3AssemblyConstants
	{
        public const string DefaultXmlNamespace = "http://yudiz.com/MiracleIChart/1.0";
       // public const string DefaultXmlNamespace = "http://magnetgroupng.com/matfinancials/1.0";

    }
}
