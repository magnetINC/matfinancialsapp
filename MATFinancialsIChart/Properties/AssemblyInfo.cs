﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MATFInancialsIChart")]
[assembly: AssemblyDescription("Controls for visualization of MAT Financials data")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Magnet Consulting Associates Limited")]
[assembly: AssemblyProduct("MATFinancialsIChart")]
[assembly: AssemblyCopyright("Copyright © 2016 Magnet Consulting Associates Limited")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(true)]

//In order to begin building localizable applications, set 
//<UICulture>CultureYouAreCodingWith</UICulture> in your .csproj file
//inside a <PropertyGroup>.  For example, if you are using US english
//in your source files, set the <UICulture> to en-US.  Then uncomment
//the NeutralResourceLanguage attribute below.  Update the "en-US" in
//the line below to match the UICulture setting in the project file.

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]

//[assembly: ThemeInfo(
//    ResourceDictionaryLocation.SourceAssembly,
//    ResourceDictionaryLocation.SourceAssembly
//)]


[assembly: AssemblyVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
[assembly: AssemblyFileVersionAttribute("1.0.0.0")]
